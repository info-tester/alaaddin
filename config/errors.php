<?php 
$errors = [
	'-32700' => [
		'code' 		=> '-32700',
		'message'	=> 'Parse error',
		'meaning'	=> 'Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.'
	],
	'-33000' => [
		'code' 		=> '-33000',
		'message'	=> 'Email is already exists.',
		'meaning'	=> 'This email you entered is already exists. Please try with another email.'
	],
	'-33001' => [
		'code' 		=> '-33001',
		'message'	=> 'Profile picture has been successfully updated.',
		'meaning'	=> 'Your profile picture has been successfully changed.'
	],
	'-33002' => [
		'code' 		=> '-33002',
		'message'	=> 'This email is already exists. Please verify your mail id.',
		'meaning'	=> 'You are using the mail is already exist in the database and it is unverified.'
	],
	'-33003' => [
		'code' 		=> '-33003',
		'message'	=> 'This email is already exists. Your account is temporarily inactive. Please write to '. env('SUPPORT_EMAIL'),
		'meaning'	=> 'Account is temporarily inactivate by admin. contact our support team.'
	],
	'-33004' => [
		'code' 		=> '-33004',
		'message'	=> 'This email is already exists. Register with another email .',
		'meaning'	=> 'This email is already using an another account.'
	],
	'-33005' => [
		'code' 		=> '-33005',
		'message'	=> 'This account has been deleted from our site. Please contact our support team.',
		'meaning'	=> 'Account deleted by user or admin.'
	],
	'-33006' => [
		'code' 		=> '-33006',
		'message'	=> 'This email is available.',
		'meaning'	=> 'Email is available.'
	],
	'-33007' => [
		'code' 		=> '-33007',
		'message'	=> 'Invalid email format.',
		'meaning'	=> 'The email entered is not valid.'
	],
	'-33008' => [
		'code' 		=> '-33008',
		'message'	=> 'Email is mandatory',
		'meaning'	=> 'Email is mandatory'
	],
	'-33009' => [
		'code' 		=> '-33009',
		'message'	=> 'Wrong login credentials',
		'meaning'	=> 'This email-id or phone number does not exist'
	],
	'-33010' => [
		'code' 		=> '-33010',
		'message'	=> 'Could not create token',
		'meaning'	=> 'Token Could not be created'
	],
	'-33017' => [
		'code' 		=> '-33017',
		'message'	=> 'Profile updated successfully',
		'meaning'	=> 'You have updated your profile'
	],
	'-33018' => [
		'code' 		=> '-33018',
		'message'	=> 'No information',
		'meaning'	=> 'The information is not present in our database'
	],
	'-33029' => [
		'code' 		=> '-33029',
		'message'	=> 'Invalid request',
		'meaning'	=> 'Invalid request'
	],
	'-33034' => [
		'code' 		=> '-33034',
		'message'	=> 'Thank you for registering',
		'meaning'	=> 'Thank you for registering.'
	],
	'-33039' => [
		'code' 		=> '-33039',
		'message'	=> 'Error',
		'meaning'	=> 'Wrong OTP verification code'
	],
	'-33040' => [
		'code' 		=> '-33040',
		'message'	=> 'Account verified, registration was successful.',
		'meaning'	=> 'Account verified, registration was successful.'
	],
	'-33041' => [
		'code' 		=> '-33041',
		'message'	=> 'No result',
		'meaning'	=> 'Not found.'
	],
	'-33042' => [
		'code' 		=> '-33042',
		'message'	=> 'Password changed correctly',
		'meaning'	=> 'Password changed correctly'
	],
	'-33049' => [
		'code' 		=> '-33049',
		'message'	=> 'Order inserted correctly',
		'meaning'	=> 'Order successfully registered'
	],
	'-33051' => [
		'code' 		=> '-33051',
		'message'	=> 'Merchant not found!',
		'meaning'	=> 'Merchant not found!'
	],
	'-33058' => [
		'code' 		=> '-33058',
		'message'	=> 'Profile picture updated.',
		'meaning'	=> 'Profile picture updated.'
	],
	'-33059' => [
		'code' 		=> '-33059',
		'message'	=> 'Profile updated',
		'meaning'	=> 'Profile updated'
	],
	'-33072' => [
		'code' 		=> '-33072',
		'message'	=> 'Password reset successful.',
		'meaning'	=> 'Password reset successful.'
	],
	'-33073' => [
		'code' 		=> '-33073',
		'message'	=> 'Verification successful.',
		'meaning'	=> 'Verification code OK'
	],
	'-33077' => [
		'code' 		=> '-33077',
		'message'	=> 'Email not verified',
		'meaning'	=> 'Your account is not verified'
	],
	'-33078' => [
		'code' 		=> '-33078',
		'message'	=> 'Your account has been removed by the administrator',
		'meaning'	=> 'Your account has been removed by the administrator'
	],
	'-33079' => [
		'code' 		=> '-33079',
		'message'	=> 'OTP verification code sent successfully',
		'meaning'	=> 'Check your mailbox'
	],
	'-33080' => [
		'code' 		=> '-33080',
		'message'	=> 'Product added',
		'meaning'	=> 'Product successfully added'
	],
	'-33081' => [
		'code' 		=> '-33081',
		'message'	=> 'No category',
		'meaning'	=> 'No categories found'
    ],
    '-33082' => [
		'code' 		=> '-33082',
		'message'	=> 'Updated product',
		'meaning'	=> 'Updated product'
	],
    '-33083' => [
		'code' 		=> '-33083',
		'message'	=> 'Opening updated',
		'meaning'	=> 'Opening updated successfully'
	],
    '-33084' => [
		'code' 		=> '-33084',
		'message'	=> 'Product status',
		'meaning'	=> 'Product status updated'
	],
    '-33085' => [
		'code' 		=> '-33085',
		'message'	=> 'Token Expired',
		'meaning'	=> 'User session has expired'
	],
	'-33086' => [
		'code' 		=> '-33086',
		'message'	=> 'Login error!',
		'meaning'	=> 'Account temporarily inactive'
	],
	'-33087' => [
		'code' 		=> '-33087',
		'message'	=> 'Customer not found!',
		'meaning'	=> 'Customer not found!'
	],
	'-33088' => [
		'code' 		=> '-33088',
		'message'	=> 'Phone is already exists.',
		'meaning'	=> 'This phone you entered is already exists. Please try with another phone.'
	],
	'-33089' => [
		'code' 		=> '-33089',
		'message'	=> 'Awaiting Approval',
		'meaning'	=> 'Your account has been awaiting approval for administrator'
	],
	'-33090' => [
		'code' 		=> '-33090',
		'message'	=> 'Account is inactive',
		'meaning'	=> 'Your account inactive by the administrator'
	],
	'-33091' => [
		'code' 		=> '-33091',
		'message'	=> 'Password update successfully',
		'meaning'	=> 'You have updated your password'
	],
];

if(!defined('ERRORS')) {
	define('ERRORS', $errors);
}