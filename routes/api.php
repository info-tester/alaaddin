<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// without login goes here..
Route::group(['namespace' => 'Api'], function() {
    
    # payment
    Route::post('make-payment', 'Modules\Payment\PaymentController@makePayment');

    

    Route::post('customer-cancel-order','Modules\Profile\ProfileController@customerCancelOrder');
    
    Route::post('getOrderProductSpecificDetails','Modules\Search\SearchController@getOrderProductSpecificDetails');
    Route::post('register','Modules\Auth\RegisterController@register')->name('api.register');
    Route::post('emailverify','Modules\Auth\RegisterController@emailVerify');
    Route::post('resend-code','Modules\Auth\RegisterController@resendVcode');

    Route::post('userverifyLoginViaOtp','Modules\Auth\LoginViaOtp\LoginViaOtpController@userverifyLoginViaOtp');
    Route::post('resend-verify-otp-by-login-via-otp','Modules\Auth\LoginViaOtp\LoginViaOtpController@userResendOtpVerifyByLogin');

    Route::post('login','Modules\Auth\LoginController@login')->name('api.login');

    // Route::post('login-driver','Modules\Auth\LoginController@loginDriver');
    Route::post('login-driver','Modules\Auth\DriverLoginController@loginDriver');
    #user forget password
    Route::post('forgot-pass-user','Modules\Auth\ForgotPasswordController@forgotPassword');
    #driver forget password
    Route::post('forgot-pass-driver','Modules\Auth\ForgotPasswordController@forgotPasswordDriver');
    #User update password
    Route::post('update-pass-user','Modules\Auth\ForgotPasswordController@passwordUpdate');//user
    #driver update password
    Route::post('update-pass-driver','Modules\Auth\ForgotPasswordController@passwordUpdateDriver');//driver
    Route::post('get-categories','Modules\Search\SearchController@getCategories');
    Route::post('home/{deviceId?}','Modules\Search\SearchController@index');
    Route::post('see-all/{deviceId?}','Modules\Search\SearchController@seeAll');
    Route::post('see-all-reviews-of-product','Modules\Search\SearchController@seeAllProductReviews');
    Route::get('home-new/{deviceId?}','Modules\Search\SearchController@index1');
    Route::post('product-by-feature','Modules\Search\SearchController@showProductByFeature');
    Route::post('change-lang','Modules\Search\SearchController@changeLanguage');
    Route::post('search','Modules\Search\SearchController@searchProducts');
    Route::post('all-products','Modules\Search\SearchController@allProducts');
    Route::post('filter','Modules\Search\SearchController@getFilterVal');
    Route::post('product-details','Modules\Product\ProductController@productDetails');
    Route::post('see-all-related-products','Modules\Product\ProductController@seeAllRelatedProducts');
    Route::post('product-details-new','Modules\Product\ProductController@productDetails1');
    Route::post('product-price','Modules\Product\ProductController@productPrice');
    Route::post('merchant-public-profile','Modules\Merchant\MerchantController@merchantProfile');
    Route::post('see-all-specfic-merchant-related-products','Modules\Merchant\MerchantController@seeAllmerchantrelatedProducts');

    
    Route::post('check-customer-info-by-google-id', 'Modules\Auth\GoogleSignupLoginController@checkcustomerinfobygoogleid');
    Route::post('customer-login-via-google-step-1', 'Modules\Auth\GoogleSignupLoginController@customerLoginViaGoogleStep1');
    Route::post('customer-verify-otp', 'Modules\Auth\GoogleSignupLoginController@customerVerifyOtp');
    

    #facebook login
    Route::post('facebook-login', 'Modules\Auth\SocialLoginController@facebookLogin');
    Route::post('google-login', 'Modules\Auth\SocialLoginController@googleLogin');
    Route::post('apple-login', 'Modules\Auth\SocialLoginController@appleLogin');

    

    #content management api
    Route::get('view-about-us', 'Modules\ContentManagement\ContentManagementController@showAboutUs');
    Route::get('show-contact-us', 'Modules\ContentManagement\ContentManagementController@showContactUs');
    // Route::get('show-contact-us', 'Modules\ContentManagement\ContentManagementController@showContactUs');
    Route::get('show-faq', 'Modules\ContentManagement\ContentManagementController@showFaq');
    Route::post('show-details-faq', 'Modules\ContentManagement\ContentManagementController@showDetailsFaq');
    Route::get('show-terms', 'Modules\ContentManagement\ContentManagementController@showTerms');
    Route::get('show-help', 'Modules\ContentManagement\ContentManagementController@showHelp');
    Route::get('show-privacy', 'Modules\ContentManagement\ContentManagementController@showPrivacy');
    Route::post('post-contact-us-form', 'Modules\ContentManagement\ContentManagementController@postContactUsForm');
    Route::post('send-notify','Modules\ContentManagement\ContentManagementController@sendNotify');
    Route::post('see-all-merchant-reviews','Modules\ContentManagement\ContentManagementController@seeAllMerchantReviews');

    Route::get('show-razorpaykeyandsecretkey','Modules\PaymentKeyTable\PaymentKeyTableController@showpaymentkeytable');

    # share product from app redirect to play store
    Route::get('share', function(Request $request) {
        return redirect()->to(env('ANDROID_LINK'));
    });

    Route::post('update-payment-method', 'Modules\PlaceOrder\PlaceOrderController@updatePaymentMethod');
    Route::get('apple-app-site-association', 'Modules\Search\SearchController@appleShare');
    Route::get('invalidateToken/{token}', 'Modules\ContentManagement\ContentManagementController@invalidateToken');

    Route::post('update-firebase-token', 'Modules\Product\ProductController@updateFirebaseToken');

    //merchant
    Route::post('merchant-register','Modules\Merchant\Auth\RegisterController@register');
    Route::post('merchant-otp-verification-by-login','Modules\Merchant\Auth\RegisterController@MerchantOtpVerifyByLogin');
    Route::post('merchant-email-verify','Modules\Merchant\Auth\RegisterController@emailVerify');
    Route::post('merchant-resend-code','Modules\Merchant\Auth\RegisterController@resendVcode');

    
    Route::post('merchant-resend-otp-verify-by-login','Modules\Merchant\Auth\LoginViaOtp\LoginViaOtpController@MerchantResendOtpVerifyByLogin');

    Route::post('merchant/login','Modules\Merchant\Auth\LoginController@login');
    Route::post('forgot-pass-merchant','Modules\Merchant\Auth\ForgotPasswordController@forgotPassword');
    Route::post('update-pass-merchant','Modules\Merchant\Auth\ForgotPasswordController@passwordUpdate');
    Route::post('merchant-google-login', 'Modules\Auth\SocialLoginController@MerchantgoogleLogin');
    
    Route::post('check-merchant-info-by-google-id', 'Modules\Merchant\Auth\GoogleSignupLoginController@checkmerchantinfobygoogleid');
    Route::post('merchant-login-via-google-step-1', 'Modules\Merchant\Auth\GoogleSignupLoginController@merchantLoginViaGoogleStep1');
    Route::post('merchant-otp-verification', 'Modules\Merchant\Auth\GoogleSignupLoginController@MerchantOtpVerification');
    
    
    
    Route::get('get-all-country','Modules\Profile\ProfileController@getCountry');
    Route::post('get-all-city','Modules\Profile\ProfileController@getAllCity');
    Route::post('get-all-state','Modules\Profile\ProfileController@getAllState');
    

    Route::post('get-unit-master-and-category','Modules\Merchant\Product\ProductController@getUnitMasterAndCategory');

});

Route::group(['namespace' => 'Api','middleware' => ['jwt.auth']], function () {

    # cart functionality
    Route::get('get-cart/{deviceId?}', 'Modules\Cart\CartController@index');
    Route::post('add-to-cart', 'Modules\Cart\CartController@addToCart');
    Route::post('update-cart', 'Modules\Cart\CartController@updateCart');
    Route::get('remove-cart/{cartDetailsId}', 'Modules\Cart\CartController@removeCart');

    #checkout page
    Route::get('checkout/{deviceId?}', 'Modules\Checkout\CheckoutController@getCheckoutData');
    Route::post('checkout', 'Modules\Checkout\CheckoutController@submitCheckout');
    Route::post('checkout1', 'Modules\Checkout\CheckoutController@submitCheckout1');
    
    Route::post('customer-update-bank-details','Modules\Profile\ProfileController@updateCustomerBankDetails');
    Route::post('show-customer-bank-details','Modules\Profile\ProfileController@showCustomerBankDetails');
    
    Route::post('customer-wallet-history','Modules\WalletDetails\WalletDetailsController@customerwalletHistory');

    Route::post('user-details','Modules\Profile\ProfileController@userDetails');
    Route::post('user-update','Modules\Profile\ProfileController@updateUser');
    Route::post('user-change-password','Modules\Profile\ProfileController@userChangePassword');
    Route::post('user-emailverify','Modules\Profile\ProfileController@userEmailVerify');
    Route::get('user-dashboard','Modules\Profile\ProfileController@userDashboard');
    Route::get('edit-profile','Modules\Profile\ProfileController@editProfile');

    // order history post method with pagination
    Route::post('order-history','Modules\Profile\ProfileController@orderHistory');

    // Route::get('order-history','Modules\Profile\ProfileController@orderHistoryOldWithoutPagination');
    Route::get('order-history-details/{id}','Modules\Profile\ProfileController@orderHistoryDetails');
    Route::get('customer-reward-points','Modules\Profile\ProfileController@rewardPoints');
    Route::get('payment-history','Modules\Profile\ProfileController@paymentHistory');
    // Route::get('my-wish-list','Modules\Profile\ProfileController@wishListOld');
    Route::post('my-wish-list','Modules\Profile\ProfileController@wishList');
    Route::post('remove-my-wish-list/{id}','Modules\Profile\ProfileController@removeWishlist');
    // Route::post('add-fav/{id}','Modules\Profile\ProfileController@addFavorite');

    Route::post('add-fav/{id}','Modules\Profile\ProfileController@addToFev');
    Route::post('user-address-book','Modules\Profile\ProfileController@userAddressBook');
    Route::post('update-address-book','Modules\Profile\ProfileController@updateUserAddressBook');
    Route::post('set-default-address-book','Modules\Profile\ProfileController@setDefault');
    Route::post('delete-address-book','Modules\Profile\ProfileController@deleteAddressBook');
    Route::get('edit-address-book/{id}','Modules\Profile\ProfileController@editAddressBook');
    Route::post('add-address-book','Modules\Profile\ProfileController@storeAddressBook');
    Route::post('forget-password-link','Modules\Profile\ProfileController@forgetPasswordCustomer');
    Route::post('post-review','Modules\Profile\ProfileController@submitReviewRatings');
    
    //Driver App
    Route::post('dashboard','Modules\Driver\DriverController@dashboard');
    Route::post('order-list','Modules\Driver\DriverController@orderList');
    Route::post('order-assign','Modules\Driver\DriverController@orderAssign');
    Route::get('order-details/{id}','Modules\Driver\DriverController@orderDetails');
    // Route::get('driver-profile','Modules\Driver\DriverController@driverProfile');
    Route::post('change-password','Modules\Driver\DriverController@changePassword');
    Route::post('get-language-details','Modules\Driver\DriverController@getLanguageDetails');
    Route::post('change-payment-method','Modules\Driver\DriverController@changePaymentMethod');
    Route::post('add-notes','Modules\Driver\DriverController@addNotes');
    Route::post('change-status','Modules\Driver\DriverController@changeStatus');
    Route::post('search-list-datewise','Modules\Driver\DriverController@searchListDatewise');
    Route::post('kuwait-city','Modules\Driver\DriverController@getAllCity');

    //logout
    Route::post('logout','Modules\Auth\LoginController@logout');
    
    Route::post('logout-driver','Modules\Auth\DriverLoginController@logoutDriver');
    // Route::post('logout-driver','Modules\Auth\LoginController@logoutDriver');

    # place order
    Route::get('get-order-data/{orderId}', 'Modules\PlaceOrder\PlaceOrderController@getOrderData');
    Route::post('place-order', 'Modules\PlaceOrder\PlaceOrderController@placeOrder');
    
    
    Route::post('apply-coupon', 'Modules\PlaceOrder\PlaceOrderController@applyCoupon');
    
    
});

//merchant panel
Route::group(['namespace' => 'Api','middleware' => ['jwt.auth']], function () {
    Route::post('merchant-delete-request','Modules\Merchant\Finance\FinanceController@requestdelete');
    Route::post('merchant-details','Modules\Merchant\Profile\ProfileController@merchantDetails');
    Route::post('merchant-update','Modules\Merchant\Profile\ProfileController@updateMerchant');
    Route::post('merchant-change-password','Modules\Merchant\Profile\ProfileController@merchantChangePassword');
    Route::post('merchant-update-email-verify','Modules\Merchant\Profile\ProfileController@merchantEmailVerify');
    Route::get('merchant-dashboard','Modules\Merchant\Profile\ProfileController@merchantDashboard');
    Route::get('merchant-edit-profile','Modules\Merchant\Profile\ProfileController@editMerchantProfile');
    Route::post('merchant-update-bank','Modules\Merchant\Profile\ProfileController@updateMerchantBank');
    Route::post('merchant-product-list','Modules\Merchant\Product\ProductController@index');
    
    
    
    Route::post('merchant-store-product','Modules\Merchant\Product\ProductController@merchantStoreProduct');
    Route::post('merchant-edit-product','Modules\Merchant\Product\ProductController@merchantEditProduct');
    Route::post('merchant-update-product','Modules\Merchant\Product\ProductController@merchantUpdateProduct');
    Route::post('merchant-product-status/{id?}','Modules\Merchant\Product\ProductController@statusProduct');
    Route::post('merchant-product-delete/{id?}','Modules\Merchant\Product\ProductController@deleteProduct');
    Route::post('merchant-product-set-default-image/{id?}','Modules\Merchant\Product\ProductController@setDefaultImg');
    Route::post('merchant-set-default-image-of-product','Modules\Merchant\Product\ProductController@merchantSetDefaultImageOfProduct');
    Route::post('merchant-product-image-delete/{id?}','Modules\Merchant\Product\ProductController@removeImg');
    Route::post('merchant-order-history','Modules\Merchant\Order\OrderController@index');
    Route::get('merchant-order-details-{id?}','Modules\Merchant\Order\OrderController@viewOrderDetails');
    Route::post('merchant-order-status-change','Modules\Merchant\Order\OrderController@orderStatusChange');
    Route::post('merchant-earning','Modules\Merchant\Finance\FinanceController@earningReport');
    Route::post('merchant-withdraw-list','Modules\Merchant\Finance\FinanceController@merchantWithdrawlist');
    Route::post('merchant-withdraw-request','Modules\Merchant\Finance\FinanceController@requestWithdraw');
    Route::post('show-merchant-due','Modules\Merchant\Finance\FinanceController@showMerchantDue');
    Route::get('get-merchant-due','Modules\Merchant\Finance\FinanceController@getMerchantDue');
    Route::post('merchant-reviews','Modules\Merchant\Profile\ProfileController@MerchantReviews');
    // Route::get('merchant-reviews','Modules\Merchant\Profile\ProfileController@MerchantReviewsOldWithoutPagination');
});