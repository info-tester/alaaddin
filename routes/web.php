<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Modules'], function() {
	
	# Home page routing
	Route::get('/', 'Home\HomeController@index')->name('home');
	
	

	Route::get('/add-to-fevourite/{id}', 'Home\HomeController@addFavorite');
	Route::get('change-language', 'Home\HomeController@changeLanguage')->name('change.lang');
	Route::get('/send-notification-test/{no}', 'Home\HomeController@sendNotificationTest')->name('send.notification.test');
	Route::get('/send-notification-cron', 'Home\HomeController@sendNotificatonCron')->name('send.notification.cron');
	//Route::get('/merchant-all-earning', 'Home\HomeController@merchantEarningAllDetails')->name('merchant.all.earning');
	# search for vue js
	Route::get('search/{cat?}', 'Search\SearchController@index')->name('search');

	Route::post('search-products-vue', 'Search\SearchController@searchProductsVue')->name('search.products.vue');

	Route::get('check-cat-or-subcat/{slug}', 'Search\SearchController@checkCatOrSubCat')->name('check.cat.or.subcat');
	Route::get('get-brands/{id}', 'Search\SearchController@getBrands')->name('get.brands');
	Route::get('get-parent-category/{subCatId}', 'Search\SearchController@getParentCategory')->name('get.parent.categories');
	Route::get('get-sub-categories/{id}', 'Search\SearchController@getSubCategories')->name('get.sub.categories');
	Route::get('get-variants/{id}', 'Search\SearchController@getVariants')->name('get.variants');

	# product details 
	Route::get('product/{slug}', 'Product\ProductController@index')->name('product');
	Route::get('get-product-details/{slug}', 'Product\ProductController@getProduct')->name('product.details');

	Route::post('get-variant-price', 'Product\ProductController@getVariantPrice');
	
	Route::get('get-all-variants/{slug}', 'Product\ProductController@getAllVariants');
	Route::get('get-product-price/{slug}', 'Product\ProductController@getProductPrice');

	Route::get('check-loggedin', 'Home\HomeController@checkLoggedin')->name('check.loggedin');

	Route::get('add-to-fev/{id}', 'Search\SearchController@addToFev');
	Route::post('autocomplete/keyword', 'Product\ProductController@fetchProducts')->name('autocomplete.keyword');

	// cart module
	Route::post('add-to-cart', 'Cart\CartController@addToCart')->name('add.to.cart');
	Route::get('cart', 'Cart\CartController@index')->name('cart');
	Route::post('update-cart', 'Cart\CartController@updateCart')->name('update.cart');
	Route::get('remove-cart/{id}', 'Cart\CartController@removeCart')->name('remove.cart');

	// check-out module
	Route::get('guest-check-out', 'CheckOut\CheckOutController@checkOut')->name('guest.check.out');

	// Route::get('guest-order-details/{orderNo}', 'GuestOrder\GuestOrderController@guestOrderDetails')->name('guest.order.details');
	Route::get('guest-edit-address/{orderNo}', 'CheckOut\CheckOutController@editAddress')->name('guest.edit.address');
	Route::post('store-oreder', 'CheckOut\CheckOutController@storeOreder')->name('store.oreder');

	// user-check-out module
	Route::get('check-out', 'CheckOut\UserCheckOutController@checkOut')->name('check.out');
	Route::get('user-edit-address/{orderNo}', 'CheckOut\UserCheckOutController@editAddress')->name('user.edit.address');
	Route::post('user-store-oreder', 'CheckOut\UserCheckOutController@storeOreder')->name('user.store.oreder');
	Route::get('place-order/{orderno}', 'Order\OrderController@orderPlacement')->name('order.placement');
	Route::post('order-placement/{orderno}', 'Order\OrderController@placeOrder')->name('place.order');
	
	Route::get('order-success', 'Order\OrderController@orderSuccess')->name('order.success');

	// Route::get('guest-order-details/{orderNo}', 'GuestOrder\GuestOrderController@guestOrderDetails')->name('guest.order.details');
	Route::get('track-order', 'GuestOrder\GuestOrderController@guestOrderDetails')->name('guest.order.details');

	//For user 
	Route::get('dashboard', 'User\UserController@viewDashboard')->name('dashboard');
	Route::get('edit-profile', 'User\UserController@editProfile')->name('user.edit.profile');

	Route::post('fetch-kuwait-customer-details', 'User\UserController@fetchAllCityDetails')->name('fetch.kuwait.customer.details');
	Route::post('store-user-details', 'User\UserController@storeUserDetails')->name('store.user.details');
    Route::post('check-new-email', 'User\UserController@chkNewEmailExist')->name('user.check.new.email');
    Route::get('resend-email', 'User\UserController@reSendVerificationMail')->name('resend.email');
	Route::get('my-wishlist', 'User\UserController@myWishlist')->name('user.wishlist');
	Route::get('remove-wishlist/{id}', 'User\UserController@removeWishlist')->name('user.remove.wishlist');

	//Address book
	Route::get('address-book', 'Address\AddressController@showAddressBook')->name('user.show.address.book');
	Route::get('add-address-book', 'Address\AddressController@addAddressBook')->name('user.add.address.book');
	Route::post('store-address-book', 'Address\AddressController@storeAddressBook')->name('store.user.address');
	Route::get('edit-address-book/{id}', 'Address\AddressController@showEditAddressBook')->name('user.edit.address.book');
	Route::post('update-address-book/{id}', 'Address\AddressController@updateAddressBook')->name('user.update.address.book');
	Route::get('remove-address/{id}', 'Address\AddressController@removeAddress')->name('user.remove.address');
	Route::get('set-def-address/{id}', 'Address\AddressController@setDefault')->name('user.set.def.address');
	Route::post('/autocomplete/city', 'Address\AddressController@fetchCity')->name('autocomplete.city');
	Route::post('/check/city', 'Address\AddressController@checkCity')->name('exist.city.check');

	//user reward
	Route::get('/rewards-point', 'Address\AddressController@rewardsPoint')->name('rewards.point');

	//merchant profile
	Route::get('merchant-profile/{slug}', 'Merchant\MerchantController@index')->name('merchant.profile');
	Route::any('seller', 'Merchant\MerchantController@sellerSearch')->name('seller.search');

	Route::get('order-history', 'OrderHistory\OrderHistoryController@manageOrders')->name('order.history.list');

	Route::get('order-details/{order_no}','OrderHistory\OrderHistoryController@orderDetails')->name('order.history.order.details');
	Route::post('review-post-customer/{id}', 'OrderHistory\OrderHistoryController@postReviewCustomer')->name('post.review.customer');
	Route::get('order-cancel/{order_no}','OrderHistory\OrderHistoryController@orderCancel')->name('order.history.order.cancel');
	Route::get('payment-history','OrderHistory\OrderHistoryController@showPaymentHistory')->name('order.online.payment.history');

	// payment
	Route::get('payment/init','Payment\PaymentController@init')->name('payment.init');
	Route::post('execute-payment','Payment\PaymentController@createInvoice')->name('execute.payment');
	Route::post('submit-payment','Payment\PaymentController@submitPayment')->name('submit.payment');

	# if direct payment not enable
	Route::get('payment-notify/{id}','Payment\PaymentController@notifyPayment')->name('payment.notify');
	Route::get('payment-cancel/{id}','Payment\PaymentController@errorPayment')->name('payment.error');
	Route::get('payment-failed/{id}','Payment\PaymentController@paymentFailed')->name('payment.failed');

	Route::get('under-maintanance', 'Home\HomeController@underMaintanance')->name('under.maintanance');

	// Driver review through mail
	Route::get('driver-review/{order_no}/{user_id}/{token}','Driver_review\DriverReviewContoller@index')->name('driver.review.mail');
	Route::post('submit-driver-review','Driver_review\DriverReviewContoller@submitDriverReview')->name('submit.driver.review.mail');
});

Auth::routes();

//user login and signup
Route::post('check-email', 'Auth\RegisterController@chkEmailExist')->name('user.check.email');
Route::post('check-email-forget-password', 'Auth\RegisterController@chkEmailForgetPassword')->name('user.check.email.forget.password');
Route::get('/user/verify/{vcode}/{id}', 'Auth\RegisterController@verifyEmail')->name('verify');
Route::get('/user/verify/new/{vcode}/{id}', 'Auth\RegisterController@verifyNewEmail')->name('new.verify');

Route::get('register-success', 'Auth\RegisterController@success')->name('register.success');

//update new emailid by verification mail(change email from old to new)
Route::get('/verify-customer-new-email/{vcode}/{id}', 'Auth\RegisterController@verifyUpdateEmail')->name('verify.new.email.customer');

Route::get('/verify-driver-new-email/{vcode}/{id}', 'Auth\RegisterController@verifyDriverUpdateEmail')->name('verify.new.email.driver');

// for static pages
Route::get('about-us', 'Modules\Others\OthersController@showAboutUs')->name('about.us');
Route::get('contact-us', 'Modules\Others\OthersController@showContactUs')->name('contact.us');

Route::post('contact-us-formpost', 'Modules\Others\OthersController@postContactUsForm')->name('contact.us.formpost');

Route::get('faq', 'Modules\Others\OthersController@showFaq')->name('faq');

Route::get('faq-details/{id}', 'Modules\Others\OthersController@showDetailsFaq')->name('faq.details');

Route::get('terms-conditions', 'Modules\Others\OthersController@showTerms')->name('terms');
Route::get('privacy-policy', 'Modules\Others\OthersController@showPrivacy')->name('privacy');
Route::get('help', 'Modules\Others\OthersController@showHelp')->name('help');

#forget password 
// showFormResetPassword
Route::get('/forget-password','Auth\ForgotPasswordController@showFormResetPassword')->name('user.forget.password.set');
Route::post('/reset-link','Auth\ForgotPasswordController@sendResetPasswordLink')->name('user.link.forget.password.set');
Route::get('/reset-link','Auth\ForgotPasswordController@sendResetPasswordLink')->name('user.link.forget.password.set');
Route::get('/reset-user-password/{id}','Auth\ForgotPasswordController@updateResetPassword')->name('user.update.forget.password.set');
Route::post('/reset-user-password/{id}','Auth\ForgotPasswordController@updateResetPassword')->name('user.update.forget.password.set');
// for social login
Route::get('/login/{social}','Auth\LoginController@socialLogin')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
Route::get('/login/{social}/callback','Auth\LoginController@handleProviderCallback')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
Route::get('social/login/page/{id}', 'Auth\LoginController@facebookEmailLoginPage')->name('facebook.email.login.page');
Route::post('social/login/{id}', 'Auth\LoginController@facebookEmailLogin')->name('facebook.email.login');
Route::post('/get-all-city-kuwait-details', 'Admin\Modules\Orders\OrdersController@fetchCity')->name('fetch.city.all.details');

Route::get('send-whatsapp-message/{id?}', 'Modules\WhatsApp\WhatsAppController@sendMessage')->name('send.whatsapp.message');
//Clear configurations:
Route::get('/config-clear', function() {
    $status = Artisan::call('config:clear');
    return '<h1>Configurations cleared</h1>';
});

//Clear cache:
Route::get('/cache-clear', function() {
    $status = Artisan::call('cache:clear');
    return '<h1>Cache cleared</h1>';
});

//Clear configuration cache:
Route::get('/config-cache', function() {
    $status = Artisan::call('config:cache');
    return '<h1>Configurations cache cleared</h1>';
});
Route::get('/api-gen', function() {
    $status = Artisan::call('l5-swagger:generate');
    return '<h1>api gen</h1>';
});

Route::fallback(function(){
    return view('errors.404');
});


Route::get('/debug-sentry', function () {
    throw new Exception('My 2nd Sentry error!');
});

Route::get('generate-product-code', 'Modules\Product\ProductController@generateProductCode');
Route::get('update-order-driver', 'Modules\Home\HomeController@updateOrderTable');
Route::post('update-firebase-token', 'Modules\Product\ProductController@updateFirebaseToken');
Route::post('apply-coupon', 'Modules\Order\OrderController@applyCoupon')->name('apply.coupon');