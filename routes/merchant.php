<?php

Route::group(['namespace' => 'Merchant'], function() {

    Route::get('/', 'HomeController@index')->name('merchant.dashboard');
    Route::get('/refresh-dashboard', 'HomeController@changeDash')->name('merchant.dashboard.refresh');
    Route::get('change-language', 'HomeController@changeLanguage')->name('merchant.change.lang');

    Route::get('/get-order-stat', 'HomeController@getOrderStat')->name('get.order.stat');

    // Login
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('merchant.login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('merchant.logout');

    // Register
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('merchant.register');
    Route::post('fetch-kuwait-city-name-details', 'Auth\RegisterController@fetchCity')->name('fetch.kuwait.city.all.details');

    Route::post('register', 'Auth\RegisterController@register');
    Route::post('check-email', 'Auth\RegisterController@chkEmailExist')->name('check.email');
    Route::post('check-new-email', 'Auth\RegisterController@chkNewEmailExist')->name('check.new.email');
    Route::get('/verify/{vcode}/{id}', 'Auth\RegisterController@verifyEmail')->name('verify');
    Route::get('/verify/new/{vcode}/{id}', 'Auth\RegisterController@verifyNewEmail')->name('new.verify');
    Route::get('/success', 'Auth\RegisterController@success')->name('success');

    // Passwords
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('merchant.password.email');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('merchant.password.request');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('merchant.password.reset');
    Route::get('edit/profile', 'HomeController@showEditProfile')->name('merchant.edit.profile');
    Route::post('store/profile', 'HomeController@storeProfileDetails')->name('merchant.store.profile');
    Route::post('sate', 'HomeController@fetchState')->name('state.fetch');
    Route::get('/remove-image/{id}', 'HomeController@removeImg')->name('remove.image');

    //------------------------For Products-----------------//    
    Route::any('/merchant-manage-product', 'Modules\Product\ProductController@productView')->name('merchant.manage.product');
    Route::post('/merchant-get-product-datatable', 'Modules\Product\ProductController@index')->name('merchant.get.product.datatable');
    // Route::post('/merchant-manage-product', 'Modules\Product\ProductController@productView')->name('merchant.manage.product');  
    Route::get('/merchant-add-product-step-one', 'Modules\Product\ProductController@productAddFirst')->name('merchant.add.product.step.one');
    Route::post('/merchant-get-sub-category', 'Modules\Product\ProductController@fetchSubcat')->name('merchant.sub.cat.fetch');

    Route::post('/merchant-get-variants', 'Modules\Product\ProductController@getVariants')->name('merchant.get.variants');
    Route::post('/merchant-check-price-dependency', 'Modules\Product\ProductController@checkPriceDependency')->name('merchant.check.price.dependency');
    Route::post('/merchant-store-product-step-one', 'Modules\Product\ProductController@storeProduct')->name('merchant.store.product');
    Route::get('/merchant-add-product-step-two/{slug}', 'Modules\Product\ProductController@productAddSec')->name('merchant.add.product.step.two');
    Route::post('/merchant-store-product-step-two', 'Modules\Product\ProductController@storeProductStepTwo')->name('merchant.store.product.step.two');

    //for ajax store variant, did not done
    Route::post('/merchant-store-product-variant', 'Modules\Product\ProductController@storeProductVariant')->name('merchant.store.product.variant');

    Route::get('/merchant-delete-product-variant/{id}', 'Modules\Product\ProductController@deleteProductVariant')->name('merchant.delete.product.variant');
    Route::get('/merchant-add-product-step-three/{slug}', 'Modules\Product\ProductController@productAddThrd')->name('merchant.add.product.step.three');
    Route::post('/merchant-store-product-step-three', 'Modules\Product\ProductController@storeProductStepThree')->name('merchant.store.product.step.three');
    Route::get('/merchant-add-product-step-four/{slug}', 'Modules\Product\ProductController@productAddFourth')->name('merchant.add.product.step.four');
    Route::post('/merchant-store-product-step-four', 'Modules\Product\ProductController@storeProductStepFour')->name('merchant.store.product.step.four');
    Route::get('/merchant-edit-product/{slug}', 'Modules\Product\ProductController@editProduct')->name('merchant.edit.product');
    Route::post('/merchant-update-product/{slug}', 'Modules\Product\ProductController@updateProduct')->name('merchant.update.product');
    Route::get('/merchant-remove-product/{id}', 'Modules\Product\ProductController@deleteProduct')->name('merchant.remove.product');
    Route::get('/merchant-status-product/{id}', 'Modules\Product\ProductController@statusProduct')->name('merchant.status.product');
    Route::get('/merchant-remove-variant/{id}', 'Modules\Product\ProductController@removeVar')->name('merchant.remove.var');
    Route::get('/merchant-remove-image/{id}', 'Modules\Product\ProductController@removeImg')->name('merchant.remove.image');
    Route::get('/merchant-set-default-product/{id}', 'Modules\Product\ProductController@setDefaultImg')->name('merchant.set.default.product');
    Route::get('/merchant-product-discount/{id}', 'Modules\Product\ProductController@setDiscount')->name('merchant.product.discount');
    Route::post('/merchant-store-product-discount', 'Modules\Product\ProductController@storeDiscount')->name('merchant.store.product.discount');
    Route::get('/merchant-access-deny', 'Modules\Product\ProductController@accessDeny')->name('merchant.access.deny');

    /*************************************Messages*********************************/
    Route::get('/merchant-messages', 'Modules\Messages\MessagesController@showMessages')->name('merchant.message.show');
    Route::get('/merchant-view-conversations/{id}', 'Modules\Messages\MessagesController@viewMessage')->name('merchant.view.message.conversation');
    /*************************************External Orders*********************************/

    //check range of shipping cost
    Route::get('/check-range-exist', 'Modules\ExternalOrder\ExternalOrderController@checkRangeExist')->name('check.range.exist');

    
    Route::get('/get-kuwait-cities', 'Modules\ExternalOrder\ExternalOrderController@getKuwaitCity')->name('merchant.get.kuwait.city');

    //add external order
    Route::get('/merchant-add-external-order', 'Modules\ExternalOrder\ExternalOrderController@addExternalOrder')->name('merchant.add.external.order');

    //add external order
    Route::post('/merchant-add-external-order', 'Modules\ExternalOrder\ExternalOrderController@addExternalOrder')->name('merchant.add.external.order');

    //edit external order
    Route::get('/edit-external-order/{id}', 'Modules\ExternalOrder\ExternalOrderController@editOrderDetails')->name('merchant.edit.external.order');
    Route::post('/edit-external-order/{id}', 'Modules\ExternalOrder\ExternalOrderController@editOrderDetails')->name('merchant.edit.external.order');
    Route::post('/get-all-city-kuwait', 'Modules\ExternalOrder\ExternalOrderController@fetchCity')->name('merchant.external.fetch.city');
    Route::post('/get-all-pickup-city-kuwait', 'Modules\ExternalOrder\ExternalOrderController@fetchCityPickup')->name('merchant.external.fetch.pickup.city');
     //get merchant address
     Route::post('/get-merchant-address', 'Modules\ExternalOrder\ExternalOrderController@getMerchantAddress')->name('merchant.get.merchant.address');

    // Create anonymous external order
    Route::get('/create-external-order/{slug}/{token}', 'Modules\ExternalOrder\ExternalOrderController@addExternalOrder')->name('merchant.create.anonymous.external.order'); 
    Route::post('/create-external-order/{slug}/{token}', 'Modules\ExternalOrder\ExternalOrderController@addExternalOrder')->name('merchant.create.anonymous.external.order'); 
    Route::get('/show-external-order/{slug}/{token}/{id}', 'Modules\ExternalOrder\ExternalOrderController@showExternalOrder')->name('merchant.show.anonymous.external.order'); 

    //Address book
    Route::get('/manage-address-book', 'Modules\Address_book\AddressBookController@manageAddressBook')->name('merchant.manage.address.book');
    Route::post('/manage-address-book', 'Modules\Address_book\AddressBookController@manageAddressBook')->name('merchant.manage.address.book');
    Route::get('/add-address', 'Modules\Address_book\AddressBookController@addAddress')->name('merchant.add.address');
    Route::post('/insert-address', 'Modules\Address_book\AddressBookController@insertAddress')->name('merchant.insert.address');
    Route::get('/edit-address/{id}', 'Modules\Address_book\AddressBookController@editAddress')->name('merchant.edit.address');
    Route::post('/update-address', 'Modules\Address_book\AddressBookController@updateAddress')->name('merchant.update.address');
    Route::post('/delete-address', 'Modules\Address_book\AddressBookController@deleteAddress')->name('merchant.delete.address');

    
    /********************************Orders**********************************************/

    //list of orders
    Route::get('/merchant-order', 'Modules\Orders\OrdersController@manageOrders')->name('merchant.list.order');
    
    //list of orders
    Route::post('/merchant-order', 'Modules\Orders\OrdersController@manageOrders')->name('merchant.list.order');
    //search order 
    Route::post('/merchant-search-order', 'Modules\Orders\OrdersController@searchOrders')->name('merchant.search.order');
    //view order details
    Route::get('/merchant-view-order/{id}', 'Modules\Orders\OrdersController@viewOrderDetails')->name('merchant.view.order');
    //cancel order by merchant
    Route::get('/merchant-cancel-order/{id}', 'Modules\Orders\OrdersController@cancelOrder')->name('merchant.cancel.order');
    //change status of ordered items(processing to shipped to delivered to returned)
    Route::get('/merchant-deliver-order/{id}', 'Modules\Orders\OrdersController@orderStatusChange')->name('merchant.status.change.order');

    Route::get('/bulk-print-orders/{ids}', 'Modules\Orders\BulkPrintOrder@printBulkOrder')->name('merchant.bulk.print.orders');
    
    Route::post('/check/city', 'Modules\Orders\OrdersController@checkCity')->name('merchant.exist.city.check');
    

    /**************************************Finance******************************************************/
    //finance (view earnings ie. commission withdraw etc)
    Route::get('/finance', 'Modules\Finance\FinanceController@index')->name('merchant.finance');

    //request withdraw
    Route::post('/request-withdraw/{id}', 'Modules\Finance\FinanceController@requestWithdraw')->name('merchant.request.withdraw');
    //check duplicate request for withdrawl
    Route::get('/check-duplicate', 'Modules\Finance\FinanceController@checkDuplicate')->name('check.duplicate.request.withdraw');
    //edit external order

    Route::get('/order-report', 'Modules\Report\ReportController@orderReport')->name('merchant.order.report');
    Route::get('/search-order-report', 'Modules\Report\ReportController@searchOrderReport')->name('merchant.search.order.report');
    Route::get('/merchant-order-report-export', 'Modules\Report\ReportController@orderReportExport')->name('merchant.order.report.export');

    Route::get('/earning-report', 'Modules\Report\ReportController@earningReport')->name('merchant.earning.report');
    Route::get('/search-earning-report', 'Modules\Report\ReportController@searchEarningReport')->name('merchant.search.earning.report');
    Route::get('/merchant-earning-report-export', 'Modules\Report\ReportController@earningReportExport')->name('merchant.earning.report.export');

    // Driver review through mail
	Route::get('driver-review/{order_no}/{user_id}/{token}','Modules\Driver_review\ReviewDriverController@index')->name('merchant.driver.review.mail');
    Route::post('submit-driver-review','Modules\Driver_review\ReviewDriverController@submitDriverReview')->name('merchant.submit.driver.review.mail');
    
    //Invoice
    Route::get('invoice-list','Modules\Invoice\MerchantInvoiceController@invoiceList')->name('merchant.invoice');
    Route::post('invoice-list','Modules\Invoice\MerchantInvoiceController@invoiceList')->name('merchant.invoice');
    Route::get('view-invoice/{id}','Modules\Invoice\MerchantInvoiceController@viewInvoice')->name('merchant.view.invoice');
    Route::get('print-invoice-details/{id}','Modules\Invoice\MerchantInvoiceController@printInvoiceDetails')->name('merchant.print.invoice.details');
    Route::get('print-bulk-invoice-details/{id}','Modules\Invoice\MerchantInvoiceController@printBulkInvoice')->name('merchant.print.bulk.invoice.details');
});