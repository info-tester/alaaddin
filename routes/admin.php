<?php

Route::group(['namespace' => 'Admin'], function() {

    Route::get('/', 'HomeController@index')->name('admin.dashboard');
    Route::get('/product-sales-graph', 'HomeController@productSalesGraph')->name('admin.product.sales.graph');
    Route::get('change-language', 'HomeController@changeLanguage')->name('admin.change.lang');

    // Login
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('admin.logout');

    // Register
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('admin.register');
    Route::post('register', 'Auth\RegisterController@register');

    // Passwords
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('admin.password.update');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');

    /*
    |---------------------------------------------------------------------------
    | Settings
    |---------------------------------------------------------------------------
    | This section is used for Settings only
    |
    */
    Route::group(['middleware' => 'App\Http\Middleware\SubAdmin:1'], function() {
        /*************** SubAdmin ************************/
        //list of subadmins
        Route::get('/list-sub-admin', 'Modules\SubAdmin\SubAdminController@index')->name('admin.list.subadmin');

        //search subadmins
        Route::post('/list-sub-admin', 'Modules\SubAdmin\SubAdminController@index')->name('admin.list.subadmin');

        //add subadmin
        Route::get('/add-sub-admin', 'Modules\SubAdmin\SubAdminController@addSubadminDetails')->name('admin.add.subadmin');

        //add subadmin
        Route::post('/add-sub-admin', 'Modules\SubAdmin\SubAdminController@addSubadminDetails')->name('admin.add.subadmin');

        //edit details of subadmin
        Route::get('/edit-sub-admin/{id}', 'Modules\SubAdmin\SubAdminController@editSubAdmin')->name('admin.edit.subadmin');

        //update subadmin details
        Route::post('/edit-sub-admin/{id}', 'Modules\SubAdmin\SubAdminController@editSubAdmin')->name('admin.edit.subadmin');

        //status change of sub admin
        Route::get('/status-sub-admin/{id}', 'Modules\SubAdmin\SubAdminController@statusChange')->name('admin.status.subadmin');

        Route::get('/update-status-sub-admin', 'Modules\SubAdmin\SubAdminController@updateStatus')->name('admin.update.status.subadmin');

        //check duplicate email at the time of creation
        Route::get('/check-sub-admin-email', 'Modules\SubAdmin\SubAdminController@checkAdminDuplicateEmail')->name('admin.check.email');

        //check duplicate email(edit )
        Route::get('/check-sub-admin-change-email', 'Modules\SubAdmin\SubAdminController@checkSubAdminEditChangeEmail')->name('admin.check.change.email');

        //check duplicate phone (edit phone)
        Route::get('/check-sub-admin-change-phone', 'Modules\SubAdmin\SubAdminController@checkSubAdminEditChangePhone')->name('admin.check.change.phone');

        Route::get('/check-sub-admin-phone', 'Modules\SubAdmin\SubAdminController@verifyDuplicatePhone')->name('admin.check.phone');

        //delete
        Route::get('/delete-sub-admin/{id}', 'Modules\SubAdmin\SubAdminController@delete')->name('admin.delete.subadmin');

        Route::get('/delete-sub-admin-details', 'Modules\SubAdmin\SubAdminController@delete')->name('admin.delete.subadmin');

        Route::get('/delete-sub-admin-details','Modules\SubAdmin\SubAdminController@deleteSubadmin')->name('admin.delete.subadmin.details');

        // Sub admin log
        Route::get('/show-subadmin-logs/{id}', 'Modules\SubAdmin\SubAdminController@showLogs')->name('admin.show.logs');

        Route::post('/show-subadmin-logs', 'Modules\SubAdmin\SubAdminController@showLogs')->name('admin.show.logs');
        /******************Push notification*****************************/
        Route::get('/send-push-notification', 'Modules\Messages\MessagesController@sendPushNotification')->name('admin.send.push.notification');
        Route::get('/get-reg-id', 'Modules\Messages\MessagesController@getNotificationRegid')->name('admin.get.reg.id');
        Route::post('/send-push-notification-to-user', 'Modules\Messages\MessagesController@sendPushNotificationToUser')->name('admin.send.push.notification.to.user');
        Route::post('/send-push-notification', 'Modules\Messages\MessagesController@sendPushNotification')->name('admin.send.push.notification');
        
        /**********************************************Messages*************************************************/
        
        Route::get('/manage-messages', 'Modules\Messages\MessagesController@showMessages')->name('admin.list.messages');
        Route::get('/view-messages/{id}', 'Modules\Messages\MessagesController@viewMessage')->name('admin.view.message.details');

        /**********************************************Country*************************************************/
        Route::get('/show-hide-country', 'Modules\Country\CountryController@showHideCountry')->name('admin.show.hide.country');
        Route::get('/manage-country', 'Modules\Country\CountryController@manageCountry')->name('admin.list.country');
        Route::get('/add-country', 'Modules\Country\CountryController@addCountry')->name('admin.add.country');
        Route::post('/add-country', 'Modules\Country\CountryController@addCountry')->name('admin.add.country');
        Route::get('/edit-country/{id}', 'Modules\Country\CountryController@editCountry')->name('admin.edit.country');
        Route::post('/update-country/{id}', 'Modules\Country\CountryController@updateCountry')->name('admin.update.country');
        Route::get('/delete-country/{id}', 'Modules\Country\CountryController@deleteCountry')->name('admin.delete.country');
        /**********************************************City*************************************************/

        //search city by city name
        Route::any('/admin-change-status-of-city', 'Modules\City\CityController@adminchangeStatusOfCity')->name('admin.change.status.of.city');
        Route::get('/list-city', 'Modules\City\CityController@manageCity')->name('admin.list.city');
        Route::post('/list-city', 'Modules\City\CityController@manageCity')->name('admin.list.city');
        Route::any('/list-unit', 'Modules\UnitMaster\UnitMasterController@index')->name('admin.list.unit');
        
        Route::any('/customer-withdraw/{id}', 'Modules\CustomerWithdrawl\CustomerWithdrawlController@customerWithdraw')->name('admin.customer.withdraw');
        
        Route::post('/view-customer-withdraw-details/{id}', 'Modules\CustomerWithdrawl\CustomerWithdrawlController@viewCustomerWithdrawDetails')->name('admin.view.customer.withdraw.details');
        Route::post('/view-customer-specific-orders/{id}', 'Modules\CustomerWithdrawl\CustomerWithdrawlController@viewCustomerSpecificOrders')->name('admin.view.customer.specific.orders');

        Route::any('/edit-unit/{id}', 'Modules\UnitMaster\UnitMasterController@editUnit')->name('admin.edit.unit');
        Route::any('/add-unit', 'Modules\UnitMaster\UnitMasterController@addUnit')->name('admin.add.unit');


        /*************** Settings loyalty point ************************/
        Route::get('/update-loyalty-point', 'Modules\Setting\SettingController@showLoyaltyPoint')->name('admin.uplate.loyalty');
        Route::post('/stor-loyalty-point/{id}', 'Modules\Setting\SettingController@storeLoyaltyPoint')->name('admin.store.loyalty');
        Route::get('/manage-maintenance', 'Modules\Setting\SettingController@manageMaintence')->name('admin.manage.maintenance');
        Route::post('/manage-maintenance', 'Modules\Setting\SettingController@manageMaintence')->name('admin.manage.maintenance');

        /***************Coupon******************************** */
        Route::get('/manage-coupon', 'Modules\Coupon\CouponController@manageCoupon')->name('admin.manage.coupon');
        Route::post('/manage-coupon', 'Modules\Coupon\CouponController@manageCoupon')->name('admin.manage.coupon.post');
        Route::get('/add-coupon', 'Modules\Coupon\CouponController@addCoupon')->name('admin.add.coupon');
        Route::post('/insert-coupon', 'Modules\Coupon\CouponController@insertCoupon')->name('admin.insert.coupon');

        Route::get('/edit-coupon/{id}', 'Modules\Coupon\CouponController@editCoupon')->name('admin.edit.coupon');
        Route::post('/update-coupon', 'Modules\Coupon\CouponController@updateCoupon')->name('admin.update.coupon');
        Route::post('/delete-coupon', 'Modules\Coupon\CouponController@deleteCoupon')->name('admin.delete.coupon');
        Route::post('/check-coupon-code', 'Modules\Coupon\CouponController@checkCouponCode')->name('admin.check.cupon.code');
        /***************Time Slot******************************** */
        Route::get('/manage-time-slot', 'Modules\Time_slots\TimeSlotController@index')->name('admin.manage.time.slot');
        Route::post('/manage-time-slot', 'Modules\Time_slots\TimeSlotController@index')->name('admin.manage.time.slot');
        Route::get('/add-time-slot', 'Modules\Time_slots\TimeSlotController@addTimeSlot')->name('admin.add.time.slot');
        Route::post('/insert-time-slot', 'Modules\Time_slots\TimeSlotController@insertTimeslot')->name('admin.insert.time.slot');
        Route::get('/edit-time-slot/{id}', 'Modules\Time_slots\TimeSlotController@editTimeslot')->name('admin.edit.time.slot');
        Route::post('/update-time-slot', 'Modules\Time_slots\TimeSlotController@updateTimeslot')->name('admin.update.time.slot');
        Route::post('/delete-time-slot', 'Modules\Time_slots\TimeSlotController@deleteTimeslot')->name('admin.delete.time.slot');

        Route::post('/check-time-range', 'Modules\Time_slots\TimeSlotController@checkTimeRange')->name('admin.check.time.range');
    });
    
    /*
    |---------------------------------------------------------------------------
    | Start Prouct Settings
    |---------------------------------------------------------------------------
    | This section is used for Product settings only
    |
    */
    Route::group(['middleware' => 'App\Http\Middleware\SubAdmin:2'], function() {
        /*************** Product Category ************************/
        //list of product category
        Route::get('/list-category', 'Modules\Category\CategoryController@index')->name('admin.list.category');

        //search list of product category
        Route::post('/list-category', 'Modules\Category\CategoryController@index')->name('admin.list.category');

        Route::get('/search-category-list-ajax', 'Modules\Category\CategoryController@searchCategory')->name('admin.search.category');
        // datatable
        Route::post('/load-category-list', 'Modules\Category\CategoryController@searchCategoryDataTable')->name('admin.load.category.list');

        //check duplicate category name
        Route::get('/check-category-name', 'Modules\Category\CategoryController@checkDuplicateCategoryName')->name('admin.check.category.name');

        //add product category
        Route::get('/add-category', 'Modules\Category\CategoryController@addCategory')->name('admin.add.category');

        //add product category details to the categories table & category_details table
        Route::post('/add-category', 'Modules\Category\CategoryController@addCategory')->name('admin.add.category');

        //edit category
        Route::get('/edit-category/{slug}', 'Modules\Category\CategoryController@editCategory')->name('admin.edit.category');

        //update category details
        Route::post('/edit-category/{slug}','Modules\Category\CategoryController@editCategory')->name('admin.edit.category');

        //delete category
        Route::get('/delete-category/{slug}', 'Modules\Category\CategoryController@deleteCategory')->name('admin.delete.category');

        Route::get('/category-delete-permanently', 'Modules\Category\CategoryController@categoryDelete')->name('admin.category.delete.permanently');

        Route::get('/change-status/{slug}', 'Modules\Category\CategoryController@changeStatus')->name('admin.change.status.category');

        Route::get('/update-status', 'Modules\Category\CategoryController@updateStatus')->name('admin.update.status.category');
        // add in menu
        Route::get('/add-category-in-menu/{slug}', 'Modules\Category\CategoryController@AddInMenu')->name('admin.add.in.menu');

        // update add in menu by ajax(yes/no)
        Route::get('/update-add-category-in-menu', 'Modules\Category\CategoryController@UpdateAddInMenu')->name('admin.update.add.in.menu');

        Route::get('/popular-category/{slug}', 'Modules\Category\CategoryController@popularCategory')->name('admin.popular.category.menu');

        // update popular menu by ajax (yes/no)
        Route::get('/update-popular-category', 'Modules\Category\CategoryController@updatePopularCategory')->name('admin.update.popular.category.menu');


        //add category
        // Route::get('/', 'HomeController@addCategory')->name('admin.add.category');

        /***************Manufacturer ************************/

        //list product manufacturer
        Route::get('/list-manufacturer', 'Modules\Manufacturer\ManufacturerController@index')->name('admin.list.manufacturer');

        //search manufacturer
        Route::post('/list-manufacturer', 'Modules\Manufacturer\ManufacturerController@index')->name('admin.list.manufacturer');

        //add product manufacturer
        Route::get('/add-manufacturer', 'Modules\Manufacturer\ManufacturerController@addManufacturer')->name('admin.add.manufacturer');

        //add product manufacturer
        Route::post('/add-manufacturer', 'Modules\Manufacturer\ManufacturerController@addManufacturer')->name('admin.add.manufacturer');

        //edit manufacturer
        Route::get('/edit-manufacturer/{id}', 'Modules\Manufacturer\ManufacturerController@editManufacturer')->name('admin.edit.manufacturer');

        //update manufacturer
        Route::post('/edit-manufacturer/{id}', 'Modules\Manufacturer\ManufacturerController@editManufacturer')->name('admin.edit.manufacturer');

        Route::get('/check-manufacturer-name', 'Modules\Manufacturer\ManufacturerController@checkManufacturerName')->name('admin.check.brand.name');

        Route::get('/delete-manufacturer/{id}', 'Modules\Manufacturer\ManufacturerController@deleteManufacturer')->name('admin.delete.manufacturer');
        //delete by ajax
        Route::get('/delete-manufacturer-details-permanently', 'Modules\Manufacturer\ManufacturerController@deleteManufacturerDetails')->name('admin.delete.manufacturer.details.permanently');

        /************************Product Option***************************/

        // list of product options
        Route::get('/list-product-options','Modules\ProductOption\ProductOptionController@index')->name('admin.list.product.option');

        //search product options
        Route::post('/list-product-options', 'Modules\ProductOption\ProductOptionController@index')->name('admin.list.product.option');

        //add product options
        Route::get('/add-product-option', 'Modules\ProductOption\ProductOptionController@addProductOption')->name('admin.add.product.option');

        //add product options
        Route::post('/add-product-option', 'Modules\ProductOption\ProductOptionController@addProductOption')->name('admin.add.product.option');

        //get all subcategories
        Route::get('/get-sub-categories', 'Modules\ProductOption\ProductOptionController@getSubcategory')->name('admin.get.subcategories');

        //check dependent validation based on combination of category & subcategory
        Route::get('/check-type', 'Modules\ProductOption\ProductOptionController@checkType')->name('admin.check.type');

        Route::get('/check-option-value-name', 'Modules\ProductOption\ProductOptionController@checkDuplicateOptionName')->name('admin.check.duplicate.option.name');

        //edit product option details
        Route::get('/edit-product-option/{id}', 'Modules\ProductOption\ProductOptionController@editProductOption')->name('admin.edit.product.option');

        //update product option details
        Route::post('/edit-product-option/{id}', 'Modules\ProductOption\ProductOptionController@editProductOption')->name('admin.edit.product.option');

        Route::get('/status-product-option/{id}', 'Modules\ProductOption\ProductOptionController@changeOptionStatus')->name('admin.status.option');

        Route::get('/update-status-product-option', 'Modules\ProductOption\ProductOptionController@updateOptionStatus')->name('admin.status.update.option');

        //delete product option
        Route::get('/delete-product-option/{id}', 'Modules\ProductOption\ProductOptionController@deleteProductOption')->name('admin.delete.product.option');

        /**************** Product option values ********************/

        //list of option values
        Route::get('/list-option-values/{id}', 'Modules\ProductOption\ProductOptionController@listOptionValues')->name('admin.list.option.values');

        //list of option values
        Route::post('/list-option-values/{id}', 'Modules\ProductOption\ProductOptionController@listOptionValues')->name('admin.list.option.values');

        //add option values
        Route::post('/add-option-values/{id}', 'Modules\ProductOption\ProductOptionController@addOptionValues')->name('admin.add.option.value');

        //edit product option values
        Route::get('/edit-option-values/{id}', 'Modules\ProductOption\ProductOptionController@editOptionValues')->name('admin.edit.option.value');

        //update product option values
        Route::post('/edit-option-values/{id}', 'Modules\ProductOption\ProductOptionController@editOptionValues')->name('admin.edit.option.value');

        //delete product option values
        Route::get('/delete-option-values/{id}', 'Modules\ProductOption\ProductOptionController@deleteOptionValues')->name('admin.delete.product.option.value');

        Route::get('/delete-option-permanently', 'Modules\ProductOption\ProductOptionController@deleteOption')->name('admin.delete.option.permanently');

        // Route::post('/add-option-values/{default_title}', 'Modules\ProductOption\ProductOptionController@addOptionValues')->name('admin.add.option.value');


        //------------------------For Products-----------------//    
        Route::get('/delete-product-review/{id}', 'Modules\Product\ProductController@deleteReviewProduct')->name('delete.product.review');
        Route::post('/set-featured-product', 'Modules\Product\ProductController@setFeaturedProduct')->name('set.featured.product');
        Route::get('/product-comments-review-ratings/{slug}', 'Modules\Product\ProductController@showProductReviewRatings')->name('product.view.review.ratings');
        Route::any('/manage-product', 'Modules\Product\ProductController@productView')->name('manage.product');  
        Route::post('/get-product-datatable', 'Modules\Product\ProductController@index')->name('get.product.datatable');  
        Route::get('/add-product-step-one', 'Modules\Product\ProductController@productAddFirst')->name('add.product.step.one');
        Route::post('/get-sub-category', 'Modules\Product\ProductController@fetchSubcat')->name('sub.cat.fetch');

        Route::post('/get-variants', 'Modules\Product\ProductController@getVariants')->name('get.variants');
        Route::post('/check-price-dependency', 'Modules\Product\ProductController@checkPriceDependency')->name('check.price.dependency');
        Route::post('/store-product-step-one', 'Modules\Product\ProductController@storeProduct')->name('store.product');
        Route::get('/add-product-step-two/{slug}', 'Modules\Product\ProductController@productAddSec')->name('add.product.step.two');
        Route::post('/store-product-step-two', 'Modules\Product\ProductController@storeProductStepTwo')->name('store.product.step.two');

        //for ajax store variant, did not done
        Route::post('/store-product-variant', 'Modules\Product\ProductController@storeProductVariant')->name('store.product.variant');

        Route::get('/delete-product-variant/{id}', 'Modules\Product\ProductController@deleteProductVariant')->name('delete.product.variant');
        Route::get('/add-product-step-three/{slug}', 'Modules\Product\ProductController@productAddThrd')->name('add.product.step.three');
        Route::post('/store-product-step-three', 'Modules\Product\ProductController@storeProductStepThree')->name('store.product.step.three');
        Route::get('/add-product-step-four/{slug}', 'Modules\Product\ProductController@productAddFourth')->name('add.product.step.four');
        Route::post('/store-product-step-four', 'Modules\Product\ProductController@storeProductStepFour')->name('store.product.step.four');
        Route::get('/edit-product/{slug}', 'Modules\Product\ProductController@editProduct')->name('edit.product');
        Route::post('/update-product/{slug}', 'Modules\Product\ProductController@updateProduct')->name('update.product');
        Route::get('/remove-product/{id}', 'Modules\Product\ProductController@removeProduct')->name('remove.product');
        Route::get('/delete-product', 'Modules\Product\ProductController@deleteProduct')->name('delete.product');
        // change status for product
        Route::get('/status-product', 'Modules\Product\ProductController@statusProduct')->name('status.product');
        // change status for product by ajax call
        Route::get('/update-status-product', 'Modules\Product\ProductController@updateStatusProduct')->name('status.product.update');

        Route::get('/remove-variant/{id}', 'Modules\Product\ProductController@removeVar')->name('remove.var');
        Route::get('/remove-image/{id}', 'Modules\Product\ProductController@removeImg')->name('remove.image');
        Route::get('/set-default-product/{id}', 'Modules\Product\ProductController@setDefaultImg')->name('set.default.product');
        Route::get('/product-discount/{id}', 'Modules\Product\ProductController@setDiscount')->name('product.discount');
        Route::post('/store-product-discount', 'Modules\Product\ProductController@storeDiscount')->name('store.product.discount');

        Route::get('resize-previous-product-images', 'Modules\Product\ProductController@resizePreviousProductImages');
    });
    
    /*
    |---------------------------------------------------------------------------
    | Merchants
    |---------------------------------------------------------------------------
    | This section is used for Merchants only
    |
    */
    Route::group(['middleware' => 'App\Http\Middleware\SubAdmin:4'], function() {
        /**********************Merchant******************************/
        Route::get('/add-driver-auto', 'Modules\Merchant\MerchantController@addDriverAuto')->name('admin.add.driver.assign.auto');
        Route::get('/remove-driver-auto', 'Modules\Merchant\MerchantController@removeDriverAuto')->name('admin.remove.driver.assign.auto');
        //add merchant(redirect to view page)
        Route::get('/add-merchant', 'Modules\Merchant\MerchantController@addMerchant')->name('admin.add.merchant');
        //search merchant by ajax call
        Route::post('/search-merchants', 'Modules\Merchant\MerchantController@searchMerchants')->name('admin.search.merchants');

        //add merchant in database
        Route::post('/add-merchant', 'Modules\Merchant\MerchantController@addMerchant')->name('admin.add.merchant');
        //admin email verify by admin
        Route::get('/verify-merchant-emailid/{id}', 'Modules\Merchant\MerchantController@verifyEmailAccount')->name('admin.verify.merchant.email.id');

        Route::get('/verify-merchant-email-acc', 'Modules\Merchant\MerchantController@verifyMerchantEmailByAdmin')->name('admin.verify.merchant.email.admin');

        //edit merchant(redirect to view page)
        Route::get('/edit-merchant/{id}', 'Modules\Merchant\MerchantController@editMerchant')->name('admin.edit.merchant');

        //edit merchant in database
        Route::post('/edit-merchant/{id}', 'Modules\Merchant\MerchantController@editMerchant')->name('admin.edit.merchant');

        //set commision
        Route::get('/set-commision', 'Modules\Merchant\MerchantController@setCommision')->name('admin.set.commision');

        Route::post('/update-commission','Modules\Merchant\MerchantController@updateCommision')->name('admin.update.merchant.commision');
        
        //set shipping cost
        Route::get('/set-shipping-cost', 'Modules\Merchant\MerchantController@setShippingCost')->name('admin.set.shipping.cost');

        Route::post('/update-shipping-cost', 'Modules\Merchant\MerchantController@updateShippingCost')->name('admin.update.shipping.cost');

        //view merchant profile
        Route::get('/view-merchant-profile/{id}', 'Modules\Merchant\MerchantController@viewMerchantProfile')->name('admin.view.merchant.profile');
        //send message by admin to merchant
        Route::post('/send-message/{id}', 'Modules\Merchant\MerchantController@sendMessage')->name('admin.send.message');

        //manage merchant
        Route::get('/manage-merchant', 'Modules\Merchant\MerchantController@manageMerchant')->name('admin.list.merchant');

        

        //manage merchant
        Route::post('/manage-merchant', 'Modules\Merchant\MerchantController@manageMerchant')->name('admin.list.merchant');
        
        //change status merchant
        Route::get('/status-change/{id}', 'Modules\Merchant\MerchantController@changeStatus')->name('admin.change.status');
        Route::get('/block-unblock-merchant', 'Modules\Merchant\MerchantController@blockUnblockMerchant')->name('admin.block.unblock.merchant');
        Route::get('/hide-show-merchant', 'Modules\Merchant\MerchantController@hideShowMerchant')->name('admin.hide.show.merchant');

        Route::get('/change-email-id', 'Modules\Merchant\MerchantController@changeEmailId')->name('change.merchant.email.id');

        Route::get('/change-phone-no', 'Modules\Merchant\MerchantController@changePhnNo')->name('change.merchant.phone.no');
        //Address book
        Route::get('/show-address-book/{id}', 'Modules\Merchant\MerchantController@showAddressbook')->name('show.address.book');
        Route::post('/show-address-book', 'Modules\Merchant\MerchantController@showAddressbook')->name('show.address.book');
        Route::get('/add-merchant-address/{id}', 'Modules\Merchant\MerchantController@addMerchantAddress')->name('admin.add.merchant.address');
        Route::post('/insert-merchant-address', 'Modules\Merchant\MerchantController@insertMerchantAddress')->name('admin.insert.address.book');
        Route::get('/edit-merchant-address/{id}/{merchant_id}', 'Modules\Merchant\MerchantController@editMerchantAddress')->name('admin.edit.merchant.address');
        Route::post('/update-merchant-address', 'Modules\Merchant\MerchantController@updateMerchantAddress')->name('admin.update.address.book');
        Route::post('/delete-merchant-address', 'Modules\Merchant\MerchantController@deleteMerchantAddress')->name('admin.delete.merchant.address');
        //send email notification to the merchant by admin
        Route::get('/send-email-notification', 'Modules\Merchant\MerchantController@sendEmailNotification')->name('admin.send.email.notification');

        Route::post('/send-notification-merchant/{id}', 'Modules\Driver\DriverController@sendNotification')->name('admin.send.notification.merchant');

        //send email notification to the merchant by admin
        // Route::post('/send-email-notification', 'Modules\Merchant\MerchantController@sendEmailNotification')->name('admin.send.email.notification');

        //approve merchant
        Route::get('/approve-details/{id}', 'Modules\Merchant\MerchantController@approveMerchant')->name('admin.approve.merchant');

        Route::get('/approve-merchant-details', 'Modules\Merchant\MerchantController@approveMerchantByAdmin')->name('admin.approve.merchant.details');

        //premium merchant make yes/no
        
        Route::get('/premium-merchant/{id}', 'Modules\Merchant\MerchantController@premiumMerchant')->name('admin.premium.merchant');
        //premium merchant status change by ajax
        Route::get('/premium-merchant-status-change', 'Modules\Merchant\MerchantController@premiumMerchantStatus')->name('admin.premium.merchant.status.change');
        
        Route::get('/international-order/{id}', 'Modules\Merchant\MerchantController@internationOrderStatus')->name('admin.international.order');

        Route::get('/international-order-status-change', 'Modules\Merchant\MerchantController@internationOrderStatusChange')->name('admin.international.order.status.change');

        Route::get('/approve-details/{id}', 'Modules\Merchant\MerchantController@approveMerchant')->name('admin.approve.merchant');

        //delete merchant
        Route::get('/delete-merchant/{id}', 'Modules\Merchant\MerchantController@deleteMerchant')->name('admin.delete.merchant');
        
        // delete merchant by ajax
        Route::get('/delete-merchant-permanently', 'Modules\Merchant\MerchantController@deleteMerchantPermanently')->name('admin.delete.merchant.permanently');
        Route::get('/remove-merchant-image/{id}', 'Modules\Merchant\MerchantController@removeImg')->name('remove.image');

        // Processed order status
        Route::get('/change-processed-order-status', 'Modules\Merchant\MerchantController@changeProcessedOrderStatus')->name('admin.processed.order.status');

        // Edit merchant setting
        Route::post('edit-merchant-setting', 'Modules\Merchant\MerchantController@editMerchantSetting')->name('admin.edit.merchant.setting');
        Route::post('get-merchant-setting', 'Modules\Merchant\MerchantController@getMerchantSetting')->name('admin.get.merchant.setting');
        Route::get('merchant-get-city-{stateid?}-{merchantcityid?}','Modules\Merchant\MerchantController@getCity')->name('admin.merchant.get.city');
        Route::post('get-list','Modules\Merchant\MerchantController@getList')->name('admin.getList');
    });
    
    

    


    /*
    |---------------------------------------------------------------------------
    | Customers
    |---------------------------------------------------------------------------
    | This section is used for Customers only
    |
    */
    Route::group(['middleware' => 'App\Http\Middleware\SubAdmin:3'], function() {
        /*****************Customer Management******************************/
        Route::get('/add-customer', 'Modules\Customer\CustomerController@addCustomer')->name('admin.add.customer');

        //add merchant in database
        Route::post('/add-customer', 'Modules\Customer\CustomerController@addCustomer')->name('admin.add.customer');

        //edit merchant(redirect to view page)
        Route::get('/edit-customer/{id}', 'Modules\Customer\CustomerController@editCustomer')->name('admin.edit.customer');

        //edit merchant in database
        Route::post('/edit-customer/{id}', 'Modules\Customer\CustomerController@editCustomer')->name('admin.edit.customer');
        //manage merchant
        Route::get('/manage-customer', 'Modules\Customer\CustomerController@manageCustomer')->name('admin.list.customer');

        //manage merchant
        Route::post('/manage-customer', 'Modules\Customer\CustomerController@manageCustomer')->name('admin.list.customer');

        Route::get('/delete-customer/{id}', 'Modules\Customer\CustomerController@deleteCustomer')->name('admin.delete.customer');

        // delete customer details
        Route::get('/delete-customer-details', 'Modules\Customer\CustomerController@deleteCustomerDetails')->name('admin.delete.customer.details');
        
        //change status customer
        Route::get('/status-change-customer/{id}', 'Modules\Customer\CustomerController@changeStatusCustomer')->name('admin.change.status.customer');

        // block unblock customer
        Route::get('/block-unblock-customer', 'Modules\Customer\CustomerController@blockUnblockCustomer')->name('admin.block.unblock.customer');

        //check duplicate email at the time of creation of customer(ajax call)
        Route::get('/duplicate-email', 'Modules\Customer\CustomerController@checkDuplicateEmail')->name('check.customer.duplicate.email');
        

        //check duplicate email at the time of edit of customer(at the time of update ajax call)
        Route::get('/duplicate-new-email', 'Modules\Customer\CustomerController@checkDuplicateUpdateEmail')->name('customer.duplicate.email');

        Route::get('/duplicate-change-new-phone', 'Modules\Customer\CustomerController@checkDuplicateUpdatePhone')->name('customer.duplicate.update.phone');

        Route::post('/check-duplicate-phone-for-customer', 'Modules\Customer\CustomerController@ajaxDuplicatePhoneCustomerCheck')->name('check.customer.duplicate.phone.ajax');
        Route::post('/check-duplicate-email-for-customer', 'Modules\Customer\CustomerController@ajaxDuplicateEmailCustomerCheck')->name('check.customer.duplicate.email.ajax');

        //check duplicate phone at the time of creation of customer (ajax call)
        Route::get('/duplicate-phone', 'Modules\Customer\CustomerController@checkDuplicatePhone')->name('check.customer.duplicate.phone');

        Route::get('/send-message-customer', 'Modules\Customer\CustomerController@sendMessage')->name('admin.send.message.customer');

        Route::post('/send-notification-customer/{id}', 'Modules\Driver\DriverController@sendNotification')->name('admin.send.notification.customer');

        Route::get('/view-customer-profile/{id}', 'Modules\Customer\CustomerController@viewCustomerProfile')->name('admin.view.customer.profile');
        Route::get('/verify-customer', 'Modules\Customer\CustomerController@verifyCustomer')->name('admin.verify.customer');
        Route::post('/admin-update-tag', 'Modules\Customer\CustomerController@updateTag')->name('admin.update.tag');
        
    });
    

    
    /*
    |---------------------------------------------------------------------------
    | Drivers
    |---------------------------------------------------------------------------
    | This section is used for Drivers only
    |
    */
    Route::group(['middleware' => 'App\Http\Middleware\SubAdmin:5'], function() {
        /*****************Driver Management******************************/
        Route::get('/add-driver', 'Modules\Driver\DriverController@addDriver')->name('admin.add.driver');

        //add merchant in database
        Route::post('/add-driver', 'Modules\Driver\DriverController@addDriver')->name('admin.add.driver');

        //edit merchant(redirect to view page)
        Route::get('/edit-driver/{id}', 'Modules\Driver\DriverController@editDriver')->name('admin.edit.driver');

        //edit merchant in database
        Route::post('/edit-driver/{id}', 'Modules\Driver\DriverController@editDriver')->name('admin.edit.driver');
        //manage merchant
        Route::get('/manage-driver', 'Modules\Driver\DriverController@manageDriver')->name('admin.list.driver');

        //manage merchant
        Route::post('/manage-driver', 'Modules\Driver\DriverController@manageDriver')->name('admin.list.driver');
        
        //change status merchant
        Route::get('/status-change-driver/{id}', 'Modules\Driver\DriverController@changeStatusDriver')->name('admin.change.status.driver');

        Route::get('/block-unblock-driver', 'Modules\Driver\DriverController@blockUnblockDriver')->name('admin.block.unblock.driver');

        Route::get('/delete-driver/{id}', 'Modules\Driver\DriverController@deleteDriver')->name('admin.delete.driver');
        Route::get('/delete-driver-details', 'Modules\Driver\DriverController@deleteDriverDetails')->name('admin.delete.driver.details');

        Route::get('/duplicate-email-driver', 'Modules\Driver\DriverController@checkDuplicateEmail')->name('check.driver.duplicate.email');

        Route::get('/duplicate-phone-driver', 'Modules\Driver\DriverController@checkDuplicatePhone')->name('check.driver.duplicate.phone');

        Route::get('/duplicate-email-driver-edit', 'Modules\Driver\DriverController@checkUpdateDuplicateEmail')->name('driver.duplicate.email');

        Route::get('/duplicate-phone-driver-edit', 'Modules\Driver\DriverController@checkUpdateDuplicatePhone')->name('driver.duplicate.phone');

        Route::get('/send-message-driver', 'Modules\Driver\DriverController@sendMessage')->name('admin.send.message.driver');

        Route::post('/send-notification-driver/{id}', 'Modules\Driver\DriverController@sendNotification')->name('admin.send.notification.driver');

        Route::get('/view-driver-profile/{id}', 'Modules\Driver\DriverController@viewDriverProfile')->name('admin.view.driver.profile');

        // show driver ratings
        Route::get('/driver-ratings/{id}', 'Modules\Driver\DriverController@driverRatingList')->name('admin.driver.rating.list');
        Route::post('/driver-ratings', 'Modules\Driver\DriverController@driverRatingList')->name('admin.driver.rating.list');
    });
    
    /*
    |---------------------------------------------------------------------------
    | Shipping
    |---------------------------------------------------------------------------
    | This section is used for Shipping only
    |
    */
    Route::group(['middleware' => 'App\Http\Middleware\SubAdmin:9'], function() {
        /*****************Shipping Cost******************************/
        //update shipping settings
        Route::get('/shipping-setting', 'Modules\ShippingCost\ShippingCostController@updateShippingSettings')->name('admin.shipping.setting');

        //update shipping settings
        Route::post('/shipping-setting', 'Modules\ShippingCost\ShippingCostController@updateShippingSettings')->name('admin.shipping.setting');

        //add shipping cost
        Route::get('/add-shipping-cost', 'Modules\ShippingCost\ShippingCostController@addShippingCost')->name('admin.add.shipping.cost');
        //add shipping cost
        Route::post('/add-shipping-cost', 'Modules\ShippingCost\ShippingCostController@addShippingCost')->name('admin.add.shipping.cost');

        //edit shipping cost
        Route::get('/edit-shipping-cost/{id}', 'Modules\ShippingCost\ShippingCostController@editShippingCost')->name('admin.edit.shipping.cost');
        //edit shipping cost
        Route::post('/edit-shipping-cost/{id}', 'Modules\ShippingCost\ShippingCostController@editShippingCost')->name('admin.edit.shipping.cost');
        
        //list of shipping cost
        Route::get('/list-shipping-cost', 'Modules\ShippingCost\ShippingCostController@listShippingCost')->name('admin.list.shipping.cost');

        Route::post('/list-shipping-cost', 'Modules\ShippingCost\ShippingCostController@listShippingCost')->name('admin.list.shipping.cost');

        //delete shipping cost
        Route::get('/delete-shipping-cost/{id}', 'Modules\ShippingCost\ShippingCostController@deleteShippingCost')->name('admin.delete.shipping.cost');
        Route::get('/delete-shipping-cost-details', 'Modules\ShippingCost\ShippingCostController@deleteCost')->name('admin.delete.shipping.cost.details');
        //check duplicate range by ajax call
        Route::get('/check-duplicate-shipping-cost', 'Modules\ShippingCost\ShippingCostController@checkDuplicateRange')->name('check.duplicate.range.shipping.cost');

        // zones
        Route::get('/zone-list', 'Modules\Zone\ZoneController@index')->name('admin.zone.list');
        Route::post('/zone-list', 'Modules\Zone\ZoneController@index')->name('admin.zone.list');
        Route::get('/add-zone', 'Modules\Zone\ZoneController@addZone')->name('admin.zone.add');
        Route::post('/insert-zone', 'Modules\Zone\ZoneController@insertZone')->name('admin.zone.insert');
        Route::get('/edit-zone/{id}', 'Modules\Zone\ZoneController@editZone')->name('admin.zone.edit');
        Route::post('/update-zone', 'Modules\Zone\ZoneController@updateZone')->name('admin.zone.update');
        Route::post('/delete-zone', 'Modules\Zone\ZoneController@deleteZone')->name('admin.zone.delete');

        //ZoneRate
        Route::get('/zone-rate-list', 'Modules\Zone\ZoneController@zoneRateList')->name('admin.zone.rate.list');
        Route::post('/zone-rate-list', 'Modules\Zone\ZoneController@zoneRateList')->name('admin.zone.rate.list');
        Route::get('/add-zone-rate', 'Modules\Zone\ZoneController@addZoneRate')->name('admin.zone.rate.add');
        Route::post('/insert-zone-rate', 'Modules\Zone\ZoneController@insertZoneRate')->name('admin.zone.rate.insert');
        Route::get('/edit-zone-rate/{id}', 'Modules\Zone\ZoneController@editZoneRate')->name('admin.zone.rate.edit');
        Route::post('/update-zone-rate', 'Modules\Zone\ZoneController@updateZoneRate')->name('admin.zone.rate.update');
        Route::post('/delete-zone-rate', 'Modules\Zone\ZoneController@deleteZoneRate')->name('admin.zone.rate.delete');

        Route::post('/check-duplicate-range-zone-rate', 'Modules\Zone\ZoneController@checkDuplicateRange')->name('check.duplicate.range.zone.rate');
    });
    
    /*
    |---------------------------------------------------------------------------
    | Orders - Internal Order, External Order
    |---------------------------------------------------------------------------
    | This section is used for Order only
    |
    */
    Route::group(['middleware' => 'App\Http\Middleware\SubAdmin:6|13'], function() {
        //list of orders
        Route::post('/payment-confirmation', 'Modules\Orders\OrdersController@paymentConfirmation')->name('admin.payment.confirmation');

        Route::post('/ajax-order-status-change', 'Modules\Orders\OrdersController@ajaxChangeOrderStatus')->name('admin.ajax.order.status.change');
        
        Route::post('/cancel-order-ajax-order-status-change', 'Modules\Orders\OrdersController@ajaxCancelOrderChangeOrderStatus')->name('admin.cancel.order.ajax.order.status.change');
        
        Route::get('/cancelAProductOrder/{id}', 'Modules\Orders\OrdersController@cancelAProductOrder')->name('admin.cancelAProductOrder');
        
        Route::get('/changeProductOrderStatus/{id}', 'Modules\Orders\OrdersController@changeProductOrderStatus')->name('admin.changeProductOrderStatus');
        Route::get('/changeOrderStatusChangeUpdateEarningCalculation/{id}', 'Modules\Orders\OrdersController@changeOrderStatusChangeUpdateEarningCalculation')->name('admin.changeOrderStatusChangeUpdateEarningCalculation');

        Route::get('/delete-temp-orders', 'Modules\Orders\OrdersController@deleteOrder')->name('admin.delete.temp.order');
        Route::get('/manage-orders', 'Modules\Orders\OrdersController@index')->name('admin.list.order');
        Route::post('/manage-orders', 'Modules\Orders\OrdersController@index')->name('admin.list.order');
        
        Route::post('/lists-of-orders-datatable', 'Modules\Orders\OrdersController@orderlists')->name('admin.list.orders.datatable');
        
        //driver assign in order details page
        Route::post('/driver-assign', 'Modules\Orders\OrdersController@assignDriver')->name('admin.driver.assign');
        Route::post('/driver-reassign', 'Modules\Orders\OrdersController@reassignDriver')->name('admin.driver.reassign');

        // driver reassign 
        Route::get('/driver-assign-entire-order', 'Modules\Orders\OrdersController@assignDriverEntireOrder')->name('admin.driver.assign.entire.order');
        Route::get('/driver-reassign-entire-order', 'Modules\Orders\OrdersController@reassignDriverEntireOrder')->name('admin.driver.reassign.entire.order');

        Route::post('/manage-orders', 'Modules\Orders\OrdersController@index')->name('admin.list.order');
        Route::get('/view-payment-info-details', 'Modules\AdminPaymentDetails\AdminPaymentDetailsController@viewOrderWiseInfoDtls')->name('admin.view.info.details');
        Route::get('/view-product-wise-payment-info-details', 'Modules\AdminPaymentDetails\AdminPaymentDetailsController@viewInfoDetails')->name('admin.viewOrderWiseInfoDtls.view.info.details');
        Route::get('/view-order-wise-insurance-collection', 'Modules\AdminPaymentDetails\AdminPaymentDetailsController@viewOrderWiseInfoDetails')->name('admin.view.order.wise.info.details');
        
        Route::post('/view-payment-details', 'Modules\AdminPaymentDetails\AdminPaymentDetailsController@viewPaymentDetails')->name('admin.view.payment.details');
        Route::post('/view-order-wise-payment-details', 'Modules\AdminPaymentDetails\AdminPaymentDetailsController@viewOrderWisePaymentDetails')->name('admin.view.order.wise.payment.details');
        Route::post('/view-orderwise-payment-details', 'Modules\AdminPaymentDetails\AdminPaymentDetailsController@orderwiselists')->name('admin.orderwiselists');

        // wallet
        Route::get('/customer-wallet-history', 'Modules\CustomerWallet\CustomerWalletController@customerWalletHistory')->name('admin.customer.wallet.history');
        Route::post('/view-wallet-details', 'Modules\CustomerWallet\CustomerWalletController@viewWalletDetails')->name('admin.view.wallet.details');
        

        Route::get('/payment-tbl-show', 'Modules\PaymentTblShow\PaymentTblShowController@paymenttbleShow')->name('admin.show.payment.tble');
        Route::post('/view-payment-tbl-show', 'Modules\PaymentTblShow\PaymentTblShowController@viewpaymenttbleShow')->name('admin.view.show.payment.tble');

        Route::get('/customer-cancel-request-history', 'Modules\CancelRequest\CancelRequestController@customerCancelRequest')->name('admin.customer.cancel.request.history');
        Route::post('/view-cancel-request', 'Modules\CancelRequest\CancelRequestController@viewcustomerCancelRequest')->name('admin.view.cancel.request');
        
        Route::get('/customer-cancel-accept-request/{id}', 'Modules\CancelRequest\CancelRequestController@accpetCancelRequest')->name('admin.customer.cancel.accept');
        
        Route::get('/customer-cancel-reject-request/{id}', 'Modules\CancelRequest\CancelRequestController@rejectCancelRequest')->name('admin.customer.cancel.reject');
        
        
        Route::get('/customer-Specific-Wallet-History/{id}', 'Modules\CustomerWallet\CustomerWalletController@customerSpecificWalletHistory')->name('admin.customer.specific.wallet.history');
        Route::post('/customerSpecificviewWalletDetails/{id}', 'Modules\CustomerWallet\CustomerWalletController@customerSpecificviewWalletDetails')->name('admin.customer.specific.view.wallet.details');
        

        //search orders by ajax call
        Route::post('/search-orders', 'Modules\Orders\OrdersController@searchOrder')->name('admin.search.order');
        
        //view order details
        Route::get('/view-order-details/{id}', 'Modules\Orders\OrdersController@viewOrderDetails')->name('admin.view.order');
        
        Route::get('/print-invoice/{id}', 'Modules\Orders\OrdersController@printInvoice')->name('admin.print.invoice');

        //cancel order by admin (not in use)
        Route::get('/cancel-order/{id}', 'Modules\Orders\OrdersController@cancelOrder')->name('admin.cancel.order');

        //order cancel by ajax
        Route::get('/order-cancel', 'Modules\Orders\OrdersController@orderCancel')->name('admin.order.cancel');
        
        //status change for order(items) shipped or delivered or returned
        Route::get('/status-change-order/{id}', 'Modules\Orders\OrdersController@orderStatusChange')->name('admin.status.change.order');
        Route::post('/status-change-order-details/{id}', 'Modules\Orders\OrdersController@changeAnyStatusOrderDetail')->name('admin.status.change.order.details');

        Route::get('/approve-product', 'Modules\Orders\OrdersController@approveOrderedItem')->name('admin.approve.product.order');

        Route::get('/reject-product', 'Modules\Orders\OrdersController@RejectOrderedItem')->name('admin.reject.product.order');

        Route::get('/order-status-change/{id}', 'Modules\Orders\OrdersController@orderMasterStatusChange')->name('admin.status.order.master');

        Route::get('/update-ordr-mstr-status-change', 'Modules\Orders\OrdersController@updateStatusChange')->name('admin.update.status.order.master');
        Route::get('/update-payment-method', 'Modules\Orders\OrdersController@updatePaymentMethod')->name('admin.update.payment.method');
        Route::get('/add-notes', 'Modules\Orders\OrdersController@addNotes')->name('admin.update.notes');

        //get all city by autocomplete
        Route::post('/get-all-city-kuwait', 'Modules\Orders\OrdersController@fetchCity')->name('admin.fetch.city.all');
        Route::post('/get-all-pickup-city-kuwait', 'Modules\Orders\OrdersController@fetchCityPickup')->name('admin.external.fetch.pickup.city');
        Route::post('/check/city', 'Modules\Orders\OrdersController@checkCity')->name('admin.exist.city.check');

        // External order
        Route::get('/add-order', 'Modules\Orders\OrdersController@addOrder')->name('admin.add.order');
        Route::post('/add-order', 'Modules\Orders\OrdersController@addOrder')->name('admin.add.order');

        Route::get('/edit-external-order-details/{id}', 'Modules\Orders\OrdersController@editOrderDetails')->name('admin.edit.order.details');
        Route::post('/edit-external-order-details/{id}', 'Modules\Orders\OrdersController@editOrderDetails')->name('admin.edit.order.details');

        # complete order
        Route::get('complete-order/{id}', 'Modules\Orders\OrdersController@completeOrder');
        Route::post('delete-mark-orders', 'Modules\Orders\OrdersController@deleteMarkOrders')->name('admin.delete.mark.orders');
        Route::get('edit-internal-order/{id}', 'Modules\Orders\OrdersController@editInternalOrder')->name('admin.edit.internal.order');
        Route::post('update-internal-order/{id}', 'Modules\Orders\OrdersController@updateInternalOrder')->name('admin.update.internal.order');
        Route::post('update-internal-order-qty', 'Modules\Orders\OrdersController@updateInternalOrderQty')->name('admin.update.internal.order.qty');
        Route::get('remove-item-from-order/{orderId}/{orderDetailId}', 'Modules\Orders\OrdersController@removeItemFromOrder')->name('admin.remove.item.from.order');
        //get merchant address
        Route::post('/get-merchant-address', 'Modules\Orders\OrdersController@getMerchantAddress')->name('admin.get.merchant.address');

        // Bulk print orders
        Route::get('/bulk-print-orders/{ids}', 'Modules\Orders\BulkPrintOrder@printBulkOrder')->name('admin.bulk.print.orders');
    });
    
/*
    |---------------------------------------------------------------------------
    | Orders - Finance
    |---------------------------------------------------------------------------
    | This section is used for Finance only
    |
    */
    Route::group(['middleware' => 'App\Http\Middleware\SubAdmin:7'], function() {
        /************************************  Finance ***************************************/

        //list of earnings by admin
        Route::get('/finance', 'Modules\Finance\FinanceController@index')->name('admin.finance');
        Route::post('/finance', 'Modules\Finance\FinanceController@index')->name('admin.finance');

        //list of request withdrawl by admin
        Route::get('/list-request-withdrawl', 'Modules\Finance\FinanceController@listRequestWithdrawl')->name('admin.list.request.withdrawl');
        Route::post('/viewlistRequestWithdrawl', 'Modules\Finance\FinanceController@viewlistRequestWithdrawl')->name('admin.viewlistRequestWithdrawl');

        //search list of request withdrawl by admin
        Route::post('/search-list-request-withdrawl', 'Modules\Finance\FinanceController@searchRequestedWihdrawl')->name('admin.search.request.withdrawl');

        // Route::get('/get-merchant-balence', 'Modules\Finance\FinanceController@getMerchantBalence')->name('admin.get.merchant.balence');
        Route::post('/add-request-withdrawl-for-merchant-by-admin', 'Modules\Finance\FinanceController@addWithdrawlRequestByMerchant')->name('admin.add.request.withdrawl.by.admin');
        
        //search list of earnings by admin
        Route::post('/search-earnings', 'Modules\Finance\FinanceController@searchEarnings')->name('admin.search.earnings');

        Route::post('/approve-request/{id}', 'Modules\Finance\FinanceController@approveRequest')->name('admin.approve.request');

        Route::post('/reject-request/{id}', 'Modules\Finance\FinanceController@rejectRequest')->name('admin.reject.request');

        Route::get('/order-summary/{id}', 'Modules\Finance\FinanceController@orderSummary')->name('admin.order.summary');

        // Invoice section
        Route::get('/add-invoice', 'Modules\Invoice\InvoiceController@addInvoice')->name('admin.add.invoice');
        Route::get('/manage-invoice', 'Modules\Invoice\InvoiceController@manageInvoice')->name('admin.manage.invoice');
        Route::get('/edit-invoice/{id}', 'Modules\Invoice\InvoiceController@editInvoice')->name('admin.edit.invoice');
        Route::get('/delete-invoice', 'Modules\Invoice\InvoiceController@deleteInvoice')->name('admin.delete.invoice');
        Route::get('/view-invoice/{id}', 'Modules\Invoice\InvoiceController@viewInvoice')->name('admin.view.invoice');
        Route::get('/print-invoice-details/{id}', 'Modules\Invoice\InvoiceController@printInvoiceDetails')->name('admin.print.invoice.details');
        Route::get('/bulk-print-invoice/{id}', 'Modules\Invoice\InvoiceController@printBulkInvoice')->name('admin.print.bulk.invoice');

        Route::post('/manage-invoice', 'Modules\Invoice\InvoiceController@manageInvoice')->name('admin.manage.invoice');
        Route::post('/get-all-merchant', 'Modules\Invoice\InvoiceController@getAllMerchant')->name('admin.get.all.merchant');
        Route::post('/insert-invoice', 'Modules\Invoice\InvoiceController@insertInvoice')->name('admin.insert.invoice');
        Route::post('/update-invoice', 'Modules\Invoice\InvoiceController@updateInvoice')->name('admin.update.invoice');
        Route::post('/check-invoice', 'Modules\Invoice\InvoiceController@checkInvoice')->name('admin.check.invoice');
        Route::post('/invoice-export', 'Modules\Invoice\InvoiceController@invoiceExport')->name('admin.invoice.export');
    });
    


    

/*
    |---------------------------------------------------------------------------
    | Report
    |---------------------------------------------------------------------------
    | This section is used for Report only
    |
    */
    Route::group(['middleware' => 'App\Http\Middleware\SubAdmin:10'], function() {
        /*************** Reports ************************/

        Route::get('/earning-report', 'Modules\Report\ReportController@showEarningReport')->name('admin.earning.report');
        Route::post('/lists-of-earning-orders-datatable', 'Modules\Report\ReportController@earningOrderlists')->name('admin.earning.orders.datatable');
        Route::get('/order-earning-export', 'Modules\Report\ReportController@showEarningReportExport')->name('earning.report.export');

        Route::get('/order-report', 'Modules\Report\ReportController@showOrderReport')->name('admin.order.report');
        Route::get('/order-report-export', 'Modules\Report\ReportController@showOrderReportExport')->name('order.report.export');

        Route::any('/user-report', 'Modules\Report\ReportController@showUserReport')->name('admin.user.report');
        Route::get('/user-report-export', 'Modules\Report\ReportController@showUserReportExport')->name('user.report.export');

        Route::any('/merchant-report', 'Modules\Report\ReportController@showMerchantReport')->name('admin.merchant.report');
        Route::get('/merchant-report-export', 'Modules\Report\ReportController@showMerchantReportExport')->name('merchant.report.export');

        Route::any('/product-report', 'Modules\Report\ReportController@showProductReport')->name('admin.product.report');
        Route::get('/product-report-export', 'Modules\Report\ReportController@showProductReportExport')->name('product.report.export');
        
        Route::get('/driver-report', 'Modules\Report\ReportController@showDriverReport')->name('admin.driver.report');
        Route::post('/lists-of-driver-orders-datatable', 'Modules\Report\ReportController@driverOrderlists')->name('admin.driver.report.datatable');
        Route::get('/order-driver-export', 'Modules\Report\ReportController@showDriverReportExport')->name('driver.report.export');

        Route::get('/product-analysis-report', 'Modules\Report\ReportController@showProductAnalysisReport')->name('admin.product.analysis.report');
        Route::post('/product-analysis-report', 'Modules\Report\ReportController@showProductAnalysisReport')->name('admin.product.analysis.report');
        Route::get('/product-analysis-report-export', 'Modules\Report\ReportController@showProductAnalysisReportExport')->name('product.analysis.report.export');
        Route::get('/export-quick-merchant-report/{token}', 'Modules\Report\ReportController@exportQuickMerchantReport')->name('export.quick.merchant.report');
        Route::get('/export-quick-driver-report/{token}', 'Modules\Report\ReportController@exportQuickDriverReport')->name('export.quick.driver.report');
    });
    
    /****************ADMIN MY PROFILE******************************/
    Route::get('/my-profile-details', 'Modules\MyProfile\MyProfileController@index')->name('admin.view.profile');
    Route::post('/my-profile-details', 'Modules\MyProfile\MyProfileController@index')->name('admin.view.profile');
    Route::get('/chk-duplicate-email-update', 'Modules\MyProfile\MyProfileController@checkAdminDuplicateEmail')->name('admin.check.duplicate.email.update');
    Route::get('/chk-duplicate-phone-update', 'Modules\MyProfile\MyProfileController@checkAdminDuplicatePhone')->name('admin.check.duplicate.phone.update');
    

    Route::get('/manage-content', 'Modules\Content\ContentController@show')->name('admin.manage.content');
    Route::get('/edit-content', 'Modules\Content\ContentController@edit')->name('admin.edit.manage.content');
    Route::post('/update-content', 'Modules\Content\ContentController@update')->name('admin.update.manage.content');

    /*
    |---------------------------------------------------------------------------
    | Content Management
    |---------------------------------------------------------------------------
    | This section is used for Content only
    |
    */
    Route::group(['middleware' => 'App\Http\Middleware\SubAdmin:11'], function() {
        //Faq admin start

        //add Faq
        Route::get('/add-faq', 'Modules\Faq\FaqController@addFaq')->name('admin.add.faq');
        //post Faq
        Route::post('/post-faq', 'Modules\Faq\FaqController@postFaq')->name('admin.post.faq');

        //add Faq
        Route::get('/edit-faq/{id}', 'Modules\Faq\FaqController@edeitFaq')->name('admin.edit.faq');
        //post Faq
        Route::post('/post-editfaq{id}', 'Modules\Faq\FaqController@postEditFaq')->name('admin.edit.faqpost');

        //list faq
        Route::get('/list-faq', 'Modules\Faq\FaqController@index')->name('admin.list.faq');
        //search faq
        Route::post('/list-faq', 'Modules\Faq\FaqController@index')->name('admin.list.faq');

        Route::get('/delete-faq/{id}', 'Modules\Faq\FaqController@deleteFaq')->name('admin.delete.faq');


        Route::get('/faq-category/{id?}', 'Modules\Faq\FaqController@addFaqCategory')->name('admin.faq.add.category');
        Route::post('/faq/post-category', 'Modules\Faq\FaqController@postFaqCategory')->name('admin.faq.post.category');
        Route::get('/faq/delete-category/{id}', 'Modules\Faq\FaqController@deleteFaqCategory')->name('admin.faq.delete.category');

        //faq admin end

        // page content start 3.4.20 

        Route::get('/about-page-content', 'Modules\Content\ContentController@pageAbout')->name('admin.manage.pageabout');
        Route::get('/show-help', 'Modules\Content\ContentController@getHelpPage')->name('admin.get.help.page');
        Route::post('/post-show-help', 'Modules\Content\ContentController@getHelpPage')->name('admin.get.help.page.post');
        Route::post('/about-page-content-post', 'Modules\Content\ContentController@pageAboutContentUpdate')->name('admin.manage.pageaboutadd');
        Route::get('/contact-page-content', 'Modules\Content\ContentController@pageContact')->name('admin.manage.pagecontact');
        Route::post('/contact-page-content-post', 'Modules\Content\ContentController@pageContactContentUpdate')->name('admin.manage.pagecontactadd');
        Route::get('/terms-conditions-page-content/{id}', 'Modules\Content\ContentController@pageTermsConditions')->name('admin.manage.page.termsconditions');
        Route::post('/terms-page-content-post/{id}', 'Modules\Content\ContentController@pageTermsConditionsPost')->name('admin.manage.page.termsconditions.post');
        Route::get('/help-page-content-post-show/{id}', 'Modules\Content\ContentController@pageHelpPost')->name('admin.manage.page.help.post.view');
        Route::post('/help-page-content-post/{id}', 'Modules\Content\ContentController@pageHelpPost')->name('admin.manage.page.help.post');
        Route::get('/privecy-page-content/{id}', 'Modules\Content\ContentController@pagePrivecy')->name('admin.manage.page.privecy');
        Route::post('/privecy-page-content-post/{id}', 'Modules\Content\ContentController@pagePrivecyPost')->name('admin.manage.page.privecy.post');   

        // For tiny-mcs image upload...

        Route::post('admin-article-image-upload', 'Modules\Content\ContentController@imgUpload')->name('admin.artical.img.upload');

        //banner section
        Route::get('/banner-listing', 'Modules\Content\ContentController@bannerListing')->name('admin.manage.bannerlist');
        Route::get('/banner-details/{id}', 'Modules\Content\ContentController@bannerDetails')->name('admin.manage.bannerDetails');
        Route::post('/banner-details-post/{id}', 'Modules\Content\ContentController@bannerDetailsPost')->name('admin.manage.banner.details.post');

        Route::get('/add-banner', 'Modules\Content\ContentController@addBanner')->name('admin.add.banner');
        Route::post('/insert-banner', 'Modules\Content\ContentController@insertBanner')->name('admin.insert.banner');
        Route::get('/delete-banner/{id}', 'Modules\Content\ContentController@deleteBanner')->name('admin.delete.banner');
        Route::any('/app-settings', 'Modules\Content\ContentController@appSettings')->name('admin.app.settings');
        // page content end 3.4.20  
    });

  
});
