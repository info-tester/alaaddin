<?php

namespace App\Providers;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use App\Models\ContentDetails;
use App\Models\ContactUsDetails;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         $data = [];
        view()->composer('includes.*', function ($view) use($data) {
            $data['annoucement'] = ContentDetails::where(['content_id'=>4,'language_id'=>getLanguage()->id])->first();
            $data['contact_information'] = ContactUsDetails::first();
            $view->with($data);
        });
        // URL::forceScheme('https');
    }
}
