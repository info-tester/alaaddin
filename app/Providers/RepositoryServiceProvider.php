<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            \App\Repositories\CategoryRepository::class, 
            \App\Repositories\CategoryRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\SettingRepository::class, 
            \App\Repositories\SettingRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\LanguageRepository::class, 
            \App\Repositories\LanguageRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\BrandRepository::class, 
            \App\Repositories\BrandRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\ProductRepository::class, 
            \App\Repositories\ProductRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\MerchantRepository::class, 
            \App\Repositories\MerchantRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\CountryRepository::class, 
            \App\Repositories\CountryRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\MerchantOppeningRepository::class, 
            \App\Repositories\MerchantOppeningRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\MerchantCompanyDescriptionRepository::class, 
            \App\Repositories\MerchantCompanyDescriptionRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\MerchantImageRepository::class, 
            \App\Repositories\MerchantImageRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\AdminRepository::class, 
            \App\Repositories\AdminRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\UserRepository::class, 
            \App\Repositories\UserRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\DriverRepository::class, 
            \App\Repositories\DriverRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\ShippingCostRepository::class, 
            \App\Repositories\ShippingCostRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\UserAddressBookRepository::class, 
            \App\Repositories\UserAddressBookRepositoryEloquent::class
        );
        $this->app->bind(\App\Repositories\ProductOtherOptionRepository::class, \App\Repositories\ProductOtherOptionRepositoryEloquent::class);
        
        $this->app->bind(\App\Repositories\UserFavoriteRepository::class, \App\Repositories\UserFavoriteRepositoryEloquent::class);

        $this->app->bind(\App\Repositories\OrderDetailRepository::class, \App\Repositories\OrderDetailRepositoryEloquent::class);

        $this->app->bind(\App\Repositories\OrderMasterRepository::class, \App\Repositories\OrderMasterRepositoryEloquent::class);

        $this->app->bind(\App\Repositories\CartMasterRepository::class, \App\Repositories\CartMasterRepositoryEloquent::class);

        $this->app->bind(\App\Repositories\CartDetailRepository::class, \App\Repositories\CartDetailRepositoryEloquent::class);


        $this->app->bind(\App\Repositories\OrderSellerRepository::class, \App\Repositories\OrderSellerRepositoryEloquent::class);

        $this->app->bind(\App\Repositories\CartDetailRepositoryRepository::class, \App\Repositories\CartDetailRepositoryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProductVariantRepository::class, \App\Repositories\ProductVariantRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProductCategoryRepository::class, \App\Repositories\ProductCategoryRepositoryEloquent::class);

        $this->app->bind(\App\Repositories\ProductVariantDetailRepository::class, \App\Repositories\ProductVariantDetailRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\WithdrawRepository::class, \App\Repositories\WithdrawRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OrderImageRepository::class, \App\Repositories\OrderImageRepositoryEloquent::class);
        
        $this->app->bind(\App\Repositories\CityRepository::class, \App\Repositories\CityRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\StateRepository::class, \App\Repositories\StateRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CitiDetailsRepository::class, \App\Repositories\CitiDetailsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ConversationsRepository::class, \App\Repositories\ConversationsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Models\ConversationsRepository::class, \App\Repositories\Models\ConversationsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CountryDetailsRepository::class, \App\Repositories\CountryDetailsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ConversationsRepository::class, \App\Repositories\ConversationsRepositoryEloquent::class);
        //:end-bindings:
    }
}
