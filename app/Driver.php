<?php

namespace App;

use App\Notifications\Driver\Auth\ResetPassword;
use App\Notifications\Driver\Auth\VerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Driver extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];
    protected $table = 'drivers';
    protected $guarded = [];

    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }
    
    /**
    * Method: getRating
    * Description: This method is used to get driver rating.
    * Author: Argha
    * Date: 2020-09-09
    */
    public function getRating() {
        return $this->hasMany('App\Models\DriverReview', 'driver_id', 'id');
    }

    /**
    * Method: getOrderDetails
    * Description: This method is used to get order Detail of driver
    * Author: Argha
    * Date: 2021-03-27
    */

    public function getOrderDetails()
    {
        return $this->hasMany('App\Models\OrderDetail', 'driver_id', 'id');
    }
}
