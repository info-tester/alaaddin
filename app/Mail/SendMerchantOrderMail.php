<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMerchantOrderMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['data'] =  $this->request;
        // dd(@$data['data']['order']->status);
        if(@$data['data']['order']->status == 'I'){
            $ostatus = "Incomplete";
        }
        else if(@$data['data']['order']->status == 'INP'){
            $ostatus = "In Progress";
        }
        else if(@$data['data']['order']->status == 'CM'){
            $ostatus = "Completed";
        }
        else if(@$data['data']['order']->status == 'N'){
            $ostatus = "New Order";
        }
        else if(@$data['data']['order']->status == 'OA'){
            $ostatus = "Order accepted";
        }
        else if(@$data['data']['order']->status == 'RP'){
            $ostatus = "Ready for pick up";
        }
        else if(@$data['data']['order']->status == 'OP'){
            $ostatus = "Order picked up";
        }
        else if(@$data['data']['order']->status == 'OD'){
            $ostatus = "Order delivered";
        }
        else if(@$data['data']['order']->status == 'OC'){
            $ostatus = "Order cancelled";
        }
        else if(@$data['data']['order']->status == 'PP'){
            $ostatus = "Partial pickup";
        }
        else if(@$data['data']['order']->status == 'PC'){
            $ostatus = " Partial Pickup complete";
        }
        else if(@$data['data']['order']->status == 'F'){
            $ostatus = "payment failed";
        }else{
            $ostatus = @$data['data']['order']->status;
        }
        $subject  = " Order Status is changed! ".@$data['data']['order']->order_no." details!";
        return $this->view('mail.merchant_order_details_mail', $data)
        ->to(@$data['data']['order']['orderMerchants'][0]['orderSeller']->email)
        ->subject($subject)
         ->from(env('MAIL_USERNAME'), 'Alaaddin');
    }
}
