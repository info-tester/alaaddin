<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrderDetailsMerchantMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user_data = [];
    public function __construct($data = [])
    {
        $this->user_data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->user_data;    
        $bccd = [];
        for($i=1;$i<count($this->user_data['bcc']);$i++){
            $bccd[] = $this->user_data['bcc'][$i];
        }
        return $this->view('mail.merchant_order_mail',['data' => $data])
        ->to($this->user_data['bcc'][0])
        // ->to($bccd)
        ->bcc($bccd)
        ->subject($this->user_data['subject'])
        ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
    }
}
