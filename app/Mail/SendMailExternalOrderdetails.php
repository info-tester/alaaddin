<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailExternalOrderdetails extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public $user_data = [];
    public function __construct($request,$data = [])
    {
        $this->request = $request;
        $this->user_data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        #set data
        $data = $this->user_data;    
        $bccd = [];
        #sent to admin notification mail
        for($i=1;$i<count($this->request['bcc']);$i++){
            $bccd[] = $this->request['bcc'][$i];
        }
        $data['orderMasterDetails'] = $this->request;
        // $admin_email = "infoware.solutions5@gmail.com";
        return $this->view('mail.send_mail_external_order_details', $this->request)
            ->to($this->request['bcc'][0])
            ->bcc($bccd)
            ->subject($this->request['orderMasterDetails']->subject)
            
            // ->attach('storage/app/public/upload/Modules/Cv/'.$this->request->file1)
            // ->from('support@aswagna.com', env('APP_NAME'));
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
    }
}
