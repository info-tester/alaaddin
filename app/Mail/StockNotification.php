<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StockNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $mailData = [];
    public function __construct($data = [])
    {
        $this->mailData = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $data = $this->mailData;
        $bcc = [];
        foreach (array_unique($this->mailData['emails']) as $key => $value) {
            if($key != 0) {
                $bcc[] = $value;
            }
        }
        return $this->view('mail.stock_notification',['data' => $data])
        ->to($this->mailData['emails'][0])
        ->bcc($bcc)
        ->subject('Item in stock!')
        ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
    }
}
