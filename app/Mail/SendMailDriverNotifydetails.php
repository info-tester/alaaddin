<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailDriverNotifydetails extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.for external order notificatiion that order is accepted
     *
     * @return void
     */
    public $request;
    public $user_data = [];
    public function __construct($request,$data = [])
    {
        $this->request = $request;
        $this->user_data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->user_data;    
        $bccd = [];
        for($i=0;$i<count($this->request['bcc']);$i++){
            $bccd[] = $this->request['bcc'][$i];
        }
        $data['orderMasterDetails'] = $this->request;
         //dd($this->request['orderMasterDetails']->email,$this->request);
        return $this->view('mail.send_mail_driver_assigned_details', $this->request)
        ->to($this->request['orderMasterDetails']->email)
        ->bcc($bccd)
        ->subject($this->request['orderMasterDetails']->subject)
        ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
    }
}
