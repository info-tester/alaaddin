<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyNewEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['user'] = $this->request;
        $admin_email = "infoware.solutions5@gmail.com";
        return $this->view('mail.verify_new_email', $data)
            ->to($this->request->email)
            // ->bcc()
            ->subject('Verify your email-id!Request for Email-id Change!')
            // ->attach('storage/app/public/upload/Modules/Cv/'.$this->request->file1)
            // ->from('support@aswagna.com', env('APP_NAME'));
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
    }
}
