<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyDriverNewEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['driver'] = $this->request;
        $admin_email = "infoware.solutions5@gmail.com";
        return $this->view('mail.verify_driver_new_email', $data)
            ->to($this->request->email)
            // ->bcc()
            ->subject('Verify your Email-id!Request for Email-id Change!')
            // ->attach('storage/app/public/upload/Modules/Cv/'.$this->request->file1)
            // ->from('support@aswagna.com', env('APP_NAME'));
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
    }
}
