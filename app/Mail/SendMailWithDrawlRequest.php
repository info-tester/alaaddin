<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailWithDrawlRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public function __construct($data = [])
    {
        $this->user_data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->user_data['bcc']);
        $data['merchant'] = $this->user_data;
        $bccd = [];
        for($i=0;$i<count($this->user_data['bcc']);$i++){
            $bccd[] = $this->user_data['bcc'][$i];
        }
        // dd($bccd);
        $admin_email = "infoware.solutions5@gmail.com";
        return $this->view('mail.withdrawl_request_received_mail',['data'=>$this->user_data])
            ->to($this->user_data['merchant_email'])
            // ->bcc($this->user_data['bcc'][0])
            ->bcc($this->user_data['bcc'])
            ->subject('New withdrawal request received!')
            // ->attach('storage/app/public/upload/Modules/Cv/'.$this->request->file1)
            // ->from('support@aswagna.com', env('APP_NAME'));
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
    }
}
