<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Validator;
use db;

class VerificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user_data = [];
    public function __construct($data = [])
    {
        $this->user_data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    
    public function build(){
        $user = $this->user_data;
        if($user['mailBody'] == 'updated') {
            $email = $user['email'];
            $subject = "Update Email Address";  
        } 
        elseif($user['mailBody'] == 'forget_password') {
            $email = $user->email;      
            $subject = "Password reset varify email";      
        }
        else {
            $email = $user->email;      
            $subject = "Registration Success Mail";      
        }
        return $this->view('mail.verification_mail',['user' => $user])
            ->to($email)
            ->subject($subject)
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
    }
}
