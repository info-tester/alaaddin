<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminVerifyMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public $request;
    public function __construct($request)
    {
        $this->request = $request;
    }

    public function build()
    {
        $data['data'] =  $this->request;

        
        if ($this->request['mailBody']=='forgotpassword') {
            $subject = "Forgot Password";
        }else{
            $subject = "Forgot Password";
        }
        return $this->view('mail.admin_verify_mail', $data)
                    ->to($this->request['email'])
                    ->subject($subject)
                     ->from(env('MAIL_USERNAME'), 'Alaaddin');
    }
}
