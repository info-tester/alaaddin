<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';
    protected $guarded = [];
    /*
    * Method 		: menuDetails
    * Description 	: Relationship between menu and permission to get name of menu.
    * Author 		: Jayatri
    */
    public function menuDetails(){
    	return $this->hasMany('App\Models\Menu','menu_id','menu_id')->where('language_id',getLanguage()->id);
    }
}
