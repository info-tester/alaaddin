<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ZoneDetail extends Model
{
    protected $table = 'zone_details';
    protected $guarded = [];

    /*
    * Method        : countryDetails
    * Description   : This method is used get country details
    * Author        : Argha
    * Date          : 2020-Sep-10
    */

    public function countryDetails()
    {
        return $this->hasMany('App\Models\Country','id','country_id');
    }
}
