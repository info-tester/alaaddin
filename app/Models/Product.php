<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Models\CartMaster;

class Product extends Model
{
    protected $guarded = [];

    /**
    * Method: productByLanguage
    * Description: This method is used to get product by language.
    * Author: Sanjoy 
    */
    public function productByLanguage() {
        return $this->hasOne('App\Models\ProductDetail', 'product_id', 'id')->where('language_id', getLanguage()->id)->select('id','product_id','language_id','title','description');
    }

    public function productUnitMasters() {
        return $this->hasOne('App\Models\UnitMaster', 'id', 'unit_master_id'); 
    }
    
    /**
    * Method: productVariants
    * Description: This method is used to get product by variant.
    * Author: Sanjoy 
    */
    public function productVariants() {
        return $this->hasMany('App\Models\ProductVariant', 'product_id', 'id'); 
    }
    

    /**
    * Method: productOtherVariants
    * Description: This method is used to get product by other variant which show in search.
    * Author: Sanjoy 
    */
    public function productOtherVariants() {
        return $this->hasMany('App\Models\ProductOtherOption', 'product_id', 'id'); 
    }

    /**
    * Method: productOtherVariantsGroup
    * Description: This method is used to get product by other variant which show in search.
    * Author: Sanjoy 
    */
    public function productOtherVariantsGroup() {
        return $this->hasMany('App\Models\ProductOtherOption', 'product_id', 'id'); 
    }

    public function productOtherVariantsGroup1() {
        return $this->hasMany('App\Models\ProductOtherOption', 'variant_id', 'variant_id'); 
    }

    /**
    * Method: productCategory
    * Description: This method is used to get product category.
    * Author: Sanjoy 
    */
    public function productCategory() {
        return $this->hasMany('App\Models\ProductCategory');
    }

    /**
    * Method: defaultImage
    * Description: This method is used to get product default image.
    * Author: Sanjoy 
    */
    public function defaultImage() {
        return $this->hasOne('App\Models\ProductImage')->where('is_default', 'Y')->select('id','image','product_id','is_default');  
    }

    /**
    * Method: images
    * Description: This method is used to get product all images.
    * Author: Sanjoy 
    */
    public function images() {
        return $this->hasMany('App\Models\ProductImage');  
    }

    /**
    * Method: productMarchant
    * Description: This method is used to get merchant associated to the product.
    * Author: Sanjoy 
    */
    public function productMarchant() {
        return $this->hasOne('App\Merchant', 'id', 'user_id');
    }

    /**
    * Method: productBarnd
    * Description: This method is used to get brand associated to the product.
    * Author: Sanjoy 
    */
    public function productBarnd() {
        return $this->hasOne('App\Models\Brand', 'id', 'brand_id'); 
    }

    /**
    * Method: productSubCategory
    * Description: This method is used to get sub category associated to the product.
    * Author: Sanjoy 
    */
    public function productSubCategory() {
        return $this->hasOne('App\Models\ProductCategory', 'product_id', 'id')->where('level', 'S');
    }

    /**
    * Method: productParentCategory
    * Description: This method is used to get parent category associated to the product.
    * Author: Sanjoy 
    */
    public function productParentCategory() {
        return $this->hasOne('App\Models\ProductCategory', 'product_id', 'id')->where('level', 'P');
    }
    public function getProductCategory() {
        return $this->hasOne('App\Models\ProductCategory', 'product_id', 'id')->select('id','product_id','category_id','level');
    }

    public function wishlist() {
        
        if(@getUserByToken()->id){
            return $this->hasOne('App\Models\UserFavorite', 'product_id', 'id')->where('user_id', @getUserByToken()->id);
        }else{
            return $this->hasOne('App\Models\UserFavorite', 'product_id', 'id')->where('user_id', Auth::id());
        }
        //return $this->hasOne('App\Models\UserFavorite', 'product_id', 'id')->where('user_id', Auth::id());
    }

    public function wishlist1() {
        return $this->hasOne('App\Models\UserFavorite', 'product_id', 'id')->where('user_id', @getUserByToken()->id);
    }
    public function wishlistfavourite() {
        return $this->hasMany('App\Models\UserFavorite', 'product_id', 'id');
    }
    public function show_cart_detail() {
        
        $CartMaster = CartMaster::where(['user_id'=>@getUserByToken()->id])->first();
        if(@$CartMaster){
            return $this->hasOne('App\Models\CartDetail', 'product_id', 'id')->where(['cart_master_id'=>$CartMaster->id])->with(['cartMasterTable']);
        }else{
            return $this->hasOne('App\Models\CartDetail', 'product_id', 'id')->where(['cart_master_id'=>0])->with(['cartMasterTable']);    
        }
    }
    
}
