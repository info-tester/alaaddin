<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletDetails extends Model
{
    //

    protected $table = 'user_wallet_details';
    protected $guarded = [];


    public function get_order_master()
    {
        return $this->hasOne('App\Models\OrderMaster', 'id', 'order_id');
    }
    public function view_user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function product_by_language() {
        return $this->hasOne('App\Models\ProductDetail', 'product_id', 'product_id')->where('language_id', getLanguage()->id);
    }
}
