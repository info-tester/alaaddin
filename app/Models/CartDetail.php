<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartDetail extends Model
{
    protected $guarded = [];

    /**
    * Method: productByLanguage
    * Description: This method is used to get product by language.
    * Author: Sanjoy 
    */
    public function productByLanguage() {
    	return $this->hasOne('App\Models\ProductDetail', 'product_id', 'product_id')->where('language_id', getLanguage()->id);
    }

    /**
    * Method: defaultImage
    * Description: This method is used to get product default image.
    * Author: Sanjoy 
    */
    public function defaultImage() {
    	return $this->hasOne('App\Models\ProductImage', 'product_id', 'product_id')->where('is_default', 'Y');	
    }

    /**
    * Method: productMarchant
    * Description: This method is used to get merchant associated to the product.
    * Author: Sanjoy 
    */
    public function productMarchant() {
        return $this->hasOne('App\Merchant', 'id', 'seller_id');
    }

    /**
    * Method: productMarchant
    * Description: This method is used to get product variant detail associated to the product variant.
    * Author: Surajit 
    */
    public function productVariantDetails() {
        return $this->hasOne('App\Models\ProductVariant', 'id', 'product_variant_id');
    }
    /**
    * Method: getProduct
    * Description: This method is used to get product.
    * Author: Surajit 
    */
    public function getProduct() {
        return $this->hasOne('App\Models\Product', 'id', 'product_id')->selectRaw('id,unit_master_id, price, discount_price,product_price,round(((price - product_price) / price) * 100) as percentage')
        ->addSelect(\DB::raw('product_price AS discount_price'));
    }
    /**
    * Method: cartMasterTable
    * Description: This method is used to cartMasterTable from details table
    * Author: Jayatri 
    */
    public function cartMasterTable(){
        return $this->hasOne('App\Models\CartMaster', 'id', 'cart_master_id');
    }

     /**
    * Method: sellerInfo
    * Description: This method is used to get seller info from seller table using for api
    * Author: Sanjoy
    */
    public function sellerInfo() {
        return $this->hasOne('App\Merchant', 'id', 'seller_id');
    }
}
