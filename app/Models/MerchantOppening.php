<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantOppening extends Model
{
    protected $table = 'merchant_to_oppenings';
    protected $guarded = [];
}
