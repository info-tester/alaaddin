<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantAddressBook extends Model
{
    protected $table = 'merchant_address_book';
    protected $guarded = [];

    /*
    * Method        : countryDetailsBylanguage
    * Description   : This method is used to relationship with country details
    * Author        : Argha
    * Date 			: 2020-09-17
    */

    public function countryDetailsBylanguage()
    {
        return $this->hasOne('App\Models\CountryDetails', 'country_id', 'country')->where('language_id',getLanguage()->id);
    }

    /*
    * Method        : getCityNameByLanguage
    * Description   : This method is used to relationship with city details
    * Author        : Argha
    * Date 			: 2020-09-17
    */
    public function getCityNameByLanguage() {
        return $this->hasOne('App\Models\CitiDetails', 'city_id', 'city_id')->where('language_id',getLanguage()->id);
    }
}
