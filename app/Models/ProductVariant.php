<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    protected $guarded = [];

    public function productVariantDetails() {
    	return $this->hasMany('App\Models\ProductVariantDetail', 'product_variant_id', 'id');	
    }

    public function productVariantByLanguage() {
    	return $this->hasOne('App\Models\Variant', 'id', 'variant_id')->where('language_id', getLanguage()->id);	
    }


    public function productVariantDetailsGroup() {
    	return $this->hasMany('App\Models\ProductVariantDetail', 'group_id', 'group_id')->groupBy('variant_id');	
    }

   
}
