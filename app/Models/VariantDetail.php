<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VariantDetail extends Model
{
    protected $table = 'variant_details';
    protected $guarded = [];
}
