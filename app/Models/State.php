<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';
    protected $guarded = [];
    public function cityDetails() {
    	return $this->hasMany('App\Models\City', 'state_id', 'id');
    }
}
