<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductNotification extends Model
{
    protected $table = 'product_notifications';

    protected $guarded = [];
}
