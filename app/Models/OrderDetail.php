<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_details';
    protected $guarded = [];
    use \Awobaz\Compoships\Compoships;
    /*
    * Method: productDetails
    * Description: This method is used to get product Details.
    * Author: Jayatri
    */
    public function productDetails() {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
    public function productCategoriesDetails() {
        return $this->hasOne('App\Models\ProductCategory', 'product_id', 'product_id');
    }
    public function getOrderDetailsUnitMaster() {
        return $this->hasOne('App\Models\UnitMaster', 'id', 'product_unit_master_id');
    }
    public function variantDecode(){
        return $this->id;
    }

    /*
    * Method: productVariantDetails
    * Description: This method is used to product Variant Details.
    * Author: Jayatri
    */
    public function productVariantDetails() {
        return $this->hasOne('App\Models\ProductVariant', 'id', 'product_variant_id');
    }

    /*
    * Method: sellerDetails
    * Description: This method is used to seller Details.
    * Author: Jayatri
    */
    public function sellerDetails() {
        return $this->hasOne('App\Merchant', 'id', 'seller_id');
    }

    /*
    * Method: driverDetails
    * Description: This method is used to driver Details.
    * Author: Jayatri
    */
    public function driverDetails() {
        return $this->hasOne('App\Driver', 'id', 'driver_id')->where('status', 'A');
    }
    
    /**
    * Method: productByLanguage
    * Description: This method is used to get product by language.
    * Author: Surajit 
    */
    public function productByLanguage() {
        return $this->hasOne('App\Models\ProductDetail', 'product_id', 'product_id')->where('language_id', getLanguage()->id);
    }

    /**
    * Method: defaultImage
    * Description: This method is used to get product default image.
    * Author: Surajit 
    */
    public function defaultImage() {
        return $this->hasOne('App\Models\ProductImage', 'product_id', 'product_id')->where('is_default', 'Y');  
    }

    /**
    * Method: orderMaster
    * Description: This method is used to get detail order master table.
    * Author: Surajit 
    */
    public function orderMaster() {
        return $this->hasOne('App\Models\OrderMaster', 'id', 'order_master_id');  
    }
    // public function merchantDetails() {
    //     return $this->hasOne('App\Merchant', 'id', 'order_master_id');  
    // }
    public function getOrderSellerDetail(){
        return $this->hasMany('App\Models\OrderSeller', 'seller_id', 'seller_id');
    }
    
    public function getProductOrderByMerchant(){
        return $this->hasMany('App\Merchant', 'id', 'seller_id');
    }

    public function getOrderSeller(){
        return $this->hasOne('App\Models\OrderSeller', 'seller_id', 'seller_id');
    }

    public function orderSellerDet()
    {
        return $this->hasMany('App\Models\OrderSeller', 'order_master_id', 'order_master_id');
    }
    public function getorderSellerDet()
    {
        return $this->hasOne('App\Models\OrderSeller', 'order_master_id', 'order_master_id')->where('seller_id', @getUserByToken()->id);
    }
     public function getorderSellerDetailInfo()
    {
        return $this->hasOne('App\Models\OrderSeller', ['order_master_id','seller_id'], ['order_master_id','seller_id']);
    }
    public function showOrderVehicleInformation() {
        return $this->hasOne('App\Models\OrderVehicleInformation', 'order_details_id', 'id');
    }
    public function showCancelRequest() {
        return $this->hasOne('App\Models\CancelRequest', 'order_details_id', 'id');
    }
}
