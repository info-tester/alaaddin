<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderVehicleInformation extends Model
{
    //
    protected $table = 'order_vehicle_information';
    protected $guarded = [];
}
