<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $table = 'withdraw';
    protected $guarded = [];

    /*
    * Method: merchantDetails
    * Description: This method is used to get all details about merchant.
    * Author : Jayatri
    */

    public function merchantDetails() {
    	return $this->hasOne('App\Merchant', 'id', 'seller_id');
    }
}
