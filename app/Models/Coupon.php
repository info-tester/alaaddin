<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'coupons';
    protected $guarded = [];

    /*
    * Method        : getMerchants
    * Description   : This method is used get merchant
    * Author        : Argha
    * Date 			: 2020-09-04
    */

    public function getMerchants()
    {
        return $this->hasMany('App\Models\CouponToMerchant','coupon_id','id');
    }
}
