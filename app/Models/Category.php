<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    /**
    * Method: subCategories
    * Description: This method is used to get all sub categories associated to this category
    */
    public function subCategories() {
    	return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    /**
    * Method: categoryByLanguage
    * Description: This method is used to get category details by language
    */
    public function categoryByLanguage() {
    	return $this->hasOne('App\Models\CategoryDetail', 'category_id', 'id')->where('language_id', getLanguage()->id);
    }

    public function productBySubCategory() {
        return $this->hasMany('App\Models\ProductCategory')->where('level', 'S');
    }

    public function productByCategory() {
        return $this->hasMany('App\Models\ProductCategory')->where('level', 'P');
    }

    /*
    * Method : categorySubcategoryDetails
    * Description : Relationship between category & categoryDetails
    * Author : Jayatri
    * Date : 16/01/2020
    */
    public function categorySubcategoryDetails(){
        return $this->hasMany('App\Models\CategoryDetail', 'category_id', 'id')->where('language_id', getLanguage()->id);
    }
    /*
    * Method : categorySubcategoryDetails
    * Description : Relationship between category & categoryDetails
    * Author : Jayatri
    * Date : 16/01/2020
    */
    public function categoryNameByLang(){
        return $this->hasOne('App\Models\CategoryDetail', 'category_id', 'id')->where('language_id', getLanguage()->id);
    }
    /*
    * Method : categoryDetails
    * Description : Relationship between category & categoryDetails
    * Author : Suarjit
    */
    public function categoryDetails(){
        return $this->hasOne('App\Models\CategoryDetail', 'category_id', 'id')->where('language_id', getLanguage()->id);
    }


    /*
    * Method : parentCategoryDetails
    * Description : Details of parent category
    * Author : Jayatri
    * Date : 16/01/2020
    */
    public function parentCategoryDetails(){
        return $this->hasMany('App\Models\CategoryDetail', 'category_id', 'parent_id')->where('language_id', getLanguage()->id);
    }

    /*
    * Method : parentCategoryNameByLang
    * Description : Details of parent category
    * Author : Jayatri
    * Date : 16/01/2020
    */
    public function parentCategoryNameByLang(){
        return $this->hasOne('App\Models\CategoryDetail', 'category_id', 'parent_id')->where('language_id', getLanguage()->id);
    }
    /*
    * Method : parentCatDetails
    * Description : Details of parent category
    * Author : Surajit
    */
    public function parentCatDetails(){
        return $this->hasOne('App\Models\CategoryDetail', 'category_id', 'parent_id')->where('language_id', getLanguage()->id);
    }
    
     /*
    * Method : categoryDetailsByLanguage
    * Description : Details of category by language
    * Author : Jayatri
    * Date : 19/01/2020
    */
    public function categoryDetailsByLanguage(){
        return $this->hasMany('App\Models\CategoryDetail', 'category_id', 'id')->where('language_id', getLanguage()->id);
    }
    /*
    * Method : categorySubcategoryDetails
    * Description : Relationship between category & categoryDetails
    * Author : Jayatri
    * Date : 16/01/2020
    */
    public function details(){
        return $this->hasMany('App\Models\CategoryDetail','category_id','id');
    }
    public function variant(){
        return $this->hasMany('App\Models\Variant','sub_category_id','id');
    }
    public function brand(){
        return $this->hasMany('App\Models\Brand','category_id','id');
    }
    public function categories(){
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }
    public function priceVariant(){
        return $this->hasMany('App\Models\Variant','sub_category_id','id')->where('type', 'P');
    }

    /*
    * Method : categorySubcategoryDetails
    * Description : Relationship between category & ProductCategory to count products
    * Author : Surajit
    */
    public function productByCategoryCount() {
        return $this->hasMany('App\Models\ProductCategory','category_id','id');
    }
}
