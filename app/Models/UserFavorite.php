<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFavorite extends Model
{
    protected $guarded = [];

    /**
    * Method: getProduct
    * Description: This method is used to get product.
    * Author: Surajit 
    */
    public function getProduct() {
    	return $this->hasOne('App\Models\Product', 'id', 'product_id')->where(['status' => 'A','seller_status' => 'A'])->selectRaw('id, price, discount_price,product_price,round(((price - product_price) / price) * 100) as percentage,unit_master_id,user_id,slug,product_code,total_review,total_no_reviews,avg_review,weight,from_date,to_date,is_featured,is_new,in_offers,merchant_is_featured,times_of_sold,status,seller_status,product_hide,tot_wishlist_done')
        ->addSelect(\DB::raw('product_price AS discount_price'));
    }
    /**
    * Method: getProduct
    * Description: This method is used to get product.
    * Author: Jayatri
    */
    public function gettProduct() {
    	return $this->hasOne('App\Models\Product', 'id', 'product_id')->where(['status' => 'A','seller_status' => 'A'])->selectRaw('id, price, discount_price,product_price,round(((price - product_price) / price) * 100) as percentage,unit_master_id,user_id,slug,product_code,total_review,total_no_reviews,avg_review,weight,from_date,to_date,is_featured,is_new,in_offers,merchant_is_featured,times_of_sold,status,seller_status,product_hide,tot_wishlist_done')
        ->addSelect(\DB::raw('product_price AS discount_price'));
    }
}
