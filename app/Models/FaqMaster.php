<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqMaster extends Model
{
    protected $table = 'faq_master';
    protected $guarded = [];

    /*
    * Method: brandDetails
    * Description: This method is used to get all brands with details based on language.
    * Author : Jayatri
    */

    public function faqDetailsByLanguage() {
    	return $this->hasOne('App\Models\FaqDetail', 'faq_master_id', 'id')->where('language_id', getLanguage()->id);
    }

   
    public function categoryByFaq() {
        return $this->hasOne('App\Models\FaqCategoryDetail', 'faq_category_master_id', 'faq_category_id')->where('language_id', getLanguage()->id);
    }



    public function faqDetails() {
        return $this->hasMany('App\Models\FaqDetail', 'faq_master_id', 'id');
    }
    public function faqDtlsByLanguage() {
        return $this->hasMany('App\Models\FaqDetail', 'faq_master_id', 'id')->where('language_id', getLanguage()->id);
    }

   
    // public function categoryByFaq() {
    //     return $this->hasOne('App\Models\FaqCategoryDetail', 'faq_category_master_id', 'faq_category_id')->where('language_id', getLanguage()->id);
    // }



    // public function faqDetails() {
    //     return $this->hasMany('App\Models\FaqDetail', 'faq_master_id', 'id');
    // }
    public function FaqCategoryMaster() {
        return $this->hasOne('App\Models\FaqCategoryMaster', 'id', 'faq_category_id');
    }
   
    
}
