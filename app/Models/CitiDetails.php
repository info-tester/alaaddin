<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CitiDetails extends Model
{
    //
    protected $table = 'city_details';
    protected $guarded = [];
    
}
