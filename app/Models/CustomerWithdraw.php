<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerWithdraw extends Model
{
    //
    protected $table = 'customer_withdraw';
    protected $guarded = [];
}
