<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUsDetails extends Model
{
    
    protected $table = 'contact_us_details';
    protected $guarded = [];
}
