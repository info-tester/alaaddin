<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderImage extends Model
{
    protected $table = 'order_images';
    protected $guarded = [];
}
