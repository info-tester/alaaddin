<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ZoneRateMaster extends Model
{
    protected $table = 'zone_rate_master';
    protected $guarded = [];

    /*
    * Method        : getRateDetails
    * Description   : This method is used get zone details
    * Author        : Argha
    * Date          : 2020-Sep-11
    */
    public function getRateDetails()
    {
        return $this->hasOne('App\Models\ZoneRateDetail','zone_rate_master_id','id');
    }

    /*
    * Method        : getZone1
    * Description   : This method is used get zones
    * Author        : Argha
    * Date          : 2020-Sep-11
    */
    public function getZone1()
    {
        return $this->hasOne('App\Models\ZoneMaster','id','zone_id_1');
    }

    /*
    * Method        : getZone2
    * Description   : This method is used get zones
    * Author        : Argha
    * Date          : 2020-Sep-11
    */
    public function getZone2()
    {
        return $this->hasOne('App\Models\ZoneMaster','id','zone_id_2');
    }
}
