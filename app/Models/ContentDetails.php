<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentDetails extends Model
{
    protected $table = 'content_details';
    protected $guarded = [];
}
