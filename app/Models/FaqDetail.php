<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqDetail extends Model
{
    protected $table = 'faq_detail';
    protected $guarded = [];

    /*
    * Method: brandDetails
    * Description: This method is used to get all brands with details based on language.
    * Author : Jayatri
    */

    // public function brandDetailsByLanguage() {
    // 	return $this->hasOne('App\Models\BrandDetail', 'barnd_id', 'id')->where('language_id', getLanguage()->id);
    // }
    /*
    * Method: categoryDetailsByBrand
    * Description: This method is used to get all category details associated with brand.
    * Author : Jayatri
    */

    // public function categoryDetailsByBrand() {
    // 	return $this->hasMany('App\Models\CategoryDetail', 'category_id', 'category_id')->where('language_id', getLanguage()->id);
    // }

    /*
    * Method: categoryDetailsByBrand
    * Description: This method is used to get category details associated with brand.
    * Author : Sanjoy
    */
    // public function categoryByBrand() {
    //     return $this->hasOne('App\Models\CategoryDetail', 'category_id', 'category_id')->where('language_id', getLanguage()->id);
    // }

     /*
    * Method: brandDetails
    * Description: This method is used to get all brands with details.
    * Author : Jayatri
    */

    // public function brandDetails() {
    //     return $this->hasMany('App\Models\BrandDetail', 'barnd_id', 'id');
    // }

     /*
    * Method: productsByBrand
    * Description: This method is used to count product associated to this product.
    * Author : Sanjoy
    */

    // public function productsByBrand() {
    // 	return $this->hasMany('App\Models\Product', 'brand_id', 'id')->where(['status' => 'A']);
    // }
    
}
