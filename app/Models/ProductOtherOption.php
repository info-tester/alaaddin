<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOtherOption extends Model
{
    public function productVariantDetails() {
    	return $this->hasMany('App\Models\ProductOtherOption', 'variant_id', 'variant_id');	
    }

    public function productVariantByLanguage() {
    	return $this->hasOne('App\Models\Variant', 'id', 'variant_id');	
    }

    /**
    * Method: variantValueByLanguage
    * Description: This method is used to get the name of all values of the variant
    * Author: Sanjoy
    */
    public function variantValueByLanguage() {
    	return $this->hasOne('App\Models\VariantValueDetail', 'variant_value_id', 'variant_value_id')->where('language_id', getLanguage()->id);	
    }

    /**
    * Method: getProduct
    * Description: This method is used to getProduct assoiated to this variant deleted or not
    * Author: Sanjoy
    */
    public function getProducts() {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
}
