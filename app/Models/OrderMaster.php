<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderMaster extends Model
{
   	protected $table = 'order_master';
    protected $guarded = [];
    
    /*
    * Method: orderMasterDetails
    * Description: This method is used to relationship between order master and order details
    * Author: Jayatri
    */
    //relationship between order master and order details
    public function orderMasterDetails() {
    	return $this->hasMany('App\Models\OrderDetail', 'order_master_id', 'id')->select('id','order_master_id','product_id','product_unit_master_id','product_variant_id','variants','seller_id','driver_id','quantity','weight','shipping_price','original_price',\DB::raw("ROUND(total/quantity,2) As discounted_price"),'sub_total','total','seller_commission','loading_price','insurance_price','payable_amount','admin_commission','status','review_date','cancel_note','cancelled_on','rate','comment','expected_delivery_date','expected_delivery_time','notes','driver_notes','product_note','customer_name','customer_photo','delivery_date','delivery_time','created_at','updated_at');
        
    }
    public function order_detail() {
        return $this->hasMany('App\Models\OrderDetail', 'order_master_id', 'id');
    }

    public function orderMasterExtDetails() {
        return $this->hasOne('App\Models\OrderDetail', 'order_master_id', 'id');
    }
    
    /*
    * Method: customerDetails
    * Description: This method is used to view Order Details.
    * Author: Jayatri
    */
    public function customerDetails() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    /*
    * Method: countryDetails
    * Description: This method is used to view Order Details.
    * Author: Jayatri
    */
    public function countryDetails() {
        return $this->hasOne('App\Models\Country', 'id', 'shipping_country');
    }
    /*
    * Method: shippingAddress
    * Description: This method is used to view shipping Address.
    * Author: Surajit
    */
    public function shippingAddress() {
        return $this->hasOne('App\Models\UserAddressBook', 'id', 'shipping_address_id');
    }
    /*
    * Method: billingAddress
    * Description: This method is used to view billing Address.
    * Author: Surajit
    */
    public function billingAddress() {
        return $this->hasOne('App\Models\UserAddressBook', 'id', 'billing_address_id');
    }
    
    /*
    * Method: getCountry
    * Description: This method is used to get details of Country.
    * Author : Surajit
    */

    public function getCountry() {
        return $this->hasOne('App\Models\CountryDetails', 'country_id', 'shipping_country')->where('language_id',getLanguage()->id);
    }

    public function getCityNameByLanguage() {
        return $this->hasOne('App\Models\CitiDetails', 'city_id', 'shipping_city_id')->where('language_id',getLanguage()->id);
    }
    public function getBillingCityNameByLanguage() {
        return $this->hasOne('App\Models\CitiDetails', 'city_id', 'billing_city_id')->where('language_id',getLanguage()->id);
    }
    public function getSearchCityNames() {
        return $this->hasOne('App\Models\CitiDetails', 'city_id', 'shipping_city_id')->where('language_id',getLanguage()->id);
    }
    /*
    * Method: getBillingCountry
    * Description: This method is used to get details of Country.
    * Author : Surajit
    */

    public function getBillingCountry() {
        return $this->hasOne('App\Models\CountryDetails', 'country_id', 'billing_country');
    }
    /*
    * Method: productVariantDetails
    * fDescription: This method is used to product Variant Details.
    * Author : Surajit
    */
    public function productVariantDetails(){
        return $this->hasOne('App\Models\ProductVariant', 'id', 'product_variant_id');
    }
    public function getOrderAllImages(){
        return $this->hasMany('App\Models\OrderImage', 'order_id', 'id');   
    }
    // public function getBillingCountryDetails(){
    //     return $this->hasOne('App\Models\Country', 'id', 'billing_country');   
    // }

    public function getUserDetails() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function orderSellerInfo(){
        return $this->hasOne('App\Models\OrderSeller', 'order_master_id', 'id')->where('seller_id', @getUserByToken()->id);
    }

    public function orderMerchants(){
        return $this->hasMany('App\Models\OrderSeller', 'order_master_id', 'id');
    }

    /*
    * Method: getBillingCountry
    * Description: This method is used to get details of Country for whatsapp message. 
    * Author : Sanjoy
    */
    public function getBillingCountryCode() {
        return $this->hasOne('App\Models\CountryDetails', 'country_id', 'billing_country')->where('language_id',getLanguage()->id);
    }

    /*
    * Method: paymentData
    * Description: This method is used to get details of Country for whatsapp message. 
    * Author : Sanjoy
    */
    public function paymentData() {
        return $this->hasOne('App\Models\Payment', 'order_id', 'id')->where(['type'=>'order_payment']);
    }

    /*
    * Method: getpickupCountryDetails
    * Description: This method is used to get details of Country for pickup in external order.
    * Author : Argha
    * Date: 2020-09-21
    */
    public function getpickupCountryDetails() {
        return $this->hasOne('App\Models\CountryDetails', 'country_id', 'pickup_country')->where('language_id',getLanguage()->id);
    }

    /*
    * Method: getpickupCityDetails
    * Description: This method is used to get details of Country for pickup in external order.
    * Author : Argha
    * Date: 2020-09-21
    */
    public function getpickupCityDetails() {
        return $this->hasOne('App\Models\CitiDetails', 'city_id', 'pickup_city_id')->where('language_id',getLanguage()->id);
    }

    /*
    * Method: getPreferredTime
    * Description: This method is used to get preferred time.
    * Author : Argha
    * Date: 2020-09-29
    */
    public function getPreferredTime() {
        return $this->hasOne('App\Models\TimeSlot', 'id', 'preferred_delivery_time');
    }

    /*
    * Method: getAdminForExternalOrder
    * Description: This method is used to get admin information
    * Author : Argha
    * Date: 2020-10-12
    */
    public function getAdminForExternalOrder()
    {
        return $this->hasOne('App\Admin', 'id', 'admin_id');
    }

    public function editedBy() {
        return $this->hasOne('App\Admin', 'id', 'edited_by');    
    }

    public function statusTiming() {
        return $this->hasMany('App\Models\OrderStatusTiming', 'order_id', 'id');    
    }

    /*
    * Method: getCoupon
    * Description: This method is used to get coupon details
    * Author : Argha
    * Date: 2020-11-18
    */

    public function getCoupon()
    {
        return $this->hasOne('App\Models\Coupon', 'id', 'coupon_id'); 
    }
    public function shipping_city_details() {
        return $this->hasOne('App\Models\City', 'id', 'shipping_city');
    }
    public function billing_city_details() {
        return $this->hasOne('App\Models\City', 'id', 'billing_city');
    }
    public function shipping_state_details() {
        return $this->hasOne('App\Models\State', 'id', 'shipping_state');
    }
    public function billing_state_details() {
        return $this->hasOne('App\Models\State', 'id', 'billing_state');
    }
    public function showOrderVehicleInformation() {
        return $this->hasMany('App\Models\OrderVehicleInformation', 'order_master_id', 'id');
    }
    public function refunddetails() {
        return $this->hasOne('App\Models\Payment', 'order_id', 'id')->where(['type'=>'cancel_refund']);
    }
}
