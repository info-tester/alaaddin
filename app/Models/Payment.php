<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $guarded = [];
    protected $table = 'payments';

    public function orderMasterTab() {
        return $this->hasOne('App\Models\OrderMaster', 'id', 'order_id');  
    }
}
