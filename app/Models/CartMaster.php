<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartMaster extends Model
{
	protected $table = 'cart_masters';
	
    protected $guarded = [];

    /**
    * Method: cartDetails
    * Description: This method is used to get cart details
    */
    public function cartDetails() {
    	return $this->hasMany('App\Models\CartDetail');
    }
    public function productCartMerchantDetails(){
        return $this->hasMany('App\Models\CartDetail','cart_master_id','id');
    }
}
