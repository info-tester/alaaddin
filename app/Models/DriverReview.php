<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriverReview extends Model
{
    protected $table = 'driver_reviews';
    protected $guarded = [];

     /**
    * Method: getMerchant
    * Description: This method is used to get merchant information.
    * Author: Argha
    * Date: 2020-09-09
    */
    public function getMerchant() {
        return $this->hasOne('App\Merchant', 'id', 'merchant_id');
    }

     /**
    * Method: getCustomer
    * Description: This method is used to get customer information.
    * Author: Argha
    * Date: 2020-09-09
    */
    public function getCustomer() {
        return $this->hasOne('App\User', 'id', 'customer_id');
    }

     /**
    * Method: getOrder
    * Description: This method is used to get order information
    * Author: Argha
    * Date: 2020-09-09
    */
    public function getOrder() {
        return $this->hasOne('App\Models\OrderMaster', 'id', 'order_master_id');
    }
}
