<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversations extends Model
{
    protected $table = 'conversations';
    protected $guarded = [];

    public function sendBySubAdminDetails(){
    	return $this->hasOne('App\Admin', 'id', 'send_by');
    }
    public function sendByCustomerDetails(){
    	return $this->hasOne('App\User', 'id', 'send_by');
    }
    public function sendByDriverDetails(){
    	return $this->hasOne('App\Driver', 'id', 'send_by');
    }
    public function sendByMerchantDetails(){
    	return $this->hasOne('App\Merchant', 'id', 'send_by');
    }
}
