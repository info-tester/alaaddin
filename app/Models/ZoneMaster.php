<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ZoneMaster extends Model
{
    protected $table = 'zone_master';
    protected $guarded = [];

    /*
    * Method        : zone_details
    * Description   : This method is used get zone details
    * Author        : Argha
    * Date          : 2020-Sep-10
    */
    public function zone_details()
    {
        return $this->hasMany('App\Models\ZoneDetail','zone_master_id','id');
    }
}
