<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceMaster extends Model
{
    protected $table = 'invoice_master';
    protected $guarded = [];

    /*
    * Method: getInvoiceDetails
    * Description: This method is used to get details of invoice
    * Author : Argha
    * Date   : 2020-10-20
    */
    public function getInvoiceDetails()
    {
       return $this->hasMany('App\Models\InvoiceDetail','invoice_master_id','id');
    }

    /*
    * Method: getMerchant
    * Description: This method is used to get merchant details
    * Author : Argha
    * Date   : 2020-10-21
    */

    public function getMerchant()
    {
       return $this->hasOne('App\Merchant','id','merchant_id');
    }
}
