<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantCompanyDescription extends Model
{
    protected $table = 'merchant_to_company_descriptions';
    protected $guarded = [];
    
}
