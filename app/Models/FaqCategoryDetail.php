<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqCategoryDetail extends Model
{
    protected $table = 'faq_category_detail';
    protected $guarded = [];

}
