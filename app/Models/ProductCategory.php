<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    public function activeProducts() {
    	return $this->hasOne('App\Models\Product', 'id', 'product_id')->where('status', 'A');
    }
    public function Category() {
    	return $this->hasOne('App\Models\Category', 'id', 'category_id')->with('categoryByLanguage:id,category_id,language_id,title,description')->select('id','slug','parent_id','picture','status','add_in_menu','is_popular');
    }
    public function notDeletedProducts() {
    	return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    /**
    * Method: categoryByLanguage
    * Description: This method is used to get category details by language
    */
    public function categoryByLanguage() {
    	return $this->hasOne('App\Models\CategoryDetail', 'category_id', 'category_id')->where('language_id', getLanguage()->id)->select('id','category_id','language_id','title','description');
    }
}
