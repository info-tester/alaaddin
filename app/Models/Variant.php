<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
	protected $table = 'variants';
    protected $guarded = [];

    public function variantValues() {
    	return $this->hasMany('App\Models\VariantValue')->where(['status' => 'A']);
    }

    public function variantByLanguage() {
    	return $this->hasOne('App\Models\VariantDetail','variant_id','id')->where('language_id', getLanguage()->id);	
    }
    /*
    * Method      : productSubactegoryDetail
    * Description : Relationship between variant & category based on language. 
    * Author      : Jayatri
    * Date 		  : 21/01/2020
    */
    public function productSubcategoryDetail(){
    	// return $this->hasOne('App\Models\Category');	
    	return $this->hasOne('App\Models\Category','id','sub_category_id');	
    }
    /*
    * Method      : productCategoryDetail
    * Description : Relationship between variant & category based on language. 
    * Author      : Jayatri
    * Date 		  : 21/01/2020
    */
    public function productCategoryDetail(){
    	return $this->hasOne('App\Models\Category','id','category_id');	
    }

    public function variantDetails(){
        return $this->hasMany('App\Models\VariantDetail','variant_id','id');   
    }
    
    public function variantDetailsByLanguage(){
        return $this->hasMany('App\Models\VariantDetail','variant_id','id')->where('language_id',getLanguage()->id);   
    }
}
