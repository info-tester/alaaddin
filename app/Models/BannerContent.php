<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerContent extends Model
{
    protected $table = 'page_banner_content';
    protected $guarded = [];

    /*
    * Method: contentDetailsByLanguage
    * Description: This method is used to get all content with details based on language.
    * Author : Surajit
    */

    // public function contentDetailsByLanguage() {
    // 	return $this->hasOne('App\Models\ContentDetails', 'content_id', 'id')->where('language_id', getLanguage()->id);
    // }
}
