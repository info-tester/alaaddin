<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserReward extends Model
{
    protected $table = 'user_rewards';
    protected $guarded = [];

    public function orderMaster() {
    	return $this->hasOne('App\Models\OrderMaster', 'id', 'order_id');
    }
    public function user() {
    	return $this->hasOne('App\User', 'id', 'user_id');
    }
}
