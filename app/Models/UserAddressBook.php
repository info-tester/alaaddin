<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddressBook extends Model
{
    protected $table = 'user_address_books';
    protected $guarded = [];

    /*
    * Method: Country
    * Description: This method is used to get details of Country.
    * Author : Surajit
    */

    public function getCountry() {
        return $this->hasOne('App\Models\CountryDetails', 'country_id', 'country');
    }
    public function getCityByLanguage(){
        return $this->hasOne('App\Models\CitiDetails', 'city_id', 'city_id');    
    }
    public function city_details() {
        return $this->hasOne('App\Models\City', 'id', 'city');
    }
    public function state_details() {
        return $this->hasOne('App\Models\State', 'id', 'state');
    }
}
