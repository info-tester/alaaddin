<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqCategoryMaster extends Model
{
    protected $table = 'faq_category_master';
    protected $guarded = [];

    public function categoryDetailsByLanguage(){
        return $this->hasMany('App\Models\FaqCategoryDetail', 'faq_category_master_id', 'id')->where('language_id', getLanguage()->id);
    }
    public function categoryDetailsLanguage(){
        return $this->hasOne('App\Models\FaqCategoryDetail', 'faq_category_master_id', 'id')->where('language_id', getLanguage()->id);
    }
    public function faqMasterT() {
        return $this->hasMany('App\Models\FaqMaster', 'faq_category_id', 'id')->orderBy('display_order');
    }

    public function categoryDetails(){
        return $this->hasMany('App\Models\FaqCategoryDetail', 'faq_category_master_id', 'id');
    }
}
