<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationData extends Model
{
    protected $table = 'notification_data';
    protected $guarded = [];
}
