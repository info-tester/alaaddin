<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandDetail extends Model
{
    protected $table = 'brand_details';
    protected $guarded = [];

    public function brands() {
    	return $this->hasOne('App\Models\Brand', 'id', 'barnd_id')->where('language_id', getLanguage()->id);
    }
}
