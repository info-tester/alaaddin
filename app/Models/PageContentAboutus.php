<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageContentAboutus extends Model
{
    protected $table = 'page_content_aboutus';
    protected $guarded = [];

    /*
    * Method: contentDetailsByLanguage
    * Description: This method is used to get all content with details based on language.
    * Author : Surajit
    */

    // public function contentDetailsByLanguage() {
    // 	return $this->hasOne('App\Models\ContentDetails', 'content_id', 'id')->where('language_id', getLanguage()->id);
    // }
}
