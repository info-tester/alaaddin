<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin_activities extends Model
{
    protected $table = 'admin_activities';
    protected $guarded = [];
}
