<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariantOthers extends Model
{
    protected $table   = 'product_other_options';
    protected $guarded = [];
   
}
