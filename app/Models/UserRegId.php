<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRegId extends Model
{
    protected $table = 'user_regid';

    protected $guarded = [];
}
