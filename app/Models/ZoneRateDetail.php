<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ZoneRateDetail extends Model
{
    protected $table = 'zone_rate_detail';
    protected $guarded = [];

    /*
    * Method        : getZone
    * Description   : This method is used get zone 
    * Author        : Argha
    * Date          : 2020-Sep-11
    */
    // public function getZone()
    // {
    //     return $this->hasOne('App\Models\ZoneRateMaster','id','zone_rate_master_id');
    // }

    /*
    * Method        : getZone1
    * Description   : This method is used get zones
    * Author        : Argha
    * Date          : 2020-Sep-21
    */
    public function getZone()
    {
        return $this->hasOne('App\Models\ZoneMaster','id','zone_id');
    }
}
