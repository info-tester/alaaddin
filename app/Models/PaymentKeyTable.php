<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentKeyTable extends Model
{
    //
    protected $table = 'payment_table_key';
    protected $guarded = [];
}
