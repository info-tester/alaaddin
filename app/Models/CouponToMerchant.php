<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CouponToMerchant extends Model
{
    protected $table = 'coupon_to_merchant';
    protected $guarded = [];

    /*
    * Method        : getMerchant
    * Description   : This method is to relation with merchant
    * Author        : Argha
    * Date 			: 2020-09-04
    */
    public function merchantInfo()
    {
       return $this->hasOne('App\Merchant','id','merchant_id');
    }
}
