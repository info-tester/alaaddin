<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderSeller extends Model
{
	protected $table='order_sellers';
	protected $guarded=[];
	use \Awobaz\Compoships\Compoships;

	public function orderDetail() {
		return $this->hasMany('App\Models\OrderDetail', ['order_master_id', 'seller_id'], ['order_master_id', 'seller_id']);     
	}

	public function orderDetailSeller() {
		return $this->hasMany('App\Models\OrderDetail', 'seller_id', 'seller_id'); 
	}
	public function allOrderSeller() {
		return $this->orderDetail->merge($this->orderDetailSeller);
	}

	public function orderSeller(){
		return $this->hasOne('App\Merchant', 'id', 'seller_id');    
	}
	public function orderDetails(){
		return $this->hasMany('App\Models\OrderDetail',['order_master_id', 'seller_id'], ['order_master_id', 'seller_id']);   
	}
	public function orderMasterTab() {
        return $this->hasOne('App\Models\OrderMaster', 'id', 'order_master_id');  
    }
	public function orderMerchant(){
		return $this->hasOne('App\Merchant','id','seller_id');   
	}
	// public function orderDetails(){
	// 	return $this->hasMany('App\Models\OrderDetail','order_master_id','order_master_id');   
	// }
	public function orderDetailsCount(){
		return $this->hasMany('App\Models\OrderDetail','order_master_id','order_master_id')->count();   
	}
	public function orderImages(){
		return $this->hasMany('App\Models\OrderImage','order_id','order_master_id');   
	}
	
}
