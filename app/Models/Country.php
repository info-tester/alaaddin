<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $guarded = [];
    /*
    * Method: stateDetails
    * Description: This method is used to state details.
    * Author : Jayatri
    */

    public function stateDetails() {
    	return $this->hasMany('App\Models\State', 'country_id', 'id');
    }
    public function countryDetails() {
        return $this->hasMany('App\Models\CountryDetails', 'country_id', 'id');
    }
    public function countryDetailsBylanguage() {
        return $this->hasOne('App\Models\CountryDetails', 'country_id', 'id')->where('language_id',getLanguage()->id);
    }
    
}
