<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductVariantDetail extends Model
{
    protected $guarded = [];

    public function variantValueByLanguage() {
    	return $this->hasOne('App\Models\VariantValueDetail', 'variant_value_id', 'variant_value_id')->where('language_id', getLanguage()->id);	
    }

    
    public function variantValueName() {
    	return $this->hasOne('App\Models\VariantValue', 'id', 'variant_value_id');	
    }

    public function variantValues() {
    	return $this->hasMany('App\Models\ProductVariantDetail', 'variant_id', 'variant_id')
    				// ->with('variantValueName')
    				->groupBy('variant_value_id');	
    }

    public function variantByLanguage() {
        return $this->hasOne('App\Models\VariantDetail', 'variant_id', 'variant_id')->where('language_id', getLanguage()->id);  
    }

    public function getVariant() {
        return $this->hasOne('App\Models\Variant', 'id', 'variant_id');  
    }


    public function getVariantDetail() {
        return $this->hasMany('App\Models\VariantDetail', 'variant_id', 'variant_id');  
    }



    public function getVariantValueId() {
        return $this->hasMany('App\Models\ProductVariantDetail', 'group_id', 'group_id')->groupBy('variant_value_id');  
    }


    /**
    * Method: getVariantDetails
    * Description: This method is used to get variant details to use in cart module
    */
    public function getVariantValueDetails() {
        return $this->hasMany('App\Models\VariantValueDetail', 'variant_value_id', 'variant_value_id');
    }

    /**
    * Method: getProduct
    * Description: This method is used to getProduct assoiated to this variant deleted or not
    * Author: Sanjoy
    */
    public function getProducts() {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
}
