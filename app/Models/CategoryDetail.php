<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryDetail extends Model
{
    protected $guarded = [];
    /*
    * Method : languageDetails
    * Description : Relationship between categoryDetails & language
    * Author : Jayatri
    * Date : 16/01/2020
    */
    public function languageDetails(){
        return $this->hasMany('App\Models\Language','id','language_id');
    }

    public function CategoryDet(){
        return $this->hasOne('App\Models\Category','id','category_id');
    }

}
