<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VariantValueDetail extends Model
{
    protected $table = 'variant_value_details';
    protected $guarded = [];

    /*
    * Method: getVarinatName
    * Description: This method is used to get variant name to use in cart module
    * Author: Sanjoy
    */
    public function getVarinatValue() {
    	return $this->belongsTo('App\Models\VariantValue', 'variant_value_id', 'id');
    }
}
