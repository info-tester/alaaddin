<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    protected $guarded = [];
    
    public function cityDetailsByLanguage(){
    	return $this->hasOne('App\Models\CitiDetails', 'city_id', 'id')->where('language_id', getLanguage()->id);
    }
    public function cityNamesDetails(){
    	return $this->hasMany('App\Models\CitiDetails', 'city_id', 'id');
    }
    public function state(){
    	return $this->hasOne('App\Models\State', 'id', 'state_id');
    }
}
