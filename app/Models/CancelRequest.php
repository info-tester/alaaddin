<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CancelRequest extends Model
{
    //
    protected $table = 'cancel_requests';
    protected $guarded = [];

    public function get_order_master()
    {
        return $this->hasOne('App\Models\OrderMaster', 'id', 'order_master_id');
    }
    public function product_by_language() {
        return $this->hasOne('App\Models\ProductDetail', 'product_id', 'product_id')->where('language_id', getLanguage()->id);
    }
    
}
