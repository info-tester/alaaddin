<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageContactContent extends Model
{
    protected $table = 'page_contact_content';
    protected $guarded = [];
}
