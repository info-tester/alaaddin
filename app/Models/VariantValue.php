<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VariantValue extends Model
{
	protected $table = 'variant_values';
	
    protected $guarded = [];

    public function variantValueByLanguage() {
    	return $this->hasOne('App\Models\VariantValueDetail','variant_value_id','id')->where('language_id', getLanguage()->id);
    }
    /*
    * Method 		: variantValueDetails
    * Description 	: Relationshi between variantValue & variantValueDetail
    * Author 		: Jayatri
    */
    public function variantValueDetails() {
    	return $this->hasMany('App\Models\VariantValueDetail','variant_value_id','id');
    }

}
