<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantRegId extends Model
{
    protected $table = 'merchant_firebase_id';
    protected $guarded = [];
}
