<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    //
    protected $table = 'wallet';
    protected $guarded = [];

    public function get_order_master()
    {
        return $this->hasOne('App\Models\OrderMaster', 'id', 'order_id');
    }
    public function view_user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
