<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantImage extends Model
{
    protected $table = 'merchant_to_images';
    protected $guarded = [];
    
    /*
    * Method: merchantImg
    * Description: This method is used to get all brands with details based on language.
    * Author : Jayatri
    */
}
