<?php

// use Config;
use App\Models\Language;
use App\Models\ContactUsDetails;
use App\User;


function pr($data) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

/**
* Method: getLanguage
* Description: This method is used to get language by locale lang prefix.
* Author: Sanjoy
*/ 

function getLanguage() {
	return Language::where(['prefix' => Config::get('app.locale')])->first();
}

/**
* variant combinition
*/
function variantValues($productVariantId, $groupId, $variantId, $input) {
	// dump($input);
	$result = [];
	foreach ($input as $key => $value) {
		// dd($value);
		// dump($variantId .'-'. $value->variant_id);
		if($value->group_id == $groupId && $variantId == $value->variant_id) {
			array_push($result, $value->variantValueByLanguage->name);
		}
	}
	return implode(', ', $result);
}
/**
* Method: getDays
* Description: This method is used to get days.
* Author: Surajit
*/ 

function getDays() {
	return $days = [
        'Saturday','Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'
    ];
}
/**
* Method: getCurrency
* Description: This method is used to get Currency.
* Author: Surajit
*/ 
function getCurrency() {
	return $currency = "Rs";
}


/**
* Method: getUserByToken()
* Description: This method is used to get user by auth token.
* Auth: Sanjoy
*/
function getUserByToken() {
	if(@request()->header('Authorization')) {
		$token = explode(' ', request()->header('Authorization'));
		
		if($token[1]){
			// $user = JWTAuth::authenticate($token[1]);
			$user = JWTAuth::setToken($token[1])->toUser();
			return $user;
		}
		
	}
	if(@request()->json()->all()){
		$p = @request()->json()->all();
		if(@$p){
			if(@$p['params']){
				if(@$p['params']['user_id']){
					$user_id = @$p['params']['user_id'];
					$user = User::where(['id'=>@$user_id])->first();
					return $user;
				}
			}
		}
	}
}

/**
* Method: getDefaultImageUrl
* Description: this mehtod is used to show default image.
* Author: Sanjoy
*/
function getDefaultImageUrl() {
	return env('DEFAULT_IMAGE_PATH').env('DEFAULT_IMAGE');
}

/**
* Method: getPersonDefaultImageUrl
* Description: this mehtod is used to show default image.
* Author: Jayatri
*/
function getPersonDefaultImageUrl() {
	return env('DEFAULT_PROFILE_IMAGE_PATH').env('DEFAULT_PRO_IMAGE');
}


function getContent() {
	return $contact_content = ContactUsDetails::first();
}
function sendSms($phone,$message){
	try{
		$apiKey = urlencode('NDYzODcxNmUzNzM3Nzc2YzQ4MzY3ODM0Mzk1NDM5NjY=');
		$mobileNumber = "91".$phone;
		// Message details
		$numbers = array($mobileNumber);
		$sender = urlencode('ALLAAD');
		$message = rawurlencode($message);
		$numbers = implode(',', $numbers);
		// Prepare data for POST request
		$data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
		// Send the POST request with cURL
		$ch = curl_init('https://api.textlocal.in/send/');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		// Process your response here
		return $response;
	}catch(\Exception $e){
		\Log::error($e->getMessage());
	}
}