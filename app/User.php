<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];
    protected $table = 'users';
    protected $guarded = [];
    // protected $fillable = [
    //     'fname', 'lname', 'email', 'password', 'phone', 'email_vcode',
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function userCountryDetails(){
        return $this->hasOne('App\Models\Country','id','country');
    }
    public function userCityDetails(){
        return $this->hasOne('App\Models\CitiDetails','city_id','city_id')->where('language_id',getLanguage()->id);
    }
    public function customerOrders(){
        return $this->hasMany('App\Models\OrderMaster','user_id','id')->latest()->where('status', '!=', 'D');
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function city_details() {
        return $this->hasOne('App\Models\City', 'id', 'city');
    }
    public function state_details() {
        return $this->hasOne('App\Models\State', 'id', 'state');
    }
}
