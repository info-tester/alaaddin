<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProductOtherOptionRepository;
use App\Models\ProductOtherOption;
use App\Validators\ProductOtherOptionValidator;

/**
 * Class ProductOtherOptionRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProductOtherOptionRepositoryEloquent extends BaseRepository implements ProductOtherOptionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductOtherOption::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
