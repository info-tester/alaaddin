<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserFavoriteRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserFavoriteRepository extends RepositoryInterface
{
    //
}
