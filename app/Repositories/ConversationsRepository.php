<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ConversationsRepository.
 *
 * @package namespace App\Repositories;
 */
interface ConversationsRepository extends RepositoryInterface
{
    //
}
