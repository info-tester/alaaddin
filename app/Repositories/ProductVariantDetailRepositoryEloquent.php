<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProductVariantDetailRepository;
use App\Models\ProductVariantDetail;
use App\Validators\ProductVariantDetailValidator;

/**
 * Class ProductVariantDetailRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProductVariantDetailRepositoryEloquent extends BaseRepository implements ProductVariantDetailRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductVariantDetail::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
