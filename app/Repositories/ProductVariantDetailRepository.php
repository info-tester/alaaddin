<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductVariantDetailRepository.
 *
 * @package namespace App\Repositories;
 */
interface ProductVariantDetailRepository extends RepositoryInterface
{
    //
}
