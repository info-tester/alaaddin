<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderMasterRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrderMasterRepository extends RepositoryInterface
{
    //
}
