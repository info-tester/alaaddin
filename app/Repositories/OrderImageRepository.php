<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderImageRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrderImageRepository extends RepositoryInterface
{
    //
}
