<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MerchantCompanyDescriptionRepository.
 *
 * @package namespace App\Repositories;
 */
interface MerchantCompanyDescriptionRepository extends RepositoryInterface
{
    //
}
