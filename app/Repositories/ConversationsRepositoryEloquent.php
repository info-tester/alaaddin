<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ConversationsRepository;
use App\Models\Conversations;
use App\Validators\ConversationsValidator;

/**
 * Class ConversationsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ConversationsRepositoryEloquent extends BaseRepository implements ConversationsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Conversations::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
