<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CartMasterRepository.
 *
 * @package namespace App\Repositories;
 */
interface CartMasterRepository extends RepositoryInterface
{
    //
}
