<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CartDetailRepository;
use App\Models\CartDetail;
use App\Validators\CartDetailValidator;

/**
 * Class CartDetailRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CartDetailRepositoryEloquent extends BaseRepository implements CartDetailRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CartDetail::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
