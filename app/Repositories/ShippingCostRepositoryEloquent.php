<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ShippingCostRepository;
use App\Models\ShippingCost;
use App\Validators\ShippingCostValidator;

/**
 * Class ShippingCostRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ShippingCostRepositoryEloquent extends BaseRepository implements ShippingCostRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ShippingCost::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
