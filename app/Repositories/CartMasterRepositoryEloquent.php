<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CartMasterRepository;
use App\Models\CartMaster;
use App\Validators\CartMasterValidator;

/**
 * Class CartMasterRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CartMasterRepositoryEloquent extends BaseRepository implements CartMasterRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CartMaster::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
