<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MerchantImageRepository;
use App\Models\MerchantImage;
use App\Validators\MerchantImageValidator;

/**
 * Class MerchantImageRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MerchantImageRepositoryEloquent extends BaseRepository implements MerchantImageRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MerchantImage::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
