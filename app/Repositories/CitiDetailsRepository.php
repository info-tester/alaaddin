<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CitiDetailsRepository.
 *
 * @package namespace App\Repositories;
 */
interface CitiDetailsRepository extends RepositoryInterface
{
    //
}
