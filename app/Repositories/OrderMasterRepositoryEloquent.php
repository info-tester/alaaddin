<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OrderMasterRepository;
use App\Models\OrderMaster;
use App\Validators\OrderMasterValidator;

/**
 * Class OrderMasterRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrderMasterRepositoryEloquent extends BaseRepository implements OrderMasterRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrderMaster::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
