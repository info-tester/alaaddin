<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MerchantOppeningRepository;
use App\Models\MerchantOppening;
use App\Validators\MerchantOppeningValidator;

/**
 * Class MerchantOppeningRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MerchantOppeningRepositoryEloquent extends BaseRepository implements MerchantOppeningRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MerchantOppening::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
