<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MerchantCompanyDescriptionRepository;
use App\Models\MerchantCompanyDescription;
use App\Validators\MerchantCompanyDescriptionValidator;

/**
 * Class MerchantCompanyDescriptionRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MerchantCompanyDescriptionRepositoryEloquent extends BaseRepository implements MerchantCompanyDescriptionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MerchantCompanyDescription::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
