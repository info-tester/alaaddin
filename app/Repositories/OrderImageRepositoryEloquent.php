<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OrderImageRepository;
use App\Models\OrderImage;
use App\Validators\OrderImageValidator;

/**
 * Class OrderImageRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrderImageRepositoryEloquent extends BaseRepository implements OrderImageRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrderImage::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
