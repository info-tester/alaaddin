<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CitiDetailsRepository;
use App\Models\CitiDetails;
use App\Validators\CitiDetailsValidator;

/**
 * Class CitiDetailsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CitiDetailsRepositoryEloquent extends BaseRepository implements CitiDetailsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CitiDetails::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
