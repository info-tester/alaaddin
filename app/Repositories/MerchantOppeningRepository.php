<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MerchantOppeningRepository.
 *
 * @package namespace App\Repositories;
 */
interface MerchantOppeningRepository extends RepositoryInterface
{
    //
}
