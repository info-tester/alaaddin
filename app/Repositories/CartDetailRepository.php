<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CartDetailRepository.
 *
 * @package namespace App\Repositories;
 */
interface CartDetailRepository extends RepositoryInterface
{
    //
}
