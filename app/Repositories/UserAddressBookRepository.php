<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserAddressBookRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserAddressBookRepository extends RepositoryInterface
{
    //
}
