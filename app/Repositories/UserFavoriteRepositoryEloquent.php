<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserFavoriteRepository;
use App\Models\UserFavorite;
use App\Validators\UserFavoriteValidator;

/**
 * Class UserFavoriteRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserFavoriteRepositoryEloquent extends BaseRepository implements UserFavoriteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserFavorite::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
