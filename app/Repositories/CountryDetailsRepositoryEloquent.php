<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CountryDetailsRepository;
use App\Models\CountryDetails;
use App\Validators\CountryDetailsValidator;

/**
 * Class CountryDetailsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CountryDetailsRepositoryEloquent extends BaseRepository implements CountryDetailsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CountryDetails::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
