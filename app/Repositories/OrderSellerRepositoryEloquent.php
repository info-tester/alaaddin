<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OrderSellerRepository;
use App\Models\OrderSeller;
use App\Validators\OrderSellerValidator;

/**
 * Class OrderSellerRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrderSellerRepositoryEloquent extends BaseRepository implements OrderSellerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrderSeller::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
