<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderSellerRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrderSellerRepository extends RepositoryInterface
{
    //
}
