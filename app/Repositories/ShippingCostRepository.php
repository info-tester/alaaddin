<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ShippingCostRepository.
 *
 * @package namespace App\Repositories;
 */
interface ShippingCostRepository extends RepositoryInterface
{
    //
}
