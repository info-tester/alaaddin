<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductOtherOptionRepository.
 *
 * @package namespace App\Repositories;
 */
interface ProductOtherOptionRepository extends RepositoryInterface
{
    //
}
