<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserAddressBookRepository;
use App\Models\UserAddressBook;
use App\Validators\UserAddressBookValidator;

/**
 * Class UserAddressBookRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserAddressBookRepositoryEloquent extends BaseRepository implements UserAddressBookRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserAddressBook::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
