<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MerchantImageRepository.
 *
 * @package namespace App\Repositories;
 */
interface MerchantImageRepository extends RepositoryInterface
{
    //
}
