<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CountryDetailsRepository.
 *
 * @package namespace App\Repositories;
 */
interface CountryDetailsRepository extends RepositoryInterface
{
    //
}
