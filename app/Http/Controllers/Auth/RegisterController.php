<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Driver;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use App\Mail\VerificationMail;
use Mail;
use Session;

use Illuminate\Validation\Rule; 


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register-success';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['verifyNewEmail']]);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
        ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname'     => ['required', 'string', 'max:255'],
            'lname'     => ['required', 'string', 'max:255'],
            'email'     => [
                'required', 
                'string', 
                'email', 
                'max:255', 
                Rule::unique('users')->where(function($query) {
                    $query->where('status', '<>', 'D');
                })],
            'phone'     => 'required|numeric',
            'password'  => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'fname'      => $data['fname'],
            'lname'      => $data['lname'],
            'email'      => $data['email'],
            'phone'      => $data['phone'],
            'signup_from'=> 'S',
            'email_vcode'=> time(),
            'password'   => Hash::make($data['password']),
        ]);
        $user['mailBody'] = 'registered';
        $user['customer'] = 'customer';
        $this->sendVerificationMail($user);
    }

    /**
    * Method: sendVerificationMail
    * Desc: This method is used for send verification mail to user
    */
    public function sendVerificationMail($reqData)  {        
        Mail::send(new VerificationMail($reqData));
    }

    /**
    * Method: success
    * Desc: This method is used for show success data after successfully registration
    */

    public function success() {
        return view('modules.extras.registration_success');
    }


    /**
    * Checking email exist or not
    */
    public function chkEmailExist(Request $request)
    {
        $response = [
            'jsonrpc' => '2.0'
        ];
        $merchant = User::where(['email' => trim($request->params['email'])])->where('user_type', '!=', 'G');
        $merchant = $merchant->where(function($query) use($request){
            $query->Where('status','!=','D');
        });
        $merchant = $merchant->first();
        if(@$merchant) {
            $response['error']['merchant'] = 'This email id already exists.';
            
        } else {
            $response['result']['status']  = false;
            $response['result']['message'] = 'This email is available.';
        }
        return response()->json($response);          
    }

    public function chkEmailForgetPassword(Request $request)
    {
        $response = [
            'jsonrpc' => '2.0'
        ];
        $user = User::where(['email' => trim($request->params['email'])])->where('user_type', '!=', 'G');
        $user = $user->where(function($q){
            $q->whereNotIn('user_type', ['D','G']);
        });
        $user = $user->first();
        if(@$user) {
            $response['result'] = 1;
            
        } else {
            $response['result']  = 0;
        }
        return response()->json($response);          
    }

    /*
    *method: verifyEmail
    *params: vcode, id
    *description: for verify user's mail id
    */
    public function verifyEmail($vcode, $id) {
        try {
            $merchant = User::where('email_vcode', $vcode)->first();
            if(@$merchant->email_vcode != null && @$merchant->status == 'U') {
                $update['email_verified_at'] = date('Y-m-d H:i:s');
                $update['email_vcode'] = null;
                $update['status'] = 'A';
                $update['is_email_verified'] = 'Y';
                User::where('id', $merchant->id)->update($update);
                return view('modules.extras.mail_verification_success');
            } else {
                return view('modules.extras.link_expire');
            }
        } catch (\Exception $e) {
            $error = [
                'img'     => 'oops.png',
                'heading' => 'Sorry!',
                'message' => 'Something went to wrong.',
                'title'   => 'Aswagna',
            ];
            return view('modules.errors.all_errors')->with($error);
        }
    }

    /*
    *method: verifyUpdateEmail
    *params: vcode, id
    *description: for verify customer's mail id from user table
    */
    public function verifyUpdateEmail($vcode, $id)
    {
        try {
            $merchant = User::where(['email_vcode'=>$vcode,'id'=>$id])->first();
            if(@$merchant->email_vcode != null) {
                if($merchant->tmp_email != null || $merchant->tmp_email != ""){
                    $update['email_verified_at'] = date('Y-m-d H:i:s');
                    $update['email_vcode'] = null;
                    $update['status'] = 'A';
                    $update['is_email_verified'] = 'Y';
                    $update['email'] = $merchant->tmp_email;
                    $update['tmp_email'] = 'null';

                    User::where('id', $merchant->id)->update($update);
                    // return view('modules.extras.mail_verification_success');
                    return view('merchant.modules.extras.success_mail_varifed');
                }else{
                    return view('merchant.modules.extras.link_expire');
                }
                
            }else{
                return view('modules.extras.link_expire');
            }
        }
        // If any exception arises a new log file will be generated i.e
        // if any product slug is not available and someone tries to give
        // false slug a new error view with error message will be shown.
        catch (\Exception $e) {

            $error = [
                'img'     => 'oops.png',
                'heading' => 'Sorry!',
                'message' => 'Something went to wrong.',
                'title'   => 'Aswagna',
            ];
            return view('modules.errors.all_errors')->with($error);
        }
    }
    /*
    *method: verifyDriverUpdateEmail
    *params: vcode, id
    *description: for verify driver's mail id
    */
    public function verifyDriverUpdateEmail($vcode, $id)
    {
        try {
            // dd($vcode, $id);
            $driver = Driver::where(['email_vcode'=>$vcode,'id'=>$id])->first();
            // dd($user);
            if(@$driver->email_vcode != null) {
                if($driver->tmp_email != null || $driver->tmp_email != ""){
                    $update['email_verified_at'] = date('Y-m-d H:i:s');
                    $update['email_vcode'] = 'null';
                    $update['status'] = 'A';
                    $update['is_email_verified'] = 'Y';
                    $update['email'] = $driver->tmp_email;
                    $update['tmp_email'] = 'null';

                    Driver::where('id', $driver->id)->update($update);
                    // return view('modules.extras.mail_verification_success');
                    return view('merchant.modules.extras.success_mail_varifed');
                }else{
                    return view('merchant.modules.extras.link_expire');
                }
            }else{
                return view('modules.extras.link_expire');
            }
        }
        // If any exception arises a new log file will be generated i.e
        // if any product slug is not available and someone tries to give
        // false slug a new error view with error message will be shown.
        catch (\Exception $e) {
            $error = [
                'img'     => 'oops.png',
                'heading' => 'Sorry!',
                'message' => 'Something went to wrong.',
                'title'   => 'Aswagna',
            ];
            return view('modules.errors.all_errors')->with($error);
        }
    }

    /*
    *method: verifyNewEmail
    *params: vcode, id
    *description: for verify user's mail id
    */
    public function verifyNewEmail($vcode,$id)
    {
        $user = User::where('email_vcode', $vcode)->first();

        if(@$user->email_vcode != null) {
            $update['email_vcode'] = 'null';
            $update['email'] = $user->tmp_email;
            $update['tmp_email'] = 'null';
            User::where('id', $user->id)->update($update);
            return view('modules.extras.mail_update_success');
        } else {
            return view('modules.extras.link_expire');
        }
    }
    
}
