<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Session;
use Cookie;
use Socialite;
use App\User;
use App\Mail\VerificationMail;
use Mail;
use File;
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Models\CartMaster;
use App\Models\cartDetail;
use App\Models\Setting;
use App\Models\ShippingCost;
use App\Merchant;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    protected $cartMaster, $cartMaster1, $cartDetails, $cartData, $sessionId;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CartMasterRepository $cartMaster,
                                CartMasterRepository $cartMaster1,
                                CartDetailRepository $cartDetails
                                )
    {
        $this->middleware('guest')->except('logout');

        $this->cartMaster = $cartMaster;
        $this->cartMaster1 = $cartMaster1;
        $this->cartDetails = $cartDetails;
    }

    protected function guard()
    {            
        return Auth::guard("web");
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->sessionId = session()->getId();
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        
        # check user's cart is empty or not.
        // $this->getCart(session()->getId());

        if ($this->attemptLogin($request)) {

            # update cart for this user. 
            $this->updateCart($this->sessionId);

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
    * Method: checkCart
    * Description: This method is used to check cart is available or not for this user.
    * Author: Sanjoy
    */
    private function getCart($sessionI) {
        $this->cartData = $this->cartMaster->with(['cartDetails.productByLanguage', 'cartDetails.defaultImage', 'cartDetails.productMarchant'])
                        ->orWhere(['session_id' => session()->getId()])
                        ->first();
    }

    /**
    * Mehtod: updateCart
    * Description: This method is used to update cart for this user.
    * Author: Sanjoy
    */
    private function updateCart($sessionId) {
        // if previously added to the cart then merge the cart with pervious and current 
        $existingCart = CartMaster::where(['user_id' => Auth::id()])->first();
        $currentCart = CartMaster::where(['session_id' => $sessionId])->first();
        if($existingCart && @$currentCart) {
            $existingCartDetails = CartDetail::where(['cart_master_id' => $existingCart->id])->get();
            $currentCartDetails = CartDetail::where(['cart_master_id' => $currentCart->id])->get();
            
            # if already added
            $item = 0;
            if($existingCartDetails->count()) {
                foreach ($existingCartDetails as $row) {
                    foreach ($currentCartDetails as $key => $value) {
                        # same product trying to add before login and after login
                        if($row->product_id == $value->product_id) {
                            if(@$row->product_variant_id && @$value->product_variant_id && $row->product_variant_id = $value->product_variant_id) {
                                CartDetail::where(['id' => $row->id])->update([
                                    'quantity'       => $row->quantity + $value->quantity,
                                    'shipping_price' => $row->shipping_price + $value->shipping_price,
                                    'total_discount' => $row->total_discount + $value->total_discount,
                                    'subtotal'       => $row->subtotal + $value->subtotal,
                                    'total'          => $row->total + $value->total,
                                ]);
                                CartDetail::where(['id' => $value->id])->delete();
                            } else if(!@$row->product_variant_id && !@$value->product_variant_id){
                                CartDetail::where(['id' => $row->id])->update([
                                    'quantity'       => $row->quantity + $value->quantity,
                                    'shipping_price' => $row->shipping_price + $value->shipping_price,
                                    'total_discount' => $row->total_discount + $value->total_discount,
                                    'subtotal'       => $row->subtotal + $value->subtotal,
                                    'total'          => $row->total + $value->total,
                                ]);
                                CartDetail::where(['id' => $value->id])->delete();
                            }
                        } else {
                            CartDetail::where(['id' => $value->id])->update([
                                'cart_master_id' => $row->cart_master_id
                            ]);
                        }
                    }
                }
            }
            $totalItems = CartDetail::where(['cart_master_id' => $existingCart->id])->count();
            # update cart master
            CartMaster::where(['user_id' => Auth::id()])->update([
                'total_item'        => $totalItems,
                'total_qty'         => $existingCart->total_qty + $currentCart->total_qty,
                'total_discount'    => $existingCart->total_discount + $currentCart->total_discount,
                'subtotal'          => $existingCart->subtotal + $currentCart->subtotal,
                'total'             => $existingCart->total + $currentCart->total
            ]);
            $currentCart = CartMaster::where(['session_id' => $sessionId])->delete();
        } else {
            CartMaster::where(['session_id' => $sessionId ])->update(['user_id' => Auth::id()]);
        }
    }


    /**
    * Method: calculateShippingPrice
    * Description: This method is used to calculate shipping price
    * Author: Sanjoy
    */
    private function calculateShippingPrice($cartMaster = []) {
        $cartMaster = CartMaster::where(['user_id' => Auth::id()])->first();
        if(@$cartMaster) {
            $sellers = $this->cartDetails->where(['cart_master_id' => $cartMaster->id])->pluck('seller_id')->toArray();

            $uniqueSellers = array_unique($sellers);
            $insideShippingCost = 0;
            $outsideShippingCost = 0;
            foreach ($uniqueSellers as $key => $value) {
                $merchant = Merchant::where(['id' => $value])->first();
                if($merchant->country == 134 && Auth::user()->country == 134) {
                    $insideShippingCost += $merchant->inside_shipping_cost;
                } else {
                    $details = CartDetail::where(['cart_master_id' => $cartMaster->id, 'seller_id' => $value])->get();
                    $weight = $details->sum('weight');

                    $sCost = ShippingCost::where(function($where) use ($weight) {
                        $where->where(function($where1) use ($weight) {
                            $where1->where('from_weight', '<=', @$weight)
                                    ->where('to_weight', '>', @$weight);
                        })
                        ->orWhere(function($where2) use ($weight) {
                            $where2->where('from_weight', '<', @$weight)
                                ->where('to_weight', '>=', @$weight);
                        });
                    })
                    ->first();
                    $outsideShippingCost += @$sCost->internal_order_rate;
                }
            }
            CartMaster::where(['id' => $cartMaster->id])->update([
                'shipping_price' => $outsideShippingCost + $insideShippingCost, 
                'total'          => $cartMaster->total + $outsideShippingCost + $insideShippingCost
            ]);
            return $outsideShippingCost .'-'. $insideShippingCost;
        }
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $credentials =  $request->only($this->username(), 'password');
        $credentials['status'] = 'A';

        return $credentials;
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
        ?: redirect()->intended($this->redirectPath());
    }


    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];
        $user = \App\User::where($this->username(), $request->{$this->username()})->first();

        // Check if user was successfully loaded, that the password matches
        // and active is not 1. If so, override the default error message.
        if ($user && \Hash::check($request->password, $user->password) && $user->status == 'I') {
            $errors = [$this->username() => 'Your account is not active.'];
        }
        // if ($user && \Hash::check($request->password, $user->password) && $user->status == 'AA') {
        //     $errors = [$this->username() => 'Your account is not active by Admin.'];
        // }
        if ($user && \Hash::check($request->password, $user->password) && $user->status == 'U') {
            $errors = ['not verified' => 'Your email is not verified.'];
        }
        
        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        // dd($errors);
        return redirect()->back()
        ->withInput($request->only($this->username(), 'remember'))
        ->withErrors($errors);
    }    


    /**
    * Method : socialLogin,handleProviderCallback
    * Desc: For Social Login
    * Author: Surajit
    */

    public function socialLogin($social)
    {
        return Socialite::driver($social)->redirect();
    }

    public function handleProviderCallback($social)
    {
        $userSocial = Socialite::driver($social)->stateless()->user();    
        // dd($userSocial);
        $user = User::where(['email' => $userSocial->getEmail(),'is_email_verified' => 'Y'])->where('status', '!=', 'D')->first();

        if($user) {  
            Auth::login($user);
            return redirect()->route('dashboard');
        } else {
            //split full name
            $name = $userSocial->getName();
            $splitName = explode(' ', $name, 2);
            $first_name = $splitName[0];
            $last_name = !empty($splitName[1]) ? $splitName[1] : '';
            if($userSocial->getEmail()) {

                //save profile image
                $fileContents = file_get_contents($userSocial->getAvatar());
                $file_name = $userSocial->getId() . ".jpg";
                File::put('storage/app/public/customer/profile_pics/' . $file_name, $fileContents);

                if($social=='facebook') {

                    $user = User::create([
                        'fname'         => $first_name,
                        'lname'         => $last_name,
                        'image'         => $file_name,
                        'email'         => $userSocial->getEmail(),
                        'facebook_id'   => $userSocial->getId(),
                        'provider'      => 'facebook',
                        'is_email_verified' => 'Y',
                        'signup_from' => 'F',
                        'status'      => 'A'
                    ]);
                } else {
                    $user = User::create([
                        'fname'       => @$first_name,
                        'lname'       => @$last_name,
                        'image'       => $file_name,
                        'email'       => $userSocial->getEmail(),
                        'google_id'   => $userSocial->getId(),
                        'provider'    => 'google',
                        'is_email_verified'   => 'Y',
                        'signup_from' => 'G',
                        'status'      => 'A'
                    ]);
                }
                Auth::login($user);
                return redirect()->route('dashboard');
            } else {
                if($social=='facebook') {
                    $socialInsert = 'facebook';
                    $signup_frm   = 'F';
                } else {
                    $socialInsert = 'google';                    
                    $signup_frm   = 'G';
                }
                $fileContents = file_get_contents($userSocial->getAvatar());
                $file_name = $userSocial->getId() . ".jpg";
                File::put('storage/app/public/customer/profile_pics/' . $file_name, $fileContents);
                
                $user = User::create([
                    'fname'         => @$first_name,
                    'lname'         => @$last_name,
                    'image'         => $file_name,
                    'facebook_id'   => $userSocial->getId(),
                    'provider'      => $socialInsert,
                    'signup_from'   => $signup_frm,
                    'status'        => 'A',
                    'is_email_verified'   => 'Y'
                ]);
                
                return redirect()->route('facebook.email.login.page',$userSocial->getId());
            }
        }
    }

    /**
    * Method : facebookEmailLoginPage
    * Desc: For facebook login with phone number
    * Author: Surajit
    */

    public function facebookEmailLoginPage($id)
    {
        return view('auth.facebook_email_login')->with('id', $id);
    }
    
    /**
    * Method : facebookEmailLogin
    * Desc: For facebook login update email and login user
    * Author: Surajit
    */

    public function facebookEmailLogin(Request $request, $id)
    {
        $request->validate([
            'email' => 'required|email|unique:users'
        ]);

        $update['email'] = $request->email;   
        User::where('facebook_id', $id)->update($update);

        $user = User::where('facebook_id', $id)->first();
        Auth::login($user);
        return redirect()->route('dashboard');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        return $this->loggedOut($request) ?: redirect('/');
    }
}
