<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\User;
use Illuminate\Support\Facades\Hash;
use Config;
use Validator;
use Auth;
use Mail;
use App\Mail\ResetPasswordCustomerMail;
//ResetPasswordCustomerMail

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showFormResetPassword(){
        return view('auth.reset_password');
    }

    public function sendResetPasswordLink(Request $request){
        if(@$request->all()){
            $customer = User::where('email',@$request->email)->where('status', 'A')->where('user_type', 'C')->first();
            if(@$customer){
                $user               =   new \stdClass();
                $user->fname        =   @$customer->fname;
                $user->lname        =   @$customer->lname;
                $user->email        =   @$customer->email;
                $user->id           =   encrypt(@$customer->id);
                Mail::send(new ResetPasswordCustomerMail($user));    
                //success
                session()->flash("success",__('success.-4073'));
                return redirect()->back();
            }else{
                //error
                session()->flash("error",__('errors.-5071'));
                return redirect()->back();    
            }
        }        
    }


    public function updateResetPassword($id,Request $request){
        $userId = decrypt($id);
        $user = User::where('id',$userId)->where('status', 'A')->where('user_type', 'C')->first();
        if(@$request->all()){
            if(@$user){
                if(@$request->all()){
                    if($request->new_password == $request->password_confirmation){
                        $update['password'] = Hash::make($request->new_password);
                        User::where(['id' => $userId, 'user_type' => 'C'])->update($update);    
                        session()->flash("success",__('success.-4074'));
                        return redirect()->route('login');   
                    } else {
                        //error message
                        session()->flash("error",__('errors.-5072'));
                        return redirect()->back();               
                    }
                } else {
                    return view('auth.set_new_password');
                }
            } else {
                //error messsage
                session()->flash("error","User not found! Password not changed! Please try again!"); 
                return redirect()->back();        
            }
        }else{
            return view('auth.set_new_password')->with(['id'=>$id]);
        }
    }


    public function showResetForm($id){
        
    }
}
