<?php

namespace App\Http\Controllers\Merchant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Merchant;
use App\Models\MerchantImage;
use App\Models\MerchantOppening;
use App\Models\Country;
use App\Models\State;
use App\Models\Language;
use App\Models\MerchantCompanyDescription;
use App\Models\OrderMaster;
use App\Models\OrderDetail;
use App\Models\OrderSeller;
use App\Models\MerchantRegId;
use App\Models\Product;
use Auth;
use App\Mail\VerificationMail;
use Mail;
use Session;
use Config;
use Carbon\Carbon;

class HomeController extends Controller
{

    protected $redirectTo = '/merchant/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('merchant.auth:merchant', ['except' => ['fetchState']]);
    }

    /**
     * Show the Merchant dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $m = Merchant::where('id',@Auth::guard('merchant')->user()->id)->first();
        #Total sell in dashboard
        $data['totalProfit']  = $m->total_earning;

        #Today's sell in dashboard
        $todayIntProfit = OrderDetail::with(['orderMaster'])->whereDate('updated_at', date('y-m-d'))->where(['status'=>'OD','seller_id'=>@Auth::guard('merchant')->user()->id]);
        $todayIntProfit = $todayIntProfit->whereHas('orderMaster', function($q){
            $q->where('order_type','I');
        });
        $todayIntProfit = $todayIntProfit->whereYear('updated_at', date('Y-m-d'))->whereMonth('updated_at', date('m'))->sum('total');

        $data['todayExtProfit'] = OrderDetail::with(['orderMaster'])->whereDate('updated_at', date('y-m-d'))->where(['status'=>'OD','seller_id'=>@Auth::guard('merchant')->user()->id]);
        $data['todayExtProfit'] = $data['todayExtProfit']->whereHas('orderMaster', function($q){
            $q->where('order_type','E');
        });
        $data['todayExtProfit'] = $data['todayExtProfit']->whereDate('updated_at', date('y-m-d'))->sum('sub_total');

        $todayExtProfit = $data['todayExtProfit'];
        $todayCom = ($todayIntProfit * $m->commission)/100;
        $data['todayProfit'] = ($todayIntProfit - $todayCom) + $todayExtProfit;
        
        $totalOrders = OrderMaster::with('orderMasterDetails')->whereNotIn('status',['F','I','OC','D']);
        $data['totalOrders']  = $totalOrders->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        })->count();

        $data['totalProducts'] = Product::Where('user_id',@Auth::guard('merchant')->user()->id)->where('status', '!=', 'D')->count();

        //total orders of today and this month
        $todayExOrders = OrderMaster::with('orderMasterDetails')->where('status','OD');
        $todayExOrders  = $todayExOrders->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });
        $data['todayExOrders'] = $todayExOrders->whereDate('updated_at', date('y-m-d'))->where('order_type', 'E')->count();        

        $todayInOrders = OrderMaster::with('orderMasterDetails')->where('status','OD');
        $todayInOrders  = $todayInOrders->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });

        $data['todayInOrders'] = $todayInOrders->whereDate('updated_at', date('y-m-d'))->where('order_type', 'I')->count();

        $monthlyExOrders = OrderMaster::with('orderMasterDetails')->where('status','OD');
        $monthlyExOrders  = $monthlyExOrders->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });
        $data['monthlyExOrders'] = $monthlyExOrders->whereYear('updated_at', date('Y'))->whereMonth('updated_at', date('m'))->where('order_type', 'E')->count();

        $monthlyInOrders = OrderMaster::with('orderMasterDetails')->where('status','OD');
        $monthlyInOrders  = $monthlyInOrders->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });
        $data['monthlyInOrders'] = $monthlyInOrders->whereYear('updated_at', date('Y'))->whereMonth('updated_at', date('m'))->where('order_type', 'I')->count();
        
        //total order earnings of today and this month
        $todayExOrderEarnings = OrderMaster::with('orderMasterDetails')->where(['status' => 'OD']);
        $todayExOrderEarnings  = $todayExOrderEarnings->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });
        $data['todayExOrderEarnings'] = $todayExOrderEarnings->whereDate('updated_at', date('y-m-d'))->where('order_type', 'E')->sum('subtotal');        

        $data['todayInOrderEarnings'] = ($todayIntProfit - $todayCom);

        $data['monthlyInOrderEarnings'] = OrderDetail::with(['orderMaster'])->where(['status'=>'OD','seller_id'=>@Auth::guard('merchant')->user()->id]);
        $data['monthlyInOrderEarnings'] = $data['monthlyInOrderEarnings']->whereHas('orderMaster', function($q){
            $q->where('order_type','I');
        });
        $data['monthlyInOrderEarnings'] = $data['monthlyInOrderEarnings']->whereYear('updated_at', date('Y'))->whereMonth('updated_at', date('m'))->sum('total'); 
        $data['monthlyInOrderEarnings'] = $data['monthlyInOrderEarnings'] - (($data['monthlyInOrderEarnings'] * $m->commission)/100);
        
        /*$data['monthlyExOrderEarnings'] = OrderDetail::with(['orderMaster'])
        ->where(['status'=>'OD','seller_id'=>@Auth::guard('merchant')->user()->id])
        ->whereYear('updated_at', date('Y'))
        ->whereMonth('updated_at', date('m'))
        ->sum('sub_total');*/

        $data['monthlyExOrderEarnings'] = OrderDetail::with(['orderMaster'])
        ->where(['status'=>'OD','seller_id'=>@Auth::guard('merchant')->user()->id]);
        
        $data['monthlyExOrderEarnings'] = $data['monthlyExOrderEarnings']->whereHas('orderMaster', function($q){
            $q->where('order_type','E');
        });
        
        $data['monthlyExOrderEarnings'] = $data['monthlyExOrderEarnings']->whereYear('updated_at', date('Y'))
        ->whereMonth('updated_at', date('m'))
        ->sum('sub_total'); 

        $orders = OrderMaster::whereNotIn('status', ['I','D','','F'])->with(['customerDetails','orderMasterDetails.driverDetails', 'orderMasterDetails','orderMasterDetails.productDetails','orderMasterDetails.productVariantDetails','orderMasterDetails.sellerDetails','countryDetails','shippingAddress','billingAddress','getBillingCountry','productVariantDetails']);
        $data['orders']  = $orders->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        })->orderBy('id','desc')->take(5)->get();

        $data['products'] = Product::Where('user_id',@Auth::guard('merchant')->user()->id)->where('status', '!=', 'D')
        ->with('productByLanguage','defaultImage','productMarchant:id,fname,lname','productCategory.Category','productSubCategory.Category.priceVariant')
        ->orderBy('id','desc')->take(5)->get();
    
        #code this month external order (No of orders in 30days) for graph
        $data['externalOrderMonth'] = OrderMaster::with('orderMasterDetails')->where('status','OD')->select('id','created_at',\DB::raw('YEAR(created_at) year, Day(created_at) day'),\DB::raw('Count(id) totalOrder'))->whereDate('created_at', '>', Carbon::now()->subDays(30));
        $data['externalOrderMonth']  = $data['externalOrderMonth']->whereHas('orderMasterDetails',function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });
        $data['externalOrderMonth']  = $data['externalOrderMonth']->where(function($q){
            $q->where('order_type','E');
        });
        $data['externalOrderMonth'] = $data['externalOrderMonth']->groupBy('day')->orderBy('day','asc')->get();

        for($i=0;$i<30;$i++){
            $data['extValue'][$i] = 0;
        }
        foreach(@$data['externalOrderMonth'] as $cust){
            $custDay = @$cust->day;
            $data['extValue'][$custDay] = @$cust->totalOrder;
        }

        #code for this internal order monthly (no of orders) for graph
        $data['internalOrderMonth'] = OrderMaster::with('orderMasterDetails')->where('status','OD')->select('id','created_at',\DB::raw('YEAR(created_at) year, Day(created_at) day','status'),\DB::raw('Count(id) totalOrder'))->whereDate('created_at', '>', Carbon::now()->subDays(30));
        $data['internalOrderMonth']  = $data['internalOrderMonth']->whereHas('orderMasterDetails',function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });
        $data['internalOrderMonth']  = $data['internalOrderMonth']->where(function($q){
            $q->where('order_type','I');
        });
        $data['internalOrderMonth'] = $data['internalOrderMonth']->groupBy('day')->orderBy('day','asc')->get();
        
        for($i=0;$i<30;$i++){
            $data['intValue'][$i] = 0;
        }
        foreach(@$data['internalOrderMonth'] as $cust){
            $custDay = @$cust->day;
            $data['intValue'][$custDay] = @$cust->totalOrder;
        }
        return view('merchant.modules.dashboard.home', $data);
    }
    /*
     * Method : changeDash
     * Description : Used Ajax call in merchant dashboard when notification of web in merchant dashboard comes change the value evrything in dashboard required.
     * Author : Jayatri
     */
    public function changeDash(Request $request) {
        if(@$request->all()){
            // $response = ['jsonrpc'=>'2.0'];
            // $reqData = $request->json()->all();
            // $token = $reqData['data']['token'];
            // dd($token);
            // dd();
            // dd($token);
            $token = @$request->token;
            #reg web notify
            $this->updateMerchant($token);    
        }
        
        
    }
    /**
     * Method : showEditProfile
     * Description : It'll show edit profile page
     * Author : Surajit
     */
    public function showEditProfile() {
        $data['country']  = Country::with('countryDetailsBylanguage')->where('status','!=','D')->get();
        $data['language'] = Language::where([
            'status'    =>  'A'
        ])
        ->get();
        $data['state']    = State::where([
            'country_id' =>  @Auth::guard('merchant')->user()->country
        ])->get();
        $data['time']     = MerchantOppening::where('merchant_id',@Auth::guard('merchant')->user()->id)
        ->get();
        $data['marchentImg']   = MerchantImage::where('merchant_id',@Auth::guard('merchant')->user()->id)->get();
        $data['marchentDesc']  = MerchantCompanyDescription::where('merchant_id',@Auth::guard('merchant')->user()->id)->get();
        $data['merchant']  = Merchant::with('merchantCityDetails')->where('id',@Auth::guard('merchant')->user()->id)->first();
        // dd($data);
        //dd($data['time']);
        return view('merchant.modules.dashboard.edit_profile', $data);
    }

    /**
     * Method : storeProfileDetails
     * Description : It'll store profile details into merchants, tables
     * Author : Surajit
     */
    public function storeProfileDetails(Request $request) {
         // dd($request->all());
        // $validator = $request->validate([
        //     'fname'        => 'required',
        //     'lname'        => 'required',
        //     'email'        => 'required',
        //     'phone'        => 'required',
        //     // 'description'  => 'required',
        // ]);
        // if($validator)
        // {
        //     $merchant['fname']     =   $request->fname;
        //     $merchant['lname']     =   $request->lname;
        //     $merchant['email1']    =   $request->email1;
        //     $merchant['phone']     =   $request->phone;
        //     $merchant['company_name']     =   $request->company_name;
        //     $merchant['address']   =   $request->address;
        //     $merchant['city']      =   $request->city;
        //     $merchant['state']     =   $request->state;
        //     $merchant['zipcode']   =   $request->zipcode;
        //     $merchant['country']   =   $request->country;
        //     $merchant['bank_name'] =   $request->bank_name;
        //     $merchant['account_name']    =   $request->account_name;
        //     $merchant['account_number']  =   $request->account_number;
        //     $merchant['iban_number']     =   $request->iban_number;
        //     $merchant['payment_mode']    =   $request->payment_mode;
        //     if($request->image) {
        //         $image     = $request->image; 
        //         $imgName   = time().".".$image->getClientOriginalExtension();
        //         $image->move('storage/app/public/profile_pics', $imgName);
        //         $merchant['image']     =   $imgName;
        //     }
        //     if($request->email2) {
        //         $merchant['email2']     =   $request->email2;
        //     }
        //     if($request->email3) {
        //         $merchant['email3']     =   $request->email3;
        //     }
        //     if($request->email != @Auth::guard('merchant')->user()->email){
        //         $merchant['tmp_email']       =   $request->email;
        //         $merchant['email_vcode']     =   time();
        //     }
        //     Merchant::whereId(@Auth::guard('merchant')->user()->id)->update($merchant);

        //     if($request->email != @Auth::guard('merchant')->user()->email){
        //         $merchant['id']       =   @Auth::guard('merchant')->user()->email;
        //         $merchant['email']    =   $request->email;
        //         $merchant['mailBody'] = 'updated';
        //         $this->sendVerificationMail($merchant);
        //     }
        //     $this->updatedMerchantOppening($request);
        //     //store portfolio images
        //     if($request->images) {
        //         $this->updatedMerchantImage($request);
        //     }
        //     if($request->description) {
        //         $this->updatedMerchantCompanyDescription($request);
        //     }
        // }

        // Change user password.
        if(@$request->new_password) {
            $this->changePassword($request);
        } else {
            session()->flash("success",__('success_merchant.-700'));
        }
        return redirect()->back();
    }

    /**
    * Method: sendVerificationMail
    * Desc: This method is used for send verification mail to user
    */
    public function sendVerificationMail($reqData)  {        
        Mail::send(new VerificationMail($reqData));
    }

    /*
    * Method: updatedMerchantOppening
    * Description: It will update merchant into merchant_to_oppenings tables
    * Author: Surajit
    */
    private function updatedMerchantOppening(Request $request){

        // dd($request->all());
        // $validator = $request->validate([
        //     'images' => 'required',
        //     'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        // ]);
        // if($validator)
        // {
        MerchantOppening::where('merchant_id',@Auth::guard('merchant')->user()->id)->delete();
        foreach ($request->day as $key => $row) {
            $day = $row;
            if ($request->from_time[$key] != '' && $request->to_time[$key] != '') {
                $from_time = date('H:i:s', strtotime($request->from_time[$key]));
                $to_time   = date('H:i:s', strtotime($request->to_time[$key]));
            } else {
                $from_time = $request->from_time[$key];
                $to_time   = $request->to_time[$key];
            }
            $merchant = new MerchantOppening;
            $merchant['merchant_id']     =   @Auth::guard('merchant')->user()->id;
            $merchant['days']            =   $row;
            $merchant['from_time']       =   $from_time;
            $merchant['to_time']         =   $to_time;
            $merchant->save();
        }
            // die();
        // }
    }

    /*
    * Method: updatedMerchantImage
    * Description: It will update merchant into merchant_to_images tables
    * Author: Surajit
    */
    private function updatedMerchantImage($request){

        // dd($request->all());
        $validator = $request->validate([
            'images' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        if($validator)
        {
            if($request->images) {
                $i=0;
                foreach($request->images as $key=>$file){
                    $imgName   = $i.time().".".$file->getClientOriginalExtension();
                    $file->move('storage/app/public/portfolio_pics', $imgName);
                    MerchantImage::create([
                        "merchant_id"     => @Auth::guard('merchant')->user()->id,
                        "image"           => $imgName
                    ]);
                    $i++;
                    $imgName="";
                }
            }
        }
    }

    /*
    * Method: updatedMerchantCompanyDescription
    * Description: It will add/update merchant into merchant_to_company_descriptions tables
    * Author: Surajit
    */
    private function updatedMerchantCompanyDescription($request){

        // dd($request->all());
        MerchantCompanyDescription::where('merchant_id',@Auth::guard('merchant')->user()->id)->delete();
        foreach ($request->description as $description => $descVal) {
            $merchantDesc = new MerchantCompanyDescription;
            $merchantDesc['merchant_id']     =   @Auth::guard('merchant')->user()->id;
            $merchantDesc['language_id']     =   $description;
            $merchantDesc['description']     =   nl2br($descVal['0']);
            $merchantDesc->save();
        }
    }

    /**
    * Method: changePassword
    * Description: This method is used to change user password
    * Author: Surajit 
    */
    private function changePassword($request) {
        // dd($request->all());
        if($request->new_password) {
            if(\Hash::check($request->password, @Auth::guard('merchant')->user()->password)){
                $user_detail = @Auth::guard('merchant')->user();
                $user_detail->password=\Hash::make($request->confirm_password);
                $user_detail->save();
                session()->flash("success",__('success_merchant.-700'));
            } else {
                session()->flash("error",__('success_merchant.-701'));
            }
        }
    }


    /*
    * Method: fetchState
    * Description: It will fetch state of country
    * Author: Surajit 
    */
    public function fetchState(Request $request) {
     $value = $request->get('value');
     $data  = State::select([
        'id',
        'name',
        'country_id'
    ])
     ->where([
        'country_id' =>  $value
    ])
     ->get();

     $output = '<option value="">Select State</option>';
     foreach($data as $row)
     {
      $output .= '<option value="'.$row->id.'">'.@$row->name.'</option>';
  }
  echo $output;
}

    /*
    * Method: removeImg
    * Description: It will delete from merchant_to_images table in ajax
    * Author: Surajit 
    */
    public function removeImg($id){

        $status = '';
        $response = [
            'jsonrpc'   => '2.0'
        ];      
        MerchantImage::whereId($id)->delete();
        return response()->json(['success' => 'success']);
    }


    /**
    * Method: changeLanguage
    * Description: This method is used to change language
    */
    public function changeLanguage() {

        $lang = '';
        if(Session::get('applocale') == 'ar') {
            $lang = 'en';
        } else {
            $lang = 'ar';
        }
        if (array_key_exists($lang, Config::get('languages'))) {            
            Session::put('applocale', $lang);
            // dd($lang, Config::get('languages'));
        }
        // dd(Session::get('applocale'));
        return redirect()->back();
    }
    /*
    * Method: updateMerchant
    * Description: This method is used to update merchant
    * Author: Jayatri
    */
    private function updateMerchant($token){
        $token = $token;
        // dd($token);
        // $email = $request->data['email'];
        // $password = $request->data['password'];
        $merchant = Merchant::where(['id'=>@Auth::guard('merchant')->user()->id])->first();
        if(@$merchant){
            $firebaseToken_id['firebaseToken_id']   = $token;
            Merchant::where('id',@$merchant->id)->update($firebaseToken_id);
            $this->updateMerchantMultipleToken($merchant,$token);
        }

    }
    /*
    * Method: updateMerchantMultipleToken
    * Description: This method is used to update multiple token
    * Author: Jayatri
    */
    //merchant_firebase_id
    private function updateMerchantMultipleToken($merchant,$token){
        $updateUser['reg_id'] = @$token;
        $updateUser['user_id'] = @$merchant->id;

        $regIds = MerchantRegId::where(['reg_id' => @$tokens])->first();

        if(!@$regIds) {
            MerchantRegId::create($updateUser);
        }else{
            $upd['user_id'] = @$merchant->id;
            MerchantRegId::where(['reg_id'=>@$tokens])->update($upd);
        }
    }

    /**
    * Method: getOrderStat
    * Description: This method is used to get all stat of merchant when notification is being received.
    * Author: Sanjoy
    */
    public function getOrderStat() {
        #total Profit calcuation  1sta tab in merchant dashbaord
        $totalProfit = OrderMaster::with('orderMasterDetails')->whereNotIn('status',['I','OC','D','F']);

        $m = Merchant::where('id',@Auth::guard('merchant')->user()->id)->first();
        
        #Total sell in dashboard
        $data['totalProfit']  = @$m->total_earning;
        
        #Today's sell in dashboard
        $todayIntProfit = OrderDetail::with(['orderMaster'=> function($query) {
            $query->where(function($q) {
                $q->where('order_type', 'I');
            });
        }])->where(['status'=>'OD','seller_id'=>@Auth::guard('merchant')->user()->id])->whereDate('updated_at', date('y-m-d'))->sum('total');

        $data['todayExtProfit'] = OrderDetail::with(['orderMaster'])->where(['status'=>'OD','seller_id'=>@Auth::guard('merchant')->user()->id]);
        $data['todayExtProfit'] = $data['todayExtProfit']->whereHas('orderMaster', function($q){
            $q->where('order_type','E');
        });
        $data['todayExtProfit'] = $data['todayExtProfit']->whereNotIn('status',['I','OC','D'])->whereDate('updated_at', date('y-m-d'))->sum('sub_total');

        $todayExtProfit = $data['todayExtProfit'];
        $todayCom = ($todayIntProfit * $m->commission)/100;
        $data['todayProfit'] = ($todayIntProfit - $todayCom) + $todayExtProfit;
        $totalOrders = OrderMaster::with('orderMasterDetails')->whereNotIn('status',['I','OC','D','F']);
        $data['totalOrders']  = $totalOrders->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        })->count();

        $data['totalProducts'] = Product::Where('user_id',@Auth::guard('merchant')->user()->id)->where('status', '!=', 'D')->count();

        //total orders of today and this month
        $todayExOrders = OrderMaster::with('orderMasterDetails')->whereNotIn('status',['I','F','OC','D']);
        $todayExOrders  = $todayExOrders->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });
        $data['todayExOrders'] = $todayExOrders->whereDate('updated_at', date('Y-m-d'))->where('order_type', 'E')->count();        

        $todayInOrders = OrderMaster::with('orderMasterDetails')->whereNotIn('status',['I','F','OC','D']);
        $todayInOrders  = $todayInOrders->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });

        $data['todayInOrders'] = $todayInOrders->whereDate('updated_at', date('Y-m-d'))->where('order_type', 'I')->count();

        $monthlyExOrders = OrderMaster::with('orderMasterDetails')->whereNotIn('status',['I','F','OC','D']);
        $monthlyExOrders  = $monthlyExOrders->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });
        $data['monthlyExOrders'] = $monthlyExOrders->whereYear('updated_at', date('Y'))->whereMonth('updated_at', date('m'))->where('order_type', 'E')->count();

        $monthlyInOrders = OrderMaster::with('orderMasterDetails')->whereNotIn('status',['I','F','OC','D']);
        $monthlyInOrders  = $monthlyInOrders->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });
        $data['monthlyInOrders'] = $monthlyInOrders->whereYear('updated_at', date('Y'))->whereMonth('updated_at', date('m'))->where('order_type', 'I')->count();

        //total order earnings of today and this month
        $todayExOrderEarnings = OrderMaster::with('orderMasterDetails')->where(['status' => 'OD']);
        $todayExOrderEarnings  = $todayExOrderEarnings->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });
        $data['todayExOrderEarnings'] = $todayExOrderEarnings->whereDate('updated_at', date('y-m-d'))->where('order_type', 'E')->sum('subtotal');        

        $data['todayInOrderEarnings'] = ($todayIntProfit - $todayCom);

        $data['monthlyInOrderEarnings'] = OrderDetail::with(['orderMaster'])->where(['status'=>'OD','seller_id'=>@Auth::guard('merchant')->user()->id]);
        $data['monthlyInOrderEarnings'] = $data['monthlyInOrderEarnings']->whereHas('orderMaster', function($q){
            $q->where('order_type','I');
        });
        $data['monthlyInOrderEarnings'] = $data['monthlyInOrderEarnings']->whereYear('updated_at', date('Y'))->whereMonth('updated_at', date('m'))->sum('total'); 
        $data['monthlyInOrderEarnings'] = $data['monthlyInOrderEarnings'] - (($data['monthlyInOrderEarnings'] * $m->commission)/100);
        $data['monthlyExOrderEarnings'] = OrderDetail::with(['orderMaster'])->where(['status'=>'OD','seller_id'=>@Auth::guard('merchant')->user()->id])->whereYear('updated_at', date('Y'))->whereMonth('updated_at', date('m'))->sum('sub_total');

        $data['monthlyExOrderEarnings'] = OrderDetail::with(['orderMaster'])->where(['status'=>'OD','seller_id'=>@Auth::guard('merchant')->user()->id]);
        $data['monthlyExOrderEarnings'] = $data['monthlyExOrderEarnings']->whereHas('orderMaster', function($q){
            $q->where('order_type','E');
        });
        $data['monthlyExOrderEarnings'] = $data['monthlyExOrderEarnings']->whereYear('updated_at', date('Y'))->whereMonth('updated_at', date('m'))->sum('total'); 

        $orders = OrderMaster::whereNotIn('status', ['I','D','OC','F'])->with(['customerDetails','orderMasterDetails.driverDetails','countryDetails','orderMasterDetails','orderMasterDetails.productDetails','orderMasterDetails.productVariantDetails','orderMasterDetails.sellerDetails','countryDetails','shippingAddress','billingAddress','getBillingCountry','productVariantDetails']);
        $data['orders']  = $orders->whereHas('orderMasterDetails', function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        })->orderBy('id','desc')->take(5)->get();

        $data['product'] = Product::Where('user_id',@Auth::guard('merchant')->user()->id)->where('status', '!=', 'D')
        ->with('productByLanguage','defaultImage','productMarchant:id,fname,lname','productCategory.Category','productSubCategory.Category.priceVariant')
        ->orderBy('id','desc')->take(5)->get();
        
        #code this month external order (No of orders in 30days) for graph
        $data['externalOrderMonth'] = OrderMaster::with('orderMasterDetails')->where('status','OD')->select('id','created_at',\DB::raw('YEAR(created_at) year, Day(created_at) day'),\DB::raw('Count(id) totalOrder'))->whereDate('created_at', '>', Carbon::now()->subDays(30));
        $data['externalOrderMonth']  = $data['externalOrderMonth']->whereHas('orderMasterDetails',function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });
        $data['externalOrderMonth']  = $data['externalOrderMonth']->where(function($q){
            $q->where('order_type','E');
        });
        $data['externalOrderMonth'] = $data['externalOrderMonth']->groupBy('day')->orderBy('day','asc')->get();
        for($i=0;$i<30;$i++){
            $data['extValue'][$i+1] = 0;
        }
        foreach(@$data['externalOrderMonth'] as $cust){
            $custDay = @$cust->day;
            // $data['RegistrationDay'][$custDay] = @$cust->day;
            $data['extValue'][$custDay] = @$cust->totalOrder;
        }
        #code for this internal order monthly (no of orders) for graph
        
        $data['internalOrderMonth'] = OrderMaster::with('orderMasterDetails')->where('status','OD')->select('id','created_at',\DB::raw('YEAR(created_at) year, Day(created_at) day','status'),\DB::raw('Count(id) totalOrder'))->whereDate('created_at', '>', Carbon::now()->subDays(30));
        $data['internalOrderMonth']  = $data['internalOrderMonth']->whereHas('orderMasterDetails',function($q){
            $q->where('seller_id',@Auth::guard('merchant')->user()->id);
        });
        $data['internalOrderMonth']  = $data['internalOrderMonth']->where(function($q){
            $q->where('order_type','I');
        });
        $data['internalOrderMonth'] = $data['internalOrderMonth']->groupBy('day')->orderBy('day','asc')->get();
        for($i=0;$i<30;$i++){
            $data['intValue'][$i+1] = 0;
        }
        foreach(@$data['internalOrderMonth'] as $cust){
            $custDay = @$cust->day;
            // $data['RegistrationDay'][$custDay] = @$cust->day;
            $data['intValue'][$custDay] = @$cust->totalOrder;
        }
        if(getLanguage()->id == 1){
            $data['lang'] = 1;
            $data['ordLang'] = "Order Number";
            $data['dt'] = "Date";
            $data['type'] = "Type";
            $data['total'] = "Total";
            $data['status'] = "Status";
            $data['internal'] = "Internal ";
            $data['external'] = "External ";
            $data['new'] = "New";
            $data['driver_assigned'] = "Driver Assigned";
            $data['view_all'] = "View All";
        }else{
            $data['lang'] = 2;
            $data['ordLang'] = "رقم الأمر  ";
            $data['dt'] = "تاريخ ";
            $data['type'] = "نوع ";
            $data['total'] = "مجموع  ";
            $data['status'] = "الحالة  ";
            $data['internal'] = "ير نشط   ";
            $data['external'] = "نشط  ";
            $data['new'] = "نشط  ";
            $data['driver_assigned'] = "نشط  ";
            $data['view_all'] = "عرض الكل  ";
        }
        return response()->json($data);
    }
}