<?php

namespace App\Http\Controllers\Merchant\Auth;

use App\Merchant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use App\Models\Country;
use App\Models\City;
use App\Models\CitiDetails;
use App\Repositories\CityRepository;
use App\Repositories\CitiDetailsRepository;
use App\Models\Customer;
use App\Mail\VerificationMail;
use Mail;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new admins as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/merchant/success';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CityRepository $city,
     CitiDetailsRepository $city_details)
    {
        $this->middleware('merchant.guest:merchant', ['except' => ['verifyEmail','verifyNewEmail','chkNewEmailExist']]);
        $this->city                     =   $city;
        $this->city_details             =   $city_details;
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('merchant');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {        
        $data['country']  = Country::with('countryDetailsBylanguage')->where('status','!=','D')->get();
        return view('merchant.auth.register', $data);
    }
    public function fetchCity(Request $request){
        $response = array(
            'jsonrpc' => '2.0'
        );
        // dd(@$request->all());
        // $cities = new City;
        if($request->get('city'))
        {
            // $citi = $this->city_details->where('name', 'LIKE', "%{$city}%");
            // $cities = $citi->where('language_id',getLanguage()->id)->orderBy('id')->get();
            // if(Config::get('app.locale') == 'en') {
                // $cities = $cities->where('name', 'LIKE', "%{$city}%")->orderBy('name')->get();
            // }elseif(Config::get('app.locale') == 'ar') {
                // $cities = $cities->where('name_ar', 'LIKE', "%{$city}%")->orderBy('name')->get();
            // }
            $cities = $this->city->with('cityDetailsByLanguage')->orderBy('name')->get();
            $cities = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
            $cities = $cities->where(function($q1) use($request) {
                    $q1 = $q1->where('status', '!=', 'I');
                });
            $cities = $cities->whereHas('cityDetailsByLanguage',function($query) use($request) {
                $query->where('name', 'LIKE', "%{$request->city}%");
            })->get();

        }
        $response['result']['cities'] = $cities;
        return response()->json($response);     
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);

        return $this->registered($request, $user)
        ?: redirect($this->redirectPath());
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname'         => 'required|max:255',
            'lname'         => 'required|max:255',
            'phone'         => 'required|numeric',
            'email'         => 'required|email|max:255',
            'company_name'  => 'required',
            'address'       => 'required',
            // 'city'          => 'required',
            'state'         => 'required',
            'zipcode'       => 'required',
            'avenue'        => 'required',
            'building_number'=> 'required',
            'address_note'  => 'required',
            'payment_method'  => 'required',
            'country'       => 'required',
            // 'image'         => 'required',
            'password'      => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Merchant
     */
    protected function create(array $data)
    {
        if(@$data['image']) {
            $image     = $data['image']; 
            $imgName   = time().".".$image->getClientOriginalExtension();
            $image->move('storage/app/public/profile_pics', $imgName);
        } else {
            $imgName = '';
        }
        if(@$data['country'] == 134){
            if(@$data['kuwait_city']){
                $new['city']          = @$data['kuwait_city_value_name'];
                $new['city_id']       = @$data['kuwait_city_value_id'];
            }
        }else{
            if(@$data['city']){
                $new['city']          = @$data['city'];
                $new['city_id']       = 0;
            }    
        }
        $merchant =  Merchant::create([
            'fname'         => $data['fname'],
            'lname'         => $data['lname'],
            'email'         => $data['email'],
            'phone'         => $data['phone'],
            'company_name'  => $data['company_name'],
            'address'       => $data['address'],
            'city'          => $new['city'],
            'city_id'          => $new['city_id'],
            'state'         => $data['state'],
            'zipcode'       => $data['zipcode'],
            'avenue'        => $data['avenue'],
            'building_number'=> $data['building_number'],
            'address_note'  => $data['address_note'],
            'country'       => $data['country'],
            'payment_mode' => $data['payment_method'],
            'image'         => $imgName,
            'email_vcode'   => time(),
            'password'      => Hash::make($data['password']),
            'location'      => $data['location'],
            'lat'           => $data['lat'],
            'lng'           => $data['lng'],
        ]);

        //slug update
        $fname = str_slug($data['fname']);
        $lname = str_slug($data['lname']);
        $slug = $fname.'-'.$lname;
        $check_slug = Merchant::where('slug', $slug)->first();
        if(@$check_slug) {
            $slug = $slug.'-'.$merchant->id;
        }
        Merchant::whereId($merchant->id)->update(['slug' => $slug]);
        $merchant['mailBody'] = 'registered';
        $this->sendVerificationMail($merchant);
    }

    /**
    * Method: sendVerificationMail
    * Desc: This method is used for send verification mail to user
    */
    public function sendVerificationMail($reqData)  {        
        Mail::send(new VerificationMail($reqData));
    }

    /**
    * Method: success
    * Desc: This method is used for show success data after successfully registration
    */

    public function success() {
        return view('merchant.modules.extras.registration_success');
    }


    /**
    * Checking email exist or not
    */
    public function chkEmailExist(Request $request)
    {
        $response = [
            'jsonrpc' => '2.0'
        ];
        $merchant = Merchant::where(['email' => trim($request->params['email'])]);
        $merchant = $merchant->where(function($query) use($request){
            $query->where('status','!=','D');
        });
        $merchant = $merchant->first();
        if(@$merchant) {
            $response['error']['merchant'] = 'This email id already exists.';
        } else {
            $response['result']['status']  = false;
            $response['result']['message'] = 'This email is available.';
        }
        return response()->json($response);          
    }


    /**
    * Checking new email exist or not in edit profile
    */
    public function chkNewEmailExist(Request $request)
    {
        $response = [
            'jsonrpc' => '2.0'
        ];
        $merchant = Merchant::where(['email' => trim($request->params['email'])])->where('id', '!=', @Auth::guard('merchant')->user()->id)->first();
        if(@$merchant) {
            $response['error']['merchant'] = 'This email id already exists.';
            
        } else {
            $response['result']['status']  = false;
            $response['result']['message'] = 'This email is available.';
        }
        return response()->json($response);          
    }

    /*
    *method: verifyEmail
    *params: vcode, id
    *description: for verify user's mail id
    */
    public function verifyEmail($vcode, $id)
    {
        try {
            $merchant = Merchant::where('email_vcode', $vcode)->first();
            if(@$merchant->email_vcode != null && @$merchant->status == 'U') {
                $update['email_vcode'] = 'null';
                $update['status'] = 'AA';
                $update['is_email_verified'] = 'Y';
                Merchant::where('id', $merchant->id)->update($update);
                return view('merchant.modules.extras.mail_verification_success');
            } else {
                return view('merchant.modules.extras.link_expire');
            }
        }
        // If any exception arises a new log file will be generated i.e
        // if any product slug is not available and someone tries to give
        // false slug a new error view with error message will be shown.
        catch (\Exception $e) {
            $error = [
                'img'     => 'oops.png',
                'heading' => 'Sorry!',
                'message' => 'Something went to wrong.',
                'title'   => 'Sobrandoinggresso',
            ];
            return view('modules.errors.all_errors')->with($error);
        }
    }

    /*
    *method: verifyNewEmail
    *params: vcode, id
    *description: for verify user's mail id
    */
    public function verifyNewEmail($vcode,$id)
    {
        // try {
            // dd($vcode, $id);
        $user = Merchant::where('email_vcode', $vcode)->first();
            // dd($user);
        if(@$user->email_vcode != null) {
            $update['email_vcode'] = null;
            $update['email'] = $user->tmp_email;
            $update['tmp_email'] = null;
            Merchant::where('id', $user->id)->update($update);
            return view('merchant.modules.extras.mail_verification_success');
        }else{
            return view('merchant.modules.extras.link_expire');
        }
        // }
        // catch (\Exception $e) {

        //     $error = [
        //         'img'     => 'oops.png',
        //         'heading' => 'Sorry!',
        //         'message' => 'Something went to wrong.',
        //         'title'   => 'Sobrandoinggresso',
        //     ];
        //     return view('modules.errors.all_errors')->with($error);
        // }
    }
}
