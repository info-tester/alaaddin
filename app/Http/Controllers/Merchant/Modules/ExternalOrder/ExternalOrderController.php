<?php

namespace App\Http\Controllers\Merchant\Modules\ExternalOrder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\Customer;
use Validator;
use Auth;
use App\Models\City;
use App\Models\MerchantRegId;
use App\Models\Notification;
use App\Models\Language;
use App\Models\ShippingCost;
use App\Models\CitiDetails;
use App\Models\CountryDetails;
use App\Models\OrderMaster;
use App\Models\MerchantAddressBook;
use App\Models\ZoneMaster;
use App\Models\ZoneDetail;
use App\Models\ZoneRateDetail;
use App\Models\TimeSlot;
use App\Models\OrderStatusTiming;

use App\Driver;
use App\Merchant;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\ShippingCostRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\OrderImageRepository;
use App\Repositories\StateRepository;
use App\Repositories\CityRepository;
use App\Repositories\CitiDetailsRepository;
use App\Repositories\SettingRepository;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;
use App\Mail\SendMailExternalOrderdetails;
use Config;

class ExternalOrderController extends Controller
{
    protected $order_master,$order_details,$shipping_cost,$order_sellers;
    public function __construct(OrderMasterRepository $order_master,
        SettingRepository $settings,
        ShippingCostRepository $shipping_cost,
        OrderDetailRepository $order_details,
        OrderSellerRepository $order_sellers,
        MerchantRepository $merchants,
        OrderImageRepository $order_image,
        StateRepository $state,
        CityRepository $city,
        CitiDetailsRepository $city_details,
        Request $request)
    {
        $this->order_master             =   $order_master;
        $this->order_details            =   $order_details;
        $this->shipping_cost            =   $shipping_cost;
        $this->order_sellers            =   $order_sellers;
        $this->merchants                =   $merchants;
        $this->order_image              =   $order_image;
        $this->state                    =   $state;
        $this->city                     =   $city;
        $this->city_details             =   $city_details;
        $this->settings                 =   $settings;
    }

    private function setLanguage() {
        $lang = Language::where(['prefix' => Config::get('app.locale')])->first();
        $this->languageId = $lang->id;
    }

    private function getLanguageId() {
        return $this->languageId;
    }

    /*
    * Method: addExternalOrder
    * Description: This method is used to add External Order.
    * Author: Jayatri
    * Modified by: Argha
    * Date : 2020-09-21
    */
    public function addExternalOrder(Request $request){
        $countries = Country::with('countryDetailsBylanguage')->where('status','!=','D');
        $countries = $countries->where(function($q1) use($request) {
            $q1 = $q1->where('status', '!=', 'I');
        });
        $countries = $countries->get();
        $states = $this->state->where('country_id',134)->get();
        $cities = $this->city->with('cityDetailsByLanguage')->get();
        if($request->slug && $request->token){
            $merchant = Merchant::with(['Country','merchantCityDetails'])->where('slug',$request->slug)->first();
        } else {
            $merchant = Merchant::with(['Country','merchantCityDetails'])->where('id',Auth::guard('merchant')->user()->id)->first();
        }
        
        $timeslot = TimeSlot::get();
        $addressBook = MerchantAddressBook::with([
            'countryDetailsBylanguage',
            'getCityNameByLanguage'
        ])
        ->where('merchant_id', $merchant->id)
        ->get();

        $defaultAddress = MerchantAddressBook::with([
            'countryDetailsBylanguage',
            'getCityNameByLanguage'
        ])
        ->where('merchant_id', $merchant->id)
        ->where('is_default','Y')
        ->first();
        // try{
        if(@$request->all()){
            // dd($merchant);
            if($request->slug && $request->token){
                $from_country = $merchant->country;
                $to_country   = $request->country;
            }else{
                if($request->from_address == 'T'){
                    $merchant_address1 = MerchantAddressBook::where('id',$request->from_address_id)->first();
                    
                    if($merchant_address1 == null){
                        session()->flash("error",__('errors.-5092'));
                        return redirect()->back();
                    }
                    $from_country = $merchant_address1->country;
                }else{
                    $from_country = $request->pickup_country;
                }
        
                if($request->to_address == 'T'){
                    $merchant_address2 = MerchantAddressBook::where('id',$request->to_address_id)->first();
                    if($merchant_address2 == null){
                        session()->flash("error",__('errors.-5092'));
                        return redirect()->back();
                    }
                    $to_country = $merchant_address2->country;
                }else{
                    $to_country = $request->country;
                }
            }
            if(@$from_country != 134 && $to_country != 134){
                session()->flash("error",__('errors.-5050'));
                return redirect()->back();
            }
            else{
                $new['fname']                   = @$request->fname;
                $new['shipping_fname']          = @$request->fname;
                $new['lname']                   = @$request->lname;
                $new['shipping_lname']          = @$request->lname;
                $new['email']                   = @$request->email;
                $new['shipping_phone']          = @$request->phone;
                $new['payment_method']          = @$request->payment_method;
                $new['billing_phone']           = @$request->phone;
                
                if($request->slug && $request->token){
                    $new['shipping_country']        = @$request->country;
                    $new['shipping_address']        = @$request->address;
                    $new['payment_method']          = @$request->payment_method;
                    $new['shipping_street']         = @$request->street;
                    $new['shipping_block']          = @$request->block;
                    $new['shipping_building']       = @$request->building;
                    $new['location']                = @$request->location;
                    $new['lat']                     = @$request->lat;
                    $new['lng']                     = @$request->lng;
                    $new['billing_country']         = @$request->country;
                    $new['billing_phone']           = @$request->phone;
                    $new['external_order_type']     = 'L'; // link

                    # for default pickup address
                    $new['pickup_country']      = $merchant->country;
                    $new['pickup_city']         = $merchant->city;
                    $new['pickup_city_id']      = $merchant->city_id;
                    $new['pickup_street']       = $merchant->address;
                    $new['pickup_building']     = $merchant->building_number;
                    $new['pickup_zip']          = $merchant->zipcode;
                    $new['pickup_block']        = $merchant->block;
                    $new['pickup_location']     = $merchant->location;
                    $new['pickup_address']      = $merchant->more_address;
                    $new['pickup_lat']          = $merchant->lat;
                    $new['pickup_lng']          = $merchant->lng;
                    $new['pickup_phone']        = @$merchant->pickup_phone;
                    // dd($merchant, $new);
                    if(@$request->delivery_date){
                        $new['preferred_delivery_date'] = @$request->delivery_date;
                    } else {
                        $todayDate = date("Y-m-d");
                        $new['preferred_delivery_date'] = $todayDate;
                    }
                    
                    if(@$request->from_time) {
                        $new['preferred_delivery_time'] = @$request->from_time; 
                    }
                    if(@$request->country) {
                        if($request->country == 134) {
                            $new['shipping_block']       = @$request->block;
                            $new['shipping_building']    = @$request->building;
                            
                            if(@$request->kuwait_city){
                                $name_city = $this->city_details->where('name',$request->kuwait_city)->first();
                                if(@$name_city){
                                    $new['shipping_city']      = @$name_city->name;
                                    $new['shipping_city_id']   = @$name_city->city_id; 
                                    
                                    $shipping_city_id = @$name_city->city_id;
                                } else {
                                    session()->flash("error",__('errors.-5038'));
                                    return redirect()->back();
                                }
                            }
                        } else {
                            $new['shipping_city'] = @$request->city;
                        }
                    }
                } else {
                    if($request->from_address == 'T') {
                        $new['pickup_country']      = @$merchant_address1->country;
                        $new['pickup_city']         = @$merchant_address1->city;
                        $new['pickup_city_id']      = @$merchant_address1->city_id;
                        $new['pickup_street']       = @$merchant_address1->street;
                        $new['pickup_building']     = @$merchant_address1->building_no;
                        $new['pickup_zip']          = @$merchant_address1->postal_code;
                        $new['pickup_block']        = @$merchant_address1->block;
                        $new['pickup_location']     = @$merchant_address1->location;
                        $new['pickup_address']      = @$merchant_address1->more_address;
                        $new['pickup_lat']          = @$merchant_address1->lat;
                        $new['pickup_lng']          = @$merchant_address1->lng;
                        $new['pickup_phone']        = @$merchant_address1->phone;
                    } else {
                        $new['pickup_country']      = @$request->pickup_country;
                        $new['pickup_street']       = @$request->pickup_street;
                        $new['pickup_building']     = @$request->pickup_building;
                        $new['pickup_zip']          = @$request->pickup_zip;
                        $new['pickup_block']        = @$request->pickup_block;
                        $new['pickup_location']     = @$request->pickup_location;
                        $new['pickup_address']      = @$request->pickup_address;
                        $new['pickup_lat']          = @$request->lat1;
                        $new['pickup_lng']          = @$request->lng1;
                        $new['pickup_phone']        = @$request->pickup_phone;
                        if(@$request->pickup_kuwait_city){
                            $city_name = $this->city_details->where('name',$request->pickup_kuwait_city)->first();
                            if($city_name != null){
                                $new['pickup_city']         = @$city_name->name;
                                $new['pickup_city_id']      = @$city_name->city_id;
                                $shipping_city_id = @$city_name->city_id;
                            } else {
                                session()->flash("error",__('errors.-5038'));
                                return redirect()->back();
                            }
                        } else {
                            $new['pickup_city']         = @$request->pickup_city;
                        }                   
                    }
                    if($request->to_address == 'T'){
                        $new['shipping_country']        = @$merchant_address2->country;
                        $new['shipping_address']        = @$merchant_address2->more_address;
                        $new['shipping_street']         = @$merchant_address2->street;
                        $new['shipping_block']          = @$merchant_address2->block;
                        $new['shipping_building']       = @$merchant_address2->building_no;
                        $new['shipping_zip']            = @$merchant_address2->postal_code;
                        $new['shipping_city']           = @$merchant_address2->city;
                        $new['shipping_city_id']        = @$merchant_address2->city_id;
                        $new['location']                = @$merchant_address2->location;
                        $new['lat']                     = @$merchant_address2->lat;
                        $new['lng']                     = @$merchant_address2->lng;
                        $new['billing_country']         = @$merchant_address2->country;

                        $shipping_city_id = @$merchant_address2->city_id;
                    } else {
                        $new['shipping_country']        = @$request->country;
                        $new['shipping_address']        = @$request->address;
                        $new['shipping_street']         = @$request->street;
                        $new['shipping_block']          = @$request->block;
                        $new['shipping_building']       = @$request->building;
                        $new['shipping_zip']            = @$request->zip;
                        $new['location']                = @$request->location;
                        $new['lat']                     = @$request->lat;
                        $new['lng']                     = @$request->lng;
                        $new['billing_country']         = @$request->country;
                        if(@$request->kuwait_city){
                            $city_name = $this->city_details->where('name',$request->kuwait_city)->first();
                            if($city_name != null){
                                $new['shipping_city']         = @$city_name->name;
                                $new['shipping_city_id']      = @$city_name->city_id;
                                
                                $shipping_city_id = @$city_name->city_id;
                            } else {
                                session()->flash("error",__('errors.-5038'));
                                return redirect()->back();
                            }
                        } else {
                            $new['shipping_city']         = @$request->city;
                        }
                    }
                    
                    if(@$request->delivery_date){
                        $new['delivery_date']         = @$request->delivery_date;
                    } else {
                        $todayDate = date("Y-m-d");
                        $new['delivery_date']         = $todayDate;
                    }
                    if(@$request->from_time){
                       $new['delivery_time']          = date('H:i:s', strtotime(@$request->from_time)); 
                    }
                }
                
                if(@$request->product_weight){
                    $new['product_total_weight']    = @$request->product_weight;
                }else{
                    $new['product_total_weight']    = 0.000;
                }

                // if both are belongs from kuwait
                if(@$from_country == 134 && @$to_country == 134){
                    $shipping_city = $this->city->where('id',@$shipping_city_id)->first();
                    if($merchant->applicable_city_delivery_external == 'Y'){
                        if($shipping_city->external_delivery_fee > $merchant->shipping_cost){
                            $shippingcost = $shipping_city->external_delivery_fee;
                        }else{
                            $shippingcost = $merchant->shipping_cost;
                        }
                    }else{
                        $shippingcost = $merchant->shipping_cost;
                    }
                    $subtotal  = $request->order_total - $shippingcost;
                    // if($subtotal < 0){
                    //     $subtotal = 0;
                    // }else{
                    //     $subtotal = $subtotal;
                    // }
                    $orderMerchant['shipping_price']    = $shippingcost;
                    $new['shipping_price']              = $shippingcost;
                    $new['subtotal']                    = @$subtotal;
                    $order_total = @$subtotal + $shippingcost;
                    
                    $new['order_total']                 = $order_total;
                    $new['shipping_address']            = @$request->address;
                } else {
                    # if product weight not optional
                    if(@$request->product_weight) {
                        if($request->slug && $request->token){
                            $zoneDetail = ZoneDetail::where('country_id', @$request->country)->first();
                        }else{
                            if($to_country == 134){
                                $zoneDetail = ZoneDetail::where('country_id', @$from_country)->first();
                            }else{
                                $zoneDetail = ZoneDetail::where('country_id', @$to_country)->first();
                            }
                        }
                        
                        if($zoneDetail == null){
                            session()->flash("error",__('errors.-5090'));
                            return redirect()->back();
                        }
                        $chk_zone_rate = ZoneRateDetail::where('zone_id',@$zoneDetail->zone_master_id)->first();
                        if($chk_zone_rate == null){
                            session()->flash("error",__('errors.-5091'));
                            return redirect()->back();
                        }
                        $chk_range = ZoneRateDetail::where('zone_id',@$zoneDetail->zone_master_id);

                        $infinityChk = ZoneRateDetail::where('infinity_weight', 'Y')
                        ->where('zone_id',$zoneDetail->zone_master_id)
                        ->first();
                        if($infinityChk && @$request->product_weight >= $infinityChk->from_weight) {
                            $chk_range = $infinityChk;
                            $orderMerchant['shipping_price'] = $chk_range->external_outside_kuwait;
                            $new['shipping_price']           = $chk_range->external_outside_kuwait;
                            $subtotal  = $request->order_total - $chk_range->external_outside_kuwait;
                            // if($subtotal < 0){
                            //     $subtotal = 0;
                            // }else{
                            //     $subtotal = $subtotal;
                            // }
                            $new['subtotal']                = @$subtotal;
                            $new['order_total']             = @$subtotal+@$chk_range->external_outside_kuwait;
                        } else {
                            $chk_range = $chk_range->where(function($where) use ($request) {
                                $where->where(function($where1) use ($request) {
                                    $where1->where('from_weight', '<=', @$request->product_weight)
                                    ->where('to_weight', '>', @$request->product_weight);
                                })
                                ->orWhere(function($where2) use ($request) {
                                    $where2->where('from_weight', '<', @$request->product_weight)
                                    ->where('to_weight', '>=', @$request->product_weight);
                                });
                            })->first();
                            
                            if($chk_range) {
                                $orderMerchant['shipping_price'] = $chk_range->external_outside_kuwait;
                                $new['shipping_price']           = $chk_range->external_outside_kuwait;
                                $subtotal  = $request->order_total - $chk_range->external_outside_kuwait;
                                // if($subtotal < 0){
                                //     $subtotal = 0;
                                // }else{
                                //     $subtotal = $subtotal;
                                // }
                                $new['subtotal']                = @$subtotal;
                                $new['order_total']             = @$subtotal+@$chk_range->external_outside_kuwait;
                            } else {
                                session()->flash("error",__('errors.-5091'));
                                return redirect()->back();
                            }
                        }
                    } else {
                        $new['subtotal'] = @$request->order_total;   
                        $new['order_total'] = @$request->order_total; 
                        $new['shipping_price'] = 0.000;
                    }
                }

                // shipping address
                if($merchant->driver_id != 0){
                    $new['status']              = 'DA'; //initialize status 
                } else {
                    $new['status']              = 'N'; //initialize status 
                }
                $new['order_type']              = 'E'; //external order
                $code = time();
                $chk = $this->order_master->where('order_no',$code)->first();
                if(@$chk){ //check order no is duplicate or not
                    $code = "ORD".time();
                    $new['order_no']            = $code;
                }else{
                    $new['order_no']            = $code;
                }
                if(@$request->invoice){
                    $new['invoice_no']          = @$request->invoice;
                }
                if(@$request->invoice_details){
                    $new['invoice_details']     = @$request->invoice_details;
                }
                $new['creation_type']           = 'C';
                $new['last_status_date'] = now();
                //insert into order master table
                $order = $this->order_master->create($new);
                //update order_no field of order master table
                
                $ordId = sprintf('%08d', $order->id);
                $updn['order_no'] = $orderNumber = "ORDASW".$ordId;
                $this->order_master->where('id',$order->id)->update($updn);
                
                if(@$request->image){
                    foreach($request->image as $key=>$file){
                        $i = $key;
                        $name = $i.time()."_".$file->getClientOriginalName();
                        $path = 'storage/app/public/merchant/external_order/photo/';
                        $file->move($path,$name);
                        $order_img['order_id'] = $order->id;
                        $order_img['order_no'] = $order->order_no;
                        $order_img['images'] = $name;
                        $this->order_image->create($order_img);
                    }
                }
                
                //seller id
                $orderMerchant['seller_id'] = $merchant->id;
                $orderMerchant['order_master_id'] = $order->id;
                $orderMerchant['shipping_price'] = $order->shipping_price;
                if(@$order->product_total_weight) {
                    $orderMerchant['total_weight'] = @$order->product_total_weight;
                } else {
                    $orderMerchant['total_weight'] = 0.000;
                }
                
                $orderMerchant['subtotal'] = $order->subtotal;
                $orderMerchant['order_total'] = $order->order_total;
                $orderMerchant['input_amount']    = @$request->order_total;
                // shipping address
                if($merchant->driver_id != 0){
                    $orderMerchant['status'] = 'DA'; //initialize status 
                    $orderMerchant['driver_id'] = @$merchant->driver_id;
                }else{
                    $orderMerchant['status'] = 'N'; //initialize status 
                }
                $ordersellers = $this->order_sellers->create($orderMerchant);
                if(@$order){
                    $n['seller_id'] = $merchant->id;
                    $n['order_master_id'] = $order->id;
                    if(@$request->product_weight) {
                        $n['weight'] = @$request->product_weight;
                    } else {
                        $n['weight'] = 0.000;
                    }
                    $n['sub_total'] = $order->subtotal;
                    $n['original_price'] = $order->subtotal;
                    $n['total'] = $order->order_total;
                    $n['quantity'] = 1;
                    if(@$merchant->driver_id != 0){
                        $n['status'] = 'DA';
                        $n['driver_id'] = @$merchant->driver_id;
                    } else {
                        $n['status'] = 'N';
                    }
                    $orderDetails = $this->order_details->create($n);

                    $this->insertOrderStatusTimingOnCreateExternal($order->id);
                    // $withdraws = $this->withdraw->where('seller_id',Auth::guard('merchant')->user()->id)->get();
                    // $earnings = $this->order_details->where('seller_id',Auth::guard('merchant')->user()->id)->sum('total');
                    // $commission = $earnings * (Auth::guard('merchant')->user()->commission/100);
                    // $paid = 0;
                    // $due = $commission - $paid;

                    $total_earning = $this->order_details->where('seller_id',$merchant->id)->sum('sub_total');
                    #notify merchant
                    // $this->sendNotificationDashboardUpdate($order);
                    //update at the time of delivery
                    // $update['total_earning'] = $total_earning;

                    // $update['total_commission'] = ($total_earning * (Auth::guard('merchant')->user()->commission/100));
                    // $update['total_due'] = ($total_earning * (Auth::guard('merchant')->user()->commission/100)) - (Auth::guard('merchant')->user()->total_paid);
                    // $this->merchants->where('id',Auth::guard('merchant')->user()->id)->update($update);

                    // dd($order);
                    $country_nm_txt = CountryDetails::where(['country_id'=>@$order->shipping_country,'language_id'=>1])->first();
                    $orderMasterDetails                         =   new \stdClass();
                    $orderMasterDetails->order_no               =   $updn['order_no'];
                    $orderMasterDetails->order_type             =   @$order->order_type;
                    $orderMasterDetails->payment_method         =   @$order->payment_method;
                    $orderMasterDetails->order_total            =   @$order->order_total;
                    $orderMasterDetails->product_total_weight   =   @$n['weight'];
                    $orderMasterDetails->delivery_date          =   @$order->delivery_date;
                    $orderMasterDetails->delivery_time          =   @$order->delivery_time;
                    $orderMasterDetails->shipping_fname         =   @$order->shipping_fname ;
                    $orderMasterDetails->shipping_lname         =   @$order->shipping_lname ;
                    $orderMasterDetails->shipping_email         =   @$order->shipping_email ;
                    $orderMasterDetails->shipping_phone         =   @$order->shipping_phone ;
                    $orderMasterDetails->shipping_country       =   @$country_nm_txt->name ;
                    $orderMasterDetails->shipping_city          =   @$order->shipping_city ;
                    $orderMasterDetails->shipping_state         =   @$order->shipping_state ;
                    $orderMasterDetails->shipping_street        =   @$order->shipping_street ;
                    $orderMasterDetails->shipping_block         =   @$order->shipping_block;
                    $orderMasterDetails->shipping_avenue        =   @$order->shipping_avenue;
                    $orderMasterDetails->shipping_building      =   @$order->shipping_building;
                    $orderMasterDetails->shipping_zip           =   @$order->shipping_zip;
                    $orderMasterDetails->shipping_address       =   @$order->shipping_address;
                    $orderMasterDetails->shipping_price         =   @$order->shipping_price ;
                    $orderMasterDetails->invoice_no             =   @$order->invoice_no ;
                    $orderMasterDetails->invoice_details        =   @$order->invoice_details ;
                    $orderMasterDetails->subtotal               =   @$order->subtotal ;
                    $orderMasterDetails->status                 =   @$order->status ;
                    $orderMasterDetails->created_at             =   @$order->created_at ;
                    $orderMasterDetails->updated_at             =   @$order->updated_at ;
                    $orderMasterDetails->email                  =   @$merchant->email;
                    $orderMasterDetails->email1                 =   @$merchant->email1;
                    if(@$order->status == 'N'){
                        $orderMasterDetails->subject     =   "New External order details!";
                    }else{
                        $orderMasterDetails->subject     =   "Order is accepted! External order details!";
                    }
                    
                    $data['orderMasterDetails'] = $orderMasterDetails;
                    $setting = $this->settings->where('id','!=',0)->first();
                    $count = 0;
                    if(@$order->status == 'DA'){
                        if(@$merchant->email1){
                            $data['bcc'] [$count] = $merchant->email1;
                            $count++; 
                        }
                        if(@$merchant->email2){
                            $data['bcc'] [$count] = $merchant->email2;
                            $count++; 
                        }
                        if(@$merchant->email3){
                            $data['bcc'] [$count] = $merchant->email3; 
                            $count++;
                        }
                    }
                    if(@$order->status == 'N'){
                        if(@$merchant->email1){
                            $data['bcc'] [$count] = $merchant->email1; 
                            $count++;
                        }
                        if(@$merchant->email2){
                            $data['bcc'] [$count] = $merchant->email2; 
                            $count++;
                        }
                        if(@$merchant->email3){
                            $data['bcc'] [$count] = $merchant->email3; 
                            $count++;
                        }
                    }
                     //dd($data);
                    // if(@Auth::guard('merchant')->user()->driver_id != 0){
                        Mail::send(new SendMailExternalOrderdetails($data));    
                    // }
                    if(@$orderDetails){
                        $this->sendNotification($order->order_no);
                        session()->flash("success",__('success.-4042'));
                    }else{
                        session()->flash("error",__('errors.-5038'));
                    }
                }
                if($request->slug && $request->token){
                    return redirect()->route('merchant.show.anonymous.external.order', ['slug' =>$merchant->slug,'token' => md5($merchant->id),'id' => $order->id]);
                } else {
                    return redirect()->back();
                }
                
            }
        }//end of if request
        // }
        // catch(\Exception $e){
            // return redirect()->back()->withError(['message' => $e->getMessage(), 'meaning' => $e->getMessage()]);
        // }
        if($request->slug && $request->token) {
            return view('merchant.modules.external_order.add_external_order_anonymus')->with([
                'countries' => @$countries,
                'states'    => @$states,
                'cities'    => @$cities,
                'address'   => @$addressBook,
                'merchant'  => $merchant,
                'timeslot'  => $timeslot
            ]);
        } else {
            return view('merchant.modules.external_order.external_order')->with([
                'countries' => @$countries,
                'states'    => @$states,
                'cities'    =>@$cities,
                'address'   =>@$addressBook,
                'default_address' => $defaultAddress
            ]);
        }
    }

    /**
    * Method: insertOrderStatusTimingOnCreateExternal
    * Description: This method is used to insert time in to order status timing table
    * Author: Sanjoy
    */
    private function insertOrderStatusTimingOnCreateExternal($orderId) {
        $order = OrderMaster::find($orderId);
        OrderStatusTiming::create([
            'order_id'      => $order->id,
            'status'        => 'N',
            'start_time'    => date('Y-m-d H:i:s'),
            'duration_in_minutes' => 0
        ]);
        if($order->status == 'DA') {
            OrderStatusTiming::create([
                'order_id'      => $order->id,
                'status'        => 'DA',
                'start_time'    => date('Y-m-d H:i:s'),
                'duration_in_minutes' => 0
            ]);
        }
    }

    /*
    * Method: checkRangeExist
    * Description: This method is used to check range of shipping cost related to weight exist or not.
    * Author: Jayatri
    */
    public function checkRangeExist(Request $request){
        $this->checkLoginAccess();
        $this->middleware('merchant.auth:merchant');
        $country = @Auth::guard('merchant')->user()->country;

        if(@$request->country == 134 && $country == 134){
            return 1;
        }else{
            if($request->country != 134 || $country != 134){

                $chk_range = $this->shipping_cost->where('id','!=',0);
                $chk_range = $chk_range->where(function($where) use ($request){
                    $where->where(function($where1) use ($request) {
                    // $frm_tm = @$request->from_weight;
                    // $to_tm = @$request->to_weight;

                        $where1->where('from_weight', '<=', @$request->product_weight)
                        ->where('to_weight', '>', @$request->product_weight);

                    })
                    ->orWhere(function($where2) use ($request) {

                        $where2->where('from_weight', '<', @$request->product_weight)
                        ->where('to_weight', '>=', @$request->product_weight);
                    });

                })->first();
                if(@$chk_range){
                    return 1;
                }else{
                    return 0;
                }
            }
        }
    }
    /*
    * Method: checkRangeExist
    * Description: This method is used to check range of shipping cost related to weight exist or not.
    * Author: Jayatri
    */
    public function editOrderDetails($id,Request $request){
        $this->checkLoginAccess();
        $this->middleware('merchant.auth:merchant');
        $order = $this->order_master->with(['orderMasterDetails','getOrderAllImages'])->where('id',$id)->first();
        $order_image = $this->order_image->where('order_id',$id)->get();
        $countries = Country::with('countryDetailsBylanguage')->where('status','!=','D')->get();
        $states = $this->state->where('country_id',134)->get();
        $cities = $this->city->get();
        $country = @Auth::guard('merchant')->user()->country;
        $timeslot = TimeSlot::get();
        $addressBook = MerchantAddressBook::with(['countryDetailsBylanguage','getCityNameByLanguage'])->where('merchant_id',Auth::guard('merchant')->user()->id)->get();
        $defaultAddress = MerchantAddressBook::with(['countryDetailsBylanguage','getCityNameByLanguage'])->where('merchant_id',Auth::guard('merchant')->user()->id)->where('is_default','Y')->first();
        if(@$order){
            if(@$request->all()){
                // dd($request->all());
                if(@$request->country == 134){
                    $request->validate([
                        'phone'=>'required',
                        'street'=>'required',
                        'country'=>'required',
                        'order_total'=>'required'
                    ],[
                        'phone'=>'Phone number cannot be left blank[Ex: 75656756756]!',
                        'street'=>'Street cannot be left blank!',
                        'country'=>'Country name cannot be left blank!',
                        'order_total'=>'Total cost cannot be left blank [Ex: 185.5465]!'
                    ]);
                }else{
                    $request->validate([
                        'fname'=>'required',
                        //'lname'=>'required',
                        'phone'=>'required',
                        'street'=>'required',
                        'country'=>'required',
                        'order_total'=>'required'
                    ],[
                        'fname'=>'First name cannot be left blank!',
                        //'lname'=>'Last name cannot be left blank!',
                        'phone'=>'Phone number cannot be left blank!',
                        'street'=>'Street cannot be left blank!',
                        'country'=>'Country name cannot be left blank!',
                        'order_total'=>'Price cannot be left blank!'
                    ]);    
                }
                
                try{

                    if($request->from_address == 'T'){
                        $merchant_address1 = MerchantAddressBook::where('id',$request->from_address_id)->first();
                        if($merchant_address1 == null){
                            session()->flash("error",__('errors.-5092'));
                            return redirect()->back();
                        }
                        $from_country = $merchant_address1->country;
                    }else{
                        $from_country = $request->pickup_country;
                    }
            
                    if($request->to_address == 'T'){
                        $merchant_address2 = MerchantAddressBook::where('id',$request->to_address_id)->first();
                        if($merchant_address2 == null){
                            session()->flash("error",__('errors.-5092'));
                            return redirect()->back();
                        }
                        $to_country = $merchant_address2->country;
                    }else{
                        $to_country = $request->country;
                    }
                    if($from_country != 134 && $to_country != 134){
                        session()->flash("error",__('errors.-5050'));
                        return redirect()->back();
                    } 
                    else{

                        $new['fname']                   = @$request->fname;
                        $new['shipping_fname']          = @$request->fname;
                        $new['lname']                   = @$request->lname;
                        $new['shipping_lname']          = @$request->lname;
                        $new['email']                   = @$request->email;
                        $new['shipping_phone']          = @$request->phone;
                        $new['payment_method']          = @$request->payment_method;
                        $new['billing_phone']           = @$request->phone;
                        if($order->external_order_type == 'L'){
                            $new['shipping_country']        = @$request->country;
                            $new['shipping_address']        = @$request->address;
                            $new['shipping_street']         = @$request->street;
                            $new['shipping_block']          = @$request->block;
                            $new['shipping_building']       = @$request->building;
                            $new['location']                = @$request->location;
                            $new['lat']                     = @$request->lat;
                            $new['lng']                     = @$request->lng;
                            $new['billing_country']         = @$request->country;
                            $new['billing_phone']           = @$request->phone;

                            if(@$request->delivery_date){
                                    $new['preferred_delivery_date']       = @$request->delivery_date;
                            }else{
                                $todayDate = date("Y-m-d");
                                $new['preferred_delivery_date']           = $todayDate;
                            }
                            
                            if(@$request->from_time){
                                $new['preferred_delivery_time']           = @$request->from_time; 
                            }
                            if(@$request->country){
                                if($request->country == 134){

                                    $new['shipping_block']       =   @$request->block;
                                    $new['shipping_building']    =   @$request->building;
                                    
                                    if(@$request->kuwait_city){

                                        $name_city = $this->city_details->where('name',$request->kuwait_city)->first();
                                        if(@$name_city){
                                            $new['shipping_city']      = @$name_city->name;
                                            $new['shipping_city_id']   = @$name_city->city_id;   
                                            
                                            $shipping_city_id = @$name_city->city_id;
                                        }else{
                                            session()->flash("error",__('errors.-5038'));
                                            return redirect()->back();
                                        }
                                    }
                                }else{
                                    $new['shipping_city']           = @$request->city;
                                }
                            }
                        }else{
                            if($request->from_address == 'T'){
                                $new['pickup_country']      = @$merchant_address1->country;
                                $new['pickup_city']         = @$merchant_address1->city;
                                $new['pickup_city_id']      = @$merchant_address1->city_id;
                                $new['pickup_street']       = @$merchant_address1->street;
                                $new['pickup_building']     = @$merchant_address1->building_no;
                                $new['pickup_zip']          = @$merchant_address1->postal_code;
                                $new['pickup_block']        = @$merchant_address1->block;
                                $new['pickup_location']     = @$merchant_address1->location;
                                $new['pickup_address']      = @$merchant_address1->more_address;
                                $new['pickup_lat']          = @$merchant_address1->lat;
                                $new['pickup_lng']          = @$merchant_address1->lng;
                                $new['pickup_phone']        = @$merchant_address1->phone;
                                
                            }else{
                                $new['pickup_country']      = @$request->pickup_country;
                                $new['pickup_street']       = @$request->pickup_street;
                                $new['pickup_building']     = @$request->pickup_building;
                                $new['pickup_zip']          = @$request->pickup_zip;
                                $new['pickup_block']        = @$request->pickup_block;
                                $new['pickup_location']     = @$request->pickup_location;
                                $new['pickup_address']      = @$request->pickup_address;
                                $new['pickup_lat']          = @$request->lat1;
                                $new['pickup_lng']          = @$request->lng1;
                                $new['pickup_phone']        = @$request->pickup_phone;
                                if(@$request->pickup_kuwait_city){
                                    $city_name = $this->city_details->where('name',$request->pickup_kuwait_city)->first();
                                    
                                    if($city_name != null){
                                        $new['pickup_city']         = @$city_name->name;
                                        $new['pickup_city_id']      = @$city_name->city_id;
                                    }else{
                                        session()->flash("error",__('errors.-5038'));
                                        return redirect()->back();
                                    }
                                    
                                }else{
                                    $new['pickup_city']         = @$request->pickup_city;
                                }
                                
                            }
            
                            if($request->to_address == 'T'){
                                $new['shipping_country']        = @$merchant_address2->country;
                                $new['shipping_address']        = @$merchant_address2->more_address;
                                $new['shipping_street']         = @$merchant_address2->street;
                                $new['shipping_block']          = @$merchant_address2->block;
                                $new['shipping_building']       = @$merchant_address2->building_no;
                                $new['shipping_zip']            = @$merchant_address2->postal_code;
                                $new['shipping_city']           = @$merchant_address2->city;
                                $new['shipping_city_id']        = @$merchant_address2->city_id;
                                $new['location']                = @$merchant_address2->location;
                                $new['lat']                     = @$merchant_address2->lat;
                                $new['lng']                     = @$merchant_address2->lng;
                                $new['billing_country']         = @$merchant_address2->country;

                                $shipping_city_id = @$merchant_address2->city_id;
                            }else{
                                $new['shipping_country']        = @$request->country;
                                $new['shipping_address']        = @$request->address;
                                $new['shipping_street']         = @$request->street;
                                $new['shipping_block']          = @$request->block;
                                $new['shipping_building']       = @$request->building;
                                $new['shipping_zip']            = @$request->zip;
                                $new['location']                = @$request->location;
                                $new['lat']                     = @$request->lat;
                                $new['lng']                     = @$request->lng;
                                $new['billing_country']         = @$request->country;
                                if(@$request->kuwait_city){
                                    $city_name = $this->city_details->where('name',$request->kuwait_city)->first();
                                    //dd($city_name);
                                    if($city_name != null){
                                        $new['shipping_city']         = @$city_name->name;
                                        $new['shipping_city_id']      = @$city_name->city_id;

                                        $shipping_city_id = @$city_name->city_id;
                                    }else{
                                        session()->flash("error",__('errors.-5038'));
                                        return redirect()->back();
                                    }
                                }else{
                                    $new['shipping_city']         = @$request->city;
                                }
                            }
    
                            
    
                            if(@$request->delivery_date){
                                $new['delivery_date']           = @$request->delivery_date;
                            }
                            else{
                                $todayDate = date("Y-m-d");
                                $new['delivery_date']           = $todayDate;
                            }
                            
                            if(@$request->from_time){
                                $new['delivery_time']           = date('H:i:s', strtotime(@$request->from_time)); 
                            }
                        }
                        
                        
                        if(@$request->product_weight){
                            $new['product_total_weight']    = @$request->product_weight;
                        }else{
                            $new['product_total_weight']    = 0.000;
                        }
                        if(@$request->country == 134 && $country == 134){
                            
                            $merchant = Auth::guard('merchant')->user();
                            $shipping_city = $this->city->where('id',@$shipping_city_id)->first();
                            if($merchant->applicable_city_delivery_external == 'Y'){
                                if($shipping_city->external_delivery_fee > $merchant->shipping_cost){
                                    $shippingcost = $shipping_city->external_delivery_fee;
                                }else{
                                    $shippingcost = $merchant->shipping_cost;
                                }
                            }else{
                                $shippingcost = $merchant->shipping_cost;
                            }
                            $subtotal  = $request->order_total - $shippingcost;
                            // if($subtotal < 0){
                            //     $subtotal = 0;
                            // }else{
                            //     $subtotal = $subtotal;
                            // }
                            $orderMerchant['shipping_price']    = $shippingcost;
                            $new['shipping_price']              = $shippingcost;
                            $new['subtotal']                    = @$subtotal;
                            $order_total = @$subtotal + $shippingcost;
                            
                            $new['order_total']                 = $order_total;
                            $new['shipping_address']            = @$request->address;
                            
                            
                        }else{

                            if(@$request->product_weight) {
                                if($order->external_order_type == 'L'){
                                    $zoneDetail = ZoneDetail::where('country_id', @$request->country)->first();
                                }else{
                                    if($to_country == 134){
                                        $zoneDetail = ZoneDetail::where('country_id', @$from_country)->first();
                                    }else{
                                        $zoneDetail = ZoneDetail::where('country_id', @$to_country)->first();
                                    }
                                }
                                
                                if($zoneDetail == null){
                                    session()->flash("error",__('errors.-5090'));
                                    return redirect()->back();
                                }
                                $chk_zone_rate = ZoneRateDetail::where('zone_id',@$zoneDetail->zone_master_id)->first();
                                if($chk_zone_rate == null){
                                    session()->flash("error",__('errors.-5091'));
                                    return redirect()->back();
                                }
                                $chk_range = ZoneRateDetail::where('zone_id',@$zoneDetail->zone_master_id);
        
                                $infinityChk = ZoneRateDetail::where('infinity_weight', 'Y')
                                                             ->where('zone_id',$zoneDetail->zone_master_id)
                                                             ->first();
                                if($infinityChk && @$request->product_weight >= $infinityChk->from_weight) {
                                    $chk_range = $infinityChk;
                                    $orderMerchant['shipping_price'] = $chk_range->external_outside_kuwait;
                                    $new['shipping_price']           = $chk_range->external_outside_kuwait;
                                    $subtotal  = $request->order_total - $chk_range->external_outside_kuwait;
                                    // if($subtotal < 0){
                                    //     $subtotal = 0;
                                    // }else{
                                    //     $subtotal = $subtotal;
                                    // }
                                    $new['subtotal']                = @$subtotal;
                                    $new['order_total']             = @$subtotal+@$chk_range->external_outside_kuwait;
                                } else {
                                    $chk_range = $chk_range->where(function($where) use ($request) {
                                        $where->where(function($where1) use ($request) {
                                            $where1->where('from_weight', '<=', @$request->product_weight)
                                            ->where('to_weight', '>', @$request->product_weight);
                                        })
                                        ->orWhere(function($where2) use ($request) {
                                            $where2->where('from_weight', '<', @$request->product_weight)
                                            ->where('to_weight', '>=', @$request->product_weight);
                                        });
                                    })->first();
                                    
                                    if($chk_range) {
                                        $orderMerchant['shipping_price'] = $chk_range->external_outside_kuwait;
                                        $new['shipping_price']           = $chk_range->external_outside_kuwait;
                                        $subtotal  = $request->order_total - $chk_range->external_outside_kuwait;
                                        // if($subtotal < 0){
                                        //     $subtotal = 0;
                                        // }else{
                                        //     $subtotal = $subtotal;
                                        // }
                                        $new['subtotal']                = @$subtotal;
                                        $new['order_total']             = @$subtotal+@$chk_range->external_outside_kuwait;
                                    } else {
                                        session()->flash("error",__('errors.-5091'));
                                        return redirect()->back();
                                    }
                                }
                            }else{
                                $new['subtotal'] = @$request->order_total;   
                                $new['order_total'] = @$request->order_total;  
                                $new['shipping_price'] = 0.000;  
                                //dd($new);
                            }
                        }
                        
                        if(@$request->invoice){
                            $new['invoice_no']       =   @$request->invoice;
                        }
                        if(@$request->invoice_details){
                            $new['invoice_details']    =   @$request->invoice_details;
                        }
                        $new['admin_id']           =   null;
                        $new['creation_type']      =   'E';
                        $new['edit_date']          =   now();
                        $new['edited_by']          = Auth::guard('merchant')->user()->id;
                        $this->order_master->where('id',$id)->update($new);
                        $order = $this->order_master->where('id',$id)->first();
                        // $this->order_image->where('id',$id)->delete();
                        if(@$request->image){
                            // $this->order_image->where('order_id',$id)->delete();
                            foreach($request->image as $key=>$file){
                                $i = $key;
                                $name = $i.time()."_".$file->getClientOriginalName();
                                $path = 'storage/app/public/merchant/external_order/photo/';
                                $file->move($path,$name);
                                $order_img['order_id'] = $order->id;
                                $order_img['order_no'] = $order->order_no;
                                $order_img['images'] = $name;
                                $this->order_image->create($order_img);
                            }
                        }
                        // order details update
                        // $n['seller_id'] = Auth::guard('merchant')->user()->id;
                        // $n['order_master_id'] = $order->id;
                        if(@$request->product_weight){
                            $n['weight'] = @$request->product_weight;
                        }
                        
                        $n['sub_total'] = @$order->subtotal;
                        $n['original_price'] = @$order->subtotal;
                        $n['total'] = @$order->order_total;
                        $n['quantity'] = 1;
                        // $n['status'] = 'P';
                        $this->order_details->where('order_master_id',@$order->id)->update($n);



                        $orderMerchant['seller_id'] = Auth::guard('merchant')->user()->id;
                        if(@$order->product_total_weight){
                            $orderMerchant['total_weight'] = @$order->product_total_weight;
                        }
                        $orderMerchant['subtotal'] = $order->subtotal;
                        $orderMerchant['order_total'] = $order->order_total;
                        $orderMerchant['input_amount']    = @$request->order_total;
                        $this->order_sellers->where('order_master_id',$id)->update($orderMerchant);

                        // update total earnings
                        //$total_earning = $this->order_details->where('seller_id',Auth::guard('merchant')->user()->id)->sum('sub_total');
                        
                        //$update['total_earning'] = $total_earning;
                        // $update['total_commission'] = ($total_earning * (Auth::guard('merchant')->user()->commission/100));
                        // $update['total_due'] = ($total_earning * (Auth::guard('merchant')->user()->commission/100)) - (Auth::guard('merchant')->user()->total_paid);
                        //$this->merchants->where('id',Auth::guard('merchant')->user()->id)->update($update);
                        $merchant = $this->merchants->where('id',Auth::guard('merchant')->user()->id)->first();
                        # recalculate merchant earning when change order total after delivered of the order.
                        if($order->status == 'OD') {
                            # deduct previous amount 
                            $merchant = Merchant::where(['id' => $order->orderMasterDetails[0]->seller_id])->first();

                            $totEarning = ($merchant->total_earning - $orderMaster->order_total) + @$request->order_total;
                            $totDue = ($merchant->total_due - $orderMaster->order_total) + @$request->order_total;
                            Merchant::where(['id' => $order->orderMasterDetails[0]->seller_id])->update(['total_earning' => $totEarning, 'total_due' => $totDue]);
                        }
                        session()->flash("success",__('success.-4052'));
                        return redirect()->back();
                    }
                    return redirect()->back();
                }
                catch(\Exception $e){
                    return redirect()->back()->withError(['message' => $e->getMessage(), 'meaning' => "test"]);
                }    
                
            }
            return view('merchant.modules.external_order.edit_external_order')->with([
                'countries'=>@$countries,
                'states'=>@$states,
                'cities'=>@$cities,
                'order'=>@$order,
                'order_image'=>@$order_image,
                'address'=>@$addressBook,
                'timeslot' => $timeslot
            ]);
            
        }else{
            return redirect()->back();
        }
    }
    // public function getKuwaitStates(Request $)
    /*
    * Method: getKuwaitCity
    * Description: This method is used to get all cities depending upon state and country is only for kuwait
    * Author: Jayatri
    */
    public function getKuwaitCity(Request $request){
        if(@$request->all()){
            if(@$request->state){
                $states = $this->state->where('country_id',134)->get();
                $cities = $this->city->where('state_id',@$request->state)->get();
                // return response()->json($cities);
                // dd($cities);
                return view('merchant.modules.external_order.list_cities')->with(['cities'=>@$cities]);
            }else{
                return 0;
            }
        }
    }
    /**
    *   Method  : fetchCity
    *   Use     : Auto complete city list.  
    *   Author  : Jayatri
    *
    */
    public function fetchCity(Request $request){
        $response = array(
            'jsonrpc' => '2.0'
        );
        
        if($request->get('city'))
        {
            $city = $request->get('city');
            // if(Config::get('app.locale') == 'en') {
                // $data = City::where('name', 'LIKE', "%{$city}%")->orderBy('name')->get();
            // }elseif(Config::get('app.locale') == 'ar') {
                // $data = City::where('name_ar', 'LIKE', "%{$city}%")->orderBy('name')->get();
            // }
            // $data = $this->city->with('cityDetailsByLanguage')->where('name', 'LIKE', "%{$city}%")->orderBy('name')->get();
            $data = $this->city->with('cityDetailsByLanguage')->where('status', '!=', 'D');
            $data = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
            $data = $data->where(function($q1) use($request) {
                    $q1 = $q1->where('status', '!=', 'I');
                });
            $data = $data->whereHas('cityDetailsByLanguage',function($query) use($request) {
                $query->where('name', 'LIKE', "%{$request->city}%");
            })->orderBy('id','desc')->get();
            if($data->count() > 0) {
                $output = '<ul class="dropdown-menu" style="display:block;width: 100%;padding:10px;">';
                foreach($data as $row)
                {
                    // if(Config::get('app.locale') == 'en'){
                    //         $output .= '
                    // <a href="javascript:void(0);" style="color:black;"><li class="cityChange kuwait_cities">'.$row->name.'</li></a>
                    // ';
                    // }elseif(Config::get('app.locale') == 'ar'){
                    $output .= '
                    <a href="javascript:void(0);" style="color:black;"><li class="cityChange kuwait_cities">'.$row->cityDetailsByLanguage->name.'</li></a>
                    ';                        
                    // }
                    
                }
                $output .= '</ul>';
            } else {
                $output = 'No records found..';
            }
            $response['result'] = $output;
            $response['status'] = 'SUCCESS';
            return response()->json($response);
        } else {
            $response['status'] = 'ERROR';    
            return response()->json($response);     
        }
    }  

    /**
    *   Method  : fetchCityPickup
    *   Use     : Auto complete city list.  
    *   Author  : Argha
    *   Date    : 2020-09-21
    */
    public function fetchCityPickup(Request $request){
        $response = array(
            'jsonrpc' => '2.0'
        );
        
        if($request->get('city'))
        {
            $city = $request->get('city');
            $data = $this->city->with('cityDetailsByLanguage')->where('status', '!=', 'D');
            $data = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
            $data = $data->where(function($q1) use($request) {
                    $q1 = $q1->where('status', '!=', 'I');
                });
            $data = $data->whereHas('cityDetailsByLanguage',function($query) use($request) {
                $query->where('name', 'LIKE', "%{$request->city}%");
            })->orderBy('id','desc')->get();
            if($data->count() > 0) {
                $output = '<ul class="dropdown-menu" style="display:block;width: 100%;padding:10px;">';
                foreach($data as $row)
                {
                    $output .= '
                    <a href="javascript:void(0);" style="color:black;"><li class="pickUpCityChange pickup_kuwait_cities">'.$row->cityDetailsByLanguage->name.'</li></a>
                    ';    
                    
                }
                $output .= '</ul>';
            } else {
                $output = 'No records found..';
            }
            $response['result'] = $output;
            $response['status'] = 'SUCCESS';
            return response()->json($response);
        } else {
            $response['status'] = 'ERROR';    
            return response()->json($response);     
        }
    } 
    private function checkLoginAccess(){
        $this->middleware('merchant.auth:merchant');
        if(!@Auth::guard('merchant')->user()){
            return redirect()->route('merchant.login');
        }
    }
    /*
    *Method : sendNotification
    *Description : for send Notification
    *Author : Jayatri
    */
    private function sendNotification($order_id){
        $androidDrivers = Driver::where(['status' => 'A', 'device_type' => 'A'])->pluck('firebase_reg_no');

        $iosDrivers = Driver::where(['status' => 'A', 'device_type' => 'I'])->pluck('firebase_reg_no');
        $order_dtt = OrderMaster::where('id',$order_id)->first();
        # send notification to android drivers
        if(count($androidDrivers)) {
            $msg                      = array();
            $msg['title']             = 'New Order Placed';
            $msg["body"]              = 'Recently customer placed an order! ';
            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            $msg['link_type']         = "Y";
            $msg['is_linked']         = "Y";
            $msg['image']             = '';
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]           = "10";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'registration_ids' => $androidDrivers,
                'data'             => $msg
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            
            // Closing Curl
            curl_close($ch);
        }
        

        # send notification to ios drivers
        if(count($iosDrivers)) {
            $msg                      = array();
            $msg['title']             = "New Order Placed";
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]            = "10";
            $msg["mutable-content"]   = true;
            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            $msg["body"]              = 'Recently customer placed an order! ';

            $data['message'] = "New Order Placed";
            $data['type'] = "1";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'category'                  => "CustomSamplePush",
                'content_available'         => true,
                'mutable_content'           => true,
                'registration_ids'          => $iosDrivers,
                'notification'              => $msg,
                'data'                      => $data
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            // Closing Curl
            curl_close($ch);
        }
    }
    
    /*
    *Method : showExternalOrder
    *Description : to show external order annonymus
    *Author : Argha
    *Date   : 2020-09-29
    */
    public function showExternalOrder(Request $request) {
        $merchant = Merchant::with(['Country','merchantCityDetails'])->where('slug',$request->slug)->first();
        $id = $request->id;
        $order = $this->order_master->with([
            'customerDetails.userCountryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'orderMasterDetails.productDetails.productByLanguage',
            'orderMasterDetails.productDetails.productVariants',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails.merchantCityDetails',
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'getBillingCountry',
            'productVariantDetails',
            'getOrderAllImages',
            'orderMasterDetails.productDetails.defaultImage',
            'orderMasterDetails.driverDetails',
            'getCityNameByLanguage',
            'getBillingCityNameByLanguage',
            'getPreferredTime'
        ])->where(['id'=>$id]);
		$order = $order->whereHas('orderMasterDetails', function($q) use($merchant){
			$q->where(['seller_id'=>$merchant->id]);
		});
		$order = $order->first();
		$no_of_item = $this->order_details->where(['order_master_id'=>@$id,'seller_id'=>@$merchant->id])->count();
		$ord_sub_total = $this->order_details->where(['order_master_id'=>@$id,'seller_id'=>@$merchant->id])->sum('sub_total');
		$ord_total = $this->order_details->where(['order_master_id'=>@$id,'seller_id'=>@$merchant->id])->sum('total');
		$ord_sel = $this->order_sellers->where(['order_master_id'=>@$order->id,'seller_id'=>@$merchant->id])->first();
		return view('merchant.modules.external_order.show_external_order_anonymus')->with([
			'order'=>@$order,
			'total_no_item'=>@$no_of_item,
			'ord_sub_total'=>@$ord_sub_total,
			'ord_total'=>@$ord_total,
            'ord_sel'=>@$ord_sel,
            'merchant' => $merchant
            
		]);
    }
    
    /*
    * Method: sendNotificationDashboardUpdate 
    * Description : This method is used to update dashboard automatically specialy merchant dashboard client requirement using web notification (ajax calls)
    * Author: Jayatri
    */
    // private function sendNotificationDashboardUpdate($order_id) {
    // // For Notification sending
        
    //     $order_dtt = OrderMaster::where('id',$order_id)->first();
    //     $merchant = MerchantRegId::where(['status' => 'A'])->pluck('reg_id');
    //     if(@$order_dtt) {
    //         // $user = User::where('id',$post_notification->user_id)->first();
    //         $new_registrationIds = $merchant->reg_id;
    //         $username = Auth::user()->fname." ".Auth::user()->lname;
    //         $click_action = route('merchant.dashboard');
    //         $title = $username." External Order placed!";
    //         $message = "New order Placed!";
    //         $fields = array (
    //             'registration_ids' => array($new_registrationIds),
    //             'data' => array (
    //                 "message"       => $message,
    //                 "title"         => $title,
    //                 "image"         => "{{url('firebase-logo.png)}}",
    //                 "click_action"  => $click_action,
    //             ),
    //             'notification'      => array (
    //                 "body"          => $message,
    //                 "title"         => $title,
    //                 "click_action"  => $click_action,
    //                 "icon"          => "{{url('firebase-logo.png)}}",
    //             )
    //         );
    //         $fields = json_encode ( $fields );
    //         $API_ACCESS_KEY = 'AIzaSyBER7ha21ZqIULYq7f3jhixyHDllwr_Bvc';
    //         $headers = array(
    //             'Authorization: key=' . $API_ACCESS_KEY,
    //             'Content-Type: application/json',
    //         );
    //         $ch = curl_init ();
    //         curl_setopt ( $ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    //         curl_setopt ( $ch, CURLOPT_POST, true );
    //         curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    //         curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    //         curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
    //         $result = curl_exec ( $ch );
    //         curl_close ( $ch ); 
    //         return $result.' User Name : '.$merchant->username;
    //     }
    // // End of Notification sending
    // }
    /*
    * Method: getMerchantAddress
    * Description: This method is get merchant address from address book
    * Author: Argha
    * Date : 2020-10-13
    */
    public function getMerchantAddress(Request $request)
    {
        $response['jsonrpc'] = "2.0";
        $merchant_id = $request['data']['merchant_id'];
        $merchantAddress = MerchantAddressBook::with(['countryDetailsBylanguage','getCityNameByLanguage'])
                                                ->where('merchant_id',$merchant_id)
                                                ->get();
        $defaultAddress  = MerchantAddressBook::with(['countryDetailsBylanguage','getCityNameByLanguage'])
                                                ->where('merchant_id',$merchant_id)
                                                ->where('is_default',1)
                                                ->first();
        $response['address'] = $merchantAddress;
        $response['defaultAddress'] = $defaultAddress;
        return response()->json($response);
    }
 
}
