<?php

namespace App\Http\Controllers\Merchant\Modules\Driver_review;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OrderMaster;
use App\Models\DriverReview;
use App\Models\OrderSeller;
class ReviewDriverController extends Controller
{
    /**
	* Method: index
	* Description: This method is used to show the driver review page.
    * Author: Argha
    * Date  : 2020-09-09
	*/
    public function index($order_no, $user_id, $token) {
        $data['order'] = OrderMaster::with(['orderMasterDetails.driverDetails'])
                                    ->where(['order_no'=>$order_no])
                                    ->first();
        
        if($data['order'] != null){
            $data['checkForToken'] = OrderSeller::where([
                'order_master_id' => $data['order']->id,
                'seller_id'       => $user_id,
                'verify_code'     => $token
            ])
            ->first();
            
            $data['checkReview'] = DriverReview::where([
                'merchant_id'       => $user_id, 
                'order_master_id'   => $data['order']->id
            ])
            ->first();
        }
        return view('merchant.modules.driver_review.review_driver')->with($data);
    }

    /**
	* Method: submitDriverReview
	* Description: This method is used to show submit driver review .
    * Author: Argha
    * Date  : 2020-09-09
    */
    public function submitDriverReview(Request $request) {
        $insertRating['order_master_id'] = $request->order_id;
        $insertRating['customer_id'] = 0;
        $insertRating['merchant_id'] = $request->merchant_id;
        $insertRating['driver_id'] = $request->driver_id;
        $insertRating['review_point'] = $request->driver_rating;
        $insertRating['review_comments'] = $request->comment;
        $insert = DriverReview::create($insertRating);
        if($insert){
            OrderSeller::where([
                'order_master_id'=>$request->order_id,
                'seller_id' => $request->merchant_id
            ])
            ->update(['verify_code' =>null]);
            return '1';
        }else{
            return '0';
        }
    }
}
