<?php

namespace App\Http\Controllers\Merchant\Modules\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\User;
use Validator;
use Auth;
use App\Models\Language;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\ShippingCostRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\UserRepository;
use App\Repositories\ProductRepository;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;
use Excel;

class ReportController extends Controller
{
	protected $order_master,$order_details,$shipping_cost,$customers;
	public function __construct(OrderMasterRepository $order_master,
		ShippingCostRepository $shipping_cost,
		OrderDetailRepository $order_details,
		ProductVariantRepository $product_variant,
		ProductRepository $product,
		UserRepository $customers,
		Request $request)
	{
		$this->middleware('merchant.auth:merchant');
		$this->order_master            =   $order_master;
		$this->order_details           =   $order_details;
		$this->shipping_cost           =   $shipping_cost;
		$this->customers           	   =   $customers;
		$this->product_variant         =   $product_variant;
		$this->product         		   =   $product;

	}

	/*
	* Method: orderReport
	* Description: This method is used to show order Report.
	* Author: Surajit
	*/
	public function orderReport(Request $request){
		$customers 	= $this->order_master->with([
            'orderSellerInfo' => function($q) {
                $q->where('seller_id', @Auth::guard('merchant')->user()->id);
            },
            'customerDetails',
            'orderMasterDetails',
            'countryDetails'
        ])
        ->whereNotIn('status', ['I', 'F', 'D']);

		$customers = $customers->whereHas('orderMasterDetails', function($q) use ($request){
			$q->where(['seller_id'=>Auth::guard('merchant')->user()->id]);
		});
		$customers = $customers->get();

		$external_customer = $this->order_master->with(['customerDetails','orderMasterDetails','countryDetails']);
		$external_customer = $external_customer->whereHas('orderMasterDetails', function($q) use ($request){
			$q->where(['seller_id' => Auth::guard('merchant')->user()->id,'user_id'=>0]);
		});

		$orders = $this->order_master->with(['orderSellerInfo','customerDetails','orderMasterDetails','countryDetails']);
		$orders = $orders->whereHas('orderMasterDetails', function($q) use ($request){
			$q->where('seller_id',@Auth::guard('merchant')->user()->id);
		});
        $orders = $orders->where(function($q) use ($request){
            $q->whereNotIn('status',['I','D']);
        });
		
		if(@$request->keyword){
			$orders = $orders->where(function($query) use($request){
				$query->Where('shipping_fname','like','%'.$request->keyword.'%')
				->orWhere('shipping_lname','like','%'.$request->keyword.'%')
				->orWhere('order_no',$request->keyword)
				->orWhere('shipping_phone','like','%'.$request->keyword.'%')
				->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".$request->keyword."%");
			});
		}

		if(@$request->customer){
			if(@$request->external){
				if(@$request->external == 'y'){
					$orders = $orders->where(function($q1) use($request){
						$q1 = $q1->where('id',$request->customer);
					});
				}else{
					$orders = $orders->where(function($q1) use($request){
						$q1 = $q1->where('user_id',$request->customer);
					});
				}
			}
		}

		if(@$request->payment){
			$orders = $orders->where(function($q1) use($request){
				$q1 = $q1->where('payment_method',$request->payment);
			});
		}
		$orders = $orders->orderBy('id','desc')->get();
		return view('merchant.modules.reports.order_report')->with([
			'customers'=>@$customers,
			'external_customer'=>@$external_customer,
			'orders'=>@$orders
		]);
	}


    /*
    * Method        : orderReportExport
    * Description   : This method is used to export order report.
    * Author        : Surajit
    */
    public function orderReportExport(Request $request){
    	
    	$orders 	= $this->order_master->with(['orderSellerInfo','customerDetails','orderMasterDetails','countryDetails']);
    	$orders 	= $orders->whereHas('orderMasterDetails', function($q) use ($request){
    		$q->where('seller_id',@Auth::guard('merchant')->user()->id);
		});
		 
    	if(@$request->all()){
    		if(@$request->keyword){
    			$orders = $orders->where(function($query) use($request){
    				$query->Where('shipping_fname','like','%'.$request->keyword.'%')
    				->orWhere('shipping_lname','like','%'.$request->keyword.'%')
    				->orWhere('order_no',$request->keyword)
    				->orWhere('shipping_phone','like','%'.$request->keyword.'%')
    				->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".$request->keyword."%");
    			});
    		}
            $orders     = $orders->where(function($q) use ($request){
                $q->whereNotIn('status',['I','D', 'F']);
            });
    		if(@$request->customer){
    			if(@$request->external){
    				if(@$request->external == 'y'){
    					$orders = $orders->where(function($q1) use($request){
    						$q1 = $q1->where('id',$request->customer);
    					});
    				}else{
    					$orders = $orders->where(function($q1) use($request){
    						$q1 = $q1->where('user_id',$request->customer);
    					});
    				}
    			}
    		}

    		if(@$request->payment){
    			$orders = $orders->where(function($q1) use($request){
    				$q1 = $q1->where('payment_method',$request->payment);
    			});
    		}
    		if(@$request->order_type){
    			$orders = $orders->where(function($q1) use($request){
    				$q1 = $q1->where('order_type',$request->order_type);
    			});
    		}
    		if(@$request->from_date && @$request->to_date) {
    			$orders = $orders->where(function($q1) use($request){
    				$q1 = $q1->whereBetween('created_at',[$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
    			});
    		}
    	}
		$orders = $orders->orderBy('id','desc')->get();
		//dd($orders);
    	$orders_array[] = array('Order No', 'Customer', 'Date', 'Subtotal', 'Shipping Cost', 'Total', 'Payment', 'Type', 'Status');
    	foreach($orders as $orders)
    	{
    		if($orders->order_type == 'I') {
    			$orderType = 'Internal';
    		} else {
    			$orderType = 'External';                
    		}
    		if($orders->payment_method == 'C') {
    			$paymentMethod = 'Cash On Delivery';
    		} else {
    			$paymentMethod = 'Online';                
    		}
    		if($orders->status == 'I') {
    			$status = 'Incomplete';
    		} elseif($orders->status == 'N') {
    			$status = 'New';                
    		} elseif($orders->status == 'OA') {
    			$status = 'Order accepted';                
    		} elseif($orders->status == 'DA') {
    			$status = 'Driver assigned';                
    		} elseif($orders->status == 'RP') {
    			$status = 'Ready for picked up';                
    		} elseif($orders->status == 'OP') {
    			$status = 'Order Picked up';                
    		} elseif($orders->status == 'OD') {
    			$status = 'Order Delivered';                
    		} elseif($orders->status == 'OC') {
    			$status = 'Order Cancelled';                
    		}

            if($orders->order_type == 'I'){
                $order_total = @$orders->orderSellerInfo->order_total - @$orders->orderSellerInfo->shipping_price;
            }else{
                $order_total = @$orders->orderSellerInfo->order_total;
            }

            if(@$orders->order_type == 'E'){
                $shipping_price = @$orders->orderSellerInfo->shipping_price; 
            }else{
                $shipping_price = "";    
			} 
			
			if($orders->order_type == 'I' && Auth::guard('merchant')->user()->hide_customer_info == 'N'  ){
				$customer = @$orders->customerDetails->fname.' '.@$orders->customerDetails->lname;
			}elseif($orders->order_type == 'E'){
				$customer = @$orders->shipping_fname.' '.@$orders->shipping_lname;
			}else{
				$customer = '';
			}
    		$orders_array[] = array(
    			'Order No'         => @$orders->order_no,
    			'Customer'         => @$customer,
    			'Date'             => @$orders->created_at,
    			'Subtotal'         => @$orders->orderSellerInfo->subtotal - $orders->orderSellerInfo->total_discount,
    			'Shipping Cost'    => @$shipping_price,
    			'Total'      	   => $order_total,
    			'Payment'          => $paymentMethod,
    			'Type'             => $orderType,
    			'Status'           => @$status
    		);
    	}
    	Excel::create('Order Report', function($excel) use ($orders_array){
    		$excel->setTitle('Order Report');
    		$excel->sheet('Order Report', function($sheet) use ($orders_array){
    			$sheet->fromArray($orders_array, null, 'A1', false, false);
    		});
    	})->download('xlsx');
    }

	/*
	* Method: searchOrderReport
	* Description: This method is used to search Order Reports.
	* Author: Surajit
	*/
	public function searchOrderReport(Request $request){
		$customers 	= $this->customers->where('id','!=',0);
		$orders 	= $this->order_master->with([
            'orderSellerInfo' => function($q) {
                $q->where('seller_id', @Auth::guard('merchant')->user()->id);
            },
            'customerDetails',
            'orderMasterDetails',
            'countryDetails'
        ]);
		$orders = $orders->whereHas('orderMasterDetails', function($q) use ($request){
			$q->where('seller_id', @Auth::guard('merchant')->user()->id);
		});
		
		if(@$request->keyword){
			$orders = $orders->where(function($query) use($request){
				$query->Where('shipping_fname','like','%'.$request->keyword.'%')
				->orWhere('shipping_lname','like','%'.$request->keyword.'%')
				->orWhere('order_no',$request->keyword)
				->orWhere('shipping_phone','like','%'.$request->keyword.'%')
				->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".$request->keyword."%");
			});
		}

		if(@$request->customer) {
			if(@$request->external) {
				if(@$request->external == 'y'){
					$orders = $orders->where(function($q1) use($request) {
						$q1 = $q1->where('id',$request->customer);
					});
				} else {
					$orders = $orders->where(function($q1) use($request) {
						$q1 = $q1->where('user_id',$request->customer);
					});
				}
			}
		}
        $orders = $orders->where(function($q) use ($request){
            $q->whereNotIn('status',['I','D', 'F']);
        });
		if(@$request->payment) {
			$orders = $orders->where(function($q1) use($request) {
				$q1 = $q1->where('payment_method',$request->payment);
			});
		}
		if(@$request->order_type) {
			$orders = $orders->where(function($q1) use($request) {
				$q1 = $q1->where('order_type',$request->order_type);
			});
		}
		if(@$request->from_date && @$request->to_date) {
			$orders = $orders->where(function($q1) use($request){
				$q1 = $q1->whereBetween('created_at',[$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
			});
		}
		$orders = $orders->orderBy('id','desc')->get();
		return view('merchant.modules.reports.ajax_order_report')->with([
			'customers'=>@$customers,
			'orders'=>@$orders
		]);
	}

	/*
	* Method: earningReport
	* Description: This method is used to show earning Report.
	* Author: Surajit
	*/
	public function earningReport(Request $request){
		$customers 	= $this->order_master->with(['customerDetails','orderMasterDetails','countryDetails']);

		$customers = $customers->whereHas('orderMasterDetails', function($q) use ($request){
			$q->where(['seller_id'=>Auth::guard('merchant')->user()->id]);
		});
		$customers = $customers->get();

		$external_customer = $this->order_master->with(['customerDetails','orderMasterDetails','countryDetails']);
		$external_customer = $external_customer->whereHas('orderMasterDetails', function($q) use ($request){
			$q->where(['seller_id'=>Auth::guard('merchant')->user()->id,'user_id'=>0]);
		});

		$orders 	= $this->order_master->with(['customerDetails','orderMasterDetails','countryDetails']);
		$orders 	= $orders->whereHas('orderMasterDetails', function($q) use ($request){
			$q->where('seller_id',@Auth::guard('merchant')->user()->id);
		});
		
		if(@$request->keyword){
			$orders = $orders->where(function($query) use($request){
				$query->Where('shipping_fname','like','%'.$request->keyword.'%')
				->orWhere('shipping_lname','like','%'.$request->keyword.'%')
				->orWhere('order_no',$request->keyword)
				->orWhere('shipping_phone','like','%'.$request->keyword.'%')
				->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".$request->keyword."%");
			});
		}

		if(@$request->customer){
			if(@$request->external){
				if(@$request->external == 'y'){
					$orders = $orders->where(function($q1) use($request){
						$q1 = $q1->where('id',$request->customer);
					});
				}else{

					$orders = $orders->where(function($q1) use($request){
						$q1 = $q1->where('user_id',$request->customer);
					});
				}
			}
		}

		if(@$request->payment){
			$orders = $orders->where(function($q1) use($request){
				$q1 = $q1->where('payment_method',$request->payment);
			});
		}
		$orders = $orders->orderBy('id','desc')->get();
		return view('merchant.modules.reports.earning_report')->with([
			'customers'=>@$customers,
			'external_customer'=>@$external_customer,
			'orders'=>@$orders
		]);
	}


    /*
    * Method        : earningReportExport
    * Description   : This method is used to export earning report.
    * Author        : Surajit
    */
    public function earningReportExport(Request $request){
    	// dd($request->all());
    	$orders 	= $this->order_master->with(['customerDetails','orderMasterDetails','countryDetails']);
    	$orders 	= $orders->whereHas('orderMasterDetails', function($q) use ($request){
    		$q->where('seller_id',@Auth::guard('merchant')->user()->id);
    	});
    	if(@$request->all()){
    		if(@$request->keyword){
    			$orders = $orders->where(function($query) use($request){
    				$query->Where('shipping_fname','like','%'.$request->keyword.'%')
    				->orWhere('shipping_lname','like','%'.$request->keyword.'%')
    				->orWhere('order_no',$request->keyword)
    				->orWhere('shipping_phone','like','%'.$request->keyword.'%')
    				->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".$request->keyword."%");
    			});
    		}

    		if(@$request->customer){
    			if(@$request->external){
    				if(@$request->external == 'y'){
    					$orders = $orders->where(function($q1) use($request){
    						$q1 = $q1->where('id',$request->customer);
    					});
    				}else{

    					$orders = $orders->where(function($q1) use($request){
    						$q1 = $q1->where('user_id',$request->customer);
    					});
    				}
    			}
    		}

    		if(@$request->payment){
    			$orders = $orders->where(function($q1) use($request){
    				$q1 = $q1->where('payment_method',$request->payment);
    			});
    		}
    		if(@$request->order_type){
    			$orders = $orders->where(function($q1) use($request){
    				$q1 = $q1->where('order_type',$request->order_type);
    			});
    		}
    		if(@$request->from_date && @$request->to_date) {
    			$orders = $orders->where(function($q1) use($request){
    				$q1 = $q1->whereBetween('created_at',[$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
    			});
    		}
    	}
    	$orders = $orders->orderBy('id','desc')->get();
    	$orders_array[] = array('Order No', 'Customer', 'Date', 'Subtotal', 'Shipping Cost', 'Total', 'Payment', 'Type', 'Status', 'Loyalty Point Received', 'Loyalty Point Used');
    	foreach($orders as $orders)
    	{
    		if($orders->order_type == 'I') {
    			$orderType = 'Internal';
    		} else {
    			$orderType = 'External';                
    		}
    		if($orders->payment_method == 'C') {
    			$paymentMethod = 'Cash On Delivery';
    		} else {
    			$paymentMethod = 'Online';                
    		}
    		if($orders->status == 'I') {
    			$status = 'Incomplete';
    		} elseif($orders->status == 'N') {
    			$status = 'New';                
    		} elseif($orders->status == 'OA') {
    			$status = 'Order accepted';                
    		} elseif($orders->status == 'DA') {
    			$status = 'Driver assigned';                
    		} elseif($orders->status == 'RP') {
    			$status = 'Ready for picked up';                
    		} elseif($orders->status == 'OP') {
    			$status = 'Order Picked up';                
    		} elseif($orders->status == 'OD') {
    			$status = 'Order Delivered';                
    		} elseif($orders->status == 'OC') {
    			$status = 'Order Cancelled';                
    		}

    		$orders_array[] = array(
    			'Order No'         => $orders->order_no,
    			'Customer'         => @$orders->customerDetails->fname.' '.@$orders->customerDetails->lname,
    			'Date'             => $orders->created_at,
    			'Subtotal'         => $orders->subtotal,
    			'Shipping Cost'    => $orders->shipping_price,
    			'Total'      	   => $orders->order_total,
    			'Payment'          => $paymentMethod,
    			'Type'             => $orderType,
    			'Status'           => $status,
    			'Loyalty Point Received'       => $orders->loyalty_point_received,
    			'Loyalty Point Used'           => $orders->loyalty_point_used
    		);
    	}
        // dd($orders_array);
    	Excel::create('Earning Report', function($excel) use ($orders_array){
    		$excel->setTitle('Earning Report');
    		$excel->sheet('Earning Report', function($sheet) use ($orders_array){
    			$sheet->fromArray($orders_array, null, 'A1', false, false);
    		});
    	})->download('xlsx');
    }
	/*
	* Method: searchEarningReport
	* Description: This method is used to search earning Reports.
	* Author: Surajit
	*/
	public function searchEarningReport(Request $request){
		$customers 	= $this->customers->where('id','!=',0);
		$orders 	= $this->order_master->with('customerDetails','orderMasterDetails','countryDetails');
		// ->where('seller_id',@Auth::guard('merchant')->user()->id);
		$orders = $orders->whereHas('orderMasterDetails', function($q) use ($request){
			$q->where('seller_id',@Auth::guard('merchant')->user()->id);
		});
		
		if(@$request->keyword){
			$orders = $orders->where(function($query) use($request){
				$query->Where('shipping_fname','like','%'.$request->keyword.'%')
				->orWhere('shipping_lname','like','%'.$request->keyword.'%')
				->orWhere('order_no',$request->keyword)
				->orWhere('shipping_phone','like','%'.$request->keyword.'%')
				->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".$request->keyword."%");
			});
		}

		if(@$request->customer){
			if(@$request->external){
				if(@$request->external == 'y'){
					$orders = $orders->where(function($q1) use($request){
						$q1 = $q1->where('id',$request->customer);
					});
				}else{

					$orders = $orders->where(function($q1) use($request){
						$q1 = $q1->where('user_id',$request->customer);
					});
				}
			}	

		}

		if(@$request->payment){
			$orders = $orders->where(function($q1) use($request){
				$q1 = $q1->where('payment_method',$request->payment);
			});
		}
		if(@$request->order_type){
			$orders = $orders->where(function($q1) use($request){
				$q1 = $q1->where('order_type',$request->order_type);
			});
		}
		if(@$request->from_date && @$request->to_date) {
			$orders = $orders->where(function($q1) use($request){
				$q1 = $q1->whereBetween('created_at',[$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
			});
		}
		$orders = $orders->orderBy('id','desc')->get();

		return view('merchant.modules.reports.ajax_earning_report')->with([
			'customers'=>@$customers,
			'orders'=>@$orders
		]);
	}
	
}
