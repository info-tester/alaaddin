<?php

namespace App\Http\Controllers\Merchant\Modules\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\Customer;
use App\Admin;
use Validator;
use Auth;
use App\Models\Language;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\ShippingCostRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\WithdrawRepository;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;
use App\Mail\SendMailWithDrawlRequest;

class FinanceController extends Controller
{
    protected $order_master,$order_details,$shipping_cost,$order_sellers;
    public function __construct(OrderMasterRepository $order_master,
    							ShippingCostRepository $shipping_cost,
    							OrderDetailRepository $order_details,
    							OrderSellerRepository $order_sellers,
    							WithdrawRepository $withdraw,
    							Request $request)
	{
		$this->middleware('merchant.auth:merchant',['except' => 'checkDuplicate']);
		$this->checkLoginAccess();
		// $this->middleware('merchant.auth:merchant');
		$this->order_master            =   $order_master;
        $this->order_details           =   $order_details;
        $this->shipping_cost           =   $shipping_cost;
        $this->order_sellers           =   $order_sellers;
        $this->withdraw           	   =   $withdraw;

	}
	/*
	* Method: index
	* Description: This method is used to show earnings of merchant.
	* Author: Jayatri
	*/
	public function index(){
		$this->checkLoginAccess();
		$withdraws = $this->withdraw->where('seller_id',@Auth::guard('merchant')->user()->id)->orderBy('id','desc')->get();
		// dd($withdraws);
		return view('merchant.modules.finance.view_earnings')->with([
			'withdraws'		=>	@$withdraws
		]);
	}
	/*
	* Method: requestWithdraw
	* Description: This method is used to request Withdraw money.
	* Author: Jayatri
	*/
	public function requestWithdraw(Request $request,$id){
		$this->checkLoginAccess();
		try{
			if(@Auth::guard('merchant')->user()->total_due >= @$request->amount){
				$new['seller_id'] = @Auth::guard('merchant')->user()->id;
				$new['bank_name'] = @Auth::guard('merchant')->user()->bank_name;
				$new['account_name'] = @Auth::guard('merchant')->user()->account_name;
				$new['account_no'] = @Auth::guard('merchant')->user()->account_number;
				$new['iban_number'] = @Auth::guard('merchant')->user()->iban_number;
				$new['request_date'] = now();
				$new['amount'] = @$request->amount;
				$new['balance'] = @Auth::guard('merchant')->user()->total_due;
				$new['status'] = 'N';
				$withdraw = $this->withdraw->create($new);
				// $withdrawlDetails 			= 	new \stdClass();
        		$data['merchant_name'] 		= 	@Auth::guard('merchant')->user()->fname." ".@Auth::guard('merchant')->user()->lname;
        		$data['merchant_email'] 	=	@Auth::guard('merchant')->user()->email1;
        		$data['created_at'] 		=	now();
        		$data['merchant_phone'] 	=	@Auth::guard('merchant')->user()->phone;
        		$data['account_no'] 		=	@Auth::guard('merchant')->user()->account_no;
        		$data['account_name'] 		=	@Auth::guard('merchant')->user()->account_name;
        		$data['bank_name'] 			=	@Auth::guard('merchant')->user()->bank_name;
        		$data['iban_number'] 		=	@Auth::guard('merchant')->user()->iban_number;
        		$data['amount'] 			=	@$request->amount;
        		$data['balance'] 			=	@Auth::guard('merchant')->user()->total_due;
        		$data['request_id'] 		=	@$withdraw->id;
        		$data['balance'] 			=	@Auth::guard('merchant')->user()->total_due;
        		if(@$withdraw->status == 'N'){
        			$data['wstatus'] 		=	"New";	
        		}
        		

        		$admin_notify = Admin::where('type','A')->first();
        		if(@$admin_notify){
        			if(@$admin_notify->email_1){
        				$contactList[0] = $admin_notify->email_1;    	
        			}
        			if(@$admin_notify->email_2){
        				$contactList[1] = $admin_notify->email_2;    
        			}
        			if(@$admin_notify->email_3){
        				$contactList[2] = $admin_notify->email_3;
        			}
        			
        			    
        			$contactListEmail3 = array_unique($contactList);
        			$data['bcc'] = $contactListEmail3;
        		}
        		// $withdrawlDetails->balance 	=	$Auth::guard('merchant')->user()->total_due;
        		// dd($data);
    			Mail::send(new SendMailWithDrawlRequest($data));
				session()->flash("success",__('success.-4045'));	
			}else{
				session()->flash("error",__('errors.-5042'));
			}
		}
		catch(\Exception $e){
			return redirect()->back()->withError(['message' => $e->getMessage(), 'meaning' => "Error!"]);
		}
		if(@$withdraw){
			session()->flash("success",__('success.-4045'));
		}else{
			session()->flash("error",__('errors.-5042'));
		}
		
		return redirect()->back();
	}
	/*
	* Method: requestWithdraw
	* Description: This method is used to request Withdraw money.
	* Author: Jayatri
	*/
	public function checkDuplicate(Request $request){
		$order = $this->withdraw->where(['seller_id'=>$request->seller_id,'status'=>'N'])->first();
		if(@$order){
			return 0;
		}else{
			return 1;
		}
	}
	/*
	* Method: checkLoginAccess
	* Description: This method is used to check login Access.
	* Author: Jayatri
	*/
	private function checkLoginAccess(){
		$this->middleware('merchant.auth:merchant',['except' => 'checkDuplicate']);
		if(@Auth::guard('merchant')->user()){
			return redirect()->route('merchant.login');
		}
	}
}
