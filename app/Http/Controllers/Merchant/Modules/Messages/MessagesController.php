<?php

namespace App\Http\Controllers\Merchant\Modules\Messages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\Customer;
use Validator;
use Auth;
use App\Models\City;
use App\Models\Language;
use App\Models\ShippingCost;
use App\Models\CitiDetails;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\ShippingCostRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\OrderImageRepository;
use App\Repositories\StateRepository;
use App\Repositories\CityRepository;
use App\Repositories\CitiDetailsRepository;
use App\Repositories\SettingRepository;
use App\Repositories\ConversationsRepository;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;
use App\Mail\SendMailExternalOrderdetails;
use Config;

class MessagesController extends Controller
{
	/*
	* Method 		:
	* Description 	: This method is showing the list of messages
	* Author        : Jayatri
	*/
    
	public function __construct(ConversationsRepository $conversations, Request $request)
    {
        $this->middleware('merchant.auth:merchant');
        $this->conversations =   $conversations;
    }


    public function showMessages(){
    	$conversation = $this->conversations->with(['sendBySubAdminDetails'])->where(['send_to' => Auth::guard('merchant')->user()->id])->orderBy('id','desc')->get();
    	return view('merchant.modules.messages.messagess')->with(['conversations'=>@$conversation]);
    }


    public function viewMessage($id){
    	// dd($id);
    	$message = $this->conversations->with(['sendBySubAdminDetails','sendByCustomerDetails','sendByDriverDetails','sendBySubAdminDetails','sendByMerchantDetails'])->orderBy('id','desc')->where('id',$id)->first();
    	if(@$message){
    		return view('merchant.modules.messages.view_conversation_details')->with(['message'=>@$message]);
    	}else{
    		session()->flash("error",__('errors.-5002'));
    		return redirect()->back();
    	}
    	
    }
	

}
