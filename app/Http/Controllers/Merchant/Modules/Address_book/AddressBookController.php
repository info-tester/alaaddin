<?php

namespace App\Http\Controllers\Merchant\Modules\Address_book;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\MerchantAddressBook;
use App\Models\Country;
use App\Models\City;
use App\Models\CitiDetails;
class AddressBookController extends Controller
{
    public function __construct()
    {

     $this->middleware('merchant.auth:merchant');
     $this->checkLoginAccess();

    }
    /*
    *Method : manageAddressBook
    *Description : to show merchant address list
    *Author : Argha
    * Date  : 2020-SEP-17
    */
    public function manageAddressBook(Request $request)
    {
        $address = MerchantAddressBook::with(['countryDetailsBylanguage','getCityNameByLanguage'])->where('merchant_id',Auth::guard('merchant')->user()->id);
        if($request->all()){
            

            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

            // if(@$request->columns['1']['search']['value']){
			// 	$coupons = $coupons->where(function($query) use($request){
            //         $query->Where('code','like','%'.@$request->columns['1']['search']['value'].'%');
            //     });
			// }

            // if(@$request->columns['2']['search']['value']){
			// 	$coupons = $coupons->where(function($query) use($request){
            //         $query->Where('applied_for',@$request->columns['2']['search']['value']);
            //     });
            // }
            
            // if(@$request->columns['3']['search']['value']){
			// 	$coupons = $coupons->where(function($query) use($request){
            //         $query->Where('applied_on',@$request->columns['3']['search']['value']);
            //     });
			// }
            //  //sorting
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $address = $address->orderBy('block','asc');
                } else {
                    $address = $address->orderBy('block','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $address = $address->orderBy('street','asc');
                } else {
                    $address = $address->orderBy('street','desc');
                }
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $address = $address->orderBy('postal_code','asc');
                } else {
                    $address = $address->orderBy('postal_code','desc');
                }
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $address = $address->orderBy('building_no','asc');
                } else {
                    $address = $address->orderBy('building_no','desc');
                }
            }
            if($columnIndex == 6) {
                if($columnSortOrder == 'asc') {
                    $address = $address->orderBy('more_address','asc');
                } else {
                    $address = $address->orderBy('more_address','desc');
                }
            }

            $address = $address->skip($request->start)->take($request->length)->get();
            $data['aaData'] = $address;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = count($address);
            $data["iTotalDisplayRecords"] = count($address);
            return response()->json($data);
        }else{

            return view('merchant.modules.address_book.manage_address_book');
        }
        
    }

    /*
    *Method : addAddress
    *Description : to show add address list
    *Author : Argha
    * Date  : 2020-SEP-17
    */

    public function addAddress()
    {
        $data['cities'] = City::get();
		$country        = Country::with('countryDetailsBylanguage');
		$country        = $country->where(function($q1) {
                                $q1 = $q1->where('status', '!=', 'I');
                            });
        $country        = $country->where(function($q1) {
                                $q1 = $q1->where('status','!=','D');
                            });
        $data['countries'] = $country->orderBy('id','desc')->get();
        return view('merchant.modules.address_book.add_address')->with($data);
    }

    /*
    *Method : insertAddress
    *Description : to insert address
    *Author : Argha
    * Date  : 2020-SEP-17
    */

    public function insertAddress(Request $request)
    {
       //dd(Auth::guard('merchant')->user());
       $request->validate([
            'country' => 'required',
            'street'  => 'required'
       ]);

       if($request->kuwait_city != null){
            $city = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
            $city = $city->whereHas('cityDetailsByLanguage',function($query) use($request) {
                $query->where('name', 'LIKE', "%{$request->kuwait_city}%");
            })->get();
            //dd($city);
            if(!$city) {
                session()->flash("error",__('success_user.-722'));
                return redirect()->back();		
            }
            $city_id = $city[0]->id;
            $city = $city[0]->cityDetailsByLanguage->name;
       }else{
            $city = @$request->city;
       }
       
       if($request->is_default == 'Y'){
            $checkDefault = MerchantAddressBook::where('is_default',1)->first();
            if($checkDefault != null){
                $checkDefault = MerchantAddressBook::where('id',$checkDefault->id)->update(['is_default'=>0]);
            }
            $default = 'Y';
       }else{
            $default = 'N';
       }
       $new['merchant_id'] = Auth::guard('merchant')->user()->id;
       $new['country'] = $request->country;
       $new['city'] = @$city;
       $new['city_id'] = @$city_id;
       $new['building_no'] = @$request->building;
       $new['street'] = @$request->street;
       $new['location'] = @$request->location;
       $new['postal_code'] = @$request->zip;
       $new['block'] = @$request->block;
       $new['lat'] = @$request->lat;
       $new['lng'] = @$request->lng;
       $new['phone'] = @$request->phone;
       $new['more_address'] = @$request->address;
       $new['is_default'] = @$default;
       //dd($new);
       MerchantAddressBook::create($new);

       session()->flash("success",__('success_user.-702'));

       return redirect()->route('merchant.manage.address.book');
    }

    /*
    *Method : editAddress
    *Description : to show edit address form
    *Author : Argha
    * Date  : 2020-SEP-17
    */
    public function editAddress($id)
    {
        $data['cities'] = City::get();
		$country        = Country::with('countryDetailsBylanguage');
		$country        = $country->where(function($q1) {
                                $q1 = $q1->where('status', '!=', 'I');
                            });
        $country        = $country->where(function($q1) {
                                $q1 = $q1->where('status','!=','D');
                            });
        $data['countries'] = $country->orderBy('id','desc')->get();
        $data['address']   = MerchantAddressBook::with('getCityNameByLanguage')->where('id',$id)->first();
        //dd($data['address']);
        return view('merchant.modules.address_book.edit_address')->with($data);
    }

    /*
    *Method : updateAddress
    *Description : to update address
    *Author : Argha
    * Date  : 2020-SEP-18
    */

    public function updateAddress(Request $request)
    {
        $request->validate([
            'country' => 'required',
            'street'  => 'required'
       ]);

       if($request->kuwait_city != null){
            $city = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
            $city = $city->whereHas('cityDetailsByLanguage',function($query) use($request) {
                $query->where('name', 'LIKE', "%{$request->kuwait_city}%");
            })->get();
            //dd($city);
            if(!$city) {
                session()->flash("error",__('success_user.-722'));
                return redirect()->back();		
            }
            $city_id = $city[0]->id;
            $city = $city[0]->cityDetailsByLanguage->name;
        }else{
             $city = @$request->city;
        }
       
       if($request->is_default == 'Y'){
        $checkDefault = MerchantAddressBook::where('is_default',1)->where('id','!=',$request->id)->first();
        if($checkDefault != null){
            $checkDefault = MerchantAddressBook::where('id',$checkDefault->id)->update(['is_default'=>0]);
        }
            $default = 'Y';
       }else{
            $default = 'N';
       }
       $up['merchant_id'] = Auth::guard('merchant')->user()->id;
       $up['country'] = $request->country;
       $up['city'] = @$city;
       $up['city_id'] = @$city_id;
       $up['building_no'] = @$request->building;
       $up['street'] = @$request->street;
       $up['location'] = @$request->location;
       $up['postal_code'] = @$request->zip;
       $up['block'] = @$request->block;
       $up['lat'] = @$request->lat;
       $up['lng'] = @$request->lng;
       $up['phone'] = @$request->phone;
       $up['more_address'] = @$request->address;
       $up['is_default'] = @$default;

       MerchantAddressBook::where('id',$request->id)->update($up);

       session()->flash("success",__('success_user.-704'));

       return redirect()->route('merchant.manage.address.book');
    }

    /*
    *Method : deleteAddress
    *Description : to delete address
    *Author : Argha
    * Date  : 2020-SEP-18
    */
    public function deleteAddress(Request $request)
    {
        $response['jsonrpc'] = "2.0";
        $id = $request['data']['id'];
        $delete = MerchantAddressBook::where('id',$id)->delete();
        if($delete){
            $response['status'] = "1";
        }else{
            $response['status'] = "0";
        }
        return response()->json($response);
    }
    private function checkLoginAccess(){
        $this->middleware('merchant.auth:merchant');
        if(!@Auth::guard('merchant')->user()){
            return redirect()->route('merchant.login');
        }
    }
}
