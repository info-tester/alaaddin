<?php

namespace App\Http\Controllers\Merchant\Modules\Orders;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\Models\OrderMaster;
use App\Models\OrderSeller;
use App\Models\OrderDetail;
use App\Models\Language;
use App\User;
use Validator;
use Auth;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\ShippingCostRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\UserRepository;
use App\Repositories\ProductRepository;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Hash;

use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;

class OrdersController extends Controller
{
    protected $order_master,$order_details,$shipping_cost,$customers;
    public function __construct(OrderMasterRepository $order_master,
    							ShippingCostRepository $shipping_cost,
    							OrderDetailRepository $order_details,
    							ProductVariantRepository $product_variant,
    							ProductRepository $product,
    							UserRepository $customers,
    							Request $request)
	{
		$this->middleware('merchant.auth:merchant');
		$this->checkLoginAccess();
		// $this->middleware('merchant.auth:merchant');
		$this->order_master            =   $order_master;
        $this->order_details           =   $order_details;
        $this->shipping_cost           =   $shipping_cost;
        $this->customers           	   =   $customers;
        $this->product_variant         =   $product_variant;
        $this->product         		   =   $product;

	}
	/*
	* Method: addExternalOrder
	* Description: This method is used to add External Order.
	* Author: Jayatri
	*/
	public function manageOrders(Request $request){
		$this->checkLoginAccess();
		$customers 	= $this->order_master->with([
			'orderSellerInfo',
			'customerDetails',
			'orderMasterDetails',
			'countryDetails.countryDetailsBylanguage',
			'getCityNameByLanguage'
		]);
		$customers = $customers->whereHas('orderMasterDetails', function($q) use ($request){
			$q->where(['seller_id'=>Auth::guard('merchant')->user()->id]);
		});
		$customers = $customers->get();
		$external_customer = $this->order_master->with([
			'customerDetails',
			'orderMasterDetails',
			'countryDetails.countryDetailsBylanguage',
			'getCityNameByLanguage'
		]);
		$external_customer = $external_customer->whereHas('orderMasterDetails', function($q) use ($request){
			$q->where(['seller_id'=>Auth::guard('merchant')->user()->id,'user_id'=>0]);
		});
		$orders = $this->order_master->with([
			'orderSellerInfo',
			'customerDetails',
			'orderMasterDetails',
			'countryDetails.countryDetailsBylanguage',
			'getCityNameByLanguage',
			'getpickupCountryDetails',
			'getpickupCityDetails'
		])->where('status','!=','I');
		$orders = $orders->whereNotIn('status', ['','D','I','F']);
        $orders = $orders->where('status','!=',' ');
		$orders = $orders->whereHas('orderMasterDetails', function($q) use ($request){
			$q->where('seller_id',@Auth::guard('merchant')->user()->id);
		});
		$orders = $orders->whereHas('orderSellerInfo', function($q) use ($request){
			$q->where('seller_id',@Auth::guard('merchant')->user()->id);
		});
		
		if(@$request->keyword){
            $orders = $orders->where(function($query) use($request){
	            $query->Where('shipping_fname','like','%'.$request->keyword.'%')
	            ->orWhere('shipping_lname','like','%'.$request->keyword.'%')
	            ->orWhere('order_no',$request->keyword)
	            ->orWhere('shipping_phone','like','%'.$request->keyword.'%')
	            ->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".$request->keyword."%");
            });
		}

		if(@$request->customer){
			if(@$request->external){
				if(@$request->external == 'y'){
					$orders = $orders->where(function($q1) use($request){
	            		$q1 = $q1->where('id',$request->customer);
	           		});
				} else {
					$orders = $orders->where(function($q1) use($request){
	            		$q1 = $q1->where('user_id',$request->customer);
	           		});
				}
			}
		}

		if(@$request->payment){
			$orders = $orders->where(function($q1) use($request){
            	$q1 = $q1->where('payment_method',$request->payment);
            });
		}
		if(@$request->order_type){
			$orders = $orders->where(function($q1) use($request){
            	$q1 = $q1->where('order_type',$request->order_type);
            });
		}
		$orders = $orders->orderBy('id','desc')->get();
		return view('merchant.modules.orders.list_orders')->with([
			'customers' 		=> @$customers,
			'external_customer' => @$external_customer,
			'orders' 			=> @$orders
		]);
	}

	/*
	* Method: searchOrders
	* Description: This method is used to search Orders.
	* Author: Jayatri
	*/
	public function searchOrders(Request $request){
		$this->checkLoginAccess();
		$columnIndex = $request->order[0]['column']; // Column index
        $columnName = $request->columns[$columnIndex]['data']; // Column name
        $columnSortOrder = $request->order[0]['dir']; // asc or desc

		$userId = @Auth::guard('merchant')->user()->id;
		$customers 	= $this->customers->where('id','!=',0);
		$orders 	= $this->order_master->with([
			'customerDetails',
			'orderMasterDetails',
			'countryDetails.countryDetailsBylanguage',
			'getCityNameByLanguage', 
			'orderSellerInfo' => function($q) use ($userId) {
				$q->where('seller_id', $userId);
			}
		]);
		// ->where('seller_id',@Auth::guard('merchant')->user()->id);
		$orders = $orders->where(function($q) {
			$q->whereNotIn('status', ['','D','I','F']);
		});
        $orders = $orders->whereHas('orderMasterDetails', function($q) use ($userId){
            $q->where('seller_id', $userId);
        });
        $orders = $orders->whereHas('orderSellerInfo', function($q) use ($userId){
            $q->where('seller_id', $userId);
        });
		$ps 	= $orders->count();
        $pp 	= $orders;
        $pp 	= $pp->count();
        $limit 	= $request->length;

		if(@$request->all()) {
			if(@$request->columns['0']['search']['value']){
                $orders = $orders->where(function($q1) use($request) {
                    $q1 = $q1->where('order_no','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_fname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_lname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_phone','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_email','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".$request->columns['0']['search']['value']."%");
                });
            }
            if(@$request->columns['4']['search']['value']){
                $orders = $orders->where('order_type',$request->columns['4']['search']['value']);
            } 
            
            if(@$request->columns['5']['search']['value']){
                $orders = $orders->where('payment_method',$request->columns['5']['search']['value']);
            }
            if(@$request->columns['6']['search']['value']){
                $orders = $orders->where('status', $request->columns['6']['search']['value']);
            }
            if(@$request->columns['15']['search']['value']){
                $orders = $orders->where( function($q4) use ($request){
                   $q4->where('user_id',$request->columns['15']['search']['value']);
                });
            }
            
            $pp = $orders;
            $pp = $pp->count();
            $limit = $request->length;
            
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('order_no','asc');
                } else {
                    $orders = $orders->orderBy('order_no','desc');
                }
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('shipping_fname','asc');
                } else {
                    $orders = $orders->orderBy('shipping_lname','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('created_at','asc');
                } else {
                    $orders = $orders->orderBy('created_at','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('subtotal','asc');
                } else {
                    $orders = $orders->orderBy('subtotal','desc');
                }
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('shipping_price','asc');
                } else {
                    $orders = $orders->orderBy('shipping_price','desc');
                }
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('order_total','asc');
                } else {
                    $orders = $orders->orderBy('order_total','desc');
                }
            }
            if($columnIndex == 6) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('payment_method','asc');
                } else {
                    $orders = $orders->orderBy('payment_method','desc');
                }
            }
            if($columnIndex == 7) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('shipping_city','asc');
                } else {
                    $orders = $orders->orderBy('shipping_city','desc');
                }
            }
            if($columnIndex == 8) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('order_type','asc');
                } else {
                    $orders = $orders->orderBy('order_type','desc');
                }
            }
            
            if($columnIndex == 9) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('delivery_date','asc');
                } else {
                    $orders = $orders->orderBy('delivery_date','desc');
                }
            }
            if($columnIndex == 10) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('delivery_time','asc');
                } else {
                    $orders = $orders->orderBy('delivery_time','desc');
                }
            }
            if($columnIndex == 11) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('status','asc');
                } else {
                    $orders = $orders->orderBy('status','desc');
                }
            }
            $orders = $orders->skip($request->start)->take($request->length)->get();
		}

		$data['aaData'] = $orders;
        $data["draw"] = intval($request->draw);
        $data['iTotalRecords'] = $ps;
        $data["iTotalDisplayRecords"] = $pp;

        return response()->json($data);
	}
	
	/*
	* Method: viewOrderDetails
	* Description: This method is used to view Order Details.
	* Author: Jayatri
	*/
	public function viewOrderDetails($id){
		$this->checkLoginAccess();
		$order = $this->order_master->with([
            'customerDetails.userCountryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'orderMasterDetails.productDetails.productByLanguage',
            'orderMasterDetails.productDetails.productVariants',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails.merchantCityDetails',
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'getBillingCountry',
            'productVariantDetails',
            'getOrderAllImages',
            'orderMasterDetails.productDetails.defaultImage',
            'orderMasterDetails.driverDetails',
            'getCityNameByLanguage',
            'getBillingCityNameByLanguage',
            'getpickupCountryDetails',
            'getpickupCityDetails',
            'getPreferredTime'
        ])
        ->where(['id'=>$id]);
		$order = $order->whereHas('orderMasterDetails', function($q) {
			$q->where(['seller_id'=>Auth::guard('merchant')->user()->id]);
		});
		$order = $order->first();
		$no_of_item = $this->order_details->where(['order_master_id'=>@$id,'seller_id'=>@Auth::guard('merchant')->user()->id])->count();
		$ord_sub_total = $this->order_details->where(['order_master_id'=>@$id,'seller_id'=>@Auth::guard('merchant')->user()->id])->sum('sub_total');
		$ord_total = $this->order_details->where(['order_master_id'=>@$id,'seller_id'=>@Auth::guard('merchant')->user()->id])->sum('total');
		$ord_sel = OrderSeller::where(['order_master_id'=>@$order->id,'seller_id'=>@Auth::guard('merchant')->user()->id])->first();
		return view('merchant.modules.orders.view_order_details')->with([
			'order'=>@$order,
			'total_no_item'=>@$no_of_item,
			'ord_sub_total'=>@$ord_sub_total,
			'ord_total'=>@$ord_total,
			'ord_sel'=>@$ord_sel
		]);
	}
	/*
	* Method: cancelOrder
	* Description: This method is used to cancel Order.
	* Author: Jayatri
	*/
	public function cancelOrder($id){
		$this->checkLoginAccess();
		$order = $this->order_master->where('id',$id)->first();
		if(@$order){
			$update['status'] = 'C';
			$this->order_master->where('id',$id)->update($update);
			$updaten['status'] = 'C';
			$this->order_details->where('order_master_id',$id)->update($updaten);
			// start
			if(@$order->status == 'S')
			if($order->product_variant_id != 0){
                $variant = $this->product_variant->where('id',$order->product_variant_id)->first();
                if($variant->stock_quantity >0){
                    $updatestock['stock_quantity'] = $variant->stock_quantity - $order->quantity;
                    $this->product_variant->where('id',$order->product_variant_id)->update($updatestock);
                    $this->order_details->where('id',$id)->update($update);
                    // session()->flash("success",__('success.-4044'));
                }else{
                    // session()->flash("error",__('errors.-5041'));
                }
            }else{
                if($order->product_id != 0){
                    $s = $this->product->where('id',$order->product_id)->first();
                    $v = $s->stock - $order->quantity;

                    $product_stock['stock'] = $v;
                    $this->product->where('id',$order->product_variant_id)->update($product_stock);
                    $this->order_details->where('id',$id)->update($update);
                } else {
                    $this->order_details->where('id',$id)->update($update);
                }
        	}
			//  end
			session()->flash("success",__('success.-4042'));
		}else{
			session()->flash("error",__('errors.-5038'));
		}
		return redirect()->back();
	}
    
	public function orderStatusChange($id){
		$this->checkLoginAccess();
        $order = $this->order_details->where('id',$id)->first();
        if(@$order) {
            if(@$order->status == 'DA') {
            	$update['status'] = 'RP';
                $this->order_details->where('id',$id)->update($update);

                $totalReady = OrderDetail::where(['order_master_id' => $order->order_master_id, 'status' => 'RP', 'seller_id' => $order->seller_id])->count();
                $totalReady1 = OrderDetail::where(['order_master_id' => $order->order_master_id, 'seller_id' => $order->seller_id])->count();

                # updating order seller table if all item is ready for pickup of this seller.
                if($totalReady1 == $totalReady) {
                    OrderSeller::where(['seller_id' => $order->seller_id, 'order_master_id' => $order->order_master_id])->update(['status' => 'RP']);
                }

                # updating order master table if all seller product is ready for pickup
                $totalReadyMaster = OrderDetail::where(['order_master_id' => $order->order_master_id, 'status' => 'RP'])->count();
                $totalReadyMaster1 = OrderDetail::where(['order_master_id' => $order->order_master_id])->count();

                if($totalReadyMaster == $totalReadyMaster1) {
                    OrderMaster::where(['id' => $order->order_master_id])->update(['status' => 'RP']);
                }
                session()->flash("success",__('success.-4044'));
            }
        } else {
            session()->flash("error",__('errors.-5041'));
        }
        return redirect()->back();
    }

    /**
    *   Method  : checkCity
    *   Use     : check existing in city list.  
    *   Author  : surajit
    */
    public function checkCity(Request $request){
    	$response = array(
    		'jsonrpc' => '2.0'
    	);
    	
    	if($request->get('city')) {
    		$city = $request->get('city');
    		$data = City::where('name', $city)->first();
    		if($data) {
    			$response['status'] = 'SUCCESS';
    			return response()->json($response);    			
    		} else {
    			$response['status'] = 'ERROR';    
    			return response()->json($response);    	
    		}
    	} else {
    		$response['status'] = 'ERROR';    
    		return response()->json($response);    	
    	}
    }

    private function checkLoginAccess(){
		if(@Auth::guard('merchant')->user()){
			return redirect()->route('merchant.login');
		}
	}
	
}
