<?php

namespace App\Http\Controllers\Merchant\Modules\Orders;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Driver;
use App\Merchant;
use App\Models\OrderMaster;
use App\Models\OrderSeller;
use App\Models\OrderDetail;
use App\Models\CountryDetails;
use App\Models\CitiDetails;
class BulkPrintOrder extends Controller
{
    public function __construct(){
        $this->middleware('merchant.auth:merchant');
    }

    /*
    * Method        : printBulkOrder
    * Description   : This method is used to print bulk order
    * Author        : Argha
    * Date          : 2020-10-09
    */
    public function printBulkOrder($ids) {
        $ids = json_decode(base64_decode($ids));
        $driver = Driver::where(['status'=>'A'])->get();
        // dd(\Auth::guard('merchant')->user());
        $order = OrderMaster::with([
            'customerDetails.userCountryDetails.countryDetailsBylanguage',
            'orderMasterDetails' => function($q) {
                $q->where('seller_id', \Auth::guard('merchant')->user()->id);
            },
            'orderMasterDetails.productDetails.productByLanguage',
            'orderMasterDetails.productDetails.productVariants',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails.merchantCityDetails',
            'orderMasterDetails.productDetails.defaultImage',
            'orderMasterDetails.driverDetails',
            'orderSellerInfo' => function($q) {
                $q->where('seller_id', \Auth::guard('merchant')->user()->id);
            },
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'getBillingCountry',
            'productVariantDetails',
            'getOrderAllImages',
            'orderMasterExtDetails.sellerDetails',
            'orderMasterExtDetails.driverDetails',
            'getCityNameByLanguage',
            'getBillingCityNameByLanguage'
        ])->whereIn('id', $ids)->get();

        if(@$order){
            return view('merchant.modules.orders.print_bulk_orders')->with([
                'orders'        => @$order,
                'total_no_item' => @$no_of_item,
                'ord_sub_total' => @$ord_sub_total,
                'ord_total'     => @$ord_total,
                'ord_sel'       => @$ord_sel,
                'drivers'       => @$driver
            ]);
        } else {
          session()->flash("error",__('errors.-5002'));
          return redirect()->back();
        }
    }
}
