<?php

namespace App\Http\Controllers\Merchant\Modules\Invoice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\InvoiceMaster;
use App\Models\InvoiceDetail;
use App\Merchant;
use Auth;
class MerchantInvoiceController extends Controller
{
    /*
    * Method: invoiceList
    * Description: This method is used show list of merchant invoice list
    * Author : Argha
    * Date   : 2020-11-04
    */
    public function invoiceList(Request $request)
    {  
        $invoices = InvoiceMaster::with(['getMerchant','getInvoiceDetails'])
                                    ->where('merchant_id',Auth::guard('merchant')->user()->id)
                                    ->orderBy('id','desc');
        $pp = $invoices ;
        $pp = $pp->count();
        if($request->all()){
            //dd($request->all());
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc
            $searchValue = $request['search']['value']; // search value

            if($searchValue){
                $invoices = $invoices->where(function($query) use($searchValue){
                    $query->Where('invoice_no','like','%'.@$searchValue.'%')
                          ->orWhere('name','like','%'.@$searchValue.'%')
                          ->orWhere('total','like','%'.@$searchValue.'%');
                });
            }
             //sorting
             if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $invoices = $invoices->orderBy('invoice_no','asc');
                } else {
                    $invoices = $invoices->orderBy('invoice_no','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $invoices = $invoices->orderBy('invoice_type','asc');
                } else {
                    $invoices = $invoices->orderBy('invoice_type','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $invoices = $invoices->orderBy('applicable_to','asc');
                } else {
                    $invoices = $invoices->orderBy('applicable_to','desc');
                }
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $invoices = $invoices->orderBy('total','asc');
                } else {
                    $invoices = $invoices->orderBy('total','desc');
                }
            }
            
            $ppp = $invoices;
            $ppp = $invoices->count();
            $invoices = $invoices->skip($request->start)->take($request->length)->get();
            $data['aaData'] = $invoices;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $pp;
            $data["iTotalDisplayRecords"] = $ppp;
            return response()->json($data);
        }else{
            return view('merchant.modules.invoice.invoice_list');
        }
        
    }
    /*
    * Method: viewInvoice
    * Description: This method is used show invoice details page
    * Author : Argha
    * Date   : 2020-11-04
    */
    public function viewInvoice($id)
    {
        $data['invoice'] = InvoiceMaster::with(['getMerchant','getInvoiceDetails'])->where('id',$id)->first();
        return view('merchant.modules.invoice.view_invoice')->with($data);
    }

     /*
    * Method: printInvoiceDetails
    * Description: This method is used for single print for invoice
    * Author : Argha
    * Date   : 2020-11-04
    */
    public function printInvoiceDetails($id)
    {
        $data['invoice'] = InvoiceMaster::with(['getMerchant','getInvoiceDetails'])->where('id',$id)->first();
        return view('merchant.modules.invoice.print_invoice_details')->with($data);
    }

    /*
    * Method: printBulkInvoice
    * Description: This method is used for bulk print for invoice
    * Author : Argha
    * Date   : 2020-11-04
    */
    public function printBulkInvoice($ids)
    {
        $ids = json_decode(base64_decode($ids));
        $data['invoices'] = InvoiceMaster::with(['getMerchant','getInvoiceDetails'])
                                        ->whereIN('id',$ids)
                                        ->orderBy('id','desc')
                                        ->get();
        return view('merchant.modules.invoice.print_bulk_invoices')->with($data);
    }
}
