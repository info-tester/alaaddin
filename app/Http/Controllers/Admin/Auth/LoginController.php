<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cookie;
use App\Models\Admin_activities;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.guest:admin', ['except' => 'logout']);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        // return view('admin.auth.login');
        try {
            if(Cookie::get('email') != null && Cookie::get('password') != null && Cookie::get('amdin_remember') != null) {
                $data['email'] = Cookie::get('amdin_email');
                $data['password'] = Cookie::get('amdin_password');
                $data['remember'] = Cookie::get('amdin_remember');
            }
            return view('admin.auth.login')->with(@$data);
        } catch (Exception $e) {
            abort(404);
        }
    }
    protected function authenticated(Request $request, $admin)
    {
        if((@$admin) && @$request->remember == 'on') {
            // check remember me cookie have or not
            // set remember me cookie
            $email_cookie = Cookie::queue('amdin_email', $request->email, 10080);
            $pass_cookie = Cookie::queue('amdin_password', $request->password, 10080);
            $remember_coockie = Cookie::queue('amdin_remember', $request->remember, 10080); 
        }
        else {
            Cookie::queue(Cookie::forget('amdin_remember'));
            Cookie::queue(Cookie::forget('amdin_email'));
            Cookie::queue(Cookie::forget('amdin_password'));
        }
    }
    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);

    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');
        $credentials['status'] = 'A';

        return $credentials;
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        $this->insertLoginInfo('LI');
        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];
        $user = \App\Merchant::where($this->username(), $request->{$this->username()})->first();
        // Check if user was successfully loaded, that the password matches
        // and active is not 1. If so, override the default error message.
        if ($user && \Hash::check($request->password, $user->password) && $user->status == 'I') {
            $errors = [$this->username() => 'Your account is not active.'];
        }
        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        // dd($errors);
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->insertLoginInfo('LO');
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->route('admin.login');
    }

    /*
    * Method        : insertLoginInfo
    * Description   : This method is insert sub admin login information
    * Author        : Argha
    * Date 			: 2020-Aug-28
    */
    public function insertLoginInfo($type)
    {
        // Insert subadmin login information
            $location = '';
            $device = '';
            // browser
            if(\Browser::isDesktop() == 'true'){
                $device = 'D';
            }else{
                $device = 'M';
            }
            // location
            $ip = \Request::ip();
            //$ip = '43.239.80.27';
            $data = \Location::get($ip);
            if($data){
                $country = @$data->countryName ? $data->countryName.',':'';
                $region = @$data->regionName ? $data->regionName .',':'';
                $city   = @$data->cityName ? $data->cityName : '';
                $location = $country.$region.$city ;
            }else{
                $location = 'Localhost';
            }
            $addLoginInfo['admin_id'] = $this->guard()->user()->id;
            $addLoginInfo['ip'] = \Request::ip();
            $addLoginInfo['login_location'] = $location;
            $addLoginInfo['device'] = $device;
            $addLoginInfo['type'] = $type;
            $addLoginInfo['browser'] = \Browser::browserName();

            Admin_activities::create($addLoginInfo);
    }

}
