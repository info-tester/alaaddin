<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OrderMaster;
use App\Models\OrderDetail;
use App\Merchant;
use App\Admin;
use App\User;
use Session;
use Config;
use Auth;
use Carbon\Carbon;

class HomeController extends Controller
{

    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /**
     * Show the Admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function productSalesGraph(){
        $orderTotalMonth = OrderMaster::select('id','order_total', \DB::raw("DATE_FORMAT(updated_at, '%m-%Y') new_date"),  \DB::raw('YEAR(updated_at) year, MONTH(updated_at) month'))
        ->groupby('year','month')
        ->first();
        return $orderTotalMonth->order_total;
    }

    public function index() {
        $totSales = OrderMaster::where(['status'=>'OD','order_type'=>'I'])->sum('order_total');
        $sales1 = OrderMaster::where(['status'=>'OD','order_type'=>'I'])->sum('shipping_price');
        $data['sales'] = $totSales - $sales1;

        // Sales for this month
        $totSales_month = OrderMaster::where(['status'=>'OD','order_type'=>'I'])->whereMonth('created_at',Carbon::now()->month)->sum('order_total');
        $sales_month = OrderMaster::where(['status'=>'OD','order_type'=>'I'])->whereMonth('created_at',Carbon::now()->month)->sum('shipping_price');
        $data['sales_month'] = $totSales_month - $sales_month;

        $data['totalOrders'] = OrderMaster::whereNotIn('status', ['I','OC','D'])->count();
        // Total order this month
        $data['totalOrdersMonth'] = OrderMaster::whereNotIn('status', ['I','OC','D'])->whereMonth('created_at',Carbon::now()->month)->count();
        
        $data['totalCustomers'] = User::where('status', 'A')->count();
        // Total customer this month
        $data['totalCustomersMonth'] = User::where('status', 'A')->whereMonth('created_at',Carbon::now()->month)->count();

        $data['totalMerchants'] = Merchant::whereIn('status', ['A','AA','U'])->count();
        // Total merchant this month
        $data['totalMerchantsMonth'] = Merchant::whereIn('status', ['A','AA','U'])->whereMonth('created_at',Carbon::now()->month)->count();

        $data['orders'] = OrderMaster::whereNotIn('status', ['I','D',''])->with(['customerDetails','orderMasterDetails.driverDetails','countryDetails','orderMasterDetails','orderMasterDetails.productDetails','orderMasterDetails.productVariantDetails','orderMasterDetails.sellerDetails','countryDetails','shippingAddress','billingAddress','getBillingCountry','productVariantDetails'])->orderBy('id','desc')->take(5)->get();
        $data['total_earnings'] = OrderMaster::where(['status'=>'OD'])->sum('shipping_price') + OrderMaster::where(['status'=>'OD'])->sum('total_commission');

        $data['deliveryfee'] = OrderMaster::where(['status'=>'OD'])->sum('shipping_price');
        // Total delivery fee this month
        $data['deliveryfeeMonth'] = OrderMaster::where(['status'=>'OD'])->whereMonth('created_at',Carbon::now()->month)->sum('shipping_price');

        $data['com'] = OrderMaster::where(['status'=>'OD'])->sum('total_commission');
        // Total commission this month
        $data['commissionThisMonth'] = OrderMaster::where(['status'=>'OD'])->whereMonth('created_at',Carbon::now()->month)->sum('total_commission');
        
        $data['exto'] = OrderMaster::where(['status'=>'OD','order_type'=>'E'])->count();
        // Total external order this month
        $data['externalThisMonth'] = OrderMaster::where(['status'=>'OD','order_type'=>'E'])->whereMonth('created_at',Carbon::now()->month)->count();

        $data['intero'] = OrderMaster::where(['status'=>'OD','order_type'=>'I'])->count();
        // Total internal order this month
        $data['internalThisMonth'] = OrderMaster::where(['status'=>'OD','order_type'=>'I'])->whereMonth('created_at',Carbon::now()->month)->count();

        $data['android_order'] = OrderMaster::where(['status'=>'OD','order_type'=>'I', 'order_from' => 'A'])->count();
        // Total android order this month
        $data['android_order_this_month'] = OrderMaster::where(['status'=>'OD','order_type'=>'I', 'order_from' => 'A'])->whereMonth('created_at',Carbon::now()->month)->count();

        $data['ios_order'] = OrderMaster::where(['status'=>'OD','order_type'=>'I', 'order_from' => 'I'])->count();
        // Total ios order this month
        $data['ios_order_this_month'] = OrderMaster::where(['status'=>'OD','order_type'=>'I', 'order_from' => 'I'])->whereMonth('created_at',Carbon::now()->month)->count();

        $data['web_order'] = OrderMaster::where(['status'=>'OD','order_type'=>'I', 'order_from' => 'W'])->count();
        // Total web order this month
        $data['web_order_this_month'] = OrderMaster::where(['status'=>'OD','order_type'=>'I', 'order_from' => 'W'])->whereMonth('created_at',Carbon::now()->month)->count();

        $data['customer'] = User::where('user_type', 'C')->where('status', '!=', 'D')->take(5)->get();
        
        #total external order value without shipping cost
        $data['totalExternalValue'] = OrderMaster::select('id',\DB::raw('sum(`order_total`) - sum(`shipping_price`) order_total'), \DB::raw("DATE_FORMAT(updated_at, '%m-%Y') new_date"),  \DB::raw('YEAR(updated_at) year, MONTH(updated_at) month'));
        
        $data['totalExternalValue'] = $data['totalExternalValue']->where(function($q){
            $q->where(['status'=>'OD','order_type'=>'E']);
        });

        $data['totalExternalValue'] = $data['totalExternalValue']->groupby('month')->orderBy('month','asc')->get();

        #code for dashboard graph
        #code for graph of product sales for order total for each month in admin dashboard graph (1st graph)
        $data['orderTotalMonth'] = OrderMaster::select('id',\DB::raw('sum(`order_total`) - sum(`shipping_price`) order_total'), \DB::raw("DATE_FORMAT(updated_at, '%m-%Y') new_date"),  \DB::raw('YEAR(updated_at) year, MONTH(updated_at) month'));
        
        $data['orderTotalMonth'] = $data['orderTotalMonth']->where(function($q){
            $q->where('status', 'OD');
            $q->where('order_type', 'I');
        });

        $data['orderTotalMonth'] = $data['orderTotalMonth']->groupby('month')->orderBy('month','asc')->get();

        $productSales = [
            'month'     => [],
            'amount'    => []
        ];
        $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        for($i=0; $i<12; $i++) {
            $productSales['months'][] = $months[$i];
            $productSales['amount'][] = 0;
        }
        foreach(@$data['orderTotalMonth'] as $totalM){
            $productSales['amount'][$totalM->month - 1] = $totalM->order_total;
        }
        $data['product_sales'] = $productSales;

        #code for product categories with most sales top 5 category
        $data['topSalesProductCategory'] = \DB::table('order_details')
        ->join('product_categories', 'order_details.product_id', '=', 'product_categories.product_id')
        ->join('category_details', 'category_details.category_id', '=', 'product_categories.category_id')
        ->join('order_master', 'order_master.id', '=', 'order_details.order_master_id')
        ->where('order_details.product_id','!=',0)
        ->whereNotIn('order_master.status',['I','F','D','OC'])
        ->where('category_details.language_id',getLanguage()->id)
        ->where('product_categories.level','=','P')
        ->select('product_categories.category_id','category_details.title',\DB::raw('sum(`total`) category_total'))
        ->groupBy('product_categories.category_id')
        ->orderBy('category_total','desc')
        ->take(5)
        ->get();
        
        $data['totalSumCategories'] = 0;
        foreach(@$data['topSalesProductCategory'] as $totCategories){
            $data['totalSumCategories'] = $data['totalSumCategories'] + @$totCategories->category_total;
        }
        $data['categoryNames'][0] = "";
        $data['sum_value'][0] = 0;
        foreach(@$data['topSalesProductCategory'] as $key=>$categoryName){
            if(@$data['totalSumCategories'] > 0.000){
                $data['percentage'][$key] = number_format((@$categoryName->category_total/@$data['totalSumCategories'])*100,2);
            }else{
                $data['percentage'][$key] = 0.00;
            }
            $data['categoryNames'][$key] = @$categoryName->title." - ".@$data['percentage'][$key]." %";
            $data['sum_value'][$key] = @$categoryName->category_total;
        }

        #code for customer graph
        $data['customerGraph'] = User::where('status','A')->select('id','status','created_at',\DB::raw('YEAR(created_at) year, Day(created_at) day'),\DB::raw('Count(id) totalCustomer'))->whereDate('created_at', '>', Carbon::now()->subDays(30))->groupBy('day')->orderBy('day','asc')->get();
        // dd($data['customerGraph']);
        $data['noCustomer'] = [];
        for($i=0;$i<30;$i++){
            $data['day'][$i] = $i+1;
            $data['RegistrationDay'][$i+1] = $i+1;
            $data['noCustomer'][] = 0;
        }
        
        $data['highestCustomer'] = User::where('status','A')->select('id','created_at',\DB::raw('YEAR(created_at) year, Day(created_at) day'),\DB::raw('Count(id) totalCustomer'))->whereDate('created_at', '>', Carbon::now()->subDays(30))->groupBy('day')->orderBy('totalCustomer','desc')->first();

        if(@$data['highestCustomer']){
            $data['totalCustHighest'] = $data['highestCustomer']->totalCustomer;
        }else{
            $data['totalCustHighest'] = 0.000;
        }

        foreach(@$data['customerGraph'] as $cust){
            if(@$cust->day != 31){
                $custDay = @$cust->day;
                $data['RegistrationDay'][$custDay - 1] = @$cust->day;
                $data['noCustomer'][$custDay - 1] = @$cust->totalCustomer;
            }
        }
        $data['admin_will_get'] = OrderMaster::where(['status'=>'OD'])->sum('admin_commission')+OrderMaster::where(['status'=>'OD'])->sum('insurance_price');
        if(Auth::guard('admin')->user()->type == "A") {
            return view('admin.modules.dashboard.home', $data);
        }elseif(Auth::guard('admin')->user()->type == "S") {
            $subAdmin = Admin::with('sidebarAccess','sidebarAccess.menuDetails')->where('id',Auth::guard('admin')->user()->id)->first();
            $sidebarAccess = [];
            foreach ($subAdmin->sidebarAccess as $value) {
                $sidebarAccess[] = $value->menu_id;
            }
            if(in_array('8', @$sidebarAccess)) {
                if(in_array('6', @$sidebarAccess)) {
                    $data['accessDenied6'] = '6';
                }
                if(in_array('3', @$sidebarAccess)) {
                    $data['accessDenied3'] = '3';
                }
                $data['sidebarAccess'] = @$sidebarAccess;
            } else {
                $data['accessDenied'] = '1';
            }
        }
        
        
        return view('admin.modules.dashboard.home', $data);
    }


    /**
    * Method: changeLanguage
    * Description: This method is used to change language
    */
    public function changeLanguage() {

        $lang = '';
        if(Session::get('applocale') == 'ar') {
            $lang = 'en';
        } else {
            $lang = 'ar';
        }
        if (array_key_exists($lang, Config::get('languages'))) {            
            Session::put('applocale', $lang);
            // dd($lang, Config::get('languages'));
        }
        // dd(Session::get('applocale'));
        return redirect()->back();
    }

}