<?php

namespace App\Http\Controllers\Admin\Modules\UnitMaster;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Models\UnitMaster;

class UnitMasterController extends Controller
{
    
    public function index(Request $request)
    {
        if(@$request->all()){
            
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc
            $cc = UnitMaster::count(); // total customer before filtering
            $ccc = UnitMaster::count(); // total customers before paginate
            $unit = array();
            if(@$request->columns['1']['search']['value']){
                $unit = $unit->where(function($query) use($request){
                    $query = $query->where('unit_name','LIKE',"%".@$request->columns['1']['search']['value']."%");
                });
            }
            // sorting
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $unit = UnitMaster::orderBy('unit_name','asc');
                } else {
                    $unit = UnitMaster::orderBy('unit_name','desc');
                }
                $unit = $unit->skip($request->start)->take($request->length)->get();
            }elseif($columnIndex == 1){
                if($columnSortOrder == 'asc') {
                    $unit = UnitMaster::orderBy('unit_name','asc');
                } else {
                    $unit = UnitMaster::orderBy('unit_name','desc');
                }
                $unit = $unit->skip($request->start)->take($request->length)->get();
            }else{
                $unit = UnitMaster::skip($request->start)->take($request->length)->get();
            }
            $data['aaData'] = $unit;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $cc;
            $data["iTotalDisplayRecords"] = $ccc;
            return response()->json($data);
        }else{
            $unit = UnitMaster::orderBy('id','desc')->get();
            return view('admin.modules.unit_master.unit_master');
        }
    }
    public function editUnit($id,Request $request){
        if(@$request->all()){
            $ucheck = UnitMaster::where(['unit_name'=>$request->unit_name])->whereNotIn('id',[$id])->whereIn('status',['A','I'])->first();
            if(@$ucheck){
                return redirect()->back()->with("error","This name is already exist");
            }
            $unit = UnitMaster::where(['id'=>$id])->first();
            if($unit){
                $upd['unit_name'] = $request->unit_name;
                UnitMaster::where(['id'=>$id])->update($upd);
                return redirect()->back()->with("success","Unit updated successfully!");    
            }else{
                
                return redirect()->back()->with("error","Error!Unit is not updated because gievn id is not associated with unit!");    
            }
            
        }else{
            $data['unit'] = UnitMaster::where(['id'=>$id])->first();
            return view('admin.modules.unit_master.edit_unit_master',$data);
        }
    }
    public function addUnit(Request $request){
        if(@$request->all()){
            // check duplicate name
            $ucheck = UnitMaster::where(['unit_name'=>$request->unit_name])->whereIn('status',['A','I'])->first();
            if(@$ucheck){
                return redirect()->back()->with("error","This name is already exist");
            }
            $upd['unit_name'] = $request->unit_name;
            $upd['weight_in_ton'] = $request->weight_in_ton;
            $u = UnitMaster::create($upd);
            return redirect()->back()->with("success","Unit created successfully!");
        }else{
            return view('admin.modules.unit_master.add_unit_master');
        }
    }
}
