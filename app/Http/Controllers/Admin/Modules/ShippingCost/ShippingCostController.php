<?php

namespace App\Http\Controllers\Admin\Modules\ShippingCost;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\Customer;
use Validator;
use Auth;
use App\Models\Language;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Repositories\ShippingCostRepository;
use App\Repositories\SettingRepository;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;

class ShippingCostController extends Controller
{
    protected $shipping_cost,$setting;
    public function __construct(ShippingCostRepository $shipping_cost,
        SettingRepository $setting,
        Request $request
    )
    {
        $this->middleware('admin.auth:admin');
        $this->checkLoginAccess();
        $this->shipping_cost            =   $shipping_cost;
        $this->setting            =   $setting;
        // dd(auth::guard('admin')->user()->type);
    }
    /*
    * Method        : addShippingCost
    * Description   : This method is used to add Shipping Cost.
    * Author        : Jayatri
    * Date 			: 10/02/2020
    */
    public function addShippingCost(Request $request){
        $this->checkLoginAccess();
        $this->checkAccess();
        if(@$request->all()){
            $request->validate([
                'from_weight'=>'required',
                // 'to_weight'=>'required',
                'internal_order_rate'=>'required',
                'external_order_rate'=>'required'
            ],[
                'from_weight'=>'Please provide from weight with 3 decimal places!',
                // 'to_weight'=>'Please provide from weight with 3 decimal places!',
                'internal_order_rate'=>'Please provide internal order rate with 3 decimal places!',
                'external_order_rate'=>'Please provide external order rate with 3 decimal places!'
            ]);
            $new['from_weight'] 			= @$request->from_weight;
            $new['internal_order_rate'] 	= @$request->internal_order_rate;
            $new['external_order_rate'] 	= @$request->external_order_rate;
            $new['status'] 					= 'A';
            if(@$request->infinity_weight) {
                $new['infinity_weight']         = 'Y';
            } else {
                $new['to_weight']               = @$request->to_weight;
                $new['infinity_weight']         = 'N';
            }

            $scost = $this->shipping_cost->create($new);
            if(@$scost){
                session()->flash("success",__('success.-4038'));
            }else{
                session()->flash("error",__('errors.-5035'));
            }
            return redirect()->route('admin.list.shipping.cost');
        }
        return view('admin.modules.shipping_cost.add_shipping_cost');
    }
    /*
    * Method        : editShippingCost
    * Description   : This method is used to edit Shipping Cost.
    * Author        : Jayatri
    * Date 			: 10/02/2020
    */
    public function editShippingCost(Request $request,$id){
        $this->checkLoginAccess();
        $this->checkAccess();
        $shipping_cost = $this->shipping_cost->where('id',$id)->first();

        if(@$shipping_cost){
          if(@$request->all()){
    			// dd($request->all());
            $new['from_weight'] 			= @$request->from_weight;
            $new['internal_order_rate'] 	= @$request->internal_order_rate;
            $new['external_order_rate'] 	= @$request->external_order_rate;
            $new['status'] 					= 'A';
            if(@$request->infinity_weight) {
                $new['to_weight']               = @$request->to_weight;
                $new['infinity_weight']         = 'Y';
            } else {
                $new['to_weight']               = @$request->to_weight;
                $new['infinity_weight']         = 'N';
            }

            $this->shipping_cost->where('id',$id)->update($new);
            session()->flash("success",__('success.-4039'));
            return redirect()->route('admin.list.shipping.cost');
        }else{
         return view('admin.modules.shipping_cost.edit_shipping_cost')->with(['shipping_cost'=>@$shipping_cost]);
     }
 }else{
  session()->flash("error",__('errors.-5002'));
  return redirect()->route('admin.list.shipping.cost');
}
}
    /*
    * Method        : listShippingCost
    * Description   : This method is used to list Shipping Cost.
    * Author        : Jayatri
    * Date 			: 10/02/2020
    */
    public function listShippingCost(Request $request){
        $this->checkLoginAccess();
        $this->checkAccess();
        $shipping_costs = $this->shipping_cost->get();
        return view('admin.modules.shipping_cost.list_shipping_cost')->with(['shipping_costs'=>@$shipping_costs]);
    }
    /*
    * Method        : updateShippingSettings
    * Description   : This method is used to update Shipping Settings.
    * Author        : Jayatri
    * Date 			: 10/02/2020
    */
    public function updateShippingSettings(Request $request){
        $this->checkLoginAccess();
        $this->checkAccess();
        $setting = $this->setting->first();
        if(@$setting){
          if(@$request->all()){
             $update['enable_international_order'] 	= @$request->international_order;
             $update['order_shipping_cost'] 			= @$request->internal_order_cost;
             $this->setting->where('id',$setting->id)->update($update);
             session()->flash("success",__('success.-4041'));
             return redirect()->back();
         }
     }else{
      session()->flash("error",__('errors.-5002'));
      return redirect()->route('admin.list.shipping.cost');
  }

  return view('admin.modules.shipping_cost.shipping_setting_page')->with(['setting'=>@$setting]);
}
    /*
    * Method        : updateShippingSettings
    * Description   : This method is used to update Shipping Settings.
    * Author        : Jayatri
    * Date 			: 10/02/2020
    */
    public function checkDuplicateRange(Request $request){
        // dd($request->all());
        $this->checkLoginAccess();
        $this->checkAccess();
        if(@$request->all()){

            if(@$request->old_id){
                $chk_range = $this->shipping_cost->where('id','!=',$request->old_id);
            }else{
                $chk_range = $this->shipping_cost->where('id','!=',0);
            }

            if(@$request->to_weight != 0) {
                if(@$request->old_id){
                    $chk_infinity = $this->shipping_cost->where('id','!=',$request->old_id)->where('infinity_weight', 'Y')->first();
                }else{
                    $chk_infinity = $this->shipping_cost->where('infinity_weight', 'Y')->first();
                }
                if($chk_infinity && $chk_infinity->from_weight < @$request->to_weight) {
                    $chk_range = $chk_infinity;
                } else {

                    $chk_range = $chk_range->where(function($where) use ($request) {
                        $where->where(function($where1) use ($request) {
                            $frm_tm = @$request->from_weight;
                            $to_tm = @$request->to_weight;

                            $where1->where('from_weight', '<', $frm_tm)
                            ->where('to_weight', '>=', $frm_tm);

                        })
                        ->orWhere(function($where2) use ($request) {
                            $frm_tm = @$request->from_weight;
                            $to_tm = @$request->to_weight;

                            $where2->where('from_weight', '<=', $to_tm)
                            ->where('to_weight', '>', $to_tm);
                        });

                    })->first();
                }
            } else {
                if(@$request->old_id){
                    $chk_infinity = $this->shipping_cost->where('id','!=',$request->old_id)->where('infinity_weight', 'Y')->first();
                }else{
                    $chk_infinity = $this->shipping_cost->where('infinity_weight', 'Y')->first();
                }
                if($chk_infinity) {
                    $chk_range = $chk_infinity;
                } else {
                    $chk_range = $chk_range->where(function($where) use ($request) {
                        $where->where(function($where1) use ($request) {
                            $frm_tm = @$request->from_weight;
                            $where1->where('from_weight', '<=', $frm_tm)
                            ->where('to_weight', '>=', $frm_tm);
                        });
                    })->first();

                    // dd($chk_range);
                }

            }
            if(@$chk_range){
            	return 0;
            }else{
            	return 1;	
            }
        }else{
          return 0;
      }
  }


    /*
    * Method        : deleteShippingCost
    * Description   : This method is used to delete Shipping Cost.
    * Author        : Jayatri
    * Date 			: 10/02/2020
    */
    public function deleteShippingCost($id){
        $this->checkLoginAccess();
        $this->checkAccess();
        $shipping_cost = $this->shipping_cost->where('id',$id)->first();
        if(@$shipping_cost){
          $this->shipping_cost->where('id',$id)->delete();
          session()->flash("success",__('success.-4040'));
          return redirect()->back();
        }else{
            session()->flash("error",__('errors.-5037'));
            return redirect()->back();
        }
    }
    /*
    * Method        : deleteCost
    * Description   : This method is used to delete Shipping Cost.
    * Author        : Jayatri
    * Date          : 10/02/2020
    */
    public function deleteCost(Request $request){
        // $this->checkLoginAccess();
        // $this->checkAccess();
        $id = @$request->id;
        $shipping_cost = $this->shipping_cost->where('id',$id)->first();
        if(@$shipping_cost){
          $this->shipping_cost->where('id',$id)->delete();
          // session()->flash("success",__('success.-4040'));
          // return redirect()->back();
          return 1;
        }else{
            // session()->flash("error",__('errors.-5037'));
            // return redirect()->back();
            return 0;
        }
    }
  private function checkAccess(){
    $this->checkLoginAccess();
    if(Auth::guard('admin')->user()->type == "A"){

    }else{
        $this->middleware('admin.auth:admin');
        $this->middleware(function ($request, $next) {
            $this->admin = Admin::with('sidebarAccess.menuDetails')
            ->where('id', auth::guard('admin')->user()->id)
            ->first();
            $permission = array(); 
            if($this->admin->sidebarAccess !=null)
            {
                foreach($this->admin->sidebarAccess as $row)
                {
                    array_push($permission, @$row->menu_id); 
                }

            }  
                //dd($permission);
            if(in_array(9, $permission))
            {
                return $next($request);
            }
            else
            {
                return redirect()->back();
            }
        });  
    }
}
private function checkLoginAccess(){
    if(!@Auth::guard('admin')->user()){
        return redirect()->route('admin.login');
    }
}
}
