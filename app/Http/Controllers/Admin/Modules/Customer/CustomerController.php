<?php

namespace App\Http\Controllers\Admin\Modules\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\User;
use Validator;
use Auth;
use App\Models\Language;
use App\Models\Admin;
use App\Models\CustomerWithdraw;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Repositories\UserRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\CountryRepository;
use App\Repositories\ConversationsRepository;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;

class CustomerController extends Controller
{
    protected $customers,$language,$countries;
    public function __construct(UserRepository $customers,
                                LanguageRepository $language,
                                ConversationsRepository $conversations,
                                CountryRepository $countries,Request $request
                            )
    {
        $this->middleware('admin.auth:admin');
        $this->checkLoginAccess();
        $this->customers            =   $customers;
        $this->language             =   $language;
        $this->countries            =   $countries;
        $this->conversations        =   $conversations;
    }

    /*
    * Method        : addCustomer
    * Description   : This method is used to add customer
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function addCustomer(Request $request){
		if(@$request->all()){
    		$request->validate([
                'fname'                         =>'required',
                'lname'                         =>'required',
                'email'                         =>'required|email',
                'phone'                         =>'required',
                'profile_pic'                   =>'mimes:jpeg,jpg,png'
            ],[
                'fname.required'                =>'First name field is required',
                'lname.required'                =>'Last name field is required',
                'email.required'                =>'Email field is required',
                'phone.required'                =>'Phone field is required',
                'profile_pic.mimes'             =>'Please upload picture with extension jpg/jpeg/png'
            ]);
            
            $new['fname']               = @$request->fname;
            $new['lname']               = @$request->lname;
            $new['email']               = @$request->email;
            $new['phone']               = @$request->phone;
            $new['tag']        	        = @$request->tag;
            $new['email_vcode']         = rand(100000,999999);
            $new['signup_from']         = 'S';
            $new['status']              = 'A';
            if(@$request->profile_pic){
                $pic   = $request->profile_pic;
                $name  = time().'.'.$pic->getClientOriginalExtension();
                $pic->move('storage/app/public/customer/profile_pics/', $name);
                $new['image'] = $name;
            }
            $customer = $this->customers->create($new);
            session()->flash("success",__('success.-4029'));
    		return redirect()->route('admin.list.customer');
    	}
    	return view('admin.modules.customers.add_customer');
    }
    public function ajaxDuplicateEmailCustomerCheck(Request $request){
     try{
         $email = trim($request->email);
         $customer = User::where('email',@$request->email)->whereIn('status',['I','A','U'])->whereNotIn('id',[$request->user_id])->first();
         if(@$customer){
             return 'false';
         }else{
             return 'true';
         }
     }catch(\Exception $e){
         \Log::error($e->getMessage());
         return 'true';
     }
   }
   public function ajaxDuplicatePhoneCustomerCheck(Request $request){
     try{
         $phone = trim($request->phone);
         $customer = User::where('phone',@$request->phone)->whereIn('status',['I','A','U'])->whereNotIn('id',[$request->user_id])->first();
         if(@$customer){
             return 'false';
         }else{
             return 'true';
         }
     }catch(\Exception $e){
         \Log::error($e->getMessage());
         return 'true';
     }
   }
    /*
    * Method        : viewCustomerProfile
    * Description   : This method is used to view customer profile.
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function viewCustomerProfile(Request $request,$id){
    	$customer = $this->customers->with('customerOrders')->where('id',@$id)->first();
        $customerwithdraws = CustomerWithdraw::where(['buyer_id'=>$id])->get();
    	if(@$customer){
    		return view('admin.modules.customers.view_customer_profile')->with(['customer'=>@$customer,'customerwithdraws'=>$customerwithdraws]);
    	}else{
    		return redirect()->back();
    	}
    }
    /*
    * Method        : addCustomer
    * Description   : This method is used to add customer
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */

    public function editCustomer(Request $request,$id){
    	$customer = User::where('id',$id)->first();
        $countries = Country::with('countryDetailsBylanguage')->where('status','!=','D')->get();
    	if(@$customer){
    		if(@$request->all()){
    			$request->validate([
                'fname'                         =>'required|string|max:255',
                'lname'                         =>'required|string|max:255',
                'phone'                         =>'required|numeric|digits_between:10,12',
                'email'                         =>'required|email|regex:/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/'
	            ],
	            [
	                'fname.required'                =>'First name field is required',
                    'fname.string'                =>'First name should be string',
	                'lname.required'                =>'Last name field is required',
	                'phone.required'                =>'Phone field is required',
                    'phone.numeric'                 =>'Phone number should be number',
                    'phone.digits_between'          =>'Phone number should be 10 digits to 12 digits',
	                'email.required'                =>'Email field is required',
                    'email.email'                   =>'Please provide valid email-id',
                    'email.regex'                   =>'Please provide valid email-id'
	            ]);
                $customer = User::where(['email'=>$request->email])->whereNotIn('id',[$id])->whereIn('status',['I','A','U'])->first();
                if(@$customer){
                    return redirect()->back()->with("error",__('errors.-33000'));
                }
                $customer = User::where(['phone'=>$request->phone])->whereNotIn('id',[$id])->whereIn('status',['I','A','U'])->first();
                if(@$customer){
                    return redirect()->back()->with("error",ERRORS['-33088']);
                }
    			if(@$request->fname){
    				$new['fname']               = @$request->fname;
    			}
    			if(@$request->lname){
    				$new['lname']               = @$request->lname;
    			}
                if(@$request->phone){
                    $new['phone']               = @$request->phone;
                }
    			if(@$request->email){
    				$new['email']        	    = @$request->email;
                }
                if(@$request->tag){
    				$new['tag']        	        = @$request->tag;
    			}
                if(@$request->country){
                    $new['country']                 = @$request->country;
                }
                if(@$request->state){
                    $new['state']                 = @$request->state;
                }
                if(@$request->city){
                    $new['city']                 = @$request->city;
                }
                if(@$request->zip){
                    $new['zipcode']                 = @$request->zip;
                }
                if(@$request->landmark){
                    $new['landmark']                 = @$request->landmark;
                }
                if(@$request->more_address){
                    $new['address']                 = @$request->more_address;
                }

	            if(@$request->profile_pic){
	                $pic   = $request->profile_pic;
	                $name  = time().'.'.$pic->getClientOriginalExtension();
	                $pic->move('storage/app/public/customer/profile_pics/', $name);
	                $new['image'] = $name;
	            }
                if(@$request->nw_password){
                    $password = @$request->nw_password;
                    $new['password'] = \Hash::make($password);   
                }
            	User::where('id',$id)->update($new);
            	if(@$request->email){
            		session()->flash("success",__('success.-4030'));
    			}
	    		return redirect()->route('admin.list.customer');
    		}else{
    			return view('admin.modules.customers.edit_customer')->with(['customer'=>@$customer,'countries'=>@$countries,]);
    		}
    	} else{

    		return redirect()->back()->with("error",__('errors.-5100'));
    	}
    }
    /*
    * Method        : manageCustomer
    * Description   : This method is used to manage customer
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function manageCustomer(Request $request){
    	$customers = $this->customers->where('status','!=','D');
    	if(@$request->all()){
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

            $cc = $customers;
            $cc = $cc->count(); // total customer before filtering

			if(@$request->columns['1']['search']['value']){
				$customers = $customers->where(function($query) use($request){
                $query->Where('fname','like','%'.$request->columns['1']['search']['value'].'%')
                ->orWhere('lname','like','%'.$request->columns['1']['search']['value'].'%')
                ->orWhere('phone','like','%'.$request->columns['1']['search']['value'].'%')
                ->orWhere('email','like','%'.$request->columns['1']['search']['value'].'%')
                ->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".$request->columns['1']['search']['value']."%");
                });
                
			}
			if(@$request->columns['2']['search']['value']){
				$customers = $customers->where(function($query) use($request){
                    $query->Where('status',$request->columns['2']['search']['value']);
                });
			}
            if(@$request->columns['3']['search']['value']){
                $customers = $customers->where(function($query) use($request){
                    $query->Where('signup_from', $request->columns['3']['search']['value']);
                });
            }
            if(@$request->columns['4']['search']['value']){
                $customers = $customers->where(function($query) use($request){
                    $query->Where('tag', $request->columns['4']['search']['value']);
                });
            }
            if(@$request->columns['5']['search']['value']){
                $date = explode(',', $request->columns['5']['search']['value']);
                $from_date = $date[0];
                $to_date = $date[1];
                $customers = $customers->whereBetween('created_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);
            }
            $ccc = $customers;
            $ccc = $ccc->count(); // total customers before paginate

            // sorting
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $customers = $customers->orderBy('fname','asc');
                } else {
                    $customers = $customers->orderBy('fname','desc');
                }
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $customers = $customers->orderBy('email','asc');
                } else {
                    $customers = $customers->orderBy('email','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $customers = $customers->orderBy('phone','asc');
                } else {
                    $customers = $customers->orderBy('phone','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $customers = $customers->orderBy('created_at','asc');
                } else {
                    $customers = $customers->orderBy('created_at','desc');
                }
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $customers = $customers->orderBy('status','asc');
                } else {
                    $customers = $customers->orderBy('status','desc');
                }
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $customers = $customers->orderBy('id','asc');
                } else {
                    $customers = $customers->orderBy('id','desc');
                }
            }
            $customers = $customers->skip($request->start)->take($request->length)->get();

            $data['aaData'] = $customers;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $cc;
            $data["iTotalDisplayRecords"] = $ccc;
            return response()->json($data);
		}else{
            $customers = $customers->orderBy('id','desc')->get();
            return view('admin.modules.customers.list_customers')->with([
                'customers' =>  @$customers
            ]);
        }

    }
    /*
    * Method        : checkDuplicateEmail
    * Description   : This method is used to checkDuplicateEmail
    * Author        : Jayatri
    * Date 			: 05.02.2020
    */
    public function checkDuplicateEmail(Request $request){
        $this->checkLoginAccess();
		$customer = $this->customers->where('email',@$request->email);
        $customer = $customer->where(function($query) use($request){
            $query->Where('status','!=','D');
        });
        $customer = $customer->first();
		if(@$customer){
			return 0;
		}else{
			return 1;
		}
    }
    /*
    * Method        : checkDuplicateUpdatePhone
    * Description   : This method is used to check Duplicate Change Phone
    * Author        : Jayatri
    * Date          : 05.02.2020
    */
    public function checkDuplicateUpdatePhone(Request $request){
        $this->checkLoginAccess();
        $customer = $this->customers->where('phone','!=',@$request->old_phone);
        $customer = $customer->where('phone',@$request->phone);
        $customer = $customer->where(function($query) use($request){
            $query->Where('status','!=','D');
        });
        $customer = $customer->first();
        if(@$customer){
            return 0;
        }else{
            return 1;
        }
    }
    /*
    * Method        : checkDuplicateEmail
    * Description   : This method is used to checkDuplicateEmail
    * Author        : Jayatri
    * Date          : 05.02.2020
    */
    public function checkDuplicateUpdateEmail(Request $request){
        $this->checkLoginAccess();
        
        $customer = $this->customers->where('email',@$request->email)->whereNotIn('email',[@$request->old_email]);
        $customer = $customer->where(function($query) use($request){
            $query->Where('status','!=','D');
        });
        $customer = $customer->first();

        if(@$customer){
            return 0;
        }else{
            return 1;
        }
    }
    /*
    * Method        : checkDuplicateEmail
    * Description   : This method is used to checkDuplicateEmail
    * Author        : Jayatri
    * Date 			: 05.02.2020
    */
    public function checkDuplicatePhone(Request $request){

		$customer = $this->customers->where('phone',@$request->phone);
        $customer = $customer->where(function($query){
            $query->Where('status','!=','D');
        });
        $customer = $customer->first();

		if(@$customer){
			return 0;
		}else{
			return 1;
		}
    }
    
    public function sendMessage(Request $request){
        $this->checkLoginAccess();
    	$customer = $this->customers->where('id',$request->id)->first();
        if(@$customer){
            $new['user_type'] = 'C';
            $new['send_by'] = @Auth::guard('admin')->user()->id;
            $new['conversation_id'] = time();
            $new['email_id'] = @$request->email;
            $new['title'] = @$request->title;
            $new['messages'] = @$request->message;
            $new['name'] = @$request->name;
            $new['send_to'] = @$customer->id;
            $conversation = $this->conversations->create($new);

            if(@$conversation){
                $merchantDetails = new \stdClass();
                $merchantDetails->name = @$request->name;
                $merchantDetails->fname = @$request->name;
                $merchantDetails->email =@$request->email;
                $merchantDetails->title =@$request->title;
                $merchantDetails->message = @$request->message;
                Mail::send(new SendMailToMerchant($merchantDetails));
                return 1;    
            }else{
                return 0;
            }
        } else {
            return 0;
        }
    }
    /*
    * Method        : sendNotification
    * Description   : This method is used to send Notification.
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function sendNotification($id,Request $request){
        $this->checkLoginAccess();
        $driver = $this->customers->where('id',$id)->first();
        if(@$driver){
            $merchantDetails            =   new \stdClass();
            $merchantDetails->name      =   $driver->name;
            $merchantDetails->fname     =   $driver->fname;
            $merchantDetails->email     =   $driver->email;
            $merchantDetails->title     =   $request->title;
            $merchantDetails->message   =   $request->message;
            Mail::send(new SendMailToMerchant($merchantDetails));

            session()->flash("success",__('success.-4036'));
        }else{
            session()->flash("error",__('errors.-5032'));
        }
        return redirect()->back();
    }
    /*
    * Method        : manageCustomer
    * Description   : This method is used to manage customer
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function changeStatusCustomer($id){
        $this->checkLoginAccess();
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
              $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                            ->where('id', auth::guard('admin')->user()->id)
                            ->first();
                $permission = array();
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id);
                    }

                }
                //dd($permission);
                if(in_array(3, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });
        }
    	$customer = $this->customers->where('id',@$id)->first();

    	if(@$customer){
    		if(@$customer->status == 'I'){
    			$update['status'] = 'A';
    		}else if(@$customer->status == 'A'){
    			$update['status'] = 'I';
    		}
    		$this->customers->where('id',@$id)->update($update);
    		session()->flash("success",__('success.-4031'));
    	}else{
    		session()->flash("error",__('errors.-5026'));
    	}
    	return redirect()->back();
    }
    /*
    * Method        : blockUnblockCustomer
    * Description   : This method is used to block or unblock customer by ajax
    * Author        : Jayatri
    * Date          : 11/03/2020
    */
    public function blockUnblockCustomer(Request $request){
        $customer = $this->customers->where('id',@$request->id)->first();

        if(@$customer){
            if(@$customer->status == 'I'){
                $update['status'] = 'A';
            }else if(@$customer->status == 'A'){
                $update['status'] = 'I';
            }
            $this->customers->where('id',@$request->id)->update($update);
            $customer = $this->customers->where('id',@$request->id)->first();
            return response()->json($customer);
        }else{
            return 0;
        }
    }
    /*
    * Method        : manageCustomer
    * Description   : This method is used to manage customer
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function deleteCustomer(Request $request,$id){
    	$customer = $this->customers->where('id',@$id)->first();
    	if(@$customer){
    		if(@$customer->image){
                $image_path = '/storage/app/public/customer/profile_pics/'.$customer->image;
                if (file_exists(@$image_path)){
                    unlink(@$image_path);
                }
        	}
    		$this->customers->where('id',@$id)->delete();
    		session()->flash("success",__('success.-4031'));
    	}else{
    		session()->flash("error",__('errors.-5026'));
    	}
    	return redirect()->back();
    }

    /*
    * Method        : manageCustomer
    * Description   : This method is used to manage customer
    * Author        : Sanjoy
    * Date          : 15/01/2020
    */
    public function verifyCustomer(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        $this->customers->where('id',@$request->id)->update(['status' => 'A']);
        $response['result']['message'] = __('success.-4088');

        return response()->json($response);
    }

    /*
    * Method        : manageCustomer
    * Description   : This method is used to manage customer by ajax call
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function deleteCustomerDetails(Request $request){
        // $this->checkLoginAccess();
        // if(Auth::guard('admin')->user()->type == "A"){

        // }else{
        //     $this->middleware('admin.auth:admin');
        //       $this->middleware(function ($request, $next) {
        //         $this->admin = Admin::with('sidebarAccess.menuDetails')
        //                     ->where('id', auth::guard('admin')->user()->id)
        //                     ->first();
        //         $permission = array();
        //         if($this->admin->sidebarAccess !=null)
        //         {
        //             foreach($this->admin->sidebarAccess as $row)
        //             {
        //                 array_push($permission, @$row->menu_id);
        //             }

        //         }
        //         //dd($permission);
        //         if(in_array(3, $permission))
        //         {
        //             return $next($request);
        //         }
        //         else
        //         {
        //             return redirect()->back();
        //         }
        //     });
        // }

        $customer = $this->customers->where('id',@$request->id)->first();
        if(@$customer){
            if(@$customer->image){
                $image_path = '/storage/app/public/customer/profile_pics/'.$customer->image;
                if (file_exists(@$image_path)){
                    unlink(@$image_path);
                }
            }
            $updatec['status'] = 'D';
            $this->customers->where('id',@$request->id)->update($updatec);
            // session()->flash("success",__('success.-4031'));
            return 1;
        }else{
            // session()->flash("error",__('errors.-5026'));
            return 0;
        }
        // return redirect()->back();
    }

    /*
    * Method        : updateTag
    * Description   : This method is used to update tag
    * Author        : Argha
    * Date          : 2020-09-14
    */
    public function updateTag(Request $request)
    {
        //dd($request->all());

        $tagUpdate =$this->customers->where('id',$request->user_id)->update(['tag' => $request->tag]);
        if($tagUpdate){
            return '1';
        }else{
            return '0';
        }

    }
    /*
    * Method        : checkLoginAccess
    * Description   : This method is used to check Login Access
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    private function checkLoginAccess(){
        if(!@Auth::guard('admin')->user()){
            return redirect()->route('admin.login');
        }
    }
}
