<?php

namespace App\Http\Controllers\Admin\Modules\Orders;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

use App\Models\Country;
use App\User;
use App\Models\Language;
use App\Models\Admin;
use App\Models\City;
use App\Models\Setting;
use App\Models\UserReward;
use App\Models\MerchantRegId;
use App\Models\Wallet;
use App\Models\WalletDetails;
use App\Driver;
use App\Merchant;
use App\Models\OrderMaster;
use App\Models\OrderSeller;
use App\Models\OrderDetail;
use App\Models\CountryDetails;
use App\Models\CitiDetails;
use App\Models\Product;
use App\Models\Payment;
use App\Models\Coupon;
use App\Models\MerchantAddressBook;
use App\Models\ZoneMaster;
use App\Models\ZoneDetail;
use App\Models\ZoneRateDetail;
use App\Models\TimeSlot;
use App\Models\OrderStatusTiming;

use App\Repositories\ShippingCostRepository;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\UserRepository;
use App\Repositories\DriverRepository;
use App\Repositories\OrderImageRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CityRepository;
use App\Repositories\CitiDetailsRepository;
use App\Repositories\SettingRepository;
use App\Repositories\CountryDetailsRepository;
use App\Repositories\CountryRepository;
use App\Repositories\UserAddressBookRepository;

use Illuminate\Support\Facades\Hash;

use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;
use App\Mail\SendMailDriverNotifydetails;//notification for external order (driver assigned)
use App\Mail\SendOrderAceptedMail;//notification for internal order (driver assigned)
use App\Mail\SendMailExternalOrderdetails;
use App\Mail\OrderMail;
use App\Mail\SendOrderDetailsMerchantMail;
use App\Mail\SendOrderMail;
use App\Mail\SendMerchantOrderMail;
use Config;
use Validator;
use Auth;

# use whatsapp controller
use App\Http\Controllers\Modules\WhatsApp\WhatsAppController;

class OrdersController extends Controller
{
    protected $order_master, $merchants, $customers, $drivers, $order_details, $order_sellers, $order, $orderDetails, $orderSeller, $merchant;
    
    # whatsapp send message
    protected $whatsApp;

    public function __construct(OrderMasterRepository $order_master,
        MerchantRepository $merchants,
        UserRepository $customers,
        DriverRepository $drivers,
        OrderDetailRepository $order_details,
        OrderSellerRepository $order_sellers,
        CityRepository $city,
        CitiDetailsRepository $city_details,
        ShippingCostRepository $shipping_cost,
        OrderImageRepository $order_image,
        ProductVariantRepository $product_variant,
        ProductVariantRepository $productVariant,
        ProductRepository $product,
        SettingRepository $settings,
        CountryRepository $country,
        Request $request,
        WhatsAppController $whatsApp,
        UserAddressBookRepository $userAddr
    )
    {
        $this->middleware('admin.auth:admin');
        $this->order_master             = $order_master;
        $this->order                    = $order_master;
        $this->order_details            = $order_details;
        $this->orderDetails             = $order_details;
        $this->order_sellers            = $order_sellers;
        $this->orderSeller              = $order_sellers;
        $this->drivers                  = $drivers;
        $this->merchants                = $merchants;
        $this->merchant                 = $merchants;
        $this->customers                = $customers;
        $this->city                     = $city;
        $this->city_details             = $city_details;
        $this->shipping_cost            = $shipping_cost;
        $this->order_image              = $order_image;
        $this->product_variant          = $product_variant;
        $this->productVariant           = $productVariant;
        $this->product                  = $product;
        $this->settings                 = $settings;
        $this->whatsApp                 = $whatsApp;
        $this->country                  = $country;
        $this->userAddr                 = $userAddr;
        // $this->pdf                      =   $pdf;
    }
    

    /*
    * Method        : index
    * Description   : This method is used to view all orders by admin and manage it.
    * Author        : Jayatri
    * Date          : 13/02/2020
    */
    public function index(Request $request){
        
        $customers = $this->order_master->where('user_id','!=',0)->get();
        $external_customer = $this->order_master->where('user_id',0)->get();
        $driver = $this->drivers->where('status', 'A')->get();
        $drivers_ord = $this->drivers->where('status','A')->get();
        $userCustomer = User::where('status','!=','D')->orderBy('fname','asc')->get();
        $kuwaitCities = City::where('id','!=',0);
        // $kuwaitCities = $kuwaitCities->where(function($q1) {
        //     $q1 = $q1->where('language_id',getLanguage()->id);
        // });
        $kuwaitCities = $kuwaitCities->where(function($q1) {
            $q1 = $q1->where('status','!=','D');
        });
        $kuwaitCities = $kuwaitCities->orderBy('name','asc')->get();
        $merchants = $this->merchants->where('status','!=','D')->orderBy('fname','asc')->get();
        $orders = $this->order_master->with('getCityNameByLanguage')->where('status','!=','I');

        $orders = $orders->where('status','!=','D');
        $orders = $orders->where('status','!=','');
        $orders = $orders->where('status','!=','F');
        
        if(@$request->all() && @$request->merchant_id == ''){
            if(@$request->id){
                $orders = $orders->with([
                    'customerDetails',
                    'orderMasterDetails.driverDetails',
                    'countryDetails.countryDetailsBylanguage',
                    'orderMasterDetails',
                    'orderMasterDetails.productDetails',
                    'orderMasterDetails.productDetails.productUnitMasters',
                    'orderMasterDetails.productVariantDetails',
                    'orderMasterDetails.sellerDetails',
                    'countryDetails.countryDetailsBylanguage',
                    'shippingAddress',
                    'billingAddress',
                    'getBillingCountry',
                    'productVariantDetails',
                    'getCityNameByLanguage',
                    'getpickupCountryDetails',
                    'getpickupCityDetails',
                    'getPreferredTime',
                    'getAdminForExternalOrder',
                    'orderMerchants.orderSeller',
                ])
                ->where('id',$request->id);
            }
        } else {
            $orders = $orders->with([
                'customerDetails',
                'orderMasterDetails.driverDetails',
                'countryDetails.countryDetailsBylanguage',
                'orderMasterDetails',
                'orderMasterDetails.productDetails.productUnitMasters',
                'orderMasterDetails.productVariantDetails',
                'orderMasterDetails.sellerDetails',
                'countryDetails.countryDetailsBylanguage',
                'shippingAddress',
                'billingAddress',
                'getBillingCountry',
                'productVariantDetails',
                'getCityNameByLanguage',
                'getpickupCountryDetails',
                'getpickupCityDetails',
                'getPreferredTime',
                'getAdminForExternalOrder',
                'orderMerchants.orderSeller'
            ])
            ->orderBy('id','desc');
        }

        if(@$request->merchant_id){
            $orders = $orders->with([
                'customerDetails',
                'orderMasterDetails.driverDetails',
                'countryDetails.countryDetailsBylanguage',
                'orderMasterDetails',
                'orderMasterDetails.productDetails.productUnitMasters',
                'orderMasterDetails.productVariantDetails',
                'orderMasterDetails.sellerDetails',
                'countryDetails.countryDetailsBylanguage',
                'shippingAddress',
                'billingAddress',
                'getBillingCountry',
                'productVariantDetails',
                'getCityNameByLanguage',
                'getpickupCountryDetails',
                'getpickupCityDetails',
                'getPreferredTime',
                'getAdminForExternalOrder',
                'orderMerchants.orderSeller'
            ])
            ->where('user_id',$request->merchant_id);
        }
        $orders = $orders->orderBy('created_at','desc')->get();
        $order_detail = $this->order_details->get();
        $order_seller = $this->order_sellers->get();
        //dd($orders);
        $coupons = Coupon::get();
        //$key = $request->all();
        
        return view('admin.modules.orders.manage_orders')->with([
            'customers'       => @$customers,
            'drivers'         => @$driver,
            'orders'          => @$orders,
            'merchants'       => @$merchants,
            'order_detail'    => @$order_detail,
            'external_customer'   => @$external_customer,
            'order_seller'    => @$order_seller,
            'drivers_ord'     => @$drivers_ord,
            'userCustomer'    => @$userCustomer,
            'kuwaitCities'    => @$kuwaitCities,
            //'key'             => @$key,
            'coupons'         => $coupons
        ]);
    }

    /*
    * Method        : orderlists
    * Description   : This method is used to view all orders by admin and manage it.
    * Author        : Jayatri
    * Date          : 13/02/2020
    */
    public function orderlists(Request $request){
        $columnIndex = $request->order[0]['column']; // Column index
        $columnName = $request->columns[$columnIndex]['data']; // Column name
        $columnSortOrder = $request->order[0]['dir']; // asc or desc
        $orders = $this->order_master->with('getCityNameByLanguage');
        // $orders = $orders->where('status','!=','I');
        $orders = $orders->where('status','!=','D');
        // $orders = $orders->where('status','!=','F');
        $orders = $orders->where('status','!=', '');
        $ps =  $orders->count();
        $pp = $orders;
        $pp = $pp->count();
        $limit = $request->length;
        $p = $orders = $orders->with([
            'customerDetails',
            'orderMasterDetails.driverDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'orderMasterDetails.productDetails.productUnitMasters',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails',
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'shipping_city_details',
            'shipping_state_details',
            'getBillingCountry',
            'productVariantDetails',
            'getCityNameByLanguage',
            'paymentData',
            'getAdminForExternalOrder',
            'orderMerchants.orderSeller',
            'editedBy',
            'getPreferredTime',
        ]);
        
        $p = $p->count();
        
        if(@$request->all()){
            if(@$request->columns['0']['search']['value']){
                $orders = $orders->where(function($q1) use($request) {
                    $q1 = $q1->where('order_no','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_fname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_lname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_phone','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_email','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".$request->columns['0']['search']['value']."%");
                });
            }
            if(@$request->columns['1']['search']['value']){
                $date = explode(',', $request->columns['1']['search']['value']);
                $from_date = $date[0];
                $to_date = $date[1];
                $orders = $orders->whereBetween('created_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);
            }
            if(@$request->columns['11']['search']['value']){
                $orders = $orders->where(function($q2) use($request) {
                    $q2 = $q2->where('invoice_no','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('invoice_details','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('notes','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('driver_notes','like','%'.$request->columns['11']['search']['value'].'%');
                });
            }
            if(@$request->columns['19']['search']['value']){
                $orders = $orders->where(function($q2) use($request) {
                    $q2 = $q2->where('shipping_country','!=',134);
                });
                $orders = $orders->where(function($q2) use($request) {
                    $q2 = $q2->where('shipping_city','like','%'.$request->columns['19']['search']['value'].'%');
                });
            }
            # order type - all type accessible for super admin but only this is accessible which is given by admin
            $roles = Auth()->guard('admin')->user()->roles->pluck('menu_id')->toArray();
            if(@$request->columns['7']['search']['value']){
                if(Auth()->guard('admin')->user()->type == 'A') {
                    $orders = $orders->where('order_type',$request->columns['7']['search']['value']);
                    if(@$request->columns['23']['search']['value'] && $request->columns['7']['search']['value'] == 'E'){
                        if($request->columns['23']['search']['value'] == 'A'){
                            $orders = $orders->where('order_type','E');
                        } elseif($request->columns['23']['search']['value'] == 'S' || $request->columns['23']['search']['value'] == 'L') {
                            $orders = $orders->where('external_order_type',$request->columns['23']['search']['value']);
                        }
                    }
                } else {
                    # both order accessible
                    if(in_array(6, $roles) && in_array(13, $roles)) {
                        $orders = $orders->where('order_type', $request->columns['7']['search']['value']);
                        if(@$request->columns['23']['search']['value'] == 'E'){
                            if($request->columns['23']['search']['value'] == 'A'){
                                $orders = $orders->where('order_type','E');
                            }elseif($request->columns['23']['search']['value'] == 'S' || $request->columns['23']['search']['value'] == 'L'){
                                $orders = $orders->where('external_order_type',$request->columns['23']['search']['value']);
                            }
                        }
                    } else if(in_array(6, $roles)) {
                        $orders = $orders->where('order_type', 'I');
                    } else if(in_array(13, $roles)) {
                        $orders = $orders->where('order_type', 'E');
                        if(@$request->columns['23']['search']['value'] && $request->columns['7']['search']['value'] == 'E'){
                            if($request->columns['23']['search']['value'] == 'A') {
                                $orders = $orders->where('order_type','E');
                            } elseif($request->columns['23']['search']['value'] == 'S' || $request->columns['23']['search']['value'] == 'L'){
                                $orders = $orders->where('external_order_type',$request->columns['23']['search']['value']);
                            }
                        }
                    }
                } 
            } 
            
            # for sub admin without filter
            if(Auth()->guard('admin')->user()->type == 'S'){
                # both order accessible
                if(in_array(6, $roles) && in_array(13, $roles)) {
                    $orders = $orders->whereIn('order_type', ['I', 'E']);
                } else if(in_array(6, $roles)) {
                    $orders = $orders->where('order_type', 'I');
                } else if(in_array(13, $roles)) {
                    $orders = $orders->where('order_type', 'E');
                }
            } 
            if(@$request->columns['8']['search']['value']){
                $orders = $orders->where('payment_method',$request->columns['8']['search']['value']);
            }
            if(@$request->columns['5']['search']['value']){
                $status = explode(',', $request->columns['5']['search']['value']);
                $orders = $orders->whereIn('status', $status);
                /*if(in_array($request->columns['6']['search']['value'], ['I', 'F'])) {
                    $orders = $orders->whereIn('status', ['I', 'F']);    
                } else {
                }*/
            }
            if(@$request->columns['4']['search']['value']){
                $orders = $orders->whereHas('orderMasterDetails', function($q) use ($request){
                   $q->where('seller_id',$request->columns['4']['search']['value']);
                });
            }
            if(@$request->columns['14']['search']['value']){
                $orders = $orders->whereHas('orderMasterDetails', function($q4) use ($request){
                   $q4->where('driver_id',$request->columns['14']['search']['value']);
                });
            }
            if(@$request->columns['17']['search']['value']){
                $orders = $orders->where('order_from', $request->columns['17']['search']['value']);
            }
            if(@$request->columns['3']['search']['value']){
                $orders = $orders->where( function($q4) use ($request){
                   $q4->where('user_id',$request->columns['3']['search']['value']);
                });
            }
            if(@$request->columns['2']['search']['value']){
                $orders = $orders->where( function($q4) use ($request){
                   $q4->where('shipping_city',$request->columns['2']['search']['value']);
                });
            }
            if(@$request->columns['6']['search']['value']){
                $orders = $orders->whereHas('paymentData', function($q4) use ($request){
                   $q4->where('txn_id',$request->columns['6']['search']['value']);
                });
            }
            if(@$request->columns['22']['search']['value']){
                $orders = $orders->where('coupon_code', @$request->columns['22']['search']['value']);
            }
            $pp = $orders;
            $pp = $pp->count();
            $limit = $request->length;
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('order_no','asc');
                } else {
                    $orders = $orders->orderBy('order_no','desc');
                }
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('created_at','asc');
                } else {
                    $orders = $orders->orderBy('created_at','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('shipping_fname','asc');
                } else {
                    $orders = $orders->orderBy('shipping_lname','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('shipping_city','asc');
                } else {
                    $orders = $orders->orderBy('shipping_city','desc');
                }
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('payable_amount','asc');
                } else {
                    $orders = $orders->orderBy('payable_amount','desc');
                }
            }
            // if($columnIndex == 6) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('order_type','asc');
            //     } else {
            //         $orders = $orders->orderBy('order_type','desc');
            //     }
            // }
            if($columnIndex == 6) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('payment_method','asc');
                } else {
                    $orders = $orders->orderBy('payment_method','desc');
                }
            }
            // if($columnIndex == 8) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('invoice_no','asc');
            //     } else {
            //         $orders = $orders->orderBy('invoice_no','desc');
            //     }
            // }
            //  if($columnIndex == 9) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('invoice_details','asc');
            //     } else {
            //         $orders = $orders->orderBy('invoice_details','desc');
            //     }
            // }
            // if($columnIndex == 10) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('notes','asc');
            //     } else {
            //         $orders = $orders->orderBy('notes','desc');
            //     }
            // }
            // if($columnIndex == 11) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('driver_notes','asc');
            //     } else {
            //         $orders = $orders->orderBy('driver_notes','desc');
            //     }
            // }
            // if($columnIndex == 7) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('id','asc');
            //     } else {
            //         $orders = $orders->orderBy('id','desc');
            //     }
            // }
            // if($columnIndex == 13) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('delivery_date','asc');
            //     } else {
            //         $orders = $orders->orderBy('delivery_date','desc');
            //     }
            // }
            // if($columnIndex == 14) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('delivery_time','asc');
            //     } else {
            //         $orders = $orders->orderBy('delivery_time','desc');
            //     }
            // }
            // if($columnIndex == 15) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('preferred_delivery_date','asc');
            //     } else {
            //         $orders = $orders->orderBy('preferred_delivery_date','desc');
            //     }
            // }
            // if($columnIndex == 16) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('preferred_delivery_time','asc');
            //     } else {
            //         $orders = $orders->orderBy('preferred_delivery_time','desc');
            //     }
            // }
            if($columnIndex == 7) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('status','asc');
                } else {
                    $orders = $orders->orderBy('status','desc');
                }
            }
            if($columnIndex == 8) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('id','asc');
                } else {
                    $orders = $orders->orderBy('id','desc');
                }
            }
            // if($columnIndex == 20) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('status','asc');
            //     } else {
            //         $orders = $orders->orderBy('status','desc');
            //     }
            // }
            // if($columnIndex == 21) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('payment_method','asc');
            //     } else {
            //         $orders = $orders->orderBy('payment_method','desc');
            //     }
            // }
            // if($columnIndex == 22) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('admin_id','asc');
            //     } else {
            //         $orders = $orders->orderBy('admin_id','desc');
            //     }
            // }
            // if($columnIndex == 23) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('creation_type','asc');
            //     } else {
            //         $orders = $orders->orderBy('creation_type','desc');
            //     }
            // }
            // if($columnIndex == 24) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('edit_date','asc');
            //     } else {
            //         $orders = $orders->orderBy('edit_date','desc');
            //     }
            // }
            // if($columnIndex == 25) {
            //     if($columnSortOrder == 'asc') {
            //         $orders = $orders->orderBy('edited_by','asc');
            //     } else {
            //         $orders = $orders->orderBy('edited_by','desc');
            //     }
            // }
            if($columnIndex == 9) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('order_from','asc');
                } else {
                    $orders = $orders->orderBy('order_from','desc');
                }
            }
            //For total subtotal and shipping_cost calculate
            $getOrder = $orders;
            $getOrder = $getOrder->pluck('id');
            
            $orders = $orders->skip($request->start)->take($request->length)->get();
            $data['aaData'] = $orders;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $ps;
            $data["iTotalDisplayRecords"] = $pp;
            $codOrder = OrderMaster::whereIn('id', $getOrder)->where('payment_method', 'C')->whereNotIn('status', ['I', 'F']);
            
            $totalCodSubtotal = $codOrder->sum('subtotal');
            $totalCodSubtotal1 = $codOrder->sum('total_product_price');
            $totalCodSubtotal = $totalCodSubtotal - $totalCodSubtotal1;
            $totalCodShpPrc = $codOrder->sum('loading_price');
            $totalCodShpPrc = $codOrder->sum('order_total');

            $onlineOrder = OrderMaster::whereIn('id', $getOrder)->where('payment_method', 'O')->whereNotIn('status', ['I', 'F']);

            $totalOnlineSubtotal = $onlineOrder->sum('subtotal');
            $totalOnlineSubtotal1 = $onlineOrder->sum('total_product_price');
            $totalOnlineSubtotal = $totalOnlineSubtotal - $totalOnlineSubtotal1;
            $totalOnlineShpPrc = $onlineOrder->sum('loading_price');
            $totalOnlineOrder = $onlineOrder->sum('order_total');

            $data['totalCodSubtotal'] = number_format($totalCodSubtotal, 2);
            $data['totalOnlineSubtotal'] = number_format($totalOnlineSubtotal, 2);
            $data['totalCodShpPrc'] = number_format($totalCodShpPrc, 2);
            $data['totalOnlineShpPrc'] = number_format($totalOnlineShpPrc, 2);
            $data['orderTotal'] = number_format($totalOnlineOrder, 2);
            return response()->json($data);    
        } else {
            $orders =$orders->orderBy('id','desc')->get();
            return response()->json($orders); 
        }    
    }

    /*
    * Method        : index
    * Description   : This method is used to view all orders by admin and manage it.
    * Author        : Jayatri
    * Date          : 13/02/2020
    */
    public function searchOrder(Request $request){
        if(@$request->all()){
            $driver = $this->drivers->where('status','A')->get();
            $orders = $this->order_master->with([
                'customerDetails',
                'orderMasterDetails',
                'countryDetails.countryDetailsBylanguage',
                'orderMasterDetails.productDetails.productUnitMasters',
                'orderMasterDetails.productVariantDetails',
                'orderMasterDetails.sellerDetails',
                'countryDetails.countryDetailsBylanguage',
                'shippingAddress',
                'billingAddress',
                'getBillingCountry',
                'productVariantDetails',
                'getCityNameByLanguage',
                'paymentData',
                'getAdminForExternalOrder',
                'orderMerchants.orderSeller'
            ])->where('status','!=','I');
            
            $orders = $orders->where('status','!=','');
            $orders = $orders->where('status','!=',' ');
            $orders = $orders->where('status','!=','D');
            if(@$request->keyword){
                $orders = $orders->where(function($query) use($request){
                    $query->where('shipping_fname','like','%'.$request->keyword.'%')
                    ->orWhere('shipping_lname','like','%'.$request->keyword.'%')
                    ->orWhere('order_no',$request->keyword)
                    ->orWhere('shipping_email','like','%'.$request->keyword.'%')
                    ->orWhere('shipping_phone','like','%'.$request->keyword.'%')
                    ->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".$request->keyword."%");
                });
            }
            if(@$request->merchant){
                $orders = $orders->whereHas('orderMasterDetails', function($q) use ($request){
                    $q->where('seller_id',$request->merchant);
                });
            }
            if(@$request->customer){
                if(@$request->external){
                    if(@$request->external == 'y'){
                        $orders = $orders->where(function($q1) use($request){
                            $q1 = $q1->where('id',$request->customer);
                        });
                    } else {
                        $orders = $orders->where(function($q1) use($request){
                            $q1 = $q1->where('user_id',$request->customer);
                        });
                    }
                }
            }
            if(@$request->payment_method){
                $orders = $orders->where(function($q1) use($request){
                   $q1 = $q1->where('payment_method',$request->payment_method);
               });
            }
            //order type
            if(@$request->o_t){
                $orders = $orders->where(function($q1) use($request){
                   $q1 = $q1->where('order_type',$request->o_t);
               });
            }
            if(@$request->type){
                $orders = $orders->where(function($q1) use($request){
                    $q1 = $q1->where('status',$request->type);
                });
            }
            $orders = $orders->orderBy('id','desc')->get();
            return view('admin.modules.orders.ajax_manage_orders')->with([
                'orders'=>@$orders,
                'drivers'=>@$driver
            ]);
        } else {
            return 0;
        }
    }

    /*
    * Method        : viewOrderDetails
    * Description   : This method is used to view order details by admin and manage it.
    * Author        : Jayatri
    * Date          : 13/02/2020
    */
    public function viewOrderDetails($id){

        $driver = $this->drivers->where(['status'=>'A'])->get();
        $order = $this->order_master->with([
            'customerDetails.userCountryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails.showOrderVehicleInformation',
            'orderMasterDetails.getOrderDetailsUnitMaster',
            'orderMasterDetails.productDetails.productByLanguage',
            'orderMasterDetails.productDetails.productVariants',
            'orderMasterDetails.productDetails.productUnitMasters',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails.merchantCityDetails',
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'getBillingCountry',
            'productVariantDetails',
            'getOrderAllImages',
            'orderMasterDetails.productDetails.defaultImage',
            'orderMasterExtDetails.sellerDetails',
            'orderMasterDetails.driverDetails',
            'orderMasterExtDetails.driverDetails',
            'getCityNameByLanguage',
            'getBillingCityNameByLanguage',
            'paymentData',
            'getpickupCountryDetails',
            'getpickupCityDetails',
            'getPreferredTime'
        ])
        ->where('id',$id)->first();

        # order status timing
        $orderStatusTiming = OrderStatusTiming::where(['order_id' => $id])->get();
        if(@$order){
            
            return view('admin.modules.orders.view_order_details')->with([
                'order'             => @$order,
                'drivers'           => @$driver,
                'orderStatusTiming' => $orderStatusTiming
            ]);
        } else {
          session()->flash("error",__('errors.-5002'));
          return redirect()->back();
        }
    }

    
    //order master driver assign
    public function assignDriverEntireOrder(Request $request){
        if(@$request->all()){
            if(@$request->order_id){
                if(@$request->assign != 'N'){
                    $order = $this->order_master->where('id',$request->order_id)->first();
                    if(@$order){
                        $sellers = OrderSeller::where(['order_master_id' => $request->order_id])->pluck('seller_id');

                        $driver = $this->drivers->where('id',$request->assign)->first();
                        if(@$driver){
                            $new['status'] = 'DA';
                            $newt['status'] = 'DA';
                            $newt['last_status_date'] = now();
                            $this->order_master->where('id',@$request->order_id)->update($newt);
                            $mstr_ord = $this->order_master->where('id',@$request->order_id)->first();
                            $new['driver_id'] = @$request->assign;
                            
                            $this->order_details->where('order_master_id',@$request->order_id)->update($new);

                            # update to order seller status and driver_id feture added in later
                            OrderSeller::where(['order_master_id' => @$request->order_id])->update($new);

                            # send mail to driver
                           // $this->sendMailDriverAssignEntireOrder($mstr_ord);
                            return 1;
                        } else {
                            return 0;
                        }
                    } else {
                        return 0;
                    }
                } else if(@$request->assign == 'N') {
                    $new['driver_id'] = 0;
                    $this->order_sellers->where('order_master_id',$request->order_id)->update(['status' => 'DA', 'driver_id' => 0]);
                    $this->order_details->where('order_master_id',@$request->order_id)->update($new);
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function reassignDriverEntireOrder(Request $request){
        if(@$request->all()){
            if(@$request->order_id){
                if(@$request->assign != 'N'){
                    $order = $this->order_master->where('id',$request->order_id)->first();
                    if(@$order){
                        # assign driver to merchant for later assing
                        $sellers = OrderSeller::where(['order_master_id' => $request->order_id])->pluck('seller_id');

                        $driver = $this->drivers->where('id',$request->assign)->first();
                        if(@$driver){
                            $new['driver_id'] = @$request->assign;
                            //$new['last_status_date'] = now();
                            if($request->is_final_assign == 'Y') {
                                $this->order_master->where('id',$request->order_id)->update(['is_final_driver' => 'Y', 'status' => 'DA','last_status_date' => now()]);
                                $this->order_sellers->where('order_master_id',$request->order_id)->update(['status' => 'DA', 'driver_id' => $driver->id]);
                                $this->order_details->where('order_master_id',@$request->order_id)->update(['status' => 'DA']);
                            }
                            $this->order_sellers->where('order_master_id',$request->order_id)->update($new);
                            $this->order_details->where('order_master_id',@$request->order_id)->update($new);
                            if(@$request->is_final_assign != 'Y') {
                                //$this->sendMailDriverAssignEntireOrder($order);
                            }
                            return 1;
                        }else{
                            return 0;
                        }
                    }else{
                        return 0;
                    }
                } else if($request->assign == 'N') {
                    $new['driver_id'] = 0;
                    $this->order_sellers->where('order_master_id',$request->order_id)->update(['status' => 'DA', 'driver_id' => 0]);
                    $this->order_details->where('order_master_id',@$request->order_id)->update($new);
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }    
    }

    /*
    * Method        : assignDriver
    * Description   : This method is used to assign driver.
    * Author        : Jayatri
    * Date          : 13/02/2020
    */
    public function assignDriver(Request $request){
        if(@$request->order_id && @$request->assign){
            //dd($request->all());
            $order = $this->order_details->where('id',$request->order_id)->first();
            $driver = $this->drivers->where('id',$request->assign)->first();
            if($order){
                if(@$request->assign == 'N') {
                    $new['driver_id'] = 0;
                } else {
                    $new['driver_id'] = @$request->assign;
                }
                $new['status'] = 'DA';
                $this->order_details->where('id',@$request->order_id)->update($new);
                $dtls = $this->order_details->where('id',@$request->order_id)->first();
                $mstr = $this->order_master->where('id',@$order->order_master_id)->first();
                if(@$mstr){
                    if($mstr->order_type == 'I'){
                        if($request->assign_type == 'EO') {
                            $this->order_details->where('order_master_id',@$order->order_master_id)->update($new);
                            $news['status'] = 'DA';
                            $news['last_status_date'] = now();
                            $this->order_master->where('id',@$order->order_master_id)->update($news);
                            OrderSeller::where(['order_master_id' => $mstr->id])->update($new);
                        } elseif($request->assign_type == 'SS') {
                            $this->order_details->where(['seller_id'=> @$order->seller_id ,'order_master_id' => $mstr->id])->update($new);

                            OrderSeller::where(['seller_id' => @$order->seller_id, 'order_master_id' => $mstr->id])->update($new);
                        }
                        // start
                        $order_m_id = $order->order_master_id;
                        $flag = 0;
                        $items = $this->order_details->where('order_master_id',$order_m_id)->get();
                        if(@$items){
                            foreach(@$items as $it){
                                if($it->status != 'DA'){
                                    $flag = 1;
                                }
                            }
                        }
                        if($flag == 0){
                            $newst['status'] = 'DA';
                            $newst['last_status_date'] = now();
                            $this->order_master->where('id',$order->order_master_id)->update($newst);
                            
                            #send mail to driver for entire order
                            $mstrOrdUpd = $this->order_master->where('id',@$order->order_master_id)->first();
                            //$this->sendMailDriverAssignEntireOrder($mstrOrdUpd);
                            session()->flash("success",__('success.-4044'));
                        }
                    } else {
                        # for external order only
                        OrderSeller::where(['order_master_id' => $mstr->id])->update($new);
                        $news['status'] = 'DA';
                        $news['last_status_date'] = now();
                        $this->order_master->where('id',@$order->order_master_id)->update($news);

                        #send mail to driver for entire order
                        $mstrOrdUpd = $this->order_master->where('id',@$order->order_master_id)->first();
                        //$this->sendMailDriverAssignEntireOrder($mstrOrdUpd);
                    }
                }
                session()->flash("success",__('success.-4055'));
            }
        } else {
            session()->flash("error",__('errors.-5055'));
        }
        return redirect()->back();
    }

    /**
    * Method: reassignDriver
    * Description: This method is used to re-assign driver
    * Author: Jayatri/ modified by Sanjoy
    */
    public function reassignDriver(Request $request){
        if(@$request->order_id_reassign && @$request->reassign){
            
            $order = $this->order_details->where('id',$request->order_id_reassign)->first();
            
            if($order){
                if(@$request->reassign == 'N') {
                    $new['driver_id'] = 0;
                } else {
                    $new['driver_id'] = @$request->reassign;
                }
                $driver = $this->drivers->where('id',$request->reassign)->first();
                if(@$order->driver_id == 0){
                    $new['status'] = 'DA';
                }
                $this->order_details->where('id',@$request->order_id_reassign)->update($new);
                OrderSeller::where(['seller_id' => @$order->seller_id, 'order_master_id' => $order->order_master_id])->update($new);
                
                $master_order = $this->order_master->where('id',$order->order_master_id)->first();
                $order_details = $this->order_details->where('order_master_id',$order->order_master_id)->get();
                $order_count = count($order_details);
                $checkOrderStatus = 'A'; // flag
                foreach ($order_details as $val) {
                    if($val->status != 'DA'){
                        $checkOrderStatus = "NA";
                    }
                }
                if($order_count >1){
                    if($checkOrderStatus != "NA"){
                        $orderMaster['status'] = 'DA';
                        $orderMaster['last_status_date'] = now();
                        $this->order_master->where('id',$order->order_master_id)->update($orderMaster);
                    }
                } else {
                    if($order_details[0]['status'] =='N'){
                        $orderMaster['status'] = 'DA';
                        $orderMaster['last_status_date'] = now();
                        $this->order_master->where('id',$order->order_master_id)->update($orderMaster);
                    }
                }
                if($master_order->order_type == 'E'){
                    //$this->sendMailDriverAssignEntireOrder($master_order);    
                }
                session()->flash("success",__('success.-4058'));
            }else{
                session()->flash("error",__('errors.-5056'));
            }
        }else{
            session()->flash("error",__('errors.-5056'));
        }
        return redirect()->back();
    }

    public function addNotes(Request $request){
        if(@$request->all()){
            $order = $this->order_master->where('id',@$request->id)->first();
            if(@$order){
                $update['notes'] = @$request->notes;
                $this->order_master->where('id',@$request->id)->update($update);
                return 1;
                // session()->flash("success",__('success.-4058'));                            
            }else{
                return 0;
                // session()->flash("error",__('errors.-5056'));        
            }
        }else{
            // session()->flash("error",__('errors.-5056'));
            return 0;
        }
    }

    private function printfile($filename) {
        try {
            $contents = \File::get($filename);
            printfile($contents);  
        } catch (Illuminate\Filesystem\FileNotFoundException $exception) {
            die("The file doesn't exist");
        }
    }

    public function printInvoice($id){
        $driver = $this->drivers->where(['status'=>'A'])->get();
        $order = $this->order_master->with([
            'customerDetails.userCountryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'orderMasterDetails.productDetails.productByLanguage',
            'orderMasterDetails.productDetails.productVariants',
            'orderMasterDetails.productDetails.productUnitMasters',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails.merchantCityDetails',
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'getBillingCountry',
            'productVariantDetails',
            'getOrderAllImages',
            'orderMasterDetails.productDetails.defaultImage',
            'orderMasterExtDetails.sellerDetails',
            'orderMasterDetails.driverDetails',
            'orderMasterExtDetails.driverDetails',
            'getCityNameByLanguage',
            'getBillingCityNameByLanguage'
        ])->where('id',$id)->first();
        $total_weight = $this->order_details->where('order_master_id',@$id)->sum('weight');
        if(@$order){
            return view('admin.modules.orders.pdf_template')->with([
                'order'=>@$order,
                'drivers'=>@$driver,
                'total_weight' => @$total_weight
            ]);
        }else{
          session()->flash("error",__('errors.-5002'));
          return redirect()->back();
        }
    }

    /*
    * Method        : OrderCancel(used and ajax call)
    * Description   : This method is used to cancel Order.
    * Author        : Jayatri
    * Date          : 13/02/2020
    */
    public function orderCancel(Request $request){
        $id = @$request->id;
        $order = $this->order_master->where('id',$id)->first();
        $ps = @$order->status;
        if(@$order){
            # return  for incomplete and failed order
            if($order->status == 'I' || $order->status == 'F') {
                return 1;
            }

            # when cancel order from any status
            if($order->order_type == 'I') {
                if(@$order->status != 'OC'){
                    #increase stock
                    $this->increaseStock(@$order);
                    $this->deductLoyaltyPoint($order);
                }
                
                # if delivered to cancel
                if($order->status == 'OD') {
                    OrderMaster::where(['id' => $id])->update(['loyalty_point_received' => 0]);
                }
            }

            if(@$order->status == 'OP' || @$order->status == 'N' || @$order->status == 'RP'){
                $update['status'] = 'OC';
                $this->order_master->where('id',$id)->update($update);
                $updaten['status'] = 'OC';
                $this->order_details->where('order_master_id',$id)->where('status', '!=', 'C')->update($updaten);
                OrderSeller::where('order_master_id',$id)->where('status', '!=', 'C')->update($updaten);
                return 1;
            }

            if(@$order->status == 'OD' && @$order->order_type == 'E'){
                $this->order_master->where(['id'=>@$request->id])->update(['total_commission'=>0]);
                $this->order_sellers->where(['order_master_id'=>@$request->id])->update(['total_commission'=>0]);
                $details = $this->order_details->where('order_master_id',@$request->id)->first();
                $merchant = $this->merchants->where(['id'=>@$details->seller_id])->first();
                $total_earning = $merchant->total_earning - @$order->subtotal;
                $total_due = $merchant->total_due - @$order->subtotal;
                $updateM['total_earning'] = $total_earning;
                $updateM['total_due'] = $total_due;
                $this->merchants->where(['id'=>@$details->seller_id])->update($updateM);    
                $this->order_sellers->where('order_master_id',@$request->id)->update(['earning' => 0.000]);
                $update['status'] = 'OC';
                $this->order_master->where('id',$id)->update($update);
                $updaten['status'] = 'OC';
                $this->order_details->where('order_master_id',$id)->update($updaten);
                OrderSeller::where('order_master_id',$id)->where('status', '!=', 'C')->update($updaten);
                return 1;
            } else if(@$order->status == 'OD' && @$order->order_type == 'I'){
                $flag = $this->setCommisionDeduct($order);
                if($flag == 0) {
                    return 0;
                } else {
                    $update['status'] = 'OC';
                    $this->order_master->where('id',$id)->update($update);
                    $updaten['status'] = 'OC';
                    $this->order_details->where('order_master_id',$id)->where('status', '!=', 'C')->update($updaten);
                    return 1;
                }
            } else {
                $update['status'] = 'OC';
                $this->order_master->where('id',$id)->update($update);
                $updaten['status'] = 'OC';
                $this->order_details->where('order_master_id',$id)->where('status', '!=', 'C')->update($updaten);

                OrderSeller::where('order_master_id',$id)->where('status', '!=', 'C')->update($updaten);
                return 1;
            }
            session()->flash("success",__('success.-4043'));
        } else {
            session()->flash("error",__('errors.-5040'));
            return 0;
        }
    }

    /*
    * Method        : cancelOrder Donot use this function not in use currently
    * Description   : This method is used to cancel Order.
    * Author        : Jayatri
    * Date          : 13/02/2020
    */
    public function cancelOrder($id){
        $order = $this->order_master->where('id',$id)->first();
        if(@$order){
            $update['status'] = 'OC';
            $this->order_master->where('id',$id)->update($update);
            $updaten['status'] = 'OC';
            $this->order_details->where('order_master_id',$id)->update($updaten);

            OrderSeller::where('order_master_id',$id)->where('status', '!=', 'C')->update($updaten);
            if(@$order->status == 'OP'){
               
            }
            if(@$order->status == 'OD' && @$order->order_type == 'E'){
                $details = $this->order_details->where('order_master_id',@$request->id)->first();

                $total_earning = $this->order_details->where(['seller_id'=>@$details->seller_id,'order_master_id'=>@$request->id])->sum('sub_total');
                $merchant = $this->merchants->where(['id'=>@$details->seller_id])->first();
                $updateM['total_earning'] = $total_earning - $details->original_price;
                $updateM['total_due'] =  (@$merchant->total_due - $details->original_price);
                $this->merchants->where(['id'=>@$details->seller_id])->update($updateM);
                $this->order_sellers->where('order_master_id', $request->id)->update(['earning' => 0.00]);
            }else if(@$order->status == 'OD' && @$order->order_type == 'I'){
                $flag = $this->setCommisionDeduct($order);
                if($flag == 0){
                    return 0;
                }else{
                    $update['status'] = 'OC';
                    $this->order_master->where('id',$id)->update($update);
                    $updaten['status'] = 'OC';
                    $this->order_details->where('order_master_id',$id)->update($updaten);
                    $this->deductLoyaltyPoint($order);
                }
            }
            session()->flash("success",__('success.-4043'));
        }else{
            session()->flash("error",__('errors.-5040'));
        }
        return redirect()->back();
    }

    private function deductLoyaltyPoint($order){
        $loyalty_point = User::where('id',$order->user_id)->first();
        if(@$loyalty_point){
            $loyalty_balance = $loyalty_point->loyalty_balance;
            if(@$order->loyalty_point_received) {
                $loyalty_balance = $loyalty_balance - $order->loyalty_point_received;
            }
            if(@$order->loyalty_point_used) {
                $loyalty_balance = $loyalty_balance + $order->loyalty_point_used;
            }
            $update['loyalty_total'] = $loyalty_point->loyalty_total - $order->loyalty_point_received;
            $update['loyalty_used'] = $loyalty_point->loyalty_used - $order->loyalty_point_used;
            $update['loyalty_balance'] = $loyalty_balance;
            User::where('id',$order->user_id)->update($update);
        }
    }

    /*
    * Method: deductStock
    * Description: 
    * Author: Jayatri
    */
    private function deductStock($orderChk){
        $details = $this->order_details->where('order_master_id',@$orderChk->id)->get();
        foreach($details as $dtl){
            if($dtl->product_variant_id != 0){
                $variant = $this->product_variant->where('id',$dtl->product_variant_id)->first();
                if($variant->stock_quantity >= $dtl->quantity){
                    $updatestock['stock_quantity'] = $variant->stock_quantity - $dtl->quantity;
                    $this->product_variant->where('id',$dtl->product_variant_id)->update($updatestock);
                } else {
                    return 0;
                }
            } else {
                if($dtl->product_id != 0){
                    $product = $this->product->where('id',@$dtl->product_id)->first();    
                    if($product->stock >= $dtl->quantity){
                        $stock_update['stock'] = $product->stock - $dtl->quantity;
                        $this->product->where('id',@$dtl->product_id)->update($stock_update);
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }
            }
        }
    }

    /*
    * Method: decreaseStock
    * Description: This method is used to descrease Stock
    * Author: Jayatri
    */
    private function decreaseStock($orderChk) {   
        $details = $this->order_details->where('order_master_id',@$orderChk->id)->where('status', '!=', 'C')->get();
        
        foreach($details as $dtl){
            if($dtl->product_variant_id != 0){
                $variant = $this->product_variant->where('id',$dtl->product_variant_id)->first();
                if($variant->stock_quantity >= $dtl->quantity){
                    $updatestock['stock_quantity'] = $variant->stock_quantity - $dtl->quantity;
                    $this->product_variant->where('id',$dtl->product_variant_id)->update($updatestock);
                }
            } else {
                if($dtl->product_id != 0){
                    $product = $this->product->where('id',@$dtl->product_id)->first();    
                    if($product->stock >= $dtl->quantity){
                        $stock_update['stock'] = $product->stock - $dtl->quantity;
                        $this->product->where('id',@$dtl->product_id)->update($stock_update);
                    }
                }
            }   
        }
    }

    /*
    * Method: increaseStock
    * Description: 
    * Author: Jayatri
    */
    private function increaseStock($orderChk){
        $details = $this->order_details->where('order_master_id',@$orderChk->id)->where('status', '!=', 'C')->get();
        foreach($details as $dtl){
            if($dtl->product_variant_id != 0){
                $variant = $this->product_variant->where('id',$dtl->product_variant_id)->first();
                $updatestock['stock_quantity'] = $variant->stock_quantity + $dtl->quantity;
                $this->product_variant->where('id',$dtl->product_variant_id)->update($updatestock);
            }else{
                $product = $this->product->where('id',@$dtl->product_id)->first();    
                $stock_update['stock'] = $product->stock + $dtl->quantity;
                $this->product->where('id',@$dtl->product_id)->update($stock_update);
            }
        }
    }

    private function setCommisionDeduct($orderChk) {   
        $details = $this->order_details->where('order_master_id',@$orderChk->id)->where('status', '!=', 'C')->get();
        $ord_s = $this->order_sellers->where('order_master_id',@$orderChk->id)->get();
        if(@$ord_s){
            foreach($ord_s as $os){
                $total_earn = $this->order_sellers->where(['order_master_id'=>@$orderChk->id,'seller_id'=>$os->seller_id])->sum('order_total');
                $shippingPrice = $this->order_sellers->where(['order_master_id'=>@$orderChk->id,'seller_id'=>$os->seller_id])->sum('shipping_price');
                $total_earn = $total_earn - $shippingPrice;
                $total_commission = $this->order_sellers->where(['order_master_id'=>@$orderChk->id,'seller_id'=>$os->seller_id])->sum('total_commission');
                $merchant = $this->merchants->where('id',@$os->seller_id)->first();
                $toal_earn = $merchant->total_earning - ($total_earn - $total_commission);
                $total_due = $merchant->total_due - ($total_earn - $total_commission) ;
                $total_commission =  $merchant->total_commission - $total_commission ;
                $merchant_update['total_earning'] = $toal_earn;
                $merchant_update['total_due'] = $total_due;
                $merchant_update['total_commission'] = $total_commission;
                $this->merchants->where('id',$os->seller_id)->update($merchant_update);
                $this->order_sellers->where('id', $os->id)->update(['earning' => 0.00]);
            }
             
            $update['total_commission'] = 0.000;
            $this->order_master->where('id',@$orderChk->id)->update($update);
            return 1;
        }
        
    }

    private function setCommision($orderChk) {   
        $details = $this->order_details->where('order_master_id',@$orderChk->id)->get();
        //set comission
        $ord_s = $this->order_sellers->where('order_master_id',@$orderChk->id)->get();
        if(@$ord_s){
            foreach(@$ord_s as $os){
                $total_earn = $this->order_sellers->where(['order_master_id'=>@$orderChk->id,'seller_id'=>$os->seller_id])->sum('order_total');
                $shippingPrice = $this->order_sellers->where(['order_master_id'=>@$orderChk->id,'seller_id'=>$os->seller_id])->sum('shipping_price');
                $individual_earning = (@$os->subtotal - @$os->total_discount)- @$os->total_commission;
                $total_earn = $total_earn - $shippingPrice;
                
                $total_commission = $this->order_sellers->where(['order_master_id'=>@$orderChk->id,'seller_id'=>$os->seller_id])->sum('total_commission');
                
                $merchant = $this->merchants->where('id',@$os->seller_id)->first();
                
                $toal_earn = ($total_earn - $total_commission)+$merchant->total_earning;
                $total_due = ($total_earn - $total_commission) + $merchant->total_due;
                $total_commission = $total_commission + $merchant->total_commission;
                
                $merchant_update['total_earning'] = $toal_earn;
                $merchant_update['total_due'] = $total_due;
                $merchant_update['total_commission'] = $total_commission;
                $this->merchants->where('id',$os->seller_id)->update($merchant_update); 
                $this->order_sellers->where('id', $os->id)->update(['earning' => $individual_earning]);
            }
            $ord_comm = $this->order_sellers->where(['order_master_id'=>@$orderChk->id])->sum('total_commission');
             
            $update['total_commission'] = $ord_comm;
            $update['delivery_date'] = now();
            $this->order_master->where('id',@$orderChk->id)->update($update);
            return 1;
        }        
    }
    
    /*
    * Method        : orderStatusChange(not in use)
    * Description   : Not in use
    * Author        : Jayatri(not in use)
    * Date          : 13/02/2020(not in use)
    */
    public function orderStatusChange($id){
        $order = $this->order_details->where('id',$id)->first();
        $mstr_ord = $this->order_master->where('id',$order->order_master_id)->first();
        if(@$order){
            if(@$order->status == 'N'){
                $update['status'] = 'OA';
                $this->order_details->where('id',$id)->update($update);
                session()->flash("success",__('success.-4044'));
            }
            elseif(@$order->status == 'RP'){ //if order is ready for pickup to order picked up
                $update['status'] = 'OP';
                if($order->product_variant_id != 0){
                    $variant = $this->product_variant->where('id',$order->product_variant_id)->first();
                    if($variant->stock_quantity >0){
                        $updatestock['stock_quantity'] = $variant->stock_quantity - $order->quantity;
                        $this->product_variant->where('id',$order->product_variant_id)->update($updatestock);
                        $this->order_details->where('id',$id)->update($update);
                        // session()->flash("success",__('success.-4044'));
                    }
                } else {
                    if($order->product_id != 0) {
                        $s = $this->product->where('id',$order->product_id)->first();
                        $v = $s->stock - $order->quantity;
                        $product_stock['stock'] = $v;
                        $this->product->where('id',$order->product_variant_id)->update($product_stock);
                        $this->order_details->where('id',$id)->update($update);
                    } else {
                        $this->order_details->where('id',$id)->update($update);
                    }
                    session()->flash("success",__('success.-4044'));
                }
            }
            elseif(@$order->status == 'OP'){ // order picked up to order delivered
                $order_dls = $this->order_details->where('id',$id)->first();
                if(@$order_dls){
                    $driverDls = $this->drivers->where('id',@$order_dls->driver_id)->first();
                    if(@$driverDls){
                        $up['assigned'] = 'F';
                        $this->drivers->where('id',@$order_dls->driver_id)->update($up);
                        $update['status'] = 'OD';
                        
                        // $update['driver_id'] = 0;
                        $this->order_details->where('id',$id)->update($update);
                        $ord_mstrs = $this->order_master->where('id',$order_dls->order_master_id)->first();
                        //if cod is_paid y
                        if($ord_mstrs->payment_method == 'C'){
                            $updateOrdMstr['is_paid'] = 'Y';
                            $this->order_master->where('id',$order_dls->order_master_id)->update($updateOrdMstr);
                        }
                        session()->flash("success",__('success.-4044'));
                    }else{
                        session()->flash("error",__('errors.-5041'));
                    }
                }else{
                    session()->flash("error",__('errors.-5041'));
                }
                
            }
            elseif(@$order->status == 'DA'){//if driver is assigned to ready for picked up
                $update['status'] = 'RP';
                $this->order_details->where('id',$id)->update($update);
                session()->flash("success",__('success.-4044'));
            }

            if(@$mstr_ord->order_type == 'E'){
                $order_m = $this->order_master->where('id',$order->order_master_id)->first();
                if(@$order_m){
                    $update_mstr_st['status'] = $update['status'];
                    $this->order_master->where('id',$order->order_master_id)->update($update_mstr_st);
                    session()->flash("success",__('success.-4044'));
                }else{
                    session()->flash("error",__('errors.-5041'));
                }
            } elseif(@$mstr_ord->order_type == 'I'){
                $order_m_id = $order->order_master_id;
                $flag = 0;
                $items = $this->order_details->where('order_master_id',$order_m_id)->get();
                if(@$items){
                    foreach(@$items as $it){
                        if($it->status != $update['status']){
                            $flag = 1;
                        }
                    }
                }
                if($flag == 0){
                    $this->order_master->where('id',$order->order_master_id)->update($update);
                    session()->flash("success",__('success.-4044'));
                }
            }
        } else {
            session()->flash("error",__('errors.-5041'));
        }
        return redirect()->back();
    }

    /*
    *   Method  : updateStatusChange
    *   Use     : update Status Change  for order master table (by ajax) commission update done
    *   Author  : Jayatri
    */
    public function updateStatusChange(Request $request){
        $order = $this->order_master->with([
            'orderMasterDetails.productByLanguage',
            'orderMasterDetails.productDetails.productUnitMasters',
            'orderMasterDetails.productVariantDetails',
            'customerDetails',
            'getBillingCountryCode',
            'orderMerchants.orderSeller'
        ])->where('id',@$request->id)->first();
        $sendEmail = 0;
        $sendWhatsApp = 0;
        $sendWhatsAppExternal = 0;
        foreach ($order->orderMerchants as $key => $value) {
            if($value->orderSeller->notify_customer_by_email_internal == 'Y') {
                $sendEmail += 1; 
            }
            if($value->orderSeller->notify_customer_by_whatsapp_internal == 'Y') {
                $sendWhatsApp += 1;
            }

            if($value->orderSeller->notify_customer_by_whatsapp_external == 'Y') {
                $sendWhatsAppExternal += 1;
            }
        }
        
        if(@$order){
            // $merchant = $this->merchants->where(['id'=>@$details->seller_id])->first();
            if(@$request->status){
                # insert to order status timing table
                //$this->insertOrderStatusTiming($order, $request->status);

                $prevStatus = @$order->status;
                $updateOrder['status'] = $request->status;
                $updateOrder['last_status_date'] = date('Y-m-d H:i:s');
                $this->order_master->where('id',@$request->id)->update($updateOrder);
                
                $update['status'] = $request->status;
                $orderMail = $this->order_master->where('id',@$request->id)->first();
                $this->order_details->where('order_master_id',@$request->id)->where('status', '!=', 'C')->update($update);

                # update order seller table
                OrderSeller::where('order_master_id',@$request->id)->update($update);
                # send whatsapp message to customer on picked up and delivered for internal orders and delivered for external orders.
                if((@$request->status == 'OP') || @$request->status == 'OD') {
                    if(@$order->order_type == 'I') {
                        if(@$sendWhatsApp > 0) {
                            //dd($sendWhatsApp);
                            $this->whatsApp->sendMessage(@$request->id);
                        }
                    } else if(@$order->order_type == 'E') {
                        if(@$sendWhatsAppExternal > 0) {
                            $this->whatsApp->sendMessage(@$request->id);
                        }
                    }
                    //$this->whatsApp->sendMessageMerchants(@$request->id);
                }
                $order = $this->order_master->where('id',@$request->id)->first();
                if(@$prevStatus == 'OD' && @$order->order_type == 'E') {
                    if(@$request->status == 'N' || @$request->status == 'DA' || @$request->status == 'OP' || @$request->status == 'OC') {
                        $details = $this->order_details->where('order_master_id',@$request->id)->first();
                        $merchant = $this->merchants->where(['id'=>@$details->seller_id])->first();
                        $total_earning = $merchant->total_earning - @$order->subtotal;
                        $this->order_sellers->where('order_master_id',@$request->id)->update(['earning' => $total_earning]);
                        $total_due = $merchant->total_due - @$order->subtotal;
                        $updateM['total_earning'] = $total_earning;
                        $updateM['total_due'] = $total_due;
                        $this->merchants->where(['id'=>@$details->seller_id])->update($updateM);                
                        $this->order_master->where(['id'=>@$request->id])->update(['total_commission' => 0]);
                        $this->order_sellers->where(['order_master_id'=>@$request->id])->update(['total_commission' => 0]);
                    }
                } else if(@$prevStatus == 'OD' && @$order->order_type == 'I') {
                    if(@$request->status == 'N' || @$request->status == 'DA' || @$request->status == 'OP' || @$request->status == 'RP' || @$request->status == 'OC'){
                        $this->order_master->whereId($order->id)->update(['loyalty_point_received' => 0]);
                        $flag = $this->setCommisionDeduct($order);
                        if($flag == 0) {
                            return 0;
                        } else {
                            $update['status'] = $request->status;
                            $this->order_master->where('id',@$request->id)->update($update);
                            $this->order_details->where('order_master_id',@$request->id)->where('status', '!=', 'C')->update($update); 
                            $this->deductLoyaltyPointDeliveredToNew($order);
                        }
                    }
                } else if(@$order->status == 'OD' && @$order->order_type == 'E') {
                    $details = $this->order_details->where('order_master_id',@$request->id)->first();
                    $total_earning = $this->order_details->where(['seller_id'=>@$details->seller_id,'order_master_id'=>@$request->id])->sum('sub_total');
                    $this->order_sellers->where('order_master_id',@$request->id)->update(['earning' => $total_earning]);
                    $merchant = $this->merchants->where(['id'=>@$details->seller_id])->first();
                    $updateM['total_earning'] = $total_earning+@$merchant->total_earning;
                    $updateM['total_due'] = (@$merchant->total_due)+$total_earning;
                    $this->merchants->where(['id'=>@$details->seller_id])->update($updateM);                
                    $price = $order->subtotal;
                    $this->order_master->where(['id'=>@$details->order_master_id])->update(['delivery_date'=>now()]);
                } else if(@$order->status == 'OD' && @$order->order_type == 'I') {
                    $flag = $this->setCommision($order);
                    if($flag == 0) {
                        return 0;
                    } else {
                        $update['status'] = $request->status;
                        $this->order_master->where('id',@$request->id)->update($update);
                        $this->order_details->where('order_master_id',@$request->id)->where('status', '!=', 'C')->update($update);
                        
                        # when order delivered credit loyalty point to customer account
                        $this->creaditLoyaltyPoint($order);
                    }
                }
                # maintain stock from cancel to any status
                if(@$prevStatus == 'OC' && @$order->order_type == 'I') {
                    // $this->increaseStock(@$order);
                    #decrease stock changed from order cancel to new/assigned picked up /delivered
                    $this->decreaseStock($order);

                    # when status cancel to other like new or delivered etc
                    $this->reCalculateLoyalty($order);
                    if($order->status != 'OD') {
                        $this->order_master->whereId($order->id)->update(['loyalty_point_received' => 0]);
                    }
                }

                # calculate delayInMins
                if(@$request->status == 'OP' && @$order->order_type == 'E') {
                    if(@$order->external_order_type == 'S') {
                        $now = time(); // or your date as well
                        $your_date = strtotime($order->delivery_date .' '.$order->delivery_time);
                        $datediff = $now - $your_date;
                        $delayInMins = 0;
                        if($datediff > 0) {
                            $delayInMins = round($datediff / 60);
                        }
                        OrderSeller::where(['order_master_id' => $order->id])->update([
                            'delay_in_minutes'   => $delayInMins, 
                            'driver_pickup_date' => date('Y-m-d H:i:s')
                        ]);
                        OrderMaster::where(['id' => $order->id])->update([
                            'delay_in_minutes'   => $delayInMins, 
                            // 'driver_pickup_date' => date('Y-m-d H:i:s')
                        ]);
                    } else if(@$order->external_order_type == 'L') {
                        $order = OrderMaster::with(['getPreferredTime'])->where(['id' => $order->id])->first();
                        $now = time(); // or your date as well
                        $your_date = strtotime($order->preferred_delivery_date .' '.$order->getPreferredTime->to_time);
                        $datediff = $now - $your_date;
                        $delayInMins = 0;
                        if($datediff > 0) {
                            $delayInMins = round($datediff / 60);
                        }
                        OrderSeller::where(['order_master_id' => $order->id])->update([
                            'delay_in_minutes'   => $delayInMins, 
                            // 'driver_pickup_date' => date('Y-m-d H:i:s')
                        ]);

                        OrderMaster::where(['id' => $order->id])->update([
                            'delay_in_minutes'   => $delayInMins, 
                            'driver_pickup_date' => date('Y-m-d H:i:s')
                        ]);
                    }
                }

                # send notification to customer about update order status
                $orderNotify = $this->order_master->where('id',@$request->id)->first();
                if(@$orderNotify) {
                    if($orderNotify->status != 'DA'){

                        $this->sendMailDriverAssignEntireOrder($orderNotify);     
                    }
                }
                $order = $this->order_master->where('id',@$request->id)->first();
                return response()->json($order);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
    * Method: insertOrderStatusTiming
    * Description: This method is used to insert time in to order status timing table
    * Author: Sanjoy
    */
    private function insertOrderStatusTiming($order, $status) {
        $prevStatus = $order->status;

        $prevOrderStatus = OrderStatusTiming::where(['order_id' => $order->id, 'status' => $prevStatus])->first();
        # check same status is already exist or not
        $currentOrderStatus = OrderStatusTiming::where(['order_id' => $order->id, 'status' => $status])->first();
        # if already inserted in the table then only need to update end_time and duration_in_minutes
        if(@$currentOrderStatus) {
            if(@$prevOrderStatus) {
                # update  end_time and duration_in_minutes for previous status
                $durationInMinutes = (time() - strtotime($prevOrderStatus->start_time));
                OrderStatusTiming::where(['order_id' => $order->id, 'status' => $prevStatus])
                ->update([
                    'end_time'              => date('Y-m-d H:i:s'),
                    'duration_in_minutes'   => $durationInMinutes + $prevOrderStatus->duration_in_minutes
                ]);
            }
            
            $durationInMinutes = (time() - strtotime($currentOrderStatus->start_time));
            OrderStatusTiming::where(['order_id' => $order->id, 'status' => $status])
            ->update([
                'start_time'              => date('Y-m-d H:i:s'),
                
            ]);                
        } else {
            if(@$prevOrderStatus) {
                $durationInMinutes = (time() - strtotime($prevOrderStatus->start_time));
                # update end_date for previous status
                OrderStatusTiming::where(['order_id' => $order->id, 'status' => $prevStatus])
                ->update([
                    'end_time'              => date('Y-m-d H:i:s'),
                    'duration_in_minutes'   => $durationInMinutes + $prevOrderStatus->duration_in_minutes
                ]);
            }
            OrderStatusTiming::create([
                'order_id'            => $order->id,
                'status'              => $status,
                'start_time'          => date('Y-m-d H:i:s')
            ]);
        }
    }

    /**
    * Method: changeAnyStatusOrderDetail
    * Description: This method is used to change any status (inside order details) if allow the item status is same status order master table status will be that.(Example : if all of item status is delivered order master table will be delivered)
    * Author: Jayatri
    */
    public function changeAnyStatusOrderDetail(Request $request, $id){
        if(@$request->all()){
            $order = $this->order_details->where('id',$id)->first();
            $order_master_st = $this->order_master->with(['orderMerchants.orderSeller'])->where('id',@$order->order_master_id)->first();
            if(@$order){
                $nm = "status_change_detail".strval($id);
                if(@$request->$nm){
                    $update['status'] = $request->$nm;
                    $this->order_details->where('id',$id)->update($update);
                    session()->flash("success",__('success.-4044'));
                    $orderNotify = $this->order_master->where('id',@$order->order_master_id)->first();
                    if(@$orderNotify->order_type == 'I'){
                        // $this->sendNotificationCustomer(@$orderNotify->id,@$orderNotify->user_id);
                    }
                    
                    $order_type = $this->order_master->where('id',$order->order_master_id)->first();

                    # for external order
                    if($order_type->order_type == 'E'){
                        # update order seller table
                        OrderSeller::where('order_master_id', $order->order_master_id)->update($update);

                        $order_m = $this->order_master->where('id',$order->order_master_id)->first();
                        if(@$order_m){
                            $order_master_st = $order_m;
                            $prevStatus = $order_master_st->status;
                            if(@$prevStatus == 'OD' && @$order_master_st->order_type == 'E'){
                                if(@$request->$nm == 'N' || @$request->$nm == 'DA' || @$request->$nm == 'OP' || @$request->$nm == 'OC'){
                                    $details = $this->order_details->where('order_master_id',@$order->order_master_id)->first();
                                    $merchant = $this->merchants->where(['id'=>@$details->seller_id])->first();
                                    $total_earning = $merchant->total_earning - @$order_master_st->subtotal;
                                    $total_due = $merchant->total_due - @$order_master_st->subtotal;
                                    $updateM['total_earning'] = $total_earning;
                                    $updateM['total_due'] = $total_due;
                                    $this->merchants->where(['id'=>@$details->seller_id])->update($updateM);                
                                    $this->order_master->where(['id'=>@$order->order_master_id])->update(['total_commission'=>0]);
                                    $this->order_sellers->where(['order_master_id'=>@$order->order_master_id])->update(['total_commission'=>0]);
                                }
                            }
                            else if(@$request->$nm == 'OD' && @$order_master_st->order_type == 'E'){
                                $details = $this->order_details->where('order_master_id',@$order->order_master_id)->first();
                                $total_earning = $this->order_details->where(['seller_id'=>@$details->seller_id,'order_master_id'=>@$order->order_master_id])->sum('sub_total');
                                $merchant = $this->merchants->where(['id'=>@$details->seller_id])->first();
                                $updateM['total_earning'] = $total_earning + $merchant->total_earning;
                                $updateM['total_due'] = $total_earning + $merchant->total_due;
                                $this->merchants->where(['id'=>@$details->seller_id])->update($updateM); 
                                $this->order_sellers->where('order_master_id',@$order->order_master_id)->update(['earning' => $total_earning]);               
                                $price = $order_master_st->subtotal;
                                $update_mstr_st['delivery_date'] = now();
                            }
                            
                            $update_mstr_st['status'] = $update['status'];
                            $this->order_master->where('id',$order->order_master_id)->update($update_mstr_st);
                            $updateMstrOrd = $this->order_master->where('id',$order->order_master_id)->first();
                            
                            # send whatsapp message when delivered the order.
                            if(@$request->$nm == 'OD' && @$order_master_st->order_type == 'E'){
                                $details = $this->order_details->where('order_master_id',@$order->order_master_id)->first();
                                $merchant = $this->merchants->where(['id'=>@$details->seller_id])->first();
                                if(@$merchant->notify_customer_by_whatsapp_external == 'Y'){
                                    $this->whatsApp->sendMessage(@$updateMstrOrd->id);
                                }
                                
                            }
                            
                            if(@$updateMstrOrd){
                                //$this->sendMailDriverAssignEntireOrder($updateMstrOrd); 
                                if(@$request->$nm  != 'DA'){

                                    $this->sendMailDriverAssignEntireOrder($updateMstrOrd);     
                                }    
                            }
                            session()->flash("success",__('success.-4044'));
                        }else{
                            session()->flash("error",__('errors.-5041'));
                        }
                    } elseif($order_type->order_type == 'I') {
                        if($order_master_st->is_more_seller == 'Y' && $order_master_st->is_final_driver == 'N' && $request->$nm == 'OP') {
                            OrderSeller::where(['order_master_id' => $order->order_master_id, 'seller_id' => $order->seller_id])->update(['status' => 'PP']);
                            $this->order_details->where(['order_master_id' => $order->order_master_id, 'seller_id' => $order->seller_id])->update(['status' => 'PP']);
                        } else {
                            # update order seller table
                            OrderSeller::where(['order_master_id' => $order->order_master_id, 'seller_id' => $order->seller_id])->update($update);
                            $this->order_details->where(['order_master_id' => $order->order_master_id, 'seller_id' => $order->seller_id])->update($update);
                        }
                        
                        # partial completed or not
                        if($order_master_st->is_more_seller == 'Y' && $order_master_st->is_final_driver == 'N' && $request->$nm == 'OP') {
                            $status1 = 'PP';
                        } else {
                            $status1 = $request->$nm;
                        }

                        $flag = 0;
                        $order_m_id = $order->order_master_id;
                        $items = $this->order_details->where('order_master_id',$order_m_id)->where('status', '!=', 'C')->get();
                        if(@$items){
                            foreach($items as $it){
                                if($it->status != $status1){
                                    $flag = 1;
                                }
                            }
                        }

                        $prevStatus = $order_master_st->status;
                        # for partial order cancellation
                        if(@$request->$nm == 'C' && @$order_master_st->order_type == 'I') {
                            # when cancel a delivered product
                            if(@$prevStatus == 'OD'){
                                # increase stock for particular products.
                                $this->increaseStockPartial($id);
                                
                                # re calculate seller earning, commission, due etc
                                $this->recalculateSellerEarning($order);

                                # recalculate order data
                                $this->recalculateOrderDuringCancel($order);
                            } else {
                                # when cancel a non delivered product.
                                # increase stock for particular products.
                                $this->increaseStockPartial($id);

                                # recalculate order data
                                $this->recalculateOrderDuringCancel($order);
                            }
                            session()->flash("success",__('success.-4044'));
                            return redirect()->back();
                        }
                        //dd($flag);
                        if($flag == 0){
                            #Internal Order 
                            if(@$prevStatus == 'OD' && @$order_master_st->order_type == 'I'){
                                if(@$request->$nm == 'N' || @$request->$nm == 'DA' || @$request->$nm == 'OP' || @$request->$nm == 'RP'){
                                    $flag = $this->setCommisionDeduct($order_master_st);
                                    if($flag == 0){
                                        return 0;
                                    } else {
                                        $this->deductLoyaltyPoint($order);
                                    }
                                }
                            }
                            else if(@$request->$nm == 'OD' && @$order_master_st->order_type == 'I'){
                                $flag = $this->setCommision($order_master_st);
                                if($flag == 0){
                                    return 0;
                                }else{
                                    $this->creaditLoyaltyPoint($order);                                    
                                }
                            }

                            # when whole status changed to PP we need to change to PC
                            if($order_master_st->is_more_seller == 'Y' && $order_master_st->is_final_driver == 'N' && $request->$nm == 'OP') {
                                $update['status'] = 'PC'; // partial pickup completed
                            } else {
                                $update['status'] = $request->$nm;
                            }

                            OrderSeller::where(['order_master_id'=> $order_master_st->id])->update($update);
                            $this->order_details->where(['order_master_id'=>$order_master_st->id])->update($update);

                            $od_details = $this->order_details->where(['order_master_id'=>$order_master_st->id])->get();
                            $stat_flag = 0;
                            foreach ($od_details as  $det) {
                                if($det->status != 'OD'){
                                    $stat_flag = 1;
                                }
                            }
                            if($stat_flag == 0){
                                $update['status'] = 'OD';
                            }
                            OrderMaster::where('id',$order_master_st->id)->update($update);
                            if(@$request->$nm == 'OP' || @$request->$nm == 'OD') {
                                $sendWhatsApp = 0;
                                foreach ($order_master_st->orderMerchants as $key => $value) {
                                    if($value->orderSeller->notify_customer_by_whatsapp_internal == 'Y') {
                                        $sendWhatsApp += 1;
                                    }
                                    
                                }
                                # send whatsapp message to customer
                                if($sendWhatsApp > 0){
                                    $this->whatsApp->sendMessage(@$order->order_master_id);
                                }
                                //$this->whatsApp->sendMessageMerchants(@$order->order_master_id);
                            }
                            session()->flash("success",__('success.-4044'));
                        }
                    }
                } else {
                    session()->flash("error",__('errors.-5041'));    
                }
            } else {
                session()->flash("error",__('errors.-5041'));
            }
        }
        return redirect()->back();
    }

    /**
    * Method: deductLoyaltyPoint
    * Description: This method is used to deduct loyalty point when order status becomes deliverd to new
    * Author: Sanjoy 
    */
    private function deductLoyaltyPointDeliveredToNew($order){
        $loyalty_point = User::where('id',$order->user_id)->first();
        if(@$loyalty_point){
            $loyalty_balance = $loyalty_point->loyalty_balance;
            if(@$order->loyalty_point_received) {
                $loyalty_balance = $loyalty_balance - $order->loyalty_point_received;
            }
            
            $update['loyalty_total'] = $loyalty_point->loyalty_total - $order->loyalty_point_received;
            $update['loyalty_balance'] = $loyalty_balance;
            User::where('id',$order->user_id)->update($update);
        }
    }



    /**
    * Method: reCalculateLoyalty
    * Description: This method is used to re calculate loyalty during cancel to other status
    * Author: Sanjoy
    */
    private function reCalculateLoyalty($order) {
        # if status changing cancel to delivered then loyalty point earn
        $loyalty_point = User::where('id',$order->user_id)->first();
        if(@$loyalty_point){
            $loyalty_balance = $loyalty_point->loyalty_balance;
            if(@$order->loyalty_point_used) {
                $loyalty_balance = $loyalty_balance - $order->loyalty_point_used;
            }
            $update['loyalty_used'] = $loyalty_point->loyalty_used + $order->loyalty_point_used;
            $update['loyalty_balance'] = $loyalty_balance;
            User::where('id',$order->user_id)->update($update);
        }
    }

    /**
    * Method: creaditLoyaltyPoint
    * Description: This method is used to credit loyalty point
    * Author: Sanjoy
    */
    public function creaditLoyaltyPoint($order) {
        $setting = Setting::first();
        $loyaltyPoint = ($order->subtotal - $order->total_discount)*$setting->one_kwd_to_point;
        $totalOrder = $loyaltyPoint*$setting->one_point_to_kwd;
        $this->order_master->whereId($order->id)->update(['loyalty_point_received' => $loyaltyPoint]);
        $this->customers->whereId($order->user_id)->increment('loyalty_balance', $loyaltyPoint);
        $this->customers->whereId($order->user_id)->increment('loyalty_total', $loyaltyPoint);

        $addUserReward = new UserReward;
        $addUserReward['user_id']        = $order->user_id;
        $addUserReward['order_id']       = $order->id;
        $addUserReward['earning_points'] = $loyaltyPoint;
        $addUserReward['credit']         = '1';
        $addUserReward->save();
    }

    /**
    * 
    */
    private function recalculateOrderDuringCancel($orderDetail) {
        $orderSeller = OrderSeller::where(['order_master_id' => $orderDetail->order_master_id,'seller_id' => $orderDetail->seller_id])->first();
        $merchant = Merchant::find($orderDetail->seller_id);
        OrderSeller::where(['id' => $orderSeller->id])->update([
            'subtotal'          => $orderSeller->subtotal - $orderDetail->sub_total,
            'total_discount'    => $orderSeller->total_discount - $orderDetail->discounted_price,
            'order_total'       => $orderSeller->order_total - $orderDetail->total,
            'total_weight'      => $orderSeller->order_total - $orderDetail->weight,
            'total_commission'  => $orderSeller->total_commission - ($orderDetail->total * $merchant->commission / 100),
        ]);
        $orderMaster = OrderMaster::where(['id' => $orderDetail->order_master_id])->first();
        OrderMaster::where(['id' => $orderDetail->order_master_id])->update([
            'total_discount'    => $orderMaster->total_discount - $orderDetail->discounted_price,
            'subtotal'          => $orderMaster->subtotal -       $orderDetail->sub_total,
            'order_total'       => $orderMaster->order_total - $orderDetail->total,
            'total_commission'  => $orderSeller->sum('total_commission')
        ]);
    }

    private function recalculateSellerEarning($orderDetail) {
        $merchant = Merchant::find($orderDetail->seller_id);

        $total_earn = OrderSeller::where(['order_master_id'=>@$orderDetail->order_master_id,'seller_id'=>$orderDetail->seller_id])->sum('order_total');
        $shippingPrice = OrderSeller::where(['order_master_id'=>@$orderDetail->order_master_id,'seller_id'=>$orderDetail->seller_id])->sum('shipping_price');
        $total_earn = $total_earn - $shippingPrice;
        $total_commission = OrderSeller::where(['order_master_id'=>@$orderDetail->order_master_id,'seller_id'=>$orderDetail->seller_id])->sum('total_commission');
        OrderSeller::where(['order_master_id'=>@$orderDetail->order_master_id,'seller_id'=>$orderDetail->seller_id])->update(['earning' => $individual_earning]);
        $toal_earn = $merchant->total_earning - ($total_earn - $total_commission);
        $total_due = $merchant->total_due - ($total_earn - $total_commission);
        $total_commission =  $merchant->total_commission - $total_commission ;
        
        $merchant_update['total_earning'] = $toal_earn;
        $merchant_update['total_due'] = $total_due;
        $merchant_update['total_commission'] = $total_commission;
        $this->merchants->where('id',$orderDetail->seller_id)->update($merchant_update);
        
    }

     /*
    * Method: increaseStockPartial
    * Description: 
    * Author: Sanjoy
    */
    private function increaseStockPartial($id){
        $dtl = $this->order_details->where('id',@$id)->first();
        if($dtl->product_variant_id != 0){
            $variant = $this->product_variant->where('id',$dtl->product_variant_id)->first();
            $updatestock['stock_quantity'] = $variant->stock_quantity + $dtl->quantity;
            $this->product_variant->where('id',$dtl->product_variant_id)->update($updatestock);
        }else{
            $product = $this->product->where('id',@$dtl->product_id)->first();    
            $stock_update['stock'] = $product->stock + $dtl->quantity;
            $this->product->where('id',@$dtl->product_id)->update($stock_update);
        }
    }

    /**
    * Method: updatePaymentMethod
    * Use   : update Status Change  for order master table (by ajax)
    * Author: Jayatri
    */  
    public function updatePaymentMethod(Request $request){
        $order = $this->order_master->where('id',@$request->id)->first();
        if(@$order){
            if(@$order->payment_method == 'C'){
                $update['payment_method'] = 'O';
            }else{
                $update['payment_method'] = 'C';
            }
            
            $this->order_master->where('id',@$request->id)->update($update);
            $order = $this->order_master->where('id',@$request->id)->first();
            return response()->json($order);
        }else{
            return 0;
        }
    }

    // not in use
    public function orderMasterStatusChange($id){
        $order = $this->order_master->where('id',$id)->first();
        if(@$order){
            if(@$order->status == 'N'){
                $update['status'] = 'I';
            }
            elseif(@$order->status == 'I'){
                $update['status'] = 'P';
            }
            $this->order_master->where('id',$id)->update($update);
            session()->flash("success",__('success.-4044'));
        }else{
            session()->flash("error",__('errors.-5041'));
        }
        return redirect()->back();
    }

    // not in use
    public function approveOrderedItem(Request $request){
        $order = $this->order_details->where('id',$request->id)->first();
        if(@$order){
            if(@$order->status == 'P'){
                $update['status'] = 'S';
            }elseif(@$order->status == 'S'){
                $update['status'] = 'D';
            }elseif(@$order->status == 'D'){
                $update['status'] = 'R';
            }

            $this->order_details->where('id',$id)->update($update);
            return 1;
        }else{
            return 0;    
        }

    }
    // not in use
    public function RejectOrderedItem(Request $request){
        $order = $this->order_details->where('id',$request->id)->first();
        if(@$order){
            if(@$order->status == 'P'){
                $update['status'] = 'S';
            }elseif(@$order->status == 'S'){
                $update['status'] = 'D';
            }elseif(@$order->status == 'D'){
                $update['status'] = 'R';
            }

            $this->order_details->where('id',$id)->update($update);
            return 1;
        }else{
            return 0;    
        }

    }

    /*
    * Method: deleteOrder
    * Description: This method is used to delete  Order.
    * Author: Jayatri
    */
    public function deleteOrder(Request $request){
        $order = $this->order_master->where('id',@$request->id)->first();
        if(@$order){
            $update['status'] =  'D';
            $this->order_master->where('id',@$request->id)->update($update);
            $this->order_details->where('order_master_id',@$request->id)->update($update);
            $this->order_sellers->where('order_master_id',@$request->id)->update($update);
            return 1;
        } else {
            return 0;
        }
    }

    /*
    * Method: addOrder
    * Description: This method is used to add External Order.
    * Author: Jayatri
    */
    public function addOrder(Request $request){
        $merchants = $this->merchants->where('status','!=','D')->get();
        $countries = Country::with('countryDetailsBylanguage')->where('status','!=','D')->get();
        $cities = $this->city->with('cityDetailsByLanguage')->where('status','!=','D')->get();
        $country =  @$request->merchant_country;

        $todayDate = date("Y-m-d");
        if(@$request->all()){
            // dd($request->all());
            $merchant = $this->merchants->where('id',@$request->merchant)->first();
            if($request->from_address == 'T'){
                $merchant_address1 = MerchantAddressBook::where('id',$request->from_address_id)->first();
                if($merchant_address1 == null){
                    session()->flash("error",__('errors.-5092'));
                    return redirect()->back();
                }
                $from_country = $merchant_address1->country;
            }else{
                $from_country = $request->pickup_country;
            }
    
            if($request->to_address == 'T'){
                $merchant_address2 = MerchantAddressBook::where('id',$request->to_address_id)->first();
                if($merchant_address2 == null){
                    session()->flash("error",__('errors.-5092'));
                    return redirect()->back();
                }
                $to_country = $merchant_address2->country;
            }else{
                $to_country = $request->country;
            }
            if(@$from_country != 134 && $to_country != 134){
                session()->flash("error",__('errors.-5050'));
                return redirect()->back();
            } else {
                $new['fname']                   = @$request->fname;
                $new['shipping_fname']          = @$request->fname;
                $new['lname']                   = @$request->lname;
                $new['shipping_lname']          = @$request->lname;
                $new['email']                   = @$request->email;
                $new['shipping_phone']          = @$request->phone;
                $new['payment_method']          = @$request->payment_method;
                $new['billing_phone']           = @$request->phone;
                if($request->from_address == 'T') {
                    $new['pickup_country']      = @$merchant_address1->country;
                    $new['pickup_city']         = @$merchant_address1->city;
                    $new['pickup_city_id']      = @$merchant_address1->city_id;
                    $new['pickup_street']       = @$merchant_address1->street;
                    $new['pickup_building']     = @$merchant_address1->building_no;
                    $new['pickup_zip']          = @$merchant_address1->postal_code;
                    $new['pickup_block']        = @$merchant_address1->block;
                    $new['pickup_location']     = @$merchant_address1->location;
                    $new['pickup_address']      = @$merchant_address1->more_address;
                    $new['pickup_lat']          = @$merchant_address1->lat;
                    $new['pickup_lng']          = @$merchant_address1->lng;
                    $new['pickup_phone']        = @$merchant_address1->phone;
                } else {
                    $new['pickup_country']      = @$request->pickup_country;
                    $new['pickup_street']       = @$request->pickup_street;
                    $new['pickup_building']     = @$request->pickup_building;
                    $new['pickup_zip']          = @$request->pickup_zip;
                    $new['pickup_block']        = @$request->pickup_block;
                    $new['pickup_location']     = @$request->pickup_location;
                    $new['pickup_address']      = @$request->pickup_address;
                    $new['pickup_lat']          = @$request->lat1;
                    $new['pickup_lng']          = @$request->lng1;
                    $new['pickup_phone']        = @$request->pickup_phone;
                    if(@$request->pickup_kuwait_city){
                        $city_name = $this->city_details->where('name',$request->pickup_kuwait_city)->first();
                        if($city_name != null){
                            $new['pickup_city']         = @$city_name->name;
                            $new['pickup_city_id']      = @$city_name->city_id;
                            $shipping_city_id = @$city_name->city_id;
                        } else {
                            session()->flash("error",__('errors.-5038'));
                            return redirect()->back();
                        }
                    } else {
                        $new['pickup_city']         = @$request->pickup_city;
                    }
                }

                if($request->to_address == 'T'){
                    $new['shipping_country']        = @$merchant_address2->country;
                    $new['shipping_address']        = @$merchant_address2->more_address;
                    $new['shipping_street']         = @$merchant_address2->street;
                    $new['shipping_block']          = @$merchant_address2->block;
                    $new['shipping_building']       = @$merchant_address2->building_no;
                    $new['shipping_zip']            = @$merchant_address2->postal_code;
                    $new['shipping_city']           = @$merchant_address2->city;
                    $new['shipping_city_id']        = @$merchant_address2->city_id;
                    $new['location']                = @$merchant_address2->location;
                    $new['lat']                     = @$merchant_address2->lat;
                    $new['lng']                     = @$merchant_address2->lng;
                    $new['billing_country']         = @$merchant_address2->country;

                    $shipping_city_id = @$merchant_address2->city_id;
                }else{
                    $new['shipping_country']        = @$request->country;
                    $new['shipping_address']        = @$request->address;
                    $new['shipping_street']         = @$request->street;
                    $new['shipping_block']          = @$request->block;
                    $new['shipping_building']       = @$request->building;
                    $new['shipping_zip']            = @$request->zip;
                    $new['location']                = @$request->location;
                    $new['lat']                     = @$request->lat;
                    $new['lng']                     = @$request->lng;
                    $new['billing_country']         = @$request->country;
                    if(@$request->kuwait_city){
                        $city_name = $this->city_details->where('name',$request->kuwait_city)->first();
                        if($city_name != null){
                            $new['shipping_city']    = @$city_name->name;
                            $new['shipping_city_id'] = @$city_name->city_id;
                            
                            $shipping_city_id = @$city_name->city_id;
                        }else{
                            session()->flash("error",__('errors.-5038'));
                            return redirect()->back();
                        }
                    }else{
                        $new['shipping_city']         = @$request->city;
                    }
                }
                if(@$request->delivery_date){
                    $new['delivery_date']           = @$request->delivery_date;
                }else{
                    $todayDate = date("Y-m-d");
                    $new['delivery_date']           = $todayDate;
                }
                if(@$request->from_time){
                    $new['delivery_time']           = date('H:i:s', strtotime(@$request->from_time)); 
                }
                if(@$request->product_weight){
                    $new['product_total_weight']    = @$request->product_weight;
                }else{
                    $new['product_total_weight']    = 0.000;
                }
                // if both are belongs from kuwait
                if(@$from_country == 134 && @$to_country == 134){
                    $shipping_city = $this->city->where('id',@$shipping_city_id)->first();
                    if($merchant->applicable_city_delivery_external == 'Y'){
                        if($shipping_city->external_delivery_fee > $merchant->shipping_cost){
                            $shippingcost = $shipping_city->external_delivery_fee;
                        }else{
                            $shippingcost = $merchant->shipping_cost;
                        }
                    }else{
                        $shippingcost = $merchant->shipping_cost;
                    }
                    $subtotal  = $request->order_total - $shippingcost;
                    // if($subtotal < 0){
                    //     $subtotal = 0;
                    // }else{
                    //     $subtotal = $subtotal;
                    // }
                    $orderMerchant['shipping_price']    = $shippingcost;
                    $new['shipping_price']              = $shippingcost;
                    $new['subtotal']                    = @$subtotal;
                    $order_total = @$subtotal + $shippingcost;
                    
                    $new['order_total']                 = $order_total;
                    $new['shipping_address']            = @$request->address;
                } else {
                    # if product weight not optional
                    if(@$request->product_weight) {
                        if($request->slug && $request->token){
                            $zoneDetail = ZoneDetail::where('country_id', @$request->country)->first();
                        }else{
                            if($to_country == 134){
                                $zoneDetail = ZoneDetail::where('country_id', @$from_country)->first();
                            }else{
                                $zoneDetail = ZoneDetail::where('country_id', @$to_country)->first();
                            }
                        }
                        
                        if($zoneDetail == null){
                            session()->flash("error",__('errors.-5090'));
                            return redirect()->back();
                        }
                        $chk_zone_rate = ZoneRateDetail::where('zone_id',@$zoneDetail->zone_master_id)->first();
                        if($chk_zone_rate == null){
                            session()->flash("error",__('errors.-5091'));
                            return redirect()->back();
                        }
                        $chk_range = ZoneRateDetail::where('zone_id',@$zoneDetail->zone_master_id);

                        $infinityChk = ZoneRateDetail::where('infinity_weight', 'Y')
                                                     ->where('zone_id',$zoneDetail->zone_master_id)
                                                     ->first();
                        if($infinityChk && @$request->product_weight >= $infinityChk->from_weight) {
                            $chk_range = $infinityChk;
                            $orderMerchant['shipping_price'] = $chk_range->external_outside_kuwait;
                            $new['shipping_price']           = $chk_range->external_outside_kuwait;
                            $subtotal  = $request->order_total - $chk_range->external_outside_kuwait;
                            // if($subtotal < 0){
                            //     $subtotal = 0;
                            // }else{
                            //     $subtotal = $subtotal;
                            // }
                            $new['subtotal']                = @$subtotal;
                            $new['order_total']             = @$subtotal+@$chk_range->external_outside_kuwait;
                        } else {
                            $chk_range = $chk_range->where(function($where) use ($request) {
                                $where->where(function($where1) use ($request) {
                                    $where1->where('from_weight', '<=', @$request->product_weight)
                                    ->where('to_weight', '>', @$request->product_weight);
                                })
                                ->orWhere(function($where2) use ($request) {
                                    $where2->where('from_weight', '<', @$request->product_weight)
                                    ->where('to_weight', '>=', @$request->product_weight);
                                });
                            })->first();
                            
                            if($chk_range) {
                                $orderMerchant['shipping_price'] = $chk_range->external_outside_kuwait;
                                $new['shipping_price']           = $chk_range->external_outside_kuwait;
                                $subtotal  = $request->order_total - $chk_range->external_outside_kuwait;
                                // if($subtotal < 0){
                                //     $subtotal = 0;
                                // }else{
                                //     $subtotal = $subtotal;
                                // }
                                $new['subtotal']        = @$subtotal;
                                
                                $new['order_total']     = @$subtotal+@$chk_range->external_outside_kuwait;
                            } else {
                                session()->flash("error",__('errors.-5091'));
                                return redirect()->back();
                            }
                        }
                    } else {
                        $new['subtotal'] = @$request->order_total;   
                        $new['order_total'] = @$request->order_total; 
                        $new['shipping_price'] = 0.000;  
                    }
                }
                // shipping address
                $merchant = $this->merchants->where('id',@$request->merchant)->first();
                if($merchant->driver_id != 0){
                    $new['status'] = 'DA'; //initialize status
                }else{
                    $new['status'] = 'N'; //initialize status
                }
                $new['order_type'] = 'E'; //external order

                $code = time();
                $chk = $this->order_master->where('order_no',$code)->first();
                if(@$chk){ //check order no is duplicate or not
                    $code = "ORDASW".time();
                    $new['order_no'] = $code;
                }else{
                    $new['order_no'] = $code;
                }
                if(@$request->invoice){
                    $new['invoice_no']       = @$request->invoice;
                }
                if(@$request->invoice_details){
                    $new['invoice_details']  = @$request->invoice_details;
                }
                $new['admin_id']           = Auth::guard('admin')->user()->id;
                $new['creation_type']      = 'C';
                $new['verify_code']        = time().rand(111, 999);
                $new['last_status_date'] = now();
                $order = $this->order_master->create($new);
                
                $ordId = sprintf('%08d', $order->id);
                $updn['order_no'] = $orderNumber = "ORDASW".$ordId;
                $this->order_master->where('id',$order->id)->update($updn);
                $updOrder = $this->order_master->where('id',$order->id)->first();
                if(@$request->image){
                    foreach($request->image as $key=>$file){
                        $i = $key;
                        $name = $i.time()."_".$file->getClientOriginalName();
                        $path = 'storage/app/public/merchant/external_order/photo/';
                        $file->move($path,$name);
                        $order_img['order_id'] = $order->id;
                        $order_img['order_no'] = $updn['order_no'];
                        $order_img['images'] = $name;
                        $this->order_image->create($order_img);
                    }
                }
                $orderMerchant['seller_id'] = @$merchant->id;
                $orderMerchant['order_master_id'] = $order->id;
                $orderMerchant['shipping_price'] = $order->shipping_price;
                if(@$order->product_total_weight){
                    $orderMerchant['total_weight'] = @$order->product_total_weight;
                }else{
                    $orderMerchant['total_weight'] = 0.000;
                }
                $orderMerchant['subtotal'] = $order->subtotal;
                $orderMerchant['order_total'] = $order->order_total;
                if($merchant->driver_id != 0){
                    $orderMerchant['driver_id']               = $merchant->driver_id; //initialize status 
                    $orderMerchant['status']                  = 'DA'; //initialize status 
                }else{
                    $orderMerchant['status']                  = 'N'; //initialize status     
                }
                $orderMerchant['verify_code'] = time();
                $orderMerchant['input_amount']    = @$request->order_total;
                $ordersellers = $this->order_sellers->create($orderMerchant);
                
                if(@$order){
                    $n['seller_id'] = @$merchant->id;
                    $n['order_master_id'] = $order->id;
                    if(@$request->product_weight) {
                        $n['weight'] = @$request->product_weight;
                    } else {
                        $n['weight'] = 0.000;
                    }
                    
                    $n['sub_total'] = $order->subtotal;
                    $n['original_price'] = $order->subtotal;
                    $n['total'] = $order->order_total;
                    $n['quantity'] = 1;
                    if($merchant->driver_id != 0) {
                        $n['driver_id']               = $merchant->driver_id; //initialize status 
                        $n['status']                  = 'DA'; //initialize status 
                        
                    } else {
                        $n['status']                  = 'N'; //initialize status     
                    }
                    
                    $orderDetails = $this->order_details->create($n);
                    $total_earning = $this->order_details->where('seller_id',@$request->merchant)->sum('sub_total');

                    # insert to order timing table
                    $this->insertOrderStatusTimingOnCreateExternal($updOrder->id);

                    # send web notification to update merchant dashboard
                    // $this->sendNotificationDashboardUpdate(@$order->id, @$merchant->id);

                    #send external order details(as per condition)
                    $this->sendMailDriverAssignEntireOrder(@$updOrder);

                    if(@$orderDetails){
                        $this->sendNotification(@$order->id);
                        session()->flash("success",__('success.-4042'));
                    }else{
                        session()->flash("error",__('errors.-5038'));
                    }
                }
                session()->flash("success",__('success.-4042'));
                return redirect()->back();
            }
        } else {
            return view('admin.modules.orders.add_external_order_details')->with([
                'countries' => @$countries,
                'todayDate' => @$todayDate,
                'cities'    => @$cities,
                'merchants' => @$merchants
            ]);
        }
    }

    /**
    * Method: insertOrderStatusTimingOnCreateExternal
    * Description: This method is used to insert time in to order status timing table
    * Author: Sanjoy
    */
    private function insertOrderStatusTimingOnCreateExternal($orderId) {
        $order = OrderMaster::find($orderId);
        OrderStatusTiming::create([
            'order_id'      => $order->id,
            'status'        => 'N',
            'start_time'    => date('Y-m-d H:i:s'),
            'duration_in_minutes' => 0
        ]);
        if($order->status == 'DA') {
            OrderStatusTiming::create([
                'order_id'      => $order->id,
                'status'        => 'DA',
                'start_time'    => date('Y-m-d H:i:s'),
                'duration_in_minutes' => 0
            ]);
        }
    }

    /**
    *   Method  : checkCity
    *   Use     : check existing in city list.  
    *   Author  : surajit
    */ 
    public function editOrderDetails($id,Request $request){
        $countries = Country::with('countryDetailsBylanguage')->where('status','!=','D')->get();
        $cities = $this->city->where('status','!=','D')->get();
        $order = $orderMaster = $this->order_master->with([
            'customerDetails',
            'orderMasterDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'orderMasterDetails.productDetails.productByLanguage',
            'orderMasterDetails.productDetails.productVariants',
            'orderMasterDetails.productDetails.productUnitMasters',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails',
            'countryDetails',
            'shippingAddress',
            'billingAddress',
            'getBillingCountry',
            'productVariantDetails',
            'getOrderAllImages',
            'orderMasterExtDetails.sellerDetails'
        ])->where('id',$id)->first();
        //dd($order);       
        if(@$order->orderMasterDetails[0]){
            $timeslot = TimeSlot::get();
            $addressBook = MerchantAddressBook::with(['countryDetailsBylanguage','getCityNameByLanguage'])->where('merchant_id',$order->orderMasterDetails[0]->seller_id)->get();
            $country = $order->orderMasterExtDetails->sellerDetails->country;
            try {
                if(@$request->all()) {
                    if($request->from_address == 'T') {
                        $merchant_address1 = MerchantAddressBook::where('id',$request->from_address_id)->first();
                        
                        if($merchant_address1 == null){
                            session()->flash("error",__('errors.-5092'));
                            return redirect()->back();
                        }
                        $from_country = $merchant_address1->country;
                    } else {
                        $from_country = $request->pickup_country;
                    }
                    if($request->to_address == 'T') {
                        $merchant_address2 = MerchantAddressBook::where('id',$request->to_address_id)->first();
                        if($merchant_address2 == null){
                            session()->flash("error",__('errors.-5092'));
                            return redirect()->back();
                        }
                        $to_country = $merchant_address2->country;
                    } else {
                        $to_country = $request->country;
                    }
                    if($from_country != 134 && $to_country != 134) {
                        session()->flash("error",__('errors.-5050'));
                        return redirect()->back();
                    }
                    //shipping details
                    $new['fname']                   = @$request->fname;
                    $new['shipping_fname']          = @$request->fname;
                    $new['lname']                   = @$request->lname;
                    $new['shipping_lname']          = @$request->lname;
                    $new['email']                   = @$request->email;
                    $new['shipping_phone']          = @$request->phone;
                    $new['payment_method']          = @$request->payment_method;
                    $new['billing_phone']           = @$request->phone;
                    if($order->external_order_type == 'L'){
                        $new['shipping_country']        = @$request->country;
                        $new['shipping_address']        = @$request->address;
                        $new['shipping_street']         = @$request->street;
                        $new['shipping_block']          = @$request->block;
                        $new['shipping_building']       = @$request->building;
                        $new['location']                = @$request->location;
                        $new['lat']                     = @$request->lat;
                        $new['lng']                     = @$request->lng;
                        $new['billing_country']         = @$request->country;
                        $new['billing_phone']           = @$request->phone;

                        if(@$request->delivery_date) {
                            $new['preferred_delivery_date'] = @$request->delivery_date;
                        } else {
                            $todayDate = date("Y-m-d");
                            $new['preferred_delivery_date'] = $todayDate;
                        }
                        if(@$request->from_time) {
                            $new['preferred_delivery_time'] = @$request->from_time; 
                        }
                        if(@$request->country) {
                            if($request->country == 134){
                                $new['shipping_block']       =   @$request->block;
                                $new['shipping_building']    =   @$request->building;
                                if(@$request->kuwait_city){
                                    $name_city = $this->city_details->where('name',$request->kuwait_city)->first();
                                    if(@$name_city) {
                                        $new['shipping_city']      = @$name_city->name;
                                        $new['shipping_city_id']   = @$name_city->city_id;
                                        $shipping_city_id = @$name_city->city_id;
                                    } else {
                                        session()->flash("error",__('errors.-5038'));
                                        return redirect()->back();
                                    }
                                }
                            } else {
                                $new['shipping_city']   = @$request->city;
                            }
                        }
                    } else {
                        if($request->from_address == 'T') {
                            $new['pickup_country']      = @$merchant_address1->country;
                            $new['pickup_city']         = @$merchant_address1->city;
                            $new['pickup_city_id']      = @$merchant_address1->city_id;
                            $new['pickup_street']       = @$merchant_address1->street;
                            $new['pickup_building']     = @$merchant_address1->building_no;
                            $new['pickup_zip']          = @$merchant_address1->postal_code;
                            $new['pickup_block']        = @$merchant_address1->block;
                            $new['pickup_location']     = @$merchant_address1->location;
                            $new['pickup_address']      = @$merchant_address1->more_address;
                            $new['pickup_lat']          = @$merchant_address1->lat;
                            $new['pickup_lng']          = @$merchant_address1->lng;
                            $new['pickup_phone']        = @$merchant_address1->phone;
                        } else {
                            $new['pickup_country']      = @$request->pickup_country;
                            $new['pickup_street']       = @$request->pickup_street;
                            $new['pickup_building']     = @$request->pickup_building;
                            $new['pickup_zip']          = @$request->pickup_zip;
                            $new['pickup_block']        = @$request->pickup_block;
                            $new['pickup_location']     = @$request->pickup_location;
                            $new['pickup_address']      = @$request->pickup_address;
                            $new['pickup_lat']          = @$request->lat1;
                            $new['pickup_lng']          = @$request->lng1;
                            $new['pickup_phone']        = @$request->pickup_phone;
                            if(@$request->pickup_kuwait_city){
                                $city_name = $this->city_details->where('name',$request->pickup_kuwait_city)->first();
                                if($city_name != null) {
                                    $new['pickup_city']    = @$city_name->name;
                                    $new['pickup_city_id'] = @$city_name->city_id;
                                    $merchant_city_id = @$city_name->city_id;
                                } else {
                                    session()->flash("error",__('errors.-5038'));
                                    return redirect()->back();
                                }
                            }else{
                                $new['pickup_city']         = @$request->pickup_city;
                            }
                        }
                        if($request->to_address == 'T'){
                            $new['shipping_country']        = @$merchant_address2->country;
                            $new['shipping_address']        = @$merchant_address2->more_address;
                            $new['shipping_street']         = @$merchant_address2->street;
                            $new['shipping_block']          = @$merchant_address2->block;
                            $new['shipping_building']       = @$merchant_address2->building;
                            $new['shipping_zip']            = @$merchant_address2->postal_code;
                            $new['shipping_city']           = @$merchant_address2->city;
                            $new['shipping_city_id']        = @$merchant_address2->city_id;
                            $new['location']                = @$merchant_address2->location;
                            $new['lat']                     = @$merchant_address2->lat;
                            $new['lng']                     = @$merchant_address2->lng;
                            $new['billing_country']         = @$merchant_address2->country;

                            $shipping_city_id = @$merchant_address2->city_id;
                        }else{
                            $new['shipping_country']        = @$request->country;
                            $new['shipping_address']        = @$request->address;
                            $new['shipping_street']         = @$request->street;
                            $new['shipping_block']          = @$request->block;
                            $new['shipping_building']       = @$request->building;
                            $new['shipping_zip']            = @$request->zip;
                            $new['location']                = @$request->location;
                            $new['lat']                     = @$request->lat;
                            $new['lng']                     = @$request->lng;
                            $new['billing_country']         = @$request->country;
                            if(@$request->kuwait_city){
                                $city_name = $this->city_details->where('name',$request->kuwait_city)->first();
                                if($city_name != null){
                                    $new['shipping_city']    = @$city_name->name;
                                    $new['shipping_city_id'] = @$city_name->city_id;

                                    $shipping_city_id = @$city_name->city_id;
                                }else{
                                    session()->flash("error",__('errors.-5038'));
                                    return redirect()->back();
                                }
                            }else{
                                $new['shipping_city']         = @$request->city;
                            }
                        }
                        if(@$request->delivery_date){
                            $new['delivery_date']           = @$request->delivery_date;
                        } else {
                            $todayDate = date("Y-m-d");
                            $new['delivery_date']           = $todayDate;
                        }
                        if(@$request->from_time){
                            $new['delivery_time']           = date('H:i:s', strtotime(@$request->from_time)); 
                        }
                    }
                    
                    if(@$request->product_weight){
                        $new['product_total_weight']    = @$request->product_weight;
                    }else{
                        $new['product_total_weight']    = 0.000;
                    }
                    if(@$request->country == 134 && $country == 134){
                        $merchant = $this->merchants->where('id',@$order->orderMasterDetails[0]->seller_id)->first();
                        $shipping_city = $this->city->where('id',@$shipping_city_id)->first();
                        if($merchant->applicable_city_delivery_external == 'Y'){
                            if($shipping_city->external_delivery_fee > $merchant->shipping_cost){
                                $shippingcost = $shipping_city->external_delivery_fee;
                            } else {
                                $shippingcost = $merchant->shipping_cost;
                            }
                        } else {
                            $shippingcost = $merchant->shipping_cost;
                        }
                        $subtotal  = $request->order_total - $shippingcost;
                        // if($subtotal < 0) {
                        //     $subtotal = 0;
                        // } else {
                        //     $subtotal = $subtotal;
                        // }
                        $orderMerchant['shipping_price']    = $shippingcost;
                        $new['shipping_price']              = $shippingcost;
                        $new['subtotal']                    = @$subtotal;
                        $order_total = @$subtotal + $shippingcost;
                        
                        $new['order_total']                 = $order_total;
                        $new['shipping_address']            = @$request->address;
                    } else {
                        if(@$request->product_weight) {
                            if($order->external_order_type == 'L') {
                                $zoneDetail = ZoneDetail::where('country_id', @$request->country)->first();
                            } else {
                                if($to_country == 134) {
                                    $zoneDetail = ZoneDetail::where('country_id', @$from_country)->first();
                                } else {
                                    $zoneDetail = ZoneDetail::where('country_id', @$to_country)->first();
                                }
                            }
                            
                            if($zoneDetail == null) {
                                session()->flash("error",__('errors.-5090'));
                                return redirect()->back();
                            }
                            $chk_zone_rate = ZoneRateDetail::where('zone_id',@$zoneDetail->zone_master_id)->first();
                            if($chk_zone_rate == null) {
                                session()->flash("error",__('errors.-5091'));
                                return redirect()->back();
                            }
                            $chk_range = ZoneRateDetail::where('zone_id',@$zoneDetail->zone_master_id);
                            $infinityChk = ZoneRateDetail::where('infinity_weight', 'Y')
                                                         ->where('zone_id',$zoneDetail->zone_master_id)
                                                         ->first();
                            if($infinityChk && @$request->product_weight >= $infinityChk->from_weight) {
                                $chk_range = $infinityChk;
                                $orderMerchant['shipping_price'] = $chk_range->external_outside_kuwait;
                                $new['shipping_price']           = $chk_range->external_outside_kuwait;
                                $subtotal  = $request->order_total - $chk_range->external_outside_kuwait;
                                // if($subtotal < 0){
                                //     $subtotal = 0;
                                // }else{
                                //     $subtotal = $subtotal;
                                // }
                                $new['subtotal']                = @$subtotal;
                                $new['order_total']             = @$subtotal+@$chk_range->external_outside_kuwait;
                            } else {
                                $chk_range = $chk_range->where(function($where) use ($request) {
                                    $where->where(function($where1) use ($request) {
                                        $where1->where('from_weight', '<=', @$request->product_weight)
                                        ->where('to_weight', '>', @$request->product_weight);
                                    })
                                    ->orWhere(function($where2) use ($request) {
                                        $where2->where('from_weight', '<', @$request->product_weight)
                                        ->where('to_weight', '>=', @$request->product_weight);
                                    });
                                })->first();
                                
                                if($chk_range) {
                                    $orderMerchant['shipping_price'] = $chk_range->external_outside_kuwait;
                                    $new['shipping_price']           = $chk_range->external_outside_kuwait;
                                    $subtotal  = $request->order_total - $chk_range->external_outside_kuwait;
                                    // if($subtotal < 0){
                                    //     $subtotal = 0;
                                    // }else{
                                    //     $subtotal = $subtotal;
                                    // }
                                    $new['subtotal']                = @$subtotal;
                                    $new['order_total']             = @$subtotal+@$chk_range->external_outside_kuwait;
                                } else {
                                    session()->flash("error",__('errors.-5091'));
                                    return redirect()->back();
                                }
                            }
                        } else {
                            $new['subtotal']        = @$request->order_total;  
                            $new['order_total']     = @$request->order_total;  
                            $new['shipping_price']  = 0.000;
                        }
                    }
                    $new['invoice_no']         = @$request->invoice;
                    $new['invoice_details']    = @$request->invoice_details;
                    // $new['admin_id']           = Auth::guard('admin')->user()->id;
                    $new['creation_type']      = 'E';
                    $new['edit_date']          = now();
                    $new['edited_by']          = Auth::guard('admin')->user()->id;
                    $this->order_master->where('id',$id)->update($new);
                    $order = $this->order_master->where('id',$id)->first();
                    #code update order details table
                    if(@$request->image){
                        foreach($request->image as $key=>$file){
                            $i = $key;
                            $name = $i.time()."_".$file->getClientOriginalName();
                            $path = 'storage/app/public/merchant/external_order/photo/';
                            $file->move($path,$name);
                            $order_img['order_id'] = $order->id;
                            $order_img['order_no'] = $order->order_no;
                            $order_img['images'] = $name;
                            $this->order_image->create($order_img);
                        }
                    }
                    if(@$request->product_weight){
                        $n['weight'] = @$request->product_weight;
                    }
                    $n['sub_total'] = @$order->subtotal;
                    $n['original_price'] = @$order->subtotal;
                    $n['total'] = @$order->order_total;
                    $n['quantity'] = 1;
                    $this->order_details->where('order_master_id',@$order->id)->update($n);
                    //total earnings update
                    $o_dtls = $this->order_details->where('order_master_id',$order->id)->first();
                    $total_earning = $this->order_details->where('seller_id',$o_dtls->seller_id)->sum('sub_total');
                    $orderMerchant['seller_id'] = $order->orderMasterDetails[0]->seller_id;
                    $orderMerchant['total_weight'] = $order->product_total_weight;
                    $orderMerchant['subtotal'] = $order->subtotal;
                    $orderMerchant['order_total'] = $order->order_total;
                    $orderMerchant['input_amount']    = @$request->order_total;
                    $this->order_sellers->where('order_master_id',$id)->update($orderMerchant);
                
                    # recalculate merchant earning when change order total after delivered of the order.
                    if($order->status == 'OD') {
                        # deduct previous amount 
                        $merchant = Merchant::where(['id' => $order->orderMasterDetails[0]->seller_id])->first();

                        $totEarning = ($merchant->total_earning - $orderMaster->order_total) + @$request->order_total;
                        $totDue = ($merchant->total_due - $orderMaster->order_total) + @$request->order_total;
                        Merchant::where(['id' => $order->orderMasterDetails[0]->seller_id])->update(['total_earning' => $totEarning, 'total_due' => $totDue]);
                    }
                    session()->flash("success",__('success.-4052'));
                    return redirect()->back();
                }
            }
            catch(\Exception $e){
                return redirect()->back()->withError(['message' => $e->getMessage(), 'meaning' => "test"]);
            }
            return view('admin.modules.orders.edit_order_details')->with([
                'order'=>@$order,
                'countries'=>@$countries,
                'cities'=>@$cities,
                'address'=>@$addressBook,
                'timeslot' => $timeslot
            ]);
        } else {
            session()->flash("error",__('errors.-5002'));
            return redirect()->back();
        }
    }

    /**
    *   Method  : checkCity
    *   Use     : check existing in city list.  
    *   Author  : surajit
    */
    public function checkCity(Request $request){
        $response = array(
            'jsonrpc' => '2.0'
        );
        
        if($request->get('city')) {
            $city = $request->get('city');
            $data = $this->city_details->where('name', $city)->first();
            if($data) {
                $response['status'] = 'SUCCESS';
                return response()->json($response);             
            } else {
                $response['status'] = 'ERROR';    
                return response()->json($response);     
            }
        } else {
            $response['status'] = 'ERROR';    
            return response()->json($response);     
        }
    }

    /**
    *   Method  : fetchCity
    *   Use     : This method is used to fetch city by city name!  
    *
    */
    public function fetchCity(Request $request){
        $response = array(
            'jsonrpc' => '2.0'
        );
        if($request->get('city')) {
            $cities = $this->city->with('cityDetailsByLanguage')->where('status', '!=', 'D');
            $cities = $cities->where(function($q1) use($request) {
                $q1 = $q1->where('status', '!=', 'I');
            });
            $cities = $cities->whereHas('cityDetailsByLanguage',function($query) use($request) {
                $query->where('name', 'LIKE', "%{$request->city}%");
            });
            $cities = $cities->orderBy('name')->get();
        }
        $response['result']['cities'] = $cities;
        return response()->json($response);     
    }

    /**
    *   Method  : fetchCityPickup
    *   Use     : Auto complete city list.  
    *   Author  : Argha
    *   Date    : 2020-10-08
    */
    public function fetchCityPickup(Request $request){
        $response = array(
            'jsonrpc' => '2.0'
        );
        
        if($request->get('city')) {
            $city = $request->get('city');
            $data = $this->city->with('cityDetailsByLanguage')->where('status', '!=', 'D');
            $data = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
            $data = $data->where(function($q1) use($request) {
                $q1 = $q1->where('status', '!=', 'I');
            });
            $data = $data->whereHas('cityDetailsByLanguage',function($query) use($request) {
                $query->where('name', 'LIKE', "%{$request->city}%");
            })->orderBy('id','desc')->get();
            if($data->count() > 0) {
                $output = '<ul class="dropdown-menu" style="display:block;width: 100%;padding:10px;">';
                foreach($data as $row) {
                    $output .= '<a href="javascript:void(0);" style="color:black;"><li class="pickUpCityChange pickup_kuwait_cities">'.$row->cityDetailsByLanguage->name.'</li></a>';
                }
                $output .= '</ul>';
            } else {
                $output = 'No records found..';
            }
            $response['result'] = $output;
            $response['status'] = 'SUCCESS';
            return response()->json($response);
        } else {
            $response['status'] = 'ERROR';    
            return response()->json($response);     
        }
    }

    /**
    *   Method  : sendNotification
    *   Use     : This method is used to send notification in driver app for if new order placed!  
    *   Author  : Jayatri
    *
    */
    private function sendNotification($order_id){
        $androidDrivers = Driver::where(['status' => 'A', 'device_type' => 'A'])->pluck('firebase_reg_no');

        $iosDrivers = Driver::where(['status' => 'A', 'device_type' => 'I'])->pluck('firebase_reg_no');
        $order_dtt = OrderMaster::where('id',$order_id)->first();
        # send notification to android drivers
        if(count($androidDrivers)) {
            $msg                      = array();
            $msg['title']             = 'New Order Placed';
            $msg["body"]              = 'Recently customer placed an order! ';
            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            $msg['link_type']         = "Y";
            $msg['is_linked']         = "Y";
            $msg['image']             = '';
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]           = "10";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'registration_ids' => $androidDrivers,
                'data'             => $msg
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            
            // Closing Curl
            curl_close($ch);
        }
        # send notification to ios drivers
        if(count($iosDrivers)) {
            $msg                      = array();
            $msg['title']             = "New Order Placed";
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]            = "10";
            $msg["mutable-content"]   = true;
            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            $msg["body"]              = 'Recently customer placed an order! ';

            $data['message'] = "New Order Placed";
            $data['type'] = "1";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'category'                  => "CustomSamplePush",
                'content_available'         => true,
                'mutable_content'           => true,
                'registration_ids'          => $iosDrivers,
                'notification'              => $msg,
                'data'                      => $data
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            // Closing Curl
            curl_close($ch);
        }
    }

    /**
    *   Method  : sendNotificationCustomer
    *   Use     : This method is used to send notification in customer app for if order status changed!  
    *   Author  : Jayatri
    */
    private function sendNotificationCustomer($order_id,$userId){
        $androidDrivers = User::where(['status' => 'A', 'device_type' => 'A'])->pluck('firebase_reg_no');
        $iosDrivers = User::where(['id'=>@$userId,'status' => 'A', 'device_type' => 'I'])->pluck('firebase_reg_no');
        $order_dtt = OrderMaster::where('id',$order_id)->first();
        # send notification to android drivers
        if(count($androidDrivers)) {
            $msg                      = array();
            if(@$order_dtt->status == 'N'){
                $msg['title']             = " Order No : ".@$order_dtt->order_no;
                $msg["body"]              = 'Your have successfully placed the order! ';
            }
            if(@$order_dtt->status == 'DA'){
                $msg['title']             = ''." "." Order No : ".@$order_dtt->order_no;
                $msg["body"]              = 'Your order is out for delivery! ';
            }
            if(@$order_dtt->status == 'RP'){
                $msg['title']             = " "." Order No : ".@$order_dtt->order_no;
                $msg["body"]              = 'Your order status is ready to be picked up! ';
            }
            if(@$order_dtt->status == 'OP'){
                $msg['title']             = ''." "." Order No : ".@$order_dtt->order_no;
                $msg["body"]              = 'Your order is picked up! ';
            }
            if(@$order_dtt->status == 'OD'){
                $msg['title']             = ''." "." Order No : ".@$order_dtt->order_no;
                $msg["body"]              = 'Your order Delivered! ';
            }
            
            $msg['link_type']         = "Y";
            $msg['is_linked']         = "Y";
            $msg['image']             = '';
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]            = "10";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'registration_ids' => $androidDrivers,
                'data'             => $msg
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            
            // Closing Curl
            curl_close($ch);
        }
        
        # send notification to ios drivers
        if(count($iosDrivers)) {
            $msg                      = array();
            $msg['title']             = "New Order Placed";
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]            = "10";
            $msg["mutable-content"]   = true;
            $msg["body"]              = 'Recently customer placed an order!';

            $data['message'] = "New Order Placed";
            $data['type'] = "1";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'category'                  => "CustomSamplePush",
                'content_available'         => true,
                'mutable_content'           => true,
                'registration_ids'          => $iosDrivers,
                'notification'              => $msg,
                'data'                      => $data
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            // Closing Curl
            curl_close($ch);
        }
    }
    
    /**
    * Method: sendCustomerAndSellerMail 
    */
    private function sendCustomerMail($orderId) {
        $data = [];
        $data['order'] = $order = $this->order_master->where('id', $orderId)
        ->with([
            'shippingAddress.getCountry',
            'billingAddress.getCountry',
            'getCountry'
        ])->first();
        
        $data['orderDetails'] = $this->order_details->where(['order_master_id' => $orderId])
        ->with([
            'sellerDetails',
            'defaultImage',
            'productByLanguage',
            'productVariantDetails'
        ])
        ->get();
        
        $flag = 0;
        foreach ($data['orderDetails'] as $va) {
            if($va->seller_id){
                if($va->sellerDetails->notify_customer_by_email_internal == 'Y'){
                    $flag = 1;
                }
            }
        }
        $contactList = [];
        $i=0;
        if(@$order->user_type == 'C') {
            #customer
            $data['fname'] = @$order->fname;
            $data['lname'] = @$order->lname;
            $data['email'] = @$order->email;
            $data['user_type'] = @$order->user_type;
        } else {
            #guest
            $user = User::whereId($order->user_id)->first();
            $data['fname'] = $user->fname;
            $data['lname'] = $user->lname;  
            $data['email'] = $user->email;
            $data['user_type'] = $user->user_type; 
        }
        $data['shipping_price'] = @$order->shipping_price;
        $data['orderNo'] = $order->order_no;
        $data['language_id'] = getLanguage()->id;
        // $contactList[$i] = @$order->shipping_email;
        $data['sub_total'] = @$order->subtotal;
        $data['order_total'] = @$order->order_total;
        $data['total_discount'] = $order->total_discount;
        if(@$order->status == 'N'){
            $data['subject'] = "Your order has been placed successfully! Order no: ".@$order->order_no;
        }else if(@$order->status == 'DA'){
            $data['subject'] = "Your order is out for delivery! Order No:".@$order->order_no." ! Order Details!";
        }
        else if(@$order->status == 'RP'){
            $data['subject'] = "Your order is ready for pick up! Order No:".@$order->order_no." ! Order Details!";
        }
        else if(@$order->status == 'OP'){
            $data['subject'] = "Your order is picked up! Order No:".@$order->order_no." ! Order Details!";
        }
        else if(@$order->status == 'OD'){
            $data['subject'] = "Your order is delivered successfully! Order No:".@$order->order_no." ! Order Details!";
            # driver rating link
            $data['driver_rating_link'] = url('driver-review/' . @$order->order_no . '/' . @$order->user_id . '/' .@$order->verify_code);
        }

        #send mail admin to notify for new order only as client requirement (internal order)
        $i=0;
        if(@$order->status =='N'){
            $setting = Setting::first();
            if(@$setting->email_1) {
                $contactList[$i] = $setting->email_1;
                $i++;
            }
            if(@$setting->email_2) {
                $contactList[$i] = $setting->email_2;
                $i++;
            }
            if(@$setting->email_3) {
                $contactList[$i] = $setting->email_3;
                $i++;
            }
        }
        $contactList = array_unique($contactList);
        $data['bcc'] = $contactList;
        #send to customer mail(order status can be new/driver assigned)
        if(@$order->status == 'OD'){
            if($flag == 1){
                Mail::send(new OrderMail($data));
            }
            
        }
    }

    /**
    * 
    */
    public function sendMerchantMail($orderId) {
        $data = [];
        $data['order'] = $order = $this->order_master->where('id', $orderId)
        ->with([
            'shippingAddress.getCountry',
            'billingAddress.getCountry',
            'getCountry'
        ])->first();

        $dtls_mail = $this->order_details->with([
            'sellerDetails',
            'getOrderSeller' => function($q) use($orderId) {
                $q->where('order_master_id', $orderId);
            }
        ])
        ->where([
            'order_master_id' => $order->id
        ])
        ->groupBy('seller_id')
        ->get();
        foreach(@$dtls_mail as $contact){
            
            # send mail order details to seller mail
            $contactList = [];
            $data['bcc'] = $contactList;
            $i = 0;
            //start
            $data['user_type'] = 'C';
            $data['orderDetails'] = OrderDetail::where([
                'order_master_id' => $order->id,
                'seller_id'       => @$contact->seller_id
            ])
            ->with([
                'defaultImage',
                'productByLanguage',
                'productVariantDetails',
            ])
            ->get();

            $data['hide_customer_info'] = $contact->sellerDetails->hide_customer_info;
            $data['sub_total'] =  $this->order_details->where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
            $data['order_total'] =  $this->order_details->where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
            $data['total_discount'] = $this->order_sellers->where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total_discount');
            $data['shipping_price'] = "NA";
            $data['language_id'] = getLanguage()->id;
            if(@$order->status == 'N'){
                $data['subject'] = "New Order Placed! Order No: ".@$order->order_no."! Order Details!";
            }else if(@$order->status =='DA'){
                $data['subject'] = "Order is Accepted! Order No: ".@$order->order_no."! Order Details!";    
            }else if(@$order->status =='RP'){
                $data['subject'] = "Order is ready for pickup! Order No: ".@$order->order_no."! Order Details!";    
            }else if(@$order->status =='OP'){
                $data['subject'] = "Order is picked up! Order No: ".@$order->order_no."! Order Details!";
                # driver rating link
                if(@$contact->driver_id) {
                    $data['driver_rating_link'] = url('merchant/driver-review/' . @$order->order_no . '/' . $contact->seller_id .'/' . @$contact->getOrderSeller->verify_code);
                }
            }else if(@$order->status == 'OD'){
                $data['subject'] = "Order is Delivered! Order No: ".@$order->order_no."! Order Details!";    
            }

            // Fill the Bcc email address
            $contactList[$i] = $contact->sellerDetails->email;
            $i++;
            #send mail to merchant if status driver assigned(client requirement)
            if(@$order->status =='DA'){
                if($contact->sellerDetails->email1) {
                    $contactListEmail1[$i] = $contact->sellerDetails->email1;
                    $i++;
                }
                if($contact->sellerDetails->email2) {
                    $contactListEmail2[$i] = $contact->sellerDetails->email2;
                    $i++;
                }
                if($contact->sellerDetails->email3) {
                    $contactListEmail3[$i] = $contact->sellerDetails->email3;
                    $i++;
                }    
            }
            $contactList = array_unique($contactList);
            if(@$contactListEmail1 != '') {
                $contactListEmail1 = array_unique($contactListEmail1);
                $contactList = array_merge($contactList, $contactListEmail1);
            }
            if(@$contactListEmail2 != '') {
                $contactListEmail2 = array_unique($contactListEmail2);
                $contactList = array_merge($contactList, $contactListEmail2);
            }
            if(@$contactListEmail3 != '') {
                $contactListEmail3 = array_unique($contactListEmail3);
                $contactList = array_merge($contactList, $contactListEmail3);
            }
            $data['bcc'] = array_unique($contactList);
            if($order->status == 'OD'){
                if($contact->sellerDetails->notify_merchant_on_delivery_internal == 'Y') {
                    Mail::queue(new SendOrderDetailsMerchantMail($data));
                }
            } else {
                
                if($order->status == 'N' || $order->status == 'DA' || $order->status == 'OP'){
                    Mail::queue(new SendOrderDetailsMerchantMail($data));
                }
                
            }
            $this->sendNotificationDashboardUpdate($contact->order_master_id,@$contact->seller_id);
        }
    }

    // /*
    // * Method: sendNotificationDashboardUpdate 
    // * Description : This method is used to update dashboard automatically specialy merchant dashboard client requirement using web notification (ajax call)
    // * Author: Jayatri
    // */
    private function sendNotificationDashboardUpdate($orderId, $merchantId) {
        // For Notification sending
        $order_dtt = OrderMaster::where('id',$orderId)->first();
        $new_registrationIds = MerchantRegId::where(['user_id' => $merchantId])->pluck('reg_id')->toArray();
        if(@$order_dtt) {
            $username = "Testing ...";
            $click_action = route('merchant.dashboard');
            $title = $username." External Order placed!";
            $message = "New order Placed!";
            $fields = array (
                'registration_ids' => $new_registrationIds,
                'data' => array (
                    "message"       => $message,
                    "title"         => $title,
                    "image"         => url('firebase-logo.png'),
                    "click_action"  => $click_action,
                ),
                'notification'      => array (
                    "body"          => $message,
                    "title"         => $title,
                    "click_action"  => $click_action,
                    "icon"          => "url('firebase-logo.png')",
                )
            );
            $fields = json_encode ( $fields );
            $API_ACCESS_KEY = 'AAAAcQGyZzs:APA91bEst24UvPwO2UpmnRh6OUH_Y_On82ZRIm1ekOSKcekCuOc1m2yYR2w6CGDt09nBY8dmL5h_8C5Cs_KMgIPd1m0mZ8oMUDWz75KaDJmJeNOkTFklIS-hYgWYA3qHnbO-zb76jNK-';
            $headers = array(
                'Authorization: key=' . $API_ACCESS_KEY,
                'Content-Type: application/json',
            );
            $ch = curl_init ();
            curl_setopt ( $ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt ( $ch, CURLOPT_POST, true );
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
            $result = curl_exec ( $ch );
            curl_close ( $ch );
            return $result.' New Order placed! ';
        }
        // End of Notification sending
    }

    /*
    * Method        : sendMailDriverAssignEntireOrder
    * Description   : This method is used to sendMailDriverAssignEntireOrder
    * Author        : Jayatri
    */
    private function sendMailDriverAssignEntireOrder($order) {
        //dd($order);
        if(@$order->order_type == 'E') {
            $details = $this->order_details->where('order_master_id',@$order->id)->first();
            $merchant = $this->merchants->where(['id'=>@$details->seller_id])->first();
            $country_nm_txt = CountryDetails::where(['country_id'=>@$order->shipping_country,'language_id'=>1])->first();
            // start
            // $update_ord_sellers
            $orderMasterDetails                         = new \stdClass();
            $orderMasterDetails->order_no               = @$order->order_no;
            $orderMasterDetails->order_type             = @$order->order_type;
            $orderMasterDetails->payment_method         = @$order->payment_method;
            $orderMasterDetails->order_total            = @$order->order_total;
            $orderMasterDetails->product_total_weight   = @$details->weight;
            $orderMasterDetails->invoice_no             = @$order->invoice_no;
            $orderMasterDetails->invoice_details        = @$order->invoice_details;
            $orderMasterDetails->delivery_date          = @$order->delivery_date;
            $orderMasterDetails->delivery_time          = @$order->delivery_time;
            $orderMasterDetails->shipping_fname         = @$order->shipping_fname ;
            $orderMasterDetails->shipping_lname         = @$order->shipping_lname ;
            $orderMasterDetails->shipping_email         = @$order->shipping_email ;
            $orderMasterDetails->shipping_phone         = @$order->shipping_phone ;
            $orderMasterDetails->shipping_country       = @$country_nm_txt->name;
            $orderMasterDetails->shipping_city          = @$order->shipping_city ;
            $orderMasterDetails->shipping_state         = @$order->shipping_state ;
            $orderMasterDetails->shipping_street        = @$order->shipping_street ;
            $orderMasterDetails->shipping_avenue        = @$order->shipping_avenue ;
            $orderMasterDetails->shipping_building      = @$order->shipping_building;
            $orderMasterDetails->shipping_block         = @$order->shipping_block;
            $orderMasterDetails->shipping_zip           = @$order->shipping_zip ;
            $orderMasterDetails->shipping_address       = @$order->shipping_address ;
            $orderMasterDetails->shipping_price         = @$order->shipping_price ;
            $orderMasterDetails->subtotal               = @$order->subtotal ;
            $orderMasterDetails->status                 = @$order->status ;
            $orderMasterDetails->created_at             = @$order->created_at ;
            $orderMasterDetails->updated_at             = @$order->updated_at ;
            $orderMasterDetails->email                  = @$order->shipping_email;
            $orderMasterDetails->subject                = "Order details!";
            $data['orderMasterDetails']                 = $orderMasterDetails;
            $setting = $this->settings->where('id','!=',0)->first();
            $count = 0;
            $contactList = [];
            #sent to admin 
            
            if(@$order->status == 'N'){

                $count = 0;
                $orderMasterDetails->email   = $setting->email_1;
                $orderMasterDetails->subject = "New External order placed! Order No: ".@$order->order_no."";
                if(@$setting->email_2){
                    $contactList[$count] = $setting->email_2; 
                    $count++;
                }
                if(@$setting->email_3){
                    $contactList[$count] = $setting->email_3;
                    $count++;
                }
                $contactList = array_unique($contactList);
                
                $data['bcc'] = $contactList;
                if(@$data['orderMasterDetails']->email) {
                    Mail::send(new SendMailDriverNotifydetails($data));
                    $this->sendNotificationDashboardUpdate($order->id,@$details->seller_id);
                }
                // Send mail to customer
                $orderMasterDetails->email   = @$order->email;
                $orderMasterDetails->subject = "New order placed! Order No: ".@$order->order_no."";
                if(@$data['orderMasterDetails']->email) {
                    Mail::send(new SendMailDriverNotifydetails($data));
                    $this->sendNotificationDashboardUpdate($order->id,@$details->seller_id);
                }

                // Send mail to merchant
                $orderMasterDetails->email   = @$merchant->email;
                
                $orderMasterDetails->subject = "New order placed! Order No: ".@$order->order_no."";
                if(@$data['orderMasterDetails']->email) {
                    Mail::send(new SendMailDriverNotifydetails($data));
                    $this->sendNotificationDashboardUpdate($order->id,@$details->seller_id);
                }
            }
            #sent to merchant
            if(@$order->status == 'DA'){
                $count = 0;
                $orderMasterDetails->email   = @$merchant->email;
                $orderMasterDetails->subject = "Order is accepted! External order details! Order No : ".@$order->order_no;
                if(@$merchant->email1){
                    $contactList[$count] = @$merchant->email1;
                    $count++;
                }
                if(@$merchant->email2){
                    $contactList[$count] = @$merchant->email2;
                    $count++;
                }
                if(@$merchant->email3){
                    $contactList[$count] = @$merchant->email3;
                    $count++;
                }
                if(@$merchant->email){
                    $contactList[$count] = @$merchant->email;
                    $count++;
                }

                $contactList = array_unique($contactList);
                $data['bcc'] = $contactList;
                if(@$data['orderMasterDetails']->email) {
                    Mail::send(new SendMailDriverNotifydetails($data));
                    $this->sendNotificationDashboardUpdate($order->id,@$details->seller_id);
                }
                // Send mail to customer

            }
            # send merchant on order picked up
            if(@$order->status == 'OP'){
                $count = 0;
                $orderMasterDetails->email   = @$merchant->email;
                $orderMasterDetails->subject = "Your order is picked up! Order No:".@$order->order_no." ! Order Details!";
                if(@$merchant->email1){
                    $contactList[$count] = @$merchant->email1;
                    $count++;
                }
                if(@$merchant->email2){
                    $contactList[$count] = @$merchant->email2;
                    $count++;
                }
                if(@$merchant->email3){
                    $contactList[$count] = @$merchant->email3;
                    $count++;
                }
                if(@$merchant->email){
                    $contactList[$count] = @$merchant->email;
                    $count++;
                }

                $contactList = array_unique($contactList);
                $get_verify_code = $this->order_sellers->where(['order_master_id' =>@$order->id,'seller_id' => @$merchant->id])->first();
                $data['driver_rating_link'] = url('merchant/driver-review/' . @$order->order_no . '/' . @$merchant->id . '/' .@$get_verify_code->verify_code);
                $data['bcc'] = $contactList;
                if(@$data['orderMasterDetails']->email) {
                    // dd($data);
                    Mail::send(new SendMailDriverNotifydetails($data));
                    $this->sendNotificationDashboardUpdate($order->id,@$details->seller_id);
                }
            }

            #send to customer
            if(@$order->status == 'OD'){
                $count = 0;
                if(@$merchant->notify_customer_by_email_external == 'Y') {
                    $orderMasterDetails->email   = @$order->email;
                    $orderMasterDetails->subject = "Your order is delivered successfully! External order details! Order no: ".@$order->order_no;

                    $contactList = array_unique($contactList);
                    $data['bcc'] = $contactList;
                    if(@$data['orderMasterDetails']->email) {
                        // $data['driver_rating_link'] = url('driver-review/' . @$order->order_no . '/' . @$order->user_id . '/' .@$order->verify_code);
                        Mail::send(new SendMailDriverNotifydetails($data));
                        $this->sendNotificationDashboardUpdate($order->id,@$details->seller_id);
                    }
                }

                # notify merchant on external order delivered
                if(@$merchant->notify_merchant_on_delivery_external == 'Y') {
                    $orderMasterDetails->email   = @$merchant->email;
                    $orderMasterDetails->subject = "Order has been delivered successfully! External order details! Order no: ".@$order->order_no;

                    $contactList = array_unique($contactList);
                    $data['bcc'] = $contactList;
                    if(@$data['orderMasterDetails']->email) {
                        $data['driver_rating_link'] = '';
                        Mail::send(new SendMailDriverNotifydetails($data));
                        $this->sendNotificationDashboardUpdate($order->id,@$details->seller_id);
                    }
                }
            }
            
        } else if(@$order->order_type == 'I') {
            $order = $this->order_master->with([
                'orderMasterDetails.productByLanguage',
                'orderMasterDetails.productDetails',
                'orderMasterDetails.productDetails.productUnitMasters',
                'orderMasterDetails.productVariantDetails',
                'customerDetails',
                'getBillingCountryCode',
                'orderMerchants.orderSeller'
            ])
            ->where(['id' => @$order->id])
            ->first();
            
            $this->sendMerchantMail(@$order->id);

            $sendEmail = 0;
            // foreach ($order->orderMerchants as $key => $value) {
            //     if($value->orderSeller->notify_customer_by_email_internal == 'Y') {
            //         $sendEmail += 1; 
            //     }
            // }
            // if($sendEmail > 0) {
            //     $this->sendCustomerMail(@$order->id);    
            // }
            $this->sendCustomerMail(@$order->id); 
        }
    }

    /**
    * Method: completeOrder
    * Description: This method is used to complete order
    * Author: Sanjoy
    */
    public function completeOrder($id) {
        $order = OrderMaster::find($id);
        
        $this->increaseTimesOfSold($order->id);

        # set total commistion in order_seller table
        $this->setMerchantCommisionIncompleteOrder($order);

        # insert to payment table.
        $this->insertPayment($order);

        # deduct loyalty point if customer registered
        /*if(@Auth::user()) {
            $this->updateOrderLoyaltyPoint($request, $order);
            $this->deductLoyaltyPointCompleteOrder($order->id);
        }*/

        # update order status if payment of is COD
        $this->updateOrderStatus($order->id);

        # drcrease stock(quantity) when order placed
        $this->decreaseStock(@$order);

        # send whatsapp message to merchant
        $this->whatsApp->sendMessageMerchants($order->id);

        # send mail to customer and seller
        $this->sendCustomerAndSellerMail($order->id);

        session()->flash('success', ['message' => 'Success', 'meaning' => 'Now order is completed.']);
        return redirect()->back();
    }

    private function sendCustomerAndSellerMailOld($orderId) {
        $data = [];
        $data['order'] = $order = OrderMaster::where('id', $orderId)
        ->with([
            'shippingAddress.getCountry',
            'billingAddress.getCountry',
            'getCountry'
        ])
        ->first();

        $data['orderDetails'] = OrderDetail::where(['order_master_id' => $orderId])
        ->with([
            'sellerDetails',
            'defaultImage',
            'productByLanguage',
            'productVariantDetails'
        ])
        ->get();
        
        $contactList = [];
        $i=0;
        $user = User::whereId($order->user_id)->first();
        $data['fname']          = $user->fname;
        $data['lname']          = $user->lname;  
        $data['email']          = $user->email;
        $data['user_type']      = $user->user_type;
        $data['shipping_price'] = @$order->shipping_price;
        $data['orderNo']        = @$order->order_no;
        $data['language_id']    = getLanguage()->id;
        $data['sub_total']      = @$order->subtotal;
        $data['order_total']    = @$order->order_total;
        $data['total_discount'] = @$order->total_discount;

        $contactList[$i]        = @$order->shipping_email;
        if(@$order->status == 'N'){
            $data['subject'] = "Your order has been placed successfully! Order no: ".@$order->order_no;
        } else if(@$order->status == 'DA') {
            $data['subject'] = "Your order is accepted! Order No:".@$order->order_no." ! Order Details!";
        } else if(@$order->status == 'RP') {
            $data['subject'] = "Your order is ready for pick up! Order No:".@$order->order_no." ! Order Details!";
        } else if(@$order->status == 'OP') {
            $data['subject'] = "Your order is picked up! Order No:".@$order->order_no." ! Order Details!";
        } else if(@$order->status == 'OD') {
            $data['subject'] = "Your order is delivered successfully! Order No:".@$order->order_no." ! Order Details!";
        }

        #send mail admin to notify for new order only as client requirement (internal order)
        $i=0;
        if(@$order->status =='N'){
            $setting = Setting::first();
            if(@$setting->email_1) {
                $contactList[$i] = $setting->email_1;
                $i++;
            }
            if(@$setting->email_2) {
                $contactList[$i] = $setting->email_2;
                $i++;
            }
            if(@$setting->email_3) {
                $contactList[$i] = $setting->email_3;
                $i++;
            }
        }
        $contactList = array_unique($contactList);
        $data['bcc'] = $contactList;

        #send to customer mail(order status can be new/driver assigned)
        Mail::queue(new OrderMail($data));
        
        # send mail order details to seller mail
        $contactList = [];
        $data['bcc'] = $contactList;
        $dtls_mail = OrderDetail::with([
            'sellerDetails'
        ])
        ->where(['order_master_id' => $order->id])
        ->groupBy('seller_id')
        ->get();
        
        foreach(@$dtls_mail as $contact) {
            $contactList = [];
            $data['bcc'] = [];
            $contactListEmail3 = [];
            $i = 0;
            //start
            $data['user_type'] = 'C';
            $data['hide_customer_info'] = $contact->sellerDetails->hide_customer_info;

            $data['orderDetails'] = OrderDetail::where([
                'order_master_id' => $order->id,
                'seller_id'       => @$contact->seller_id
            ])
            ->with([
                'sellerDetails',
                'defaultImage',
                'productByLanguage',
                'productVariantDetails'
            ])
            ->get();
            
            $data['sub_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
            $data['order_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
            $data['total_discount'] = OrderSeller::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total_discount');
            $data['shipping_price'] = "NA";
            if(@$order->status == 'N') {
                $data['subject'] = "New Order Placed!Order No : ".@$order->order_no." ! Order Details!";
            } else if(@$order->status =='DA') {
                $data['subject'] = "Order is Accepted! Order No : ".@$order->order_no." ! Order Details!";    
            } else if(@$order->status =='RP') {
                $data['subject'] = "Order is ready for pickup! Order No : ".@$order->order_no." ! Order Details!";    
            } else if(@$order->status =='OP') {
                $data['subject'] = "Order is picked up! Order No : ".@$order->order_no." ! Order Details!";    
            } else if(@$order->status =='OD') {
                $data['subject'] = "Order is Delivered! Order No : ".@$order->order_no." ! Order Details!";    
            }
            
            // Fill the Bcc email address
            $contactList[$i] = $contact->sellerDetails->email;
            $i++;
            #send mail to merchant if status driver assigned(client requirement)
            // if(@$order->status =='DA'){
            //     if($contact->sellerDetails->email1) {
            //         $contactListEmail1[$i] = $contact->sellerDetails->email1;
            //         $i++;
            //     }
            //     if($contact->sellerDetails->email2) {
            //         $contactListEmail2[$i] = $contact->sellerDetails->email2;
            //         $i++;
            //     }
            //     if($contact->sellerDetails->email3) {
            //         $contactListEmail3[$i] = $contact->sellerDetails->email3;
            //         $i++;
            //     }    
            // }
            $contactList = array_unique($contactList);
            if(@$contactListEmail1 != '') {
                $contactListEmail1 = array_unique($contactListEmail1);
                $contactList = array_merge($contactList, $contactListEmail1);
            }
            if(@$contactListEmail2 != '') {
                $contactListEmail2 = array_unique($contactListEmail2);
                $contactList = array_merge($contactList, $contactListEmail2);
            }
            if(@$contactListEmail3 != '') {
                $contactListEmail3 = array_unique($contactListEmail3);
                $contactList = array_merge($contactList, $contactListEmail3);
            }
            $data['bcc'] = $contactList;
            #send web notification to merchant only for update merchant dashboard
            Mail::queue(new SendOrderDetailsMerchantMail($data));
            $this->sendNotificationDashboardUpdate($contact->order_master_id, $contact->seller_id);
        }
    }

    /**
    * Method: increaseTimesOfSold
    * Description: This method is used to increase product times of sold.
    * Author: Sanjoy
    */
    public function increaseTimesOfSold($orderId) {
        $orderDtl = OrderDetail::where(['order_master_id' => $orderId])->get();
        foreach ($orderDtl as $key => $value) {
            Product::where(['id' => $value->product_id])->increment('times_of_sold', $value->quantity);
        }
    }

    /**
    * Method: setCommision
    * Description: This method is used to set commission
    * Author: Sanjoy
    */
    private function setMerchantCommisionIncompleteOrder($orderChk) {
        $orderSel = $this->orderSeller->where(['order_master_id' => $orderChk->id])->get(); 
        foreach ($orderSel as $sp) {
            $sellerCommision = $this->merchant->whereId($sp->seller_id)->first();
            $total = $this->orderDetails->where([
                'order_master_id' => $orderChk->id,
                'seller_id'       => $sp->seller_id
            ])->sum('total');
            $totalCommision = ($sellerCommision->commission*$total)/100;
            if($totalCommision < $sellerCommision->minimum_commission){
                if($sellerCommision->minimum_commission > $orderChk->order_total) {
                    $update['total_commission'] = $orderChk->order_total; 
                } else {
                    $update['total_commission'] = $sellerCommision->minimum_commission; 
                }
            } else {
                $update['total_commission'] = $totalCommision; 
            }
            $this->orderSeller->whereId($sp->id)->update($update);
        }
    }

    /**
    * method: insertPayment
    * Description: This method is used to insert to payment table.
    * Author: Sanjoy
    */
    private function insertPayment($reqData = []) {
        Payment::create([
            'user_id'       => $reqData->user_id,
            'order_id'      => $reqData->id,
            'amount'        => $reqData->order_total,
            'payment_mode'  => $reqData->payment_method,
            'status'        => 'I' 
        ]);
    }

    /**
    * Method: updateOrderLoyaltyPoint
    * Description: This method is used to update loyalty point to order master table.
    * Author: Sanjoy
    */
    private function updateOrderLoyaltyPoint($request, $order) {
        $setting = Setting::first();
        if(@$request->reward_point) {    
            if(@$request->reward_point == 'Y') {  
                $totalOrder = $request->loyalty_point*$setting->one_point_to_kwd;
                $this->order->whereId($order->id)->increment('loyalty_point_used', $request->loyalty_point);
                $this->order->whereId($order->id)->decrement('order_total', number_format($totalOrder, 2));
                $this->order->whereId($order->id)->update(['loyalty_amount' => number_format($totalOrder, 2)]);
            }
        }
    }

    /**
    * Method: calculateLoyaltyPoint
    * Description: This method is used to deduct loyalty point from customer account.
    * Author: Sanjoy
    */
    private function deductLoyaltyPointCompleteOrder($orderId) {
        $setting = Setting::first();
        $order = OrderMaster::where(['id' => $orderId])->first();
        if(@$order->loyalty_point_used > 0) {  
            $totalOrder = $order->loyalty_point_used*$setting->one_point_to_kwd;
            
            # insert to user reward table
            $addUserReward = new UserReward;
            $addUserReward['user_id']        = $order->user_id;
            $addUserReward['order_id']       = $orderId;
            $addUserReward['earning_points'] = $order->loyalty_point_used;
            $addUserReward['debit']          = '1';
            $addUserReward->save();

            # update customer total reward
            $this->user->whereId($order->user_id)->decrement('loyalty_balance', $order->loyalty_point_used);
            $this->user->whereId($order->user_id)->increment('loyalty_used', $order->loyalty_point_used);
        }
    }

        /**
    * Method: updateOrderStatus
    * Description: This method is used to change status of order_master and order_details
    * Author: Sanjoy
    */
    private function updateOrderStatus($orderId) {
        $ordDetl = $this->order_details->where(['order_master_id' => $orderId])->get()->pluck('seller_id');
        $sellerDriver = $this->merchant->select('id','driver_id')->whereIn('id', $ordDetl)->get();
        foreach ($sellerDriver as $sd) {
            if($sd->driver_id != '0') {
                $assignDriver['driver_id'] = $sd->driver_id;    
                $assignDriver['status'] = 'DA';    
                $this->order_details->where(['seller_id' => $sd->id,'order_master_id' => $orderId])->update($assignDriver);
                $this->orderSeller->where(['seller_id' => $sd->id,'order_master_id' => $orderId])->update($assignDriver);
            }
        }
        $drvAssndChck = $this->order_details->where(['order_master_id' => $orderId, 'driver_id' => '0'])->count();
        if($drvAssndChck > 0) {
            $order['status'] = 'N';   
        } else {
            $order['status'] = 'DA';   
        } 
        $this->order->where(['id' => $orderId])->update($order);
    }

    /**
    */
    public function deleteMarkOrders(Request $request) {
        $reqData = $request->params;
        foreach($reqData['order_id'] as $orderId) {
            $this->cancelOrderMarkDelete($orderId);

            $this->deleteMarkOrder($orderId);
        }
    }

    /*
    * Method        : cancelOrderMarkDelete
    * Description   : This method is used to cancel Order for multiple order deleted.
    * Author        : Sanjoy
    */
    public function cancelOrderMarkDelete($id){
        $order = OrderMaster::where('id',$id)->first();
        $ps = @$order->status;
        if(@$order){
            # return  for incomplete and failed order
            if($order->status == 'I' || $order->status == 'F') {
                return 1;
            }

            # when cancel order from any status
            if($order->order_type == 'I') {
                if(@$order->status != 'OC'){
                    #increase stock
                    $this->increaseStock(@$order);
                    $this->deductLoyaltyPoint($order);
                }
                # if delivered to cancel
                if($order->status == 'OD') {
                    OrderMaster::where(['id' => $id])->update(['loyalty_point_received' => 0]);
                }
            }

            if(@$order->status == 'OP' || @$order->status == 'N' || @$order->status == 'RP') {
                $update['status'] = 'OC';
                OrderMaster::where('id',$id)->update($update);
                $updaten['status'] = 'OC';
                $this->order_details->where('order_master_id',$id)->where('status', '!=', 'C')->update($updaten);
                OrderSeller::where('order_master_id',$id)->where('status', '!=', 'C')->update($updaten);
                return 1;
            }

            if(@$order->status == 'OD' && @$order->order_type == 'E') {
                OrderMaster::where(['id'=>@$request->id])->update(['total_commission'=>0]);
                $this->order_sellers->where(['order_master_id'=>@$request->id])->update(['total_commission'=>0]);
                $details = $this->order_details->where('order_master_id',@$request->id)->first();
                $merchant = $this->merchants->where(['id'=>@$details->seller_id])->first();
                $total_earning = $merchant->total_earning - @$order->subtotal;
                $total_due = $merchant->total_due - @$order->subtotal;
                $updateM['total_earning'] = $total_earning;
                $updateM['total_due'] = $total_due;
                $this->merchants->where(['id'=>@$details->seller_id])->update($updateM);    
                $update['status'] = 'OC';
                OrderMaster::where('id',$id)->update($update);
                $updaten['status'] = 'OC';
                $this->order_details->where('order_master_id',$id)->update($updaten);
                OrderSeller::where('order_master_id',$id)->where('status', '!=', 'C')->update($updaten);
                return 1;
            } else if(@$order->status == 'OD' && @$order->order_type == 'I') {
                $flag = $this->setCommisionDeduct($order);
                if($flag == 0) {
                    return 0;
                } else {
                    $update['status'] = 'OC';
                    OrderMaster::where('id',$id)->update($update);
                    $updaten['status'] = 'OC';
                    $this->order_details->where('order_master_id',$id)->where('status', '!=', 'C')->update($updaten);
                    return 1;
                }
            } else {
                $update['status'] = 'OC';
                OrderMaster::where('id',$id)->update($update);
                $updaten['status'] = 'OC';
                $this->order_details->where('order_master_id',$id)->where('status', '!=', 'C')->update($updaten);
                OrderSeller::where('order_master_id',$id)->where('status', '!=', 'C')->update($updaten);
                return 1;
            }
        }
    }

    /*
    * Method: deleteMarkOrder
    * Description: This method is used to delete  Order.
    * Author: Sanjoy
    */
    public function deleteMarkOrder($id){
        $order = OrderMaster::where('id', @$id)->first();
        if(@$order){
            $update['status'] = 'D';
            $this->order_master->where('id',@$id)->update($update);
            $this->order_details->where('order_master_id',@$id)->update($update);
            $this->order_sellers->where('order_master_id',@$id)->update($update);
            return 1;
        }
    }

    public function editInternalOrder($id = NULL) {
        $data = [];
        $data['city']   = City::get();
        $data['bcity']  = City::get();
        $data['country'] = $this->country->with('countryDetailsBylanguage')->where('status','!=','D')->get();
        $data['language_id'] = getLanguage()->id;
        $data['setting'] = Setting::first();
        $data['order'] = $data['orderMaster'] = $order = OrderMaster::where('id', $id)
        ->with([
            'shippingAddress.getCountry',
            'billingAddress.getCountry',
            'getCountry',
            'getBillingCountry',
            'customerDetails'
        ])
        ->first();
        // dd($order);
        $data['orderNo'] = $order->order_no;
        
        // to get the products informationa from order_details
        $data['orderDetails'] = $this->orderDetails->where(['order_master_id' => $id])
        ->with([
            'sellerDetails',
            'defaultImage',
            'productByLanguage',
            'productVariantDetails',
            'productDetails.productUnitMasters',
            'productDetails:id,slug'
        ])
        ->get();
        $data['userAddr'] = $this->userAddr->where(['user_id' => $order->user_id])->with('getCountry')->get();
        return view('admin.modules.orders.edit_internal_order')->with($data);
    }

    public function updateInternalOrder(Request $request, $orderId = NULL) {
        $order['shipping_fname']        = $request->shipping_fname;
        $order['shipping_lname']        = $request->shipping_lname;
        $order['shipping_email']        = $request->shipping_email;
        $order['shipping_phone']        = $request->shipping_phone;
        $order['shipping_country']      = $request->shipping_country;
        $order['shipping_street']       = $request->shipping_street;
        $order['shipping_block']        = $request->shipping_block;
        $order['shipping_building']     = $request->shipping_building;
        $order['shipping_more_address'] = $request->shipping_more_address;

        // location, lat, lng
        $order['location']              = @$request->location;
        $order['lat']                   = @$request->lat;
        $order['lng']                   = @$request->lng;

        if($request->shipping_city) {
            $order['shipping_city']     = $request->shipping_city;
        } elseif($request->shipping_city_id) {
            $cityId = CitiDetails::where('name', $request->shipping_city_id)->first();
            $order['shipping_city_id']  = $cityId->city_id;   
            $order['shipping_city']     = $request->shipping_city_id;             
        }
        
        if($request->shipping_postal_code) {
            $order['shipping_postal_code']  = $request->shipping_postal_code;
        }
        
        if($request->same_as_shipping) {
            if($request->shp_save_addr) {
                $addrShp = $this->user_addr->whereId($request->shp_save_addr)->first();
                $order['billing_address_id']    = $request->shp_save_addr;
                $order['billing_fname']         = $addrShp->shipping_fname;
                $order['billing_lname']         = $addrShp->shipping_lname;
                $order['billing_email']         = $addrShp->email;
                $order['billing_phone']         = $addrShp->phone;
                $order['billing_country']       = $addrShp->country;
                $order['billing_city']          = $addrShp->city;
                $order['billing_street']        = $addrShp->street;
                $order['billing_block']         = $addrShp->block;
                $order['billing_building']      = $addrShp->building;
                $order['billing_postal_code']   = $addrShp->postal_code;
                $order['billing_more_address']  = $addrShp->more_address;
                if($addrShp->country == 134) {
                    $order['billing_city_id']   = $addrShp->city_id;   
                }
            } else {
                $order['billing_fname']         = $request->shipping_fname;
                $order['billing_lname']         = $request->shipping_lname;
                $order['billing_email']         = $request->shipping_email;
                $order['billing_phone']         = $request->shipping_phone;
                $order['billing_country']       = $request->shipping_country;
                // $order['billing_city']       = $request->shipping_city;
                $order['billing_street']        = $request->shipping_street;
                $order['billing_block']         = $request->shipping_block;
                $order['billing_building']      = $request->shipping_building;
                $order['billing_more_address']  = $request->shipping_more_address;
                if($request->shipping_postal_code) {
                    $order['billing_postal_code']  = $request->shipping_postal_code;
                }
                if($request->shipping_city) {
                    $order['billing_city']      = $request->shipping_city;
                } elseif($request->shipping_city_id) {
                    $cityId = CitiDetails::where('name', $request->shipping_city_id)->first();
                    $order['billing_city_id']   = $cityId->city_id;   
                    $order['billing_city']      = $request->shipping_city_id;         
                }
            }
        } else {
            $order['billing_fname']         = $request->billing_fname;
            $order['billing_lname']         = $request->billing_lname;
            $order['billing_email']         = $request->billing_email;
            $order['billing_phone']         = $request->billing_phone;
            $order['billing_country']       = $request->billing_country;
            // $order['billing_city']       = $request->billing_city;
            $order['billing_street']        = $request->billing_street;
            $order['billing_block']         = $request->billing_block;
            $order['billing_building']      = $request->billing_building;
            $order['billing_more_address']  = $request->billing_more_address;
            if($request->billing_postal_code) {
                $order['billing_postal_code']   = $request->billing_postal_code;
            }
            if($request->billing_city) {
                $order['billing_city']      = $request->billing_city;
            } elseif($request->billing_city_id) {
                $cityId = CitiDetails::where('name', $request->billing_city_id)->first();
                $order['billing_city_id']   = $cityId->city_id;
                $order['billing_city']      = $request->billing_city_id;          
            }
        }

        if($request->shp_save_addr) {
            $addrShp = $this->user_addr->whereId($request->shp_save_addr)->first();
            $order['shipping_address_id']   = $request->shp_save_addr;
            $order['shipping_fname']        = $addrShp->shipping_fname;
            $order['shipping_lname']        = $addrShp->shipping_lname;
            $order['shipping_email']        = $addrShp->email;
            $order['shipping_phone']        = $addrShp->phone;
            $order['shipping_country']      = $addrShp->country;
            $order['shipping_city']         = $addrShp->city;
            $order['shipping_street']       = $addrShp->street;
            $order['shipping_block']        = $addrShp->block;
            $order['shipping_building']     = $addrShp->building;
            $order['shipping_postal_code']  = $addrShp->postal_code;
            $order['shipping_more_address'] = $addrShp->more_address;

            // location, lat, lng
            $order['location']              = @$addrShp->location;
            $order['lat']                   = @$addrShp->lat;
            $order['lng']                   = @$addrShp->lng;
            if($addrShp->country == 134) {
                $order['shipping_city_id']  = $addrShp->city_id;  
            }
        }
        if($request->bill_save_addr) {
            $addrBill = $this->user_addr->whereId($request->bill_save_addr)->first();
            $order['billing_address_id']    = $request->bill_save_addr;
            $order['billing_fname']         = $addrBill->shipping_fname;
            $order['billing_lname']         = $addrBill->shipping_lname;
            $order['billing_email']         = $addrBill->email;
            $order['billing_phone']         = $addrBill->phone;
            $order['billing_country']       = $addrBill->country;
            $order['billing_city']          = $addrBill->city;
            $order['billing_street']        = $addrBill->street;
            $order['billing_block']         = $addrBill->block;
            $order['billing_building']      = $addrBill->building;
            $order['billing_postal_code']   = $addrBill->postal_code;
            $order['billing_more_address']  = $addrBill->more_address;
            if($addrShp->country == 134) {
                $order['billing_city_id']   = $addrBill->city_id;  
            }
        }
        $orderUpdate = $this->order->where('id', $orderId)->update($order);
        # calculate shipping price.
        $order = $this->order->where(['id' => $orderId])->first();
        
        $prevEarning = $this->merchantPrevEarning($order);
        
        $this->calculateShippingPrice($order);
        
        # calculate coupon code
        // $this->calculateCoupon($request, $order);

        # deduct loyalty point if customer registered
        $this->deductLoyaltyPoint($order);
        
        $order2 = $this->order->where(['id' => $orderId])->first();
        $this->setCommisionForInterOrder($order2);
        
        if($order2->status == 'OD'){
            $this->recalculateMerchantEarning($order2,$prevEarning);
        }
        return redirect()->back();
    }
    /**
    * Method: merchantPrevEarning
    * Description: This method is used to calculate merchant previous earning
    * Author: Argha
    * Date : 24-03-2021
    */
    private function merchantPrevEarning($order)
    {
        $sellers =  $this->orderSeller->where(['order_master_id' => $order->id])->groupBy('seller_id')->get();
        $arr = array();
        foreach ($sellers as $val) {
            $arr[$val->seller_id] = $val->earning;
        }
        return $arr;
    }
    /**
    * Method: recalculateMerchantEarning
    * Description: This method is used to recalculate merchant earning at the time of  Edit internal order
    * Author: Argha
    * Date : 24-03-2021
    */
    private function recalculateMerchantEarning($order,$prevEarning)
    {
        
        $sellers =  $this->orderSeller->where(['order_master_id' => $order->id])->groupBy('seller_id')->get();
        foreach ($sellers as $val) {
            foreach ($prevEarning as $k => $earn) {
                if($k == $val->seller_id){
                    $merchant = Merchant::where(['id' => $val->seller_id])->first();
                    $merchant_earning = (@$merchant->total_earning - @$earn ) + @$val->earning;
                    $merchant_due = (@$merchant->total_due - @$earn ) + @$val->earning;

                    Merchant::where(['id' => $val->seller_id])->update(['total_earning' => $merchant_earning , 'total_due' => $merchant_due]);

                }
            }
        }
    }
    /**
    * Method: calculateShippingPrice
    * Description: This method is used to calculate shipping price
    * Author: Sanjoy
    */
    private function calculateShippingPrice($orderMaster) {
        $sellers = $this->orderDetails->where(['order_master_id' => $orderMaster->id])->pluck('seller_id')->toArray();

        $uniqueSellers = array_unique($sellers);
        $setting = Setting::first();
        $insideShippingCost = 0;
        $outsideShippingCost = 0;
        $merHgCst = [];
        foreach ($uniqueSellers as $key => $value) {
            $merchant = Merchant::where(['id' => $value])->first();
            if($merchant->country == 134 && $orderMaster->shipping_country == 134) {
                # if city delivery applicable for merchant
                if($merchant->applicable_city_delivery == 'Y') {
                    $cityDelivery = City::where(['id' => $orderMaster->shipping_city_id])->first();
                    if(@$cityDelivery->delivery_fees > $merchant->inside_shipping_cost) {
                        $merHgCst[] = @$cityDelivery->delivery_fees;
                    } else {
                        $merHgCst[] = @$merchant->inside_shipping_cost;
                    }
                } else {
                    $merHgCst[] = @$merchant->inside_shipping_cost;
                }
            } else {
                $details = $this->orderDetails->where(['order_master_id' => $orderMaster->id, 'seller_id' => $value])->get();
                $weight = $details->sum('weight');

                # if seller from outside kuwait and buyer from kuwait
                if($orderMaster->shipping_country == 134) {
                    $zone = ZoneDetail::where(['country_id' => $merchant->country])->first();
                } else {
                    $zone = ZoneDetail::where(['country_id' => $orderMaster->shipping_country])->first();
                }
                $infinityChk = ZoneRateDetail::where(['zone_id' => $zone->zone_master_id])->where('infinity_weight', 'Y')->first();
                if(@$infinityChk && @$weight >= $infinityChk->from_weight) {
                    $sCost = $infinityChk;
                } else {
                    $sCost = ZoneRateDetail::where(['zone_id' => $zone->zone_master_id])
                    ->where(function($where) use ($weight) {
                        $where->where(function($where1) use ($weight) {
                            $where1->where('from_weight', '<=', @$weight)
                            ->where('to_weight', '>', @$weight);
                        })
                        ->orWhere(function($where2) use ($weight) {
                            $where2->where('from_weight', '<', @$weight)
                            ->where('to_weight', '>=', @$weight);
                        });
                    })
                    ->first();
                }
                $outsideShippingCost += @$sCost->internal_outside_kuwait;
            }
        }
        if(count($merHgCst)) {
            $insideShippingCost = max($merHgCst);
        }
        $orderDet = $this->order->where(['id' => $orderMaster->id])->first();
        
        $this->order->where(['id' => $orderMaster->id])->update([
            'shipping_price'    => 0, 
            'order_total'       => $orderDet->order_total - $orderDet->shipping_price
        ]);

        $orderDet = $this->order->where(['id' => $orderMaster->id])->first();
        $this->order->where(['id' => $orderMaster->id])
        ->update([
            'shipping_price'    => $outsideShippingCost + $insideShippingCost,
            'order_total'       => $orderDet->order_total + $outsideShippingCost + $insideShippingCost
        ]);
    }

    private function calculateShippingPrice1($orderMaster) {
        $sellers = $this->orderDetails->where(['order_master_id' => $orderMaster->id])->pluck('seller_id')->toArray();
        $uniqueSellers = array_unique($sellers);
        $setting = Setting::first();
        $insideShippingCost = 0;
        $outsideShippingCost = 0;
        $merHgCst = [];
        foreach ($uniqueSellers as $key => $value) {
            $merchant = Merchant::where(['id' => $value])->first();
            if($merchant->country == 134 && $orderMaster->shipping_country == 134) {
                # if city delivery applicable for merchant
                if($merchant->applicable_city_delivery == 'Y') {
                    $cityDelivery = City::where(['id' => $orderMaster->shipping_city_id])->first();
                    if(@$cityDelivery->delivery_fees > $merchant->inside_shipping_cost) {
                        $merHgCst[] = @$cityDelivery->delivery_fees;
                    } else {
                        $merHgCst[] = @$merchant->inside_shipping_cost;
                    }
                } else {
                    $merHgCst[] = @$merchant->inside_shipping_cost;
                }
            } else {
                $details = $this->orderDetails->where(['order_master_id' => $orderMaster->id, 'seller_id' => $value])->get();
                $weight = $details->sum('weight');

                # if seller from outside kuwait and buyer from kuwait
                if($orderMaster->shipping_country == 134) {
                    $zone = ZoneDetail::where(['country_id' => $merchant->country])->first();
                } else {
                    $zone = ZoneDetail::where(['country_id' => $orderMaster->shipping_country])->first();
                }
                $infinityChk = ZoneRateDetail::where(['zone_id' => $zone->zone_master_id])->where('infinity_weight', 'Y')->first();
                if(@$infinityChk && @$weight >= $infinityChk->from_weight) {
                    $sCost = $infinityChk;
                } else {
                    $sCost = ZoneRateDetail::where(['zone_id' => $zone->zone_master_id])
                    ->where(function($where) use ($weight) {
                        $where->where(function($where1) use ($weight) {
                            $where1->where('from_weight', '<=', @$weight)
                            ->where('to_weight', '>', @$weight);
                        })
                        ->orWhere(function($where2) use ($weight) {
                            $where2->where('from_weight', '<', @$weight)
                            ->where('to_weight', '>=', @$weight);
                        });
                    })
                    ->first();
                }
                $outsideShippingCost += @$sCost->internal_outside_kuwait;
            }
        }
        if(count($merHgCst)) {
            $insideShippingCost = max($merHgCst);
        }
        $orderDet = $this->order->where(['id' => $orderMaster->id])->first();
        
        // $this->order->where(['id' => $orderMaster->id])->update([
        //     'shipping_price'    => 0, 
        //     'order_total'       => $orderDet->order_total - $orderDet->shipping_price
        // ]);

        $orderDet = $this->order->where(['id' => $orderMaster->id])->first();
        $this->order->where(['id' => $orderMaster->id])
        ->update([
            'shipping_price'    => $outsideShippingCost + $insideShippingCost,
            'order_total'       => $orderDet->order_total + $outsideShippingCost + $insideShippingCost
        ]);
    }

    public function updateInternalOrderQty(Request $request) {
        $response = [
            'jsonrpc' => '2.0'
        ];
        $reqData = $request->params;
        $orderDet = OrderDetail::where(['id' => $reqData['order_details_id']])->first();
        $prevQty = $orderDet->quantity;
        $diff = $reqData['quantity'] - $prevQty;
        $orderMaster = OrderMaster::find($reqData['order_master_id']); 
        if($diff < 0){
            $diff = abs($diff);
            if($reqData['product_variant_id']){
                $getStock = $this->productVariant->where(['id' => $reqData['product_variant_id']])->first();
                $currentStock = $getStock->stock_quantity + $diff;
                if($currentStock < 0){
                    $response['error']['code'] = '5062';
                    $response['error']['meaning'] = 'Please add stock for this items!Stock is not enough & will get nagative!';
                    return response()->json($response);
                }else{
                    //if($orderMaster->status == 'OD'){
                        $this->productVariant->where(['id' => $reqData['product_variant_id']])->update(['stock_quantity' => $currentStock]);
                    //}
                    
                }
                
            }else{
                $getStock = $this->product->where(['id' => $reqData['product_id']])->first();
                $currentStock = $getStock->stock + $diff;
                if($currentStock < 0){
                    $response['error']['code'] = '5062';
                    $response['error']['meaning'] = 'Please add stock for this items!Stock is not enough & will get nagative!';
                    return response()->json($response);
                }else{
                    //if($orderMaster->status == 'OD'){
                        $this->product->where(['id' => $reqData['product_id']])->update(['stock' => $currentStock]);
                    //}
                }
            }
        }else{
            if($reqData['product_variant_id']){
                $getStock = $this->productVariant->where(['id' => $reqData['product_variant_id']])->first();
                $currentStock = $getStock->stock_quantity - $diff;
                if($currentStock < 0){
                    $response['error']['code'] = '5062';
                    $response['error']['meaning'] = 'Please add stock for this items!Stock is not enough & will get nagative!';
                    return response()->json($response);
                }else{
                    //if($orderMaster->status == 'OD'){
                        $this->productVariant->where(['id' => $reqData['product_variant_id']])->update(['stock_quantity' => $currentStock]);
                    //}
                }
                
            }else{
                $getStock = $this->product->where(['id' => $reqData['product_id']])->first();
                $currentStock = $getStock->stock - $diff;
                if($currentStock < 0){
                    $response['error']['code'] = '5062';
                    $response['error']['meaning'] = 'Please add stock for this items!Stock is not enough & will get nagative!';
                    return response()->json($response);
                }else{
                    //if($orderMaster->status == 'OD'){
                        $this->product->where(['id' => $reqData['product_id']])->update(['stock' => $currentStock]);
                    //}
                }
            }
        }
        OrderDetail::where(['id' => $reqData['order_details_id']])->update(['quantity' => $reqData['quantity']]);

        $orderDetails = OrderDetail::where(['id' => $reqData['order_details_id']])->first();
        
        $this->updateOrderDetails($orderDetails);
        $this->updateOrderSeller($orderDetails);
        $this->updateOrderMasterPrice($reqData['order_master_id']);

        $orderMaster = OrderMaster::find($reqData['order_master_id']);
        $this->calculateShippingPrice1($orderMaster);

        //$this->setCommisionForInterOrder($orderMaster);

        return response()->json($response);
    }

    public function removeItemFromOrder($orderDetailId, $orderMasterId) {
        $orderDetail = OrderDetail::where(['id' => $orderDetailId])->first();
        //dd($orderDetail);
        $orderSeller = $this->orderSeller->where(['order_master_id' => $orderMasterId,'seller_id' => $orderDetail->seller_id])->first();
        $merchant = Merchant::where(['id' => $orderDetail->seller_id])->first();
        $prevEarning = $orderSeller->earning;
        $prevStock = $orderDetail->quantity;
        $varient_id = $orderDetail->product_variant_id;
        $product_id = $orderDetail->product_id;
        
        OrderDetail::where(['id' => $orderDetailId])->delete();

        $this->updateOrderSeller($orderDetail);
        
        $this->updateOrderMasterPrice($orderMasterId);
        
        $orderMaster = OrderMaster::find($orderMasterId);
        $this->calculateShippingPrice1($orderMaster);

        $this->setCommisionForInterOrder($orderMaster);
        if($varient_id){
            $getStock = $this->productVariant->where(['id' => $varient_id])->first();
            $previouStock = $getStock->stock_quantity;
            $currentStock = $previouStock + $prevStock;
            //if($orderMaster->status == 'OD'){
                $this->productVariant->where(['id' => $varient_id])->update(['stock_quantity' => $currentStock]);
            //}
        }else{
            $getStock = $this->product->where(['id' => $product_id])->first();
            $previouStock = $getStock->stock;
            $currentStock = $previouStock + $prevStock;
            //if($orderMaster->status == 'OD'){
                $this->product->where(['id' => $product_id])->update(['stock' => $currentStock]);
            //}
        }
        if($orderMaster->status == 'OD'){
            $orderSeller = $this->orderSeller->where(['order_master_id' => $orderMasterId,'seller_id' => $orderDetail->seller_id])->first();
            if($orderSeller){
                $current_earning = $orderSeller->earning;
            }else{
                $current_earning = 0;
            }
            $merchant_earning = (@$merchant->total_earning - @$prevEarning ) + @$current_earning;
            $merchant_due = (@$merchant->total_due - @$prevEarning ) + @$current_earning;

            Merchant::where(['id' => $orderDetail->seller_id])->update(['total_earning' => $merchant_earning , 'total_due' => $merchant_due]);
        }
        return redirect()->back();
    }


    /**
    * 
    */
    private function updateOrderDetails($orderDetailsIns) {
        $productVariant = $this->checkStockAndVariant($orderDetailsIns->product_variant_id);
        $product = $this->product->whereId($orderDetailsIns->product_id)->first();
        $originalPrice = 0;
        $discountedPrice = 0;
        $total_discount = 0;
        $subtotal = 0;
        $total = 0;
        $weight = 0;
        if(@$productVariant->price != 0) {
            #if discount price is available then calculate this.
            $subtotal = $total = $productVariant->price * $orderDetailsIns->quantity;
            $originalPrice = $productVariant->price;
            if($productVariant->discount_price && date('Y-m-d')>=$productVariant->from_date && date('Y-m-d')<=$productVariant->to_date) {
                $total_discount = ($productVariant->price - $productVariant->discount_price) * $orderDetailsIns->quantity;
                $discountedPrice = $productVariant->discount_price;
            } else {
                $total_discount = 0.000;
            }
            $total = $subtotal - $total_discount;
            $weight = $productVariant->weight * $orderDetailsIns->quantity;
        } else {
            $originalPrice = $product->price;
            $discountedPrice = $product->discount_price;
            if($product->discount_price != 0 && date('Y-m-d')>=$product->from_date && date('Y-m-d')<=$product->to_date) {
                $total_discount = ($product->price - $product->discount_price) * $orderDetailsIns->quantity;
            } else {
                $total_discount = 0.000;
            }
            $subtotal = $product->price * $orderDetailsIns->quantity;
            $total = $subtotal - $total_discount;   
            $weight = $product->weight * $orderDetailsIns->quantity;
        }
        return $this->orderDetails->whereId($orderDetailsIns->id)->update([
            'original_price'    => $originalPrice,
            'discounted_price'  => $discountedPrice,
            'sub_total'         => $subtotal,
            'total'             => $total,
            'weight'            => $weight
        ]);
    }

    private function updateOrderSeller($orderDetails = []) {
        $sp = $this->orderSeller->where(['order_master_id' => $orderDetails->order_master_id, 'seller_id' => $orderDetails->seller_id])->first();
        $sellerCommision = $this->merchant->whereId($sp->seller_id)->first();

        $totalWeight = $this->orderDetails->where([
            'order_master_id' => $sp->order_master_id,
            'seller_id'       => $sp->seller_id
        ])->sum('weight');
        $subTotal = $this->orderDetails->where([
            'order_master_id' => $sp->order_master_id,
            'seller_id'       => $sp->seller_id
        ])->sum('sub_total');
        $total = $this->orderDetails->where([
            'order_master_id' => $sp->order_master_id,
            'seller_id'       => $sp->seller_id
        ])->sum('total');
        $totalDiscount = $subTotal - $total;

        $orderSel['seller_id']          = $sp->seller_id;
        $orderSel['order_master_id']    = $sp->order_master_id;
        $orderSel['total_weight']       = $totalWeight;
        $orderSel['subtotal']           = $subTotal;
        $orderSel['total_discount']     = $totalDiscount;
        $orderSel['order_total']        = $total;
        $orderSelD = $this->orderSeller->where(['id' => $sp->id])->update($orderSel);
    }

    /**
    * 
    */
    private function updateOrderMasterPrice($orderMasterId) {
        $order = $this->order->whereId($orderMasterId)->first();
        $orderDetails = $this->orderDetails->where('order_master_id', $orderMasterId);
        $discount = $orderDetails->sum('discounted_price');
        $subTotal = $orderDetails->sum('sub_total');
        $orderTotal = $orderDetails->sum('total');
        $totalDiscount = $subTotal - $orderTotal;
        $totalWeight = $orderDetails->sum('weight');

        return $this->order->whereId($orderMasterId)->update([
            'total_discount'        => $totalDiscount,
            'subtotal'              => $subTotal,
            'order_total'           => $orderTotal,
            // 'total_commission'       => $totalCommission
        ]);
    }

    /**
    * Method: setCommision
    * Description: This method is used to set commission
    * Author: Sanjoy
    */
    private function setCommisionForInterOrder($order) {  
        
        $orderSel = $this->orderSeller->where(['order_master_id' => $order->id])->get(); 
        foreach ($orderSel as $sp) {
            $sellerCommision = $this->merchant->whereId($sp->seller_id)->first();
            $total = $this->orderDetails->where([
                'order_master_id' => $order->id,
                'seller_id'       => $sp->seller_id
            ])->sum('total');
            $totalCommision = ($sellerCommision->commission * $total)/100;
            //dd($totalCommision); 
            if($totalCommision < $sellerCommision->minimum_commission){
                if($sellerCommision->minimum_commission > $total) {
                    $update['total_commission'] = $total;
                    $individual_earning = (@$sp->subtotal - @$sp->total_discount)- @$total;
                    $update['earning'] = $individual_earning;
                } else {
                    $update['total_commission'] = $sellerCommision->minimum_commission;
                    $individual_earning = (@$sp->subtotal - @$sp->total_discount)- @$sellerCommision->minimum_commission;
                    $update['earning'] = $individual_earning;
                }
            } else {
                $update['total_commission'] = $totalCommision;
                $individual_earning = (@$sp->subtotal - @$sp->total_discount)- @$totalCommision;
                $update['earning'] = $individual_earning;
            }
            
            
            $this->orderSeller->whereId($sp->id)->update($update);
        }
    }

    /**
    * Method: checkStockAndVariant
    * Description: This method is used to check variant stock and correct variand ID.
    * Author: Sanjoy
    */
    private function checkStockAndVariant($proVarId) {
        $variants = $this->productVariant->whereId($proVarId)->first();
        if($variants && $variants->stock_quantity) {
            return $variants;
        }
        return false;
    }


    /**
    * 
    */
    public function calculateCoupon($requset, $order) {
        # check coupon code exist or not
        $checkCoupon = Coupon::with(['getMerchants'])->where(['code' => $requset->coupon_code])->first();
        if(!@$checkCoupon) {
            return 'Invalid coupon.';
        } else if($checkCoupon->start_date > date('Y-m-d')){
            return 'Invalid coupon.';
        } else if($checkCoupon->end_date < date('Y-m-d')){
            return 'Coupon has been expired.';
        } else if($checkCoupon->no_of_times == 0){
            return 'This is a used coupon.';
        }

        # check the coupon is associated with merchant or not
        $orderMerchants = $order->orderMerchants->pluck('seller_id')->toArray();

        if($checkCoupon->applied_for == 'M') {
            $couponMerchants = $checkCoupon->getMerchants->pluck('merchant_id')->toArray();
            $associatedMerchants = array_intersect($orderMerchants, $couponMerchants);
            if(!@$associatedMerchants) {
                return 'No seller associated with this coupon.';
            }
        } else {
            $associatedMerchants = $this->orderSeller->where('order_master_id', $order->id)->pluck('seller_id')->toArray();
        }

        $couponDiscount = 0;
        $shippingPrice = @$order->shipping_price;
        if($checkCoupon->applied_on == 'SUB') { // subtotal
            $response['result']['discount_for'] = 'SUB';
            if($checkCoupon->discount_type == 'F') {
                if(@$order->subtotal) {
                    $couponDiscount = $checkCoupon->discount;

                    if($checkCoupon->coupon_bearer == 'M') {
                        $orderSel = $this->orderSeller->where('order_master_id', $order->id)->whereIn('seller_id', $associatedMerchants)->get();
                        foreach ($orderSel as $sp) {
                            $sellerCommision = $this->merchant->whereId($sp->seller_id)->first();
                            $total = $this->orderDetails->where([
                                'order_master_id' => $order->id,
                                'seller_id'       => $sp->seller_id
                            ])->sum('total');

                            # finding seller percentage from this order
                            $sellerPercentage = (($total * 100) / $order->subtotal );
                            $sellerCouponPercentAmount = ($couponDiscount * $sellerPercentage) / 100;
                            $totalCommision = ($sellerCommision->commission*$total)/100;

                            if($totalCommision < $sellerCommision->minimum_commission){
                                if($sellerCommision->minimum_commission > $order->order_total) {
                                    $update['total_commission'] = $order->order_total - $sellerCouponPercentAmount; 
                                } else {
                                    $update['total_commission'] = $sellerCommision->minimum_commission - $sellerCouponPercentAmount;
                                }
                            }else{
                                $update['total_commission'] = $totalCommision - $sellerCouponPercentAmount;
                            }
                            $update['coupon_distributed_amount'] = $sellerCouponPercentAmount;
                            $update['order_total']               = $sp->order_total - $sellerCouponPercentAmount;
                            $this->orderSeller->whereId($sp->id)->update($update);
                        }
                    }
                }
            } else if($checkCoupon->discount_type == 'P') {
                $couponDiscount = (($order->subtotal * $checkCoupon->discount) /100);

                # calculate seller commission for the coupon
                if($checkCoupon->coupon_bearer == 'M') {
                    $orderSel = $this->orderSeller->where('order_master_id', $order->id)->whereIn('seller_id', $associatedMerchants)->get();
                    foreach ($orderSel as $sp) {
                        $sellerCommision = $this->merchant->whereId($sp->seller_id)->first();
                        $total = $this->orderDetails->where([
                            'order_master_id' => $order->id,
                            'seller_id'       => $sp->seller_id
                        ])->sum('total');

                        # finding seller percentage from this order
                        $sellerPercentage = (($total * 100) / $order->subtotal );
                        $sellerCouponPercentAmount = ($couponDiscount * $sellerPercentage) / 100;
                        
                        $totalCommision = ($sellerCommision->commission * $total)/100;
                        $totalCommision = $totalCommision * $checkCoupon->discount / 100;

                        if($totalCommision < $sellerCommision->minimum_commission){
                            if($sellerCommision->minimum_commission > $order->order_total) {
                                $update['total_commission'] = ($order->order_total * $checkCoupon->discount) / 100; 
                            } else {
                                $update['total_commission'] = ($sellerCommision->minimum_commission * $checkCoupon->discount) / 100;
                            }
                        }else{
                            $update['total_commission'] = $totalCommision;
                        }
                        $update['coupon_distributed_amount'] = $sellerCouponPercentAmount;
                        $update['order_total']               = $sp->order_total - $sellerCouponPercentAmount;
                        $this->orderSeller->whereId($sp->id)->update($update);
                    }
                }
            }
        } else if($checkCoupon->applied_on == 'SHI') { // shipping
            $response['result']['discount_for'] = 'SHI';
            if($checkCoupon->discount_type == 'F') {
                if(@$order->shipping_price > 0) {
                    $couponDiscount = $checkCoupon->discount;
                    $shippingPrice = @$order->shipping_price - $checkCoupon->discount;
                }
            } else if($checkCoupon->discount_type == 'P') {
                $couponDiscount = (($order->shipping_price * $checkCoupon->discount) /100);
                $shippingPrice = @$order->shipping_price - $couponDiscount;
            }
        }

        # update order master 
        $params = [
            'coupon_id'         => $checkCoupon->id,
            'coupon_code'       => $requset->coupon_code,
            'coupon_discount'   => $couponDiscount,
            'shipping_price'    => $shippingPrice,
            'order_total'       => $order->order_total - $couponDiscount
        ];
        OrderMaster::where(['id' => $order->id])->update($params);
    }
    
    /*
    * Method: getMerchantAddress
    * Description: This method is get merchant address from address book
    * Author: Argha
    * Date : 2020-10-08
    */
    public function getMerchantAddress(Request $request)
    {
        $response['jsonrpc'] = "2.0";
        $merchant_id = $request['data']['merchant_id'];
        $merchantAddress = MerchantAddressBook::with(['countryDetailsBylanguage','getCityNameByLanguage'])
                                                ->where('merchant_id',$merchant_id)
                                                ->get();
        $defaultAddress  = MerchantAddressBook::with(['countryDetailsBylanguage','getCityNameByLanguage'])
                                                ->where('merchant_id',$merchant_id)
                                                ->where('is_default',1)
                                                ->first();
        $response['address'] = $merchantAddress;
        $response['defaultAddress'] = $defaultAddress;
        return response()->json($response);
    }
    public function calculateEarning($order){
        $orderDetails = OrderDetail::where(['order_master_id'=>$order->id,'status'=>'OD'])->groupBy('seller_id')->get();
        
        foreach(@$orderDetails as $orderDetail){
            
            $merchant = Merchant::where('id',$orderDetail->seller_id)->first();

            $total_insurance_price = OrderDetail::where(['seller_id'=>$orderDetail->seller_id,'status'=>'OD'])->sum('insurance_price');
            $seller_commission = OrderDetail::where(['seller_id'=>$orderDetail->seller_id,'status'=>'OD'])->sum('seller_commission');
            $total = OrderDetail::where(['seller_id'=>$orderDetail->seller_id,'status'=>'OD'])->sum('total');
            $total_loading_inloading_price = OrderDetail::where(['seller_id'=>$orderDetail->seller_id,'status'=>'OD'])->sum('loading_price');
            $total_admin_commission = OrderDetail::where(['seller_id'=>$orderDetail->seller_id,'status'=>'OD'])->sum('admin_commission');
            
            $total_earning = $total-$total_admin_commission+$seller_commission+$total_loading_inloading_price;
            $seller_commission = OrderDetail::where(['seller_id'=>$orderDetail->seller_id,'status'=>'OD'])->sum('seller_commission');
            

            $merchantUpd['total_commission'] = $total_admin_commission;
            $merchantUpd['gross_earning'] = $total_earning+$total_admin_commission;
            $merchantUpd['net_earning'] = $total_earning;
            $merchantUpd['commission_paid'] = $total_admin_commission;
            $merchantUpd['total_earning'] = $total_earning+$total_admin_commission;
            $merchantUpd['total_due'] = $merchantUpd['total_earning'] - $merchantUpd['total_commission'] - $merchant->total_paid;
            $total_due = $merchantUpd['total_due'];
            $merchantUpd['total_insurance_price'] = $total_insurance_price;
            $merchantUpd['total_loading_inloading_price'] = $total_loading_inloading_price;
            $merchantUpd['total_admin_commission'] = $total_admin_commission;
            $merchantUpd['total_product_price'] = $total;
            $merchantUpd['total_seller_commission'] = $seller_commission;
            Merchant::where('id',$orderDetail->seller_id)->update($merchantUpd);
            // Merchant::where('id',$orderDetail->seller_id)->update(['total_commission'=>$seller_commission,'total_earning'=>$total_earning,'total_due'=>$total_due,'total_insurance_price'=>$total_insurance_price,'total_loading_inloading_price'=>$total_loading_inloading_price,'total_admin_commission'=>$total_admin_commission,'total_product_price'=>$total,'total_seller_commission'=>$seller_commission]);
        }
    }
    public function ajaxChangeOrderStatus(Request $request){
        try{
            
            $id = @$request->data['id'];
            $order = OrderMaster::where('id',$id)->first();
            if(@$order->status == 'N'){
                OrderDetail::where(['order_master_id'=>$id])->update(['status'=>'OA']);
                OrderMaster::where('id',$id)->update(['status'=>'OA']);
            }
            if(@$order->status == 'OA'){
                OrderDetail::where(['order_master_id'=>$id])->update(['status'=>'OP']);
                OrderMaster::where('id',$id)->update(['status'=>'OP']);
            }
            if(@$order->status == 'OP'){
                OrderDetail::where(['order_master_id'=>$id])->update(['status'=>'OD','delivery_date'=>date('Y-m-d'),'delivery_time'=>date('H:i:s')]);
                OrderMaster::where('id',$id)->update(['status'=>'OD','delivery_date'=>date('Y-m-d'),'delivery_time'=>date('H:m:s')]);

                $order = OrderMaster::where('id',$id)->first();
                $this->calculateEarning($order);
            }
                
            $order = OrderMaster::where('id',$id)->first();
            $orderM = OrderMaster::where('id',$id)->first();
            # send mail to customer and seller 
            // $this->sendAdminCustomerAndSellerMail($order->id);
            $order = OrderMaster::with(['customerDetails.userCountryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails.getOrderDetailsUnitMaster',
            'orderMasterDetails.productDetails.productByLanguage',
            'orderMasterDetails.productDetails.productVariants',
            'orderMasterDetails.productDetails.productUnitMasters',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails.merchantCityDetails',
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'getBillingCountry',
            'productVariantDetails',
            'getOrderAllImages',
            'orderMasterDetails.productDetails.defaultImage',
            'orderMasterExtDetails.sellerDetails',
            'orderMasterDetails.driverDetails',
            'orderMasterExtDetails.driverDetails',
            'getCityNameByLanguage',
            'getBillingCityNameByLanguage',
            'paymentData',
            'getpickupCountryDetails',
            'getpickupCityDetails',
            'getPreferredTime'])->where('id',$order->id)->first();
                $data['order'] = @$order;
                Mail::send(new SendOrderMail($data));
                foreach(@$order->orderMasterDetails as $o){

                    $order = OrderMaster::with(['customerDetails.userCountryDetails.countryDetailsBylanguage',
                    'orderMasterDetails'=> function($query) use($o) {
                        $query->where(function($q) use($o){
                            $q->where('seller_id', $o->seller_id);
                        });
                    },
                    'orderMerchants'=> function($query) use($o) {
                        $query->where(function($q) use($o){
                            $q->where(['seller_id'=>$o->seller_id]);
                        });
                    },
                    'countryDetails.countryDetailsBylanguage',
                    'orderMasterDetails.getOrderDetailsUnitMaster',
                    'orderMasterDetails.productDetails.productByLanguage',
                    'orderMasterDetails.productDetails.productVariants',
                    'orderMasterDetails.productDetails.productUnitMasters',
                    'orderMasterDetails.productVariantDetails',
                    'orderMasterDetails.sellerDetails.merchantCityDetails',
                    'countryDetails.countryDetailsBylanguage',
                    'shippingAddress',
                    'billingAddress',
                    'getBillingCountry',
                    'productVariantDetails',
                    'getOrderAllImages',
                    'orderMasterDetails.productDetails.defaultImage',
                    'orderMasterExtDetails.sellerDetails',
                    'orderMasterDetails.driverDetails',
                    'orderMasterExtDetails.driverDetails',
                    'getCityNameByLanguage',
                    'getBillingCityNameByLanguage',
                    'paymentData',
                    'getpickupCountryDetails',
                    'getpickupCityDetails',
                    'getPreferredTime'])->where('id',$orderM->id)->first();
                    $data['order'] = @$order;
                    Mail::send(new SendMerchantOrderMail($data));    
                }
            $data['output'] = 1;
            $data['order'] = $order;
            $data['message'] = " order status changed successfully!";
            return response()->json($data,200);    
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            $data['output'] = 0;
            $data['message'] = $e->getMessage();
            return response()->json($data,200);
        }
    }
    private function sendCustomerAndSellerMail($orderId,$user) {
        $data = [];
        $data['order'] = $order = OrderMaster::where(['id' => $orderId])
        ->with([
            'shippingAddress.getCountry',
            'billingAddress.getCountry',
            'getCountry'
        ])->first();

        $data['orderDetails'] = OrderDetail::where(['order_master_id' => $orderId])
        ->with([
            'sellerDetails',
            'defaultImage',
            'productByLanguage',
            'productVariantDetails'
        ])
        ->get();

        $contactList = [];
        $i=0;
        $user = $this->user->whereId($order->user_id)->first();
        $data['fname']          = $user->fname;
        $data['lname']          = $user->lname;  
        $data['email']          = $user->email;
        $data['user_type']      = $user->user_type;
        $data['shipping_price'] = @$order->shipping_price;
        $data['orderNo']        = @$order->order_no;
        $data['language_id']    = getLanguage()->id;
        $data['bcc']            = $contactList;
        $data['sub_total']      = @$order->subtotal;
        $data['order_total']    = @$order->order_total;
        $data['total_discount'] = @$order->total_discount;
        if(@$order->status == 'N') {
            $data['subject'] = "Your order has been placed successfully! Order no: ".@$order->order_no;
        } else {
            $data['subject'] = "Your order is out for delivery! Order No:".@$order->order_no." ! Order Details!";
        }
        
        #send mail admin to notify for new order only as client requirement (internal order)
        $i=0;
        if(@$order->status =='N'){
            $setting = Setting::first();
            if(@$setting->email_1) {
                $contactList[$i] = $setting->email_1;
                $i++;
            }
            if(@$setting->email_2) {
                $contactList[$i] = $setting->email_2;
                $i++;
            }
            if(@$setting->email_3) {
                $contactList[$i] = $setting->email_3;
                $i++;
            }
        }
        $data['bcc'] = $contactList;
        #send to customer mail(order status can be new/driver assigned)
        Mail::send(new OrderMail($data));
        
        # send mail order details to seller mail
        $dtls_mail = OrderDetail::where(['order_master_id' => $order->id])->groupBy('seller_id')->get();
        
        foreach(@$dtls_mail as $contact) {
            $contactListEmail1 = [];
            $contactListEmail2 = [];
            $contactListEmail3 = [];
            $contactList = [];
            $data['bcc'] = [];
            $data['orderDetails'] = [];
            $i = 0;
            $data['user_type'] = 'C';
            $data['hide_customer_info'] = $contact->sellerDetails->hide_customer_info;

            $data['orderDetails'] = OrderDetail::where(['order_master_id' => $order->id,'seller_id'=>@$contact->seller_id])
            ->with([
                'sellerDetails',
                'defaultImage',
                'productByLanguage',
                'productVariantDetails'
            ])
            ->get();

            $data['sub_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
            $data['order_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
            $data['total_discount'] = OrderSeller::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total_discount');
            $data['shipping_price'] = "NA";
            if(@$order->status == 'N') {
                $data['subject'] = "New Order Placed! Order No : ".@$order->order_no." ! Order Details!";
            } else {
                $data['subject'] = "Order is Accepted! Order No : ".@$order->order_no." ! Order Details!";    
            }
            
            $contactList[$i] = $contact->sellerDetails->email;
            $i++;
            $contactList = array_unique($contactList);
            if(@$contactListEmail1 != '') {
                $contactListEmail1 = array_unique($contactListEmail1);
                $contactList = array_merge($contactList, $contactListEmail1);
            }
            if(@$contactListEmail2 != '') {
                $contactListEmail2 = array_unique($contactListEmail2);
                $contactList = array_merge($contactList, $contactListEmail2);
            }
            if(@$contactListEmail3 != '') {
                $contactListEmail3 = array_unique($contactListEmail3);
                $contactList = array_merge($contactList, $contactListEmail3);
            }
            $data['bcc'] = $contactList;
            Mail::send(new SendOrderDetailsMerchantMail($data));
        }
    }
    
    public function changeProductOrderStatus($id,Request $request){
        try{
            $order = OrderDetail::where('id',$id)->first();
            if(!@$order){
                return redirect()->back()->with("error","Order details is not found!");
            }
            $orderM = OrderMaster::where('id',$order->order_master_id)->first();
            $user = User::where(['id'=>$orderM->user_id])->first();
            
            if(@$order->status == 'N'){
                OrderDetail::where(['id'=>$id])->update(['status'=>'OA']);
                
                $p_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'status'=>'OA'])->count();
                $cancel_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'status'=>'OC'])->count();
                $pr_count = OrderDetail::where(['order_master_id'=>$order->order_master_id])->count();
                if($pr_count == ($p_count-$cancel_count)){
                    
                    OrderMaster::where(['id'=>$orderM->id])->update(['status'=>'OA']);    
                }
            }
            
            if(@$order->status == 'OA'){
                OrderDetail::where(['id'=>$id])->update(['status'=>'OP']);
                
                $p_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'status'=>'OP'])->count();
                $cancel_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'status'=>'OC'])->count();
                $pr_count = OrderDetail::where(['order_master_id'=>$order->order_master_id])->count();
                if($pr_count == ($p_count-$cancel_count)){
                    OrderMaster::where(['id'=>$orderM->id])->update(['status'=>'OP']);    
                }
            }
            
            if(@$order->status == 'OP'){
                OrderDetail::where(['id'=>$id])->update(['status'=>'OD','delivery_date'=>date('Y-m-d'),'delivery_time'=>date('H:m:s')]);
                
                $orderM = OrderMaster::where('id',$orderM->id)->first();
                // calculate earning
                $p_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'status'=>'OD'])->count();
                $cancel_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'status'=>'OC'])->count();
                $pr_count = OrderDetail::where(['order_master_id'=>$order->order_master_id])->count();
                if($pr_count == ($p_count-$cancel_count)){
                    OrderMaster::where(['id'=>$orderM->id])->update(['status'=>'OD','delivery_date'=>date('Y-m-d'),'delivery_time'=>date('H:m:s')]);    
                }
                $os_delivered_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id,'status'=>'OD'])->whereNotIn('status',['C'])->count();
                $os_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->whereNotIn('status',['C'])->count();
                
                if($os_count == $os_delivered_count){
                    OrderSeller::where(['order_master_id'=>$orderM->id])->update(['status'=>'OD']);    
                }
                
                $this->calculateEarning($orderM);
            }

            $order = OrderMaster::with(['customerDetails.userCountryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails.getOrderDetailsUnitMaster',
            'orderMasterDetails.productDetails.productByLanguage',
            'orderMasterDetails.productDetails.productVariants',
            'orderMasterDetails.productDetails.productUnitMasters',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails.merchantCityDetails',
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'orderMerchants.orderSeller',
            'getBillingCountry',
            'productVariantDetails',
            'getOrderAllImages',
            'orderMasterDetails.productDetails.defaultImage',
            'orderMasterExtDetails.sellerDetails',
            'orderMasterDetails.driverDetails',
            'orderMasterExtDetails.driverDetails',
            'getCityNameByLanguage',
            'getBillingCityNameByLanguage',
            'paymentData',
            'getpickupCountryDetails',
            'getpickupCityDetails',
            'getPreferredTime'])->where('id',$orderM->id)->first();
                $data['order'] = @$order;
                Mail::send(new SendOrderMail($data));
                foreach(@$order->orderMerchants as $o){

                    $order = OrderMaster::with(['customerDetails.userCountryDetails.countryDetailsBylanguage',
                    'orderMasterDetails'=> function($query) use($o) {
                        $query->where(function($q) use($o){
                            $q->where('seller_id', $o->seller_id);
                        });
                    },
                    'orderMerchants'=> function($query) use($o) {
                        $query->where(function($q) use($o){
                            $q->where(['seller_id'=>$o->seller_id]);
                        });
                    },
                    'orderMerchants.orderSeller',
                    'countryDetails.countryDetailsBylanguage',
                    'orderMasterDetails.getOrderDetailsUnitMaster',
                    'orderMasterDetails.productDetails.productByLanguage',
                    'orderMasterDetails.productDetails.productVariants',
                    'orderMasterDetails.productDetails.productUnitMasters',
                    'orderMasterDetails.productVariantDetails',
                    'orderMasterDetails.sellerDetails.merchantCityDetails',
                    'countryDetails.countryDetailsBylanguage',
                    'shippingAddress',
                    'billingAddress',
                    'getBillingCountry',
                    'productVariantDetails',
                    'getOrderAllImages',
                    'orderMasterDetails.productDetails.defaultImage',
                    'orderMasterExtDetails.sellerDetails',
                    'orderMasterDetails.driverDetails',
                    'orderMasterExtDetails.driverDetails',
                    'getCityNameByLanguage',
                    'getBillingCityNameByLanguage',
                    'paymentData',
                    'getpickupCountryDetails',
                    'getpickupCityDetails',
                    'getPreferredTime'])->where('id',$orderM->id)->first();
                    $data['order'] = @$order;
                    Mail::send(new SendMerchantOrderMail($data));    
                }
                return redirect()->back()->with('success',@$order->order_no." This order status is changed successfully!");
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return redirect()->back()->with('error',$e->getMessage());
        }
    }
    private function sendAdminCustomerAndSellerMail($orderId) {
        $data = [];
        $data['order'] = $order = OrderMaster::with('customerDetails')->where(['id' => $orderId])
        ->with([
            'shippingAddress.getCountry',
            'billingAddress.getCountry',
            'getCountry'
        ])->first();

        $data['orderDetails'] = OrderDetail::where(['order_master_id' => $orderId])
        ->with([
            'sellerDetails',
            'defaultImage',
            'productByLanguage',
            'productVariantDetails'
        ])
        ->get();

        $contactList = [];
        $i=0;
        // $user = User::whereId($order->user_id)->first();
        $user = @$order->customerDetails;
        $data['fname']          = $user->fname;
        $data['lname']          = $user->lname;  
        $data['email']          = $user->email;
        $data['user_type']      = $user->user_type;
        $data['shipping_price'] = @$order->shipping_price;
        $data['orderNo']        = @$order->order_no;
        $data['language_id']    = getLanguage()->id;
        $data['bcc']            = $contactList;
        $data['sub_total']      = @$order->subtotal;
        $data['order_total']    = @$order->order_total;
        $data['total_discount'] = @$order->total_discount;
        if(@$order->status == 'N') {
            $data['subject'] = "Your order has been placed successfully! Order no: ".@$order->order_no;
        } else {
            $data['subject'] = "Your order status has been changed ! Order No:".@$order->order_no." ! Order Details!";
        }
        
        #send mail admin to notify for new order only as client requirement 
        $i=0;
        // if(@$order->status =='N'){
            $setting = Setting::first();
            if(@$setting->email_1) {
                $contactList[$i] = $setting->email_1;
                $i++;
            }
            if(@$setting->email_2) {
                $contactList[$i] = $setting->email_2;
                $i++;
            }
            if(@$setting->email_3) {
                $contactList[$i] = $setting->email_3;
                $i++;
            }
        // }
        $data['bcc'] = $contactList;
        #send to customer mail(order status can be new/driver assigned)
        Mail::send(new OrderMail($data));
        
        # send mail order details to seller mail
        $dtls_mail = OrderDetail::where(['order_master_id' => $order->id])->groupBy('seller_id')->get();
        
        foreach(@$dtls_mail as $contact) {
            $contactListEmail1 = [];
            $contactListEmail2 = [];
            $contactListEmail3 = [];
            $contactList = [];
            $data['bcc'] = [];
            $data['orderDetails'] = [];
            $i = 0;
            $data['user_type'] = 'C';
            $data['hide_customer_info'] = $contact->sellerDetails->hide_customer_info;

            $data['orderDetails'] = OrderDetail::where(['order_master_id' => $order->id,'seller_id'=>@$contact->seller_id])
            ->with([
                'sellerDetails',
                'defaultImage',
                'productByLanguage',
                'productVariantDetails'
            ])
            ->get();

            $data['sub_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
            $data['order_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
            $data['total_discount'] = OrderSeller::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total_discount');
            $data['shipping_price'] = "NA";
            if(@$order->status == 'N') {
                $data['subject'] = "New Order Placed! Order No : ".@$order->order_no." ! Order Details!";
            } else {
                $data['subject'] = "Order is Accepted! Order No : ".@$order->order_no." ! Order Details!";    
            }
            
            $contactList[$i] = $contact->sellerDetails->email;
            $i++;
            // if(@$order->status =='DA') {
            //     if($contact->sellerDetails->email1) {
            //         $contactListEmail1[$i] = $contact->sellerDetails->email1;
            //         $i++;
            //     }
            //     if($contact->sellerDetails->email2) {
            //         $contactListEmail2[$i] = $contact->sellerDetails->email2;
            //         $i++;
            //     }
            //     if($contact->sellerDetails->email3) {
            //         $contactListEmail3[$i] = $contact->sellerDetails->email3;
            //         $i++;
            //     }    
            // }
            $contactList = array_unique($contactList);
            if(@$contactListEmail1 != '') {
                $contactListEmail1 = array_unique($contactListEmail1);
                $contactList = array_merge($contactList, $contactListEmail1);
            }
            if(@$contactListEmail2 != '') {
                $contactListEmail2 = array_unique($contactListEmail2);
                $contactList = array_merge($contactList, $contactListEmail2);
            }
            if(@$contactListEmail3 != '') {
                $contactListEmail3 = array_unique($contactListEmail3);
                $contactList = array_merge($contactList, $contactListEmail3);
            }
            $data['bcc'] = $contactList;
            Mail::send(new SendOrderDetailsMerchantMail($data));
            // $this->sendNotificationDashboardUpdate($contact->order_master_id,$contact->seller_id);
        }
    }
    public function ajaxCancelOrderChangeOrderStatus(Request $request){
        try{
            // dd($request->all());
            $id = @$request->data['id'];

            $order = OrderMaster::where('id',$id)->first();
                OrderDetail::where(['order_master_id'=>$id])->update(['status'=>'OC']);
                OrderMaster::where('id',$id)->update(['status'=>'OC']);
            
            $user = User::where(['id'=>$order->user_id])->first();
            $orderId = $id;
            $data = [];
            $data['order'] = $order = OrderMaster::where(['id' => $orderId])
            ->with([
                'shippingAddress.getCountry',
                'billingAddress.getCountry',
                'getCountry'
            ])->first();

            $data['orderDetails'] = OrderDetail::where(['order_master_id' => $orderId])
            ->with([
                'sellerDetails',
                'defaultImage',
                'productByLanguage',
                'productVariantDetails'
            ])
            ->get();

            $contactList = [];
            $i=0;
            $user = $this->user->whereId($order->user_id)->first();
            $data['fname']          = $user->fname;
            $data['lname']          = $user->lname;  
            $data['email']          = $user->email;
            $data['user_type']      = $user->user_type;
            $data['shipping_price'] = @$order->shipping_price;
            $data['orderNo']        = @$order->order_no;
            $data['language_id']    = getLanguage()->id;
            $data['bcc']            = $contactList;
            $data['sub_total']      = @$order->subtotal;
            $data['order_total']    = @$order->order_total;
            $data['total_discount'] = @$order->total_discount;
            if(@$order->status == 'N') {
                $data['subject'] = "Your order has been placed successfully! Order no: ".@$order->order_no;
            } else {
                $data['subject'] = "Your order is out for delivery! Order No:".@$order->order_no." ! Order Details!";
            }
            
            #send mail admin to notify for new order only as client requirement (internal order)
            $i=0;
            if(@$order->status =='N'){
                $setting = Setting::first();
                if(@$setting->email_1) {
                    $contactList[$i] = $setting->email_1;
                    $i++;
                }
                if(@$setting->email_2) {
                    $contactList[$i] = $setting->email_2;
                    $i++;
                }
                if(@$setting->email_3) {
                    $contactList[$i] = $setting->email_3;
                    $i++;
                }
            }
            $data['bcc'] = $contactList;
            Mail::send(new OrderMail($data));
            # send mail order details to seller mail
            $dtls_mail = OrderDetail::where(['order_master_id' => $order->id])->groupBy('seller_id')->get();
            foreach(@$dtls_mail as $contact) {
                $contactListEmail1 = [];
                $contactListEmail2 = [];
                $contactListEmail3 = [];
                $contactList = [];
                $data['bcc'] = [];
                $data['orderDetails'] = [];
                $i = 0;
                $data['user_type'] = 'C';
                $data['hide_customer_info'] = $contact->sellerDetails->hide_customer_info;

                $data['orderDetails'] = OrderDetail::where(['order_master_id' => $order->id,'seller_id'=>@$contact->seller_id])
                ->with([
                    'sellerDetails',
                    'defaultImage',
                    'productByLanguage',
                    'productVariantDetails'
                ])
                ->get();

                $data['sub_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
                $data['order_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
                $data['total_discount'] = OrderSeller::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total_discount');
                $data['shipping_price'] = "NA";
                if(@$order->status == 'N') {
                    $data['subject'] = "New Order Placed! Order No : ".@$order->order_no." ! Order Details!";
                } else {
                    $data['subject'] = "Order is Accepted! Order No : ".@$order->order_no." ! Order Details!";    
                }
                
                $contactList[$i] = $contact->sellerDetails->email;
                $i++;
                $contactList = array_unique($contactList);
                if(@$contactListEmail1 != '') {
                    $contactListEmail1 = array_unique($contactListEmail1);
                    $contactList = array_merge($contactList, $contactListEmail1);
                }
                if(@$contactListEmail2 != '') {
                    $contactListEmail2 = array_unique($contactListEmail2);
                    $contactList = array_merge($contactList, $contactListEmail2);
                }
                if(@$contactListEmail3 != '') {
                    $contactListEmail3 = array_unique($contactListEmail3);
                    $contactList = array_merge($contactList, $contactListEmail3);
                }
                $data['bcc'] = $contactList;
                Mail::send(new SendOrderDetailsMerchantMail($data));
            }

            $order = OrderMaster::where('id',$id)->first();
            $data['output'] = 1;
            $data['order'] = $order;
            $data['message'] = " order status changed successfully!";
            return response()->json($data,200);    
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            $data['output'] = 0;
            $data['message'] = $e->getMessage();
            return response()->json($data,200);
        }
    }
    public function cancelAProductOrder($id,Request $request){
        try{
                $order = OrderDetail::where('id',$id)->whereNotIn('status',['OC'])->first();
                if(!@$order){
                    return redirect()->back();
                }
                $orderM = OrderMaster::where('id',$order->order_master_id)->first();
                OrderDetail::where(['id'=>$id])->update(['status'=>'OC','cancelled_on'=>date('Y-m-d')]);
            
                $userInfo = User::where(['id'=>$orderM->user_id])->first();
                $updUser['wallet_amount'] = $userInfo->wallet_amount+$order->payable_amount;
                User::where(['id'=>$orderM->user_id])->update($updUser);

                
                // $wcr['amount'] = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereIn('status',['OC'])->sum('payable_amount');
                // $wcr['type'] = 'IN';
                // $wcr['description'] = " Refund money of ".getCurrency()." ".$wcr['amount']." for ".$orderM->order_no;
                // $wcr['w_date'] = date('Y-m-d');
                // $walletHistory = Wallet::where(['user_id'=>$userInfo->id,'order_id'=>$orderM->id])->first();
                
                // if(!@$walletHistory){
                //     $wcr['user_id'] = $userInfo->id;
                //     $wcr['order_id'] = $orderM->id;
                //     $wcr['order_no'] = $orderM->order_no;
                //     $wallet = Wallet::create($wcr);
                // }else{
                //     $wallet = Wallet::where(['user_id'=>$userInfo->id,'order_id'=>$orderM->id])->update($wcr);
                // }
                // $walletHistory = Wallet::where(['user_id'=>$userInfo->id,'order_id'=>$orderM->id])->first();
                $productDtals = Product::with(['productByLanguage'])->where(['id'=>$order->product_id])->first();

                // $wdcr['wallet_id'] = $walletHistory->id;
                // $wdcr['amount'] = $order->payable_amount;
                // $wdcr['type'] = 'IN';
                // $wdcr['description'] = " Refund money of ".getCurrency()." ".$wdcr['amount']." for ".@$productDtals->productByLanguage->title." in ".$orderM->order_no;
                // $wdcr['w_d_date'] = date('Y-m-d');
                // $wdcr['user_id'] = $orderM->user_id;
                // $wdcr['order_id'] = $orderM->id;
                // $wdcr['product_id'] = $order->product_id;
                // $wdcr['order_details_id'] = $order->id;
                // $walletdetails = WalletDetails::create($wdcr);

                $weight = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('weight');
                
                $updMstr['total_discount'] = $orderM->total_discount- ($order->original_price - $order->discounted_price);
                $updMstr['total_product_price'] = $orderM->total_product_price- $order->total;
                $updMstr['payable_amount'] = $orderM->payable_amount-$order->payable_amount;
                $updMstr['order_total'] = $orderM->order_total-$order->payable_amount;
                $updMstr['subtotal'] = $orderM->subtotal-$order->sub_total;
                $updMstr['total_product_price'] = $orderM->total_product_price-$order->total;
                $updMstr['insurance_price'] = $orderM->insurance_price-$order->insurance_price;
                $updMstr['loading_price'] = $orderM->loading_price-$order->loading_price;
                $updMstr['admin_commission'] = $orderM->admin_commission-$order->admin_commission;
                $updMstr['total_seller_commission'] = $orderM->total_seller_commission-$order->seller_commission;
                $updMstr['product_total_weight'] = $weight;
                OrderMaster::where(['id'=>$order->order_master_id])->update($updMstr);

                $ords = OrderSeller::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->first();
                $updOrds['loading_price'] = $ords->loading_price-$order->loading_price;
                $updOrds['insurance_price'] = $ords->insurance_price-$order->insurance_price;
                $updOrds['admin_commission'] = $ords->admin_commission-$order->admin_commission;
                $updOrds['seller_commission'] = $ords->seller_commission-$order->seller_commission;
                $updOrds['payable_amount'] = $ords->payable_amount-$order->payable_amount;
                $updOrds['subtotal'] = $ords->subtotal-$order->sub_total;
                $dis = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->whereNotIn('status',['OC'])->sum('original_price') - OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->whereNotIn('status',['OC'])->sum('discounted_price');
                $updOrds['total_discount'] = $dis;
                $updOrds['order_total'] = $ords->order_total-$order->payable_amount;
                $updOrds['total_commission'] = $ords->total_commission;
                $updOrds['earning'] = $ords->earning - $order->loading_price - $order->seller_commission - ($order->total-$order->admin_commission);
                $updOrds['total_weight'] = $ords->earning - $order->loading_price - $order->seller_commission - ($order->total-$order->admin_commission);
                
                OrderSeller::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->update($updOrds);

                $o_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->count();
                $o_c_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->where('status','OC')->count();

                if($o_count == $o_c_count){
                    OrderSeller::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->update(['status'=>'CM']);
                }else{
                    $o_o_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->whereNotIn('status',['OC'])->count();
                    $o_d_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->where('status','OD')->count(); 
                    if($o_o_count = $o_d_count){

                        OrderSeller::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->update(['status'=>'CM']);
                    }
                }                

                $o_count = OrderDetail::where(['order_master_id'=>$order->order_master_id])->count();
                $o_c_count = OrderDetail::where(['order_master_id'=>$order->order_master_id])->where('status','OC')->count();

                if($o_count == $o_c_count){
                    OrderMaster::where(['id'=>$order->order_master_id])->update(['status'=>'CM']);

                }else{
                    $o_o_count = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->count();
                    $o_d_count = OrderDetail::where(['order_master_id'=>$order->order_master_id])->where('status','OD')->count();

                    if($o_o_count = $o_d_count){
                        OrderMaster::where(['id'=>$order->order_master_id])->update(['status'=>'CM']);
                    }
                }


            $user = User::where(['id'=>$orderM->user_id])->first();

            $orderId = $orderM->id;
            $data = [];
            $data['order'] = $order = OrderMaster::where(['id' => $orderId])
            ->with([
                'shippingAddress.getCountry',
                'billingAddress.getCountry',
                'getCountry'
            ])->first();

            $data['orderDetails'] = OrderDetail::where(['order_master_id' => $orderId])
            ->with([
                'sellerDetails',
                'defaultImage',
                'productByLanguage',
                'productVariantDetails'
            ])
            ->get();

            $contactList = [];
            $i=0;
            // $user = $this->user->whereId($order->user_id)->first();
            $data['fname']          = $user->fname;
            $data['lname']          = $user->lname;  
            $data['email']          = $user->email;
            $data['user_type']      = $user->user_type;
            $data['shipping_price'] = @$order->shipping_price;
            $data['orderNo']        = @$order->order_no;
            $data['language_id']    = getLanguage()->id;
            $data['bcc']            = $contactList;
            $data['sub_total']      = @$order->subtotal;
            $data['order_total']    = @$order->order_total;
            $data['total_discount'] = @$order->total_discount;
            if(@$order->status == 'INP') {
                $data['subject'] = "Your order status is changed! Order no: ".@$order->order_no;
            }
            elseif(@$order->status == 'OA') {
                $data['subject'] = "Order is Accepted ! Order no: ".@$order->order_no;
            }
            elseif(@$order->status == 'OP') {
                $data['subject'] = "Order is Picked up! Order no: ".@$order->order_no;
            }
            elseif(@$order->status == 'OD') {
                $data['subject'] = "Order is delivered! Order no: ".@$order->order_no;
            }
            else {
                $data['subject'] = "Your order status is changed! Order No:".@$order->order_no." ! Order Details!";
            }
            
            #send mail admin to notify for new order only as client requirement (internal order)
            $i=0;
            if(@$order->status =='N'){
                $setting = Setting::first();
                if(@$setting->email_1) {
                    $contactList[$i] = $setting->email_1;
                    $i++;
                }
                if(@$setting->email_2) {
                    $contactList[$i] = $setting->email_2;
                    $i++;
                }
                if(@$setting->email_3) {
                    $contactList[$i] = $setting->email_3;
                    $i++;
                }
            }
            $data['bcc'] = $contactList;
            Mail::send(new OrderMail($data));
            # send mail order details to seller mail
            $dtls_mail = OrderDetail::where(['order_master_id' => $order->id])->groupBy('seller_id')->get();
            foreach(@$dtls_mail as $contact) {
                $contactListEmail1 = [];
                $contactListEmail2 = [];
                $contactListEmail3 = [];
                $contactList = [];
                $data['bcc'] = [];
                $data['orderDetails'] = [];
                $i = 0;
                $data['user_type'] = 'C';
                $data['hide_customer_info'] = $contact->sellerDetails->hide_customer_info;

                $data['orderDetails'] = OrderDetail::where(['order_master_id' => $order->id,'seller_id'=>@$contact->seller_id])
                ->with([
                    'sellerDetails',
                    'defaultImage',
                    'productByLanguage',
                    'productVariantDetails'
                ])
                ->get();

                $data['sub_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
                $data['order_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
                $data['total_discount'] = OrderSeller::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total_discount');
                $data['shipping_price'] = "NA";
                $orderStatus = @$order->status;
                if(@$order->status == 'OA'){
                    $orderStatus = "Order Accepted";
                }
                else if(@$order->status == 'OP'){
                    $orderStatus = "Order Picked Up";
                }
                else if(@$order->status == 'OD'){
                    $orderStatus = "Order Delivered";
                }
                else if(@$order->status == 'OC'){
                    $orderStatus = "Order Cancelled";
                }
                else if(@$order->status == 'CM'){
                    $orderStatus = "Completed";
                }
                else if(@$order->status == 'INP') {
                    $data['subject'] = " Order No : ".@$order->order_no." ! Order Details!";
                } else {
                    $data['subject'] = " Order No : ".@$order->order_no." ! Order Details!";
                }
                
                $contactList[$i] = $contact->sellerDetails->email;
                $i++;
                $contactList = array_unique($contactList);
                if(@$contactListEmail1 != '') {
                    $contactListEmail1 = array_unique($contactListEmail1);
                    $contactList = array_merge($contactList, $contactListEmail1);
                }
                if(@$contactListEmail2 != '') {
                    $contactListEmail2 = array_unique($contactListEmail2);
                    $contactList = array_merge($contactList, $contactListEmail2);
                }
                if(@$contactListEmail3 != '') {
                    $contactListEmail3 = array_unique($contactListEmail3);
                    $contactList = array_merge($contactList, $contactListEmail3);
                }
                $data['bcc'] = $contactList;
                Mail::send(new SendOrderDetailsMerchantMail($data));
            }

            $order = OrderMaster::where('id',$id)->first();
            return redirect()->back()->with('success',"This product is cancelled successfully!");
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return redirect()->back()->with('error',$e->getMessage());
        }
    }

    public function changeOrderStatusChangeUpdateEarningCalculation($id,Request $request){
        try{
            $order = OrderDetail::where(['id'=>$id])->first();
            if(!@$order){
                return redirect()->back()->with("error","Order details id is not found!");
            }
            $orderM = OrderMaster::where('id',$order->order_master_id)->first();
            $user = User::where(['id'=>$orderM->user_id])->first();
            
            
            if(@$request->status[$id] == 'OA'){
                OrderDetail::where(['id'=>$id])->update(['status'=>'OA']);
                // $p_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'status'=>'OA'])->count();
                // $cancel_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'status'=>'OC'])->count();
                // $pr_count = OrderDetail::where(['order_master_id'=>$order->order_master_id])->count();
                // if($pr_count == ($p_count-$cancel_count)){
                //     OrderMaster::where(['id'=>$orderM->id])->update(['status'=>'OA']);    
                // }
            }
            if(@$request->status[$id] == 'OP'){
                OrderDetail::where(['id'=>$id])->update(['status'=>'OP']);
                
                // $p_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'status'=>'OP'])->count();
                // $cancel_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'status'=>'OC'])->count();
                // $pr_count = OrderDetail::where(['order_master_id'=>$order->order_master_id])->count();
                // if($pr_count == ($p_count-$cancel_count)){
                //     OrderMaster::where(['id'=>$orderM->id])->update(['status'=>'OP']);    
                // }
            }
            
            if(@$request->status[$id] == 'OD'){

                OrderDetail::where(['id'=>$id])->update(['status'=>'OD','delivery_date'=>date('Y-m-d'),'delivery_time'=>date('H:m:s')]);
                
                $orderM = OrderMaster::where('id',$orderM->id)->first();

                // calculate earning
                $od_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'status'=>'OD'])->count();
                $cancel_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'status'=>'OC'])->count();
                $pr_count = OrderDetail::where(['order_master_id'=>$order->order_master_id])->count();

                if($pr_count == ($pr_count-$cancel_count)){

                    OrderMaster::where(['id'=>$orderM->id])->update(['status'=>'OD','delivery_date'=>date('Y-m-d'),'delivery_time'=>date('H:m:s')]);    

                }

                $os_delivered_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id,'status'=>'OD'])->whereNotIn('status',['C'])->count();

                $os_count = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->whereNotIn('status',['C'])->count();
                
                if($os_count == $os_delivered_count){
                    OrderSeller::where(['order_master_id'=>$orderM->id])->update(['status'=>'OD']);    
                }
                $this->calculateEarningOfO($orderM,$id);
            }

            $order = OrderMaster::with(['customerDetails.userCountryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails.getOrderDetailsUnitMaster',
            'orderMasterDetails.productDetails.productByLanguage',
            'orderMasterDetails.productDetails.productVariants',
            'orderMasterDetails.productDetails.productUnitMasters',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails.merchantCityDetails',
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'orderMerchants.orderSeller',
            'getBillingCountry',
            'productVariantDetails',
            'getOrderAllImages',
            'orderMasterDetails.productDetails.defaultImage',
            'orderMasterExtDetails.sellerDetails',
            'orderMasterDetails.driverDetails',
            'orderMasterExtDetails.driverDetails',
            'getCityNameByLanguage',
            'getBillingCityNameByLanguage',
            'paymentData',
            'getpickupCountryDetails',
            'getpickupCityDetails',
            'getPreferredTime'])->where('id',$orderM->id)->first();
                $data['order'] = @$order;
                Mail::send(new SendOrderMail($data));
                foreach(@$order->orderMerchants as $o){

                    $order = OrderMaster::with(['customerDetails.userCountryDetails.countryDetailsBylanguage',
                    'orderMasterDetails'=> function($query) use($o) {
                        $query->where(function($q) use($o){
                            $q->where('seller_id', $o->seller_id);
                        });
                    },
                    'orderMerchants'=> function($query) use($o) {
                        $query->where(function($q) use($o){
                            $q->where(['seller_id'=>$o->seller_id]);
                        });
                    },
                    'orderMerchants.orderSeller',
                    'countryDetails.countryDetailsBylanguage',
                    'orderMasterDetails.getOrderDetailsUnitMaster',
                    'orderMasterDetails.productDetails.productByLanguage',
                    'orderMasterDetails.productDetails.productVariants',
                    'orderMasterDetails.productDetails.productUnitMasters',
                    'orderMasterDetails.productVariantDetails',
                    'orderMasterDetails.sellerDetails.merchantCityDetails',
                    'countryDetails.countryDetailsBylanguage',
                    'shippingAddress',
                    'billingAddress',
                    'getBillingCountry',
                    'productVariantDetails',
                    'getOrderAllImages',
                    'orderMasterDetails.productDetails.defaultImage',
                    'orderMasterExtDetails.sellerDetails',
                    'orderMasterDetails.driverDetails',
                    'orderMasterExtDetails.driverDetails',
                    'getCityNameByLanguage',
                    'getBillingCityNameByLanguage',
                    'paymentData',
                    'getpickupCountryDetails',
                    'getpickupCityDetails',
                    'getPreferredTime'])->where('id',$orderM->id)->first();
                    $data['order'] = @$order;
                    Mail::send(new SendMerchantOrderMail($data));    
                }
                return redirect()->back()->with('success',"Status is changed successfully!");
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return redirect()->back()->with('error',$e->getMessage());
        }
    }
    public function calculateEarningOfO($order,$id){
        $orderDetail = OrderDetail::where(['order_master_id'=>$order->id,'id'=>$id,'status'=>'OD'])->first();
        $merchant = Merchant::where('id',$orderDetail->seller_id)->first();
        $OrderSeller = OrderSeller::where(['order_master_id'=>$order->id,'seller_id'=>$orderDetail->seller_id])->first();
        $merchantUpd['total_commission'] = $merchant->total_commission+$orderDetail->admin_commission;
        $merchantUpd['gross_earning'] = $merchant->gross_earning+$orderDetail->loading_price+$orderDetail->seller_commission+$orderDetail->total;
        $merchantUpd['net_earning'] = $merchant->net_earning+$orderDetail->loading_price+$orderDetail->seller_commission+$orderDetail->total-$orderDetail->admin_commission;
        $merchantUpd['commission_paid'] = $merchant->commission_paid+$orderDetail->admin_commission;
        $merchantUpd['total_earning'] = $merchantUpd['gross_earning'];
        $merchantUpd['total_due'] = $merchant->total_due+$orderDetail->loading_price+$orderDetail->seller_commission+$orderDetail->total-$orderDetail->admin_commission;
        $merchantUpd['total_insurance_price'] = $merchant->total_insurance_price+$orderDetail->insurance_price;
        $merchantUpd['total_loading_inloading_price'] = $merchant->total_loading_inloading_price+$orderDetail->loading_price;
        $merchantUpd['total_admin_commission'] = $merchant->total_admin_commission+$orderDetail->admin_commission;
        $merchantUpd['total_product_price'] = $merchant->total_product_price+$orderDetail->total;
        $merchantUpd['total_seller_commission'] = $merchant->total_seller_commission+$orderDetail->seller_commission;
        Merchant::where('id',$orderDetail->seller_id)->update($merchantUpd);
    }
    public function paymentConfirmation(Request $request){
        $orderid = @$request->data['orderid'];
        $order = OrderMaster::whereId($orderid)->first();
        if(@$order){
            if(@$order->status == 'I'){
                OrderMaster::whereId($orderid)->update(['status'=>'INP']);
            }

            $p = Payment::where(['order_id'=>$orderid])->first();
            
            if(@$p){
                Payment::where(['order_id'=>$orderid])->update(['status'=>'S']);    
            }else{
                Payment::create(['order_id'=>$order->id,'user_id'=>$order->user_id,'amount'=>$order->payable_amount,'payment_mode'=>$order->payment_method,'status'=>'S']);
            }

            $data['message'] = @$order->order_no." is confirmed! Payment confirmed!";
            return response()->json($data,200);
        }else{
            $data['message'] = "You have provided wrong order id! order id : ".@$orderid;
            return response()->json($data,200);
        }
    }
}
