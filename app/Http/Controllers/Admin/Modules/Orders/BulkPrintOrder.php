<?php

namespace App\Http\Controllers\Admin\Modules\Orders;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Driver;
use App\Merchant;
use App\Models\OrderMaster;
use App\Models\OrderSeller;
use App\Models\OrderDetail;
use App\Models\CountryDetails;
use App\Models\CitiDetails;
class BulkPrintOrder extends Controller
{
    public function __construct(){
        $this->middleware('admin.auth:admin');
    }

    /*
    * Method        : printBulkOrder
    * Description   : This method is used to print bulk order
    * Author        : Argha
    * Date          : 2020-10-09
    */
    public function printBulkOrder($ids) {
        $ids = json_decode(base64_decode($ids));
        $driver = Driver::where(['status'=>'A'])->get();
        $order = OrderMaster::with([
            'customerDetails.userCountryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'orderMasterDetails.productDetails.productByLanguage',
            'orderMasterDetails.productDetails.productVariants',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails.merchantCityDetails',
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'getBillingCountry',
            'productVariantDetails',
            'getOrderAllImages',
            'orderMasterDetails.productDetails.defaultImage',
            'orderMasterExtDetails.sellerDetails',
            'orderMasterDetails.driverDetails',
            'orderMasterExtDetails.driverDetails',
            'getCityNameByLanguage',
            'getBillingCityNameByLanguage'
        ])->whereIN('id',$ids)->get();
        
        if(@$order){
            return view('admin.modules.orders.print_bulk_orders')->with([
                'orders'    => @$order,
                'drivers'   => @$driver
            ]);
        } else {
          session()->flash("error",__('errors.-5002'));
          return redirect()->back();
        }
    }
}
