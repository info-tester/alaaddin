<?php

namespace App\Http\Controllers\Admin\Modules\CancelRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CancelRequest;
use App\Models\City;
use App\User;
use App\Merchant;
use App\Models\Product;
use App\Models\Payment;
use App\Models\OrderMaster;
use App\Models\OrderSeller;
use App\Models\OrderDetail;

class CancelRequestController extends Controller
{
    /*
    * Method        :viewcustomerCancelRequest
    * Description   :This method is used to view customer Cancel Request
    */
    public function viewcustomerCancelRequest(Request $request){
        $columnIndex = $request->order[0]['column'];
        $columnSortOrder = $request->order[0]['dir'];

        $o = CancelRequest::with(['get_order_master','get_order_master.customerDetails','product_by_language'])->where('id','!=',0);
        
        if(@$request->columns['0']['search']['value']){
            $o = $o->whereHas('get_order_master',function($q1) use($request) {
                $q1 = $q1->where('order_no','like','%'.$request->columns['0']['search']['value'].'%');
            });
        }
        if(@$request->columns['1']['search']['value']){
            $o = $o->whereHas('get_order_master.customerDetails',function($q1) use($request) {
                $q1 = $q1->where('id',$request->columns['1']['search']['value']);
            });
        }
        if(@$request->columns['2']['search']['value']){
            $o = $o->where(function($q1) use($request) {
                $q1 = $q1->where('status',$request->columns['2']['search']['value']);
            });
        }
        if(@$request->columns['3']['search']['value']){
            $o = $o->where(function($q1) use($request) {
                $q1 = $q1->where('cancel_date','>=',$request->columns['3']['search']['value']);
            });
        }
        if(@$request->columns['4']['search']['value']){
            $o = $o->where(function($q1) use($request) {
                $q1 = $q1->where('cancel_date','<=',$request->columns['4']['search']['value']);
            });
        }

        $ps =  $o->count();
        $pp = $o;
        $pp = $pp->count();
        $limit = $request->length;
        $p = $o = $o;
        $p =  $p->count();
        if($columnIndex == 0) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('id','asc');
            } else {
                $o = $o->orderBy('id','desc');
            }
        }
        if($columnIndex == 1) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('order_id','asc');
            } else {
                $o = $o->orderBy('order_id','desc');
            }
        }
        if($columnIndex == 2) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('id','asc');
            } else {
                $o = $o->orderBy('id','desc');
            }
        }
        if($columnIndex == 3) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('id','asc');
            } else {
                $o = $o->orderBy('id','desc');
            }
        }
        if($columnIndex == 4) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('amount','asc');
            } else {
                $o = $o->orderBy('amount','desc');
            }
        }
        if($columnIndex == 5) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('cancel_date','asc');
            } else {
                $o = $o->orderBy('cancel_date','desc');
            }
        }
        if($columnIndex == 6) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('status','asc');
            } else {
                $o = $o->orderBy('status','desc');
            }
        }
        $o = $o->orderBy('id','desc')->skip($request->start)->take($request->length)->get();
        $data['aaData'] = $o;
        $data["draw"] = intval($request->draw);
        $data['iTotalRecords'] = $ps;
        $data["iTotalDisplayRecords"] = $pp;
        return response()->json($data);
    }
    /*
    * Method        :customerCancelRequest
    * Description   :This method is used to customer Cancel Request
    */
    public function customerCancelRequest(Request $request){
        
        $data['customers'] = User::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        $data['merchants'] = Merchant::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        $data['status'] = Merchant::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        return view('admin.modules.cancel_request.cancel_request')->with($data);
    }
    public function accpetCancelRequest($id){
        
    
    }
    
    public function rejectCancelRequest($id){
    
    }
}
