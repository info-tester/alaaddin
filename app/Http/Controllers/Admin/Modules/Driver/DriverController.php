<?php

namespace App\Http\Controllers\Admin\Modules\Driver;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\Driver;
use App\Merchant;
use App\Models\OrderDetail;
use App\Models\DriverReview;
use Validator;
use Auth;
use App\Models\Language;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Repositories\DriverRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\CountryRepository;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\CityRepository;
use App\Repositories\ShippingCostRepository;
use App\Repositories\OrderImageRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductRepository;
use App\Repositories\UserRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\ConversationsRepository;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailDriverdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;
use App\Mail\VerifyDriverNewEmail;

class DriverController extends Controller
{
    protected $drivers,$language,$countries;
    public function __construct(DriverRepository $drivers,
                                OrderMasterRepository $order_master,
                                OrderDetailRepository $order_details,
                                OrderSellerRepository $order_sellers,
                                LanguageRepository $language,
                                UserRepository $customers,
                                CountryRepository $countries,
                                CityRepository $city,
                                ShippingCostRepository $shipping_cost,
                                OrderImageRepository $order_image,
                                ProductVariantRepository $product_variant,
                                ProductRepository $product,
                                MerchantRepository $merchants,
                                ConversationsRepository $conversations,
                                Request $request
                            )
    {
        $this->middleware('admin.auth:admin');
        $this->checkLoginAccess();
        $this->drivers            	=   $drivers;
        $this->language             =   $language;
        $this->countries            =   $countries;
        $this->order_master             =   $order_master;
        $this->order_details            =   $order_details;
        $this->order_sellers            =   $order_sellers;
        $this->merchants                =   $merchants;
        $this->customers                =   $customers;
        $this->city                     =   $city;
        $this->shipping_cost            =   $shipping_cost;
        $this->order_image              =   $order_image;
        $this->product_variant          =   $product_variant;
        $this->product                  =   $product;
        $this->conversations            =   $conversations;
        // dd(auth::guard('admin')->user()->type);
    }
    
    /*
    * Method        : addDriver
    * Description   : This method is used to add Driver
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function addDriver(Request $request){
        if(@$request->all()){
            $request->validate([
                'fname'                         =>'required',
                'lname'                         =>'required',
                'email'                         =>'required|email',
                // 'email'                         =>'required|email|unique:drivers',
                'phone'                         =>'required',
                'profile_pic'                   =>'image|mimes:jpeg,jpg,png'
            ],[
                'fname.required'                =>'First name field is required',
                'lname.required'                =>'Last name field is required',
                'email.required'                =>'Email field is required',
                'phone.required'                =>'Phone field is required',
                'profile_pic.image'             =>'Please upload picture with extension jpg/jpeg/png'
            ]);
            $password = @$request->password;
            $new['fname']               = @$request->fname;
            $new['lname']               = @$request->lname;
            $new['email']               = @$request->email;
            $new['phone']               = @$request->phone;
            $new['email_vcode']         = time();
            $new['password']            = Hash::make($password);
            $new['status']              = 'A';
            if(@$request->profile_pic){
                $pic   = $request->profile_pic;
                $name  = time().'.'.$pic->getClientOriginalExtension();
                $pic->move('storage/app/public/driver/profile_pics/', $name);
                $new['image'] = $name;
            }
            $driver = $this->drivers->create($new);
            if(@$driver){
                $driverDetails = new \stdClass();
                $driverDetails->name = @$driver->fname." ".@$driver->lname; 
                $driverDetails->email =@$driver->email;
                $driverDetails->phone =@$driver->phone;
                $driverDetails->password =$password;
                Mail::send(new SendMailDriverdetails($driverDetails));
                session()->flash("success",__('success.-4032'));
            }else{
                session()->flash("error",__('errors.-5029'));   
            }
            return redirect()->route('admin.list.driver');
        }
    	return view('admin.modules.drivers.add_driver');
    }
    /*
    * Method        : editDriver
    * Description   : This method is used to edit Driver 
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function editDriver(Request $request,$id){
        $driver = $this->drivers->where('id',$id)->first();
        if(@$driver){
            if(@$request->all()){
                $request->validate([
                    'fname'                         =>'required',
                    'lname'                         =>'required',
                    'profile_pic'                   =>'image|mimes:jpeg,jpg,png'
                ],
                [
                    'fname.required'                =>'First name field is required',
                    'lname.required'                =>'Last name field is required',
                    'profile_pic.image'             =>'Please upload picture with extension jpg/jpeg/png'
                ]);
                if(@$request->fname){
                    $new['fname']               = @$request->fname;
                }
                $new['phone']                   = @$request->phone;
                if(@$request->lname){
                    $new['lname']               = @$request->lname;
                }
                if(@$request->email){
                    $new['email']           = @$request->email;
                }
                if(@$request->profile_pic){
                    $pic   = $request->profile_pic;
                    $name  = time().'.'.$pic->getClientOriginalExtension();
                    $pic->move('storage/app/public/driver/profile_pics/', $name);
                    $new['image'] = $name;
                }
                $this->drivers->where('id',@$id)->update($new);
                if(@$request->email){

                    // $customerDetails                =   new \stdClass();
                    // $customerDetails->fname         =   $driver->fname; 
                    // $customerDetails->email         =   $driver->email;
                    // $customerDetails->new_email     =   $request->email;
                    // $customerDetails->email_vcode   =   $vcode;
                    // $customerDetails->id            =   $driver->id;
                    // // dd($customerDetails);
                    // Mail::send(new VerifyDriverNewEmail($customerDetails));
                    session()->flash("success",__('success.-4037'));
                }
                session()->flash("success",__('success.-4033'));
                return redirect()->route('admin.list.driver');
            }else{
                return view('admin.modules.drivers.edit_driver')->with(['driver'=>@$driver]);   
            }
        }else{
            return redirect()->back();
        }
    	return view('admin.modules.drivers.edit_driver');
    }
    /*
    * Method        : manageDriver
    * Description   : This method is used to manage Driver
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function manageDriver(Request $request){
        $drivers = $this->drivers->with(['getRating'])->where('status','!=','D');
        $dd = $drivers;
        $dd = $dd->count();
        if(@$request->all()){
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

            if(@$request->columns['1']['search']['value']){
                $drivers = $drivers->where(function($query) use($request){
                    $query->Where('fname','like','%'.@$request->columns['1']['search']['value'].'%')
                    ->orWhere('lname','like','%'.@$request->columns['1']['search']['value'].'%')
                    ->orWhere('phone','like','%'.@$request->columns['1']['search']['value'].'%')
                    ->orWhere('email','like','%'.@$request->columns['1']['search']['value'].'%')
                    ->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".@$request->columns['1']['search']['value']."%");
                });
            }
            if(@$request->columns['2']['search']['value']){
                $drivers = $drivers->where(function($query) use($request){
                    $query->Where('status', @$request->columns['2']['search']['value']);
                });
                $drivers = $drivers->where(function($query) use($request){
                    $query->whereNotIn('status', ['D']);
                });
            }
            $ddd = $drivers;
            $ddd = $ddd->count();

            // sorting
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $drivers = $drivers->orderBy('fname','asc');
                } else {
                    $drivers = $drivers->orderBy('fname','desc');
                }
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $drivers = $drivers->orderBy('email','asc');
                } else {
                    $drivers = $drivers->orderBy('email','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $drivers = $drivers->orderBy('phone','asc');
                } else {
                    $drivers = $drivers->orderBy('phone','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $drivers = $drivers->orderBy('status','asc');
                } else {
                    $drivers = $drivers->orderBy('status','desc');
                }
            }

            $drivers = $drivers->skip($request->start)->take($request->length)->get();

            $data['aaData'] = $drivers;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $dd;
            $data["iTotalDisplayRecords"] = @$ddd;
            return response()->json($data);
        } else {
            $drivers = $drivers->orderBy('id','desc')->get();
            return view('admin.modules.drivers.list_drivers')->with([
                'drivers'=>@$drivers
            ]);
        }
    }

    /*
    * Method        : checkDuplicateEmail
    * Description   : This method is used to checkDuplicateEmail
    * Author        : Jayatri
    * Date          : 05.02.2020
    */
    public function checkDuplicateEmail(Request $request){
        $driver = Driver::where('id','!=',0);
        $driver = $driver->where(function($q1) use($request){
            $q1 = $q1->where('email',@$request->email);
        });
        $driver = $driver->where(function($q2) use($request) {
            $q2 = $q2->where('status','!=','D');
        });
        $driver = $driver->first();
         // $driver;
        if(@$driver){
            return 0;
        }else{
            return 1;
        }
    }

    /*
    * Method        : checkDuplicateEmail
    * Description   : This method is used to checkDuplicateEmail
    * Author        : Jayatri
    * Date          : 05.02.2020
    */
    public function checkDuplicatePhone(Request $request){
        $driver = Driver::where('status','!=','D');
        $driver = $driver->where(function($q1) use($request){
            $q1 = $q1->where('phone',@$request->phone);
        });
        $driver = $driver->where(function($q2) use($request) {
            $q2 = $q2->where('status','!=','D');
        });
        // $driver = $driver->where(function($q2) use($request) {
        //     $q2 = $q2->whereNotIn('status',['D']);
        // });
        $driver = $driver->first();
        if(@$driver){
            return 0;
        }else{
            return 1;
        }
    }

    /*
    * Method        : checkUpdateDuplicateEmail
    * Description   : This method is used to check Update Duplicate Email
    * Author        : Jayatri
    * Date          : 05.02.2020
    */
    public function checkUpdateDuplicateEmail(Request $request){
        // $driver = $this->drivers->where(['email'=>@$request->email])->whereNotIn('email',[@$request->old_email])->where('status', '!=', 'D')->first();
        $driver = $this->drivers;

        $driver = $driver->where(function($q1) use($request){
            $q1 = $q1->whereNotIn('email',[@$request->old_email]);
        });
        $driver = $driver->where(function($q3) use($request){
            $q3 = $q3->where('email',@$request->email);
        });
        $driver = $driver->where(function($q2) use($request) {
            $q2 = $q2->whereNotIn('status',['D']);
        });
        $driver = $driver->first();
        if(@$driver){

            return 0;
        }else{
            return 1;
        }
    }

    /*
    * Method        : checkUpdateDuplicatePhone
    * Description   : This method is used to check Update Duplicate Phone.
    * Author        : Jayatri
    * Date          : 05.02.2020
    */
    public function checkUpdateDuplicatePhone(Request $request){
        $driver = $this->drivers;
        $driver = $driver->where(function($q1) use($request){
            $q1 = $q1->where('phone',@$request->phone);
        });
        $driver = $driver->where(function($q2) use($request) {
            $q2 = $q2->whereNotIn('status',['D']);
        });
        $driver = $driver->first();
        if(@$driver){
            return 0;
        }else{
            return 1;
        }
    }

    /*
    * Method        : deleteDriver
    * Description   : This method is used to delete Driver
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function sendMessage(Request $request){
        $driver = $this->drivers->where('id',$request->id)->first();
        if(@$driver){
            $new['user_type'] = 'D';
            $new['send_by'] = @Auth::guard('admin')->user()->id;
            $new['conversation_id'] = time();
            $new['email_id'] = @$request->email;
            $new['title'] = @$request->title;
            $new['messages'] = @$request->message;
            $new['name'] = $driver->fname." ".$driver->lname;
            $new['send_to'] = $driver->id;
            $conversation = $this->conversations->create($new);
            
            $merchantDetails = new \stdClass();
            $merchantDetails->name = @$request->name; 
            $merchantDetails->fname = @$request->name; 
            $merchantDetails->email =@$request->email;
            $merchantDetails->title =@$request->title;
            $merchantDetails->message =@$request->message;
            Mail::send(new SendMailToMerchant($merchantDetails));
            return 1;
                
        }else{
            return 0;
        }
    }

    /*
    * Method        : sendNotification
    * Description   : This method is used to send Notification.
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function sendNotification($id,Request $request){
        $driver = $this->drivers->where('id',$id)->first();
        if(@$driver){
            

            $merchantDetails            =   new \stdClass();
            $merchantDetails->name      =   $driver->name; 
            $merchantDetails->fname     =   $driver->fname; 
            $merchantDetails->email     =   $driver->email;
            $merchantDetails->title     =   $request->title;
            $merchantDetails->message   =   $request->message;
            Mail::send(new SendMailToMerchant($merchantDetails));

            session()->flash("success",__('success.-4036'));
        }else{
            session()->flash("error",__('errors.-5032'));
        }
        return redirect()->back();
    }

    /*
    * Method        : changeStatusDriver
    * Description   : This method is used to change Status Driver
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function changeStatusDriver($id){
        $driver = $this->drivers->where('id',@$id)->first();
        if(@$driver){
            if(@$driver->status == 'I'){
                $update['status'] = 'A';
            }else if(@$driver->status == 'A'){
                $update['status'] = 'I';
            }
            $this->drivers->where('id',@$id)->update($update);
            session()->flash("success",__('success.-4035'));
        }else{
            session()->flash("error",__('errors.-5032'));
        }
        return redirect()->back();
    }

    /*
    * Method        : blockUnblockDriver
    * Description   : This method is used to block unblock driver
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function blockUnblockDriver(Request $request){
        
        $driver = $this->drivers->where('id',@$request->id)->first();

        if(@$driver){
            if(@$driver->status == 'I'){
                $update['status'] = 'A';
            }else if(@$driver->status == 'A'){
                $update['status'] = 'I';
            }
            $this->drivers->where('id',@$request->id)->update($update);
            $driver = $this->drivers->where('id',@$request->id)->first();
            // session()->flash("success",__('success.-4035'));
            return response()->json($driver);
        }else{
            // session()->flash("error",__('errors.-5032'));
            return 0;
        }
        // return redirect()->back();
    }

    /*
    * Method        : viewCustomerProfile
    * Description   : This method is used to view customer profile.
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function viewDriverProfile(Request $request,$id){
        $driver = $this->drivers->where('id',@$id)->first();
        if(@$driver){
            // dd($driver->id);
            $orders = $this->order_master->with(['customerDetails','orderMasterDetails.driverDetails','countryDetails','orderMasterDetails','orderMasterDetails.productDetails','orderMasterDetails.productVariantDetails','orderMasterDetails.sellerDetails','countryDetails','shippingAddress','billingAddress','getBillingCountry','productVariantDetails'])->where('status','OD');
            $orders = $orders->whereHas('orderMasterDetails', function($q) use ($request,$driver,$id){
                   $q->where('driver_id',$driver->id);
                });
            $orders = $orders->orderBy('id','desc')->get();
            $total_no = $orders->count();
            $total_sum = $orders->sum('subtotal');
            $total_cost = $orders->sum('order_total');
            // dd($orders);
            return view('admin.modules.drivers.view_profile')->with([
                'driver'=>@$driver,
                'total_no'=>@$total_no,
                'total_sum'=>@$total_sum,
                'total_cost'=>@$total_cost,
                'orders'=>@$orders
            ]);
        }else{
            return redirect()->back();
        }
    }

    /*
    * Method        : deleteDriver
    * Description   : This method is used to delete Driver
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function deleteDriver($id){
        $driver = $this->drivers->where('id',@$id)->first();

        if(@$driver){
            if(@$driver->image){
                $image_path = '/storage/app/public/driver/profile_pics/'.$driver->image; 
                if (file_exists(@$image_path)){
                    unlink(@$image_path);
                }
            }
            $this->drivers->where('id',@$id)->update(['status' => 'D']);
            // $this->drivers->where('id',@$id)->delete();
            session()->flash("success",__('success.-4034'));
        }else{
            session()->flash("error",__('errors.-5031'));
        }
    	return redirect()->back();
    }

    /*
    * Method        : deleteDriverDetails
    * Description   : This method is used to delete Driver(ajax)
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function deleteDriverDetails(Request $request){
        $driver = $this->drivers->where('id',@$request->id)->first();
        if(@$driver){
            $chk = Merchant::where('driver_id',@$driver->id)->where('status', '!=', 'D')->count();
            $ord_chk = OrderDetail::with(['orderMaster'])
                                    ->where('driver_id',@$driver->id)
                                    ->whereHas('orderMaster', function($q) {
                                        $q->where('status', '!=', 'D');
                                    })
                                    ->count();
            if($chk > 0 || $ord_chk > 0) {
                return 0;
            } else{
                $this->drivers->where('id',@$request->id)->update(['status' => 'D']);
                Merchant::where('driver_id',@$driver->id)->update(['driver_id' => 0]);
                return 1;
            }
        }else {
            return 0;
        }
    }

    /*
    * Method        : driverRatingList
    * Description   : This method is used to show driver rating list
    * Author        : Argha
    * Date          : 2020-Sep-09
    */
    public function driverRatingList(Request $request)
    {
        $drivers_rating = DriverReview::where('driver_id',$request->id)->with(['getMerchant','getCustomer','getOrder']);
        $dd = $drivers_rating;
        $dd = $dd->count();
        if(@$request->all()){
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

            // if(@$request->columns['1']['search']['value']){
            //     $drivers = $drivers->where(function($query) use($request){
            //         $query->Where('fname','like','%'.@$request->columns['1']['search']['value'].'%')
            //         ->orWhere('lname','like','%'.@$request->columns['1']['search']['value'].'%')
            //         ->orWhere('phone','like','%'.@$request->columns['1']['search']['value'].'%')
            //         ->orWhere('email','like','%'.@$request->columns['1']['search']['value'].'%')
            //         ->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".@$request->columns['1']['search']['value']."%");
            //     });
            // }
            // if(@$request->columns['2']['search']['value']){
            //     $drivers = $drivers->where(function($query) use($request){
            //         $query->Where('status', @$request->columns['2']['search']['value']);
            //     });
            //     $drivers = $drivers->where(function($query) use($request){
            //         $query->whereNotIn('status', ['D']);
            //     });
            // }
            $ddd = $drivers_rating;
            $ddd = $ddd->count();

            // sorting
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $drivers_rating = $drivers_rating->whereHas('getOrder',function($q){
                        $q->orderBy('order_no','asc');
                    });
                } else {
                    $drivers_rating = $drivers_rating->whereHas('getOrder',function($q){
                        $q->orderBy('order_no','desc');
                    });
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $drivers_rating = $drivers_rating->orderBy('review_point','asc');
                } else {
                    $drivers_rating = $drivers_rating->orderBy('review_point','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $drivers_rating = $drivers_rating->orderBy('review_comments','asc');
                } else {
                    $drivers_rating = $drivers_rating->orderBy('review_comments','desc');
                }
            }

            $drivers_rating = $drivers_rating->skip($request->start)->take($request->length)->get();

            $data['aaData'] = $drivers_rating;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $dd;
            $data["iTotalDisplayRecords"] = @$ddd;
            return response()->json($data);
        } else {
            return view('admin.modules.drivers.rating_list');
        }
    }
    private function checkLoginAccess(){
        if(!@Auth::guard('admin')->user()){
            return redirect()->route('admin.login');
        }
    }

}
