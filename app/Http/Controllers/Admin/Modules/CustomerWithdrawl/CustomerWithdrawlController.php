<?php

namespace App\Http\Controllers\Admin\Modules\CustomerWithdrawl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wallet;
use App\Models\WalletDetails;
use App\Models\City;
use App\User;
use App\Merchant;
use App\Models\Product;
use App\Models\OrderMaster;
use App\Models\CustomerWithdraw;
use App\Models\OrderSeller;
use App\Models\OrderDetail;
use Validator;

class CustomerWithdrawlController extends Controller
{
    //
    public function customerWalletHistory(Request $request){
        $data['customers'] = User::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        $data['cities'] = City::whereNotIn('status',['D'])->orderBy('name','asc')->get();
        $data['merchants'] = Merchant::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        return view('admin.modules.customer_wallet.customer_wallet')->with($data);
    }
    public function customerSpecificWalletHistory($id,Request $request){
        $data['customers'] = User::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        $data['cities'] = City::whereNotIn('status',['D'])->orderBy('name','asc')->get();
        $data['merchants'] = Merchant::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        $data['id'] = $id;
        $data['user'] =  User::where('id',$id)->first();
        return view('admin.modules.customers.customer_specific_wallet_history')->with($data);
    }
    public function viewCustomerWithdrawDetails($id,Request $request){
        $columnIndex = $request->order[0]['column'];
        $columnSortOrder = $request->order[0]['dir'];
        $o = CustomerWithdraw::where('id','!=',0);
            $ps =  $o->count();
            $pp = $o;
            $pp = $pp->count();
            $limit = $request->length;
            $p = $o = $o;
            $p =  $p->count();
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('bank_name','asc');
                } else {
                    $o = $o->orderBy('bank_name','desc');
                }
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('branch_name','asc');
                } else {
                    $o = $o->orderBy('branch_name','desc');
                }
                
            }
            if($columnIndex == 2) {
                
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('account_number','asc');
                } else {
                    $o = $o->orderBy('account_number','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('account_number','asc');
                } else {
                    $o = $o->orderBy('account_number','desc');
                }
                
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('account_name','asc');
                } else {
                    $o = $o->orderBy('account_name','desc');
                }
                
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('ifsc_code','asc');
                } else {
                    $o = $o->orderBy('ifsc_code','desc');
                }
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('amount','asc');
                } else {
                    $o = $o->orderBy('amount','desc');
                }
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('description','asc');
                } else {
                    $o = $o->orderBy('description','desc');
                }
            }
            
            $o = $o->orderBy('id','desc')->skip($request->start)->take($request->length)->get();
            $data['aaData'] = $o;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $ps;
            $data["iTotalDisplayRecords"] = $pp;
            return response()->json($data);

    }
    public function customerSpecificviewWalletDetails($id,Request $request){
        $columnIndex = $request->order[0]['column'];
        $columnSortOrder = $request->order[0]['dir'];

        $o = WalletDetails::with(['get_order_master','view_user','product_by_language'])->where('user_id',$id);
            
            if(@$request->columns['0']['search']['value']){
                $o = $o->whereHas('get_order_master',function($q1) use($request) {
                    $q1 = $q1->where('order_no','like','%'.$request->columns['0']['search']['value'].'%');
                });
            }
            
            $ps =  $o->count();
            $pp = $o;
            $pp = $pp->count();
            $limit = $request->length;
            $p = $o = $o;
            $p =  $p->count();
            if($columnIndex == 0) {

                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('id','asc');
                } else {
                    $o = $o->orderBy('id','desc');
                }
                
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('order_id','asc');
                } else {
                    $o = $o->orderBy('order_id','desc');
                }
                
            }
            if($columnIndex == 2) {
                
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('id','asc');
                } else {
                    $o = $o->orderBy('id','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('id','asc');
                } else {
                    $o = $o->orderBy('id','desc');
                }
                
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('amount','asc');
                } else {
                    $o = $o->orderBy('amount','desc');
                }
                
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('w_d_date','asc');
                } else {
                    $o = $o->orderBy('w_d_date','desc');
                }
                
            }
            
            $o = $o->orderBy('id','desc')->skip($request->start)->take($request->length)->get();
            $data['aaData'] = $o;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $ps;
            $data["iTotalDisplayRecords"] = $pp;
            return response()->json($data);

    }
    public function customerWithdraw($id,Request $request){
        if(@$request->all()){
            $customer = User::where('id',$id)->first();
            if(!@$customer){
                return redirect()->route('admin.list.customer');
            }
            if(!@$customer->bank_name || !@$customer->branch_name || !@$customer->account_number || !@$customer->account_name || !@$customer->ifsc_code){
                return redirect()->route('admin.list.customer');
            }
            $validator = $request->validate([
                'description'        => 'required',
                'amount'       => 'required|numeric|min:1'
            ]);
            if(!$validator) {
                return redirect()->back()->with("error","description required and amount should be greater than 1 and value should be a number!");
            }
            
                if($customer->wallet_amount >= $request->amount && $customer->wallet_amount > 0){
                    $crcw['buyer_id'] = $id;
                    $crcw['buyer_withdrawl_date'] = date('Y-m-d');
                    $crcw['bank_name'] = $customer->bank_name;
                    $crcw['branch_name'] = $customer->branch_name;
                    $crcw['account_number'] = $customer->account_number;
                    $crcw['account_name'] = $customer->account_name;
                    $crcw['ifsc_code'] = $customer->ifsc_code;
                    $crcw['description'] = $request->description;
                    $crcw['amount'] = $request->amount;
                    $cw = CustomerWithdraw::create($crcw);
                    $uupd['wallet_amount'] = $customer->wallet_amount - $request->amount;
                    User::where('id',$id)->update($uupd);
                    return redirect()->route('admin.view.customer.profile',$customer->id)->with("success","Customer withdraw created successfully!");
                }else{
                    return redirect()->route('admin.view.customer.profile',$customer->id);
                }
            
        }else{
            $data['customer'] = $customer = User::where('id',$id)->first();
            if(!@$customer){
                return redirect()->route('admin.list.customer');
            }
            if(!@$customer->bank_name){
                return redirect()->route('admin.view.customer.profile',$customer->id);
            }
            return view('admin.modules.customer_withdraw.add_customer_withdraw',$data);
        }
    }
    public function viewCustomerSpecificOrders($id,Request $request){
        $columnIndex = $request->order[0]['column']; // Column index
        $columnName = $request->columns[$columnIndex]['data']; // Column name
        $columnSortOrder = $request->order[0]['dir']; // asc or desc
        $orders = OrderMaster::with('getCityNameByLanguage');
        $orders = $orders->where('status','!=','D');
        $orders = $orders->where('status','!=', '');
        $ps =  $orders->count();
        $pp = $orders;
        $pp = $pp->count();
        $limit = $request->length;
        $p = $orders = $orders->with([
            'customerDetails',
            'orderMasterDetails.driverDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'orderMasterDetails.productDetails.productUnitMasters',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails',
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'shipping_city_details',
            'shipping_state_details',
            'getBillingCountry',
            'productVariantDetails',
            'getCityNameByLanguage',
            'paymentData',
            'getAdminForExternalOrder',
            'orderMerchants.orderSeller',
            'editedBy',
            'getPreferredTime',
        ])->whereNotIn('status',['I','D'])->where(['user_id'=>$id]);
        
        $p = $p->count();
        
        if(@$request->all()){
            if(@$request->columns['0']['search']['value']){
                $orders = $orders->where(function($q1) use($request) {
                    $q1 = $q1->where('order_no','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_fname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_lname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_phone','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_email','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".$request->columns['0']['search']['value']."%");
                });
            }
            if(@$request->columns['1']['search']['value']){
                $date = explode(',', $request->columns['1']['search']['value']);
                $from_date = $date[0];
                $to_date = $date[1];
                $orders = $orders->whereBetween('created_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);
            }
            if(@$request->columns['11']['search']['value']){
                $orders = $orders->where(function($q2) use($request) {
                    $q2 = $q2->where('invoice_no','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('invoice_details','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('notes','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('driver_notes','like','%'.$request->columns['11']['search']['value'].'%');
                });
            }
            if(@$request->columns['19']['search']['value']){
                $orders = $orders->where(function($q2) use($request) {
                    $q2 = $q2->where('shipping_country','!=',134);
                });
                $orders = $orders->where(function($q2) use($request) {
                    $q2 = $q2->where('shipping_city','like','%'.$request->columns['19']['search']['value'].'%');
                });
            }
            # order type - all type accessible for super admin but only this is accessible which is given by admin
            $roles = Auth()->guard('admin')->user()->roles->pluck('menu_id')->toArray();
            if(@$request->columns['7']['search']['value']){
                if(Auth()->guard('admin')->user()->type == 'A') {
                    $orders = $orders->where('order_type',$request->columns['7']['search']['value']);
                    if(@$request->columns['23']['search']['value'] && $request->columns['7']['search']['value'] == 'E'){
                        if($request->columns['23']['search']['value'] == 'A'){
                            $orders = $orders->where('order_type','E');
                        } elseif($request->columns['23']['search']['value'] == 'S' || $request->columns['23']['search']['value'] == 'L') {
                            $orders = $orders->where('external_order_type',$request->columns['23']['search']['value']);
                        }
                    }
                } else {
                    # both order accessible
                    if(in_array(6, $roles) && in_array(13, $roles)) {
                        $orders = $orders->where('order_type', $request->columns['7']['search']['value']);
                        if(@$request->columns['23']['search']['value'] == 'E'){
                            if($request->columns['23']['search']['value'] == 'A'){
                                $orders = $orders->where('order_type','E');
                            }elseif($request->columns['23']['search']['value'] == 'S' || $request->columns['23']['search']['value'] == 'L'){
                                $orders = $orders->where('external_order_type',$request->columns['23']['search']['value']);
                            }
                        }
                    } else if(in_array(6, $roles)) {
                        $orders = $orders->where('order_type', 'I');
                    } else if(in_array(13, $roles)) {
                        $orders = $orders->where('order_type', 'E');
                        if(@$request->columns['23']['search']['value'] && $request->columns['7']['search']['value'] == 'E'){
                            if($request->columns['23']['search']['value'] == 'A') {
                                $orders = $orders->where('order_type','E');
                            } elseif($request->columns['23']['search']['value'] == 'S' || $request->columns['23']['search']['value'] == 'L'){
                                $orders = $orders->where('external_order_type',$request->columns['23']['search']['value']);
                            }
                        }
                    }
                } 
            } 
            
            # for sub admin without filter
            if(Auth()->guard('admin')->user()->type == 'S'){
                # both order accessible
                if(in_array(6, $roles) && in_array(13, $roles)) {
                    $orders = $orders->whereIn('order_type', ['I', 'E']);
                } else if(in_array(6, $roles)) {
                    $orders = $orders->where('order_type', 'I');
                } else if(in_array(13, $roles)) {
                    $orders = $orders->where('order_type', 'E');
                }
            } 
            if(@$request->columns['8']['search']['value']){
                $orders = $orders->where('payment_method',$request->columns['8']['search']['value']);
            }
            if(@$request->columns['5']['search']['value']){
                $status = explode(',', $request->columns['5']['search']['value']);
                $orders = $orders->whereIn('status', $status);
                /*if(in_array($request->columns['6']['search']['value'], ['I', 'F'])) {
                    $orders = $orders->whereIn('status', ['I', 'F']);    
                } else {
                }*/
            }
            if(@$request->columns['4']['search']['value']){
                $orders = $orders->whereHas('orderMasterDetails', function($q) use ($request){
                   $q->where('seller_id',$request->columns['4']['search']['value']);
                });
            }
            if(@$request->columns['14']['search']['value']){
                $orders = $orders->whereHas('orderMasterDetails', function($q4) use ($request){
                   $q4->where('driver_id',$request->columns['14']['search']['value']);
                });
            }
            if(@$request->columns['17']['search']['value']){
                $orders = $orders->where('order_from', $request->columns['17']['search']['value']);
            }
            if(@$request->columns['3']['search']['value']){
                $orders = $orders->where( function($q4) use ($request){
                   $q4->where('user_id',$request->columns['3']['search']['value']);
                });
            }
            if(@$request->columns['2']['search']['value']){
                $orders = $orders->where( function($q4) use ($request){
                   $q4->where('shipping_city',$request->columns['2']['search']['value']);
                });
            }
            if(@$request->columns['6']['search']['value']){
                $orders = $orders->whereHas('paymentData', function($q4) use ($request){
                   $q4->where('txn_id',$request->columns['6']['search']['value']);
                });
            }
            if(@$request->columns['22']['search']['value']){
                $orders = $orders->where('coupon_code', @$request->columns['22']['search']['value']);
            }
            $pp = $orders;
            $pp = $pp->count();
            $limit = $request->length;
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('order_no','asc');
                } else {
                    $orders = $orders->orderBy('order_no','desc');
                }
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('created_at','asc');
                } else {
                    $orders = $orders->orderBy('created_at','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('shipping_fname','asc');
                } else {
                    $orders = $orders->orderBy('shipping_lname','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('shipping_city','asc');
                } else {
                    $orders = $orders->orderBy('shipping_city','desc');
                }
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('payable_amount','asc');
                } else {
                    $orders = $orders->orderBy('payable_amount','desc');
                }
            }
            if($columnIndex == 6) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('payment_method','asc');
                } else {
                    $orders = $orders->orderBy('payment_method','desc');
                }
            }
            
            if($columnIndex == 7) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('status','asc');
                } else {
                    $orders = $orders->orderBy('status','desc');
                }
            }
            if($columnIndex == 8) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('id','asc');
                } else {
                    $orders = $orders->orderBy('id','desc');
                }
            }
            
            if($columnIndex == 9) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('order_from','asc');
                } else {
                    $orders = $orders->orderBy('order_from','desc');
                }
            }
            //For total subtotal and shipping_cost calculate
            $getOrder = $orders;
            $getOrder = $getOrder->pluck('id');
            
            $orders = $orders->skip($request->start)->take($request->length)->get();
            $data['aaData'] = $orders;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $ps;
            $data["iTotalDisplayRecords"] = $pp;
            $codOrder = OrderMaster::whereIn('id', $getOrder)->where('payment_method', 'C')->whereNotIn('status', ['I', 'F']);
            
            $totalCodSubtotal = $codOrder->sum('subtotal');
            $totalCodSubtotal1 = $codOrder->sum('total_product_price');
            $totalCodSubtotal = $totalCodSubtotal - $totalCodSubtotal1;
            $totalCodShpPrc = $codOrder->sum('loading_price');
            $totalCodShpPrc = $codOrder->sum('order_total');

            $onlineOrder = OrderMaster::whereIn('id', $getOrder)->where('payment_method', 'O')->whereNotIn('status', ['I', 'F']);

            $totalOnlineSubtotal = $onlineOrder->sum('subtotal');
            $totalOnlineSubtotal1 = $onlineOrder->sum('total_product_price');
            $totalOnlineSubtotal = $totalOnlineSubtotal - $totalOnlineSubtotal1;
            $totalOnlineShpPrc = $onlineOrder->sum('loading_price');
            $totalOnlineOrder = $onlineOrder->sum('order_total');

            $data['totalCodSubtotal'] = number_format($totalCodSubtotal, 2);
            $data['totalOnlineSubtotal'] = number_format($totalOnlineSubtotal, 2);
            $data['totalCodShpPrc'] = number_format($totalCodShpPrc, 2);
            $data['totalOnlineShpPrc'] = number_format($totalOnlineShpPrc, 2);
            $data['orderTotal'] = number_format($totalOnlineOrder, 2);
            return response()->json($data);    
        } else {
            $orders =$orders->orderBy('id','desc')->get();
            return response()->json($orders); 
        }    
    }
}
