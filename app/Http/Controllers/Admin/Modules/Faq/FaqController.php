<?php

namespace App\Http\Controllers\Admin\Modules\Faq;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryDetail;
use App\Models\Language;
use App\Models\Admin;
use App\Models\Brand;
use App\Models\BrandDetail;
use App\Models\Product;


use App\Models\FaqCategoryDetail;
use App\Models\FaqCategoryMaster;
use App\Models\FaqDetail;
use App\Models\FaqMaster;

use Validator;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
        $this->checkLoginAccess();
        $this->middleware('admin.auth:admin');
        
        
    }
    /*
    * Method        : index
    * Description   : This method is used to show list of product manufacturer from brand & brand details.
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function index(Request $request){

        $this->checkLoginAccess();
        $this->checkAccess();

        $brands = FaqMaster::with(['faqDetailsByLanguage', 'categoryByFaq']);
    	$category = FaqCategoryDetail::get();
        $brands = $brands->orderBy('id','desc')->get();
    
        return view('admin.modules.faq.list_faq')->with([
            'category'=>@$category,
            'brands'=>@$brands
        ]);
    }
    
    /*
    * Method        : addManufacturer
    * Description   : This method is used to add product manufacturer 
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */

    public function addFaq(){

        $this->checkLoginAccess();
        $this->checkAccess();
        
        $category = FaqCategoryMaster::with(['categoryDetailsByLanguage'])->get();
        $language = Language::where('status','A')->get();
        return view('admin.modules.faq.add_faq')->with([
            'category'=>@$category,
            'language'=>@$language,
        ]);
    }




    public function postFaq(Request $request){
        $this->checkLoginAccess();
        $this->checkAccess();
        $category = FaqCategoryMaster::with(['categoryDetailsByLanguage'])->get();
        $language = Language::where('status','A')->get();
        
        if(@$request->all()){
            // $request->validate([
            //     'category'=>'required'                
            // ],[
            //     'category.required'=>'Category field is required'
            // ]);

            DB::transaction(function() use($request,$language,$category){
                $new['type'] = $request->type;
                $new['display_order'] = $request->display_order;
                $faqdata = FaqMaster::create($new);
                
                foreach(@$request->faq_ques as $key=>$val){
                    $new1['faq_ques'] = $val;
                    $new1['language_id'] = $language[$key]->id;
                    $new1['faq_master_id'] = $faqdata->id;
                    $new1['faq_answer'] = $request->faq_answer[$key];
                    $brandDetails = FaqDetail::create($new1);
                }
                session()->flash("success",__('success.-4070'));
            });
            return redirect()->back();
        }

    }

    //edit page
    public function edeitFaq(Request $request,$id){
        $this->checkLoginAccess();
        $this->checkAccess();

        $brands = FaqMaster::with(['faqDetails', 'categoryByFaq'])->where('id',$id)->first();
        $language = Language::where('status','A')->get();
        $category = FaqCategoryMaster::with(['categoryDetailsByLanguage'])->get();

        return view('admin.modules.faq.edit_faq')->with([
         'category'=>@$category,
         'language'=>@$language,
         'brand'=>@$brands,
        ]);
    }

    //edit post page
    public function postEditFaq(Request $request,$id){
        $this->checkLoginAccess();
        $this->checkAccess();

        $brand = FaqMaster::with(['faqDetails', 'categoryByFaq'])->where('id',$id)->first();
        $language = Language::where('status','A')->get();
        $category = FaqCategoryMaster::with(['categoryDetailsByLanguage'])->get();
        
        if(@$brand){
            $brandDetails = FaqDetail::where('faq_master_id',$brand->id)->get();
            if(@$brandDetails){
                if($request->all()){
                    DB::transaction(function() use($request,$language,$category,$brand,$id){
                        if(@$request->faq_ques){
                            foreach(@$request->faq_ques as $key=>$val){
                                $update['faq_ques'] = $val;
                                $update['faq_answer'] = $request->faq_answer[$key];
                                FaqDetail::where(['faq_master_id'=>$brand->id,'language_id'=>$language[$key]->id])->update($update);
                            }
                        }
                        session()->flash("success",__('success.-4068'));
                    });
                    $up['type'] = $request->type;
                    $up['display_order'] = $request->display_order;
                    FaqMaster::whereId(@$brand->id)->update($up);
                    return redirect()->back();
                }
            }
        }else{
            //unauthorized access
            session()->flash("error",__('errors.-5007'));
            return redirect()->route('admin.list.faq');
        }
    }


    public function deleteFaq($id){
        $this->checkLoginAccess();
        $this->checkAccess();
        $brand = FaqMaster::where('id',$id)->first();
        
        if(@$brand){
            $brandDetails = FaqDetail::where('faq_master_id',$brand->id)->get();
            if(@$brandDetails){
                DB::transaction(function() use($id,$brand){
                    FaqDetail::where('faq_master_id',$brand->id)->delete();
                    FaqMaster::where('id',$id)->delete();
                });
                // session()->flash("success");
                session()->flash("success",__('success.-4069'));
                return redirect()->back();
            }else{
                session()->flash("error");
                return redirect()->route->back();
            }
        }else{
            session()->flash("error");
            return redirect()->route->back();
        }
        return redirect()->back();
    }




    private function checkAccess(){
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(2, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
    }
    private function checkLoginAccess(){
        if(!@Auth::guard('admin')->user()){
            return redirect()->route('admin.login');
        }
    }

    /**
    * Method: addFaqCategory
    * Description: This method is used to add faq category.
    * Author: Sanjoy
    */
    public function addFaqCategory($id = NULL) {
        $data['languages'] = Language::get();
        $data['faq_categories'] = FaqCategoryMaster::with(['categoryDetailsLanguage'])->get();

        if(@$id) {
            $data['faq_category'] = FaqCategoryMaster::with(['categoryDetails'])->first();
        }
        return view('admin.modules.faq.add_category')->with($data);
    }

    /**
    * 
    */
    public function postFaqCategory(Request $request) {
        #for edit
        if(@$request->master_id) {
            DB::transaction(function() use($request){
                $new = [
                    'status' => 'A'
                ];
                FaqCategoryDetail::where(['faq_category_master_id' => $request->master_id])->delete();
                foreach(@$request->categories as $key=>$val){
                    $new1['language_id']            = $key;
                    $new1['faq_category_master_id'] = $request->master_id;
                    $new1['faq_category']           = $val;
                    FaqCategoryDetail::create($new1);
                }
            });
        } else {
            # for adding
            DB::transaction(function() use($request){
                $new = [
                    'status' => 'A'
                ];
                $catMaster = FaqCategoryMaster::create($new);
                
                foreach(@$request->categories as $key=>$val){
                    $new1['language_id']            = $key;
                    $new1['faq_category_master_id'] = $catMaster->id;
                    $new1['faq_category']           = $val;
                    FaqCategoryDetail::create($new1);
                }
            });
        }
        
        session()->flash("success",__('success.-4086'));
        return redirect()->route('admin.faq.add.category');
    }

    /**
    *
    */
    public function deleteFaqCategory($id = NULL) {
        $faq = FaqMaster::where(['faq_category_id' => $id])->first();
        if(@$faq) {
            session()->flash('error', __('errors.-5079'));
            return redirect()->back();
        }
        FaqCategoryDetail::where(['faq_category_master_id' => $id])->delete();
        FaqCategoryMaster::where(['id' => $id])->delete();

        session()->flash('success', __('success.-4087'));
        return redirect()->back();
    }

}
