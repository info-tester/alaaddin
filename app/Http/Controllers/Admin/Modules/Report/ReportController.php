<?php

namespace App\Http\Controllers\Admin\Modules\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Models\Country;
use App\User;
use Validator;
use Auth;
use App\Merchant;
use App\Driver;
use App\Models\CategoryDetail;
use App\Models\Variant;
use App\Models\OrderMaster;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductDetail;
use App\Models\ProductVariantOthers;
use App\Models\VariantValue;
use App\Models\ProductVariant;
use App\Models\ProductVariantDetail;
use App\Models\ProductImage;
use App\Models\OrderDetail;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Language;
use App\Models\Admin;
use App\Models\City;
use App\Repositories\ShippingCostRepository;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\CityRepository;
use App\Repositories\UserRepository;
use App\Repositories\DriverRepository;
use App\Repositories\OrderImageRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductRepository;
use Excel;


use Illuminate\Support\Facades\Hash;

class ReportController extends Controller
{
    protected $order_master,$merchants,$customers,$drivers,$order_details,$order_sellers;
    public function __construct( OrderMasterRepository $order_master,
       MerchantRepository $merchants,
       UserRepository $customers,
       DriverRepository $drivers,
       OrderDetailRepository $order_details,
       OrderSellerRepository $order_sellers,
       CityRepository $city,
       ShippingCostRepository $shipping_cost,
       OrderImageRepository $order_image,
       ProductVariantRepository $product_variant,
       ProductRepository $product,
       Request $request
   )
    {
        $this->middleware('admin.auth:admin');
        $this->order_master             =   $order_master;
        $this->order_details            =   $order_details;
        $this->order_sellers            =   $order_sellers;
        $this->drivers                  =   $drivers;
        $this->merchants                =   $merchants;
        $this->customers                =   $customers;
        $this->city                     =   $city;
        $this->shipping_cost            =   $shipping_cost;
        $this->order_image              =   $order_image;
        $this->product_variant          =   $product_variant;
        $this->product                  =   $product;
    }

    /*
    * Method        : showEarningReport
    * Description   : This method is used to show earning report.
    * Author        : Surajit
    */
    public function showEarningReport(Request $request){
        $customers = $this->order_master->where('user_id','!=',0)->get();
        $external_customer = $this->order_master->where('user_id',0)->get();
        $driver = $this->drivers->where('status','!=','D')->get();
        $merchants = $this->merchants->where('status','!=','D')->get();
        $orders = $this->order_master->where('status', 'OD');

        if(@$request->all() && @$request->merchant_id == ''){
            if(@$request->id){
                $orders = $orders->where('id',$request->id);
            }
        }else{
            $orders = $orders->with([
                'customerDetails',
                'orderMasterDetails.driverDetails',
                'countryDetails',
                'orderMasterDetails',
                'orderMasterDetails.productDetails',
                'orderMasterDetails.productVariantDetails',
                'orderMasterDetails.sellerDetails',
                'countryDetails',
                'shippingAddress',
                'billingAddress',
                'getBillingCountry',
                'productVariantDetails'
            ])
            ->orderBy('id','desc');
        }

        if(@$request->merchant_id){
            $orders = $orders->Where('user_id',$request->merchant_id);
        }
        $orders = $orders->orderBy('created_at','desc')->get();
        $order_detail = $this->order_details->get();
        $order_seller = $this->order_sellers->get();
        $key = $request->all();

        return view('admin.modules.reports.earning_report')->with([
          'customers'       =>  @$customers,
          'drivers'         =>  @$driver,
          'orders'          =>  @$orders,
          'merchants'       =>  @$merchants,
          'order_detail'    =>  @$order_detail,
          'external_customer'   =>  @$external_customer,
          'order_seller'    =>  @$order_seller,
          'key'             =>  @$key
      ]);
    }


    /*
    * Method        : earningOrderlists
    * Description   : This method is used to view all orders by admin and manage it.
    * Author        : Surajit
    */
    public function earningOrderlists(Request $request){
        $columnIndex = $request->order[0]['column']; // Column index
        $columnName = $request->columns[$columnIndex]['data']; // Column name
        $columnSortOrder = $request->order[0]['dir']; // asc or desc

        $orders = $this->order_master->where('status','OD');
        $ps =  $orders->count();
        $pp = $orders;
        $pp = $pp->count();
        $limit = $request->length;
        $p = $orders = $orders->with([
            'customerDetails',
            'orderMasterDetails.driverDetails',
            'countryDetails',
            'orderMasterDetails',
            'orderMasterDetails.productDetails',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails',
            'countryDetails',
            'shippingAddress',
            'billingAddress',
            'getBillingCountry',
            'productVariantDetails'
        ])
        ->orderBy('id','desc');
        $p =  $p->count();
        //dd($orders->get());
        if($request->all()){
            if(@$request->columns['0']['search']['value']){
                $orders = $orders->Where('order_no','like','%'.$request->columns['0']['search']['value'].'%');
            }
            if(@$request->columns['4']['search']['value']){
                $orders = $orders->Where('order_type',$request->columns['4']['search']['value']);
            }
            if(@$request->columns['5']['search']['value']){
                $orders = $orders->Where('payment_method',$request->columns['5']['search']['value']);
            }
            if(@$request->columns['6']['search']['value']){
                $orders = $orders->Where('status',$request->columns['6']['search']['value']);
            }
            if(@$request->columns['7']['search']['value']){
                $sellerId = $request->columns['7']['search']['value'];
                $orders = $orders->with(['orderSellerInfo' => function($q) use($sellerId) {
                    $q->where('seller_id', $sellerId);
                }]);

                $orders = $orders->whereHas('orderMasterDetails', function($q) use ($request){
                   $q->where('seller_id',$request->columns['7']['search']['value']);
               });
            }
            if(@$request->columns['1']['search']['value']){
                $date = explode(',', $request->columns['1']['search']['value']);
                $from_date = $date[0];
                $to_date = $date[1];
                $orders = $orders->whereBetween('created_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);
            }
            $pp = $orders;
            $pp = $pp->count();
            $limit = $request->length;
            
            if(@$columnIndex){
                if($columnIndex == 0) {
                    $orders = $orders->get()->toArray();

                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['order_no'];
                        $c2 = @$b['order_no'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 1) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['customerDetails']['fname'];
                        $c2 = @$b['customerDetails']['fname'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 2) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['order_no'];
                        $c2 = @$b['order_no'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }
                if($columnIndex == 3) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['order_type'];
                        $c2 = @$b['order_type'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }
                if($columnIndex == 4) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['payment_method'];
                        $c2 = @$b['payment_method'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }
                if($columnIndex == 5) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['status'];
                        $c2 = @$b['status'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }
                if($columnIndex == 6) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['total_commission'];
                        $c2 = @$b['total_commission'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }
                if($columnIndex == 7) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['shipping_price'];
                        $c2 = @$b['shipping_price'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }
                if($columnIndex == 8) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['order_total'];
                        $c2 = @$b['order_total'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                $orders = array_slice($orders, $request->start, $limit);
                $p = array();
                foreach ($orders as $value) {
                    array_push($p, $value);
                }
                $orders = $p;
            } else {
                $orders = $orders->orderBy($columnName, $columnSortOrder)->skip($request->start)->take($limit)->get()->toArray();
            }
        }

        $data['aaData'] = $orders;
        $data["draw"] = intval($request->draw);
        $data['iTotalRecords'] = $ps;
        $data["iTotalDisplayRecords"] = $pp;
        return response()->json($data);
    }


    /*
    * Method        : showEarningReportExport
    * Description   : This method is used to export earning report.
    * Author        : Surajit
    */
    public function showEarningReportExport(Request $request){
        // dd($request->all());
        $orders = $this->order_master->where('status','OD');
        $p = $orders = $orders->with([
            //'customerDetails',
            'orderMasterDetails.driverDetails',
            //'countryDetails',
            'orderMasterDetails',
            //'orderMasterDetails.productDetails',
            //'orderMasterDetails.productVariantDetails',
            //'orderMasterDetails.sellerDetails',
            //'countryDetails',
            //'shippingAddress',
            //'billingAddress',
            //'getBillingCountry',
            //'productVariantDetails',
        ])
        ->orderBy('id','desc');
        //dd($orders->get());
        
        if(@$request->all()){
            if(@$request->keyword){
                $orders = $orders->Where('order_no','like','%'.$request->keyword.'%');
                
            }
               
            if(@$request->o_t){
                $orders = $orders->Where('order_type',$request->o_t);
            }
            if(@$request->payment_method){
                $orders = $orders->Where('payment_method',$request->payment_method);
            }
            if(@$request->type){
                $orders = $orders->Where('status',$request->type);
            }
            if(@$request->merchant){
                $sellerId = $request->merchant;
                $orders = $orders->with(['orderSellerInfo' => function($q) use($sellerId) {
                    $q->where('seller_id', $sellerId);
                }]);
                $orders = $orders->whereHas('orderMasterDetails', function($q) use ($sellerId){
                    $q->where('seller_id',$sellerId);
                });
            }
            if(@$request->from_date && @$request->to_date){
                $orders = $orders->whereBetween('created_at',[$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
            }
        }
        //dd($orders->get());
        $orders = $orders->get();
        
        
        $orders_array[] = array('Order No', 'Date', 'Customer', 'Type', 'Payment', 'Status', 'Commission Amount', 'Shipping Cost', 'Order Total');
        foreach($orders as $orders)
        {
            if($orders->order_type == 'I') {
                $orderType = __('admin_lang.internal_1');
            } else {
                $orderType = __('admin_lang.external_1');
            }
            if($orders->payment_method == 'C') {
                $paymentMethod = __('admin_lang.cod');
            } else {
                $paymentMethod = __('admin_lang.online');
            }
            if($orders->status == 'I') {
                $status = 'Incomplete';
            } elseif($orders->status == 'N') {
                $status = __('admin_lang.new');
            } elseif($orders->status == 'OA') {
                $status = __('admin_lang.order_accepted');
            } elseif($orders->status == 'DA') {
                $status = __('admin_lang.driver_assigned');
            } elseif($orders->status == 'RP') {
                $status = __('admin_lang.ready_pick_up');                
            } elseif($orders->status == 'OP') {
                $status = __('admin_lang.ord_picked_up');
            } elseif($orders->status == 'OD') {
                $status = __('admin_lang.order_delivered');
            } elseif($orders->status == 'OC') {
                $status = __('admin_lang.order_cancel');                
            }
            if(@$request->merchant){
                if($orders->orderSellerInfo){
                    $commission = $orders->orderSellerInfo->total_commission;
                    $order_total = $orders->orderSellerInfo->subtotal - $orders->orderSellerInfo->total_discount;
                }
            }else{
                $commission = $orders->total_commission;
                $order_total = $orders->order_total;
            }
            $orders_array[] = array(
                'Order No'         => $orders->order_no,
                'Date'             => $orders->created_at,
                'Customer'         => $orders->shipping_fname.' '.$orders->shipping_lname,
                'Type'             => $orderType,
                'Payment'          => $paymentMethod,
                'Status'           => @$status,
                'Commission Amount'  => number_format((double) $commission,3),
                'Shipping Cost'      => number_format((double) $orders->shipping_price,3),
                'Order Total'        => number_format((double) $order_total,3)
            );
        }
        //dd($orders_array);
        Excel::create('Earning Report', function($excel) use ($orders_array){
          $excel->setTitle('Earning Report');
          $excel->sheet('Earning Report', function($sheet) use ($orders_array){
           $sheet->fromArray($orders_array, null, 'A1', false, false);
        });
      })->download('xlsx');
    }


    /*
    * Method        : showOrderReport
    * Description   : This method is used to show order report.
    * Author        : Surajit
    */
    public function showOrderReport(Request $request){
        $customers = $this->order_master->where('user_id','!=',0)->get();
        $external_customer = $this->order_master->where('user_id',0)->get();
        $driver = $this->drivers->where('status','!=','D')->get();
        $merchants = $this->merchants->where('status','!=','D')->get();
        $orders = $this->order_master->whereNotIn('status', ['I','D','F']);


        if(@$request->all() && @$request->merchant_id == ''){
            if(@$request->id){
                $orders = $orders->where('id',$request->id);
            }
        }else{
            $orders = $orders->with(['customerDetails','orderMasterDetails.driverDetails','countryDetails','orderMasterDetails','orderMasterDetails.productDetails','orderMasterDetails.productVariantDetails','orderMasterDetails.sellerDetails','countryDetails','shippingAddress','billingAddress','getBillingCountry','productVariantDetails'])->orderBy('id','desc');
        }

        if(@$request->merchant_id){
            $orders = $orders->Where('user_id',$request->merchant_id);
        }
        $orders = $orders->orderBy('created_at','desc')->get();
        $order_detail = $this->order_details->get();
        $order_seller = $this->order_sellers->get();
        $key = $request->all();

        return view('admin.modules.reports.order_report')->with([
          'customers'       =>  @$customers,
          'drivers'     =>  @$driver,
          'orders'      =>  @$orders,
          'merchants'       =>  @$merchants,
          'order_detail'    =>  @$order_detail,
          'external_customer'   =>  @$external_customer,
          'order_seller'    =>  @$order_seller,
          'key'             =>  @$key
      ]);
    }


    /*
    * Method        : showOrderReportExport
    * Description   : This method is used to export order report.
    * Author        : Surajit
    */
    public function showOrderReportExport(Request $request){
        // dd($request->all());
        $orders = $this->order_master->with(['customerDetails','orderMasterDetails.driverDetails','countryDetails.countryDetailsBylanguage','orderMasterDetails','orderMasterDetails.productDetails','orderMasterDetails.productVariantDetails','orderMasterDetails.sellerDetails','countryDetails.countryDetailsBylanguage','shippingAddress','billingAddress','getBillingCountry','productVariantDetails','getCityNameByLanguage'])->whereNotIn('status', ['D','']);
        if(@$request->all()){
            if(@$request->keyword){
                // $orders = $orders->Where('order_no','like','%'.$request->keyword.'%');
                $orders = $orders->where(function($q1) use($request) {
                    $q1 = $q1->where('order_no','like','%'.$request->keyword.'%')
                    ->orWhere('shipping_fname','like','%'.$request->keyword.'%')
                    ->orWhere('shipping_lname','like','%'.$request->keyword.'%')
                    ->orWhere('shipping_phone','like','%'.$request->keyword.'%')
                    ->orWhere('shipping_email','like','%'.$request->keyword.'%')
                    ->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".$request->keyword."%");
                });
            }
            if(@$request->shipping_city_nm){
                // $orders = $orders->Where('order_no','like','%'.$request->keyword.'%');
                $orders = $orders->where(function($q2) use($request) {
                    $q2 = $q2->where('shipping_city','like','%'.$request->shipping_city_nm.'%')
                    ->orWhere('invoice_no','like','%'.$request->shipping_city_nm.'%')
                    ->orWhere('invoice_details','like','%'.$request->shipping_city_nm.'%')
                    ->orWhere('notes','like','%'.$request->shipping_city_nm.'%')
                    ->orWhere('driver_notes','like','%'.$request->shipping_city_nm.'%');
                });
            }   
            if(@$request->shipping_city_search){
                $orders = $orders->where(function($q2) use($request) {
                    $q2 = $q2->where('shipping_country','!=',134);
                });
                $orders = $orders->where(function($q2) use($request) {
                    $q2 = $q2->where('shipping_city','like','%'.$request->shipping_city_search.'%');
                });
            }
            if(@$request->driver_search){
                $orders = $orders->whereHas('orderMasterDetails', function($q4) use ($request){
                   $q4->where('driver_id',$request->driver_search);
               });
            }
            if(@$request->customer_search){
                $orders = $orders->where( function($q4) use ($request){
                   $q4->where('user_id',$request->customer_search);
               });
            }
            if(@$request->o_t){
                $orders = $orders->Where('order_type',$request->o_t);
            }
            if(@$request->payment_method){
                $orders = $orders->Where('payment_method',$request->payment_method);
            }
            if(@$request->type){
                $orders = $orders->Where('status',$request->type);
            }
            if(@$request->merchant){
                $orders = $orders->whereHas('orderMasterDetails', function($q) use ($request){
                 $q->where('seller_id',$request->merchant);
             });
            }
            if(@$request->from_date && @$request->to_date){
                $orders = $orders->whereBetween('created_at',[$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
            }
            
            if(@$request->coupon_code){
                $orders = $orders->where('coupon_code', @$request->coupon_code);
            }
        }
        $orders = $orders->orderBy('created_at','desc')->get();
        $orders_array[] = array('Order No', 'Date', 'Customer','Shipping Phone', 'Shipping city','Order Total', 'Type', 'Payment','Invoice No','Invoice details','Admin notes','Driver notes','Drivers','Pickup Date','Pickup Time', 'Status');
        // setSize('A1',18,25);
        foreach($orders as $order)
        {
            if($order->order_type == 'I') {
                $orderType = __('admin_lang.internal_1');
            } else {
                $orderType = __('admin_lang.external_1');
            }
            if($order->payment_method == 'C') {
                $paymentMethod = __('admin_lang.cod');
            } else {
                $paymentMethod = __('admin_lang.online');                
            }
            if($order->status == 'I') {
                $status = 'Incomplete';
            } elseif($order->status == 'N') {
                $status = __('admin_lang.new');
            } elseif($order->status == 'OA') {
                $status = __('admin_lang.order_accepted');
            } elseif($order->status == 'DA') {
                $status = __('admin_lang.driver_assigned');                
            } elseif($order->status == 'RP') {
                $status = __('admin_lang.ready_pick_ups');                
            } elseif($order->status == 'OP') {
                $status = __('admin_lang.ord_picked_up');
            } elseif($order->status == 'OD') {
                $status = __('admin_lang.ord_dlv');                
            } elseif($order->status == 'OC') {
                $status = __('admin_lang.order_cancel');
            }elseif($order->status == 'F') {
                $status = 'Payment Failed';
            }
            $shippingCityName = " ";
            if(@$order->shipping_country == 134){
                if(@$order->getCityNameByLanguage->name){
                    $shippingCityName = $order->getCityNameByLanguage->name;    
                }else{
                    $shippingCityName = "";    
                }
                
            }else{
                if(@$order->shipping_city){
                    $shippingCityName = $order->shipping_city;
                }else{
                    $shippingCityName = "";    
                }
                
            }
            $driverNames = "";
            if(@$order->orderMasterDetails){
                foreach(@$order->orderMasterDetails as $key=>$dtls){
                    if($dtls->driver_id != 0){
                        $driverNames =$driverNames." ".($key+1)." . ".@$dtls->driverDetails->fname." ".@$dtls->driverDetails->lname;    
                    }
                    // else{
                        // $driverNames = $driverNames."";
                    // }
                }
            }
            if(@$order->invoice_no){
                $invoice_no = $order->invoice_no;    
            }else{
                $invoice_no = " ";    
            }
            if(@$order->invoice_details){
                $invoice_details = $order->invoice_details;    
            }else{
                $invoice_details = " ";    
            }
            if(@$order->notes){
                $notes = $order->notes;    
            }else{
                $notes = " ";    
            }
            if(@$order->driver_notes){
                $driver_notes = $order->driver_notes;    
            }else{
                $driver_notes = " ";    
            }
            if(@$order->delivery_date){
                $delivery_date = $order->delivery_date;    
            }else{
                $delivery_date = " ";    
            }
            if(@$order->delivery_time){
                $delivery_time = $order->delivery_time;    
            }else{
                $delivery_time = " ";    
            }
            
            $orders_array[] = array(
                'Order No'              => @$order->order_no,
                'Date'                  => @$order->created_at,
                'Customer'              => @$order->shipping_fname.' '.@$order->shipping_lname,
                'Shipping Phone'        => @$order->shipping_phone,
                'Shipping city'         => @$shippingCityName,
                'Order Total'           => (double) $order->order_total,
                'Type'                  => @$orderType,
                'Payment'               => @$paymentMethod,
                'Invoice No'            => @$invoice_no,
                'Invoice details'       => @$invoice_details,
                'Admin notes'           => @$notes,
                'Driver notes'          => @$driver_notes,
                'Drivers'               => @$driverNames,
                'Pickup date'           => @$delivery_date,
                'Pickup time'           => @$delivery_time,
                'Status'                => @$status
            );
        }
        // dd($orders_array);
        Excel::create('Order Report', function($excel) use ($orders_array){
            $excel->setTitle('Order Report');
            $excel->sheet('Order Report', function($sheet) use ($orders_array){
            $sheet->fromArray($orders_array, null, 'A1', false, false);
            for( $intRowNumber = 1; $intRowNumber <= count($orders_array) + 1; $intRowNumber++){
                $sheet->setSize('A' . $intRowNumber, 25, 14);
                $sheet->setSize('B' . $intRowNumber, 25, 14);
                $sheet->setSize('C' . $intRowNumber, 25, 14);
                $sheet->setSize('D' . $intRowNumber, 25, 14);
                $sheet->setSize('E' . $intRowNumber, 25, 14);
                $sheet->setSize('F' . $intRowNumber, 25, 14);
                $sheet->setSize('G' . $intRowNumber, 25, 14);
                $sheet->setSize('H' . $intRowNumber, 25, 14);
                $sheet->setSize('I' . $intRowNumber, 25, 14);
                $sheet->setSize('J' . $intRowNumber, 25, 14);
                $sheet->setSize('K' . $intRowNumber, 25, 14);
                $sheet->setSize('L' . $intRowNumber, 25, 14);
                $sheet->setSize('M' . $intRowNumber, 25, 14);
                $sheet->setSize('N' . $intRowNumber, 25, 14);
                $sheet->setSize('O' . $intRowNumber, 25, 14);
                $sheet->setSize('P' . $intRowNumber, 25, 14);
            }
            $sheet->freezeFirstRow();
            $sheet->cell('A1:O1', function($cell) {
                $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '11',
                    'bold'       =>  true
                ));
            });
       });
      })->download('xlsx');
    }

    /*
    * Method        : showUserReport
    * Description   : This method is used to show user report.
    * Author        : Surajit
    */
    public function showUserReport(Request $request){
        $customers = $this->customers->where('status','!=','D');
        if(@$request->all()){
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

            $cc = $customers;
            $cc = $cc->count(); // total customer before filtering

            if(@$request->columns['1']['search']['value']){
                $customers = $customers->where(function($query) use($request){
                    $query->Where('fname','like','%'.$request->columns['1']['search']['value'].'%')
                    ->orWhere('lname','like','%'.$request->columns['1']['search']['value'].'%')
                    ->orWhere('phone','like','%'.$request->columns['1']['search']['value'].'%')
                    ->orWhere('email','like','%'.$request->columns['1']['search']['value'].'%')
                    ->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".$request->columns['1']['search']['value']."%");
                });
                
            }
            if(@$request->columns['2']['search']['value']){
                $customers = $customers->where(function($query) use($request){
                    $query->Where('status',$request->columns['2']['search']['value']);
                });
            }
            if(@$request->columns['3']['search']['value']){
                $customers = $customers->where(function($query) use($request){
                    $query->Where('signup_from', $request->columns['3']['search']['value']);
                });
            }
            $ccc = $customers;
            $ccc = $ccc->count(); // total customers before paginate

            // sorting
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $customers = $customers->orderBy('fname','asc');
                } else {
                    $customers = $customers->orderBy('fname','desc');
                }
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $customers = $customers->orderBy('email','asc');
                } else {
                    $customers = $customers->orderBy('email','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $customers = $customers->orderBy('phone','asc');
                } else {
                    $customers = $customers->orderBy('phone','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $customers = $customers->orderBy('signup_from','asc');
                } else {
                    $customers = $customers->orderBy('signup_from','desc');
                }
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $customers = $customers->orderBy('status','asc');
                } else {
                    $customers = $customers->orderBy('status','desc');
                }
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $customers = $customers->orderBy('created_at','asc');
                } else {
                    $customers = $customers->orderBy('created_at','desc');
                }
            }

            $customers = $customers->skip($request->start)->take($request->length)->get();

            $data['aaData'] = $customers;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $cc;
            $data["iTotalDisplayRecords"] = $ccc;
            return response()->json($data);
        }else{
            $customers = $customers->orderBy('id','desc')->get();
            return view('admin.modules.reports.user_report')->with([
                'customers' =>  @$customers
            ]);
        }
    }


    /*
    * Method        : showUserReportExport
    * Description   : This method is used to export user report.
    * Author        : Surajit
    */
    public function showUserReportExport(Request $request){
        // dd($request->all());
        $customers = $this->customers->where('status','!=','D');
        if(@$request->all()){
            if(@$request->keyword){
                $customers = $customers->where(function($query) use($request){
                    $query->Where('fname','like','%'.$request->keyword.'%')
                    ->orWhere('lname','like','%'.$request->keyword.'%')
                    ->orWhere('phone','like','%'.$request->keyword.'%')
                    ->orWhere('email','like','%'.$request->keyword.'%')
                    ->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".$request->keyword."%");
                });                
            }
            if(@$request->status){
                $customers = $customers->where(function($query) use($request){
                    $query->Where('status',$request->status);
                });

            }
            if(@$request->signup_from){
                $customers = $customers->where(function($query) use($request){
                    $query->Where('signup_from', $request->signup_from);
                });
            }
            if(@$request->from_date && @$request->to_date){
                $customers = $customers->whereBetween('created_at',[$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
            }
        }
        $customers = $customers->orderBy('id','desc')->get();
        $customers_array[] = array('Name', 'Email', 'Phone Number', 'Signup From', 'Status', 'Date');
        foreach($customers as $customers)
        {
            if($customers->signup_from == 'S') {
                $signupFrom = 'Website';
            } elseif($customers->signup_from == 'F') {
                $signupFrom = 'Facebook';                
            } elseif($customers->signup_from == 'G') {
                $signupFrom = 'Google';                
            }

            if($customers->status == 'A') {
                $status = 'Active';
            } elseif($customers->status == 'I') {
                $status = 'Inactive';                
            } elseif($customers->status == 'U') {
                $status = 'Unverified';                
            }

            $customers_array[] = array(
                'Name'             => $customers->fname.' '.$customers->lname,
                'Email'            => $customers->email,
                'Phone Number'     => $customers->phone,
                'Signup From'      => $signupFrom,
                'Status'           => $status,
                'Date'             => $customers->created_at
            );
        }
        // dd($customers_array);
        Excel::create('User Report', function($excel) use ($customers_array){
          $excel->setTitle('User Report');
          $excel->sheet('User Report', function($sheet) use ($customers_array){
           $sheet->fromArray($customers_array, null, 'A1', false, false);
       });
      })->download('xlsx');
    }

    /*
    * Method        : showMerchantReport
    * Description   : This method is used to show merchant report.
    * Author        : Surajit
    */
    public function showMerchantReport(Request $request){
        $merchants = $this->merchants->where('status','!=','D');
        $mm = $merchants;
        $mm = $mm->count(); // total merchant before filter

        if(@$request->all()) {
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

            if(@$request->columns['1']['search']['value']){
                // dd(@$request->keyword);
                $merchants = $merchants->where(function($query) use($request){
                    $query->Where('fname','like','%'.@$request->columns['1']['search']['value'].'%')
                    ->orWhere('lname','like','%'.@$request->columns['1']['search']['value'].'%')
                    ->orWhere('email','like','%'.@$request->columns['1']['search']['value'].'%')
                    ->orWhere('phone','like','%'.@$request->columns['1']['search']['value'].'%')
                    ->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".@$request->columns['1']['search']['value']."%");
                });
            }

            if(@$request->columns['2']['search']['value']){
                $merchants = $merchants->where(function($query) use($request){
                    $query->Where('status', $request->columns['2']['search']['value']);
                });
            }
            $mmm = $merchants;
            $mmm = $mmm->count(); // count before paginate

            // sorting
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $merchants = $merchants->orderBy('fname','asc');
                } else {
                    $merchants = $merchants->orderBy('fname','desc');
                }
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $merchants = $merchants->orderBy('email','asc');
                } else {
                    $merchants = $merchants->orderBy('email','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $merchants = $merchants->orderBy('phone','asc');
                } else {
                    $merchants = $merchants->orderBy('phone','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $merchants = $merchants->orderBy('inside_shipping_cost','asc');
                } else {
                    $merchants = $merchants->orderBy('inside_shipping_cost','desc');
                }
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $merchants = $merchants->orderBy('shipping_cost','asc');
                } else {
                    $merchants = $merchants->orderBy('shipping_cost','desc');
                }
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $merchants = $merchants->orderBy('premium_merchant','asc');
                } else {
                    $merchants = $merchants->orderBy('premium_merchant','desc');
                }
            }
            if($columnIndex == 6) {
                if($columnSortOrder == 'asc') {
                    $merchants = $merchants->orderBy('international_order','asc');
                } else {
                    $merchants = $merchants->orderBy('international_order','desc');
                }
            }
            if($columnIndex == 7) {
                if($columnSortOrder == 'asc') {
                    $merchants = $merchants->orderBy('status','asc');
                } else {
                    $merchants = $merchants->orderBy('status','desc');
                }
            }
            if($columnIndex == 8) {
                if($columnSortOrder == 'asc') {
                    $merchants = $merchants->orderBy('created_at','asc');
                } else {
                    $merchants = $merchants->orderBy('created_at','desc');
                }
            }
            $merchants = $merchants->skip($request->start)->take($request->length)->get();

            $data['aaData'] = $merchants;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $mm;
            $data["iTotalDisplayRecords"] = @$mmm;
            return response()->json($data);
        }else{

            $merchants =$merchants->orderBy('id','desc')->get();

            return view('admin.modules.reports.merchant_report')->with([
                'merchants'=>@$merchants
            ]);
        }
    }


    /*
    * Method        : showMerchantReportExport
    * Description   : This method is used to export merchant report.
    * Author        : Surajit
    */
    public function showMerchantReportExport(Request $request){
        $merchants = $this->merchants->where('status','!=','D');
        if(@$request->all()){
            if(@$request->keyword){
                $merchants = $merchants->where(function($query) use($request){
                    $query->Where('fname','like','%'.@$request->keyword.'%')
                    ->orWhere('lname','like','%'.@$request->keyword.'%')
                    ->orWhere('email','like','%'.@$request->keyword.'%')
                    ->orWhere('phone','like','%'.@$request->keyword.'%')
                    ->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".@$request->keyword."%");
                });
            }

            if(@$request->status){
                $merchants = $merchants->where(function($query) use($request){
                    $query->Where('status', $request->status);
                });
            }

            if(@$request->from_date && @$request->to_date){
                $merchants = $merchants->whereBetween('created_at',[$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
            }
        }
        $merchants =$merchants->orderBy('id','desc')->get();
        $merchants_array[] = array('Name', 'Email', 'Phone Number', 'Internal Shipping Cost', 'External Shipping Cost', 'Premium Merchant', 'International Order', 'Total Earning', 'Total Commission', 'Total Due', 'Status', 'Date');
        foreach($merchants as $merchant)
        {
            if($merchant->premium_merchant == 'Y') {
                $premiumMerchant = __('admin_lang.yes');
            } elseif($merchant->premium_merchant == 'N') {
                $premiumMerchant = __('admin_lang.no');                
            }
            if($merchant->international_order == 'ON') {
                $internationalOrder = __('admin_lang.on');
            } elseif($merchant->international_order == 'OFF') {
                $internationalOrder = __('admin_lang.off');                
            }
            $status = '';
            if($merchant->status == 'A') {
                $status = __('admin_lang.Active');
            } elseif($merchant->status == 'I') {
                $status = __('admin_lang.Inactive');                
            } elseif($merchant->status == 'AA') {
                $status = __('admin_lang.awaiting_approval');                
            } elseif($merchant->status == 'U') {
                $status = __('admin_lang.un_verified');                
            }

            $merchants_array[] = array(
                'Name'                       => $merchant->fname.' '.$merchant->lname,
                'Email'                      => $merchant->email,
                'Phone Number'               => $merchant->phone,
                'Internal Shipping Cost'     => (double) $merchant->inside_shipping_cost,
                'External Shipping Cost'     => (double) $merchant->shipping_cost,
                'Premium Merchant'           => $premiumMerchant,
                'International Order'        => $internationalOrder,
                'Total Earning'     => (double) $merchant->total_earning,
                'Total Commission'     => (double) $merchant->total_commission,
                'Total Due'     => (double) $merchant->total_due,
                'Status'                     => $status,
                'Date'                       => $merchant->created_at
            );
        }
        // dd($merchants_array);
        Excel::create('Merchant Report', function($excel) use ($merchants_array){
            $excel->setTitle('Merchant Report');
            $excel->sheet('Merchant Report', function($sheet) use ($merchants_array){
                $sheet->fromArray($merchants_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }

    /*
    * Method        : showProductReport
    * Description   : This method is used to show product report.
    * Author        : Surajit
    */
    public function showProductReport(Request $request){

        $columnIndex = $request->order[0]['column']; // Column index
        $columnName = $request->columns[$columnIndex]['data']; // Column name
        $columnSortOrder = $request->order[0]['dir']; // asc or desc

        $p1 = $product = Product::where('status', '!=', 'D')
        ->with('productByLanguage','defaultImage','productMarchant:id,fname,lname','productCategory.Category','productSubCategory.Category.priceVariant','productParentCategory.Category','productSubCategory.Category','productVariants');
        $p1 =  $p1->count();
        if($request->all() && @$request->merchant_id == '')
        {
            if(@$request->columns['4']['search']['value']){
                $keyword = $request->columns['4']['search']['value'];
                $product = $product->where(function($q) use($keyword){
                    $q->where('product_code', 'LIKE', '%'.$keyword.'%');

                    $q->orWhereHas('productByLanguage',function($query) use($keyword) {
                        $query->where('title','like','%'.$keyword.'%');
                    });
                });
            }
            if(@$request->columns['1']['search']['value']){
                $product = $product->whereHas('productCategory',function($query) use($request) {
                    $query->Where('category_id',$request->columns['1']['search']['value']);
                });
            }
            if(@$request->columns['2']['search']['value']){
                if(@$request->columns['1']['search']['value']){
                    $catDetails = Category::where(['id' => $request->columns['2']['search']['value'], 'parent_id' => $request->columns['1']['search']['value']])->first();
                    if($catDetails) {
                        $product = $product->whereHas('productCategory',function($query) use($request) {
                            $query->Where('category_id',$request->columns['2']['search']['value']);
                        });
                    }
                } else {
                    $product = $product->whereHas('productCategory',function($query) use($request) {
                        $query->Where('category_id',$request->columns['2']['search']['value']);
                    });
                }
            }
            if(@$request->columns['3']['search']['value']){
                $product = $product->Where('user_id',$request->columns['3']['search']['value']);
            }
            if(@$request->columns['7']['search']['value']){
                $product = $product->Where('status',$request->columns['7']['search']['value']);
            }
            if(@$request->search['value']){
                $price = explode(',', $request->search['value']);
                $price_min = $price[0];
                $price_max = $price[1];
                $product = $product->whereBetween('price', [$price_min, $price_max]);
            }
            if(@$request->columns['8']['search']['value']){
                $date = explode(',', $request->columns['8']['search']['value']);
                $from_date = $date[0];
                $to_date = $date[1];
                $product = $product->whereBetween('created_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);
            }
            $pp = $product;
            $pp = $pp->count();
            $limit = $request->length;
            
            if($columnIndex <= 6 || $columnIndex == 9) {
                if($columnIndex == 0) {
                    $product = $product->get()->toArray();
                    uasort($product, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['product_category'][0]['category']['category_by_language']['title'];
                        $c2 = @$b['product_category'][0]['category']['category_by_language']['title'];
                        $res = strcmp($c1, $c2);
                        if($res == 0) {
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 1) {
                    $product = $product->get()->toArray();
                    uasort($product, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['product_category'][1]['category']['category_by_language']['title'];
                        $c2 = @$b['product_category'][1]['category']['category_by_language']['title'];
                        $res = strcmp($c1, $c2);
                        if($res == 0) {
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 2) {
                    $product = $product->get()->toArray();
                    uasort($product, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['product_marchant']['fname'].' '.@$a['product_marchant']['lname'];
                        $c2 = @$b['product_marchant']['fname'].' '.@$b['product_marchant']['lname'];
                        $res = strcmp($c1, $c2);
                        if($res == 0) {
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 3) {
                    $product = $product->get()->toArray();
                    uasort($product, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['product_by_language']['title'];
                        $c2 = @$b['product_by_language']['title'];
                        $res = strcmp($c1, $c2);
                        if($res == 0) {
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }
                # stock ordering properly not working.
                if($columnIndex == 4) {
                    $product = $product->get()->toArray();
                    uasort($product, function($a, $b) use ($columnSortOrder){
                        if(@$a['product_variants']) {
                            $c1 = @$a['product_variants']['stock_quantity'];
                            $c2 = @$b['product_variants']['stock_quantity'];
                            $res = strcmp($c1, $c2);
                            if($res == 0) {
                                return 0;
                            } elseif($res < 0) {
                                return $columnSortOrder == 'asc' ? -1 : 1;
                            } else {
                                return $columnSortOrder == 'asc' ? 1 : -1;
                            }
                        } else {
                            $c1 = @$a['stock'];
                            $c2 = @$b['stock'];
                            $res = strcmp($c1, $c2);
                            if($res == 0) {
                                return 0;
                            } elseif($res < 0) {
                                return $columnSortOrder == 'asc' ? -1 : 1;
                            } else {
                                return $columnSortOrder == 'asc' ? 1 : -1;
                            }
                        }
                    });
                }

                if($columnIndex == 5) {
                    $product = $product->get()->toArray();
                    uasort($product, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['price'];
                        $c2 = @$b['price'];
                        $res = strcmp($c1, $c2);
                        if($res == 0) {
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 6) {
                    $product = $product->get()->toArray();
                    uasort($product, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['is_featured'];
                        $c2 = @$b['is_featured'];
                        $res = strcmp($c1, $c2);
                        if($res == 0) {
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 7) {
                    $product = $product->get()->toArray();
                    uasort($product, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['status'];
                        $c2 = @$b['status'];
                        $res = strcmp($c1, $c2);
                        if($res == 0) {
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 8) {
                    $product = $product->get()->toArray();
                    uasort($product, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['product_marchant']['status'];
                        $c2 = @$b['product_marchant']['status'];
                        $res = strcmp($c1, $c2);
                        if($res == 0) {
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 9) {
                    $product = $product->get()->toArray();
                    uasort($product, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['created_at'];
                        $c2 = @$b['created_at'];
                        $res = strcmp($c1, $c2);
                        if($res == 0) {
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }
                $product = array_slice($product, $request->start, $limit);
                $p = array();
                foreach ($product as $value) {
                    array_push($p, $value);
                }
                $product = $p;
            } else {
                $product = $product->orderBy($columnName, $columnSortOrder)->skip($request->start)->take($limit)->get()->toArray();
            }
            $data['aaData'] = $product;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $p1;
            $data["iTotalDisplayRecords"] = $pp;
            return response()->json($data);
        }

        $data['subCategory']    = Category::select([
            'id',
            'parent_id',
            'status'
        ])
        ->where('status', 'A')
        ->where('parent_id', '!=', '0')
        ->with('categoryByLanguage:id,category_id,language_id,title')
        ->get();
        
        $data['highestPrice'] = ProductVariant::orderBy('price','DESC')->first();

        $data['user']           = Merchant::select([
            'id',
            'fname',
            'lname'
        ])
        ->where('status','A')
        ->get();
        $data['category']       = Category::select([
            'id',
            'parent_id',
            'status'
        ])
        ->where([
            'parent_id' =>  '0',
            'status'    =>  'A'
        ])
        ->with('categoryByLanguage:id,category_id,language_id,title')
        ->get();

        $product = $product->orderBy('id','DESC')->get();
        $data['key'] = $request->all();
        $data['product'] = $product;
        return view("admin.modules.reports.product_report",$data);
    }


    /*
    * Method        : showProductReportExport
    * Description   : This method is used to export products report.
    * Author        : Surajit
    */
    public function showProductReportExport(Request $request){
        // dd($request->all());
        $product = Product::where('status', '!=', 'D')
        ->with('productByLanguage','defaultImage','productMarchant:id,fname,lname','productCategory.Category','productSubCategory.Category.priceVariant');
        if(@$request->all()){
            if(@$request->keyword){
                $product = $product->whereHas('productByLanguage',function($query) use($request) {
                    $query->Where('title','like','%'.$request->keyword.'%');
                });
            }
            if(@$request->category){
                $product = $product->whereHas('productCategory',function($query) use($request) {
                    $query->Where('category_id',$request->category);
                });
            }
            if(@$request->sub_category){
                $product = $product->whereHas('productCategory',function($query) use($request) {
                    $query->Where('category_id',$request->sub_category);
                });
            }
            if(@$request->user_id){
                $product = $product->Where('user_id',$request->user_id);
            }
            if(@$request->status){
                $product = $product->Where('status',$request->status);
            }
            if(@$request->price){
                $price = explode(',', $request->price);
                $price_min = $price[0];
                $price_max = $price[1];
                $product = $product->whereBetween('price', [$price_min, $price_max]);
            }

            if(@$request->from_date && @$request->to_date){
                $product = $product->whereBetween('created_at',[$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
            }
        }
        $product = $product->orderBy('id','DESC')->get();
        $products_array[] = array('Category', 'Subcategory', 'Merchant', 'Title', 'Stock', 'Price', 'Is Featured', 'Status', 'Merchant Status', 'Date');
        foreach($product as $product)
        {
            if($product->is_featured == 'Y') {
                $isFeatured = __('admin_lang.yes');
            } elseif($product->is_featured == 'N') {
                $isFeatured = __('admin_lang.no');                
            }
            if($product->international_order == 'ON') {
                $internationalOrder = __('admin_lang.on');
            } elseif($product->international_order == 'OFF') {
                $internationalOrder = __('admin_lamng.off');
            }

            if($product->status == 'A') {
                $status = __('admin_lang.Active');
            } elseif($product->status == 'I') {
                $status = __('admin_lang.block');
            } elseif($product->status == 'W') {
                $status = __('admin_lang.awaiting_approval');
            }

            if($product->seller_status == 'A') {
                $sellerStatus = __('admin_lang.Active');
            } elseif($product->seller_status == 'I') {
                $sellerStatus = __('admin_lang.block');
            } elseif($product->seller_status == 'W') {
                $sellerStatus = __('admin_lang.awaiting_approval');
            }

            // dd($product->productVariants);
            $totStock = 0;
            if($product->productVariants->isNotEmpty()){
                for($i=0; $i<count($product->productVariants); $i++){
                    $totStock = $totStock + $product->productVariants[$i]->stock_quantity;    
                }
            }else{
                $totStock = $product->stock;
            }
            $products_array[] = array(
                'Category'              => $product->productCategory[0]->Category->categoryByLanguage->title,
                'Subcategory'           => $product->productCategory[1]->Category->categoryByLanguage->title,
                'Merchant'              => $product->productMarchant->fname.' '.$product->productMarchant->lname,
                'Title'                 => $product->productByLanguage->title,
                'Stock'                 => $totStock,
                'Price'                 => (double) $product->price,
                'Is Featured'           => $isFeatured,
                'Status'                => $status,
                'Merchant Status'       => $sellerStatus,
                'Date'                  => $product->created_at
            );
        }
        // dd($products_array);
        Excel::create('Product Report - '. date('Y-m-d'), function($excel) use ($products_array){
            $excel->setTitle('Product Report - '. date('Y-m-d'));
            $excel->sheet('Product Report - '. date('Y-m-d'), function($sheet) use ($products_array){
                $sheet->fromArray($products_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }

    /*
    * Method        : showDriverReport
    * Description   : This method is used to show driver report.
    * Author        : Surajit
    */
    public function showDriverReport(Request $request){
        $driver = $this->drivers->where('status', 'A')->get();
        $merchants = $this->merchants->where('status','!=','D')->get();
        $orders = $this->order_details->where('driver_id','!=','0')->with([
            'orderMaster.getCountry',
            'orderMaster.getCoupon',
            'productByLanguage:product_id,title',
            'driverDetails:id,fname,lname',
            'sellerDetails:id,fname,lname'
        ]);

        $orders = $orders->whereHas('orderMaster',function($query) {
            $query->whereNotIn('status', ['','D','I','OC']);
        })->orderBy('created_at','desc')->get();
        $key = $request->all();
        return view('admin.modules.reports.driver_report')->with([
            'drivers'     =>  @$driver,
            'merchants'   =>  @$merchants,
            'orders'      =>  @$orders,
            'key'         =>  @$key
        ]);
    }


    /*
    * Method        : driverOrderlists
    * Description   : This method is used to view all orders by admin and manage it.
    * Author        : Surajit
    */
    public function driverOrderlists(Request $request){
        $columnIndex = $request->order[0]['column']; // Column index
        $columnName = $request->columns[$columnIndex]['data']; // Column name
        $columnSortOrder = $request->order[0]['dir']; // asc or desc
        //dd($request->columns['7']['search']['value'],$request->columns['4']['search']['value']);
        $orders = $this->order_details->with([
            'orderMaster',
            'orderMaster.getCountry',
            'orderMaster.getCoupon',
            'productByLanguage:product_id,title',
            'driverDetails:id,fname,lname',
            'sellerDetails:id,fname,lname,email,phone',
        ])
        ->where('driver_id','!=','0')
        ->where('status','!=','');
        $pp = $orders;
        $pp = $pp->count();
        $limit = $request->length;
        $p = $orders = $orders->with([
            'orderMaster',
            'orderMaster.getCountry',
            'orderMaster.getCoupon',
            'productByLanguage:product_id,title',
            'driverDetails:id,fname,lname',
            'sellerDetails:id,fname,lname,email,phone'
        ])
        ->groupBy(['order_master_id', 'driver_id']);
        $p =  $p->count();
        $orders = $orders->whereHas('orderMaster',function($query) {
            $query->whereNotIn('status', ['','D','I','OC','F']);
        });
        $ps =  count($orders->get());
        
        if($request->all()){
            if(@$request->columns['2']['search']['value']){
                $orders = $orders->whereHas('driverDetails',function($query) use($request) {
                    $query->where('id',$request->columns['2']['search']['value']);
                });
            }
            if(@$request->columns['3']['search']['value']){
                $orders = $orders->where(function($query3) use($request) {
                    $query3->where('seller_id',$request->columns['3']['search']['value']);
                });
            }
            if(@$request->columns['8']['search']['value']){
                $orders = $orders->whereHas('orderMaster',function($query8) use($request) {
                    $query8->where('payment_method',$request->columns['8']['search']['value']);
                });
            }

            $roles = Auth()->guard('admin')->user()->roles->pluck('menu_id')->toArray();
            if(@$request->columns['7']['search']['value']){
                if(Auth()->guard('admin')->user()->type == 'A') {
                    $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                        $query7->where('order_type',$request->columns['7']['search']['value']);
                    });

                    if(@$request->columns['4']['search']['value']){
                        if($request->columns['4']['search']['value'] == 'A'){

                            $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                                $query7->where('order_type','E');
                            });

                        }elseif($request->columns['4']['search']['value'] == 'S' || $request->columns['4']['search']['value'] == 'L'){

                            $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                                $query7->where('external_order_type',$request->columns['4']['search']['value']);
                            });
                        }
                    }
                    
                } else {
                    # both order accessible
                    if(in_array(6, $roles) && in_array(13, $roles)) {
                        $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                            $query7->where('order_type',$request->columns['7']['search']['value']);
                        });
    
                        if(@$request->columns['4']['search']['value']){
                            if($request->columns['4']['search']['value'] == 'A'){
    
                                $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                                    $query7->where('order_type','E');
                                });
    
                            }elseif($request->columns['4']['search']['value'] == 'S' || $request->columns['4']['search']['value'] == 'L'){
    
                                $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                                    $query7->where('external_order_type',$request->columns['4']['search']['value']);
                                });
                            }
                        }
                    } else if(in_array(6, $roles)) {

                        $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                            $query7->where('order_type','I');
                        });

                    } else if(in_array(13, $roles)) {
                        $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                            $query7->where('order_type','E');
                        });
    
                        if(@$request->columns['4']['search']['value']){
                            if($request->columns['4']['search']['value'] == 'A'){
    
                                $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                                    $query7->where('order_type','E');
                                });
    
                            }elseif($request->columns['4']['search']['value'] == 'S' || $request->columns['4']['search']['value'] == 'L'){
    
                                $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                                    $query7->where('external_order_type',$request->columns['4']['search']['value']);
                                });
                            }
                        }
                    }
                } 
            }
            if(@$request->columns['9']['search']['value']){
                $orders = $orders->where('status',$request->columns['9']['search']['value']);
            }
            if(@$request->columns['1']['search']['value']){
                $orders = $orders->whereHas('orderMaster',function($query8) use($request) {
                    $date = explode(',', $request->columns['1']['search']['value']);
                    $from_date = $date[0];
                    $to_date = $date[1];
                    $query8->whereBetween('last_status_date',[$from_date." 00:00:00", $to_date." 23:59:59"]);
                });
            }
            $pp = count($orders->get());
            $limit = $request->length;

            //For total subtotal and shipping_cost calculate
            $getOrder = $orders;
            $getOrder = $getOrder->pluck('order_master_id');
            
            $codOrder = $this->order_master->whereIn('id', $getOrder)->where('payment_method', 'C');
            $codOrder = $codOrder->whereHas('orderMasterDetails',function($query) use($request) {
                $query->where('status', 'OD');
            });
            $codOrder2 = $codOrder->get();

            $onlineOrder = $this->order_master->whereIn('id', $getOrder)->where('payment_method', 'O');
            $onlineOrder = $onlineOrder->whereHas('orderMasterDetails',function($query) use($request) {
                $query->where('status', 'OD');
            });
            $onlineOrder2 = $onlineOrder->get();

            $totalCodSubtotal = 0.000;
            $totalOnlineSubtotal = 0.000;
            foreach ($codOrder2 as  $c) {
                $codSubtotal = $c->subtotal;
                $codDiscount = $c->total_discount;
                $codLoyalityAmount = $c->loyalty_amount;
                if($c->coupon_id != null){
                    if($c->coupon_applied_on != null){
                        $applied_on = $c->coupon_applied_on;
                    }else{
                        $applied_on = $c->getCoupon->applied_on;
                    }
                    if($applied_on == 'SUB'){
                        $codSubtotal = $codSubtotal - $c->coupon_discount;
                    }
                }

                $overAllCodsubtotal = ($codSubtotal - $codLoyalityAmount)-$codDiscount;
                $totalCodSubtotal = $totalCodSubtotal + $overAllCodsubtotal;
            }

            foreach ($onlineOrder2 as  $o) {
                $onlineSubtotal = $o->subtotal;
                $onlineDiscount = $o->total_discount;
                $onlineLoyalityAmount = $o->loyalty_amount;
                if($o->coupon_id != null){
                    if($o->coupon_applied_on != null){
                        $applied_on = $o->coupon_applied_on;
                    }else{
                        $applied_on = $o->getCoupon->applied_on;
                    }
                    if($applied_on == 'SUB'){
                        $onlineSubtotal = $onlineSubtotal - $o->coupon_discount;
                    }
                }

                $overAllonlinesubtotal = ($onlineSubtotal - $onlineLoyalityAmount)-$onlineDiscount;
                $totalOnlineSubtotal = $totalOnlineSubtotal + $overAllonlinesubtotal;
            }
            //dd($totalCodSubtotal,$totalOnlineSubtotal);
            // $totalCodSubtotal = $codOrder->sum('subtotal');
            // $totalCodProductDiscount = $codOrder->sum('total_discount');
            // $totalCodSubtotal1 = $codOrder->sum('loyalty_amount');
            
            // $totalCodSubtotal = ($totalCodSubtotal - $totalCodSubtotal1) - $totalCodProductDiscount;

            // $totalOnlineSubtotal = $onlineOrder->sum('subtotal');
            // $totalOnlineProductDiscount = $onlineOrder->sum('total_discount');
            // $totalOnlineSubtotal1 = $onlineOrder->sum('loyalty_amount');

            // $totalOnlineSubtotal = ($totalOnlineSubtotal - $totalOnlineSubtotal1) - $totalOnlineProductDiscount;

            $totalCodShpPrc = $codOrder->sum('shipping_price');
            $totalOnlineShpPrc = $onlineOrder->sum('shipping_price');
            $totalOrderDelivered = $this->order_master->whereIn('id', $getOrder)->where('status', 'OD')->count();

            if($columnIndex < 16){
                if($columnIndex == 0) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['order_master']['order_no'];
                        $c2 = @$b['order_master']['order_no'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 1) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['created_at'];
                        $c2 = @$b['created_at'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 2) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['driver_details']['fname'].' '.@$a['driver_details']['lname'];
                        $c2 = @$b['driver_details']['fname'].' '.@$b['driver_details']['lname'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }
                
                if($columnIndex == 3) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['seller_details']['fname'].' '.@$a['seller_details']['lname'];
                        $c2 = @$b['seller_details']['fname'].' '.@$b['seller_details']['lname'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 4) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['order_master']['shipping_price'];
                        $c2 = @$b['order_master']['shipping_price'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 5) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['order_master']['subtotal'];
                        $c2 = @$b['order_master']['subtotal'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 6) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['order_master']['order_total'];
                        $c2 = @$b['order_master']['order_total'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 7) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['order_master']['order_type'];
                        $c2 = @$b['order_master']['order_type'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 8) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['order_master']['payment_method'];
                        $c2 = @$b['order_master']['payment_method'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 9) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['status'];
                        $c2 = @$b['status'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }

                if($columnIndex == 10) {
                    $orders = $orders->get()->toArray();
                    uasort($orders, function($a, $b) use ($columnSortOrder){
                        $c1 = @$a['updated_at'];
                        $c2 = @$b['updated_at'];
                        $res = strcmp($c1, $c2);
                        if($res == 0){
                            return 0;
                        } elseif($res < 0) {
                            return $columnSortOrder == 'asc' ? -1 : 1;
                        } else {
                            return $columnSortOrder == 'asc' ? 1 : -1;
                        }
                    });
                }
                $orders = array_slice($orders, $request->start, $limit);
                $p = array();
                foreach ($orders as $value) {
                    array_push($p, $value);
                }
                $orders = $p;
            } else {
                $orders = $orders->orderBy($columnName, $columnSortOrder)->skip($request->start)->take($limit)->get()->toArray();
            }
        }

        $data['aaData'] = $orders;
        $data['totalCodSubtotal'] = $totalCodSubtotal;
        $data['totalOnlineSubtotal'] = $totalOnlineSubtotal;
        $data['totalCodShpPrc'] = $totalCodShpPrc;
        $data['totalOnlineShpPrc'] = $totalOnlineShpPrc;
        $data['totalOrderDelivered'] = $totalOrderDelivered;
        $data["draw"] = intval($request->draw);
        $data['iTotalRecords'] = $ps;
        $data["iTotalDisplayRecords"] = $pp;
        return response()->json($data);
    }


    /*
    * Method        : showDriverReportExport
    * Description   : This method is used to export driver report.
    * Author        : Surajit
    */
    public function showDriverReportExport(Request $request){
         //dd($request->all());
        $orders = $this->order_details->where('driver_id','!=','0')->with(['orderMaster.getCountry','productByLanguage:product_id,title','driverDetails:id,fname,lname','sellerDetails:id,fname,lname'])->groupBy(['order_master_id', 'driver_id']);
        $orders = $orders->whereHas('orderMaster',function($query) {
            $query->whereNotIn('status', ['','D','I','OC','F']);
        });
        if(@$request->all()){
            if(@$request->driver){
                $orders = $orders->whereHas('driverDetails',function($query) use($request) {
                    $query->where('id',$request->driver);
                });
            }
            if(@$request->status){
                $orders = $orders->where('status',$request->status);
            }
            if(@$request->seller_nm){
                $orders = $orders->where(function($query) use($request) {
                    $query->where('seller_id',$request->seller_nm);
                });
            }
            if(@$request->pm){
                $orders = $orders->whereHas('orderMaster',function($query) use($request) {
                    $query->where('payment_method',$request->pm);
                });
            }
            if(@$request->from_date && @$request->to_date){
                $orders = $orders->whereHas('orderMaster',function($query8) use($request) {
                    $from_date = $request->from_date;
                    $to_date = $request->to_date;
                    $query8->whereBetween('last_status_date',[$from_date." 00:00:00", $to_date." 23:59:59"]);
                });
            }
            $roles = Auth()->guard('admin')->user()->roles->pluck('menu_id')->toArray();
            if(@$request->o_t){
                if(Auth()->guard('admin')->user()->type == 'A') {
                    $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                        $query7->where('order_type',$request->o_t);
                    });
                    if($request->o_t == 'E'){
                        if(@$request->external_order_type){
                            if($request->external_order_type == 'A'){
    
                                $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                                    $query7->where('order_type','E');
                                });
    
                            }elseif($request->external_order_type == 'S' || $request->external_order_type == 'L'){
    
                                $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                                    $query7->where('external_order_type',$request->external_order_type);
                                });
                            }
                        }
                    }
                    
                    
                } else {
                    # both order accessible
                    if(in_array(6, $roles) && in_array(13, $roles)) {
                        $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                            $query7->where('order_type',$request->$request->o_t);
                        });
                        if($request->o_t == 'E'){
                            if(@$request->external_order_type){
                                if($request->external_order_type == 'A'){
        
                                    $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                                        $query7->where('order_type','E');
                                    });
        
                                }elseif($request->external_order_type == 'S' || $request->external_order_type == 'L'){
        
                                    $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                                        $query7->where('external_order_type',$request->external_order_type);
                                    });
                                }
                            }
                        }
                    } else if(in_array(6, $roles)) {

                        $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                            $query7->where('order_type','I');
                        });

                    } else if(in_array(13, $roles)) {
                        $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                            $query7->where('order_type','E');
                        });
                        if($request->o_t == 'E'){
                            if(@$request->external_order_type){
                                if($request->external_order_type == 'A'){
        
                                    $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                                        $query7->where('order_type','E');
                                    });
        
                                }elseif($request->external_order_type == 'S' || $request->external_order_type == 'L'){
        
                                    $orders = $orders->whereHas('orderMaster',function($query7) use($request) {
                                        $query7->where('external_order_type',$request->external_order_type);
                                    });
                                }
                            }
                        }
                    }
                } 
            }
        }
        $orders = $orders->orderBy('created_at','desc')->get();
        $orders_array[] = array('Order No', 'Date', 'Driver', 'Seller', 'Shipping Cost', 'Subtotal', 'Order Total', 'Order Type', 'Payment Method', 'Status','Last Status Date');
        foreach($orders as $order)
        {
            if($order->orderMaster->order_type == 'I') {
                $orderType = __('admin_lang.internal_1');
            } else {
                $orderType = __('admin_lang.external_1');                
            }
            if($order->orderMaster->payment_method == 'C') {
                $paymentMethod = __('admin_lang.cod');
            } else {
                $paymentMethod = __('admin_lang.online');                
            }
            if($order->status == 'I') {
                $status = __('admin_lang.incomplete');
            } elseif($order->status == 'N') {
                $status = __('admin_lang.new');                
            } elseif($order->status == 'OA') {
                $status = __('admin_lang.order_accepted');                
            } elseif($order->status == 'DA') {
                $status = __('admin_lang.driver_assigned');                
            } elseif($order->status == 'RP') {
                $status = __('admin_lang.ready_pick_up');                
            } elseif($order->status == 'OP') {
                $status = __('admin_lang.ord_picked_up');                
            } elseif($order->status == 'OD') {
                $status = __('admin_lang.ord_dlv');                
            } elseif($order->status == 'OC') {
                $status = __('admin_lang.order_cancel');                
            }

            $orders_array[] = array(
                'Order No'          => @$order->orderMaster->order_no,
                'Date'              => @$order->orderMaster->created_at,
                'Driver'            => @$order->driverDetails->fname.' '.@$order->driverDetails->lname,
                'Seller'            => @$order->sellerDetails->fname.' '.@$order->sellerDetails->lname,
                'Shipping Cost'     => (double) @$order->orderMaster->shipping_price,
                'Subtotal'          => (double) @$order->orderMaster->subtotal,
                'Order Total'       => (double) @$order->orderMaster->order_total,
                'Order Type'        => @$orderType,
                'Payment Method'    => @$paymentMethod,
                'Status'            => @$status,
                'Last Status Date'  => @$order->orderMaster->last_status_date
            );
        }
        // dd($orders_array);
        Excel::create('Driver Report', function($excel) use ($orders_array){
          $excel->setTitle('Driver Report');
          $excel->sheet('Driver Report', function($sheet) use ($orders_array){
           $sheet->fromArray($orders_array, null, 'A1', false, false);
       });
      })->download('xlsx');
    }

    /*
    * Method        : showProductAnalysisReport
    * Description   : This method is used to show product report analysis page
    * Author        : Argha
    * Date          : 2020-10-13
    */
    public function showProductAnalysisReport(Request $request) {
        $merchants = $this->merchants->where('status','!=','D')->get();
        $orders_details = OrderDetail::with([
            'orderMaster',
            'sellerDetails',
            'productDetails.productByLanguage'
        ])
        ->whereHas('orderMaster',function($q){
            $q->where('order_type','I')
            ->where('status','!=','D')
            ->where('status','!=','OC')
            ->where('status','!=','I')
            ->where('status','!=','F');
        })
        ->orderBy('id','desc');
        $dd = $orders_details;
        $dd = $dd->count();
        if($request->all()){
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

            if(@$request->columns['2']['search']['value']){
                $orders_details = $orders_details->where('seller_id', $request->columns['2']['search']['value']);
            }

            if(@$request->columns['1']['search']['value']){
                $date = explode(',', $request->columns['1']['search']['value']);
                $from_date = $date[0];
                $to_date = $date[1];
                $orders_details = $orders_details->whereBetween('created_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);
            }
            
            if(@$request->columns['0']['search']['value']){
                $orders_details = $orders_details->where(function($query) use($request) {
                    $query->whereHas('orderMaster',function($q1) use($request){
                        $q1->where('order_no','LIKE',"%".$request->columns['0']['search']['value']."%");
                    })
                    ->orWhereHas('productDetails.productByLanguage',function($q3) use($request){
                        $q3->where('title','LIKE',"%".$request->columns['0']['search']['value']."%");
                    })
                    ->orWhereHas('productDetails',function($q4) use($request){
                        $q4->where('product_code','LIKE',"%".$request->columns['0']['search']['value']."%")
                           ->orWhere('times_of_sold','LIKE',"%".$request->columns['0']['search']['value']."%");
                    });
                });
            }
            $ddd = $orders_details;
            $ddd = $ddd->count();
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $orders_details = $orders_details->orderBy('created_at','asc');
                } else {
                    $orders_details = $orders_details->orderBy('created_at','desc');
                }
            }
            $orders_details = $orders_details->skip($request->start)->take($request->length)->get();
            //dd($orders_details);
            $data['aaData'] = $orders_details;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $dd;
            $data["iTotalDisplayRecords"] = $ddd ;
            return response()->json($data);
        } else {

            return view('admin.modules.reports.product_analysis_report')->with(['merchants' => $merchants]);
        }
    }

    /*
    * Method        : showProductAnalysisReportExport
    * Description   : This method is used to export report product report analysis page
    * Author        : Argha
    * Date          : 2020-10-13
    */
    public function showProductAnalysisReportExport(Request $request)
    {
        $orders_details = $this->order_details->with(['orderMaster','sellerDetails','productDetails.productByLanguage'])
                                        ->whereHas('orderMaster',function($q){
                                            $q->where('order_type','I')
                                            ->where('status','!=','D')
                                            ->where('status','!=','OC')
                                            ->where('status','!=','I')
                                            ->where('status','!=','F');
                                        });
        if(@$request->all()){
            if(@$request->user_id){
                $orders_details = $orders_details->where('seller_id', $request->user_id);
            }
            if(@$request->keyword){
                $orders_details = $orders_details->whereHas('orderMaster',function($q1) use($request){
                    $q1->where('order_no','LIKE',"%".$request->keyword."%");
                })
                ->orWhereHas('productDetails.productByLanguage',function($q3) use($request){
                    $q3->where('title','LIKE',"%".$request->keyword."%");
                })
                ->orWhereHas('productDetails',function($q4) use($request){
                    $q4->where('product_code','LIKE',"%".$request->keyword."%");
                });
            }
            
            if(@$request->from_date && @$request->to_date){
                $orders_details = $orders_details->whereBetween('created_at',[$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
            }
        }
        $orders_details = $orders_details->orderBy('id','DESC')->get();
        //dd($orders_details);
        $order_array[] = array('Order no','Date','Merchant','Product Title','Price','Qty');
        foreach($orders_details as $details)
        {
            
            $order_array[] = array(
                'Order no'              => $details->orderMaster->order_no,
                'Date'                  => date('Y-m-d H:i:s',strtotime($details->created_at)),
                'Merchant'              => $details->sellerDetails->fname.' '.$details->sellerDetails->lname,
                'Product Title'         => $details->productDetails->productByLanguage->title,
                'Price'                 => $details->original_price,
                'Qty'                   => $details->quantity,
            );
        }
        //dd($order_array);
        Excel::create('Product Analysis - '. date('Y-m-d'), function($excel) use ($order_array){
            $excel->setTitle('Product Analysis - '. date('Y-m-d'));
            $excel->sheet('Product Analysis - '. date('Y-m-d'), function($sheet) use ($order_array){
                $sheet->fromArray($order_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }

    /*
    * Method        : exportQuickMerchantReport
    * Description   : This method is used to export quick checking report of merchant
    * Author        : Argha
    * Date          : 2021-03-27
    */
    public function exportQuickMerchantReport(Request $request)
    {
        $merchant = Merchant::with(['getOrderSellerDetails.orderMasterTab']);
        
        $data = json_decode(base64_decode($request->token));
        $merchant = $merchant->where('status','A')
                            ->orderBy('id','desc')
                            ->get();
        
        $merchant_array[] = array('Merchant','Total Delivery Fee','Total Sales');
        foreach ($merchant as $val) {
            if($val->getOrderSellerDetails){
                $totalShippingCost = 0 ;
                $totalSales = 0;
                foreach ($val->getOrderSellerDetails as $val2) {
                    if(@$val2){
                        if($val2->orderMasterTab){
                            if($val2->orderMasterTab->status == 'OD'){
                                //dump($val2);
                                if($data->date_from && $data->date_to){
                                    $from_date = strtotime($data->date_from." 00:00:00");
                                    $to_date = strtotime($data->date_to." 23:59:59");
                                    //dd($from_date,$to_date,$val2,$val2->orderMasterTab);
                                    $check_date = strtotime(date('Y-m-d H:i:s',strtotime($val2->orderMasterTab->last_status_date)));
                                    if(($check_date >= $from_date) && ($check_date <= $to_date)){
                                        $totalShippingCost = $totalShippingCost + $val2->shipping_price;
                                        $totalSales = $totalSales + (@$val2->subtotal - @$val2->total_discount);
                                    }
                                }else{
                                    $totalShippingCost = $totalShippingCost + $val2->shipping_price;
                                    $totalSales = $totalSales + (@$val2->subtotal - @$val2->total_discount);
                                }
                                
                            }
                        }
                    }
                    
                    
                    
                }
            }
            $merchant_array[] = array(
                'Merchant'              => $val->fname." ".$val->lname,
                'Total Delivery Fee'    => $totalShippingCost,
                'Total Sales'           => $totalSales,
            );
        }
        Excel::create('Merchant Check - '. date('Y-m-d'), function($excel) use ($merchant_array){
            $excel->setTitle('Merchant Check - '. date('Y-m-d'));
            $excel->sheet('Merchant Check - '. date('Y-m-d'), function($sheet) use ($merchant_array){
                $sheet->fromArray($merchant_array, null, 'A1', false, false);
            });
        })->download('xlsx');
        
    }
    /*
    * Method        : exportQuickDriverReport
    * Description   : This method is used to export quick checking report of driver
    * Author        : Argha
    * Date          : 2021-03-27
    */
    public function exportQuickDriverReport(Request $request)
    {
        $data = json_decode(base64_decode($request->token));
        $driver = Driver::with('getOrderDetails.orderMaster')
                        ->where('status','A')
                        ->orderBy('id','desc')
                        ->get();
        $driver_array[] = array('Driver','Total Cash','Total Online');
        //dd($driver);
        foreach ($driver as $val) {
            $total_cash = 0;
            $total_online = 0;
            if($val->getOrderDetails){
                foreach ($val->getOrderDetails as $val2) {
                    if($val2->orderMaster){
                        if($val2->status == 'OD'){
                            if($data->date_from && $data->date_to){
                                //dd($data->date_from." 00:00:00",$data->date_to." 23:59:59",date('Y-m-d H:i:s',strtotime($val2->orderMaster->last_status_date)));
                                $from_date = strtotime($data->date_from." 00:00:00");
                                $to_date = strtotime($data->date_to." 23:59:59");
                                $check_date = strtotime(date('Y-m-d H:i:s',strtotime($val2->orderMaster->last_status_date)));
                                if(($check_date >= $from_date) && ($check_date <= $to_date)){
                                    if($val2->orderMaster->payment_method == 'C'){
                                        $total_cash = $total_cash + $val2->total;
                                    }
                                    if($val2->orderMaster->payment_method == 'O'){
                                        $total_online = $total_online + $val2->total;
                                    }
                                }
                                
                            }else{
                                if($val2->orderMaster->payment_method == 'C'){
                                    $total_cash = $total_cash + $val2->total;
                                }
                                if($val2->orderMaster->payment_method == 'O'){
                                    $total_online = $total_online + $val2->total;
                                }
                            }
                        }
                    }
                }

                $driver_array[] = array(
                    'Driver'            => $val->fname." ".$val->lname,
                    'Total Cash'        => $total_cash,
                    'Total Online'      => $total_online,
                );
            }
        }
        //dd($driver_array);
        Excel::create('Driver Check - '. date('Y-m-d'), function($excel) use ($driver_array){
            $excel->setTitle('Driver Check - '. date('Y-m-d'));
            $excel->sheet('Driver Check - '. date('Y-m-d'), function($sheet) use ($driver_array){
                $sheet->fromArray($driver_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }
}
