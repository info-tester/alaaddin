<?php

namespace App\Http\Controllers\Admin\Modules\Content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\Models\Help;
use App\Customer;
use Validator;
use Auth;
use App\Models\Language;

use App\Models\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Repositories\CityRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\CountryRepository;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;

use App\Models\Content;
use App\Models\ContentDetails;
use App\Models\ContactUsDetails;


use App\Models\PageContentAboutus;
use App\Models\PageContactContent;
use App\Models\BannerContent;

class ContentController extends Controller
{
    protected $cities,$language,$countries;
    public function __construct(CityRepository $cities,
        LanguageRepository $language,
        CountryRepository $countries,
        Request $request
    )
    {
        $this->middleware('admin.auth:admin');
        $this->cities            =   $cities;
        $this->language        =   $language;
        $this->countries       =   $countries;
    }


    /*
    * Method        : show
    * Description   : This method is used to manage content
    * Author        : Surajit
    */
    public function show(Request $request){
        $city = $this->cities->where('id','!=','0');
        if(@$request->all()){
            $city = $city->orderBy('id','desc')->get();
            return view('admin.modules.city.list_city')->with([
                'city' =>  @$city
            ]);
        }else{
            $city = $city->orderBy('id','desc')->get();
            return view('admin.modules.content.list_content')->with([
                'city' =>  @$city
            ]);
        }
    }


    /*
    * Method        : edit
    * Description   : This method is used to add content
    * Author        : Surajit
    */
    public function edit(Request $request,$id){
        $lang = Language::get();
        $city = $this->cities->where('id',@$id)->first();
        if(@$city){
            return view('admin.modules.content.edit_content')->with(['city'=>@$city]);
        }else{
            return redirect()->back();
        }
    }


    /*
    * Method        : update
    * Description   : This method is used to update content
    * Author        : Surajit
    */
    public function update(Request $request,$id){
        
        $city = $this->cities->where('id',@$id)->first();
        if(@$city){
            $request->validate([
                'name'          => 'required|unique:cities,name,'.$id,
            ],
            [
                'name.required' =>'Country name field is required',
            ]);
            if(@$request->name){
                $new['name']               = @$request->name;
                $new['name_ar']            = @$request->name_ar;
            }
            $this->cities->where('id',$id)->update($new);
            session()->flash("success",__('success.-4071'));
            return redirect()->route('admin.manage.content');
        }else{
            return redirect()->back();
        }
    }


    // ======page conter start ========
    public function pageAbout(Request $request){
        $about_content = PageContentAboutus::first();
        
        return view('admin.modules.content.add_about_page_content')->with([
            'about_content'=>@$about_content,
        ]);

    }


    /*
    * Method        : pageAboutContentUpdate
    * Description   : This method is used to update about us
    */ 
    public function pageAboutContentUpdate(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'about_us_title' => 'required',
                'about_us_description' => 'required'
            ],
            [
                'about_us_title.required'                =>'Title is required',
                'about_us_description.required'                =>'Description is required',
            ]);
            if($validator->fails()){
                return redirect()->back()->with("error",$validator->errors()->first());
            }
            if(@$validator) {
                $data = $request->all();
                
                $pageContentAboutus = PageContentAboutus::where('id','!=',0)->first();
                if(@$pageContentAboutus){
                    if(@$request->all()){
                        $update['about_us_title'] = @$request->about_us_title;
                        $about_us_description = str_replace("<img","<img class='abt-img' style='width:auto;height:auto;max-width:100%;max-height:100%;'",$request->about_us_description);
                        $update['about_us_description'] = $about_us_description;
                        PageContentAboutus::where('id',$pageContentAboutus->id)->update($update);
                        return redirect()->back()->with("success","About us updated successfully!");
                    }else{
                        return redirect()->back()->with("error","Please try again!");
                    }
                }
            }else{
                return redirect()->back()->with("error",$validator->errors()->fir);    
            }

        }catch(\Exception $e){
            return redirect()->back()->with("error",$e->getMessage());
        }
    }




    private function checkAccess(){
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(2, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
    }

    private function checkLoginAccess(){
        if(!@Auth::guard('admin')->user()){
            return redirect()->route('admin.login');
        }
    }


    // =========terms page========
    public function pageTermsConditions(Request $request ,$id){
        $term_content = ContentDetails::where('content_id',$id)->get();
        $language = Language::where('status','A')->get();
        return view('admin.modules.content.add_term_page_content')->with([
            'term_content'=>@$term_content,
            'language'=>@$language,
            'pageid'=>@$id,
        ]);

    }

    //edit post page
    public function pageTermsConditionsPost(Request $request,$pageid){
        $termsDetails = ContentDetails::where('content_id',$pageid)->get();
        $language = Language::where('status','A')->get();
        
        if($request->all()){
            DB::transaction(function() use($request,$language,$pageid){
                if(@$request->title){
                    foreach(@$request->title as $key=>$val){
                        $update['title'] = $val;
                        $update['description'] = $request->contract_desc[$key];
                        ContentDetails::where(['content_id'=>$pageid,'language_id'=>$language[$key]->id])->update($update);
                    }
                }
                // session()->flash("success");
                session()->flash("success",__('success.-5003'));
            });
            return redirect()->back();
        }else{
            //unauthorized access
            // session()->flash("error");
            session()->flash("error",__('errors.-32700'));
            return redirect()->back();
        }
    }
    public function pageHelpPost(Request $request,$pageid){
        $termsDetails = ContentDetails::where('content_id',$pageid)->get();
        $language = Language::where('status','A')->get();
        
        if($request->all()){
            DB::transaction(function() use($request,$language,$pageid){
                if(@$request->title){
                    foreach(@$request->title as $key=>$val){
                        $update['title'] = $val;
                        $update['description'] = @$request->contract_desc[$key];
                        ContentDetails::where(['content_id'=>3,'language_id'=>$language[$key]->id])->update($update);
                    }
                }
                session()->flash("success",__('success.-4071'));
            });
            return redirect()->back();
        }else{
            return view('admin.modules.content.add_help_page_content')->with([
                'term_content'=>@$termsDetails,
                'pageid'=>@$pageid,
                'language'=>@$language,
            ]);
        }
    }

    /**
    * Method: appSettings
    * Description: This method is used to set app config
    * Author: Sanjoy
    */
    public function appSettings(Request $request) {
        $data['app_settings'] = ContactUsDetails::first();
        if($request->all()) {
            $update = [
                'ios_version'           => $request->ios_version,
                'ios_version_check'     => $request->ios_version_check,
                'message_for_ios'       => $request->message_for_ios,
                'android_version'       => $request->android_version,
                'android_version_check' => $request->android_version_check,
                'message_for_android'   => $request->message_for_android
            ];
            ContactUsDetails::where(['id' => 1])->update($update);
            session()->flash("success",__('success.-4089'));
            return redirect()->back();
        }
        return view('admin.modules.content.app_settings')->with($data);
    }


// ====privecy page====

    public function pagePrivecy(Request $request ,$id){
        $term_content1 = ContentDetails::where('content_id',2)->get();
        $language = Language::where('status','A')->get();
       
        return view('admin.modules.content.add_privecy_page_content')->with([
            'term_content1'=>@$term_content1,
            'language'=>@$language,
            'pageid'=>@$id,
        ]);
    }


    //edit post page
    public function pagePrivecyPost(Request $request,$pageid){
        $termsDetails = ContentDetails::where('content_id',$pageid)->get();
        $language = Language::where('status','A')->get();
        if($request->all()){
            DB::transaction(function() use($request,$language,$pageid){
                if(@$request->title){
                    foreach(@$request->title as $key=>$val){
                        $update['title'] = $val;
                        $update['description'] = $request->contract_desc[$key];
                        ContentDetails::where(['content_id'=>$pageid,'language_id'=>$language[$key]->id])->update($update);
                    }
                }
                session()->flash("success",__('success.-5004'));
            });
            return redirect()->back();
        }else{
            //unauthorized access
            // session()->flash("error");
            session()->flash("error",__('errors.-32700'));
            return redirect()->back();
        }
    }


    // =====contact page====
    public function pageContact(Request $request){
        $contact_content = PageContactContent::get();
        $language = Language::where('status','A')->get();
        return view('admin.modules.content.add_contact_page_content')->with([
            'contact_content'=>@$contact_content,
            'language'=>@$language,
        ]);
    }



    public function pageContactContentUpdate(Request $request){
        $data = $request->all();
        $language = Language::where('status','A')->get();
        $contactDetails = PageContactContent::where('page_id',1)->get();
        if(@$contactDetails){
            if($request->all()){
                DB::transaction(function() use($request,$language){
                    if(@$request->first_sec_second_heading){
                        foreach(@$request->first_sec_second_heading as $key=>$val){
                            $update['sec_heading_contact'] = $val;

                            $update['first_sec_first_title'] = @$request->first_sec_first_title[$key];
                            $update['first_sec_first_content'] = nl2br(@$request->first_sec_first_content[$key]);
                            $update['boxone_hed_caption'] = @$request->boxone_hed_caption[$key];
                            $update['boxone_butn_link'] = @$request->boxone_butn_link[$key];

                            $update['first_sec_second_heading'] = @$request->first_sec_second_heading[$key];
                            $update['first_sec_second_content'] = nl2br(@$request->first_sec_second_content[$key]);
                            $update['boxtwo_hed_caption'] = @$request->boxtwo_hed_caption[$key];
                            $update['boxtwo_butn_link'] = @$request->boxtwo_butn_link[$key];

                            $update['first_sec_third_heading'] = @$request->first_sec_third_heading[$key];
                            $update['first_sec_third_content'] = nl2br(@$request->first_sec_third_content[$key]);
                            $update['boxthree_hed_caption'] = @$request->boxthree_hed_caption[$key];
                            $update['boxthree_butn_link'] = @$request->boxthree_butn_link[$key];

                            $update['contact_us_form_heading'] = @$request->contact_us_form_heading[$key];
                            $update['facebook_link'] = @$request->facebook_link[$key];
                            $update['twitter_link'] = @$request->twitter_link[$key];
                            $update['linkedin_link'] = @$request->linkedin_link[$key];
                            $update['youtube_link'] = @$request->youtube_link[$key];
                            PageContactContent::where(['page_id'=> 1,'language_id'=>$language[$key]->id])->update($update);
                        }
                    }
                    session()->flash("success",__('success.-4071'));
                });
            }
            session()->flash("error",__('errors.-32700'));
            return redirect()->back();
        }
    }


    // banner management

    public function bannerListing(){
        $bannerdata =  BannerContent::where('language_id', getLanguage()->id)->get();

        return view('admin.modules.content.list_banner')->with([
            'bannerdata'=>@$bannerdata            
        ]);

    }


    public function bannerDetails($id){
        $banner_details = BannerContent::where('banner_id',$id)->get();
        $language = Language::where('status','A')->get();
       
        return view('admin.modules.content.edit_banner_content')->with([
            'banner_details'=>@$banner_details,
            'language'=>@$language,
            'pageid'=>@$id,
        ]);

    }

    public function addBanner()
    {

        $language = Language::where('status','A')->get();

            $totbannercount = BannerContent::count();
            // if($totbannercount > 5){
                // return redirect()->route('admin.manage.bannerlist');        
            // }
        
       //dd($language);
        return view('admin.modules.content.add_banner_content')->with([
            'language'=>@$language
        ]);
    }

    public function insertBanner(Request $request)
    {
        $language = Language::where('status','A')->get();
        $ban_ids = array();
        $ban_id = 0;
        //dd($request->all());
        if($request->all()){
            //DB::transaction(function() use($request,$language){
                // if(@$request->banner_heading){
                //     foreach(@$request->banner_heading as $key=>$val){
                //         $insert['banner_heading'] = $val;
                //         $insert['banner_sub_heading'] = $request->banner_sub_heading[$key];
                //         $insert['banner_desc'] = $request->banner_desc[$key];
                //         // $insert['banner_buttton_cap'] = $request->banner_buttton_cap[$key];
                //         // $insert['banner_buttton_url'] = $request->banner_buttton_url[$key];
                //         $insert['language_id'] = $language[$key]->id;
                //         $ban = BannerContent::create($insert);
                //         array_push($ban_ids,$ban->id);
                //         $ban_id = $ban->id;
                //     }
                // }
                //dd($ban_id,$ban_ids);
                
            //});

            if(@$request->banner_image){
                
                    $pic   = $request->banner_image;
                    $name = time().'.'.$pic->getClientOriginalExtension();
                    $pic->move('storage/app/public/bannerimg/', $name);
                    $banner['banner_image'] = $name;   
                    // foreach ($ban_ids as  $id) { 
                        $banner['banner_id'] =   BannerContent::count()+1;   
                        //dd($banner);           
                        // BannerContent::where(['id'=> $id])->update($banner);
                        $banner['language_id'] = 1;
                        $banner['language_id'] = 1;
                        $b = BannerContent::create($banner);
                    // }
                
            }
            session()->flash("success",__('success.-5005'));
            return redirect()->back();
        } else {
            session()->flash("error",__('errors.-32700'));
            return redirect()->back();
        }
    }
    public function bannerDetailsPost(Request $request,$id){
        $bannerDetails = BannerContent::where('banner_id',$id)->first();
        $language = Language::where('status','A')->get();
        if($request->all()){
            DB::transaction(function() use($request,$language,$id){
                if(@$request->banner_heading){
                    foreach(@$request->banner_heading as $key=>$val){
                        $update['banner_heading'] = $val;
                        $update['banner_sub_heading'] = $request->banner_sub_heading[$key];
                        $update['banner_desc'] = $request->banner_desc[$key];
                        // $update['banner_buttton_cap'] = $request->banner_buttton_cap[$key];
                        // $update['banner_buttton_url'] = $request->banner_buttton_url[$key];
                        BannerContent::where(['banner_id'=>$id,'language_id'=>$language[$key]->id])->update($update);
                    }
                }
                session()->flash("success",__('success.-5006'));
            });

            if(@$request->banner_image){
                $image_path = 'storage/app/public/bannerimg/'.@$bannerDetails->banner_image;
                if(file_exists(@$image_path)){
                   @unlink(@$image_path);
                }
                $pic   = $request->banner_image;
                $name = time().'.'.$pic->getClientOriginalExtension();
                $pic->move('storage/app/public/bannerimg/', $name);
                $banner['banner_image'] = $name;                      
                BannerContent::where(['banner_id'=> $id])->update($banner);
            }
            return redirect()->back();
        } else {
            session()->flash("error",__('errors.-32700'));
            return redirect()->back();
        }


    }

    public function deleteBanner($banner_id)
    {
        //dd($banner_id);
        $getBanner = BannerContent::where(['banner_id'=> $banner_id])->first();
        if(@$getBanner->banner_image && file_exists('storage/app/public/bannerimg/'.$getBanner->banner_image)){
            unlink('storage/app/public/bannerimg/'.$getBanner->banner_image);
        }
        BannerContent::where(['banner_id'=> $banner_id])->delete();
        $msg['success']['message'] = "Banner deleted successfully";
        session()->flash("success",__('success.-5000'));
        return redirect()->back();
    }

    /*
    *
    *
    */ 
    // ======page conter end ========
    public function imgUpload(Request $request)
    {
        $jsn = [
                "jsonrpc"   =>  "2.0"
        ];
        $doc_name           = $request['file']->getClientOriginalName();
        $explode_doc_name   = explode('.',$doc_name);
        $extension          = end($explode_doc_name);
        $doc_name           = time().str_random(6).'.'.$extension;
        $newFilePath        = "storage/app/public/uploads/content_image/";
        $request['file']->move($newFilePath, $doc_name);
        $jsn['status'] = 'SUCCESS';
        $jsn['location'] = url('/').'/storage/app/public/uploads/content_image/'.$doc_name;
        return response()->json($jsn);
    }

    public function getHelpPage(Request $request){
        
        $data['help'] = Help::first();
        if(!@$data['help']){
            return redirect()->back()->with("error","Admin panel help row not found!");
        }
        if(@$request->all()){

            $updateHelp['title'] = @$request->title;
            $updateHelp['description'] = @$request->description;
            $updateHelp['help_image'] = @$request->help_image;
            $updateHelp['c_title'] = @$request->c_title;
            $updateHelp['c_desc'] = @$request->c_desc;
            $updateHelp['f_title'] = @$request->f_title;
            $updateHelp['f_desc'] = @$request->f_desc;
            $updateHelp['email_title'] = @$request->email_title;
            $updateHelp['email_desc'] = @$request->email_desc;
            $updateHelp['title_1'] = @$request->title_1;
            $updateHelp['title_2'] = @$request->title_2;
            $updateHelp['title_3'] = @$request->title_3;

            if(@$request->help_image){
                $pic   = $request->help_image;
                $name = time().'.'.$pic->getClientOriginalExtension();
                $pic->move('storage/app/public/help/', $name);
                $updateHelp['help_image'] = $name;                      
            }
            
            Help::where('id',$data['help']->id)->update($updateHelp);

            return redirect()->back();
        }else{
            return view('admin.modules.content.help')->with($data);

        }
    }
}