<?php

namespace App\Http\Controllers\Admin\Modules\Messages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ConversationsRepository;
use App\Models\Country;
use App\Models\OrderMaster;
use App\User;
use Validator;
use Auth;
use App\Models\Language;
use App\Models\Admin;
use App\Models\City;
use App\Models\UserRegId;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Repositories\ShippingCostRepository;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\CityRepository;
use App\Repositories\UserRepository;
use App\Repositories\DriverRepository;
use App\Repositories\OrderImageRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductRepository;
use App\Models\CitiDetails;
use App\Models\NotificationData;
use App\Repositories\CitiDetailsRepository;
use App\Repositories\SettingRepository;
use App\Repositories\CountryDetailsRepository;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;
use App\Mail\SendMailDriverNotifydetails;
use App\Mail\SendMailExternalOrderdetails;
use Config;

class MessagesController extends Controller
{
	public function __construct(ConversationsRepository $conversations,
     Request $request)
    {
        $this->middleware('admin.auth:admin');
        // $this->checkLoginAccess();
        $this->conversations =   $conversations;
    }
    public function showMessages(){
    	$conversations = $this->conversations->with(['sendBySubAdminDetails','sendByCustomerDetails','sendByDriverDetails','sendBySubAdminDetails','sendByMerchantDetails'])->orderBy('id','desc')->get();
    	// dd($conversations);
    	return view('admin.modules.messages.conversations')->with(['conversations'=>@$conversations]);
    }

    public function sendPushNotification(Request $request){
        if(@$request->all()){
            $title_eng = @$request->title_eng;
            $title_ar = @$request->title_ar;
            $body_eng = @$request->body_eng;
            $body_ar = @$request->body_ar;
            #send notification
            try{
                //$this->sendNotificationCustomer($title_eng,$title_ar,$body_eng,$body_ar);
                //dd($request->all());
                $insNotDetails = [
                    'title_eng' => @$request->title_eng,
                    'title_ar' => @$request->title_ar,
                    'body_eng' => @$request->body_eng,
                    'body_ar' => @$request->body_ar,
                    'total_ios_reg_id' => @$request->ios_count,
                    'total_android_reg_id' => @$request->android_count,
                    'total_ios_sended' => 0,
                    'total_android_sended' =>0,    
                    'status' => 'N',    
                ];
                NotificationData::create($insNotDetails);    
            }
            catch(\Exception $e){
                return redirect()->back()->withError(['message' => $e->getMessage(), 'meaning' => "test"]);
            }
            session()->flash("success",__('success.-4083'));        
            
            return redirect()->back();
        } else {
            $data['android'] = UserRegId::where('type', 'A')->where('reg_id','!=',null)->pluck('reg_id')->toArray();
            $data['ios'] = UserRegId::where('type', 'I')->where('reg_id','!=',null)->pluck('reg_id')->toArray();
            $data['android_count'] = count($data['android']);
            $data['ios_count'] = count($data['ios']);
            $all_android = array_chunk($data['android'],500);
            $all_ios = array_chunk($data['ios'],500);
            $data['an_block'] = json_encode($all_android);
            $data['ios_block'] = json_encode($all_ios);
            return view('admin.modules.notification.send_notification')->with($data);
        }
    }
    
    public function viewMessage($id){
    	$message = $this->conversations->with(['sendBySubAdminDetails','sendByCustomerDetails','sendByDriverDetails','sendBySubAdminDetails','sendByMerchantDetails'])->orderBy('id','desc')->where('id',$id)->first();
    	if(@$message){
    		return view('admin.modules.messages.conversations_details')->with(['message'=>@$message]);
    	}else{
    		session()->flash("error",__('errors.-5002'));
    		return redirect()->back();
    	}
    	
    }
    /**
    *   Method  : getNotificationRegid
    *   Use     : This method is used to get regid for notification  
    *   Author  : Argha
    *   Date    : 2020-Dec-23
    */
    public function getNotificationRegid()
    {
        $android = UserRegId::where('type', 'A')->where('reg_id','!=',null)->pluck('reg_id')->toArray();
        $ios = UserRegId::where('type', 'I')->where('reg_id','!=',null)->pluck('reg_id')->toArray();
        $all_android = array_chunk($android,500);
        $all_ios = array_chunk($ios,500);
        //dd($android,$ios);
        $response['jsonrpc'] = 2.0;
        $response['status'] = 1;
        $response['android'] = $all_android;
        $response['ios'] = $all_ios;

        return response()->json($response);
    }
    /**
    *   Method  : sendPushNotificationToUser
    *   Use     : To send push notification to android and ios user.
    *   Author  : Argha
    *   Date    : 2020-Dec-23
    */
    public function sendPushNotificationToUser(Request $request)
    {
        $data= $request->data;
        $response['jsonrpc'] = 2.0;
        $response['status'] = 0;
        $count = $data['count'] *60;
        //$response['count'] = $count;
        $response['device_type'] = $data['type'];
        $count_start = $data['count'] * 500;
        $count_end = ($data['count']+1) * 500;
        //sleep($count);
        $response['count_start'] = $count_start;
        
        $title_eng = $data['title_eng'];
        $title_ar = $data['title_ar'];
        $body_eng = $data['body_eng'];
        $body_ar = $data['body_ar'];

        if($data['type'] == 'A'){
            if(count($data['array']) > 0){
                // $arr1 = [0 =>'fmjQpnRBTY6nTmNVnuozN7:APA91bE5uIEuEdoTwogeS-Vekjb3RLjE8fqhS2psIZywOSTYCaMcqBk48RwSgb0Xr-y-s95xVW2tlBcm59lsebmYFwD6A0kE6CroMGKWR9eP9eL6uldjh4A5wKzIP-Lfd52p5ZqoiR-X'];
                // $data['array']  = array_merge($arr1,$data['array']);
                //dd($data['array']);
                
                $an = UserRegId::where('type', $data['type'])
                                ->where('reg_id','!=',null)
                                ->skip($count_start)
                                ->take(500)
                                ->pluck('reg_id');
                $count_end = count($an);
                $response['count_end']  = $count_start + $count_end;
                //dump($an);
                
                $msg                      = array();
                $msg['title']             = $title_eng.'  '.$title_ar;
                $msg["body"]              = $body_eng.'  '.$body_ar;
                $msg["message"]           = $body_eng.'  '.$body_ar;
                $msg['link_type']         = "Y";
                $msg['is_linked']         = "Y";
                $msg['image']             = '';
                $msg['soundname']         = 'pick';
                $msg['content-available'] = '1';
                $msg["info"]              = "";
                $msg["priority"]          = "2";
                $msg["volume"]           = "10";
                $headers = array(
                    'Authorization: key=' . env('FIREBASE_KEY'),
                    'Content-Type: application/json',
                );
                $fields = array(
                    'registration_ids' => $an,
                    'data'             => $msg
                );
                // Initializing Curl
                $ch = curl_init();
                // Posting data to the following URL
                curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

                // Post Data = True, Defining Headers and SSL Verifier = false
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                // Posting fields array in json format
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                // Executing Curl
                $result = curl_exec($ch);
                // Closing Curl
                curl_close($ch);
            }
        }

        if($data['type'] == 'I'){
            if(count($data['array']) > 0){

                $io = UserRegId::where('type', $data['type'])
                                ->where('reg_id','!=',null)
                                ->skip($count_start)
                                ->take(500)
                                ->pluck('reg_id');
                $count_end = count($io);
                $response['count_end']  = $count_start + $count_end;
                //dump($io);
                $msg                      = array();
                $msg['title']             = $title_eng.'  '.$title_ar;
                
                $msg['soundname']         = 'pick';
                $msg['content-available'] = '1';
                $msg["info"]              = "";
                $msg["priority"]          = "2";
                $msg["volume"]            = "10";
                $msg["mutable-content"]   = true;
                $msg["body"]              = $body_eng.'  '.$body_ar;
                $msg["message"]           = $body_eng.'  '.$body_ar;
                $data['type'] = "1";
                $headers = array(
                    'Authorization: key=' . env('FIREBASE_KEY'),
                    'Content-Type: application/json',
                );
                $fields = array(
                    'category'                  => "CustomSamplePush",
                    'content_available'         => true,
                    'mutable_content'           => true,
                    'registration_ids'          => $io,
                    'notification'              => $msg,
                    'data'                      => $data,
                    'priority'                  =>'high'
                );
                // Initializing Curl
                $ch = curl_init();
                // Posting data to the following URL
                curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

                // Post Data = True, Defining Headers and SSL Verifier = false
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                // Posting fields array in json format
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                // Executing Curl
                $result = curl_exec($ch);

                // dd($result);
                // Closing Curl
                curl_close($ch);
            }
        }
        $response['status'] = 1;
         return response()->json($response);
    }
    /**
    *   Method  : sendNotificationCustomer
    *   Use     : This method is used to send notification in customer app for if order status changed!  
    *   Author  : Jayatri
    * Not using From Date 20202-Dec-23
    */
    private function sendNotificationCustomer($title_eng,$title_ar,$body_eng,$body_ar){
        $androidUsers = UserRegId::where('type', 'A')->where('reg_id','!=',null)->pluck('reg_id')->toArray();
        $iosUsers1 = UserRegId::where('type', 'I')
                            ->where('reg_id','!=',null)
                            ->select('reg_id')
                            ->whereBetween('id', [1, 900])
                            ->get()->toArray();
        $iosUsers2 = array();
        $iosUsers3 = [
            0 =>'dj_Vu4an3aU:APA91bFRL-LcMHaTOYcp4Nv4QUm-GNFZHBuJCZhz-eSNrxEVyoB5O7sBKVPnJ4v9jjAWXh5pjHifD8A_Q6AUhRALzbKNFNaFY1RCg-tYMTCDPtrkxobUh4RnltlQiKucmvQWpYIExNGb',
            1 => 'dRphLnxYj-M:APA91bHojlvhy3s_X41-jVae_MsuzDPRwbKMM9T35XswFOK2XHENSOvSKec_3KBuAOj3FOlQwGGCbG-1Ian_os__6M0qL6spu-XyP5dypNBOLyBw26hD4aKm5jvv5hWajdAmYCLrB1xA'
        ];
        foreach ($iosUsers1 as $val) {
           array_push($iosUsers2,@$val['reg_id']);
        }
        // foreach ($iosUsers2 as $val) {
        //     array_push($iosUsers3,@$val['reg_id']);
        // }
        $iosUsers = array_merge($iosUsers2,$iosUsers3);
        // $androidUsers = UserRegId::where('type', 'A')
        //                         ->where('reg_id','dCmQkrxUSVKsxTx3INGsyz:APA91bFHZpRl70Dm4FvwvHHHSqj8lnm5BVME1eKabfzbK_TW6wlRDIfky4MDpHoI3nFaqgEf-m0K8-Z3Dg-6oxKp2kigRICxvUJ-vgvZFUTumpRYV_9VCmRB5uBhhU_tKf7_nZ1nX8bZ')
        //                         ->pluck('reg_id')
        //                         ->toArray();

        // $iosUsers = UserRegId::where('type', 'I')
        //                     ->where('reg_id','dj_Vu4an3aU:APA91bFRL-LcMHaTOYcp4Nv4QUm-GNFZHBuJCZhz-eSNrxEVyoB5O7sBKVPnJ4v9jjAWXh5pjHifD8A_Q6AUhRALzbKNFNaFY1RCg-tYMTCDPtrkxobUh4RnltlQiKucmvQWpYIExNGb')
        //                     ->pluck('reg_id')
        //                     ->toArray();
        
        // $order_dtt = OrderMaster::where('id',$order_id)->first();
        // $iosUsers= [
        //     0 =>'dj_Vu4an3aU:APA91bFRL-LcMHaTOYcp4Nv4QUm-GNFZHBuJCZhz-eSNrxEVyoB5O7sBKVPnJ4v9jjAWXh5pjHifD8A_Q6AUhRALzbKNFNaFY1RCg-tYMTCDPtrkxobUh4RnltlQiKucmvQWpYIExNGb',
        //     1 => 'dRphLnxYj-M:APA91bHojlvhy3s_X41-jVae_MsuzDPRwbKMM9T35XswFOK2XHENSOvSKec_3KBuAOj3FOlQwGGCbG-1Ian_os__6M0qL6spu-XyP5dypNBOLyBw26hD4aKm5jvv5hWajdAmYCLrB1xA'
        // ];
        //$androidUsers = [
            //0 =>'cbVV1PpvR8SbK338VGFBSl:APA91bElMXw8V5_vvFnlBjh5Y3_xcpEGU_WI1kYvf-uOd0xfa01lpBHA5NwPaCIZmNu4IsIBazJC2nzQvluOWgqo2KMgwtHAz6yN8xdYAzwlliCIlzCwPb93xORnbkbFTgvoeUJDes82',
            // 1 => 'eJnBEEr4QJSzBwaNYcCzxw:APA91bF_F6IlPnC3iu0EV_Ou50VmWw4azcMebLiiC4m6-hYxZ9e9IQEHJe02Fxz8zuqy9YEXSn2jHHWkBvkXuURfydadDe1wPFBUoPFyhCyb8dHPBiZmNKL3Vb5lXHK74Sl7qkDRLbPt',
        //];
        //dd($androidUsers, $iosUsers);
        # send notification to android drivers
        if(count($androidUsers)) {
                $msg                      = array();
            // if(@$order_dtt->status == 'N'){
                $msg['title']             = $title_eng.'  '.$title_ar;
                $msg["body"]              = $body_eng.'  '.$body_ar;
                $msg["message"]           = $body_eng.'  '.$body_ar;
            // }
            // if(@$order_dtt->status == 'DA'){
            //     $msg['title']             = ''." "." Order No : ".@$order_dtt->order_no;
            // $msg["body"]              = 'Your order is out for delivery! ';
            // }
            // if(@$order_dtt->status == 'RP'){
            //     $msg['title']             = " "." Order No : ".@$order_dtt->order_no;
            // $msg["body"]              = 'Your order status is ready to be picked up! ';
            // }
            // if(@$order_dtt->status == 'OP'){
            //     $msg['title']             = ''." "." Order No : ".@$order_dtt->order_no;
            //     $msg["body"]              = 'Your order is picked up! ';
            // }
            // if(@$order_dtt->status == 'OD'){
            //     $msg['title']             = ''." "." Order No : ".@$order_dtt->order_no;
            // $msg["body"]              = 'Your order Delivered! ';
            // }
            

            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            $msg['link_type']         = "Y";
            $msg['is_linked']         = "Y";
            $msg['image']             = '';
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]           = "10";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'registration_ids' => $androidUsers,
                'data'             => $msg
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            // Closing Curl
            curl_close($ch);
        }
        // dd($result);
        # send notification to ios drivers
        if(count($iosUsers)) {
            $msg                      = array();
            $msg['title']             = $title_eng.'  '.$title_ar;
            
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]            = "10";
            $msg["mutable-content"]   = true;
            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            // $msg["body"]              = 'Recently customer placed an order! ';

            // $data['message'] = "New Order Placed";
            $msg["body"]              = $body_eng.'  '.$body_ar;
            $msg["message"]           = $body_eng.'  '.$body_ar;
            $data['type'] = "1";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'category'                  => "CustomSamplePush",
                'content_available'         => true,
                'mutable_content'           => true,
                'registration_ids'          => $iosUsers,
                'notification'              => $msg,
                'data'                      => $data
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);

            // dd($result);
            // Closing Curl
            curl_close($ch);
        }

        #send notification to ios drivers
        // if(count($iosUsers)) {
        //     $msg                      = array();
        //     $msg['title']             = "New Order Placed";
        //     $msg['soundname']         = 'pick';
        //     $msg['content-available'] = '1';
        //     $msg["info"]              = "";
        //     $msg["priority"]          = "2";
        //     $msg["volume"]            = "10";
        //     $msg["mutable-content"]   = true;
        //     // $msg['title']             = "msg title";
        //     // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
        //     $msg["body"]              = 'Ios customer placed an order!';

        //     $data['message'] = "New Order Placed";
        //     $data['type'] = "1";
        //     $headers = array(
        //         'Authorization: key=' . env('FIREBASE_KEY'),
        //         'Content-Type: application/json',
        //     );
        //     $fields = array(
        //         'category'                  => "CustomSamplePush",
        //         'content_available'         => true,
        //         'mutable_content'           => true,
        //         'registration_ids'          => $iosUsers,
        //         'notification'              => $msg,
        //         'data'                      => $data
        //     );
        //     // Initializing Curl
        //     $ch = curl_init();
        //     // Posting data to the following URL
        //     curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

        //     // Post Data = True, Defining Headers and SSL Verifier = false
        //     curl_setopt($ch, CURLOPT_POST, true);
        //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //     // Posting fields array in json format
        //     curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        //     // Executing Curl
        //     $result = curl_exec($ch);
        //     // Closing Curl
        //     curl_close($ch);
        // }

        
    }
    
}
