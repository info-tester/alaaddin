<?php

namespace App\Http\Controllers\Admin\Modules\Zone;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ZoneMaster;
use App\Models\ZoneDetail;
use App\Models\ZoneRateDetail;
use App\Models\ZoneRateMaster;
use App\Models\Country;
use Auth;
class ZoneController extends Controller
{
    public function __construct(
        Request $request
    )
    {
        $this->middleware('admin.auth:admin');
        $this->checkLoginAccess();
    }
    /*
    * Method        : index
    * Description   : This method is used to show zone list
    * Author        : Argha
    * Date          : 2020-Sep-10
    */

    public function index(Request $request)
    {
        $this->checkLoginAccess();
        $this->checkMenuAccess();
        $zones = ZoneMaster::with(['zone_details.countryDetails.countryDetailsBylanguage']);
        $dd = $zones->count();
        if($request->all()){
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc
            //dd($request->all());
            if(@$request->columns['1']['search']['value']){
                $zones = $zones->Where('zone_name','like','%'.@$request->columns['1']['search']['value'].'%');
            }
            $ddd = $zones;
            $ddd = $ddd->count();

            // sorting
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $zones = $zones->orderBy('zone_name','asc');
                } else {
                    $zones = $zones->orderBy('zone_name','desc');
                }
            }
            

            $zones = $zones->skip($request->start)->take($request->length)->get();

            $data['aaData'] = $zones;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $dd;
            $data["iTotalDisplayRecords"] = @$ddd;
            return response()->json($data);
        }else{
            return view('admin.modules.zone.zone_list');
        }
    }

    /*
    * Method        : addZone
    * Description   : This method is used to show add zone page
    * Author        : Argha
    * Date          : 2020-Sep-10
    */

    public function addZone()
    {
        $this->checkLoginAccess();
        $this->checkMenuAccess();
        $getZoneCountry = ZoneDetail::select('country_id')->get();
        $allCountries = array();
        foreach ($getZoneCountry as $v) {
            array_push($allCountries,$v->country_id);
        }
        

        $data['country'] = Country::with('countryDetailsBylanguage')
                                    ->whereNotIn('id',$allCountries)
                                    ->where('id','!=',134)
                                    ->where('status','!=','D')
                                    ->get();
        return view('admin.modules.zone.add_zone')->with($data);
    }

    /*
    * Method        : addZone
    * Description   : This method is used to show add zone page
    * Author        : Argha
    * Date          : 2020-Sep-10
    */

    public function insertZone(Request $request)
    {
        $this->checkLoginAccess();
        $this->checkMenuAccess();

        $request->validate([
            'zone_name' => 'required',
            'country'   => 'required'
        ]);

        $insZone['zone_name'] = $request->zone_name;

        $res1 = ZoneMaster::create($insZone);

        if($request->country){
            foreach ($request->country as $cou) {
                $insZoneDet['zone_master_id'] = $res1->id;
                $insZoneDet['country_id'] = $cou;

                ZoneDetail::create($insZoneDet);
            }
            
        }
        if($res1){
            session()->flash("success",__('success.-4092'));
        }else{
            session()->flash("errors",__('success.-5082'));
        }

        return redirect()->route('admin.zone.list');
    }

     /*
    * Method        : editZone
    * Description   : This method is used to show edit zone page
    * Author        : Argha
    * Date          : 2020-Sep-10
    */

    public function editZone($id)
    {
        $this->checkLoginAccess();
        $this->checkMenuAccess();
        $getZoneCountry = ZoneDetail::select('country_id')->where('zone_master_id','!=',$id)->get();
        $allCountries = array();
        foreach ($getZoneCountry as $v) {
            array_push($allCountries,$v->country_id);
        }

        $data['country'] = Country::with('countryDetailsBylanguage')
                                    ->whereNotIn('id',$allCountries)
                                    ->where('status','!=','D')
                                    ->get();
        $data['zone_data'] = ZoneMaster::with('zone_details')
                            ->where('id',$id)
                            ->first();
        return view('admin.modules.zone.edit_zone')->with($data);
    }

     /*
    * Method        : updateZone
    * Description   : This method is used to update zone page
    * Author        : Argha
    * Date          : 2020-Sep-10
    */

    public function updateZone(Request $request)
    {
        $this->checkLoginAccess();
        $this->checkMenuAccess();

        $request->validate([
            'zone_name' => 'required',
            'country'   => 'required'
        ]);

        $upZone['zone_name'] = $request->zone_name;

         $res1 = ZoneMaster::where('id',$request->id)->update($upZone);
        
        if($request->country){
            $checkzone = ZoneDetail::where('zone_master_id',$request->id)->first();
            if($checkzone != null){
                ZoneDetail::where('zone_master_id',$request->id)->delete();
            }
            foreach ($request->country as $cou) {
                $insZoneDet['zone_master_id'] = $request->id;
                $insZoneDet['country_id'] = $cou;

                ZoneDetail::create($insZoneDet);
            }
            
        }
        if($res1){
            session()->flash("success",__('success.-4093'));
        }else{
            session()->flash("errors",__('success.-5083'));
        }

        return redirect()->route('admin.zone.list');
    }

     /*
    * Method        : deleteZone
    * Description   : This method is used to delete zone
    * Author        : Argha
    * Date          : 2020-Sep-10
    */
    public function deleteZone(Request $request)
    {
        $id = $request['data']['id'];
        $response['jsonrpc'] = '2.0';
        $zone = ZoneMaster::where('id',$id)->delete();
        ZoneDetail::where('zone_master_id',$id)->delete();
        if($zone){
            $response['status'] = 1;
        }else{
            $response['status'] = 0;
        }
        return response()->json($response);
    }

    /********************************************************Zone Rate ***************************************************** */
    /*
    * Method        : zoneRateList
    * Description   : This method is used to show zone rate list
    * Author        : Argha
    * Date          : 2020-Sep-11
    */

    public function zoneRateList(Request $request)
    {
        $this->checkLoginAccess();
        $this->checkMenuAccess();

        $zoneRate = ZoneRateDetail::with(['getZone']);
        $dd = $zoneRate->count();
        if($request->all()){
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc
            //dd($request->all());
            
            $ddd = $zoneRate;
            $ddd = $ddd->count();

            // sorting
            // if($columnIndex == 0) {
            //     if($columnSortOrder == 'asc') {
            //         $zoneRate = $zoneRate->orderBy('zone_name','asc');
            //     } else {
            //         $zoneRate = $zoneRate->orderBy('zone_name','desc');
            //     }
            // }
            

            $zoneRate = $zoneRate->skip($request->start)->take($request->length)->get();

            $data['aaData'] = $zoneRate;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $dd;
            $data["iTotalDisplayRecords"] = @$ddd;
            return response()->json($data);
        }else{
            return view('admin.modules.zone_rate.zone_rate_list');
        }
    }

    /*
    * Method        : addZoneRate
    * Description   : This method is used to add zone rate
    * Author        : Argha
    * Date          : 2020-Sep-11
    */

    public function addZoneRate()
    {
        $data['zones'] = ZoneMaster::get();
        return view('admin.modules.zone_rate.add_zone_rate')->with($data);
    }

    /*
    * Method        : insertZoneRate
    * Description   : This method is used to insert zone rate
    * Author        : Argha
    * Date          : 2020-Sep-11
    */

    public function insertZoneRate(Request $request)
    {
        $request->validate([
            'from_weight'=>'required',
            'internal_order_rate'=>'required',
            'external_order_rate'=>'required'
        ],[
            'from_weight'=>'Please provide from weight with 3 decimal places!',
            'internal_order_rate'=>'Please provide internal order rate with 3 decimal places!',
            'external_order_rate'=>'Please provide external order rate with 3 decimal places!'
        ]);
        // $checkZone = ZoneRateMaster::where(['zone_id_1' => $request->zone_id_1,'zone_id_2' => $request->zone_id_2])
        //                             ->orWhere(function($q) use($request){
        //                                 $q->where(['zone_id_1' => $request->zone_id_2,'zone_id_2' => $request->zone_id_1]);
        //                             })
        //                             ->first();
        //dd($checkZone);
        // if($checkZone == null){
        //     $ins_zone['zone_id_1']              = @$request->zone_id_1;
        //     $ins_zone['zone_id_2']              = @$request->zone_id_2;
            
        //     $insert_zone = ZoneRateMaster::create($ins_zone);
        //     $zone_id = @$insert_zone->id;
        // }else{
        //     $zone_id = $checkZone->id;
        // }
        $new['from_weight'] 			    = @$request->from_weight;
        $new['internal_outside_kuwait'] 	= @$request->internal_order_rate;
        $new['external_outside_kuwait'] 	= @$request->external_order_rate;
        $new['zone_id'] 	                = @$request->zone_id_1;
        if(@$request->infinity_weight) {
            $new['infinity_weight']         = 'Y';
        } else {
            $new['to_weight']               = @$request->to_weight;
            $new['infinity_weight']         = 'N';
        }

        $zcost = ZoneRateDetail::create($new);
        if(@$zcost){
            session()->flash("success",__('success.-4094'));
        }else{
            session()->flash("error",__('errors.-5084'));
        }
        return redirect()->route('admin.zone.rate.list');
    }

    /*
    * Method        : editZoneRate
    * Description   : This method is used to add zone rate
    * Author        : Argha
    * Date          : 2020-Sep-11
    */

    public function editZoneRate($id)
    {
        $data['zones'] = ZoneMaster::get();
        $data['zoneRate'] = ZoneRateDetail::with(['getZone'])->where('id',$id)->first();
        //dd($data['zoneRate']);
        return view('admin.modules.zone_rate.edit_zone_rate')->with($data);
    }

    /*
    * Method        : updateZoneRate
    * Description   : This method is used to update zone rate
    * Author        : Argha
    * Date          : 2020-Sep-11
    */

    public function updateZoneRate(Request $request)
    {
        $request->validate([
            'from_weight'=>'required',
            'internal_order_rate'=>'required',
            'external_order_rate'=>'required'
        ],[
            'from_weight'=>'Please provide from weight with 3 decimal places!',
            'internal_order_rate'=>'Please provide internal order rate with 3 decimal places!',
            'external_order_rate'=>'Please provide external order rate with 3 decimal places!'
        ]);

        // $checkZone = ZoneRateMaster::where('id','!=',$request->zone_rate_master_id)
        //                             ->where(['zone_id_1' => $request->zone_id_1,'zone_id_2' => $request->zone_id_2])
        //                             ->orWhere(function($q) use($request){
        //                                 $q->where(['zone_id_1' => $request->zone_id_2,'zone_id_2' => $request->zone_id_1]);
        //                             })
        //                             ->first();
        
        // if($checkZone == null){
        //     $ups_zone['zone_id_1']              = @$request->zone_id_1;
        //     $ups_zone['zone_id_2']              = @$request->zone_id_2;
            
        //     $update_zone = ZoneRateMaster::where('id',$request->zone_rate_master_id)->update($ups_zone);
            
        // }else{
        //     session()->flash("error",__('errors.-5086'));
        //     return redirect()->back();
        // }
        

        $new['from_weight'] 			    = @$request->from_weight;
        $new['internal_outside_kuwait'] 	= @$request->internal_order_rate;
        $new['external_outside_kuwait'] 	= @$request->external_order_rate;
        if(@$request->infinity_weight) {
            $new['infinity_weight']         = 'Y';
        } else {
            $new['to_weight']               = @$request->to_weight;
            $new['infinity_weight']         = 'N';
        }

        $zcost = ZoneRateDetail::where('id',$request->id)->update($new);
        if(@$zcost){
            session()->flash("success",__('success.-4095'));
        }else{
            session()->flash("error",__('errors.-5085'));
        }
        return redirect()->route('admin.zone.rate.list');
    }

    /*
    * Method        : deleteZoneRate
    * Description   : This method is used to delete zone rate
    * Author        : Argha
    * Date          : 2020-Sep-11
    */

    public function deleteZoneRate(Request $request)
    {
        $id = $request['data']['id'];
        $response['jsonrpc'] = '2.0';
        // $checkZoneDetail = ZoneRateDetail::where('id',$id)->get();
        // if(count($checkZoneDetail) == 1){

        //      ZoneRateMaster::where('id',$checkZoneDetail[0]['zone_rate_master_id'])->delete();
        // }
        
        $zone = ZoneRateDetail::where('id',$id)->delete();
        if($zone){
            $response['status'] = 1;
        }else{
            $response['status'] = 0;
        }
        return response()->json($response);
    }

    /*
    * Method        : checkDuplicateRange
    * Description   : This method is used to chake duplicate rate range
    * Author        : Argha
    * Date          : 2020-Sep-11
    */

    public function checkDuplicateRange(Request $request)
    {
        //dd($request->all());
        $response['jsonrpc'] = "2.0";
        if(@$request->all()){
            $req = $request['data'];
            if(@$req['old_id']){
                $checkZone = ZoneRateDetail::where('id','!=',$req['old_id'])
                                            ->where(['zone_id' => $req['from_zone']])
                                            ->first();
            }else{
                $checkZone = ZoneRateDetail::where(['zone_id' => $req['from_zone']])
                                            ->first();
            }
            
            //dd($checkZone);
            $getZoneMasterid = ZoneRateDetail::groupBy('zone_id')->get();
            $allZones = array();
            foreach ($getZoneMasterid as $zones) {
                array_push($allZones,$zones->zone_id);
            }
            if(@$req['old_id']){
                if($checkZone == null){
                    $chk_range = ZoneRateDetail::where('id','!=',$req['old_id'])->whereNotIn('zone_id',$allZones);
                }else{
                    $chk_range = ZoneRateDetail::where('id','!=',$req['old_id'])->where('zone_id',$checkZone->zone_id);
                    
                }
                
            }else{
                if($checkZone == null){
                    $chk_range = ZoneRateDetail::whereNotIn('zone_id',$allZones);
                }else{
                    $chk_range = ZoneRateDetail::where('zone_id',$checkZone->zone_id);
                    //dd($checkZone,$chk_range);
                }
            }

            if(@$req['to_weight'] != 0) {
                if(@$req['old_id']){
                    if($checkZone == null){
                        $chk_infinity = ZoneRateDetail::where('id','!=',$req['old_id'])
                                                    ->whereNotIn('zone_id',$allZones)
                                                    ->where('infinity_weight', 'Y')
                                                    ->first();
                    }else{
                        $chk_infinity = ZoneRateDetail::where('id','!=',$req['old_id'])
                                                    ->where('zone_id',$checkZone->zone_id)
                                                    ->where('infinity_weight', 'Y')
                                                    ->first();
                    }
                    
                }else{
                    if($checkZone == null){
                        
                        $chk_infinity = ZoneRateDetail::where('infinity_weight', 'Y')->whereNotIn('zone_id',$allZones)->first();
                    }else{
                        $chk_infinity = ZoneRateDetail::where('infinity_weight', 'Y')
                                                        ->where('zone_id',$checkZone->zone_id)
                                                        ->first();
                    }
                }
                if($chk_infinity && $chk_infinity['from_weight'] < @$req['to_weight']) {
                    $chk_range = $chk_infinity;
                } else {
                    $chk_range = $chk_range->where(function($where) use ($req) {
                        $where->where(function($where1) use ($req) {
                            $frm_tm = @$req['from_weight'];
                            $to_tm = @$req['to_weight'];

                            $where1->where('from_weight', '<', $frm_tm)
                            ->where('to_weight', '>=', $frm_tm);

                        })
                        ->orWhere(function($where2) use ($req) {
                            $frm_tm = @$req['from_weight'];
                            $to_tm = @$req['to_weight'];

                            $where2->where('from_weight', '<=', $to_tm)
                            ->where('to_weight', '>', $to_tm);
                        })
                        ->orWhere(function($where3) use ($req) {
                            $frm_tm = @$req['from_weight'];
                            $to_tm = @$req['to_weight'];

                            $where3->where('from_weight', $frm_tm)
                            ->where('to_weight', $to_tm);
                        });

                    })->first();
                    ///dd($chk_range);
                    // if(@$req['old_id']){
                    //     //$chk_range = $chk_range->get();
                        
                        
                    // }else{
                    //     $chk_range = $chk_range->where(function($where) use ($req) {
                    //         $where->where(function($where1) use ($req) {
                    //             $frm_tm = @$req['from_weight'];
                    //             $to_tm = @$req['to_weight'];
    
                    //             $where1->where('from_weight', '<', $frm_tm)
                    //             ->where('to_weight', '>=', $frm_tm);
    
                    //         })
                    //         ->orWhere(function($where2) use ($req) {
                    //             $frm_tm = @$req['from_weight'];
                    //             $to_tm = @$req['to_weight'];
    
                    //             $where2->where('from_weight', '<=', $to_tm)
                    //             ->where('to_weight', '>', $to_tm);
                    //         })
                    //         ->orWhere(function($where3) use ($req) {
                    //             $frm_tm = @$req['from_weight'];
                    //             $to_tm = @$req['to_weight'];
    
                    //             $where3->where('from_weight', $frm_tm)
                    //             ->where('to_weight', $to_tm);
                    //         });
    
                    //     })->first();
                    // }
                    
                    
                }
            } else {
                if(@$req['old_id']){
                    if($checkZone == null){
                        $chk_infinity = ZoneRateDetail::where('id','!=',$req['old_id'])
                                                    ->whereNotIn('zone_id',$allZones)
                                                    ->where('infinity_weight', 'Y')
                                                    ->first();
                    }else{
                        $chk_infinity = ZoneRateDetail::where('id','!=',$req['old_id'])
                                                    ->where('zone_id',$checkZone->zone_id)
                                                    ->where('infinity_weight', 'Y')
                                                    ->first();
                    }
                    
                }else{
                    if($checkZone == null){
                        
                        $chk_infinity = ZoneRateDetail::where('infinity_weight', 'Y')->whereNotIn('zone_id',$allZones)->first();
                    }else{
                        $chk_infinity = ZoneRateDetail::where('infinity_weight', 'Y')
                                                        ->where('zone_id',$checkZone->zone_id)
                                                        ->first();
                    }
                }
                if($chk_infinity) {
                    $chk_range = $chk_infinity;
                } else {
                    $chk_range = $chk_range->where(function($where) use ($req) {
                        $where->where(function($where1) use ($req) {
                            $frm_tm = @$req['from_weight'];
                            $where1->where('from_weight', '<=', $frm_tm)
                            ->where('to_weight', '>=', $frm_tm);
                        });
                    })->first();

                    // dd($chk_range);
                }

            }
            if(@$chk_range){
            	$response['status'] =  0;
            }else{
            	$response['status'] =  1;	
            }
        }else{
            $response['status'] =  0;
        }
        return response()->json($response);
    }
    private function checkLoginAccess(){
        if(!@Auth::guard('admin')->user()){
            return redirect()->route('admin.login');
        }
    }
    private function checkMenuAccess(){
    	if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array();
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id);
                    }

                }
                //dd($permission);
                if(in_array(3, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });
        }
    }
}
