<?php

namespace App\Http\Controllers\Admin\Modules\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Models\Country;
use App\Models\MerchantCompanyDescription;
use App\Models\State;
use App\Merchant;
use App\Models\MerchantImage;
use App\Models\MerchantOppening;
use App\Models\MerchantAddressBook;
use Validator;
use Auth;
// use App\Models\CountryDetails;
use App\Models\Language;
use App\Models\Product;
use App\Models\OrderMaster;
use App\Models\OrderDetail;
use App\Models\MerchantRegId;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Repositories\MerchantRepository;
use App\Repositories\DriverRepository;
use App\Repositories\MerchantOppeningRepository;
use App\Repositories\MerchantCompanyDescriptionRepository;
use App\Repositories\MerchantImageRepository;
use App\Repositories\CountryDetailsRepository;
use App\Repositories\AdminRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\CountryRepository;
use App\Repositories\ConversationsRepository;
use App\Models\CountryDetails;
use App\Models\City;
use App\Models\CitiDetails;
use App\Repositories\CityRepository;
use App\Repositories\CitiDetailsRepository;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\ApprovalMail;
use App\Mail\MerchantDetailsMailSend;
use App\Mail\VerificationMail;
use App\Mail\SendMailToMerchant;


class MerchantController extends Controller
{
    protected $merchants,$language,$countries;
    public function __construct(MerchantRepository $merchants,
        LanguageRepository $language,
        MerchantOppeningRepository $merchant_oppening,
        MerchantCompanyDescriptionRepository $merchant_company,
        MerchantImageRepository $merchant_image,
        AdminRepository $admin,
        DriverRepository $drivers,
        CountryRepository $countries,
        ConversationsRepository $conversations,
        CityRepository $city,
        CitiDetailsRepository $city_details,
        Request $request
    )
    {
        $this->checkLoginAccess();
        $this->middleware('admin.auth:admin');
        $this->merchants            =   $merchants;
        $this->language             =   $language;
        $this->countries            =   $countries;
        $this->merchant_company     =   $merchant_company;
        $this->merchant_image       =   $merchant_image;
        $this->merchant_oppening    =   $merchant_oppening;
        $this->admin                =   $admin;
        $this->drivers              =   $drivers;
        $this->conversations        =   $conversations;
        $this->city                 =   $city;
        $this->city_details         =   $city_details;


        
        // dd(auth::guard('admin')->user()->type);
    }

    public function addDriverAuto(Request $request){
        $driver = $this->drivers->where('id',@$request->assign)->first();
        if(@$driver){
            $merchant = Merchant::where('id',@$request->id)->first();
            if(@$merchant){
                $update['driver_id'] = @$request->assign;
                Merchant::where('id',@$request->id)->update($update);
                return 1;
            }else{
                return 0;
            }
            return 1;
        }else{
            return 0;    
        }
        
    }
    public function removeDriverAuto(Request $request){
        $merchant = Merchant::where('id',@$request->id)->first();
        if(@$merchant){
            $update['driver_id'] = 0;
            Merchant::where('id',@$request->id)->update($update);
            return 1;
        }else{
            return 0;
        }
    }

    /*
    * Method        : addMerchant
    * Description   : This method is used to add merchant
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function addMerchant(Request $request){
        $language = Language::where('status','A')->get();
        $countries = Country::with('countryDetailsBylanguage')->where('status','!=','D')->get();
        if(@$request->all()){
            $request->validate([
                'fname'                         =>'required',
                'lname'                         =>'required',
                'email'                         =>'required|email',
                'phone'                         =>'required',
                'company_name'                  =>'required',
                // 'city'                          =>'required',
                'street'                        =>'required',
                'state'                         =>'required',
                'zip'                           =>'required',
                'country'                       =>'required',
                'bank_name'                     =>'required',
                'account_name'                  =>'required',
                'account_number'                =>'required',
                'iban_number'                   =>'required',
                'payment_mode'                  =>'required',
                // 'profile_pic'                   =>'image|mimes:jpeg,jpg,png'
            ],[
                'fname.required'                =>'First name field is required',
                'lname.required'                =>'Last name field is required',
                'email.required'                =>'Email field is required',
                'phone.required'                =>'Phone field is required',
                'company_name.required'         =>'Company name field is required',
                // 'city.required'                 =>'City field is required',
                'street.required'               =>'Street field is required',
                'state.required'                =>'State field is required',
                'zipcode.required'              =>'Zipcode field is required',
                'country.required'              =>'Country field is required',
                'bank_name.required'            =>'Bank name field is required',
                'account_name.required'         =>'Account name field is required',
                'account_number.required'       =>'Account number field is required',
                'iban_number.required'          =>'Iban number field is required',
                'payment_mode.required'         =>'Payment mode field is required',
                // 'profile_pic.image'             =>'Please upload picture with extension jpg/jpeg/png'
            ]);
            $password = time();

            $new['fname']               = @$request->fname;
            $new['lname']               = @$request->lname;
            $new['email']               = @$request->email;
            $new['phone']               = @$request->phone;
            $new['company_name']        = @$request->company_name;

            if(@$request->street){
                $new['address']         = @$request->street;
            }
            $new['country']             = @$request->country;
            if(@$request->country == 134){
                $name_city = $this->city_details->where('name',$request->kuwait_city)->first();
                if(@$name_city){
                    if(@$request->kuwait_city){
                        $new['city']            = @$request->kuwait_city_value_name;
                        $new['city_id']            = @$request->kuwait_city_value_id;
                    }
                }
            }else{
                if(@$request->city){
                    $new['city']            = @$request->city;
                }    
            }
            
            if(@$request->state){
                $new['state']           = @$request->state;
            }
            if(@$request->zip){
                $new['zipcode']         = @$request->zip;
            }
            
            $new['status']              = 'U';
            $new['is_email_verified']   = 'N';
            $vcode = time();
            $chk = Merchant::where('email_vcode',$vcode)->first();
            if(@$chk){
                $vcode = time();
            }
            $new['email_vcode']         = $vcode;
            $password = @$request->nw_password;
            $new['password']            = Hash::make($password);

            if(@$request->email_1){
                $new['email1']  = $request->email_1;
            }
            if(@$request->email_2){
                $new['email2']  = $request->email_2;
            }
            if(@$request->email_3){
                $new['email3']  = $request->email_3;
            }
            if(@$request->profile_pic){
                $pic   = $request->profile_pic;
                $name  = time().'.'.$pic->getClientOriginalExtension();
                $pic->move('storage/app/public/customer/profile_pics/', $name);
                $new['image'] = $name;
            }
            if(@$request->cover_pic){
                $pic_cover   = $request->cover_pic;
                $cover_pic  = time().'.'.$pic_cover->getClientOriginalExtension();
                $pic_cover->move('storage/app/public/cover_pic/', $cover_pic);
                $new['cover_pic'] = $cover_pic;
            }
            if(@$request->bank_name){
                $new['bank_name']  = @$request->bank_name;
            }
            $new['account_name']        = @$request->account_name;
            if(@$request->building_number){
                $new['building_number']  = @$request->building_number;
            }
            if(@$request->address_note){
                $new['address_note']  = nl2br(@$request->address_note);
            }
            if(@$request->avenue){
                $new['avenue']  = @$request->avenue;
            }
            
            $new['account_name']        = @$request->account_name;
            $new['account_number']      = @$request->account_number;
            $new['iban_number']         = @$request->iban_number;
            $new['payment_mode']        = @$request->payment_mode;
            if(@$request->commssion){
                $new['commission']        = @$request->commssion;
            }
            if(@$request->shipping_cost){
                $new['shipping_cost']        = @$request->shipping_cost;
            }
            if(@$request->internal_shipping_cost){
                $new['inside_shipping_cost']        = @$request->internal_shipping_cost;
            }
            $new['location'] = $request->location;
            $new['lat']      = $request->lat;
            $new['lng']      = $request->lng;
            $merchant = Merchant::create($new);

            //slug update
            $fname = str_slug(@$request->fname);
            $lname = str_slug(@$request->lname);
            $slug = @$fname.'-'.@$lname;
            $check_slug = Merchant::where('slug', $slug)->first();
            if(@$check_slug) {
                $slug = $slug.'-'.$merchant->id;
            }
            Merchant::whereId($merchant->id)->update(['slug' => $slug]);
            foreach(@$request->comp_desc as $key=>$desc){
                $cm['language_id'] = $language[$key]->id;
                $cm['merchant_id'] = $merchant->id;
                $cm['description'] = nl2br($desc);
                $merchant_company = $this->merchant_company->create($cm);
            }

            if($request->images) {
                $i=0;
                foreach($request->images as $key=>$file){
                    $imgName   = $i.time().".".$file->getClientOriginalExtension();
                    $file->move('storage/app/public/merchant_portfolio', $imgName);
                    MerchantImage::create([
                        "merchant_id"     => @$merchant->id,
                        "image"           => $imgName
                    ]);
                    $i++;
                    $imgName="";
                }
            }

            if(@$request->day){
                MerchantOppening::where('merchant_id',@$merchant->id)->delete();
                foreach ($request->day as $key => $row) {
                    $day = $row;
                    $from_time = $request->from_time[$key];
                    $to_time   = $request->to_time[$key];

                    $merchantTime = new MerchantOppening;
                    $merchantTime['merchant_id']     =   @$merchant->id;
                    $merchantTime['days']            =   $row;
                    $merchantTime['from_time']       =   $from_time;
                    $merchantTime['to_time']         =   $to_time;
                    $merchantTime->save();
                }
            }
            if(@$merchant){
                $merchantDetails = new \stdClass();
                $merchantDetails->name = @$request->fname." ".@$request->lname; 
                $merchantDetails->fname = @$request->fname; 
                $merchantDetails->email =@$request->email;
                $merchantDetails->id =@$merchant->id;
                $merchantDetails->phone =@$request->phone;
                $merchantDetails->email_vcode =@$merchant->email_vcode;
                $merchantDetails->password =$password;
                Mail::send(new MerchantDetailsMailSend($merchantDetails));
                session()->flash("success",__('success.-4022'));
            }else{
                session()->flash("error",__('errors.-5022'));
            }
            return redirect()->route('admin.list.merchant');
        }
        return view('admin.modules.merchant.add_merchant')->with([
            'language'=>@$language,
            'countries'=>@$countries,
        ]);
    }
    /*
    * Method        : setCommision
    * Description   : This method is used to set commision.
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function setCommision(Request $request){
        $merchant = Merchant::where('id',@$request->id)->first();
        if(@$merchant){
            if(@$request->commission){
                // settype($request->commission,"integer");
                $update['commission'] = $request->commission;
                Merchant::where('id',@$request->id)->update($update);
                $merchant = Merchant::where('id',@$request->id)->first();
                return response()->json($merchant);
            }else{
                return 0;
            }
        }else{

            return 0;
        }
    }
    /*
    * Method        : updateCommision
    * Description   : This method is used to update commision.
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function updateCommision(Request $request){
        $merchant = Merchant::where('id',@$request->merchant_c_id)->first();
        if(@$merchant){
            if(@$request->commission){
                // settype($request->commission,"integer");
                $update['commission'] = $request->commission;
                Merchant::where('id',@$request->merchant_c_id)->update($update);
                session()->flash("success",__('success.-4054'));
                // return 1;
            }else{
                // return 0;
                session()->flash("error",__('errors.-5052'));
            }
        }else{
            session()->flash("error",__('errors.-5052'));
            // return 0;
        }
        return redirect()->back();
    }
    /*
    * Method        : setShippingCost
    * Description   : This method is used to set shipping cost.
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function setShippingCost(Request $request){
        $merchant = Merchant::where('id',@$request->id)->first();
        if(@$merchant){
            if(@$request->shipping_cost){
                // settype($request->commission,"integer");
                $update['shipping_cost'] = $request->shipping_cost;
                $update['inside_shipping_cost'] = $request->internal_shipping_cost;
                Merchant::where('id',@$request->id)->update($update);
                $merchant = Merchant::where('id',@$request->id)->first();
                // session()->flash("success",__('success.-4022'));
                // return redirect()->back();    
                return response()->json($merchant);
            }else{
                return 0;
                // session()->flash("error",__('errors.-5022'));
                // return redirect()->back();
            }
        }else{

            return 0;
        }
    }
    /*
    * Method        : updateShippingCost
    * Description   : This method is used to set shipping cost.
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function updateShippingCost(Request $request){
        // dd($request->all());
        $merchant = Merchant::where('id',@$request->id)->first();
        if(@$merchant){
            if(@$request->shipping_cost){
                // settype($request->commission,"integer");
                $update['shipping_cost'] = $request->shipping_cost;
                $update['inside_shipping_cost'] = $request->internal_shipping_cost;
                Merchant::where('id',@$request->id)->update($update);
                session()->flash("success",__('success.-4053'));
                return redirect()->back();
            }else{
                session()->flash("error",__('errors.-5051'));
                return redirect()->back();
            }
        }else{
            session()->flash("error",__('errors.-5051'));
            return redirect()->back();
        }
    }
    /*
    * Method        : editMerchant
    * Description   : This method is used to edit merchant
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function editMerchant(Request $request,$id){
        $language = Language::where('status','A')->get();
        $countries = Country::with('countryDetailsBylanguage')->where('status','!=','D')->get();
        $state = State::orderBy('name','asc')->get();
        $merchant  = Merchant::with(['merchantCompanyDetails','merchantOpenning','merchantImage','Country','merchantCityDetails'])->where('id',$id)->first();
        if(@$merchant){
            if(@$request->all()){

                $merchant = Merchant::where(['email'=>@$request->email])->where('id','!=',$id)->whereNotIn('status',['D'])->first();
                if(@$merchant){
                    return redirect()->back()->with("error",__('errors.-33000'));
                }
                $merchant = Merchant::where(['phone'=>@$request->phone])->where('id','!=',$id)->whereNotIn('status',['D'])->first();
                if(@$merchant){
                    return redirect()->back()->with("error",__('errors.-33000'));
                }
                $vcode = time();
                if(@$request->fname){
                    $new['fname'] = @$request->fname;
                }
                if(@$request->fname){
                    $new['lname'] = @$request->lname;
                }
                if(@$request->company_name){
                    $new['company_name'] = @$request->company_name; 
                }
                if(@$request->company_type){
                    $new['company_type'] = @$request->company_type; 
                }
                if(@$request->company_details){
                    $new['company_details'] = @$request->company_details; 
                }
                if(@$request->nw_password){
                    if(@$request->confirm_password){
                        $password = @$request->nw_password;
                        $new['password'] = Hash::make($password);    
                    }
                }
                 if(@$request->email){
                    $new['email'] = @$request->email;
                }
                if(@$request->email1){
                    $new['email1'] = @$request->email1;
                }
                if(@$request->street){
                    $new['address']             = @$request->street;
                }
                if(@$request->country){
                    $new['country']             = @$request->country;
                }
                if(@$request->city){
                    $new['city']            = @$request->city;
                } 
                if(@$request->state){
                    $new['state']               = @$request->state;
                }
                if(@$request->zip){
                    $new['zipcode']             = @$request->zip;
                }
                if(@$request->phone){
                    $new['phone']             = @$request->phone;
                }
                $new['city']             = $request->city;
                $new['city_id']             = $request->city;

                $new['email2']  = @$request->email_2;
                $new['email3']  = @$request->email_3;
                if(@$request->profile_pic){
                    $pic   = $request->profile_pic;
                    $name  = time().'.'.$pic->getClientOriginalExtension();
                    $pic->move('storage/app/public/customer/profile_pics/', $name);
                    $new['image'] = $name;
                }

                if(@$request->bank_name){
                    $new['bank_name']           = @$request->bank_name;
                }
                if(@$request->account_name){
                    $new['account_name']        = @$request->account_name;
                }

                if(@$request->account_number){
                    $new['account_number']      = @$request->account_number;
                }

                if(@$request->iban_number){
                    $new['iban_number']         = @$request->iban_number;
                }

                if(@$request->payment_mode){
                    $new['payment_mode']        = @$request->payment_mode;
                }
                if(@$request->commssion || @$request->commssion == 0){
                    $new['commission']        = @$request->commssion;
                }
                if(@$request->shipping_cost || @$request->shipping_cost == 0){
                    $new['shipping_cost']        = @$request->shipping_cost;
                }
                if(@$request->internal_shipping_cost || @$request->internal_shipping_cost == 0){
                    $new['inside_shipping_cost']        = @$request->internal_shipping_cost;
                }
                if(@$request->building_number){
                    $new['building_number']  = @$request->building_number;
                }
                if(@$request->address_note){
                    $new['address_note']  = @$request->address_note;
                }
                if(@$request->avenue){
                    $new['avenue']  = @$request->avenue;
                }

                $new['location'] = $request->location;
                $new['lat']      = $request->lat;
                $new['lng']      = $request->lng;

                if(@$request->cover_pic){
                    $pic_cover   = $request->cover_pic;
                    $cover_pic  = time().'.'.$pic_cover->getClientOriginalExtension();
                    $pic_cover->move('storage/app/public/cover_pic/', $cover_pic);
                    $new['cover_pic'] = $cover_pic;
                }
                Merchant::where('id',$id)->update($new);

                //check and update slug
                $merchantSlug = Merchant::where('id', $id)->first();
                $merchant = Merchant::where('id', $id)->first();
                $fname = str_slug(@$request->fname);
                $lname = str_slug(@$request->lname);
                $new_slug = $fname.'-'.$lname."-".$id;
                $new_slug = $new_slug.'-'.$id;
                    Merchant::whereId($id)->update(['slug' => $new_slug]);
                    $slug = $new_slug;
                if(@$request->comp_desc){

                    $merchant_company = $this->merchant_company->where('merchant_id',$merchant->id)->first();
                    if(@$merchant_company){
                        foreach(@$request->comp_desc as $key=>$desc){
                            $cm_update['description'] = nl2br(@$desc);
                            $this->merchant_company->where(['merchant_id'=>$merchant->id,'language_id'=>$language[$key]->id])->update($cm_update);
                        }
                    }else{
                        foreach(@$request->comp_desc as $key=>$desc){

                            $cm_new['language_id'] = $language[$key]->id;
                            $cm_new['merchant_id'] = $merchant->id;
                            $cm_new['description'] = @$desc;
                            $merchant_company = $this->merchant_company->create($cm_new);

                        }
                    }
                }

                if($request->images) {
                    $i=0;
                    foreach($request->images as $key=>$file){
                        $imgName   = $i.time().".".$file->getClientOriginalExtension();
                        $file->move('storage/app/public/merchant_portfolio', $imgName);
                        MerchantImage::create([
                            "merchant_id"     => $id,
                            "image"           => $imgName
                        ]);
                        $i++;
                        $imgName="";
                    }
                }
                session()->flash("success",__('success.-4023'));
                return redirect()->back();
            }
        }else{

            session()->flash("error",__('errors.-5002'));
            return redirect()->back();
        }

        return view('admin.modules.merchant.edit_merchant_details')->with([
            'language'=>@$language,
            'countries'=>@$countries,
            'state'=>@$state,
            'merchant'=>@$merchant
        ]);
    }
    
    public function sendMessage(Request $request,$id){
        // $merchant = Merchant::where('id',$id)->first();
        // if(@$merchant){
        //     if(@$request->title){
        //         if(@$request->messages){
        //             $new['user_type'] = 'M';
        //             $new['send_by'] = @Auth::guard('admin')->user()->id;
        //             $new['conversation_id'] = time();
        //             $new['email_id'] = @$merchant->email;
        //             $new['title'] = @$request->title;
        //             $new['messages'] = @$request->messages;
        //             $new['name'] = @$merchant->fname." ".@$merchant->lname;
        //             $new['send_to'] = @$merchant->id;
        //             $conversation = $this->conversations->create($new);

        //             $merchantDetails            =   new \stdClass();
        //             $merchantDetails->name      =   @$merchant->fname." ".@$merchant->lname; 
        //             $merchantDetails->fname     =   @$merchant->fname; 
        //             $merchantDetails->email     =   @$merchant->email;
        //             $merchantDetails->title     =   @$request->title;
        //             $merchantDetails->message   =   @$request->messages;
        //             Mail::send(new SendMailToMerchant($merchantDetails));
        //             session()->flash("success",__('success.-4027'));
        //         }
        //     }
        // }else{
        //     session()->flash("error",__('errors.-5025'));
        // }
        // return redirect()->back();
    }
    /*
    * Method        : sendNotification
    * Description   : This method is used to send Notification.
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function sendNotification($id,Request $request){
        // $driver = Merchant::where('id',$id)->first();
        // if(@$driver){
        //     $merchantDetails            =   new \stdClass();
        //     $merchantDetails->name      =   $driver->name; 
        //     $merchantDetails->fname     =   $driver->fname; 
        //     $merchantDetails->email     =   $driver->email;
        //     $merchantDetails->title     =   $request->title;
        //     $merchantDetails->message   =   $request->message;
        //     Mail::send(new SendMailToMerchant($merchantDetails));
        //     session()->flash("success",__('success.-4036'));
        // }else{
        //     session()->flash("error",__('errors.-5032'));
        // }
        // return redirect()->back();
    }
    /*
    * Method        : viewMerchantProfile
    * Description   : This method is used to view Merchant Profile
    */
    public function viewMerchantProfile(Request $request,$id){
        $language = Language::where('status','A')->get();
        $merchant  = Merchant::with(['merchantCompanyDetails','merchantOpenning','merchantImage','State','merchantCityDetails','merchantOrderDetails'])->where('id',$id)->first();
        $orderss = OrderMaster::with('orderMasterDetails')->where(['status'=>'OD','is_review_done'=>'Y']);
        $orders = $orderss->whereHas('orderMasterDetails', function($q) use ($request,$id){
                    $q->where('seller_id',$id);
                });
        $order = $orders->orderBy('id','desc')->get();
        return view('admin.modules.merchant.view_merchant_profile')->with([
            'merchant'=>@$merchant,
            'order'=>@$order
        ]);
    }
    /*
    * Method        : manageMerchant
    * Description   : This method is used to manage merchant
    */
    public function manageMerchant(Request $request){
        $drivers = $this->drivers->where(['status' => 'A'])->get(); 
        // $language = Language::where('status','A')->get();
        $merchants = Merchant::with('DriverDetails')->where('status','!=','D');
        $mm = $merchants;
        $mm = $mm->count(); // total merchant before filter

        if(@$request->all()) {
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

            if(@$request->columns['1']['search']['value']){
                // dd(@$request->keyword);
                $merchants = $merchants->where(function($query) use($request){
                    $query->Where('fname','like','%'.@$request->columns['1']['search']['value'].'%')
                    ->orWhere('lname','like','%'.@$request->columns['1']['search']['value'].'%')
                    ->orWhere('email','like','%'.@$request->columns['1']['search']['value'].'%')
                    ->orWhere('phone','like','%'.@$request->columns['1']['search']['value'].'%')
                    ->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".@$request->columns['1']['search']['value']."%");
                });
            }

            if(@$request->columns['2']['search']['value']){
                $merchants = $merchants->where(function($query) use($request){
                    $query->Where('status', $request->columns['2']['search']['value']);
                });
            }
            if(@$request->columns['8']['search']['value']){
                $date = explode(',', $request->columns['8']['search']['value']);
                $from_date = $date[0];
                $to_date = $date[1];
                $merchants = $merchants->whereBetween('created_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);
            }
            $mmm = $merchants;
            $mmm = $mmm->count(); // count before paginate
            $limit = $request->length;
            // sorting

            if($columnIndex == 3) {
                $merchants = $merchants->get()->toArray();
                uasort($merchants, function($a, $b) use ($columnSortOrder){
                    $c1 = @$a['driver_details']['fname'].' '.$a['driver_details']['lname'];
                    $c2 = @$b['driver_details']['fname'].' '.$b['driver_details']['lname'];
                    $res = strcmp($c1, $c2);
                    if($res == 0) {
                        return 0;
                    } elseif($res < 0) {
                        return $columnSortOrder == 'asc' ? -1 : 1;
                    } else {
                        return $columnSortOrder == 'asc' ? 1 : -1;
                    }
                });

                $merchants = array_slice($merchants, $request->start, $limit);
            } else {
                if($columnIndex == 0) {                    
                    if($columnSortOrder == 'asc') {
                        $merchants = $merchants->orderBy('fname','asc');
                    } else {
                        $merchants = $merchants->orderBy('fname','desc');
                    }
                }
                if($columnIndex == 1) {
                    if($columnSortOrder == 'asc') {
                        $merchants = $merchants->orderBy('email','asc');
                    } else {
                        $merchants = $merchants->orderBy('email','desc');
                    }
                }
                if($columnIndex == 2) {
                    if($columnSortOrder == 'asc') {
                        $merchants = $merchants->orderBy('phone','asc');
                    } else {
                        $merchants = $merchants->orderBy('phone','desc');
                    }
                }
                
                if($columnIndex == 3) {
                    if($columnSortOrder == 'asc') {
                        $merchants = $merchants->orderBy('rate','asc');
                    } else {
                        $merchants = $merchants->orderBy('rate','desc');
                    }
                }
                if($columnIndex == 4) {
                    if($columnSortOrder == 'asc') {
                        $merchants = $merchants->orderBy('total_earning','asc');
                    } else {
                        $merchants = $merchants->orderBy('total_earning','desc');
                    }
                }
                if($columnIndex == 5) {
                    if($columnSortOrder == 'asc') {
                        $merchants = $merchants->orderBy('total_paid','asc');
                    } else {
                        $merchants = $merchants->orderBy('total_paid','desc');
                    }
                }
                if($columnIndex == 6) {
                    if($columnSortOrder == 'asc') {
                        $merchants = $merchants->orderBy('total_due','asc');
                    } else {
                        $merchants = $merchants->orderBy('total_due','desc');
                    }
                }
                if($columnIndex == 7) {
                    if($columnSortOrder == 'asc') {
                        $merchants = $merchants->orderBy('hide_merchant','asc');
                    } else {
                        $merchants = $merchants->orderBy('hide_merchant','desc');
                    }
                }
                if($columnIndex == 8) {
                    if($columnSortOrder == 'asc') {
                        $merchants = $merchants->orderBy('created_at','asc');
                    } else {
                        $merchants = $merchants->orderBy('created_at','desc');
                    }
                }
                if($columnIndex == 9) {
                    if($columnSortOrder == 'asc') {
                        $merchants = $merchants->orderBy('status','asc');
                    } else {
                        $merchants = $merchants->orderBy('status','desc');
                    }
                }
                $merchants = $merchants->skip($request->start)->take($request->length)->get();
            }
            $data['aaData'] = $merchants;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $mm;
            $data["iTotalDisplayRecords"] = @$mmm;
            return response()->json($data);
        } else {
            $merchants =$merchants->where('status','!=','D')->orderBy('id','desc')->get();
            return view('admin.modules.merchant.manage_merchant')->with([
                'merchants' => @$merchants,
                'drivers'   => @$drivers
            ]);
        }
    }
    //searchMerchants function is not in use
    public function searchMerchants(Request $request){
        $merchants = Merchant::where('id','!=',0);
        if(@$request->all()) {
            if(@$request->status){

                $merchants = $merchants->where(function($query) use($request){
                    $query->Where('status',$request->status);
                });
                
            }
            if(@$request->keyword){

                $merchants = $merchants->where(function($query) use($request){
                    $query->Where('fname','like','%'.$request->keyword.'%')
                    ->orWhere('lname','like','%'.$request->keyword.'%')
                    ->orWhere('email','like','%'.$request->email.'%')
                    ->orWhere('phone','like','%'.$request->keyword.'%')
                    ->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".$request->keyword."%");
                });
            }

            

            $merchants =$merchants->orderBy('id','desc')->get();
            // dd($merchants);
            return view('admin.modules.merchant.ajax_manage_merchant')->with([
                'merchants'=>@$merchants
            ]);
        }
    }
    /*
    * Method        : sendEmailNotification
    * Description   : This method is used to send Email Notification
    */
    public function sendEmailNotification(Request $request){
        $merchant = Merchant::where('id',$request->id)->first();
        if(@$merchant){
            $new['user_type'] = 'M';
            $new['send_by'] = @Auth::guard('admin')->user()->id;
            $new['conversation_id'] = time();
            $new['email_id'] = @$request->email;
            $new['title'] = @$request->title;
            $new['messages'] = @$request->message;
            $new['name'] = @$request->name;
            $new['send_to'] = @$merchant->id;
            $conversation = $this->conversations->create($new);
            
            $merchantDetails = new \stdClass();
            $merchantDetails->name = @$request->name; 
            $merchantDetails->fname = @$request->name; 
            $merchantDetails->email =@$request->email;
            $merchantDetails->title =@$request->title;
            $merchantDetails->message =@$request->message;
            Mail::send(new SendMailToMerchant($merchantDetails));
            return 1;
        }else{
            return 0;
        }
    }
    
    public function changeEmailId(Request $request){
        $merchant = Merchant::where('email','!=',@$request->old_email);
        $merchant = $merchant->where(['email'=>@$request->email])->first();
        if(@$merchant){
            return 0;
        }else{
            return 1;
        }
    }
    public function changePhnNo(Request $request){
        $merchant = Merchant::where('phone','!=',@$request->old_phone);
        $merchant = $merchant->where(['phone'=>@$request->phone])->first();
        if(@$merchant){
            return 0;
        }else{
            return 1;
        }
    }
    public function changePhone(Request $request){
        $merchant = Merchant::where('phone','!=',@$request->old_phone);
        $merchant = $merchant->where(['email'=>@$request->phone])->first();
        if(@$merchant){
            return 0;
        }else{
            return 1;
        }
    }
    /*
    * Method        : changeStatus ( block or unblock merchant)
    * Description   : This method is used to changeStatus
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function changeStatus($id){
        $this->checkLoginAccess();
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(4, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
        $merchant = Merchant::where('id',$id)->first();
        if(@$merchant){
            if($merchant->status == 'A'){
                $update['status'] = 'I';
            }elseif($merchant->status == 'I'){
                $update['status'] = 'A';
            }
            Merchant::where('id',$id)->update($update);
            session()->flash("success",__('success.-4024'));
            
        }else{
            session()->flash("error",__('errors.-5002'));
        }
        return redirect()->back();
    }
    /*
    * Method        : changeStatus ( block or unblock merchant)
    * Description   : This method is used to changeStatus
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function  blockUnblockMerchant(Request $request){
        $this->checkLoginAccess();
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(4, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
        $merchant = Merchant::where('id',$request->id)->first();
        if(@$merchant){
            if($merchant->status == 'A'){
                $update['status'] = 'I';
            }elseif($merchant->status == 'I'){
                $update['status'] = 'A';
            }
            Merchant::where('id',$request->id)->update($update);
            $merchant = Merchant::where('id',$request->id)->first();
            return response()->json($merchant);
            // session()->flash("success",__('success.-4024'));
            
        }else{
            // session()->flash("error",__('errors.-5002'));
            return 0;
        }
        // return redirect()->back();
    }
    /*
    * Method        : changeStatus ( block or unblock merchant)
    * Description   : This method is used to changeStatus
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function  hideShowMerchant(Request $request){
        $this->checkLoginAccess();
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(4, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
        $merchant = Merchant::where('id',$request->id)->first();
        if(@$merchant){
            if($merchant->hide_merchant == 'Y'){
                $update['hide_merchant'] = 'N';
                $update_product['product_hide'] = 'N';
            }elseif($merchant->hide_merchant == 'N'){
                $update['hide_merchant'] = 'Y';
                $update_product['product_hide'] = 'Y';
            }
            Merchant::where('id',@$request->id)->update($update);

            Product::where('user_id',@$request->id)->update($update_product);

            $merchant = Merchant::where('id',$request->id)->first();
            return response()->json($merchant);
            // session()->flash("success",__('success.-4024'));
            
        }else{
            // session()->flash("error",__('errors.-5002'));
            return 0;
        }
        // return redirect()->back();
    }
    /*
    * Method        : verifyEmailAccount
    * Description   : This method is used to verify email-id account
    * Author        : Jayatri
    * Date          : 31/01/2020
    */
    public function verifyEmailAccount($id){
        $merchant = Merchant::where('id',$id)->first();
        if(@$merchant){
            $update['status'] = 'A';
            $update['is_email_verified'] = 'Y';
            $update['email_vcode'] = 'null';
            Merchant::where('id',$id)->update($update);
            session()->flash("success",__('success.-4056'));
        }else{
            session()->flash("error",__('errors.-5053'));
        }
        return redirect()->back();

    }
    /*
    * Method        : verifyMerchantEmailByAdmin
    * Description   : This method is used to verify email-id account
    * Author        : Jayatri
    * Date          : 11/03/2020
    */
    public function verifyMerchantEmailByAdmin(Request $request){
        $id = $request->id;
        $merchant = Merchant::where('id',$id)->first();
        if(@$merchant){
            $update['status'] = 'A';
            $update['is_email_verified'] = 'Y';
            $update['email_vcode'] = 'null';
            Merchant::where('id',$id)->update($update);
            // session()->flash("success",__('success.-4056'));
            return 1;
        }else{
            // session()->flash("error",__('errors.-5053'));
            return 0;
        }
        // return redirect()->back();

    }
    /*
    * Method        : manageMerchant
    * Description   : This method is used to manage merchant
    * Author        : Jayatri
    * Date          : 31/01/2020
    */
    public function approveMerchant($id){
        $this->checkLoginAccess();
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(4, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
        $merchant = Merchant::where('id',$id)->first();
        if(@$merchant){
            
                $update['status'] = 'A';
            
            Merchant::where('id',$id)->update($update);
            $merchant = Merchant::where('id',$id)->first();
            try{
                $mailDetails = new \StdClass();
                $mailDetails->to    = @$merchant->email;
                $mailDetails->fname = @$merchant->fname;
                $mailDetails->id    = @$merchant->id;
                $mailDetails->user = @$merchant;
                Mail::send(new ApprovalMail($mailDetails));
            }catch(\Exception $e){

            }
            session()->flash("success",__('success.-4025'));
            
        }else{
            session()->flash("error",__('errors.-5002'));
        }
        return redirect()->back();
    }
    /*
    * Method        : manageMerchant
    * Description   : This method is used to manage merchant
    * Author        : Jayatri
    * Date          : 31/01/2020
    */
    public function approveMerchantByAdmin(Request $request){
        $this->checkLoginAccess();
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(4, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }

        $merchant = Merchant::where('id',@$request->id)->first();
        if(@$merchant){
            
                $update['status'] = 'A';
            
            Merchant::where('id',@$request->id)->update($update);
            $merchant = Merchant::where('id',@$request->id)->first();
            try{
                $mailDetails = new \StdClass();
                $mailDetails->to    = @$merchant->email;
                $mailDetails->fname = @$merchant->fname;
                $mailDetails->id    = @$merchant->id;
                $mailDetails->user = @$merchant;
                Mail::send(new ApprovalMail($mailDetails));
            }catch(\Exception $e){

            }
            // session()->flash("success",__('success.-4025'));
            return 1;
            
        }else{
            // session()->flash("error",__('errors.-5002'));
            return 0;
        }
        // return redirect()->back();
    }
    /*
    * Method        : premiumMerchant
    * Description   : This method is used to make premium Merchant or not.
    * Author        : Jayatri
    * Date          : 31/01/2020
    */
    public function premiumMerchant($id){
        $this->checkLoginAccess();
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(4, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
        $merchant = Merchant::where('id',$id)->first();
        if(@$merchant){
            if($merchant->premium_merchant == 'Y'){
                $update['premium_merchant'] = 'N';
            }
            elseif($merchant->premium_merchant == 'N'){
                $update['premium_merchant'] = 'Y';
            }
            Merchant::where('id',$id)->update($update);
            session()->flash("success",__('success.-4024'));
            
        }else{
            session()->flash("error",__('errors.-5002'));
        }
        return redirect()->back();
    }
    /*
    * Method        : premiumMerchant
    * Description   : This method is used to make premium Merchant or not.
    * Author        : Jayatri
    * Date          : 31/01/2020
    */
    public function premiumMerchantStatus(Request $request){
        $this->checkLoginAccess();
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(4, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
        $merchant = Merchant::where('id',$request->id)->first();
        if(@$merchant){
            if($merchant->premium_merchant == 'Y'){
                $update['premium_merchant'] = 'N';
            }
            elseif($merchant->premium_merchant == 'N'){
                $update['premium_merchant'] = 'Y';
            }
            Merchant::where('id',$request->id)->update($update);
            // session()->flash("success",__('success.-4024'));
            $merchant = Merchant::where('id',$request->id)->first();
            return response()->json(@$merchant);
        }else{
            // session()->flash("error",__('errors.-5002'));
            return 0;
        }
        // return redirect()->back();
    }
    /*
    * Method        : internationOrderStatus
    * Description   : This method is used to internation Order Status.
    * Author        : Jayatri
    * Date          : 31/01/2020
    */
    public function internationOrderStatus($id){
        $this->checkLoginAccess();
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(4, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
        $merchant = Merchant::where('id',$id)->first();
        if(@$merchant){
            if($merchant->international_order == 'ON'){
                $update['international_order'] = 'OFF';
            }
            elseif($merchant->international_order == 'OFF'){
                $update['international_order'] = 'ON';
            }else{
                $update['international_order'] = 'ON';
            }
            Merchant::where('id',$id)->update($update);
            session()->flash("success",__('success.-4057'));
        }else{
            session()->flash("error",__('errors.-5054'));
        }
        return redirect()->back();
    }
    /*
    * Method        : internationOrderStatusChange
    * Description   : This method is used to internation Order Status. by ajax
    * Author        : Jayatri
    * Date          : 31/01/2020
    */
    public function internationOrderStatusChange(Request $request){
        $this->checkLoginAccess();
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(4, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
        // $id = @$request->id;
        $merchant = Merchant::where('id', @$request->id)->first();
        if(@$merchant){
            if($merchant->international_order == 'ON'){
                $update['international_order'] = 'OFF';
            }
            elseif($merchant->international_order == 'OFF'){
                $update['international_order'] = 'ON';
            }else{
                $update['international_order'] = 'ON';
            }
            Merchant::where('id', @$request->id)->update($update);
            $merchant = Merchant::where('id', @$request->id)->first();
            return response()->json($merchant);
            // session()->flash("success",__('success.-4057'));
        }else{
            // session()->flash("error",__('errors.-5054'));
            return 0;
        }
        // return redirect()->back();
    }
    /*
    * Method        : deleteMerchant
    * Description   : This method is used to delete Merchant
    * Author        : Jayatri
    * Date          : 31/01/2020
    */
    public function deleteMerchant($id){
        $this->checkLoginAccess();
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(4, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
        $merchant = Merchant::where('id',$id)->first();
        if(@$merchant){
            $product = Product::where('user_id',$merchant->id);
            $product = $product->where(function($q1) {
                $q1 = $q1->where('status','!=','D');
            });
            $product = $product->first();
            // if(!@$product){
            $is_order_exist = OrderDetail::where('seller_id',$merchant->id);
            $is_order_exist = $is_order_exist->where(function($q1) {
                $q1 = $q1->where('status','!=','D');
            });
            $is_order_exist = $is_order_exist->first();
            // }
            if(@$product){
                session()->flash("error",__('errors.-5034'));
            }elseif(@$is_order_exist){
                session()->flash("error",__('errors.-5034'));
            }else{

                $updateRegId['user_id'] = $id;
                $updateRegId['reg_id'] = '';
                MerchantRegId::where(['user_id'=>$id])->update($updateRegId);

                $update['status'] = 'D';
                $update['firebaseToken_id'] = '';
                // $this->merchant_company->where('merchant_id',$id)->update['status'];
                // $this->merchant_oppening->where('merchant_id',$id)->();
                // $portfolio = $this->merchant_image->where('merchant_id',$id)->first();
                // if(@$portfolio->image){
                //     $image_path = '/storage/app/public/merchant_portfolio/'.$portfolio->image; 
                //     if (file_exists(@$image_path)){
                //         unlink(@$image_path);
                //     }
                // }
                // $this->merchant_image->where('merchant_id',$id)->delete();
                // if(@$merchant->image){
                //     $image_path = '/storage/app/public/profile_pics/'.$merchant->image; 
                //     if (file_exists(@$image_path)){
                //         unlink(@$image_path);
                //     }
                // }
                Merchant::where('id',$id)->update($update);

                
                session()->flash("success",__('success.-4026'));
            }
            
            
        }else{
            session()->flash("error",__('errors.-5002'));
        }
        return redirect()->back();
    }
    /*
    * Method        : deleteMerchantPermanently
    * Description   : This method is used to delete Merchant
    * Author        : Jayatri
    * Date          : 31/01/2020
    */
    public function deleteMerchantPermanently(Request $request){
        $this->checkLoginAccess();
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(4, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
        $id = $request->id;
        $merchant = Merchant::where('id',$id)->first();
        if(@$merchant){
            $product = Product::where('user_id',$merchant->id);
            // if(!@$product){
            $product = $product->where(function($q1) use($request) {
                    $q1 = $q1->where('status','!=','D');
                });
            $product = $product->first();
            $is_order_exist = OrderDetail::with('orderMaster')->where('seller_id',$merchant->id);
            $is_order_exist = $is_order_exist->whereHas('orderMaster',function($q1) use($request) {
                    $q1 = $q1->where('status','!=','D');
                });
            $is_order_exist = $is_order_exist->whereHas('orderMaster',function($q2) use($request) {
                    $q2 = $q2->whereNotIn('status',['D','I','']);
                });
            $is_order_exist = $is_order_exist->first();
            // }
            if(@$product){
                return 0;
            }
            // elseif(@$is_order_exist){
            //     return 0;
            // }
            else{
                $update['status'] = 'D';
                Merchant::where('id',$id)->update($update);
                return 1;
            }
            
            
        }else{
            return 0;
        }
    }
    private function checkLoginAccess(){
        $this->middleware('admin.auth:admin');
        if(!@Auth::guard('admin')->user()){
            return redirect()->route('admin.login');
        }
    }
    
    /*
    * Method: removeImg
    * Description: It will delete from merchant_to_images table in ajax
    * Author: Surajit 
    */
    public function removeImg($id){

        $status = '';
        $response = [
            'jsonrpc'   => '2.0'
        ];      
        MerchantImage::whereId($id)->delete();
        return response()->json(['success' => 'success']);
    }

    /*
    * Method        : changeProcessedOrderStatus
    * Description   : This method is used to show/hide processed order text for frontend merchant profile
    * Author        : Argha
    * Date          : 2020-Aug-27
    */
    // public function changeProcessedOrderStatus(Request $request)
    // {
    //     if($request->status == 'h'){
    //         $up = Merchant::where('id',$request->id)->update(['show_order_processed'=>'N']);
    //     }else{
    //         $up = Merchant::where('id',$request->id)->update(['show_order_processed'=>'Y']);
    //     }
    //     $resp['res'] = $up;
    //     $resp['status'] = $request->status;
    //     return response()->json($resp);
    // }

    /*
    * Method        : editMerchantSetting
    * Description   : This method is used to edit merchant setting
    * Author        : Argha
    * Date          : 2020-Aug-29
    */

    public function editMerchantSetting(Request $request)
    {
        
        if(@$request->minimum_commission){
            $update['minimum_commission'] = @$request->minimum_commission;
        }

        if(@$request->show_order_processed){
            $update['show_order_processed'] = 'Y';
        }else{
            $update['show_order_processed'] = 'N';
        }

        if(@$request->hide_customer_info){
            $update['hide_customer_info'] = 'Y';
        }else{
            $update['hide_customer_info'] = 'N';
        }

        if(@$request->notify_merchant_on_delivery_internal){
            $update['notify_merchant_on_delivery_internal'] = 'Y';
        }else{
            $update['notify_merchant_on_delivery_internal'] = 'N';
        }

        if(@$request->notify_merchant_on_delivery_external){
            $update['notify_merchant_on_delivery_external'] = 'Y';
        }else{
            $update['notify_merchant_on_delivery_external'] = 'N';
        }

        if(@$request->notify_customer_by_email_internal){
            $update['notify_customer_by_email_internal'] = 'Y';
        }else{
            $update['notify_customer_by_email_internal'] = 'N';
        }

        if(@$request->notify_customer_by_email_external){
            $update['notify_customer_by_email_external'] = 'Y';
        }else{
            $update['notify_customer_by_email_external'] = 'N';
        }

        if(@$request->notify_customer_by_whatsapp_internal){
            $update['notify_customer_by_whatsapp_internal'] = 'Y';
        }else{
            $update['notify_customer_by_whatsapp_internal'] = 'N';
        }

        if(@$request->notify_customer_by_whatsapp_external){
            $update['notify_customer_by_whatsapp_external'] = 'Y';
        }else{
            $update['notify_customer_by_whatsapp_external'] = 'N';
        }

        if(@$request->applicable_city_delivery){
            $update['applicable_city_delivery'] = 'Y';
        }else{
            $update['applicable_city_delivery'] = 'N';
        }

        if(@$request->applicable_city_delivery_external){
            $update['applicable_city_delivery_external'] = 'Y';
        }else{
            $update['applicable_city_delivery_external'] = 'N';
        }
        $updateStat = Merchant::where('id',$request->merchant_setting_id)->update($update);

        $resp['res'] = $updateStat;
        return response()->json($resp);
    }

    /*
    * Method        : getMerchantSetting
    * Description   : This method is used get the merchant settings
    * Author        : Argha
    * Date          : 2020-Aug-29
    */
    public function getMerchantSetting(Request $request)
    {
        
        $id = $request['data']['id'];
        $merchantSetting = Merchant::where('id',$id)->first();
        $resp['jsonrpc'] = "2.0";
        $resp['result'] = $merchantSetting;
        return response()->json($resp);
    }

    /*
    * Method        : showAddressbook
    * Description   : This method is used get the merchant settings
    * Author        : Argha
    * Date          : 2020-SEP-29
    */
    public function showAddressbook(Request $request)
    {
        
        $id = $request->id;
        $address = MerchantAddressBook::with(['countryDetailsBylanguage','getCityNameByLanguage'])->where('merchant_id',$id)->orderBy('id','desc');
        //dd($address); 
        if($request->all()){
            
            //dd($request->all());
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

          //sorting
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $address = $address->orderBy('block','asc');
                } else {
                    $address = $address->orderBy('block','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $address = $address->orderBy('street','asc');
                } else {
                    $address = $address->orderBy('street','desc');
                }
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $address = $address->orderBy('postal_code','asc');
                } else {
                    $address = $address->orderBy('postal_code','desc');
                }
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $address = $address->orderBy('building_no','asc');
                } else {
                    $address = $address->orderBy('building_no','desc');
                }
            }
            if($columnIndex == 6) {
                if($columnSortOrder == 'asc') {
                    $address = $address->orderBy('more_address','asc');
                } else {
                    $address = $address->orderBy('more_address','desc');
                }
            }

            $address = $address->skip($request->start)->take($request->length)->get();
            $data['aaData'] = $address;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = count($address);
            $data["iTotalDisplayRecords"] = count($address);
            return response()->json($data);
        }else{

            return view('admin.modules.merchant.show_addressbook');
        }
    }

    /*
    * Method        : addMerchantAddress
    * Description   : This method is used to add merchant addressbook
    * Author        : Argha
    * Date          : 2020-OCT-13
    */

    public function addMerchantAddress($id)
    {
        $data['cities'] = City::get();
		$country        = Country::with('countryDetailsBylanguage');
		$country        = $country->where(function($q1) {
                                $q1 = $q1->where('status', '!=', 'I');
                            });
        $country        = $country->where(function($q1) {
                                $q1 = $q1->where('status','!=','D');
                            });
        $data['countries'] = $country->orderBy('id','desc')->get();
        return view('admin.modules.merchant.add_address_book')->with($data);
    }
    
    /*
    * Method        : insertMerchantAddress
    * Description   : This method is used to insert merchant addressbook
    * Author        : Argha
    * Date          : 2020-OCT-13
    */
    public function insertMerchantAddress(Request $request)
    {
        $request->validate([
            'country' => 'required',
            'street'  => 'required'
       ]);

       if($request->kuwait_city != null){
            $city = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
            $city = $city->whereHas('cityDetailsByLanguage',function($query) use($request) {
                $query->where('name', 'LIKE', "%{$request->kuwait_city}%");
            })->get();
            if(!$city) {
                session()->flash("error",__('success_user.-722'));
                return redirect()->back();		
            }
            $city_id = $city[0]->id;
            $city = $city[0]->cityDetailsByLanguage->name;
       }else{
            $city = @$request->city;
       }
       
       if($request->is_default == 'Y'){
            $checkDefault = MerchantAddressBook::where('is_default',1)->first();
            if($checkDefault != null){
                $checkDefault = MerchantAddressBook::where('id',$checkDefault->id)->update(['is_default'=>0]);
            }
            $default = 'Y';
       }else{
            $default = 'N';
       }
       $new['merchant_id'] = $request->merchant_id;
       $new['country'] = $request->country;
       $new['city'] = @$city;
       $new['city_id'] = @$city_id;
       $new['building_no'] = @$request->building;
       $new['street'] = @$request->street;
       $new['location'] = @$request->location;
       $new['postal_code'] = @$request->zip;
       $new['block'] = @$request->block;
       $new['lat'] = @$request->lat;
       $new['lng'] = @$request->lng;
       $new['phone'] = @$request->phone;
       $new['more_address'] = @$request->address;
       $new['is_default'] = @$default;
       //dd($new);
       MerchantAddressBook::create($new);

       session()->flash("success",__('success_user.-702'));

       return redirect()->back();
    }

    /*
    * Method        : editMerchantAddress
    * Description   : This method is used show edit page
    * Author        : Argha
    * Date          : 2020-OCT-13
    */
    public function editMerchantAddress($id,$merchant_id)
    {
        $data['cities'] = City::get();
		$country        = Country::with('countryDetailsBylanguage');
		$country        = $country->where(function($q1) {
                                $q1 = $q1->where('status', '!=', 'I');
                            });
        $country        = $country->where(function($q1) {
                                $q1 = $q1->where('status','!=','D');
                            });
        $data['countries'] = $country->orderBy('id','desc')->get();
        $data['address']   = MerchantAddressBook::with('getCityNameByLanguage')->where('id',$id)->first();
        return view('admin.modules.merchant.edit_address_book')->with($data);
    }

    /*
    * Method        : updateMerchantAddress
    * Description   : This method is used update address
    * Author        : Argha
    * Date          : 2020-OCT-13
    */
    public function updateMerchantAddress(Request $request)
    {
        $request->validate([
            'country' => 'required',
            'street'  => 'required'
       ]);

       if($request->kuwait_city != null){
            $city = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
            $city = $city->whereHas('cityDetailsByLanguage',function($query) use($request) {
                $query->where('name', 'LIKE', "%{$request->kuwait_city}%");
            })->get();
            //dd($city);
            if(!$city) {
                session()->flash("error",__('success_user.-722'));
                return redirect()->back();		
            }
            $city_id = $city[0]->id;
            $city = $city[0]->cityDetailsByLanguage->name;
        }else{
             $city = @$request->city;
        }
       
       if($request->is_default == 'Y'){
        $checkDefault = MerchantAddressBook::where('is_default',1)->where('id','!=',$request->id)->first();
        if($checkDefault != null){
            $checkDefault = MerchantAddressBook::where('id',$checkDefault->id)->update(['is_default'=>0]);
        }
            $default = 'Y';
       }else{
            $default = 'N';
       }
       $up['merchant_id'] = $request->merchant_id;
       $up['country'] = $request->country;
       $up['city'] = @$city;
       $up['city_id'] = @$city_id;
       $up['building_no'] = @$request->building;
       $up['street'] = @$request->street;
       $up['location'] = @$request->location;
       $up['postal_code'] = @$request->zip;
       $up['block'] = @$request->block;
       $up['lat'] = @$request->lat;
       $up['lng'] = @$request->lng;
       $up['phone'] = @$request->phone;
       $up['more_address'] = @$request->address;
       $up['is_default'] = @$default;

       MerchantAddressBook::where('id',$request->id)->update($up);

       session()->flash("success",__('success_user.-704'));

       return redirect()->back();
    }

    /*
    * Method        : deleteMerchantAddress
    * Description   : This method is used to delete address
    * Author        : Argha
    * Date          : 2020-OCT-13
    */
    public function deleteMerchantAddress(Request $request)
    {
        $response['jsonrpc'] = "2.0";
        $id = $request['data']['id'];
        $delete = MerchantAddressBook::where('id',$id)->delete();
        if($delete){
            $response['status'] = "1";
        }else{
            $response['status'] = "0";
        }
        return response()->json($response);
    }

    public function getCity($stateid){
        $cities = City::where('state_id',$stateid)->where('status','A')->get();
        $result = '<option value="">Select City</option>';
        if (!empty($cities)) {
            foreach ($cities as $city) {
                // if(@$merchantcity == @$city->id){
                //     $select = "selected";
                // }
                // else{
                    $select = "";
                // }
               $result .= '<option value='.@$city->id.' '.@$select.'>'.@$city->name.'</option>';
            }
        }
        return $result;
    }
}
