<?php

namespace App\Http\Controllers\Admin\Modules\AdminPaymentDetails;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Models\Country;
use App\User;
use App\Models\Language;
use App\Models\Admin;
use App\Models\City;
use App\Models\Setting;
use App\Models\UserReward;
use App\Models\MerchantRegId;
use App\Driver;
use App\Merchant;
use App\Models\OrderMaster;
use App\Models\OrderSeller;
use App\Models\OrderDetail;
use App\Models\CountryDetails;
use App\Models\CitiDetails;
use App\Models\Product;
use App\Models\Payment;
use App\Models\Coupon;
use App\Models\MerchantAddressBook;
use App\Models\ZoneMaster;
use App\Models\ZoneDetail;
use App\Models\ZoneRateDetail;
use App\Models\TimeSlot;
use App\Models\OrderStatusTiming;

use App\Repositories\ShippingCostRepository;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\UserRepository;
use App\Repositories\DriverRepository;
use App\Repositories\OrderImageRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CityRepository;
use App\Repositories\CitiDetailsRepository;
use App\Repositories\SettingRepository;
use App\Repositories\CountryDetailsRepository;
use App\Repositories\CountryRepository;
use App\Repositories\UserAddressBookRepository;

use Illuminate\Support\Facades\Hash;

use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;
use App\Mail\SendMailDriverNotifydetails;//notification for external order (driver assigned)
use App\Mail\SendOrderAceptedMail;//notification for internal order (driver assigned)
use App\Mail\SendMailExternalOrderdetails;
use App\Mail\OrderMail;
use App\Mail\SendOrderDetailsMerchantMail;
use App\Mail\SendOrderMail;
use App\Mail\SendMerchantOrderMail;
use Config;
use Validator;
use Auth;

# use whatsapp controller
use App\Http\Controllers\Modules\WhatsApp\WhatsAppController;

class AdminPaymentDetailsController extends Controller
{
    protected $order_master, $merchants, $customers, $drivers, $order_details, $order_sellers, $order, $orderDetails, $orderSeller, $merchant;
    
    # whatsapp send message
    protected $whatsApp;

    public function __construct(OrderMasterRepository $order_master,
        MerchantRepository $merchants,
        UserRepository $customers,
        DriverRepository $drivers,
        OrderDetailRepository $order_details,
        OrderSellerRepository $order_sellers,
        CityRepository $city,
        CitiDetailsRepository $city_details,
        ShippingCostRepository $shipping_cost,
        OrderImageRepository $order_image,
        ProductVariantRepository $product_variant,
        ProductVariantRepository $productVariant,
        ProductRepository $product,
        SettingRepository $settings,
        CountryRepository $country,
        Request $request,
        WhatsAppController $whatsApp,
        UserAddressBookRepository $userAddr
    )
    {
        $this->middleware('admin.auth:admin');
        $this->order_master             = $order_master;
        $this->order                    = $order_master;
        $this->order_details            = $order_details;
        $this->orderDetails             = $order_details;
        $this->order_sellers            = $order_sellers;
        $this->orderSeller              = $order_sellers;
        $this->drivers                  = $drivers;
        $this->merchants                = $merchants;
        $this->merchant                 = $merchants;
        $this->customers                = $customers;
        $this->city                     = $city;
        $this->city_details             = $city_details;
        $this->shipping_cost            = $shipping_cost;
        $this->order_image              = $order_image;
        $this->product_variant          = $product_variant;
        $this->productVariant           = $productVariant;
        $this->product                  = $product;
        $this->settings                 = $settings;
        $this->whatsApp                 = $whatsApp;
        $this->country                  = $country;
        $this->userAddr                 = $userAddr;
        // $this->pdf                      =   $pdf;
    }
    /*
    * Method    : viewPaymentDetails
    * Description : This method is used to view details
    */
    public function viewPaymentDetails(Request $request){
        $columnIndex = $request->order[0]['column'];
        $columnSortOrder = $request->order[0]['dir'];

        $o = OrderDetail::with(['productDetails','productCategoriesDetails','getOrderDetailsUnitMaster','sellerDetails','productByLanguage','defaultImage','orderMaster.customerDetails','orderMaster.paymentData','orderMaster.orderMerchants'])->select('id','order_master_id','product_id','product_unit_master_id','seller_id','quantity','original_price','discounted_price','sub_total','total','seller_commission','loading_price','insurance_price','payable_amount','admin_commission','status','created_at',\DB::raw('(total-admin_commission+loading_price + seller_commission) se_net_earning_pro_ord'))->whereNotIn('status',['OC'])->groupBy('order_master_id','seller_id');

        $o = $o->whereHas('orderMaster',function($q1) use($request) {
            $q1 = $q1->whereNotIn('status',['I','D','OC']);
        });

        if(@$request->columns['0']['search']['value']){
                $o = $o->whereHas('orderMaster',function($q1) use($request) {
                    $q1 = $q1->where('order_no','like','%'.$request->columns['0']['search']['value'].'%');
                })->orWhereHas('sellerDetails',function($q1) use($request) {
                    $q1 = $q1->where('fname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('fname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('lname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".$request->columns['0']['search']['value']."%");
                });
            }
            if(@$request->columns['1']['search']['value']){
                $date = explode(',', $request->columns['1']['search']['value']);
                $from_date = $date[0];
                $to_date = $date[1];
                // $orders = $orders->whereBetween('created_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);
                $o = $o->whereHas('orderMaster',function($q1) use($request,$from_date,$to_date) {
                    $q1 = $q1->whereBetween('created_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);
                });
            }
            if(@$request->columns['11']['search']['value']){
                $o = $o->whereHas('orderMaster',function($q2) use($request) {
                    $q2 = $q2->where('invoice_no','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('invoice_details','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('notes','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('driver_notes','like','%'.$request->columns['11']['search']['value'].'%');
                });
            }
            if(@$request->columns['11']['search']['value']){
                $o = $o->whereHas('orderMaster',function($q2) use($request) {
                    $q2 = $q2->where('invoice_no','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('invoice_details','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('notes','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('driver_notes','like','%'.$request->columns['11']['search']['value'].'%');
                });
            }
            if(@$request->columns['19']['search']['value']){
                
                $o = $o->whereHas('orderMaster',function($q2) use($request) {
                    $q2 = $q2->where('shipping_city','like','%'.$request->columns['19']['search']['value'].'%');
                });
            }
           
            
            
            if(@$request->columns['8']['search']['value']){
                // $o = $o->where('payment_method',$request->columns['8']['search']['value']);
                $o = $o->whereHas('orderMaster',function($q2) use($request) {
                    $q2 = $q2->where('payment_method',$request->columns['8']['search']['value']);
                });
            }
            if(@$request->columns['5']['search']['value']){
                $status = explode(',', $request->columns['5']['search']['value']);
                $o = $o->whereIn('status', $status);
            }
            if(@$request->columns['4']['search']['value']){
                $o = $o->where( function($q) use ($request){
                   $q->where('seller_id',$request->columns['4']['search']['value']);
                });
            }
            if(@$request->columns['3']['search']['value']){
                $o = $o->whereHas('orderMaster', function($q4) use ($request){
                   $q4->where('user_id',$request->columns['3']['search']['value']);
                });
            }
            // if(@$request->columns['2']['search']['value']){
            //     $o = $o->whereHas( 'orderMaster',function($q4) use ($request){
            //        $q4->where('shipping_city',$request->columns['2']['search']['value']);
            //     });
            // }
            if(@$request->columns['6']['search']['value']){
                $o = $o->whereHas('orderMaster.paymentData', function($q4) use ($request){
                   $q4->where('txn_id',$request->columns['6']['search']['value']);
                });
            }
            
        $ps =  $o->count();
        $pp = $o;
        $pp = $pp->count();
        $limit = $request->length;
        $p = $o = $o;
        $p =  $p->count();
        if($columnIndex == 0) {

            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('order_master_id','asc');
            } else {
                $o = $o->orderBy('order_master_id','desc');
            }
            
        }
        if($columnIndex == 1) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('order_master_id','asc');
            } else {
                $o = $o->orderBy('order_master_id','desc');
            }
            
        }
        if($columnIndex == 2) {
            
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('id','asc');
            } else {
                $o = $o->orderBy('id','desc');
            }
        }
        if($columnIndex == 3) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('id','asc');
            } else {
                $o = $o->orderBy('id','desc');
            }
            
        }
        if($columnIndex == 4) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('payable_amount','asc');
            } else {
                $o = $o->orderBy('payable_amount','desc');
            }
            
        }
        if($columnIndex == 5) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('insurance_price','asc');
            } else {
                $o = $o->orderBy('insurance_price','desc');
            }
            
        }
        if($columnIndex == 6) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('admin_commission','asc');
            } else {
                $o = $o->orderBy('admin_commission','desc');
            }
        }
        if($columnIndex == 7) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('se_net_earning_pro_ord','asc');
            } else {
                $o = $o->orderBy('se_net_earning_pro_ord','desc');
            }
        }
        $o = $o->orderBy('id','desc')->skip($request->start)->take($request->length)->get();
        $data['aaData'] = $o;
        $data["draw"] = intval($request->draw);
        $data['iTotalRecords'] = $ps;
        $data["iTotalDisplayRecords"] = $pp;
        return response()->json($data);

    }
    public function viewOrderWisePaymentDetails(Request $request){
        $columnIndex = $request->order[0]['column'];
        $columnSortOrder = $request->order[0]['dir'];

        $o = OrderSeller::with(['orderDetails','orderMasterTab.paymentData','orderMerchant'])->whereNotIn('status',['OC']);

        $o = $o->whereHas('orderMasterTab',function($q1) use($request) {
            $q1 = $q1->whereNotIn('status',['I','D','OC']);
        });
        // $o = $o->whereHas('orderDetails',function($q1) use($request) {
        //     $q1 = $q1->whereNotIn('status',['I','D','OC']);
        // });

        if(@$request->columns['0']['search']['value']){
                $o = $o->whereHas('orderMasterTab',function($q1) use($request) {
                    $q1 = $q1->where('order_no','like','%'.$request->columns['0']['search']['value'].'%');
                })->orWhereHas('orderMerchant',function($q1) use($request) {
                    $q1 = $q1->where('fname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('fname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('lname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".$request->columns['0']['search']['value']."%");
                });
            }
            if(@$request->columns['1']['search']['value']){
                $date = explode(',', $request->columns['1']['search']['value']);
                $from_date = $date[0];
                $to_date = $date[1];
                // $orders = $orders->whereBetween('created_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);
                $o = $o->whereHas('orderMasterTab',function($q1) use($request,$from_date,$to_date) {
                    $q1 = $q1->whereBetween('created_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);
                });
            }
            if(@$request->columns['11']['search']['value']){
                $o = $o->whereHas('orderMasterTab',function($q2) use($request) {
                    $q2 = $q2->where('invoice_no','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('invoice_details','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('notes','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('driver_notes','like','%'.$request->columns['11']['search']['value'].'%');
                });
            }
            if(@$request->columns['11']['search']['value']){
                $o = $o->whereHas('orderMasterTab',function($q2) use($request) {
                    $q2 = $q2->where('invoice_no','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('invoice_details','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('notes','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('driver_notes','like','%'.$request->columns['11']['search']['value'].'%');
                });
            }
            if(@$request->columns['19']['search']['value']){
                
                $o = $o->whereHas('orderMasterTab',function($q2) use($request) {
                    $q2 = $q2->where('shipping_city','like','%'.$request->columns['19']['search']['value'].'%');
                });
            }
           
            
            
            if(@$request->columns['8']['search']['value']){
                // $o = $o->where('payment_method',$request->columns['8']['search']['value']);
                $o = $o->whereHas('orderMasterTab',function($q2) use($request) {
                    $q2 = $q2->where('payment_method',$request->columns['8']['search']['value']);
                });
            }
            if(@$request->columns['5']['search']['value']){
                $status = explode(',', $request->columns['5']['search']['value']);
                $o = $o->whereIn('status', $status);
            }
            if(@$request->columns['4']['search']['value']){
                $o = $o->where( function($q) use ($request){
                   $q->where('seller_id',$request->columns['4']['search']['value']);
                });
            }
            if(@$request->columns['3']['search']['value']){
                $o = $o->whereHas('orderMasterTab', function($q4) use ($request){
                   $q4->where('user_id',$request->columns['3']['search']['value']);
                });
            }
            // if(@$request->columns['2']['search']['value']){
            //     $o = $o->whereHas( 'orderMaster',function($q4) use ($request){
            //        $q4->where('shipping_city',$request->columns['2']['search']['value']);
            //     });
            // }
            if(@$request->columns['6']['search']['value']){
                $o = $o->whereHas('orderMasterTab.paymentData', function($q4) use ($request){
                   $q4->where('txn_id',$request->columns['6']['search']['value']);
                });
            }
            
        $ps =  $o->count();
        $pp = $o;
        $pp = $pp->count();
        $limit = $request->length;
        $p = $o = $o;
        $p =  $p->count();
        if($columnIndex == 0) {

            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('order_master_id','asc');
            } else {
                $o = $o->orderBy('order_master_id','desc');
            }
            
        }
        if($columnIndex == 1) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('order_master_id','asc');
            } else {
                $o = $o->orderBy('order_master_id','desc');
            }
            
        }
        if($columnIndex == 2) {
            
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('id','asc');
            } else {
                $o = $o->orderBy('id','desc');
            }
        }
        if($columnIndex == 3) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('id','asc');
            } else {
                $o = $o->orderBy('id','desc');
            }
            
        }
        if($columnIndex == 4) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('payable_amount','asc');
            } else {
                $o = $o->orderBy('payable_amount','desc');
            }
            
        }
        if($columnIndex == 5) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('insurance_price','asc');
            } else {
                $o = $o->orderBy('insurance_price','desc');
            }
            
        }
        if($columnIndex == 6) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('admin_commission','asc');
            } else {
                $o = $o->orderBy('admin_commission','desc');
            }
        }
        if($columnIndex == 7) {
            if($columnSortOrder == 'asc') {
                $o = $o->orderBy('se_net_earning_pro_ord','asc');
            } else {
                $o = $o->orderBy('se_net_earning_pro_ord','desc');
            }
        }
        $o = $o->orderBy('id','desc')->skip($request->start)->take($request->length)->get();
        $data['aaData'] = $o;
        $data["draw"] = intval($request->draw);
        $data['iTotalRecords'] = $ps;
        $data["iTotalDisplayRecords"] = $pp;
        return response()->json($data);

    }
    public function viewInfoDetails(Request $request){
        $data['customers'] = User::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        $data['cities'] = City::whereNotIn('status',['D'])->orderBy('name','asc')->get();
        $data['merchants'] = Merchant::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        return view('admin.modules.view_orderprowise_earnings.view_orderprowise_earnings')->with($data);
    }
    public function viewOrderWiseInfoDtls(Request $request){
        $data['customers'] = User::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        $data['cities'] = City::whereNotIn('status',['D'])->orderBy('name','asc')->get();
        $data['merchants'] = Merchant::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        return view('admin.modules.view_orderprowise_earnings.view_orderwise')->with($data);
    }
    public function viewOrderWiseInfoDetails(Request $request){
       
        // return view('admin.modules.view_orderprowise_earnings.order_wise_view_orderprowise_earnings')->with($data);
        $customers = $this->order_master->where('user_id','!=',0)->get();
        $external_customer = $this->order_master->where('user_id',0)->get();
        $driver = $this->drivers->where('status', 'A')->get();
        $drivers_ord = $this->drivers->where('status','A')->get();
        $userCustomer = User::where('status','!=','D')->orderBy('fname','asc')->get();
        $kuwaitCities = City::where('id','!=',0);
        // $kuwaitCities = $kuwaitCities->where(function($q1) {
        //     $q1 = $q1->where('language_id',getLanguage()->id);
        // });
        $kuwaitCities = $kuwaitCities->where(function($q1) {
            $q1 = $q1->where('status','!=','D');
        });
        $kuwaitCities = $kuwaitCities->orderBy('name','asc')->get();
        $merchants = $this->merchants->where('status','!=','D')->orderBy('fname','asc')->get();
        $orders = $this->order_master->with('getCityNameByLanguage')->where('status','!=','I');

        $orders = $orders->where('status','!=','D');
        $orders = $orders->where('status','!=','');
        $orders = $orders->where('status','!=','F');
        
        if(@$request->all() && @$request->merchant_id == ''){
            if(@$request->id){
                $orders = $orders->with([
                    'customerDetails',
                    'orderMasterDetails.driverDetails',
                    'countryDetails.countryDetailsBylanguage',
                    'orderMasterDetails',
                    'orderMasterDetails.productDetails',
                    'orderMasterDetails.productDetails.productUnitMasters',
                    'orderMasterDetails.productVariantDetails',
                    'orderMasterDetails.sellerDetails',
                    'countryDetails.countryDetailsBylanguage',
                    'shippingAddress',
                    'billingAddress',
                    'getBillingCountry',
                    'productVariantDetails',
                    'getCityNameByLanguage',
                    'getpickupCountryDetails',
                    'getpickupCityDetails',
                    'getPreferredTime',
                    'getAdminForExternalOrder',
                    'orderMerchants.orderSeller',
                ])
                ->where('id',$request->id);
            }
        } else {
            $orders = $orders->with([
                'customerDetails',
                'orderMasterDetails.driverDetails',
                'countryDetails.countryDetailsBylanguage',
                'orderMasterDetails',
                'orderMasterDetails.productDetails.productUnitMasters',
                'orderMasterDetails.productVariantDetails',
                'orderMasterDetails.sellerDetails',
                'countryDetails.countryDetailsBylanguage',
                'shippingAddress',
                'billingAddress',
                'getBillingCountry',
                'productVariantDetails',
                'getCityNameByLanguage',
                'getpickupCountryDetails',
                'getpickupCityDetails',
                'getPreferredTime',
                'getAdminForExternalOrder',
                'orderMerchants.orderSeller'
            ])
            ->orderBy('id','desc');
        }

        if(@$request->merchant_id){
            $orders = $orders->with([
                'customerDetails',
                'orderMasterDetails.driverDetails',
                'countryDetails.countryDetailsBylanguage',
                'orderMasterDetails',
                'orderMasterDetails.productDetails.productUnitMasters',
                'orderMasterDetails.productVariantDetails',
                'orderMasterDetails.sellerDetails',
                'countryDetails.countryDetailsBylanguage',
                'shippingAddress',
                'billingAddress',
                'getBillingCountry',
                'productVariantDetails',
                'getCityNameByLanguage',
                'getpickupCountryDetails',
                'getpickupCityDetails',
                'getPreferredTime',
                'getAdminForExternalOrder',
                'orderMerchants.orderSeller'
            ])
            ->where('user_id',$request->merchant_id);
        }
        $orders = $orders->orderBy('created_at','desc')->get();
        $order_detail = $this->order_details->get();
        $order_seller = $this->order_sellers->get();
        //dd($orders);
        $coupons = Coupon::get();
        //$key = $request->all();
        
        return view('admin.modules.view_orderprowise_earnings.order_wise_view_orderprowise_earnings')->with([
            'customers'       => @$customers,
            'drivers'         => @$driver,
            'orders'          => @$orders,
            'merchants'       => @$merchants,
            'order_detail'    => @$order_detail,
            'external_customer'   => @$external_customer,
            'order_seller'    => @$order_seller,
            'drivers_ord'     => @$drivers_ord,
            'userCustomer'    => @$userCustomer,
            'kuwaitCities'    => @$kuwaitCities,
            //'key'             => @$key,
            'coupons'         => $coupons
        ]);
    }
    public function orderwiselists(Request $request){
        $columnIndex = $request->order[0]['column']; // Column index
        $columnName = $request->columns[$columnIndex]['data']; // Column name
        $columnSortOrder = $request->order[0]['dir']; // asc or desc
        $orders = $this->order_master->with('getCityNameByLanguage');
        // $orders = $orders->where('status','!=','I');
        $orders = $orders->where('status','!=','D');
        // $orders = $orders->where('status','!=','F');
        $orders = $orders->where('status','!=', '');
        $ps =  $orders->count();
        $pp = $orders;
        $pp = $pp->count();
        $limit = $request->length;
        $p = $orders = $orders->with([
            'customerDetails',
            'orderMasterDetails.driverDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'orderMasterDetails.productDetails.productUnitMasters',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails',
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'shipping_city_details',
            'shipping_state_details',
            'getBillingCountry',
            'productVariantDetails',
            'getCityNameByLanguage',
            'paymentData',
            'getAdminForExternalOrder',
            'orderMerchants.orderSeller',
            'editedBy',
            'getPreferredTime',
        ])->whereNotIn('status',['I','D','OC']);

        $orders = $orders->whereHas('orderMasterDetails',function($q2) use($request) {
            $q2 = $q2->whereNotIn('status',['OC']);
        });

        $p = $p->count();
        
        if(@$request->all()){
            if(@$request->columns['0']['search']['value']){
                $orders = $orders->where(function($q1) use($request) {
                    $q1 = $q1->where('order_no','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_fname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_lname','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_phone','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere('shipping_email','like','%'.$request->columns['0']['search']['value'].'%')
                    ->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".$request->columns['0']['search']['value']."%");
                });
            }
            if(@$request->columns['1']['search']['value']){
                $date = explode(',', $request->columns['1']['search']['value']);
                $from_date = $date[0];
                $to_date = $date[1];
                $orders = $orders->whereBetween('created_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);
            }
            if(@$request->columns['11']['search']['value']){
                $orders = $orders->where(function($q2) use($request) {
                    $q2 = $q2->where('invoice_no','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('invoice_details','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('notes','like','%'.$request->columns['11']['search']['value'].'%')
                    ->orWhere('driver_notes','like','%'.$request->columns['11']['search']['value'].'%');
                });
            }
            if(@$request->columns['19']['search']['value']){
                $orders = $orders->where(function($q2) use($request) {
                    $q2 = $q2->where('shipping_country','!=',134);
                });
                $orders = $orders->where(function($q2) use($request) {
                    $q2 = $q2->where('shipping_city','like','%'.$request->columns['19']['search']['value'].'%');
                });
            }
            # order type - all type accessible for super admin but only this is accessible which is given by admin
            $roles = Auth()->guard('admin')->user()->roles->pluck('menu_id')->toArray();
            if(@$request->columns['7']['search']['value']){
                if(Auth()->guard('admin')->user()->type == 'A') {
                    $orders = $orders->where('order_type',$request->columns['7']['search']['value']);
                    if(@$request->columns['23']['search']['value'] && $request->columns['7']['search']['value'] == 'E'){
                        if($request->columns['23']['search']['value'] == 'A'){
                            $orders = $orders->where('order_type','E');
                        } elseif($request->columns['23']['search']['value'] == 'S' || $request->columns['23']['search']['value'] == 'L') {
                            $orders = $orders->where('external_order_type',$request->columns['23']['search']['value']);
                        }
                    }
                } else {
                    # both order accessible
                    if(in_array(6, $roles) && in_array(13, $roles)) {
                        $orders = $orders->where('order_type', $request->columns['7']['search']['value']);
                        if(@$request->columns['23']['search']['value'] == 'E'){
                            if($request->columns['23']['search']['value'] == 'A'){
                                $orders = $orders->where('order_type','E');
                            }elseif($request->columns['23']['search']['value'] == 'S' || $request->columns['23']['search']['value'] == 'L'){
                                $orders = $orders->where('external_order_type',$request->columns['23']['search']['value']);
                            }
                        }
                    } else if(in_array(6, $roles)) {
                        $orders = $orders->where('order_type', 'I');
                    } else if(in_array(13, $roles)) {
                        $orders = $orders->where('order_type', 'E');
                        if(@$request->columns['23']['search']['value'] && $request->columns['7']['search']['value'] == 'E'){
                            if($request->columns['23']['search']['value'] == 'A') {
                                $orders = $orders->where('order_type','E');
                            } elseif($request->columns['23']['search']['value'] == 'S' || $request->columns['23']['search']['value'] == 'L'){
                                $orders = $orders->where('external_order_type',$request->columns['23']['search']['value']);
                            }
                        }
                    }
                } 
            } 
            
            # for sub admin without filter
            if(Auth()->guard('admin')->user()->type == 'S'){
                # both order accessible
                if(in_array(6, $roles) && in_array(13, $roles)) {
                    $orders = $orders->whereIn('order_type', ['I', 'E']);
                } else if(in_array(6, $roles)) {
                    $orders = $orders->where('order_type', 'I');
                } else if(in_array(13, $roles)) {
                    $orders = $orders->where('order_type', 'E');
                }
            } 
            if(@$request->columns['8']['search']['value']){
                $orders = $orders->where('payment_method',$request->columns['8']['search']['value']);
            }
            if(@$request->columns['5']['search']['value']){
                $status = explode(',', $request->columns['5']['search']['value']);
                $orders = $orders->whereIn('status', $status);
                /*if(in_array($request->columns['6']['search']['value'], ['I', 'F'])) {
                    $orders = $orders->whereIn('status', ['I', 'F']);    
                } else {
                }*/
            }
            if(@$request->columns['4']['search']['value']){
                $orders = $orders->whereHas('orderMasterDetails', function($q) use ($request){
                   $q->where('seller_id',$request->columns['4']['search']['value']);
                });
            }
            if(@$request->columns['14']['search']['value']){
                $orders = $orders->whereHas('orderMasterDetails', function($q4) use ($request){
                   $q4->where('driver_id',$request->columns['14']['search']['value']);
                });
            }
            if(@$request->columns['17']['search']['value']){
                $orders = $orders->where('order_from', $request->columns['17']['search']['value']);
            }
            if(@$request->columns['3']['search']['value']){
                $orders = $orders->where( function($q4) use ($request){
                   $q4->where('user_id',$request->columns['3']['search']['value']);
                });
            }
            if(@$request->columns['2']['search']['value']){
                $orders = $orders->where( function($q4) use ($request){
                   $q4->where('shipping_city',$request->columns['2']['search']['value']);
                });
            }
            if(@$request->columns['6']['search']['value']){
                $orders = $orders->whereHas('paymentData', function($q4) use ($request){
                   $q4->where('txn_id',$request->columns['6']['search']['value']);
                });
            }
            if(@$request->columns['22']['search']['value']){
                $orders = $orders->where('coupon_code', @$request->columns['22']['search']['value']);
            }
            $pp = $orders;
            $pp = $pp->count();
            $limit = $request->length;
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('order_no','asc');
                } else {
                    $orders = $orders->orderBy('order_no','desc');
                }
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('created_at','asc');
                } else {
                    $orders = $orders->orderBy('created_at','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('shipping_fname','asc');
                } else {
                    $orders = $orders->orderBy('shipping_lname','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('shipping_city','asc');
                } else {
                    $orders = $orders->orderBy('shipping_city','desc');
                }
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('payable_amount','asc');
                } else {
                    $orders = $orders->orderBy('payable_amount','desc');
                }
            }
            if($columnIndex == 6) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('payment_method','asc');
                } else {
                    $orders = $orders->orderBy('payment_method','desc');
                }
            }
            
            if($columnIndex == 7) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('status','asc');
                } else {
                    $orders = $orders->orderBy('status','desc');
                }
            }
            if($columnIndex == 8) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('id','asc');
                } else {
                    $orders = $orders->orderBy('id','desc');
                }
            }
            
            if($columnIndex == 9) {
                if($columnSortOrder == 'asc') {
                    $orders = $orders->orderBy('order_from','asc');
                } else {
                    $orders = $orders->orderBy('order_from','desc');
                }
            }
            //For total subtotal and shipping_cost calculate
            $getOrder = $orders;
            $getOrder = $getOrder->pluck('id');
            
            $orders = $orders->skip($request->start)->take($request->length)->get();
            $data['aaData'] = $orders;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $ps;
            $data["iTotalDisplayRecords"] = $pp;
            $codOrder = OrderMaster::whereIn('id', $getOrder)->where('payment_method', 'C')->whereNotIn('status', ['I', 'F']);
            
            $totalCodSubtotal = $codOrder->sum('subtotal');
            $totalCodSubtotal1 = $codOrder->sum('total_product_price');
            $totalCodSubtotal = $totalCodSubtotal - $totalCodSubtotal1;
            $totalCodShpPrc = $codOrder->sum('loading_price');
            $totalCodShpPrc = $codOrder->sum('order_total');

            $onlineOrder = OrderMaster::whereIn('id', $getOrder)->where('payment_method', 'O')->whereNotIn('status', ['I', 'F']);

            $totalOnlineSubtotal = $onlineOrder->sum('subtotal');
            $totalOnlineSubtotal1 = $onlineOrder->sum('total_product_price');
            $totalOnlineSubtotal = $totalOnlineSubtotal - $totalOnlineSubtotal1;
            $totalOnlineShpPrc = $onlineOrder->sum('loading_price');
            $totalOnlineOrder = $onlineOrder->sum('order_total');

            $data['totalCodSubtotal'] = number_format($totalCodSubtotal, 2);
            $data['totalOnlineSubtotal'] = number_format($totalOnlineSubtotal, 2);
            $data['totalCodShpPrc'] = number_format($totalCodShpPrc, 2);
            $data['totalOnlineShpPrc'] = number_format($totalOnlineShpPrc, 2);
            $data['orderTotal'] = number_format($totalOnlineOrder, 2);
            return response()->json($data);    
        } else {
            $orders =$orders->orderBy('id','desc')->get();
            return response()->json($orders); 
        }    
    }
}
