<?php

namespace App\Http\Controllers\Admin\Modules\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Withdraw;
use App\Customer;
use App\Admin;
use Validator;
use Auth;
use App\Models\Language;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Repositories\MerchantRepository;
use App\Repositories\WithdrawRepository;
use Mail;
use App\Mail\SendMailWithDrawlRequest;
// SendMailWithDrawlRequest

class FinanceController extends Controller
{
    protected $order_master,$order_details,$shipping_cost,$order_sellers;
    public function __construct(MerchantRepository $merchant,
    							WithdrawRepository $withdraw,
    							Request $request)
	{
		// $this->middleware('merchant.auth:merchant');
		$this->middleware('admin.auth:admin');
		$this->checkLoginAccess();
		$this->merchant  =   $merchant;
		$this->withdraw  =   $withdraw;

	}
	/*
	* Method: index
	* Description: This method is used to show the list of earnings from list of merchants.
	* Author: Jayatri
	*/
	public function index(Request $request){
		$this->checkLoginAccess();
		$finance = $this->merchant->where('status','!=','D');
		$ff = $finance;
		$ff = $ff->count();
		if(@$request->all()){
			$columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

			$finance = $finance->where(function($query) use($request){
				$query->where('fname','like','%'.@$request->columns['1']['search']['value'].'%')
					->orWhere('lname','like','%'.@$request->columns['1']['search']['value'].'%')
					->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".@$request->columns['1']['search']['value']."%");
			});

			// sorting
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $finance = $finance->orderBy('fname','asc');
                } else {
                    $finance = $finance->orderBy('fname','desc');
                }
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $finance = $finance->orderBy('email','asc');
                } else {
                    $finance = $finance->orderBy('email','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $finance = $finance->orderBy('phone','asc');
                } else {
                    $finance = $finance->orderBy('phone','desc');
                }
            }if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $finance = $finance->orderBy('total_earning','asc');
                } else {
                    $finance = $finance->orderBy('total_earning','desc');
                }
            }if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $finance = $finance->orderBy('total_commission','asc');
                } else {
                    $finance = $finance->orderBy('total_commission','desc');
                }
            }if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $finance = $finance->orderBy('total_due','asc');
                } else {
                    $finance = $finance->orderBy('total_due','desc');
                }
            }if($columnIndex == 6) {
                if($columnSortOrder == 'asc') {
                    $finance = $finance->orderBy('total_paid','asc');
                } else {
                    $finance = $finance->orderBy('total_paid','desc');
                }
			}
			
			if($columnIndex == 7) {
                if($columnSortOrder == 'asc') {
                    $finance = $finance->orderBy('id','asc');
                } else {
                    $finance = $finance->orderBy('id','desc');
                }
            }

			if($columnIndex == 8) {
                if($columnSortOrder == 'asc') {
                    $finance = $finance->orderBy('id','asc');
                } else {
                    $finance = $finance->orderBy('id','desc');
                }
            }
			$fff = $finance;
			$fff = $fff->count();

			$finance = $finance->skip($request->start)->take($request->length)->get();
			
			$data['aaData'] = $finance;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = @$ff;
            $data["iTotalDisplayRecords"] = @$fff;
            return response()->json($data);
		}
		$finance = $this->merchant->orderBy('id','desc')->get();
		return view('admin.modules.finance.view_earnings')->with(['finance'=>@$finance]);
	}
	/*
	* Method: listRequestWithdrawl
	* Description: This method is used to list of request for withdrawl.
	* Author: Jayatri
	*/
	public function listRequestWithdrawl(){
		$this->checkLoginAccess();
		$withdraw = $this->withdraw->whereIn('status',['N','S','C'])->orderBy('id','desc')->get();
		$merchants = $this->merchant->where('status','!=','D')->orderBy('fname','asc')->get();
		return view('admin.modules.finance.list_withdrawl_request')->with([
			'withdraw'=>@$withdraw,
			'merchants'=>@$merchants
		]);
	}
	public function viewlistRequestWithdrawl(Request $request){
        $columnIndex = $request->order[0]['column'];
        $columnSortOrder = $request->order[0]['dir'];

        $withdraw = Withdraw::with('merchantDetails')->whereIn('status',['N','S','C'])->orderBy('id','desc');

        
        if(@$request->columns['0']['search']['value']){
			$withdraw = $withdraw->whereHas('merchantDetails', function($q) use ($request){
				$q->where('fname', 'like','%'.$request->columns['0']['search']['value'].'%')
				->orWhere('lname','like','%'.$request->columns['0']['search']['value'].'%')
				->orWhere('email','like','%'.$request->columns['0']['search']['value'].'%')
				->orWhere('phone','like','%'.$request->columns['0']['search']['value'].'%')
				->orWhere('id',$request->columns['0']['search']['value'])
				->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".$request->columns['0']['search']['value']."%");
			});	
		}
		if(@$request->columns['1']['search']['value']) {
			$withdraw = $withdraw->whereHas('merchantDetails', function($q) use ($request){
				$q->where('id', $request->columns['1']['search']['value']);
			});	
		}
        	
        $ps =  $withdraw->count();
        $pp = $withdraw;
        $pp = $pp->count();
        $limit = $request->length;
        $p = $withdraw = $withdraw;
        $p =  $p->count();
        if($columnIndex == 0) {

            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('id','asc');
            } else {
                $withdraw = $withdraw->orderBy('id','desc');
            }
            
        }
        if($columnIndex == 1) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('request_date','asc');
            } else {
                $withdraw = $withdraw->orderBy('request_date','desc');
            }
            
        }
        if($columnIndex == 2) {
            
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('payment_date','asc');
            } else {
                $withdraw = $withdraw->orderBy('payment_date','desc');
            }
        }
        if($columnIndex == 3) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('account_no','asc');
            } else {
                $withdraw = $withdraw->orderBy('account_no','desc');
            }
            
        }
        if($columnIndex == 4) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('account_name','asc');
            } else {
                $withdraw = $withdraw->orderBy('account_name','desc');
            }
            
        }
        if($columnIndex == 5) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('bank_name','asc');
            } else {
                $withdraw = $withdraw->orderBy('bank_name','desc');
            }
            
        }
        if($columnIndex == 6) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('branch_name','asc');
            } else {
                $withdraw = $withdraw->orderBy('branch_name','desc');
            }
        }
        if($columnIndex == 7) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('ifsc_code','asc');
            } else {
                $withdraw = $withdraw->orderBy('ifsc_code','desc');
            }
        }
        if($columnIndex == 8) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('payment_method','asc');
            } else {
                $withdraw = $withdraw->orderBy('payment_method','desc');
            }
        }
        if($columnIndex == 9) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('id','asc');
            } else {
                $withdraw = $withdraw->orderBy('id','desc');
            }
        }
        if($columnIndex == 10) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('id','asc');
            } else {
                $withdraw = $withdraw->orderBy('id','desc');
            }
        }
        if($columnIndex == 11) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('id','asc');
            } else {
                $withdraw = $withdraw->orderBy('id','desc');
            }
            
        }
        if($columnIndex == 12) {
            
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('amount','asc');
            } else {
                $withdraw = $withdraw->orderBy('amount','desc');
            }
        }
        if($columnIndex == 13) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('balance','asc');
            } else {
                $withdraw = $withdraw->orderBy('balance','desc');
            }
            
        }
        if($columnIndex == 14) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('status','asc');
            } else {
                $withdraw = $withdraw->orderBy('status','desc');
            }
            
        }
        if($columnIndex == 15) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('description','asc');
            } else {
                $withdraw = $withdraw->orderBy('description','desc');
            }
            
        }
        if($columnIndex == 16) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('id','asc');
            } else {
                $withdraw = $withdraw->orderBy('id','desc');
            }
        }
        if($columnIndex == 17) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('id','asc');
            } else {
                $withdraw = $withdraw->orderBy('id','desc');
            }
        }
        if($columnIndex == 18) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('id','asc');
            } else {
                $withdraw = $withdraw->orderBy('id','desc');
            }
        }
        if($columnIndex == 19) {
            if($columnSortOrder == 'asc') {
                $withdraw = $withdraw->orderBy('id','asc');
            } else {
                $withdraw = $withdraw->orderBy('id','desc');
            }
        }
        
        $withdraw = $withdraw->orderBy('id','desc')->skip($request->start)->take($request->length)->get();
        $data['aaData'] = $withdraw;
        $data["draw"] = intval($request->draw);
        $data['iTotalRecords'] = $ps;
        $data["iTotalDisplayRecords"] = $pp;
        return response()->json($data);
    }
	/*
	* Method: addWithdrawlRequestByMerchant
	* Description: This method is used to add withdrawl request for merchant by admin.
	* Author: Jayatri
	*/
	public function addWithdrawlRequestByMerchant(Request $request){
		$merchant = $this->merchant->where(['id'=>@$request->merchant])->first();
		if(@$merchant){
			$chk = $this->withdraw->where('seller_id',@$request->merchant)->first();
			if($merchant->total_due >= @$request->amount && @$request->amount>0.000){
				if(@$request->amount){
					$new['seller_id'] = $merchant->id;
					$new['account_no'] = $merchant->account_no;
					$new['account_name'] = $merchant->account_name;
					$new['bank_name'] = $merchant->bank_name;
					$new['iban_number'] = $merchant->iban_number;
					$new['amount'] = $request->amount;
					$new['balance'] = $merchant->total_due;
					$new['description'] = " ";
					$new['request_date'] = now();
					$new['status'] = 'N';	
					//dd($new);
					$withdrawe = $this->withdraw->create($new);
            		
            		$data['merchant_name'] 		= 	@$merchant->fname." ".@$merchant->lname;
            		$data['merchant_email'] 	=	@$merchant->email;
            		$data['created_at'] 		=	now();
            		$data['status'] 			=	"New";
            		$data['merchant_phone'] 	=	@$merchant->phone;
            		$data['account_no'] 		=	@$merchant->account_number;
            		$data['account_name'] 		=	@$merchant->account_name;
            		$data['bank_name'] 			=	@$merchant->bank_name;
            		$data['iban_number'] 		=	@$merchant->iban_number;
            		$data['amount'] 			=	@$request->amount;
            		$data['balance'] 			=	@$merchant->total_due;
            		$data['request_id'] 		=	@$withdrawe->id;
            		if(@$withdrawe->status == 'N'){
            			$data['wstatus'] 			=	"New";	
            		}
            		
            		$admin_notify = Admin::where('type','A')->first();
	        		if(@$admin_notify){
	        			if(@$admin_notify->email_1) {
	        				$contactList[0] = @$admin_notify->email_1;
	        			}
	        			if(@$admin_notify->email_2) {
	        				$contactList[1] = @$admin_notify->email_2;
	        			}
	        			if(@$admin_notify->email_3) {
	        				$contactList[2] = @$admin_notify->email_3;
	        			}
	        			$contactList[0] = $admin_notify->email;
	        			$contactListEmail3 = array_unique($contactList);
	        			$data['bcc'] = $contactListEmail3;
	        		}
	        		// dd($data);
        			Mail::send(new SendMailWithDrawlRequest($data));
					session()->flash("success",__('success.-4045'));	
				}else{
					session()->flash("error",__('errors.-5042'));
					//error
				}
			}else{
				session()->flash("error",__('errors.-5042'));
			}
		}else{
			//error
			session()->flash("error",__('errors.-5042'));
		}
		return redirect()->back();
	}

	/*
	* Method: searchRequestedWihdrawl
	* Description: This method is used to search Requested Wihdrawl.
	* Author: Jayatri
	*/
	public function searchRequestedWihdrawl(Request $request){
		$this->checkLoginAccess();
		if(@$request->all()){
			$withdraw = $this->withdraw->with(['merchantDetails'])->where('id','!=',0);
			if(@$request->keyword) {
				$withdraw = $withdraw->whereHas('merchantDetails', function($q) use ($request){
					$q->where('fname', 'like','%'.$request->keyword.'%')
					->orWhere('lname','like','%'.$request->keyword.'%')
					->orWhere('email','like','%'.$request->keyword.'%')
					->orWhere('phone','like','%'.$request->keyword.'%')
					->orWhere('id',$request->keyword)
					->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".$request->keyword."%");
				});	
			}
			if(@$request->merchant) {
				$withdraw = $withdraw->whereHas('merchantDetails', function($q) use ($request){
					$q->where('id', $request->merchant);
				});	
			}
			
			$withdraw = $withdraw->orderBy('id','desc')->get(); 
			return view('admin.modules.finance.ajax_list_withdrawl_request')->with(['withdraw'=>@$withdraw]);
		}else{
			return 0;
		}
	}

	/*
	* Method: searchEarnings
	* Description: This method is used to search Earnings.
	* Author: Jayatri
	*/
	public function searchEarnings(Request $request){
		$this->checkLoginAccess();
		if(@$request->all()){
			$finance = $this->merchant->where('status','!=','D');
			$finance = $finance->where(function($query) use($request){
				$query->Where('fname','like','%'.$request->keyword.'%')
					->orWhere('lname','like','%'.$request->keyword.'%')
					->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".$request->keyword."%");
			});
			$finance = $finance->orderby('id','desc')->get();
			return view('admin.modules.finance.ajax_list_view_earnings')->with(['finance'=>@$finance]);
		}else{
			return 0;
		}
	}
	/*
	* Method: approveRequest
	* Description: This method is used to approveRequest.
	* Author: Jayatri
	*/
	public function approveRequest($id,Request $request){
		$this->checkLoginAccess();
		$withdraw = $this->withdraw->where('id',$id)->first();
		if(@$withdraw){
			$update['description'] = @$request->approve_comment;
			$update['status'] = 'S';
			$update['balance'] = $withdraw->balance - $withdraw->amount;
			$update['payment_date'] = now();
			$this->withdraw->where('id',$id)->update($update);

			$merchant = $this->merchant->where('id',$withdraw->seller_id)->first();
			if(@$withdraw->amount > 0.000){
				if(@$merchant){
					$totalEarnings 	= $merchant->total_earning;
					$totalPaid 		= $merchant->total_paid + $withdraw->amount;
					$totalDue 		= $merchant->total_due - $withdraw->amount;
					$totalComission	= $merchant->total_commission;
					// dd($totalPaid);
					$merchant_update['total_paid'] = $totalPaid;
					$merchant_update['total_due'] = $totalDue;
					$this->merchant->where('id',$withdraw->seller_id)->update($merchant_update);
					session()->flash("success",__('success.-4046'));
				}else{
					session()->flash("error",__('errors.-5043'));
				}
			}
		}else{
			session()->flash("error",__('errors.-5043'));
		}
		return redirect()->back();
	}
	/*
	* Method: rejectRequest
	* Description: This method is used to rejectRequest
	* Author: Jayatri
	*/
	public function rejectRequest($id,Request $request){
		$this->checkLoginAccess();
		$withdraw = $this->withdraw->where('id',$id)->first();
		if(@$withdraw){
			$update['description'] = @$request->reject_comment;
			$update['status'] = 'C';
			$update['payment_date'] = now();
			$this->withdraw->where('id',$id)->update($update);
			session()->flash("success",__('success.-4047'));
		}else{
			session()->flash("error",__('errors.-5044'));
		}
		return redirect()->back();
	}
	/*
	* Method: viewDetails
	* Description: This method is used to view details.
	* Author: Jayatri
	*/
	public function orderSummary($id){
		$this->checkLoginAccess();
		$merchant = $this->merchant->where('id',$id)->first();
		if(@$merchant){
			return redirect()->route('admin.list.order');	
		}else{
			return redirect()->back();
		}
	}
	/*
	* Method: checkLoginAccess
	* Description: This method is used to check login Access.
	* Author: Jayatri
	*/
	private function checkLoginAccess(){
		if(!@Auth::guard('admin')->user()){
			return redirect()->route('admin.login');
		}
	}
}
