<?php

namespace App\Http\Controllers\Admin\Modules\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductVariantDetail;
use App\Models\UnitMaster;
use Validator;
use Auth;
use App\Models\Setting;
use Session;
use App\Repositories\SettingRepository;

class SettingController extends Controller
{
	public function __construct(SettingRepository $setting)
	{
		$this->middleware('admin.auth:admin');
		$this->setting            =   $setting;
	}

    /*
    * Method        : showLoyaltyPoint
    * Description   : This method is used to edit loyalti point.
    * Author        : Surajit
    */
	public function showLoyaltyPoint(){
        $data['setting'] = Setting::first();
		return view('admin.modules.settings.manage_loyalty', $data);
	}

    /*
    * Method        : storeLoyaltyPoint
    * Description   : This method is used to store loyalti point into settins table.
    * Author        : Surajit
    */
    public function storeLoyaltyPoint(Request $request, $id){

        $update['one_kwd_to_point'] = $request->one_kwd_to_point;
        $update['one_point_to_kwd'] = $request->one_point_to_kwd;
        $update['min_point']        = $request->min_point;
        $update['max_discount']     = $request->max_discount;
        Setting::whereId($id)->update($update);
        Session::flash("success",__('success.-4048'));
        return redirect()->back();
    }
    public function manageMaintence(Request $request){
        $data['setting'] =$setting =  $this->setting->where('id','!=',0)->first();
        $data['unitmasters'] = UnitMaster::where('status','A')->get();

        if(@$setting){
            if(@$request->all()){
                
                $update['insurance'] = @$request->insurance;
                $update['commission'] = @$request->commission;
                $update['loading_unloading_price'] = @$request->loading_unloading_price;
                $update['seller_commission'] = @$request->seller_commission;
                $this->setting->where('id','!=',0)->update($update);
                Session::flash("success",__('success.-4060'));
            }else{
                return view('admin.modules.settings.maintenance',$data);
            }
        }else{
            Session::flash("error",__('errors.-5058'));
        }
        return redirect()->back();
    }
}
