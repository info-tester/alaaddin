<?php

namespace App\Http\Controllers\Admin\Modules\Time_slots;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TimeSlot;
use Auth;

class TimeSlotController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
        $this->checkLoginAccess();
    }
    /*
    * Method: index
    * Description: This method is used to show time slot list
    * Author: Argha
    * Date: 2020-09-16
    */
    public function index(Request $request)
    {
    	if(@$request->all()){
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

            $cc = TimeSlot::count(); // total customer before filtering

			
            $ccc = TimeSlot::count(); // total customers before paginate
            $time_slot = array();
            // sorting
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $time_slot = TimeSlot::orderBy('from_time','asc');
                } else {
                    $time_slot = TimeSlot::orderBy('from_time','desc');
                }
                $time_slot = $time_slot->skip($request->start)->take($request->length)->get();
            }elseif($columnIndex == 1){
                if($columnSortOrder == 'asc') {
                    $time_slot = TimeSlot::orderBy('to_time','asc');
                } else {
                    $time_slot = TimeSlot::orderBy('to_time','desc');
                }
                $time_slot = $time_slot->skip($request->start)->take($request->length)->get();
            }else{
                $time_slot = TimeSlot::skip($request->start)->take($request->length)->get();
            }
            
            

            $data['aaData'] = $time_slot;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $cc;
            $data["iTotalDisplayRecords"] = $ccc;
            return response()->json($data);
		}else{
            $time_slot = TimeSlot::orderBy('id','desc')->get();
            return view('admin.modules.time_slot.time_slot_list')->with([
                'time_slot' =>  @$time_slot
            ]);
        }
    }

    /*
    * Method: addTimeslot
    * Description: This method is used to show add time slot page
    * Author: Argha
    * Date: 2020-09-16
    */
    public function addTimeSlot(Request $request)
    {
        return view('admin.modules.time_slot.add_time_slot');
    }

    /*
    * Method: insertTimeslot
    * Description: This method is used to show insert time slot
    * Author: Argha
    * Date: 2020-09-16
    */
    public function insertTimeslot(Request $request)
    {
        $request->validate([
            'from_time' => 'required',
            'to_time'   => 'required',
        ]);

        $new['from_time'] = date('H:i:s', strtotime(@$request->from_time));
        $new['to_time'] = date('H:i:s', strtotime(@$request->to_time));

        $ins = TimeSlot::create($new);

        if($ins){
            session()->flash("success",__('success.-4096'));
        }else{
            session()->flash("error",__('errors.-5089'));
        }

        return redirect()->route('admin.manage.time.slot');
    }

    /*
    * Method: editTimeslot
    * Description: This method is used to show edit time slot page
    * Author: Argha
    * Date: 2020-09-16
    */
    public function editTimeslot($id)
    {
        $data['time_data'] = TimeSlot::where('id',$id)->first();

        return view('admin.modules.time_slot.edit_time_slot')->with($data);
    }

    /*
    * Method: updateTimeslot
    * Description: This method is used to show update time slot
    * Author: Argha
    * Date: 2020-09-16
    */
    public function updateTimeslot(Request $request)
    {
        $request->validate([
            'from_time' => 'required',
            'to_time'   => 'required',
        ]);

        $update['from_time'] = date('H:i:s', strtotime(@$request->from_time));
        $update['to_time'] = date('H:i:s', strtotime(@$request->to_time));

        $up = TimeSlot::where('id',$request->id)->update($update);

        if($up){
            session()->flash("success",__('success.-4097'));
        }else{
            session()->flash("error",__('errors.-5087'));
        }

        return redirect()->route('admin.manage.time.slot');
    }

    /*
    * Method: deleteTimeslot
    * Description: This method is used to show delete time slot
    * Author: Argha
    * Date: 2020-09-16
    */
    public function deleteTimeslot(Request $request)
    {
        $id = $request['data']['id'];
        //dd($id);
        $deleteTime = TimeSlot::where('id',$id)->delete();
        $response['jsonrpc'] = '2.0';
        if($deleteTime){
            $response['status'] = "1";
        }else{
            $response['status'] = "0";
        }

        return response()->json($response);
    }

    /*
    * Method: checkTimeRange
    * Description: This method is used to show delete time slot
    * Author: Argha
    * Date: 2020-09-16
    */

    public function checkTimeRange(Request $request)
    {
        //dd($request->all());
        $response['jsonrpc'] = '2.0';
        if($request->all()){
            $from_time = date('H:i:s', strtotime($request['data']['from_time']));
            $to_time = date('H:i:s', strtotime($request['data']['to_time']));

            if(@$request['data']['id']){
                $id = $request['data']['id'];

                $timeSlot = TimeSlot::where(function($q) use($from_time,$to_time){
                    $q->where(function($where1) use ($from_time,$to_time) {
                        $where1->where('from_time', '<', $from_time)
                        ->where('from_time', '>=', $from_time);

                    })
                    ->orWhere(function($where2) use ($from_time,$to_time) {
                        $where2->where('to_time', '<=', $to_time)
                        ->where('to_time', '>', $to_time);
                    })
                    ->orWhere(function($where3) use ($from_time,$to_time) {

                        $where3->where('from_time', $from_time)
                        ->where('to_time', $to_time);
                    });
                })
                ->where('id','!=',$id)
                ->first();
            }else{
                $timeSlot = TimeSlot::where(function($q) use($from_time,$to_time){
                    $q->where(function($where1) use ($from_time,$to_time) {
                        $where1->where('from_time', '<', $from_time)
                        ->where('from_time', '>=', $from_time);

                    })
                    ->orWhere(function($where2) use ($from_time,$to_time) {
                        $where2->where('to_time', '<=', $to_time)
                        ->where('to_time', '>', $to_time);
                    })
                    ->orWhere(function($where3) use ($from_time,$to_time) {

                        $where3->where('from_time', $from_time)
                        ->where('to_time', $to_time);
                    });
                    // $q->where('from_time', $from_time)
                    //      ->where('to_time', $to_time);
                })
                ->first();
                //dd($timeSlot);
            }
            if(@$timeSlot){
                $response['status'] =  0;
            }else{
                $response['status'] =  1;	
            }
        }else{
            $response['status'] =  1;
        }

        return response()->json($response);
        
    }
    private function checkLoginAccess(){
        if(!@Auth::guard('admin')->user()){
            return redirect()->route('admin.login');
        }
    }
}
