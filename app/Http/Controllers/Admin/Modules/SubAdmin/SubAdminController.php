<?php

namespace App\Http\Controllers\Admin\Modules\SubAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductVariantDetail;
use Validator;
use Auth;
use App\Admin;
use App\Models\Menu;
use App\Models\Permission;
use App\Models\Admin_activities;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Mail;
use App\Mail\SubAdminDetailsMailSend;
use App\Repositories\AdminRepository;

class SubAdminController extends Controller
{
	public function __construct()
	{
		$this->middleware('admin.auth:admin');
		
	}

    /*
    * Method        : index
    * Description   : This method is used to list of sub admins.
    * Author        : Jayatri
    * Date 			: 25/01/2020
    */
	public function index(Request $request){
        
		if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                            ->where('id', auth::guard('admin')->user()->id)
                            ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                }
                if(in_array(1, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
		$sub_admins = Admin::with('sidebarAccess','sidebarAccess.menuDetails')->where(['type'=>'S']);
		$menus = Menu::where('language_id',getLanguage()->id)->get();
		$permissions = Permission::with('menuDetails')->get();
		
        $ss = $sub_admins;
        $ss = $ss->count();
		if(@$request->all()){
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

			
			if(@$request->columns['1']['search']['value']){
				$sub_admins = $sub_admins->where(function($query) use($request){
                $query->Where('fname','like','%'.@$request->columns['1']['search']['value'].'%')
                ->orWhere('lname','like','%'.@$request->columns['1']['search']['value'].'%')
                ->orWhere('phone','like','%'.@$request->columns['1']['search']['value'].'%')
                ->orWhere('email','like','%'.@$request->columns['1']['search']['value'].'%')
                ->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".@$request->columns['1']['search']['value']."%");
                });
			}
            if(@$request->columns['2']['search']['value']){
                $sub_admins = $sub_admins
                ->whereHas('sidebarAccess', function($q) use ($request){
                    $q->where('menu_id', @$request->columns['2']['search']['value']);
                        // ->orWhere('email', 'like', '%'.$request->keyword.'%')
                        // ->orWhere('phone', 'like', '%'.$request->keyword.'%');
                    });
            }
            $sss = $sub_admins;
            $sss = $sss->count();

            // sorting
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $sub_admins = $sub_admins->orderBy('fname','asc');
                } else {
                    $sub_admins = $sub_admins->orderBy('fname','desc');
                }
            }
            // sorting
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $sub_admins = $sub_admins->orderBy('email','asc');
                } else {
                    $sub_admins = $sub_admins->orderBy('email','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $sub_admins = $sub_admins->orderBy('phone','asc');
                } else {
                    $sub_admins = $sub_admins->orderBy('phone','desc');
                }
            }

            $sub_admins = $sub_admins->skip($request->start)->take($request->length)->get();

            $data['aaData'] = $sub_admins;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = @$ss;
            $data["iTotalDisplayRecords"] = @$sss;
            return response()->json($data);

            /*return view('admin.modules.sub_admin.ajax_manage_subadmin')->with([
                'sub_admins'=>@$sub_admins,
                'menus'=>@$menus,
                'permissions'=>@$permissions
            ]);*/
		}else{
            $sub_admins = $sub_admins->orderBy('id','desc')->get();
            return view('admin.modules.sub_admin.manage_subadmin')->with([
            'sub_admins'=>@$sub_admins,
            'menus'=>@$menus,
            'permissions'=>@$permissions
            ]);
        }
		
	}
    /*
    * Method        : addSubadminDetails
    * Description   : This method is used to add sub-admin.
    * Author        : Jayatri
    * Date 			: 25/01/2020
    */
	public function addSubadminDetails(Request $request){
        
		if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                            ->where('id', auth::guard('admin')->user()->id)
                            ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                }  
                //dd($permission);
                if(in_array(1, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
		$sub_admins = Admin::where(['type'=>'S'])->get();
		$menus = Menu::where('language_id',getLanguage()->id)->get();
		if(@$request->all()){
			// dd($request->all());
			$request->validate([
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required|email|max:255|unique:admins',
                'phone' => 'required',
                'password' => 'required',
                'confirm_password' => 'required',
                'permission' => 'required',
                'profile_pic'=>'image|mimes:jpeg,jpg,png'
            ],[
                'fname.required' =>'First name field is required',
                'lname.required' =>'Last name field is required',
                'phone.required' =>'Phone number field is required',
                'permission.required'=>'Please select atleast one checkbox for permission',
                'email.required' => 'Email-id field is required',
                'password.required' => 'Password field is required',
                'confirm_password.required' => 'Confirm password field is required',
                'profile_pic.image'=>'Please upload image with extension jpg/jpeg/png'
            ]);
            
			if(@$request->fname){
				$new['fname'] = $request->fname;
			}
			if(@$request->lname){
				$new['lname'] = $request->lname;
			}
			if(@$request->phone){
				$new['phone'] = $request->phone;
			}
			if(@$request->email){
				$new['email'] = $request->email;
			}

			if(@$request->profile_pic){
				$pic   = $request->profile_pic;
                $name = time().'.'.$pic->getClientOriginalExtension();
                $pic->move('storage/app/public/sub_admin_pics/', $name);
				$new['profile_pic'] = $name;
			}
			if(@$request->password){
				$new['password'] = Hash::make($request->password);
			}
			DB::transaction(function() use($new,$request){
				$new['type'] = 'S';
				$new['status'] = 'A';
				$admin = Admin::create($new);
				if(@$admin){
					if(@$request->permission){
						foreach(@$request->permission as $value){
							$sidebar['admin_id'] = $admin->id;
							$sidebar['menu_id'] = $value;
							$access = Permission::create($sidebar);
						}
					}
					$adminDetails = new \stdClass();
                    $adminDetails->name = @$request->fname." ".@$request->lname; 
                    $adminDetails->email =@$request->email;
                    $adminDetails->phone =@$request->phone;
                    $adminDetails->password =@$request->password;
					Mail::send(new SubAdminDetailsMailSend($adminDetails));
					session()->flash("success",__('success.-4015'));
				}else{
					session()->flash("error",__('errors.-5016'));
				}
            });

            
            
			return redirect()->route('admin.list.subadmin');
		}
		return view('admin.modules.sub_admin.add_subadmin')->with([
			'sub_admins'=>@$sub_admins,
			'menus'=>@$menus
		]);
	}
	/*
    * Method        : editSubAdmin
    * Description   : This method is used to edit sub-admin.
    * Author        : Jayatri
    * Date 			: 25/01/2020,27/01/2020
    */
	public function editSubAdmin($id,Request $request){
		if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                            ->where('id', auth::guard('admin')->user()->id)
                            ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(1, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
		$sub_admins = Admin::where(['type'=>'S'])->get();
		$sub_admin = Admin::with('sidebarAccess','sidebarAccess.menuDetails')->where('id',$id)->first();
		if(@$sub_admin){
			$menus = Menu::where('language_id',getLanguage()->id)->get();
			if($request->all()){
				// dd($request->all());
				$request->validate([
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'permission' => 'required',
                'profile_pic'=>'image|mimes:jpeg,jpg,png'
	            ],[
	                'fname.required' =>'First name field is required',
	                'lname.required' =>'Last name field is required',
	                'phone.required' =>'Phone number field is required',
	                'permission.required'=>'Please select atleast one checkbox for permission',
	                'email.required' => 'Email-id field is required',
	                'profile_pic.image'=>'Please upload image with extension jpg/jpeg/png'
	            ]);
				if(@$request->fname){
					$new['fname'] = $request->fname;
				}
				if(@$request->lname){
					$new['lname'] = $request->lname;
				}
				if(@$request->phone){
					$new['phone'] = $request->phone;
				}
				if(@$request->email){
					$new['email'] = $request->email;
				}
				if(@$request->profile_pic){
					$pic   = $request->profile_pic;
	                $name = time().'.'.$pic->getClientOriginalExtension();
	                $pic->move('storage/app/public/sub_admin_pics/', $name);
					$new['profile_pic'] = $name;
				}
				if(@$request->password){
					$new['password'] = Hash::make($request->password);
				}
				DB::transaction(function() use($new,$request,$id,$sub_admin,$menus){
					// $new['type'] = 'S';
					// $new['status'] = 'A';
					Admin::where('id',$id)->update($new);
					Permission::where('admin_id',$id)->delete();
					if(@$sub_admin){
						if(@$request->permission){
							foreach(@$request->permission as $value){
								$sidebar['admin_id'] = $sub_admin->id;
								$sidebar['menu_id'] = $value;
								$access = Permission::create($sidebar);
							}
						}
						session()->flash("success",__('success.-4020'));
					}else{
						session()->flash("error",__('errors.-5020'));
					}
				});
			
			return redirect()->route('admin.list.subadmin');
			}
            // dd(@$sub_admin,@$sub_admins,@$menus);
			return view('admin.modules.sub_admin.edit_subadmin')->with([
			'sub_admin'=>@$sub_admin,
			'sub_admins'=>@$sub_admins,
			'menus'=>@$menus
			]);
		}else{
			session()->flash("error",__('errors.-5020'));
			return redirect()->back();
		}
	}
	/*
    * Method        : statusChange
    * Description   : This method is used to status change of subadmin.
    * Author        : Jayatri
    * Date 			: 27/01/2020
    */
    public function statusChange($id){
    	if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                            ->where('id', auth::guard('admin')->user()->id)
                            ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(1, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
    	$admin = Admin::where('id',$id)->first();
    	if(@$admin){
    		if($admin->status == 'A'){
    			$update['status'] = 'I';
    		}else{
    			$update['status'] = 'A';
    		}
			Admin::where('id',$id)->update($update);
			session()->flash("success",__('success.-4019'));
		}else{

			session()->flash("error",__('errors.-5019'));
		}
		return redirect()->back();
    }
    /*
    * Method        : updateStatus
    * Description   : This method is used to status change of subadmin.
    * Author        : Jayatri
    * Date          : 27/01/2020
    */
    public function updateStatus(Request $request){
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                            ->where('id', auth::guard('admin')->user()->id)
                            ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                }  
                //dd($permission);
                if(in_array(1, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
        $id = @$request->id;
        $admin = Admin::where('id',$id)->first();
        if(@$admin){
            if($admin->status == 'A'){
                $update['status'] = 'I';
            }else{
                $update['status'] = 'A';
            }
            Admin::where('id',$id)->update($update);
            $admin = Admin::where('id',$id)->first();
            return response()->json($admin);
            // session()->flash("success",__('success.-4019'));
        }else{

            // session()->flash("error",__('errors.-5019'));
            return 0;
        }
        // return redirect()->back();
    }
     /*
    * Method: checkAdminDuplicateEmail
    * Description: This method is check Sub-Admin Duplicate Email.
    * Author: Jayatri
    */
    public function checkAdminDuplicateEmail(Request $request){
        $user = Admin::where(["email"=>@$request->email])->first();
        if(@$user){
            return 1;
        }else{
            return 0;
        }
    }
    /*
    * Method: checkAdminEditChangeEmail
    * Description: This method is check Admin Edit Change Email.
    * Author: Jayatri
    */
    public function checkSubAdminEditChangeEmail(Request $request){
        if(@$request->all()){
            $user = Admin::where("email",'!=',@$request->old_email);
            $user = $user->where(["email"=>@$request->email])->first();
            if(@$user){
                return 1;
            }else{
                return 0;
            }
        }
    }
    /*
    * Method: checkAdminEditChangePhone
    * Description: This method is check check Admin Edit Change Phone.
    * Author: Jayatri
    */
    public function checkSubAdminEditChangePhone(Request $request){
        if(@$request->all()){
            $user = Admin::where("phone",'!=',@$request->old_phone);
            $user = $user->where(["phone"=>@$request->phone])->first();
            if(@$user){
                return 1;
            }else{
                return 0;
            }
        }
    }
    /*
    * Method: checkAdminDuplicateEmail
    * Description: This method is check Sub-Admin Duplicate Email.
    * Author: Jayatri
    */
    public function verifyDuplicatePhone(Request $request){
        $user = Admin::where(["phone"=>@$request->phone])->first();
        if(@$user){
            return 1;
        }else{
            return 0;
        }
    }
	/*
    * Method        : delete
    * Description   : This method is used to delete subadmin.
    * Author        : Jayatri
    * Date 			: 25/01/2020
    */
	public function delete($id){
		if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                            ->where('id', auth::guard('admin')->user()->id)
                            ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                }  
                //dd($permission);
                if(in_array(4, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
		$admin = Admin::where('id',$id)->first();
		if(@$admin){
			Permission::where('admin_id',$admin->id)->delete();
			Admin::where('id',$id)->delete();
			session()->flash("success",__('success.-4015'));
		}else{

			session()->flash("error",__('errors.-5016'));
		}
		return redirect()->back();
	}
    /*
    * Method        : deleteSubadmin
    * Description   : This method is used to delete subadmin.
    * Author        : Jayatri
    * Date          : 25/01/2020
    */
    public function deleteSubadmin(Request $request){
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                            ->where('id', auth::guard('admin')->user()->id)
                            ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                }  
                //dd($permission);
                if(in_array(4, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
        $id = @$request->id;
        $admin = Admin::where('id',$id)->first();
        if(@$admin){
            Permission::where('admin_id',$admin->id)->delete();
            Admin::where('id',$id)->delete();
            // session()->flash("success",__('success.-4015'));
            return 1;
        }else{

            // session()->flash("error",__('errors.-5016'));
            return 0;
        }
        // return redirect()->back();
    }
    private function checkLoginAccess(){
        if(!@Auth::guard('admin')->user()){
            return redirect()->route('admin.login');
        }
    }

    public function showLogs(Request $request)
    {
        if(@$request->all()){
            $log_detail = Admin_activities::where('admin_id',$request->id);
            $pp = $log_detail;
            $pp = $pp->count();
            //dd(@$request->all());
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

            if(@$request->columns['1']['search']['value']){
				$log_detail = $log_detail->where(function($query) use($request){
                $query->Where('created_at','like','%'.@$request->columns['1']['search']['value'].'%')
                ->orWhere('ip','like','%'.@$request->columns['1']['search']['value'].'%')
                ->orWhere('login_location','like','%'.@$request->columns['1']['search']['value'].'%')
                ->orWhere('browser', 'LIKE', "%".@$request->columns['1']['search']['value']."%");
                });
            }
            
             //sorting
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $log_detail = $log_detail->orderBy('created_at','asc');
                } else {
                    $log_detail = $log_detail->orderBy('created_at','desc');
                }
            }
            // sorting
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $log_detail = $log_detail->orderBy('ip','asc');
                } else {
                    $log_detail = $log_detail->orderBy('ip','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $log_detail = $log_detail->orderBy('device','asc');
                } else {
                    $log_detail = $log_detail->orderBy('device','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $log_detail = $log_detail->orderBy('login_location','asc');
                } else {
                    $log_detail = $log_detail->orderBy('login_location','desc');
                }
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $log_detail = $log_detail->orderBy('browser','asc');
                } else {
                    $log_detail = $log_detail->orderBy('browser','desc');
                }
            }
            $ppp = $log_detail;
            $ppp = $ppp->count();
            $log_detail = $log_detail->skip($request->start)->take($request->length)->get();
            $data['aaData'] = $log_detail;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $pp;
            $data["iTotalDisplayRecords"] = $ppp;
            return response()->json($data);
		}else{
            
            return view('admin.modules.sub_admin.subadmin_logs');
        }
    }
}
