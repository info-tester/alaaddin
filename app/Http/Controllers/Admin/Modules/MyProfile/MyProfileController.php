<?php

namespace App\Http\Controllers\Admin\Modules\MyProfile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductVariantDetail;
use Validator;
use Auth;
use App\Admin;
use App\Models\Menu;
use App\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Mail;
use App\Mail\SubAdminDetailsMailSend;
use App\Repositories\AdminRepository;
use App\Repositories\SettingRepository;

class MyProfileController extends Controller
{
    public function __construct(
    							AdminRepository $admins,
    							SettingRepository $setting)
	{
		$this->middleware('admin.auth:admin');
		$this->admins = $admins;
		$this->setting = $setting;
		
	}
	public function index(Request $request){
		if(@$request->all()){
			$admin = $this->admins->where('id',Auth::guard('admin')->user()->id)->first();
			if(@$admin){
            $request->validate([
                'profile_pic'=>'image|mimes:jpeg,jpg,png',
                'fname'=>'required',
                'lname'=>'required',
                'email'=>'required|email',
                'phone'=>'required|digits_between:10,12',
            ],[
                'profile_pic.image'=>'Please upload category picture with extension jpg/jpeg/png'
            ]);

			$update['fname'] = @$request->fname;
			$update['lname'] = @$request->lname;
			$update['email'] = @$request->email;
			$update['phone'] = @$request->phone;
			if(@Auth::guard('admin')->user()->type == 'A'){
				$update['email_1'] = @$request->email_1;
				$update['email_2'] = @$request->email_2;
				$update['email_3'] = @$request->email_3;
				$setting = $this->setting->where('id','!=',0)->first();
				if(@$setting){
					$upd['email_1'] = @$request->email_1;
					$upd['email_2'] = @$request->email_2;
					$upd['email_3'] = @$request->email_3;
					$this->setting->where('id','!=',0)->update($upd);
				}else{
					session()->flash("error",__('errors.-5057'));
					return redirect()->back();
				}
			}
			if(@$request->profile_pic){
                $pic   = $request->profile_pic;
                $name  = time().'.'.$pic->getClientOriginalExtension();
                $pic->move('storage/app/public/sub_admin_pics/', $name);
                $update['profile_pic'] = $name;
            }
            if(@$request->nw_password){
            	$password = @$request->nw_password;
            	$update['password'] = \Hash::make($password);	
            }
            

			$this->admins->where('id',Auth::guard('admin')->user()->id)->update($update);
			session()->flash("success",__('success.-4059'));
			
			}else{
				session()->flash("error",__('errors.-5057'));
			}
			return redirect()->back();
		}else{
			return view('admin.modules.my_profile.my_profile');	
		}
		
	}
	public function checkAdminDuplicateEmail(Request $request){
		if(@$request->all()){
			$admin = $this->admins->where('email','!=',@$request->old_email);
			$admin = $admin->where('email',@$request->email)->first();
			if(@$admin){
				return 0;
			}else{
				return 1;
			}
		}else{
			return 0;
		}
		
	}
	public function checkAdminDuplicatePhone(Request $request){
		if(@$request->all()){
			
			$admin = $this->admins->where('phone','!=',@$request->old_phone);
			$admin = $admin->where('phone',@$request->phone)->first();
			if(@$admin){
				return 0;
			}else{
				return 1;
			}
		}else{
			return 0;
		}
		
	}
}
