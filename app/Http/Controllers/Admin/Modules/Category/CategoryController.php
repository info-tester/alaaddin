<?php

namespace App\Http\Controllers\Admin\Modules\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryDetail;
use App\Models\Language;
use App\Models\Variant;
use App\Models\VariantDetail;
use App\Models\VariantValue;
use App\Models\VariantValueDetail;
use App\Models\ProductCategory;
use App\Models\CartDetail;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Admin;
use App\Models\Setting;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }
    /*
    * Method        : storeCategoryJson
    * Description   : This method is used to store Category Json.
    * Author        : Jayatri
    * Date          : 25/01/2020
    */
    private function storeCategoryJson(){
        // $category = Category::with(['subCategories'=> function($query) {
        //     $query->where(function($q) {
        //         $q->where('status', 'A');
        //     });
        // },'subCategories.details','details']);
        $category = Category::where(function($q1)  {
            $q1 = $q1->where('status', 'A');
        });
        // $category = $category->whereHas('subCategories',function($q2)  {
        //     $q2 = $q2->where('status', 'A');
        // });
        $category = $category->where(function($q3) {
            $q3 = $q3->where('parent_id',0);
        });
        $category = $category->with('categoryByLanguage')->get();

        return $category->toJson();
    }
	/*
    * Method        : index
    * Description   : This method is used to show list of product category from category & category details table.
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function index(Request $request){
		$parent_category = Category::with([
								'subCategories',
								'categorySubcategoryDetails',
                                'categoryNameByLang',
                                'parentCategoryNameByLang',
								'parentCategoryDetails'
							])
							->where('status',"!=",'D');
		
		if(@$request->all()) {
            if(@$request->parent_category_id){
                /*$parent_category = $parent_category->where('id', $request->parent_category_id)->orWhere("parent_id", $request->parent_category_id);*/
                
                $parent_category = $parent_category->where(function($q1) use($request) {
                	$q1 = $q1->where('id', $request->parent_category_id)
            			->orWhere("parent_id", $request->parent_category_id);
                });
            }
            if(@$request->keyword){

                $parent_category = $parent_category->whereHas('categorySubcategoryDetails', function($q) use ($request){
					$q->where('title', 'like','%'.$request->keyword.'%');
				});
					
            }
            $key = $request->all();
        }
        $parent_category = $parent_category->orderBy('id','desc')->get();
        $categories = Category::with('categoryDetailsByLanguage')->where(['parent_id'=>0])->where('status',"!=",'D')->get();
		return view('admin.modules.product_settings.category.list_product_category')->with([
			// 'language'			=>@$language,
			'parent_category'	=>@$parent_category,
			'categories'		=>@$categories,
			'key'				=>@$key
		]);
    }
    
    /*
    * Method        : getCategorylist
    * Description   : This method is used to get category list.
    * Author        : Jayatri
    * Date          : 16/01/2020
    */
    public function searchCategory(Request $request){
        $parent_category = Category::with([
                                'subCategories',
                                'categorySubcategoryDetails',
                                'parentCategoryDetails'
                            ])
                            ->where('status',"!=",'D');
        if(@$request->parent_category_id){
            /*$parent_category = $parent_category->where('id', $request->parent_category_id)->orWhere("parent_id", $request->parent_category_id);*/
            $parent_category = $parent_category->where(function($q1) use($request) {
                $q1 = $q1->where('id', $request->parent_category_id)
                    ->orWhere("parent_id", $request->parent_category_id);
            });
        }
        if(@$request->keyword){
            $parent_category = $parent_category->whereHas('categorySubcategoryDetails', function($q) use ($request){
                $q->where('title', 'like','%'.$request->keyword.'%');
            });
        }
        $parent_category = $parent_category->orderBy('id','desc')->get();
        return view('admin.modules.product_settings.category.ajax_category_list')->with(['parent_category'=>@$parent_category]);

    }
    /*
    * Method        : searchCategoryDataTable for datatable
    * Description   : This method is used to get category list.
    * Author        : Jayatri
    * Date          : 16/01/2020
    */
    public function searchCategoryDataTable(Request $request){
        $columnIndex = $request->order[0]['column']; // Column index
        $columnName = $request->columns[$columnIndex]['data']; // Column name
        $columnSortOrder = $request->order[0]['dir']; // asc or desc

        $parent_category = Category::with([
                                'subCategories',
                                'categorySubcategoryDetails',
                                'categoryNameByLang',
                                'parentCategoryNameByLang',
                                'parentCategoryDetails'
                            ])
                            ->where('status',"!=",'D');

        $ps =  $parent_category->count();
        $pp = $parent_category;
        $pp = $pp->count();
        $limit = $request->length;
        if($request->all()){

            if(@$request->columns['2']['search']['value']){
                $parent_category = $parent_category->where(function($q1) use($request) {
                    $q1 = $q1->where('id', @$request->columns['2']['search']['value'])
                        ->orWhere("parent_id", @$request->columns['2']['search']['value']);
                });
            }
            if($request->columns['1']['search']['value']){
                $parent_category = $parent_category->whereHas('categorySubcategoryDetails', function($q) use ($request){
                    $q->where('title', 'like','%'.$request->columns['1']['search']['value'].'%');
                });
            }
            if(@$request->columns['3']['search']['value']){
                $parent_category = $parent_category->where(function($q1) use($request) {
                    $q1 = $q1->where('status', @$request->columns['3']['search']['value']);
                });
            }
            if(@$request->columns['4']['search']['value']){
                $parent_category = $parent_category->where(function($q1) use($request) {
                    $q1 = $q1->where('add_in_menu', @$request->columns['4']['search']['value']);
                });
            }
            if(@$request->columns['5']['search']['value']){
                $parent_category = $parent_category->where(function($q1) use($request) {
                    $q1 = $q1->where('is_popular', @$request->columns['5']['search']['value']);
                });
            }
            $pp = $parent_category;
            $pp = $pp->count();
            if($columnIndex == 0){
                $parent_category1 = $parent_category->get();
                if($columnSortOrder == 'asc') {
                    $parent_category2 = $parent_category1->sortBy(function($parent_category, $key) {
                        return $parent_category['categoryNameByLang']['title'];
                    });

                    $parent_category = $parent_category2->slice($request->start, $request->length)->values()->all();
                }
                else
                {
                    $parent_category2 = $parent_category1->sortByDesc(function($parent_category, $key) {
                        return $parent_category['categoryNameByLang']['title'];
                    });

                    $parent_category = $parent_category2->slice($request->start, $request->length)->values()->all();
                }
            }
            if($columnIndex == 1){
                $parent_category1 = $parent_category->get();
                if($columnSortOrder == 'asc') {
                    $parent_category2 = $parent_category1->sortBy(function($parent_category, $key) {
                        return $parent_category['categoryNameByLang']['title'];
                    });

                    $parent_category = $parent_category2->slice($request->start, $request->length)->values()->all();
                }
                else
                {
                    $parent_category2 = $parent_category1->sortByDesc(function($parent_category, $key) {
                        return $parent_category['categoryNameByLang']['title'];
                    });

                    $parent_category = $parent_category2->slice($request->start, $request->length)->values()->all();
                }
            }
            if($columnIndex == 2){
                $parent_category1 = $parent_category->get();
                if($columnSortOrder == 'asc') {
                    $parent_category2 = $parent_category1->sortBy(function($parent_category, $key) {
                        return $parent_category['parentCategoryNameByLang']['title'];
                    });

                    $parent_category = $parent_category2->slice($request->start, $request->length)->values()->all();
                }
                else
                {
                    $parent_category2 = $parent_category1->sortByDesc(function($parent_category, $key) {
                        return $parent_category['parentCategoryNameByLang']['title'];
                    });

                    $parent_category = $parent_category2->slice($request->start, $request->length)->values()->all();
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $parent_category = $parent_category->orderBy('status','asc');
                } else {
                    $parent_category = $parent_category->orderBy('status','desc');
                }
                $parent_category = $parent_category->skip($request->start)->take($request->length)->get();
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $parent_category = $parent_category->orderBy('add_in_menu','asc');
                } else {
                    $parent_category = $parent_category->orderBy('add_in_menu','desc');
                }
                $parent_category = $parent_category->skip($request->start)->take($request->length)->get();
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $parent_category = $parent_category->orderBy('is_popular','asc');
                } else {
                    $parent_category = $parent_category->orderBy('is_popular','desc');
                }
                $parent_category = $parent_category->skip($request->start)->take($request->length)->get();
            }
            if($columnIndex == 6) {
                if($columnSortOrder == 'asc') {
                    $parent_category = $parent_category->orderBy('id','asc');
                } else {
                    $parent_category = $parent_category->orderBy('id','desc');
                }
                $parent_category = $parent_category->skip($request->start)->take($request->length)->get();
            }    

                // $parent_category = array_slice($parent_category, $request->start, $limit);
                // $p = array();
                // foreach ($parent_category as $value) {
                //     array_push($p, $value);
                // }
                // $parent_category = $pp;
            // } 
            // else {
                // $parent_category = $parent_category->orderBy($columnName, $columnSortOrder)->skip($request->start)->take($limit)->get()->toArray();
            // }
        }

        // end
        // $parent_category = $parent_category->orderBy('id','desc')->get();
        $data['aaData'] = $parent_category;
        $data["draw"] = intval($request->draw);
        $data['iTotalRecords'] = $ps;
        $data["iTotalDisplayRecords"] = $pp;
        return response()->json($data);
        // return view('admin.modules.product_settings.category.ajax_category_list')->with(['parent_category'=>@$parent_category]);

    }
    /*
    * Method        : checkDuplicateCategoryName
    * Description   : This method is used to check Duplicate Category Name
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function checkDuplicateCategoryName(Request $request){
        $category = CategoryDetail::with(['CategoryDet'])
                    ->where('title',$request->name)
                    ->whereHas('CategoryDet', function($q) use($request){
                        $q->where('parent_id', $request->cat_id)
                          ->where('status', '!=', 'D');
                    })
                    ->first();
        if(@$category){
            return 0;
        }else{
            return 1;
        }
    }
    /*
    * Method        : addCategory
    * Description   : This method is used to add product category & saved in category & category details table.
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function addCategory(Request $request){
    	$language = Language::where('status','A')->get();
		$category = Category::with('categoryDetailsByLanguage')->where(['parent_id'=>0])->where('status', '!=', 'D')->get();
    	if(@$request->all()){
    		
            DB::transaction(function() use($request,$category,$language){
                $cid = @$request->category;
                $cl = Language::where('status','A')->first();
                $t = "categoryName".strval($cl->id);
                settype($cid, "integer");
        		$new['parent_id'] = $cid;
                $slug = str_slug($request->$t);
                
        		$checkCategory = CategoryDetail::where('title',$request->$t)->whereHas('CategoryDet', function($query){
                        $query->where('status','!=','D');
                })->first();
                if(@$checkCategory){
                    session()->flash("error",__('errors.-5098'));
                    return redirect()->back();
                }
                
        		$new['status'] = 'A';
        		if(@$request->categoryPicture){
                    $pic   = $request->categoryPicture;
                    $name = time().'.'.$pic->getClientOriginalExtension();
                    $pic->move('storage/app/public/category_pics/', $name);
                    $new['picture'] = $name;
                }
                
                $categoryTbl = Category::create($new);

                $slug = str_slug($request->$t);
                $check_slug = Category::where('slug',$slug)->first();
                // if(@$check_slug){
                    $update['slug'] = str_slug($request->$t."_".$categoryTbl->id);
                // }else{
                    // $update['slug'] = str_slug($request->$t);
                // }
                Category::where('id',$categoryTbl->id)->update($update);
                
    		    foreach(@$language as $lang){
                	$details['category_id'] = $categoryTbl->id;
                	$details['language_id'] = $lang->id;
                	$title = "categoryName".strval($lang->id);
                	$details['title'] = $request->$title;
                	$desc = "desc_".strval($lang->id);
                	$details['description'] = $request->$desc;
                    // DB::transaction(function() use($details){
            		  $categoryDetailsTbl = CategoryDetail::create($details);
                    // });
            	}
    			if(@$categoryTbl){
                    $category_json = (string)($this->storeCategoryJson());
                    if(@$category_json){
                        Setting::where('id',1)->update(['category_json'=>$category_json]);
                    }
        			session()->flash("success",__('success.-1000'));
        			
    			}else{
        			session()->flash("error",__('errors.-32700'));
        			// return redirect()->route('admin.list.category');
    			}
            });
            return redirect()->route('admin.list.category');
    	}else{
        	return view('admin.modules.product_settings.category.add_product_category')->with(['language'=>@$language,'category'=>@$category]);
    	}
    }
    /*
    * Method        : editCategory
    * Description   : This method is used to edit product category details & updated in category & category details table.
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function editCategory(Request $request,$slug){
    	$cat_dtls = Category::with('details.languageDetails')->where(['slug'=>$slug])->first();
		$language = Language::where('status','A')->get();
		$category = Category::with('categoryDetailsByLanguage')->where(['parent_id'=>0])->whereNotIn('category_id',[$cat_dtls->id])->where('status', '!=', 'D')->get();

		if(@$cat_dtls){
            if($request->all()){
                $checkCategory = CategoryDetail::where('title',$request->categoryName[0])->whereHas('CategoryDet', function($query){
                        $query->where('status','!=','D');
                })->first();
                if(@$checkCategory){
                    session()->flash("error",__('errors.-5098'));
                    return redirect()->back();
                }
                DB::transaction(function() use($request,$category,$language,$cat_dtls,$slug){
                // if(@$request->category){
                //     $updateCategory['parent_id'] = $request->category;
                // }
                if(@$request->categoryPicture){
                    $pic   = $request->categoryPicture;
                    $name = time().'.'.$pic->getClientOriginalExtension();
                    $pic->move('storage/app/public/category_pics/', $name);
                    $updateCategory['picture'] = $name;
                    Category::where(['slug'=>$slug])->update($updateCategory);
                }
                
                $cdetails = CategoryDetail::where(['category_id'=>$cat_dtls->id])->get();
                foreach($cdetails as $key=>$details){
                    $checkCategory = CategoryDetail::where('title',$request->categoryName[$key])->whereHas('CategoryDet', function($query){
                        $query->where('status','!=','D');
                    })->where('id','!=',@$details->id)->first();
                    if(!empty($checkCategory)){
                        session()->flash("error",__('errors.-5098'));
                        return redirect()->back();
                    }
                    $updateCDetails['title'] = $request->categoryName[$key];
                    $updateCDetails['description'] = $request->desc[$key];
                    CategoryDetail::where(['category_id'=>$cat_dtls->id,'language_id'=>@$language[$key]->id])->update($updateCDetails);
                }
                $category_json = (string)($this->storeCategoryJson());
                if(@$category_json){
                    Setting::where('id',1)->update(['category_json'=>$category_json]);
                }
                //updated
                session()->flash("success",__('success.-4016'));
                });
                return redirect()->route('admin.list.category');
            }else{

                return view('admin.modules.product_settings.category.edit_product_category')->with([
                    'language'=>@$language,
                    'category'=>@$category,
                    'cat_dtls'=>@$cat_dtls
                ]);

            }
		}else{
            //unauthorized access
            session()->flash("error",__('error.-5002'));
			return redirect()->back();
		}
    	
    	
    }
    /*
    * Method        : deleteCategory
    * Description   : This method is used to delete category & category details.
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function deleteCategory($slug){
        $category = Category::where(['slug'=>@$slug])->first();
        $flag = 0;
        $isDelete = 0;
        if(@$category){
            $subCategory = CategoryDetail::where('category_id',$category->id)->first();
            if(@$subCategory){
                if(@$category->parent_id != 0){ //only for subcategory(delete)
                    $variant = Variant::where('sub_category_id',$category->id)->first();
                    $product = ProductCategory::where('category_id',$category->id)->first();
                    if(@$variant){
                        $flag = 1;
                    }elseif(@$product){
                        $flag = 1;
                    }
                    //delete start
                    if($flag == 0){
                        DB::transaction(function() use($slug,$category){
                            //subcategory delete
                            CategoryDetail::where('category_id',$category->id)->update(['status' => 'D','add_in_menu' => 'N','is_popular' => 'N']);
                        });
                        $category_json = (string)($this->storeCategoryJson());
                        if(@$category_json){
                            Setting::where('id',1)->update(['category_json'=>$category_json]);
                        }
                        //subcategory deleted
                        session()->flash("success",__('success.-4000'));
                        return redirect()->back();
                    }else{
                        //error due to related rows in another tables
                        session()->flash("error",__('errors.-5004'));
                        return redirect()->back();
                    }
                }else{ 
                    //parent category delete
                    $hasSubCategory = Category::where(['parent_id'=>$category->id])->where('status', '!=', 'D')->count();
                    if($hasSubCategory == 0){
                        // $isDelete = 0;
                        $brand = Brand::where('category_id',$category->id)->first();
                        $product_categories = ProductCategory::where('category_id',$category->id)->first();
                        $variant = Variant::where('category_id',$category->id)->first();

                        if(@$brand){
                            $isDelete = 1;
                        }elseif(@$product_categories){
                            $isDelete = 1;
                        }elseif(@$variant){
                            $isDelete = 1;
                        }
                        if($isDelete == 0){
                            //delete start
                            DB::transaction(function() use($slug,$category){
                                CategoryDetail::where('category_id',$category->id)->update(['status' => 'D','add_in_menu' => 'N','is_popular' => 'N']);
                            });
                            $category_json = (string)($this->storeCategoryJson());
                            if(@$category_json){
                                Setting::where('id',1)->update(['category_json'=>$category_json]);
                            }
                            session()->flash("success",__('success.-4000'));
                            return redirect()->back();
                        }else{
                            //if related rows exist in another tables
                            session()->flash("error",__('errors.-5004'));
                            return redirect()->back();
                        }
                        //successfully deleted because this category has no subcategory
                    }else{
                        //subcategory exist under this category! couldnot delete!
                        session()->flash("error",__('errors.-5004'));
                        return redirect()->back();
                    }
                }
            }else{
                //if category details not found from category_details table
                session()->flash("error",__('errors.-5004'));
                return redirect()->back();
            }
        }
    }

    /*
    * Method        : categoryDelete by ajax call
    * Description   : This method is used to delete category & category details.
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function categoryDelete(Request $request){
        $slug = @$request->slug;
        $category = Category::where(['slug'=>@$slug])->first();
        $flag = 0;
        $isDelete = 0;
        if(@$category){
            $subCategory = CategoryDetail::where('category_id',$category->id)->first();
            if(@$subCategory){
                if(@$category->parent_id != 0){ //only for subcategory(delete)
                    $variant = Variant::where('sub_category_id',$category->id)->where('status', '!=', 'D')->first();
                    $product = ProductCategory::with('notDeletedProducts')->whereHas('notDeletedProducts', function($q) {
                        $q->where('status', '!=', 'D');
                    })
                    ->where('category_id',$category->id)
                    ->first();

                    if(@$variant){
                        $flag = 1;
                    }elseif(@$product){
                        $flag = 1;
                    }
                    //delete start
                    if($flag == 0){
                        DB::transaction(function() use($slug,$category){
                            //subcategory delete
                            Category::with('details','brand','variant')->where(['slug'=>@$slug])->update(['status' => 'D','add_in_menu' => 'N','is_popular' => 'N']);
                        });
                        $category_json = (string)($this->storeCategoryJson());
                        if(@$category_json){
                            Setting::where('id',1)->update(['category_json'=>$category_json]);
                        }
                        $data['result'] = 1;
                        //subcategory deleted
                        return $data;
                    }else{
                        
                        $data['no_sub_count'] = 0;//as it is subcategory
                        $data['variant_count'] = Variant::where('sub_category_id',$category->id)->where('status', '!=', 'D')->count();
                        $data['product_categories_count'] = ProductCategory::with('notDeletedProducts')
                        ->whereHas('notDeletedProducts', function($q) {
                            $q->where('status', '!=', 'D');
                        })
                        ->where('category_id',$category->id)
                        ->count();
                        $data['result'] = 0;
                        $data['brand_count'] = 0;
                        $data['cat_type'] = 'S';
                        //error due to related rows in another tables
                        return $data;
                    }
                } else { 
                    //parent category delete
                    $hasSubCategory = Category::where(['parent_id'=>$category->id])->where('status', '!=', 'D')->count();
                    if($hasSubCategory == 0){
                        $pu = ProductCategory::where(['category_id'=>$category->id])->pluck('product_id')->toArray();
                        $parr = Product::whereNotIn('status',['D'])->whereIn('id',$pu)->pluck('id')->toArray();
                        $ccount = CartDetail::whereIn('product_id',$parr)->count();
                        
                        if($ccount > 0){
                            $data['result'] = 0;
                            return $data;
                        }
                        $brand = Brand::where('category_id',$category->id)->where('status', '!=', 'D')->first();
                        $product_categories = ProductCategory::with('notDeletedProducts')->whereHas('notDeletedProducts', function($q) {
                            $q->where('status', '!=', 'D');
                        })
                        ->where('category_id',$category->id)
                        ->first();
                        $variant = Variant::where('category_id',$category->id)->where('status', '!=', 'D')->first();
                        if(@$brand){
                            $isDelete = 1;
                        }elseif(@$product_categories){
                            $isDelete = 1;
                        }elseif(@$variant){
                            $isDelete = 1;
                        }
                        if($isDelete == 0){
                            //delete start
                            DB::transaction(function() use($slug,$category){
                                Category::with('details')->where(['slug'=>@$slug])->update(['status' => 'D','add_in_menu' => 'N','is_popular' => 'N']);
                            });
                            $category_json = (string)($this->storeCategoryJson());
                            if(@$category_json){
                                Setting::where('id',1)->update(['category_json'=>$category_json]);
                            }
                            
                            $data['result'] = 1;
                            return $data;
                        }else{
                            //if brand and product category variant has related to it.
                            $data['variantCount'] = 0;
                            $data['productCount'] = 0;
                            $data['brand_count'] = Brand::where('category_id',$category->id)->where('status', '!=', 'D')->count();
                            $data['no_sub_count'] = 0;
                            $data['product_categories_count'] = ProductCategory::with('notDeletedProducts')
                            ->whereHas('notDeletedProducts', function($q) {
                                $q->where('status', '!=', 'D');
                            })
                            ->where('category_id',$category->id)
                            ->count();
                            $data['variant_count'] = Variant::where('category_id',$category->id)->where('status', '!=', 'D')->count();
                            $data['result'] = 0;
                            $data['cat_type'] = 'P';
                            return $data;
                        }
                        //successfully deleted because this category has no subcategory
                    }else{
                        //no of subcategory
                        $data['brand_count'] = 0;
                        $data['no_sub_count'] = $hasSubCategory;
                        $data['result'] = 0;
                        $data['cat_type'] = 'P';
                        return $data;
                        //subcategory exist under this category! couldnot delete!
                    }
                }
            }else{
                //server table data error
                //if server error beacuse of category details not found so if it exist in category table
                $data['result'] = 500;
                return $data;
                //if category details not found from category_details table
            }
            
        }
    }
    /*
    * Method        : popularCategory
    * Description   : This method is used to popularCategory
    * Author        : Jayatri
    * Date          : 16/01/2020
    */
    public function popularCategory($slug){
        $category = Category::where('slug',$slug)->first();
        $categoryDetails = CategoryDetail::where('category_id',$category->id)->first();
        // dd($category);
        if(@$category){
            if(@$categoryDetails){
                if(@$category->is_popular == 'Y'){
                    $update['is_popular'] = 'N';
                }else if(@$category->is_popular == 'N'){
                    $update['is_popular'] = 'Y';
                }
                DB::transaction(function() use($slug,$update){
                Category::where('slug',$slug)->update($update);
                });
                $category_json = (string)($this->storeCategoryJson());
                if(@$category_json){
                    Setting::where('id',1)->update(['category_json'=>$category_json]);
                }
                session()->flash("success",__('success.-4017'));
                    return redirect()->back();
            }else{
                session()->flash("error",__('error.-5018'));
                return redirect()->back();
            }
        }else{
            session()->flash("error",__('error.-5018'));
            return redirect()->back();
        }
        return redirect()->back();
    }
    /*
    * Method        : updatePopularCategory
    * Description   : This method is used to make changes(yes/no) popular Category to show in front end
    * Author        : Jayatri
    * Date          : 16/01/2020
    */
    public function updatePopularCategory(Request $request){
        $slug = @$request->slug;
        $category = Category::where('slug',$slug)->first();
        $categoryDetails = CategoryDetail::where('category_id',$category->id)->first();
        // dd($category);
        if(@$category){
            if(@$categoryDetails){
                if(@$category->is_popular == 'Y'){
                    $update['is_popular'] = 'N';
                }else if(@$category->is_popular == 'N'){
                    $update['is_popular'] = 'Y';
                }
                DB::transaction(function() use($slug,$update){
                Category::where('slug',$slug)->update($update);
                });
                $category_json = (string)($this->storeCategoryJson());
                if(@$category_json){
                    Setting::where('id',1)->update(['category_json'=>$category_json]);
                }
                // session()->flash("success",__('success.-4017'));
                    // return redirect()->back();
                $category = Category::where('slug',$slug)->first();
                return response()->json($category);
            }else{
                // session()->flash("error",__('error.-5018'));
                // return redirect()->back();
                return 0;
            }
        }else{
            // session()->flash("error",__('error.-5018'));
            // return redirect()->back();
            return 0;
        }
        // return redirect()->back();
    }
    /*
    * Method        : AddInMenu
    * Description   : This method is used to AddInMenu
    * Author        : Jayatri
    * Date          : 16/01/2020
    */
    public function AddInMenu($slug){
        $category = Category::where('slug',$slug)->first();
        $categoryDetails = CategoryDetail::where('category_id',$category->id)->first();
        // dd($category);
        if(@$category){
            if(@$categoryDetails){
                if(@$category->add_in_menu == 'Y'){
                    $update['add_in_menu'] = 'N';
                }else if(@$category->add_in_menu == 'N'){
                    $update['add_in_menu'] = 'Y';
                }
                // DB::transaction(function() use($slug,$update){
                Category::where('slug',$slug)->update($update);
                Category::where('parent_id',$category->id)->update($update);
                // dd(Category::where('parent_id',$category->id)->get());
                // });
                $category_json = (string)($this->storeCategoryJson());
                if(@$category_json){
                    Setting::where('id',1)->update(['category_json'=>$category_json]);
                }
                session()->flash("success",__('success.-4017'));
                    return redirect()->back();
            }else{
                session()->flash("error",__('error.-5018'));
                return redirect()->back();
            }
        }else{
            session()->flash("error",__('error.-5018'));
            return redirect()->back();
        }
        return redirect()->back();
    }
    /*
    * Method        : UpdateAddInMenu
    * Description   : This method is used to Add In Menu for frontend category show (only parent category)
    * Author        : Jayatri
    * Date          : 16/01/2020
    */
    public function UpdateAddInMenu(Request $request){
        $slug = $request->slug;
        $category = Category::where('slug',$slug)->first();
        $categoryDetails = CategoryDetail::where('category_id',$category->id)->first();
        // dd($category);
        if(@$category){
            if(@$categoryDetails){
                if(@$category->add_in_menu == 'Y'){
                    $update['add_in_menu'] = 'N';
                }else if(@$category->add_in_menu == 'N'){
                    $update['add_in_menu'] = 'Y';
                }
                // DB::transaction(function() use($slug,$update){
                Category::where('slug',$slug)->update($update);
                Category::where('parent_id',$category->id)->update($update);
                // });
                $category_json = (string)($this->storeCategoryJson());
                if(@$category_json){
                    Setting::where('id',1)->update(['category_json'=>$category_json]);
                }
                // session()->flash("success",__('success.-4017'));
                    // return redirect()->back();
                $category = Category::where('slug',$slug)->first();
                return response()->json($category);
            }else{
                // session()->flash("error",__('error.-5018'));
                // return redirect()->back();
                return 0;
            }
        }else{
            // session()->flash("error",__('error.-5018'));
            // return redirect()->back();
            return 0;
        }
        // return redirect()->back();
    }
    /*
    * Method        : changeStatus
    * Description   : This method is used to change Status.
    * Author        : Jayatri
    * Date          : 16/01/2020
    */
    public function changeStatus($slug){
        $category = Category::where('slug',$slug)->first();
        $categoryDetails = CategoryDetail::where('category_id',$category->id)->first();
        // dd($category);
        if(@$category){
            if(@$categoryDetails){
                if(@$category->status == 'A'){
                    $update['status'] = 'I';
                }else if(@$category->status == 'I'){
                    $update['status'] = 'A';
                }
                DB::transaction(function() use($slug,$update){
                Category::where('slug',$slug)->update($update);
                });
                $category_json = (string)($this->storeCategoryJson());
                if(@$category_json){
                    Setting::where('id',1)->update(['category_json'=>$category_json]);
                }
                session()->flash("success",__('success.-4017'));
                    return redirect()->back();
            }else{
                session()->flash("error",__('error.-5018'));
                return redirect()->back();
            }
        }else{
            session()->flash("error",__('error.-5018'));
            return redirect()->back();
        }
        return redirect()->back();
    }
    /*
    * Method        : updateStatus
    * Description   : This method is used to update Status.
    * Author        : Jayatri
    * Date          : 16/01/2020
    */
    public function updateStatus(Request $request){
        
        $slug = @$request->slug;
        $category = Category::where('slug',$slug)->first();
        $categoryDetails = CategoryDetail::where('category_id',$category->id)->first();
        
        if(@$category){
            if(@$categoryDetails){
                if(@$category->status == 'A'){
                    
                    $pu = ProductCategory::where(['category_id'=>$category->id])->pluck('product_id')->toArray();
                    $parr = Product::whereNotIn('status',['D'])->whereIn('id',$pu)->pluck('id')->toArray();
                    $ccount = CartDetail::whereIn('product_id',$parr)->count();
                    
                    // if($ccount > 0){
                    //     return 0;
                    // }

                    $update['status'] = 'I';
                    $update['add_in_menu'] = 'N';
                    $update['is_popular'] = 'N';
                }else if(@$category->status == 'I'){
                    $update['status'] = 'A';
                }
                DB::transaction(function() use($slug,$update){
                Category::where('slug',$slug)->update($update);
                });
                $category_json = (string)($this->storeCategoryJson());
                if(@$category_json){
                    Setting::where('id',1)->update(['category_json'=>$category_json]);
                }
                $category = Category::where('slug',$slug)->first();
                return response()->json($category);
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
}
