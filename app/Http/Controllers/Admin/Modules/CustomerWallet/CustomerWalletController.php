<?php

namespace App\Http\Controllers\Admin\Modules\CustomerWallet;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wallet;
use App\Models\WalletDetails;
use App\Models\City;
use App\User;
use App\Merchant;
use App\Models\Product;
use App\Models\OrderMaster;
use App\Models\OrderSeller;
use App\Models\OrderDetail;

class CustomerWalletController extends Controller
{
    //
    public function customerWalletHistory(Request $request){
        $data['customers'] = User::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        $data['cities'] = City::whereNotIn('status',['D'])->orderBy('name','asc')->get();
        $data['merchants'] = Merchant::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        return view('admin.modules.customer_wallet.customer_wallet')->with($data);
    }
    public function customerSpecificWalletHistory($id,Request $request){
        $data['customers'] = User::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        $data['cities'] = City::whereNotIn('status',['D'])->orderBy('name','asc')->get();
        $data['merchants'] = Merchant::whereNotIn('status',['D'])->orderBy('fname','asc')->get();
        $data['id'] = $id;
        $data['user'] =  User::where('id',$id)->first();
        return view('admin.modules.customers.customer_specific_wallet_history')->with($data);
    }
    public function viewWalletDetails(Request $request){
        $columnIndex = $request->order[0]['column'];
        $columnSortOrder = $request->order[0]['dir'];

        $o = WalletDetails::with(['get_order_master','view_user','product_by_language'])->where('id','!=',0);
            
            if(@$request->columns['0']['search']['value']){
                $o = $o->whereHas('get_order_master',function($q1) use($request) {
                    $q1 = $q1->where('order_no','like','%'.$request->columns['0']['search']['value'].'%');
                });
            }
            if(@$request->columns['1']['search']['value']){
                $o = $o->whereHas('view_user',function($q1) use($request) {
                    $q1 = $q1->where('id',$request->columns['1']['search']['value']);
                });
            }
            
            $ps =  $o->count();
            $pp = $o;
            $pp = $pp->count();
            $limit = $request->length;
            $p = $o = $o;
            $p =  $p->count();
            if($columnIndex == 0) {

                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('id','asc');
                } else {
                    $o = $o->orderBy('id','desc');
                }
                
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('order_id','asc');
                } else {
                    $o = $o->orderBy('order_id','desc');
                }
                
            }
            if($columnIndex == 2) {
                
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('id','asc');
                } else {
                    $o = $o->orderBy('id','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('id','asc');
                } else {
                    $o = $o->orderBy('id','desc');
                }
                
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('amount','asc');
                } else {
                    $o = $o->orderBy('amount','desc');
                }
                
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('w_d_date','asc');
                } else {
                    $o = $o->orderBy('w_d_date','desc');
                }
                
            }
            
            $o = $o->orderBy('id','desc')->skip($request->start)->take($request->length)->get();
            $data['aaData'] = $o;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $ps;
            $data["iTotalDisplayRecords"] = $pp;
            return response()->json($data);

    }
    public function customerSpecificviewWalletDetails($id,Request $request){
        $columnIndex = $request->order[0]['column'];
        $columnSortOrder = $request->order[0]['dir'];

        $o = WalletDetails::with(['get_order_master','view_user','product_by_language'])->where('user_id',$id);
            
            if(@$request->columns['0']['search']['value']){
                $o = $o->whereHas('get_order_master',function($q1) use($request) {
                    $q1 = $q1->where('order_no','like','%'.$request->columns['0']['search']['value'].'%');
                });
            }
            
            $ps =  $o->count();
            $pp = $o;
            $pp = $pp->count();
            $limit = $request->length;
            $p = $o = $o;
            $p =  $p->count();
            if($columnIndex == 0) {

                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('id','asc');
                } else {
                    $o = $o->orderBy('id','desc');
                }
                
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('order_id','asc');
                } else {
                    $o = $o->orderBy('order_id','desc');
                }
                
            }
            if($columnIndex == 2) {
                
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('id','asc');
                } else {
                    $o = $o->orderBy('id','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('id','asc');
                } else {
                    $o = $o->orderBy('id','desc');
                }
                
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('amount','asc');
                } else {
                    $o = $o->orderBy('amount','desc');
                }
                
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $o = $o->orderBy('w_d_date','asc');
                } else {
                    $o = $o->orderBy('w_d_date','desc');
                }
            }
            
            $o = $o->orderBy('id','desc')->skip($request->start)->take($request->length)->get();
            $data['aaData'] = $o;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $ps;
            $data["iTotalDisplayRecords"] = $pp;
            return response()->json($data);

    }
    public function manageCustomerWithdraw(Request $request){

    }
}
