<?php

namespace App\Http\Controllers\Admin\Modules\Invoice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\InvoiceMaster;
use App\Models\InvoiceDetail;
use App\Merchant;
use Auth;
use Excel;
class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->checkLoginAccess();
        $this->middleware('admin.auth:admin');
    }

    /*
    * Method: addInvoice
    * Description: This method is used to add new invoicde
    * Author : Argha
    * Date   : 2020-10-20
    */

    public function addInvoice()
    {
        $data['merchants'] = Merchant::where('status','A')->get();
        return view('admin.modules.invoice.add_invoice')->with($data);
    }

    /*
    * Method: getAllMerchant
    * Description: This method is used to get the merchants
    * Author : Argha
    * Date   : 2020-10-20
    */
    public function getAllMerchant(Request $request)
    {
        $response['jsonrpc'] = '2.0';
        $mer_name = $request['merchant'];
        $merchant = Merchant::where(\DB::raw("CONCAT(`fname`,' ',`lname`)"),'like','%'.$mer_name.'%')
                            ->where('status','A')
                            ->get();
        $response['merchant'] = $merchant;
        return response()->json($response);
    }

    /*
    * Method: insertInvoice
    * Description: This method is used to insert invoice
    * Author : Argha
    * Date   : 2020-10-20
    */

    public function insertInvoice(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'invoice_type' => 'required',
            'invoice_date' => 'required',
        ]);

        $price = $request->price;
        $product_name = $request->product_name;
        $qty = $request->qty;

        $total = 0;
        for($i =0;$i<=$request->product_count-1;$i++){
            $total = $total + ($price[$i] * $qty[$i]);
        }

        if($request->merchant_id){
            // $merchant = Merchant::where(\DB::raw("CONCAT(`fname`,' ',`lname`)"),'like','%'.$request->merchant_id.'%')
            //                 ->where('status','A')
            //                 ->first();
            $merchant = Merchant::where('id',$request->merchant_id)
                            ->where('status','A')
                            ->first();
            if($merchant){
                $ins['name'] = @$merchant->fname.' '.@$merchant->fname;
                $merchant_id  = @$merchant->id;
                if($request->invoice_type == 'I'){
                    $merchant_up['total_due'] = $merchant->total_due + $total;
                    $merchant_up['invoice_in'] = $merchant->invoice_in +$total;
                }else{
                    $merchant_up['total_due'] = $merchant->total_due - $total;
                    $merchant_up['invoice_out'] = $merchant->invoice_out +$total;
                }
                Merchant::where('id',$merchant_id)->update($merchant_up);
            }
        }else{
            $ins['name'] = @$request->name;
        }
        $ins['invoice_no'] = $request->invoice_no;
        $ins['merchant_id'] = @$merchant_id;
        //$ins['name'] = @$request->name;
        $ins['total'] = $total;
        $ins['invoice_type'] = $request->invoice_type;
        $ins['invoice_date'] = $request->invoice_date ? date('Y-m-d H:i:s',strtotime($request->invoice_date)):NULL;
        $ins['applicable_to'] = $request->applicable_to;
        
        $insInvoice  = InvoiceMaster::create($ins);
        if($request->applicable_to == 'M'){
            $invoice_no  = "M".time().$insInvoice->id;
        }else{
            $invoice_no  = "O".time().$insInvoice->id;
        }
        InvoiceMaster::where('id',$insInvoice->id)->update(['invoice_no' => $invoice_no]);
        for($i =0;$i<=$request->product_count-1;$i++){
            $subtotal = ($price[$i] * $qty[$i]);

            $insDetails['invoice_master_id'] = $insInvoice->id;
            $insDetails['product_name'] = $product_name[$i];
            $insDetails['qty'] = $qty[$i];
            $insDetails['price'] = $price[$i];
            $insDetails['subtotal'] = $subtotal;
            //dd($insDetails);
            InvoiceDetail::create($insDetails);
        }

        session()->flash("success",__('success.-4098'));
        return redirect()->back();

    }
    /*
    * Method: checkInvoice
    * Description: This method is used to check duplicate invoice no.
    * Author : Argha
    * Date   : 2020-10-21
    */
    public function checkInvoice(Request $request)
    {
        $invoice_no = $request['data']['invoice_no'];
        $id   = @$request['data']['id'];
        $response['jsonrpc'] = '2.0'; 
        if($id){
           $checkInvoice =  InvoiceMaster::where('invoice_no',$invoice_no)->where('id','!=',$id)->first();
           if($checkInvoice != null){
                $response['status'] = 'false';
           }else{
                $response['status'] = 'true';
           }
        }else{
            $checkInvoice =  InvoiceMaster::where('invoice_no',$invoice_no)->first();
           if($checkInvoice != null){
                $response['status'] = 'false';
           }else{
                $response['status'] = 'true';
           }
        }

        return response()->json($response);
    }

    /*
    * Method: manageInvoice
    * Description: This method is get list of invoice
    * Author : Argha
    * Date   : 2020-10-21
    */
    public function manageInvoice(Request $request)
    {
        $invoices = InvoiceMaster::with(['getMerchant','getInvoiceDetails']);
        $pp = $invoices ;
        $pp = $pp->count();
        $merchants = Merchant::where('status','A')->get();
        if($request->all()){
            //dd($request->all());
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc
            if(@$request->columns['0']['search']['value']){
				$invoices = $invoices->where(function($query) use($request){
                    $query->Where('invoice_no','like','%'.@$request->columns['0']['search']['value'].'%')
                          ->orWhere('name','like','%'.@$request->columns['0']['search']['value'].'%')
                          ->orWhere('total','like','%'.@$request->columns['0']['search']['value'].'%');
                });
			}

            if(@$request->columns['1']['search']['value']){
                $date = explode(',', $request->columns['1']['search']['value']);
                $from_date = $date[0];
                $to_date = $date[1];
                $invoices = $invoices->whereBetween('invoice_date',[$from_date." 00:00:00", $to_date." 23:59:59"]);
            }
            if(@$request->columns['3']['search']['value']){
				$invoices = $invoices->where(function($query) use($request){
                    $query->where('invoice_type',@$request->columns['3']['search']['value']);
                });
            }
            
            if(@$request->columns['4']['search']['value']){
				$invoices = $invoices->where(function($query) use($request){
                    $query->Where('merchant_id',@$request->columns['4']['search']['value']);
                });
            }
            $ppp = $invoices;
            $ppp = $invoices->count();
             //sorting
             if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $invoices = $invoices->orderBy('invoice_date','asc');
                } else {
                    $invoices = $invoices->orderBy('invoice_date','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $invoices = $invoices->orderBy('invoice_no','asc');
                } else {
                    $invoices = $invoices->orderBy('invoice_no','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $invoices = $invoices->orderBy('invoice_type','asc');
                } else {
                    $invoices = $invoices->orderBy('invoice_type','desc');
                }
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $invoices = $invoices->orderBy('applicable_to','asc');
                } else {
                    $invoices = $invoices->orderBy('applicable_to','desc');
                }
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $invoices = $invoices->orderBy('total','asc');
                } else {
                    $invoices = $invoices->orderBy('total','desc');
                }
            }
            
            
            $invoices = $invoices->skip($request->start)->take($request->length)->get();
            $data['aaData'] = $invoices;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $pp;
            $data["iTotalDisplayRecords"] = $ppp;
            return response()->json($data);
        }else{
            return view('admin.modules.invoice.manage_invoice')->with([
                'merchants' => $merchants,
            ]);
        }
    }

    /*
    * Method: invoiceExport
    * Description: This method is export invoice
    * Author : Argha
    * Date   : 2020-10-21
    */
    public function invoiceExport(Request $request)
    {   
        $invoices = InvoiceMaster::with(['getMerchant','getInvoiceDetails']);
        if(@$request->all()){
            if(@$request->keyword){
                $invoices = $invoices->where(function($query) use($request){
                    $query->Where('invoice_no','like','%'.@$request->keyword.'%')
                          ->orWhere('name','like','%'.@$request->keyword.'%')
                          ->orWhere('total','like','%'.@$request->keyword.'%');
                });
            }
            if(@$request->invoice_type){
                $invoices = $invoices->where(function($query) use($request){
                    $query->where('invoice_type',@$request->invoice_type);
                });
            }
            if(@$request->merchant_id){
                $invoices = $invoices->where(function($query) use($request){
                    $query->Where('merchant_id',@$request->merchant_id);
                });
            }
            if(@$request->from_date && @$request->to_date){
                $invoices = $invoices->whereBetween('invoice_date',[$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
            }
        }
        $invoices = $invoices->orderBy('created_at','desc')->get();
        //dd($invoices);
        $invoice_array[] = array('Invoice Date', 'Invoice No.', 'Invoice Type', 'Applicable To', 'Total');
        foreach($invoices as $invoice)
        {
            if($invoice->invoice_type == 'I') {
                $invoiceType = 'In';
            } else {
                $invoiceType = 'Out';               
            }
            if($invoice->merchant_id) {
                $applicable_to = @$invoice->getMerchant->fname.' '.@$invoice->getMerchant->lname;
            } else {
                $applicable_to = @$invoice->name;                
            }

            $invoice_array[] = array(
                'Invoice Date'          => @$invoice->invoice_date,
                'Invoice No'            => @$invoice->invoice_no,
                'Invoice Type'          => @$invoiceType,
                'Applicable To'         => @$applicable_to,
                'Total'                 => @$invoice->total,
            );
        }
        // dd($orders_array);
        Excel::create('Invoice List', function($excel) use ($invoice_array){
          $excel->setTitle('Invoice List');
          $excel->sheet('Invoice List', function($sheet) use ($invoice_array){
           $sheet->fromArray($invoice_array, null, 'A1', false, false);
       });
      })->download('xlsx');
    }
    /*
    * Method: editInvoice
    * Description: This method is get edit page
    * Author : Argha
    * Date   : 2020-10-21
    */
    public function editInvoice($id)
    {
        $data['invoice'] = InvoiceMaster::with(['getMerchant','getInvoiceDetails'])->where('id',$id)->first();
        return view('admin.modules.invoice.edit_invoice')->with($data);
    }

    /*
    * Method: editInvoice
    * Description: This method is to update invoice
    * Author : Argha
    * Date   : 2020-10-22
    */
    public function updateInvoice(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'invoice_type' => 'required',
            'invoice_date' => 'required',
        ]);
        
        $price = $request->price;
        $product_name = $request->product_name;
        $qty = $request->qty;

        $total = 0;
        for($i =0;$i<=$request->product_count-1;$i++){
            $total = $total + ($price[$i] * $qty[$i]);
        }
        $previousInvoice = InvoiceMaster::where('id',$request->id)->first();
        //dd($previousInvoice);
        $prevTotal = $previousInvoice->total;
        $prevType = $previousInvoice->invoice_type;
        if($request->merchant_id){
            $merchant = Merchant::where('id',$request->merchant_id)
                            ->where('status','A')
                            ->first();
            if($merchant){
                $merchant_id  = @$merchant->id;
                
                if($request->invoice_type == 'I'){
                    
                    if($prevType == 'I'){
                        $prev_due = $merchant->total_due - $prevTotal;
                        $prev_in  = $merchant->invoice_in - $prevTotal;
                        $current_due = $prev_due + $total;
                        $currnet_in  = $prev_in +$total;
                    }else{
                        $prev_due = $merchant->total_due + $prevTotal;
                        $prev_out  = $merchant->invoice_out - $prevTotal;

                        $current_due = $prev_due + $total;
                        $currnet_in  = $merchant->invoice_in + $total;

                        $merchant_up['invoice_out'] = $prev_out;
                    }
                    
                    $merchant_up['total_due'] = $current_due;
                    $merchant_up['invoice_in'] = $currnet_in;
                }else{

                    if($prevType == 'O'){
                        $prev_due = $merchant->total_due +$prevTotal;
                        $prev_out  = $merchant->invoice_out - $prevTotal;
                        $current_due = $prev_due - $total;
                        $currnet_out  = $prev_out +$total;
                    }else{
                        $prev_due = $merchant->total_due - $prevTotal;
                        $prev_in  = $merchant->invoice_in - $prevTotal;

                        $current_due = $prev_due - $total;
                        $currnet_out  = $merchant->invoice_out + $total;

                        $merchant_up['invoice_in'] = $prev_in;
                    }
                    $merchant_up['total_due'] = $current_due;
                    $merchant_up['invoice_out']  = $currnet_out;
                }
                Merchant::where('id',$merchant_id)->update($merchant_up);
            }
        }
        //$ins['invoice_no'] = $request->invoice_no;
        //$ins['merchant_id'] = @$merchant_id;
        //$ins['name'] = @$request->name;
        $ins['total'] = $total;
        $ins['invoice_type'] = $request->invoice_type;
        $ins['invoice_date'] = $request->invoice_date ? date('Y-m-d H:i:s',strtotime($request->invoice_date)):NULL;
        //$ins['applicable_to'] = $request->applicable_to;
        
        InvoiceMaster::where('id',$request->id)->update($ins);
        InvoiceDetail::where('invoice_master_id',$request->id)->delete();
        for($i =0;$i<=$request->product_count-1;$i++){
            $subtotal = ($price[$i] * $qty[$i]);

            $insDetails['invoice_master_id'] = $request->id;
            $insDetails['product_name'] = $product_name[$i];
            $insDetails['qty'] = $qty[$i];
            $insDetails['price'] = $price[$i];
            $insDetails['subtotal'] = $subtotal;
            //dd($insDetails);
            InvoiceDetail::create($insDetails);
        }

        session()->flash("success",__('success.-4099'));
        return redirect()->back();
    }
    /*
    * Method: deleteInvoice
    * Description: This method is to delete invoice
    * Author : Argha
    * Date   : 2020-10-22
    */
    public function deleteInvoice(Request $request)
    {
        $invoice = InvoiceMaster::where('id',$request->id)->first();
        if($invoice->merchant_id){
            $merchant = Merchant::where('id',$invoice->merchant_id)
                                ->where('status','A')
                                ->first();
            if($merchant){
                if($invoice->invoice_type == 'I'){
                    $new_due = $merchant->total_due - $invoice->total;
                    $new_in  = $merchant->invoice_in - $invoice->total;
                    // if($new_in < 0){
                    //     $new_in = 0.000;
                    // }else{
                    //     $new_in = $new_in;
                    // }
                    $merchant_up['total_due'] = $new_due;
                    $merchant_up['invoice_in'] = $new_in;
                }else{
                    $new_due = $merchant->total_due + $invoice->total;
                    $new_out  = $merchant->invoice_out - $invoice->total;
                    // if($new_out < 0){
                    //     $new_out = 0.000;
                    // }else{
                    //     $new_out = $new_out;
                    // }
                    $merchant_up['total_due'] = $new_due;
                    $merchant_up['invoice_out'] = $new_out;
                }
                Merchant::where('id',$invoice->merchant_id)->update($merchant_up);
            }

        }
        $deleteInvoice = InvoiceMaster::where('id',$request->id)->delete();
        $deleteDetails = InvoiceDetail::where('invoice_master_id',$request->id)->delete();
        if($deleteDetails){
            return response()->json(1);
        }else{
            return response()->json(0);
        }
    }
    /*
    * Method: viewInvoice
    * Description: This method is to view invoice details
    * Author : Argha
    * Date   : 2020-10-22
    */

    public function viewInvoice($id)
    {
        $data['invoice'] = InvoiceMaster::with(['getMerchant','getInvoiceDetails'])->where('id',$id)->first();
        return view('admin.modules.invoice.view_invoice')->with($data);
    }

    /*
    * Method: printInvoiceDetails
    * Description: This method is used for single print for invoice
    * Author : Argha
    * Date   : 2020-10-22
    */
    public function printInvoiceDetails($id)
    {
        $data['invoice'] = InvoiceMaster::with(['getMerchant','getInvoiceDetails'])->where('id',$id)->first();
        return view('admin.modules.invoice.print_invoice_details')->with($data);
    }

    /*
    * Method: printBulkInvoice
    * Description: This method is used for bulk print for invoice
    * Author : Argha
    * Date   : 2020-10-22
    */
    public function printBulkInvoice($ids)
    {
        $ids = json_decode(base64_decode($ids));
        $data['invoices'] = InvoiceMaster::with(['getMerchant','getInvoiceDetails'])
                                        ->whereIN('id',$ids)
                                        ->get();
        return view('admin.modules.invoice.print_bulk_invoices')->with($data);
    }

    
    private function checkLoginAccess(){
        if(!@Auth::guard('admin')->user()){
            return redirect()->route('admin.login');
        }
    }
}
