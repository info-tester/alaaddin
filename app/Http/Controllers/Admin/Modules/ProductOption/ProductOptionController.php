<?php

namespace App\Http\Controllers\Admin\Modules\ProductOption;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryDetail;
use App\Models\Language;
use App\Models\Variant;
use App\Models\VariantDetail;
use App\Models\VariantValue;
use App\Models\VariantValueDetail;
use App\Models\ProductOtherOption;
use App\Models\ProductVariantDetail;
use App\Models\ProductCategory;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class ProductOptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /*
    * Method        : index
    * Description   : This method is used to show list of product option.
    * Author        : Jayatri
    * Date 			: 20/01/2020
    */
    public function index(Request $request){
        $categories = Category::with('categoryDetailsByLanguage')->where(['parent_id' => 0])->where('status', '!=', 'D')->get();
    	$subCategory = Category::with('categoryDetailsByLanguage')->where('parent_id','!=',0)->where('status', '!=', 'D')->get();
        $productOptions = Variant::with('variantDetailsByLanguage','productSubcategoryDetail.categorySubcategoryDetails','productCategoryDetail.categorySubcategoryDetails')->where('status','!=','D');
        // return $request->columns;
        $bb = $productOptions;
        $bb = $bb->count();
    	if(@$request->all()){
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc
            
            if(@$request->columns['0']['search']['value']){
                $productOptions = $productOptions->where(function($q1) use($request) {
                    $q1 = $q1->where('category_id', @$request->columns['0']['search']['value']);
                });
            }
            if(@$request->columns['1']['search']['value']){
                $productOptions = $productOptions->where(function($q1) use($request) {
                    $q1 = $q1->where('sub_category_id', @$request->columns['1']['search']['value']);
                });
            }
            
            $bbb = $productOptions;
            $bbb = $bbb->count();
            $limit = $request->length;
            if($columnIndex == 0) {
                $productOptions1 = $productOptions->get();
                if($columnSortOrder == 'asc') {
                    $productOptions2 = $productOptions1->sortBy(function($productOptions, $key) {
                        return $productOptions['productCategoryDetail']['categorySubcategoryDetails'][0]['title'];
                    });
                    $productOptions = $productOptions2->slice($request->start, $request->length)->values()->all();
                }
                else
                {
                    $productOptions2 = $productOptions1->sortByDesc(function($productOptions, $key) {
                        return $productOptions['productCategoryDetail']['categorySubcategoryDetails'][0]['title'];
                    });
                    $productOptions = $productOptions2->slice($request->start, $request->length)->values()->all();
                }
            }
            if($columnIndex == 1) {
                $productOptions1 = $productOptions->get();
                if($columnSortOrder == 'asc') {
                    $productOptions2 = $productOptions1->sortBy(function($productOptions, $key) {
                        return $productOptions['productSubcategoryDetail']['categorySubcategoryDetails'][0]['title'];
                    });

                    $productOptions = $productOptions2->slice($request->start, $request->length)->values()->all();
                }
                else
                {
                    $productOptions2 = $productOptions1->sortByDesc(function($productOptions, $key) {
                        return $productOptions['productSubcategoryDetail']['categorySubcategoryDetails'][0]['title'];
                    });

                    $productOptions = $productOptions2->slice($request->start, $request->length)->values()->all();
                }
            }
            if($columnIndex == 2) {
                $productOptions1 = $productOptions->get();
                if($columnSortOrder == 'asc') {
                    $productOptions2 = $productOptions1->sortBy(function($productOptions, $key) {
                        return $productOptions['variantByLanguage']['name'];
                    });

                    $productOptions = $productOptions2->slice($request->start, $request->length)->values()->all();
                }
                else
                {
                    $productOptions2 = $productOptions1->sortByDesc(function($productOptions, $key) {
                        return $productOptions['variantByLanguage']['name'];
                    });

                    $productOptions = $productOptions2->slice($request->start, $request->length)->values()->all();
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $productOptions = $productOptions->orderBy('type','asc');
                } else {
                    $productOptions = $productOptions->orderBy('type','desc');
                }
                $productOptions = $productOptions->skip($request->start)->take($request->length)->get();
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $productOptions = $productOptions->orderBy('status','asc');
                } else {
                    $productOptions = $productOptions->orderBy('status','desc');
                }
                $productOptions = $productOptions->skip($request->start)->take($request->length)->get();
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $productOptions = $productOptions->orderBy('id','asc');
                } else {
                    $productOptions = $productOptions->orderBy('id','desc');
                }
                $productOptions = $productOptions->skip($request->start)->take($request->length)->get();
            }
            $data['aaData'] = $productOptions;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $bb;
            $data["iTotalDisplayRecords"] = @$bbb;
            return response()->json($data);
    	}else{
            $productOptions = $productOptions->orderBy('id','desc')->get();
            return view('admin.modules.product_settings.product_option.list_product_option')->with([
                'category'       =>@$categories,
                'language'       =>@$language,
                'productOptions' =>@$productOptions,
                'subCategory' =>@$subCategory
            ]);
        }
    }

    /*
    * Method        : addProductOption
    * Description   : This method is used to add product option.
    * Author        : Jayatri
    * Date 			: 20/01/2020
    */
    public function addProductOption(Request $request){
    	$language = Language::where('status','A')->get();
    	$categories = Category::with('categoryDetailsByLanguage')->where(['parent_id'=>0])->where('status', '!=', 'D')->get();
    	if(@$request->all()){
    		$request->validate([
                'category'=>'required',
                'subcategory'=>'required',
                'product_option_name'=>'required',
                'dependent'=>'required'
            ],[
                'category.required'=>'Please select category',
                'subcategory.required'=>'Please select subcategory',
                'product_option_name.required'=>'Please select product option name',
                'dependent.required'=>'Please select dependent',
            ]);
            if(in_array($request->dependent, ['P', 'S'])) {
                $productCount = ProductCategory::where(['category_id'=>@$request->category,'category_id'=>@$request->subcategory])->count();
                if($productCount > 0){
                    session()->flash("error",__('errors.-5065'));
                    return redirect()->back();
                } else {
                    if(@$request->subcategory){
                        $new['category_id'] = $request->category;
                        $new['sub_category_id'] = $request->subcategory;
                        $new['upload_image'] = $request->upload_image;
                        $chk_slug = Variant::where('default_title',$request->product_option_name[0])->first();
                        if(@$chk_slug){
                            $new['default_title'] = $request->product_option_name[0]."-".time();
                        }else{
                            $new['default_title'] = $request->product_option_name[0];
                        }
                    }
                    if(@$request->dependent){
                        $new['type'] = $request->dependent;
                        $new['status'] = 'A';
                    }
                    DB::transaction(function() use($new,$request,$language){
                        $productOption = Variant::create($new);
                        if(@$productOption){
                            if(@$request->product_option_name){
                                foreach($request->product_option_name as $key=>$val){
                                    $details['variant_id'] = $productOption->id;
                                    $details['name'] = $val;
                                    $details['language_id'] = $language[$key]->id;
                                    $optionDetails = VariantDetail::create($details);
                                }
                            }
                            session()->flash("success",__('success.-4009'));
                        }else{
                            session()->flash("error",__('errors.-5010'));
                        }
                    });
                }
            } else {
        		if(@$request->subcategory){
        			$new['category_id'] = $request->category;
        			$new['sub_category_id'] = $request->subcategory;
                    $new['upload_image'] = $request->upload_image;
        			$chk_slug = Variant::where('default_title',$request->product_option_name[0])->first();
        			if(@$chk_slug){
        				$new['default_title'] = $request->product_option_name[0]."-".time();
        			}else{
        				$new['default_title'] = $request->product_option_name[0];
        			}
        		}
        		if(@$request->dependent){
        			$new['type'] = $request->dependent;
        			$new['status'] = 'A';
        		}
        		DB::transaction(function() use($new,$request,$language){
    	    		$productOption = Variant::create($new);
    	    		if(@$productOption){
    	    			if(@$request->product_option_name){
    	    				foreach($request->product_option_name as $key=>$val){
    	    					$details['variant_id'] = $productOption->id;
    	    					$details['name'] = $val;
    	    					$details['language_id'] = $language[$key]->id;
    	    					$optionDetails = VariantDetail::create($details);
    	    				}
    	    			}
                        session()->flash("success",__('success.-4009'));
    				}else{
    					session()->flash("error",__('errors.-5010'));
    				}
    			});
            }
			return redirect()->route('admin.list.product.option');
    	}else{
    		return view('admin.modules.product_settings.product_option.add_product_option')->with([
    			'category'=>@$categories,
    			'language'=>@$language
			]);
    	}
    }
    
    /*
    * Method        : editProductOption
    * Description   : This method is used to edit product option.
    * Author        : Jayatri
    * Date 			: 20/01/2020
    */
    public function editProductOption(Request $request,$id){
    	$language = Language::where('status','A')->get();
    	$categories = Category::with('categoryDetailsByLanguage')->where(['parent_id'=>0])->where('status', '!=', 'D')->get();
    	$variants = Variant::with('variantByLanguage','productCategoryDetail.categoryByLanguage','productSubcategoryDetail.categoryByLanguage','variantDetails')->where('id',$id)->first();
    	if(@$variants){
	    	if(@$request->all()){
	    		$request->validate([
	                // 'category'=>'required',
	                // 'subcategory'=>'required',
	                'product_option_name'=>'required',
	                //'dependent'=>'required'
	            ],[
	                // 'category.required'=>'Please select category',
	                // 'subcategory.required'=>'Please select subcategory',
	                'product_option_name.required'=>'Please select product option name',
	                //'dependent.required'=>'Please select dependent',
	            ]);
	            DB::transaction(function() use($request,$language,$id,$variants,$categories){
                    $chk_slug = Variant::where('default_title',$request->product_option_name[0])->first();
                    if(@$chk_slug){
                        $new['default_title'] = $request->product_option_name[0]."_".time();
                    }else{
                        $new['default_title'] = $request->product_option_name[0];
                    }
                    if(@$request->upload_image){
                        $new['upload_image'] = $request->upload_image;
                    }    
                    
    	    		if(@$request->dependent){
    	    			$new['type'] = $request->dependent;
    	    			// $new['status'] = 'A';
    	    		}
    	    		Variant::where('id',$id)->update($new);
    	    		if(@$request->product_option_name){
    					foreach($request->product_option_name as $key=>$val){
    						// $details['variant_id'] = $productOption->id;
    						$details['name'] = $val;
    						// $details['language_id'] = $language[$key]->id;
    						$optionDetails = VariantDetail::where(['variant_id'=>$variants->id,'language_id'=>$language[$key]->id])->update($details);
    					}
    				}
				});
				//updated successfully
				session()->flash("success",__('success.-4011'));
				return redirect()->route('admin.list.product.option');
	    	}

			return view('admin.modules.product_settings.product_option.edit_product_option')->with([
				'category'=>@$categories,
				'variants'=>@$variants,
				'language'=>@$language
			]);
		}else{
			//error unauthorized access
			session()->flash("error",__('errors.-5002'));
			return redirect()->back();
		}
    }

    /*
    * Method        : getSubcategory
    * Description   : This method is used to get subcategory by ajax call.
    * Author        : Jayatri
    * Date 			: 20/01/2020
    */
    public function getSubcategory(Request $request){
    	if(@$request->all()){
    		$subCategories = Category::with('categoryDetailsByLanguage')->where(['parent_id'=>$request->category])->where('status', '!=', 'D')->get();
    		if(@$subCategories){
    			return response()->json($subCategories);
    		}else{
    			return 0;
    		}
    	}else{
    		return 0;
    	}
    }

    /*
    * Method        : validationStockPriceDependent
    * Description   : This method is used to validation Stock Price Dependent by ajax call.(not in use)
    * Author        : Jayatri
    * Date 			: 20/01/2020(not in use)
    */
    public function validationStockPriceDependent(Request $request){
		return 0;
    }

    /*
    * Method        : listOptionValues
    * Description   : This method is used to listOptionValues.
    * Author        : Jayatri
    * Date 			: 20/01/2020
    */
    public function listOptionValues($id){
        $this->checkLoginAccess();
    	$language = Language::where('status','A')->get();
    	$categories = Category::with('categoryDetailsByLanguage')->where(['parent_id'=>0])->where('status', '!=', 'D')->get();
    	$variants = Variant::with('variantByLanguage','productCategoryDetail.categoryByLanguage','productSubcategoryDetail.categoryByLanguage')->where('id',$id)->first();
    	if(@$variants){
            $vid = $variants->id;
    		$optionValues = VariantValue::with('variantValueByLanguage')->where('variant_id',$variants->id)->where('status', '!=', 'D')->get();
    		return view('admin.modules.product_settings.product_option.list_option_values')->with([
    			'category'=>@$categories,
    			'language'=>@$language,
    			'variants'=>@$variants,
    			'optionValues'=>@$optionValues,
                'vid'=>@$vid
			]);
    	}else{
    		//error unauthorized access
    		session()->flash("error",__('errors.-5002'));
			return redirect()->back();
    	}
    }
    /*
    * Method        : addOptionValues
    * Description   : This method is used to addOptionValues.
    * Author        : Jayatri
    * Date 			: 20/01/2020
    */
    public function addOptionValues($id,Request $request){
        $this->checkLoginAccess();
    	$language = Language::where('status','A')->get();
    	$variant = Variant::where('id',$id)->first();
    	if(@$variant){
    		$variantDetails = VariantDetail::where('variant_id',$variant->id)->first();
    		if(@$variantDetails){
    			if(@$request->all()){
    				DB::transaction(function() use($request,$language,$id,$variant,$variantDetails){
	    				if(@$request->optionValueName){
	    					$new['variant_id'] = $variant->id;
							$new['status'] = 'A';
							$new['default_name'] = $request->optionValueName[0];
                            if(@$variant->upload_image == 'Y'){
                                if(@$request->optionValueImage){
                                    $pic   = $request->optionValueImage;
                                    $name = time().'.'.$pic->getClientOriginalExtension();
                                    $pic->move('storage/app/public/option_value_images/', $name);
                                    $new['image'] = $name;
                                }
                            }
							$variant_value = VariantValue::create($new);
	    					foreach($request->optionValueName as $key=>$value){
	    						$detail['name'] = $value;
	    						$detail['language_id'] = $language[$key]->id;
	    						$detail['variant_value_id'] = $variant_value->id;
	    						$variant_value_detail = VariantValueDetail::create($detail);
	    					}
                            if(@$variant_value){
                                session()->flash("success",__('success.-4010'));
                            }else{
                                session()->flash("error",__('errors.-5009'));
                            }
	    				}
    				});
    				return redirect()->route('admin.list.option.values',$id);
    			}
    		}else{
    			//variant details not found in variant_details table
				session()->flash("error",__('errors.-5009'));
				return redirect()->route('admin.list.option.values',$id);
    		}
    	}else{
    		//default title is not found Unauthorized access!
    		session()->flash("error",__('errors.-5009'));
			return redirect()->back();
    	}
    }
    /*
    * Method        : checkType
    * Description   : This method is used to check dependent Type.
    * Author        : Jayatri
    * Date 			: 20/01/2020
    */
    public function editOptionValues($id, $variantValueId = NULL, Request $request){
    	$this->checkLoginAccess();
    	$language = Language::where('status','A')->get();
    	$categories = Category::with('categoryDetailsByLanguage')->where(['parent_id'=>0])->where('status', '!=', 'D')->get();
    	
		if(@$request->all()){
			$chk = VariantValue::where('variant_id',$id)->first();
			if(@$chk){
				DB::transaction(function() use($request,$language,$id){
        			foreach($request->optionValueName as $key=>$value){
    					$detail['name'] = $value;
    					VariantValueDetail::where(['id'=>$request->details_ids[$key],'language_id'=>$language[$key]->id])->update($detail);
    				}
                    $variant = Variant::where('id',$id)->first();
                    if(@$variant){
                        if(@$variant->upload_image == 'Y'){
                            if(@$request->optionValueImage){
                                $pic   = $request->optionValueImage;
                                $name = time().'.'.$pic->getClientOriginalExtension();
                                $pic->move('storage/app/public/option_value_images/', $name);
                                $new['image'] = $name;
                                VariantValue::where('id',$request->hid)->update($new);
                            }
                        }    
                    }
    				session()->flash("success",__('success.-4014'));
				});
				return redirect()->back();
			}else{
				session()->flash("error",__('errors.-5002'));
				return redirect()->back();
			}
		}else{
			$option_value = VariantValue::with('variantValueDetails')->where('id',$id)->first();
			if(@$option_value){
				$variants = Variant::with('variantByLanguage','productCategoryDetail.categoryByLanguage','productSubcategoryDetail.categoryByLanguage')->where('id',$option_value->variant_id)->first();
    			$optionValues = VariantValue::with('variantValueByLanguage')->where('variant_id',$option_value->variant_id)->get();
    			return view('admin.modules.product_settings.product_option.list_option_values')->with([
					'category'=>@$categories,
					'language'=>@$language,
					'variants'=>@$variants,
					'optionValues'=>@$optionValues,
					'option_value'=>@$option_value,
				]);
			}else{
				//unauthorized access 
				session()->flash("error",__('errors.-5002'));
				return redirect()->back();
			}
		}
    }
    /*
    * Method        : checkType
    * Description   : This method is used to check dependent Type.
    * Author        : Jayatri
    * Date 			: 20/01/2020
    */
    public function checkType(Request $request){
    	$type = @$request->type;
    	$category = @$request->category;
    	$subcategory = @$request->subcategory;
        if(@$request->option_id){
            $option_id   = @$request->option_id;
            $variant = Variant::where('id','!=',@$option_id);
            $variant_1 = Variant::where('id','!=',@$option_id);
        }else{
            $variant = Variant::where('id','!=',0);
            $variant_1 = Variant::where('id','!=',0);
        }

    	if($type == 'P') {
    		$priceDependentCount = $variant->where([
                'category_id'       => @$category,
                'sub_category_id'   => @$subcategory,
                'type'              => @$type
            ])
            ->where('status', '!=', 'D')
            ->count();
    		if($priceDependentCount >=2){
    			return 0;
    		}else{
    			return 1;
    		}
    	} elseif($type == 'S') {
    		$priceDependentCount = $variant->where([
                'category_id'       => @$category,
                'sub_category_id'   => @$subcategory,
                'type'              =>
                'P'
            ])
            ->where('status', '!=', 'D')
            ->count();
            
    		$stockDependentCount = $variant_1->where([
                'category_id'       => @$category,
                'sub_category_id'   => @$subcategory,
                'type'              => 'S'
            ])
            ->where('status', '!=', 'D')
            ->count();
    		$tot_dependent = $priceDependentCount + $stockDependentCount;

    		if($stockDependentCount >= 2){
    			return 0;
    		}elseif($tot_dependent >= 3){
    			return 0;
    		}else{
    			return 1;
    		}
    	}
    }

    public function checkDuplicateOptionName(Request $request){
        if(@$request->all()){
            $variant_values = VariantValue::where(['variant_id'=>@$request->vid])->where('status', '!=', 'D');
            if(@$variant_values){
                $variant_values = $variant_values->whereHas('variantValueDetails', function($q) use ($request){
                    $q->where('name',$request->name);
                });
                $variant_values = $variant_values->orderBy('id','desc')->first();
                if(@$variant_values){
                    return 0;
                }else{
                    return 1;
                }     
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }

    /*
    * Method        : deleteProductOption
    * Description   : This method is used to delete Product Option.
    * Author        : Jayatri
    * Date 			: 24/01/2020
    */
    public function deleteProductOption($id){
        $this->checkLoginAccess();
    	$language = Language::where('status','A')->get();
    	$categories = Category::with('categoryDetailsByLanguage')->where(['parent_id'=>0])->where('status', '!=', 'D')->get();
    	$flag = 0;
    	$variants = Variant::where('id',$id)->first();
    	if(@$variants){
    		$optionValues = VariantValue::where('variant_id',$variants->id)->count();
    		$product_other_options = ProductOtherOption::where('variant_id',$variants->id)->count();

    		if($optionValues != 0){
    			$flag = 1;
    		}elseif($product_other_options != 0){
    			$flag = 1;
    		}
    		if($flag == 1){
    			//not delete
    			//if option values list exist
    			session()->flash("error",__('errors.-5011'));
				// return redirect()->route('admin.list.product.option');
    		}else{
    			DB::transaction(function() use($id){
	    			$detail = VariantDetail::where('variant_id',$id)->first();
	    			if(@$detail){
                        $update['status'] = 'D';
	    				VariantDetail::where('variant_id',$id)->update($update);
	    			}
	    			// Variant::with('variantDetails')->where('id',$id)->delete();
                    $update['status'] = 'D';
                    Variant::with('variantDetails')->where('id',$id)->update($update);
	    			//if option values list doesnot exist
	    			session()->flash("success",__('success.-4012'));
				});
    		}
    		return redirect()->route('admin.list.product.option');
    	}else{
    		session()->flash("error",__('errors.-5011'));
			return redirect()->route('admin.list.product.option');
    	}
    }
     /*
    * Method        : changeOptionStatus
    * Description   : This method is used to change Option Status.
    * Author        : Jayatri
    * Date          : 24/01/2020
    */
    public function changeOptionStatus($id){
        $this->checkLoginAccess();
        $option = Variant::where('id',$id)->first();
        if(@$option){
            if($option->status == 'A'){
                $update['status'] = 'I';
            }else{
                $update['status'] = 'A';
            }
            DB::transaction(function() use($id,$update){
                Variant::where('id',$id)->update($update);
            });
            session()->flash("success",__('success.-4021'));

        }else{
            session()->flash("error",__('errors.-5021'));
            
        }
        return redirect()->back();
    }
    /*
    * Method        : changeOptionStatus
    * Description   : This method is used to change Option Status.
    * Author        : Jayatri
    * Date          : 24/01/2020
    */
    public function updateOptionStatus(Request $request){
        // $this->checkLoginAccess();
        $id = @$request->id;
        $option = Variant::where('id',$id)->first();
        if(@$option){
            if($option->status == 'A'){
                $update['status'] = 'I';
            }else{
                $update['status'] = 'A';
            }
            DB::transaction(function() use($id,$update){
                Variant::where('id',$id)->update($update);
            });
            // session()->flash("success",__('success.-4021'));
            $option = Variant::where('id',$id)->first();
            return response()->json($option);
        }else{
            // session()->flash("error",__('errors.-5021'));
            return 0;
            
        }
        // return redirect()->back();
    }
    /*
    * Method        : deleteOptionValues
    * Description   : This method is used to delete Option Values.
    * Author        : Jayatri
    * Date 			: 24/01/2020
    */
    public function deleteOptionValues($id){
    	$this->checkLoginAccess();
    	$variant = VariantValue::where('id',$id)->first();
    	$flag = 0;
    	if(@$variant){
    		$variant_details = VariantValueDetail::where('variant_value_id',$id)->get();
    		if(@$variant_details){
    			$product_other_options = ProductOtherOption::with(['getProducts'])
                                            ->where('variant_value_id',$id)
                                            ->whereHas('getProducts', function($q) {
                                                $q->where('status', '!=', 'D');
                                            })
                                            ->get();

                $product_variant_details = ProductVariantDetail::with(['getProducts'])
                                            ->where('variant_value_id',$variant->id)
                                            ->whereHas('getProducts', function($q) {
                                                $q->where('status', '!=', 'D');
                                            })
                                            ->get();
                                            
                if($product_other_options->count() > 0){
                    $flag = 1;
                }elseif($product_variant_details->count() > 0){
                    $flag = 1;
                }
	    		if($flag == 0){
	    			//delete
	    			DB::transaction(function() use($id,$variant){
                        $update['status'] = 'D';
		    			VariantValue::where('id',$id)->update($update);;
		    			session()->flash("success",__('success.-4018'));
	    			});
					
	    		}else{
	    			//not delete
	    			session()->flash("error",__('errors.-5015'));
	    		}
                return redirect()->route('admin.list.product.option');
    		}else{
    			//error(variant details not found)
    			session()->flash("error",__('errors.-5015'));
				return redirect()->route('admin.list.product.option');
    		}
    	}else{
    		//error(unauthorized access  id not found)
    		session()->flash("error",__('errors.-5015'));
			return redirect()->route('admin.list.product.option');
    	}
    }
    /*
    * Method        : deleteOption
    * Description   : This method is used to delete Option.
    * Author        : Jayatri
    * Date          : 12/03/2020
    */
    public function deleteOption(Request $request){
        $id = @$request->id;
        $variant = Variant::where('id',$id)->first();
        $flag = 0;
        if(@$variant){
            $variant_details = VariantDetail::where('variant_id',$id)->get();
            if(@$variant_details){
                $product_other_options = ProductOtherOption::with(['getProducts'])
                                            ->where('variant_value_id',$id)
                                            ->whereHas('getProducts', function($q) {
                                                $q->where('status', '!=', 'D');
                                            })
                                            ->get();
                $product_variant_details = ProductVariantDetail::with(['getProducts'])
                                            ->where('variant_id',$variant->id)
                                            ->whereHas('getProducts', function($q) {
                                                $q->where('status', '!=', 'D');
                                            })
                                            ->get();

                if($product_other_options->count() > 0){
                    $flag = 1;
                }elseif($product_variant_details->count() > 0){
                    $flag = 1;
                }
                if($flag == 0){
                    //delete
                    $update['status'] ='D';
                    VariantValue::where('id',$id)->update($update);
                    Variant::where('id',$id)->update($update);
                    session()->flash("success",__('success.-4018'));
                    return 1;
                }else{
                    //not delete
                    return 0;
                }
            }else{
                //error(variant details not found)
                return 0;
            }
        }else{
            //error(unauthorized access  id not found)
            return 0;
        }
    }

    private function checkLoginAccess(){
        if(!@Auth::guard('admin')->user()){
            return redirect()->route('admin.login');
        }
    }
    
}
