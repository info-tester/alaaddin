<?php

namespace App\Http\Controllers\Admin\Modules\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
use App\Merchant;
use App\Models\Brand;
use App\Models\Category;
use App\Models\OrderDetail;
use App\Models\CategoryDetail;
use App\Models\Language;
use App\Models\Variant;
use App\Models\CartMaster;
use App\Models\CartDetail;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductDetail;
use App\Models\ProductVariantOthers;
use App\Models\VariantValue;
use App\Models\ProductVariant;
use App\Models\ProductVariantDetail;
use App\Models\ProductOtherOption;
use App\Models\ProductImage;
use App\Models\ProductNotification;

use App\Repositories\CityRepository;
use App\Repositories\CitiDetailsRepository;
use App\Models\OrderSeller;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\DriverRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductVariantDetailRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CountryRepository;
use App\Repositories\UserRepository;
use Auth;
use DB;
use Lang;
use Session;
use File;
use Mail;
use App\Mail\StockNotification;
use App\Mail\sendApproveProductMail;
use Image;

class ProductController extends Controller
{
	
	protected $languageId;
	
	public function __construct(OrderMasterRepository $order_master,
        OrderDetailRepository $order_details,
        DriverRepository $drivers,
        OrderSellerRepository $orderSeller,
        ProductVariantRepository $productVariant,
        ProductRepository $product,
        ProductVariantDetailRepository $productVariantDetail,
       	MerchantRepository $merchants)
	{
		$this->order             		= $order_master;
        $this->order_details            = $order_details;
        $this->merchants                = $merchants;
        $this->drivers                  = $drivers;
        $this->orderSeller              = $orderSeller;
        $this->product_variant          = $productVariant;
        $this->productVariantDetail     = $productVariantDetail;
        $this->product                  = $product;
		$this->middleware('admin.auth:admin');
	}

	private function setLanguage() {
		$lang = Language::where(['prefix' => Config::get('app.locale')])->first();
		$this->languageId = $lang->id;
	}

	private function getLanguageId() {
		return $this->languageId;
	}

	function array_to_obj($array, &$obj) {
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				$obj->$key = new \stdClass();
				array_to_obj($value, $obj->$key);
			}
			else {
				$obj->$key = $value;
			}
		}
		return $obj;
	}

	public function deleteReviewProduct($id){
		$order = OrderDetail::where('id',@$id)->first();
		if(@$order) {
			$update['comment'] = "";
			$update['rate'] = 0;
			OrderDetail::where('id',@$id)->update($update);
			$this->calculateProductRate(@$order->order_master_id);
			session()->flash("success",__('success.-4085'));
		} else {
			session()->flash("error",__('errors.-5001'));
		}
		return redirect()->back();	
	}

	private function calculateProductRate($id) {
		$orderdetails = $this->order_details->where('order_master_id',@$id)->get();
		foreach(@$orderdetails as $key=>$dtls) {
			if($dtls->product_id != 0){
				$product = $this->product->where('id',@$dtls->product_id)->first();    
				$obje = $this->order_details->where(['product_id'=>$dtls->product_id]);
				$count = $obje->where('rate','!=',0)->count();
				$total_rate = $this->order_details->where(['product_id'=>$dtls->product_id])->sum('rate');
				$updater['total_no_reviews'] = $count;
				$updater['total_review'] = $total_rate;
				if($count != 0){
					$updater['avg_review'] = $total_rate/$count;    
				}          
				$this->product->where('id',@$dtls->product_id)->update($updater);
			}
		}   
	}
	public function showProductReviewRatings($id){
		$product = Product::with('productVariants','productByLanguage','defaultImage','productMarchant:id,fname,lname','productCategory.Category','productSubCategory.Category.priceVariant','productParentCategory.Category','productSubCategory.Category','productUnitMasters')->where('slug',@$id)->first();
		$orders = OrderDetail::where(['product_id'=>@$product->id,'status'=>'OD']);
		$order = $orders->where('rate','!=',0)->get();
		return view('admin.modules.products.view_product_comments')->with(['product'=>@$product,'order'=>@$order]);
	}

	public function index(Request $request) {
		$columnIndex = $request->order[0]['column']; // Column index
		$columnName = $request->columns[$columnIndex]['data']; // Column name
		$columnSortOrder = $request->order[0]['dir']; // asc or desc

		$p1 = $product = Product::where('status', '!=', 'D')
		->with('productByLanguage','defaultImage','productMarchant:id,fname,lname','productCategory.Category','productSubCategory.Category.priceVariant','productParentCategory.Category','productSubCategory.Category','productVariants','productUnitMasters');
		$p1 =  $p1->count();
		if($request->all() && @$request->merchant_id == '')
		{
			if(@$request->columns['4']['search']['value']){
				$keyword = $request->columns['4']['search']['value'];
				$product = $product->where(function($q) use($keyword){
					$q->where('product_code', 'LIKE', '%'.$keyword.'%');

					$q->orWhereHas('productByLanguage',function($query) use($keyword) {
						$query->where('title','like','%'.$keyword.'%');
					});
				});
			}
			if(@$request->columns['1']['search']['value']){
				$product = $product->whereHas('productCategory',function($query) use($request) {
					$query->Where('category_id',$request->columns['1']['search']['value']);
				});
			}
			if(@$request->columns['3']['search']['value']){
				$product = $product->Where('user_id',$request->columns['3']['search']['value']);
			}
			if(@$request->columns['7']['search']['value']){
				$product = $product->Where('status',$request->columns['7']['search']['value']);
			}
			if(@$request->search['value']){
				$price = explode(',', $request->search['value']);
				$price_min = $price[0];
				$price_max = $price[1];
				$product = $product->whereBetween('price', [$price_min, $price_max]);
			}
            if(@$request->columns['8']['search']['value']){
                $date = explode(',', $request->columns['8']['search']['value']);
                $from_date = $date[0];
                $to_date = $date[1];
                $product = $product->whereBetween('created_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);
            }
			$pp = $product;
			$pp = $pp->count();
			$limit = $request->length;
			
			if($columnIndex) {
				if($columnIndex == 0) {
					if($columnSortOrder == 'asc'){
						$product = $product->orderBy('id','asc');
					}
					else{
						$product = $product->orderBy('id','desc');
					}
					$product = $product->get()->toArray();
				}
				if($columnIndex == 1) {
					if($columnSortOrder == 'asc'){
						$product = $product->orderBy('id','asc');
					}
					else{
						$product = $product->orderBy('id','desc');
					}
					$product = $product->get()->toArray();
				}
				if($columnIndex == 2) {
					$product = $product->get()->toArray();
					uasort($product, function($a, $b) use ($columnSortOrder){
						$c1 = @$a['product_category'][0]['category']['category_by_language']['title'];
						$c2 = @$b['product_category'][0]['category']['category_by_language']['title'];
						$res = strcmp($c1, $c2);
						if($res == 0) {
							return 0;
						} elseif($res < 0) {
							return $columnSortOrder == 'asc' ? -1 : 1;
						} else {
							return $columnSortOrder == 'asc' ? 1 : -1;
						}
					});
				}
				if($columnIndex == 3) {
					$product = $product->get()->toArray();
					uasort($product, function($a, $b) use ($columnSortOrder){
						$c1 = @$a['product_marchant']['fname'].' '.@$a['product_marchant']['lname'];
						$c2 = @$b['product_marchant']['fname'].' '.@$b['product_marchant']['lname'];
						$res = strcmp($c1, $c2);
						if($res == 0) {
							return 0;
						} elseif($res < 0) {
							return $columnSortOrder == 'asc' ? -1 : 1;
						} else {
							return $columnSortOrder == 'asc' ? 1 : -1;
						}
					});
				}

				if($columnIndex == 4) {
					$product = $product->get()->toArray();
					uasort($product, function($a, $b) use ($columnSortOrder){
						$c1 = @$a['product_by_language']['title'];
						$c2 = @$b['product_by_language']['title'];
						$res = strcmp($c1, $c2);
						if($res == 0) {
							return 0;
						} elseif($res < 0) {
							return $columnSortOrder == 'asc' ? -1 : 1;
						} else {
							return $columnSortOrder == 'asc' ? 1 : -1;
						}
					});
				}
				# stock ordering properly not working.
				if($columnIndex == 5) {
					// $product = $product->get()->toArray();
					if($columnSortOrder == 'asc'){
						$product = $product->orderBy('price','asc');
					}
					else{
						$product = $product->orderBy('price','desc');
					}
					$product = $product->get()->toArray();
					// uasort($product, function($p1, $p2) use ($columnSortOrder){
					// 	$price1 = @$p1['price'];
					// 	$price2 = @$p2['price'];
					// 	$res = strcmp($price1, $price2);
					// 	// dd($res);
					// 	if($res == 0) {
					// 		return 0;
					// 	} elseif($res < 0) {
					// 		return $columnSortOrder == 'asc' ? -1 : 1;
					// 	} else {
					// 		return $columnSortOrder == 'asc' ? 1 : -1;
					// 	}
					// });
				}

				if($columnIndex == 6) {
					$product = $product->get()->toArray();
					uasort($product, function($a, $b) use ($columnSortOrder){
						$total_no_reviews1 = @$a['total_no_reviews'];
						$total_no_reviews2 = @$b['total_no_reviews'];
						$res = strcmp($total_no_reviews1, $total_no_reviews2);
						if($res == 0) {
							return 0;
						} elseif($res < 0) {
							return $columnSortOrder == 'asc' ? -1 : 1;
						} else {
							return $columnSortOrder == 'asc' ? 1 : -1;
						}
					});
				}
				if($columnIndex == 7) {
					$product = $product->get()->toArray();
					uasort($product, function($a, $b) use ($columnSortOrder){
						$c1 = @$a['avg_review'];
						$c2 = @$b['avg_review'];
						$res = strcmp($c1, $c2);
						if($res == 0) {
							return 0;
						} elseif($res < 0) {
							return $columnSortOrder == 'asc' ? -1 : 1;
						} else {
							return $columnSortOrder == 'asc' ? 1 : -1;
						}
					});
				}
				if($columnIndex == 8) {
					$product = $product->get()->toArray();
					uasort($product, function($a, $b) use ($columnSortOrder){
						$c1 = @$a['avg_review'];
						$c2 = @$b['avg_review'];
						$res = strcmp($c1, $c2);
						if($res == 0) {
							return 0;
						} elseif($res < 0) {
							return $columnSortOrder == 'asc' ? -1 : 1;
						} else {
							return $columnSortOrder == 'asc' ? 1 : -1;
						}
					});
				}
				if($columnIndex == 9) {
					$product = $product->get()->toArray();
					uasort($product, function($a, $b) use ($columnSortOrder){
						$c1 = @$a['status'];
						$c2 = @$b['status'];
						$res = strcmp($c1, $c2);
						if($res == 0) {
							return 0;
						} elseif($res < 0) {
							return $columnSortOrder == 'asc' ? -1 : 1;
						} else {
							return $columnSortOrder == 'asc' ? 1 : -1;
						}
					});
				}
				// if($columnIndex == 10) {
				// 	$product = $product->get()->toArray();
				// 	uasort($product, function($a, $b) use ($columnSortOrder){
				// 		$c1 = @$a['seller_status'];
				// 		$c2 = @$b['seller_status'];
				// 		$res = strcmp($c1, $c2);
				// 		if($res == 0) {
				// 			return 0;
				// 		} elseif($res < 0) {
				// 			return $columnSortOrder == 'asc' ? -1 : 1;
				// 		} else {
				// 			return $columnSortOrder == 'asc' ? 1 : -1;
				// 		}
				// 	});
				// }
				if($columnIndex == 10) {
					$product = $product->get()->toArray();
					uasort($product, function($a, $b) use ($columnSortOrder){
						$c1 = @$a['product_code'];
						$c2 = @$b['product_code'];
						$res = strcmp($c1, $c2);
						if($res == 0) {
							return 0;
						} elseif($res < 0) {
							return $columnSortOrder == 'asc' ? -1 : 1;
						} else {
							return $columnSortOrder == 'asc' ? 1 : -1;
						}
					});
				}

				if($columnIndex == 11) {
					$product = $product->get()->toArray();
					uasort($product, function($a, $b) use ($columnSortOrder){
						$c1 = @$a['product_code'];
						$c2 = @$b['product_code'];
						$res = strcmp($c1, $c2);
						if($res == 0) {
							return 0;
						} elseif($res < 0) {
							return $columnSortOrder == 'asc' ? -1 : 1;
						} else {
							return $columnSortOrder == 'asc' ? 1 : -1;
						}
					});
				}
				$product = array_slice($product, $request->start, $limit);
				$p = array();
				foreach ($product as $value) {
					array_push($p, $value);
				}
				$product = $p;
			} else {
				$product = $product->orderBy($columnName, $columnSortOrder)->skip($request->start)->take($limit)->get()->toArray();
			}
			$data['aaData'] = $product;
			$data["draw"] = intval($request->draw);
			$data['iTotalRecords'] = $p1;
			$data["iTotalDisplayRecords"] = $pp;
			return response()->json($data);
		}
	}

	public function comparePro($a, $b){
		$c1 = $a->productParentCategory->Category->categoryByLanguage->title;
		$c2 = $b->productParentCategory->Category->categoryByLanguage->title;
		$res = strcmp($c1, $c2);
		if($res == 0) {
			return 0;
		} elseif($res < 0) {
			return -1;
		} else {
			return 1;
		}
	}

	/*
	* Method: productView
	* Description: Get the product details
	* Author: Surajit 
	*/
	public function productView(Request $request){
		$data['user']           = Merchant::select([
			'id',
			'fname',
			'lname'
		])
		->where('status','A')
		->orderBy('fname','asc')->get();
		$st = 'asc';
		$data['category']       = Category::join('category_details', 'categories.id', '=', 'category_details.category_id')->orderBy('category_details.title', 'asc')->select('categories.*','category_details.*')
		->with(['categoryByLanguage:id,category_id,language_id,title','categoryByLanguage'=> function ($q) use ($st) {
            $q->orderBy('title', $st);
        }])
		->where([
			'parent_id' =>  '0'
		])
		->where('status', '!=', 'D')
		->get();

		$product = Product::where('status', '!=', 'D')
		->with('productByLanguage','defaultImage','productMarchant:id,fname,lname','productCategory.Category','productSubCategory.Category.priceVariant');
		$data['highestPrice'] = Product::orderBy('price','DESC')->first();
		if($request->all() && @$request->merchant_id == '')
		{
			if(@$request->keyword){
				$keyword = $request->keyword;
				$product = $product->where(function($q) use($keyword){
					$q->where('product_code', $keyword);

					$q->orWhereHas('productByLanguage',function($query) use($request) {
						$query->where('title','like','%'.$request->keyword.'%');
					});
				});
				
			}
			if(@$request->category){
				$product = $product->whereHas('productCategory',function($query) use($request) {
					$query->where('category_id',$request->category);
				});
			}
			if(@$request->sub_category){
				$product = $product->whereHas('productCategory',function($query) use($request) {
					$query->where('category_id',$request->sub_category);
				});
			}
			if(@$request->user_id){
				$product = $product->where('user_id',$request->user_id);
			}
			if(@$request->status){
				$product = $product->where('status',$request->status);
			}
			if(@$request->price){
				$price = explode(',', $request->price);
				$price_min = $price[0];
				$price_max = $price[1];
				$product = $product->whereBetween('price', [$price_min, $price_max]);
			}

			$product = $product->orderBy('id','DESC')->get();
			$data['product'] = $product;
			return response()->json($data);
			return view("admin.modules.products.ajax_manage_products",$data);
		}

		if(@$request->merchant_id){
			$product = $product->Where('user_id',$request->merchant_id);
		}
		$data['subCategory']    = Category::select([
			'id',
			'parent_id',
			'status'
		])
		->with('categoryByLanguage:id,category_id,language_id,title')
		->where('status', '!=', 'D')
		->where('parent_id', '!=', '0')
		->get();

		$product = $product->orderBy('id','DESC')->get();
		$data['key'] = $request->all();
		$data['product'] = $product;
		return view("admin.modules.products.manage_products",$data);
	}

	/**
	* 
	*/
	public function checkPriceDependency(Request $request) {
		$response = [
			'jsonrpc' => '2.0'
		];
		$response['result']['price_dependent'] = Variant::where([
			'category_id' 		=> $request->params['category_id'], 
			'sub_category_id' 	=> $request->params['sub_category_id'], 
			'type' 				=> 'P',
			'status'			=> 'A'
		])
		->count();

		$response['result']['stock_dependent'] = Variant::where([
			'category_id' 		=> $request->params['category_id'], 
			'sub_category_id' 	=> $request->params['sub_category_id'], 
			'type' 				=> 'S',
			'status'			=> 'A'
		])
		->count();
		return response()->json($response);
	}

	/*
	* Method: productAddFirst
	* Description: to add product page step 1
	* Author: Surajit 
	*/
	public function productAddFirst(Request $request){
		$data['user'] = Merchant::select([
			'id',
			'fname',
			'lname'
		])
		->where('status','A')
		->get();
		$data['brand'] = Brand::select([
			'id',
			'name',
			'status'
		])
		->where([
			'status'    =>  'A'
		])
		->with('brandDetailsByLanguage')
		->get();
		$data['category'] = Category::select([
			'id',
			'parent_id',
			'status'
		])
		->where([
			'parent_id' =>  '0',
			'status'    =>  'A'
		])
		->with('categoryByLanguage:id,category_id,language_id,title')
		->get();
		$data['language'] = Language::select([
			'id',
			'name',
			'prefix',
			'status'
		])
		->where([
			'status'    =>  'A'
		])
		->get();
		return view("admin.modules.products.add_products_step_1", $data);
	}

	/*
	* Method: storeProduct
	* Description: It will store product into mutiple tables
	* Author: Surajit 
	*/
	public function storeProduct(Request $request){
		$validator = $request->validate([
			'user_id'        => 'required',
			'category'       => 'required',
			'sub_category'   => 'required',
			// 'brand_id'       => 'required'
		]);
		if($validator) {
			$product = new Product;
			$product['user_id']      =   @$request->user_id;
			$product['brand_id']     =   @$request->brand_id;
			$product['status'] =   'A';
			$product['seller_status'] = 'A';
			if($request->is_featured){
				$product['is_featured']  = 'Y';
			}
			if($request->is_new){
				$product['is_new']  = 'Y';
			}
			if($request->in_offers){
				$product['in_offers']  = 'Y';
			}
			if(@$request->price != null) {
				if($request->price == 0){
					session()->flash("error",__('errors.-5093'));
					return redirect()->back();
				}else{
					$product['price']  = $request->price; 
				}
				   
			}
			if(@$request->stock) {
				$product['stock']  = $request->stock;
			}
			if(@$request->weight) {
				$product['weight']  = $request->weight;    
			}
			$product->save();

			//slug update
			$slug = str_slug($request->title[1][0]);
			$check_slug = Product::where('slug', $slug)->first();
			if(@$check_slug) {
				$slug = $slug.'-'.$product->id;
			}
			$update['slug'] = $slug;

			$productId = sprintf('%06d', $product->id);
			$update['product_code'] = "ALD".$productId;
			try{
				Product::whereId($product->id)->update($update);
				$proId['product_id'] = $product->id;
				$this->addProductToCategory($request,$proId);
				$this->addProductToDetails($request,$proId);
				if(@$request->variant_value_id){
					$this->addProductToVariant($request,$proId);
				}
			} catch(\Exception $th){
                return redirect()->back()->with(['message'=>$th->getMessage()]);
            }
			return redirect()->route('add.product.step.four',$slug);
		}
	}

	/*
	* Method: addProductToCategory
	* Description: It will store product and category into product_categories tables
	* Author: Surajit 
	*/
	private function addProductToCategory(Request $request, $proId){
		$product_cat = new ProductCategory;
		$product_cat['product_id']      =   $proId['product_id'];
		$product_cat['category_id']     =   $request->category;
		$product_cat['level']           =   'P';
		$product_cat->save();

		$product_subcat = new ProductCategory;
		$product_subcat['product_id']      =   $proId['product_id'];
		$product_subcat['category_id']     =   $request->sub_category;
		$product_subcat['level']           =   'S';
		$product_subcat->save();
	}

	/*
	* Method: addProductToDetails
	* Description: It will store product details into product_details tables
	* Author: Surajit 
	*/
	private function addProductToDetails(Request $request, $proId){
		foreach ($request->title as $title => $titleVal) {
			$titleKey = $title;
			$product_detail = new ProductDetail;
			$product_detail['product_id']      =   $proId['product_id'];
			$product_detail['language_id']     =   $title;
			$product_detail['title']           =   $titleVal['0'];
			$product_detail->save();
		}

		foreach ($request->description as $desc => $descVal) {     
			$product_detail_up['description']   =   $descVal[0];
			ProductDetail::where([
				'product_id' =>$proId['product_id'],
				'language_id'=>$desc
			])
			->update($product_detail_up);
		}
	}

	/*
	* Method: addProductToVariant
	* Description: It will store product variant into product_other_options tables
	* Author: Surajit 
	*/
	private function addProductToVariant(Request $request, $proId){
		foreach ($request->variant_value_id as $variant => $variantVal) {
			$var_id = VariantValue::where('id',$variantVal)->first();
			$product_var = new ProductVariantOthers;
			$product_var['product_id']        =   $proId['product_id'];
			$product_var['variant_id']        =   $var_id->variant_id;
			$product_var['variant_value_id']  =   $variantVal;
			$product_var->save();
		}
	}

    /*
    * Method: storeProductStepTwo    
    * Description: It will store product variants into porduct_variants and product_variant_details table
    * Author: Surajit 
    */
    public function storeProductStepTwo(Request $request){
    	$validator = $request->validate([
    		'product_id'        => 'required',
    		'price_variant'     => 'required',
    		'stock_variant'     => 'required',
    		'price'             => 'required|numeric'
    	]);
    	if($validator)
    	{
    		foreach ($request->stock_variant as $stockVar) {
    			$price_var = VariantValue::whereIn('id', $request->price_variant)->get();
    			$stock_var = VariantValue::where('id', $stockVar)->first();
    			$sku       = str_slug($stock_var->default_name).'-'.str_slug($price_var[0]['default_name']).'-'.str_slug($price_var[1]['default_name']);
    			$variants  = '['.$price_var[0]['id'].','.$price_var[1]['id'].','.$stockVar.']';
    			$exists_chk= ProductVariant::where(['variants' => $variants,'product_id' => $request->product_id])->count();
    			if($exists_chk<1){

    				$productVar = new ProductVariant;
    				$productVar['product_id']   =   $request->product_id;
    				$productVar['variants']     =   $variants;
    				$productVar['sku']          =   $sku;
    				$productVar['price']        =   $request->price;
    				$productVar->save();

    				$proId['product_id']       = $request->product_id;
    				$proId['product_var_id']   = $productVar->id;
    				$proId['variant_val_id_1'] = $request->price_variant[0];
    				$proId['variant_val_id_2'] = $request->price_variant[1];
    				$proId['variant_val_id_3'] = $stockVar;
    				$this->addToProductVariantDetails($request,$proId);
    			}else{
    				session()->flash("error",__('success_product.-800'));
    				return redirect()->back();
    			}
    		}
    		return redirect()->route('add.product.step.two',$request->slug);
    	}
    }

    /*
    * Method: addToProductVariantDetails
    * Description: It will store product variant details into product_variant_details table
    * Author: Surajit 
    */
    private function addToProductVariantDetails(Request $request, $proId){
        //for 1st insert            
    	if($proId['variant_val_id_1']){
    		$var_id = VariantValue::where('id',$proId['variant_val_id_1'])->first();
    		$product_var_details = new ProductVariantDetail;
    		$product_var_details['product_id']         =   $proId['product_id'];
    		$product_var_details['product_variant_id'] =   $proId['product_var_id'];
    		$product_var_details['variant_id']         =   $var_id->variant_id;
    		$product_var_details['variant_value_id']   =   $proId['variant_val_id_1'];
    		$product_var_details->save();
    	}

        //for 2nd insert
    	if($proId['variant_val_id_2']){
    		$var_id = VariantValue::where('id',$proId['variant_val_id_2'])->first();
    		$product_var_details = new ProductVariantDetail;
    		$product_var_details['product_id']         =   $proId['product_id'];
    		$product_var_details['product_variant_id'] =   $proId['product_var_id'];
    		$product_var_details['variant_id']         =   $var_id->variant_id;
    		$product_var_details['variant_value_id']   =   $proId['variant_val_id_2'];
    		$product_var_details->save();
    	}

        //for 3rd insert 
    	if($proId['variant_val_id_3']){
    		$var_id = VariantValue::where('id',$proId['variant_val_id_3'])->first();
    		$product_var_details = new ProductVariantDetail;
    		$product_var_details['product_id']         =   $proId['product_id'];
    		$product_var_details['product_variant_id'] =   $proId['product_var_id'];
    		$product_var_details['variant_id']         =   $var_id->variant_id;
    		$product_var_details['variant_value_id']   =   $proId['variant_val_id_3'];
    		$product_var_details->save();
    	}

    }

	/**
	* 
	*/
	public function getVariants(Request $request) {
		$response = [
			'jsonrpc' => '2.0'
		];
		$response['result']['search_variants'] = Variant::select([
			'id',
			'default_title',
			'type',
			'status'
		])
		->where([
			'category_id'       => $request->params['category_id'],
			'sub_category_id'   => $request->params['sub_category_id'],
			'status'            =>  'A'
		])
		->whereIn('type', ['SH'])
		->with(
			'variantByLanguage:id,language_id,variant_id,name',
			'variantValues:id,variant_id,default_name',
			'variantValues.variantValueByLanguage'
		)
		->get();
		$response['result']['informative_variants'] = Variant::select([
			'id',
			'default_title',
			'type',
			'status'
		])
		->where([
			'category_id'       => $request->params['category_id'],
			'sub_category_id'   => $request->params['sub_category_id'],
			'status'            => 'A'
		])
		->whereIn('type', ['I'])
		->with(
			'variantByLanguage:id,language_id,variant_id,name',
			'variantValues:id,variant_id,default_name',
			'variantValues.variantValueByLanguage'
		)
		->get();
		return response()->json($response);
	}

	/*
	* Method: productAddSec
	* Description: Get the product details in add step two
	* Author: Surajit 
	*/
	public function productAddSec($slug){
		if(!@$slug) { 
			return redirect()->back();
		}

		$product     = Product::where('slug', $slug)->with('productByLanguage')->first();
		$product_cat = ProductCategory::where([
			'product_id' => $product->id,
			'level' => 'S'
		])
		->first();

		$data['product_id']    = $product->id;
		$data['slug']          = $slug;
		$data['productName']   = $product->productByLanguage->title;
		$data['price_variant'] = Variant::select([
			'id',
			'default_title',
			'type',
			'status'
		])
		->where([
			'sub_category_id'    =>  $product_cat->category_id,
			'type'               =>  'P',
			'status'			=> 'A'
		])
		->with(
			'variantByLanguage:id,language_id,variant_id,name',
			'variantValues:id,variant_id,default_name'
		)
		->get();  

		$data['stock_variant'] = Variant::select([
			'id',
			'default_title',
			'type',
			'status'
		])
		->where([
			'sub_category_id'    =>  $product_cat->category_id,
			'type'               =>  'S',
			'status'			=> 'A'
		])
		->with(
			'variantByLanguage:id,language_id,variant_id,name',
			'variantValues:id,variant_id,default_name'
		)
		->get();

		$data['fetch_data'] = ProductVariant::where('product_id',$product->id)
		->with(['productVariantDetails',
			'productVariantDetailsGroup.getVariantValueId.variantValueByLanguage'])
		->groupBy('group_id')
		->get();

		if($data['price_variant']->count() == 0 && $data['stock_variant']->count() == 0) {
			return redirect()->route('add.product.step.four', $slug);
		}
		
		return view("admin.modules.products.add_products_step_2")->with($data);
		
	}

	/**
	* To cartesian the variants
	* Author: Sanjoy
	*/
	private function cartesian($input) {
		$result = array();
		foreach ($input as $key=>$values) {
	        // If a sub-array is empty, it doesn't affect the cartesian product
			if (empty($values)) {
				continue;
			}

	        // Seeding the product array with the values from the first sub-array
			if (empty($result)) {
				foreach($values as $value) {
					$result[] = array($key => $value);
				}
			}
			else {
	            // Second and subsequent input sub-arrays work like this:
	            //   1. In each existing array inside $product, add an item with
	            //      key == $key and value == first item in input sub-array
	            //   2. Then, for each remaining item in current input sub-array,
	            //      add a copy of each existing array inside $product with
	            //      key == $key and value == first item of input sub-array

	            // Store all items to be added to $product here; adding them
	            // inside the foreach will result in an infinite loop
				$append = array();

				foreach($result as &$product) {
	                // Do step 1 above. array_shift is not the most efficient, but
	                // it allows us to iterate over the rest of the items with a
	                // simple foreach, making the code short and easy to read.
					$product[$key] = array_shift($values);

	                // $product is by reference (that's why the key we added above
	                // will appear in the end result), so make a copy of it here
					$copy = $product;

	                // Do step 2 above.
					foreach($values as $item) {
						$copy[$key] = $item;
						$append[] = $copy;
					}

	                // Undo the side effecst of array_shift
					array_unshift($values, $product[$key]);
				}

	            // Out of the foreach, we can add to $results now
				$result = array_merge($result, $append);
			}
		}

		return $result;
	}

	/*
	* Method: storeProductVariant    
	* Description: It will store product variants into porduct_variants and product_variant_details table in ajax
	* Author: Sanjoy 
	*/
	public function storeProductVariant(Request $request) {
		$response = [
			'jsonrpc'	=> '2.0'
		];

		$validator = $request->validate([
			'product_id' => 'required'
		]);
		$variantGroup = [];
		if($validator) {
			# if price dependent available
			if(@$request->price_variant) {
				$insert = [
					'product_id' => $request->product_id,
					'price'		 => $request->price
				];
				$variants = [];
				if(@$request->stock_variant) {
					$variants = $this->cartesian($request->stock_variant);
					foreach($variants as $key=>$value) {
						$variantGroup[$key] = array_map('intval', $value);
						foreach ($request->price_variant as $key1 => $value1) {
							array_push($variantGroup[$key], intval($value1));
						}
					}
				} else {
					$variantGroup[] = array_map('intval', $request->price_variant);
				}
			} else {
				$insert = [
					'product_id' => $request->product_id
				];
				$variants = [];
				if(@$request->stock_variant) {
					$variants = $this->cartesian($request->stock_variant);
					
					$variantGroup = [];
					foreach($variants as $key=>$value) {
						$variantGroup[$key] = array_map('intval', $value);
						if(@$request->price_variant) {
							foreach ($request->price_variant as $key1 => $value1) {
								array_push($variantGroup[$key], intval($value1));
							}
						}
					}
				}
			}
			
			$groupId = 0;
			foreach ($variantGroup as $key => $value) {

				# checking for same combinition is already exist or not
				$exists_chk = ProductVariant::where(['variants' => json_encode($value), 'product_id' => $request->product_id])->count();
				if($exists_chk > 0) {
					$msg = __('success_product.-800');
					return response()->json(['error' => $msg]);
				}

				$sku = [];
				$insert['variants'] = json_encode($value);
				$productVariant = ProductVariant::create($insert);
				if($key == 0) {
					$productVariant['group_id'] = $groupId = $productVariant->id;
				} else {
					$productVariant['group_id'] = $groupId;
				}
				// insert to product variant details table
				foreach ($value as $key1 => $value1) {
					$insertArr = [];
					$variantValue = VariantValue::with(['variantValueByLanguage'])->find($value1);
					array_push($sku, $variantValue->variantValueByLanguage->name);
					$insertArr['product_variant_id'] 	= $productVariant->id;
					$insertArr['variant_id'] 			= $variantValue->variant_id;
					$insertArr['variant_value_id'] 		= $value1;
					$insertArr['product_id']			= $request->product_id;
					$insertArr['group_id']				= $groupId;
					ProductVariantDetail::create($insertArr);
				}

				// update sku and group ID
				$productVariant['sku'] = implode('-', $sku);
				$productVariant->save();
			}

			$fetch_data = ProductVariant::where('product_id', $request->product_id)
			->with(['productVariantDetails',
				'productVariantDetailsGroup.getVariantValueId.variantValueByLanguage'])
			->groupBy('group_id')
			->get();

			$product_cat = ProductCategory::where([
				'product_id' => $request->product_id,
				'level' => 'S'
			])
			->first();

			$price_variant = Variant::select([
				'id',
				'default_title',
				'type',
				'status'
			])
			->where([
				'sub_category_id'    =>  $product_cat->category_id,
				'status'             =>  'A',
				'type'               =>  'P'
			])
			->get();
			
			// if subcategory is price dependent then update product base price
			if($price_variant->count()) {
				$this->updateProductBasePrice($request->product_id);
			}

			$output = '';
			$i=0;
			foreach($fetch_data as $fd)
			{
				if($i!=0){
					$output .= '<div class="adedfrm remove-rw-'.$fd->group_id.'">';
				} else {
					$output .= '<div class="adedfrm">';
				}
				foreach($fd->productVariantDetails as $pvd){
					$output .= '<div class="form-group">';
					if($i==0){
						$output .= '<label style="font-weight:600" for="input-select" class="col-form-label">'. $pvd->variantByLanguage->name .'</label>';
					}
					$output .= '<span class="show-detail remove-rw-'.$fd->id.'">'. variantValues($pvd->product_variant_id, $fd->group_id, $pvd->variant_id,  $pvd->getVariantValueId) .'</span>
					</div>';
				}
				if($price_variant->count()) {
					$output .= ' <div class="form-group">';

					if($i==0){
						$output .= '<label style="font-weight:600" for="price" class="col-form-label">Price</label>';
					}
					$output .= '<span class="show-detail remove-rw-'.$fd->id.'">'.$fd->price. ' '.getCurrency().'</span>
					</div>';

				}
				$output .= '<div class="form-group">';

				if($i==0){
					$output .= '<label style="font-weight:600" for="price" class="col-form-label">Action</label>';
				}
				$output .= '<a href="javascript:;" title="Remove" class="removeVar remove-rw-'.$fd->id.'"  data-id="'. $fd->group_id .'"><i class="fas fa-times"></i></a>
				</div>                                          
				</div>';
				$i++;
			}
		}
		$response['result']['output'] = $output;
		return response()->json($response, 200);
	}

	/**
	* 
	*/
	private function updateProductBasePrice($productId) {
		//dd('c');
		// update product base price 
		$basePrice = ProductVariant::where(['product_id' => $productId])->min('price');
		Product::where(['id' => $productId])->update(['price' => $basePrice]);
	}


	/*
	* Method: deleteProductVariant
	* Description: It will delete from product_variants & product_variant_details table
	* Author: Surajit 
	*/
	public function deleteProductVariant($id){
		ProductVariantDetail::where('product_variant_id',$id)->delete();
		ProductVariant::whereId($id)->delete();
		return redirect()->back();  
	}

	/*
	* Method: removeVar
	* Description: It will delete from product_variants & product_variant_details table in ajax
	* Author: Surajit 
	*/
	public function removeVar($id){
		$response = [
			'jsonrpc'   => '2.0'
		];      
		$productId = ProductVariant::where('group_id', $id)->first();
		ProductVariantDetail::where('group_id',$id)->delete();
		ProductVariant::where('group_id', $id)->delete();
		$proVarCount = ProductVariant::where('product_id', $productId->product_id)->count();
		$response['success']['proVarCount'] = $proVarCount;
		return response()->json($response, 200);
	}

	/*
	* Method: productAddThrd
	* Description: Get the product details in add step three
	* Author: Surajit 
	*/
	public function productAddThrd($slug){
		if(!$slug){
			return redirect()->back();
		}
		$product = Product::where('slug', $slug)->with('productByLanguage')->first();
		$data['product_id']    = $product->id;            
		$data['slug']          = $slug;    
		$data['productName']   = $product->productByLanguage->title;
		$data['fetch_data'] = ProductVariant::where('product_id',$product->id)
		->with('productVariantDetails.variantValueName.variantValueByLanguage','productVariantDetails.variantByLanguage')
		->get();
		return view("admin.modules.products.add_products_step_3",$data);
	}

	/*
	* Method: storeProductStepThree
	* Description: It will update stock quantity into product_variants table
	* Author: Surajit 
	*/
	public function storeProductStepThree(Request $request){
		//dd($request->stock_quantity);
		$i = 1;
		foreach ($request->stock_quantity as $product_var_id => $quantity) {
			$pv = ProductVariant::whereId($product_var_id)->first();
			$varient_id = json_decode($pv->variants);
			$flag = 0;
			foreach ($varient_id as $variant) {
				$det = ProductVariantDetail::where('variant_value_id',$variant)->first();
				$varient_det = Variant::whereId($det->variant_id)->first();
				if($varient_det->type == 'P'){
					$flag = 1;
				}
			}
			
			if($i == 1 && $flag == 1){
				$this->updateProductBasePrice($pv->product_id);
			}
			if($pv->stock_quantity > $request->weight[$product_var_id]) {
				$this->sendNotificationToCustomer($pv->product_id, $pv->id);
			}
			ProductVariant::whereId($product_var_id)->update([
				'stock_quantity' => $quantity,
				'weight'		 => $request->weight[$product_var_id]
			]);
			$i ++ ;
		}
		return redirect()->route('add.product.step.four',$request->slug);
	}

	/*
	* Method: productAddFourth
	* Description: Get the product details in add step four
	* Author: Surajit 
	*/
	public function productAddFourth($slug){
		if(!$slug){
			return redirect()->back();
		}     
		$data['product']  	 = $product  = Product::where('slug', $slug)->with('productByLanguage')->first();
		$data['productName'] = $product->productByLanguage->title;
		$data['productImg']  = ProductImage::where('product_id',  $product->id)->get();
		$data['pro_sub_cat'] = ProductCategory::where('product_id', $data['product']->id)
		->where('level', 'S')
		->first();
		$data['price_variant'] = Variant::select([
			'id',
			'default_title',
			'type',
			'status'
		])
		->where([
			'sub_category_id'    =>  @$data['pro_sub_cat']->category_id,
			'status'             =>  'A',
			'type'               =>  'P'
		])
		->count();
		$data['stock_variant'] = Variant::select([
			'id',
			'default_title',
			'type',
			'status'
		])
		->where([
			'sub_category_id'    =>  @$data['pro_sub_cat']->category_id,
			'status'             =>  'A',
			'type'               =>  'S'
		])
		->count();
		return view("admin.modules.products.add_products_step_4",$data);
	}


	/*
	* Method: storeProductStepFour    
	* Description: It will store product images into porduct_images table
	* Author: Surajit 
	*/
	public function storeProductStepFour(Request $request){
		// try{
			$proImage = ProductImage::Where([
				'product_id' => $request->product_id
			])
			->count();
			if($request->image) {
				$i=0;
				foreach($request->image as $key=>$file){
					$imgName   = $i.time().".".$file->getClientOriginalExtension();

					//for update image and check is defalut
					$pro_is_default = ProductImage::Where([
						'product_id' => $request->product_id,
						'is_default' => 'Y'
					])
					->first();
					if($pro_is_default) {
						$is_default="N";                            
					} else {
						if($i==0) {
							$is_default="Y";
						} else {
							$is_default="N";
						}
					}
					$file->move('storage/app/public/products', $imgName);
					
					$img = Image::make('storage/app/public/products/' . $imgName);

					# resize start
					if(!is_dir('storage/app/public/products/600')) {
						mkdir('storage/app/public/products/600');
					}
					if(!is_dir('storage/app/public/products/300')) {
						mkdir('storage/app/public/products/300');
					}
					if(!is_dir('storage/app/public/products/80')) {
						mkdir('storage/app/public/products/80');
					}
					# resize mid
					$img->resize(600, null, function($constraint) {
						$constraint->aspectRatio();
					})->save('storage/app/public/products/600/' . $imgName, 60);

					# resize small
					$img->resize(300, null, function($constraint) {
						$constraint->aspectRatio();
					})->save('storage/app/public/products/300/' . $imgName, 60);

					# resize thumb
					$img->resize(80, null, function($constraint) {
						$constraint->aspectRatio();
					})->save('storage/app/public/products/80/' . $imgName, 60);

					$proImg = ProductImage::create([
						"product_id"      => $request->product_id,
						"image"           => $imgName,
						"is_default"      => $is_default
					]);
					$i++;
					$imgName="";
				}
			} 
		/*}
        catch(\Exception $e){
            return redirect()->back()->withError(['message' => $e->getMessage(), 'meaning' => "Error!Product image is not uploaded !"]);
        }*/
		// else {
		// 	if($proImage<1) {
		// 		session()->flash("error",__('success_product.-3001'));
		// 		return redirect()->back();				
		// 	}
		// }
		session()->flash("success",__('success_product.-4001'));
		return redirect()->route('manage.product');
	}

	/*
	* Method: removeImg
	* Description: It will delete from product_images table in ajax
	* Author: Surajit 
	*/
	public function removeImg($id){
		$response = [
			'jsonrpc'   => '2.0'
		];      
		ProductImage::whereId($id)->delete();
		return response()->json(['success' => 'success']);
	}

	/*
	* Method: setDefaultImg
	* Description: It will set default into product_images table
	* Author: Surajit 
	*/
	public function setDefaultImg($id){
		$product = ProductImage::whereId($id)->first();
		ProductImage::whereId($id)->update([
			'is_default' => 'Y'
		]);
		ProductImage::where('id', '!=', $id)
		->where('product_id', $product->product_id)
		->update([
			'is_default' => 'N'
		]);
		return redirect()->back();
	}


	/*
	* Method: editProduct
	* Description: to edit product page step 1
	* Author: Surajit 
	*/
	public function editProduct($slug){
		$data['user']           = Merchant::select([
			'id',
			'fname',
			'lname'
		])
		->where('status','A')
		->get();

		$data['category']       = Category::select([
			'id', 'parent_id', 'status'
		])
		->where([
			'parent_id' =>  '0',
			'status'    =>  'A'
		])
		->with('categoryByLanguage:id,category_id,language_id,title')
		->get();

		$data['language']       = Language::select([
			'id', 'name', 'prefix', 'status'
		])
		->where([
			'status'    =>  'A'
		])
		->get();

		$data['product'] = $product = Product::where('slug',$slug)
		->with('productMarchant','productBarnd')
		->first();

		$data['pro_cat'] = $pro_cat = ProductCategory::where('product_id', $product->id)
		->where('level', 'P')
		->first();

		$data['pro_sub_cat']    = ProductCategory::where('product_id', $product->id)
		->where('level', 'S')
		->first();

		$data['subCategory']    = Category::select([
			'id',
			'parent_id',
			'status'
		])
		->where([
			'status' =>  'A'
		])
		->where([
			'parent_id' =>  @$pro_cat->category_id
		])
		->with('categoryByLanguage:id,category_id,language_id,title')
		->get();

		$data['brand']          = Brand::select([
			'id', 'name', 'status'
		])
		->where([
			'status'      =>  'A',
			'category_id' =>  @$pro_cat->category_id
		])
		->with('brandDetailsByLanguage')
		->get();

		$data['pro_details']    = ProductDetail::where('product_id', $product->id)
		->get();

		$data['pro_variants']    = ProductVariantOthers::where('product_id', $product->id)
		->get();

		$data['variant']       = Variant::select([
			'id',
			'default_title',
			'type',
			'status'
		])
		->where([
			'status'    =>  'A',
			'sub_category_id' => @$data['pro_sub_cat']->category_id
		])
		->whereIn('type', ['I','SH'])
		->with(
			'variantByLanguage:id,language_id,variant_id,name',
			'variantValues:id,variant_id,default_name',
			'variantValues.variantValueByLanguage'
		)
		->get();

		$data['price_variant'] = Variant::select([
			'id',
			'default_title',
			'type',
			'status'
		])
		->where([
			'sub_category_id'    =>  @$data['pro_sub_cat']->category_id,
			'type'               =>  'P',
			'status'			=> 'A'
		])
		->count();

		$data['stock_variant'] = Variant::select([
			'id',
			'default_title',
			'type',
			'status'
		])
		->where([
			'sub_category_id'    =>  @$data['pro_sub_cat']->category_id,
			'type'               =>  'S',
			'status'			=> 'A'

		])
		->count();

		$data['total_orders'] = OrderDetail::where(['product_id' => $product->id])->count();
		return view("admin.modules.products.edit_products_step_1", $data);
	}

	/*
	* Method: updateProduct
	* Description: It will update product into mutiple tables
	* Author: Surajit 
	*/
	public function updateProduct(Request $request, $slug) {
		$validator = $request->validate([
			'user_id'        => 'required',
			'category'       => 'required',
			'sub_category'   => 'required',
			// 'brand_id'       => 'required'
		]);
		if($validator) {
			$product = Product::where('slug', $slug)->first();
			$update_pro['user_id']      = @$request->user_id;
			$update_pro['brand_id']     = @$request->brand_id;
			if($request->discount_price == 0) {
				$update_pro['discount_price'] = $request->discount_price;
				$update_pro['from_date']    = $request->from_date;
				$update_pro['to_date']      = $request->to_date;
			}elseif($request->discount_price && $request->from_date && $request->to_date) {
				$update_pro['discount_price'] = $request->discount_price;
				$update_pro['from_date']    = $request->from_date;
				$update_pro['to_date']      = $request->to_date;
			}
			if($request->is_featured) {
				$update_pro['is_featured']  =   'Y';
			} else {
				$update_pro['is_featured']  =   'N';				
			}
			if($request->is_new){
				$update_pro['is_new']  = 'Y';
			}else{
				$update_pro['is_new']  = 'N';
			}
			if($request->in_offers){
				$update_pro['in_offers']  = 'Y';
			}else{
				$update_pro['in_offers']  = 'N';
			}
			// if(@$request->price || @$request->price == 0) {
			// 	$update_pro['price']  =   $request->price;    
			// }
			if(@$request->price != null) {
				if($request->price == 0){
					session()->flash("error",__('errors.-5093'));
					return redirect()->back();
				}else{
					if($request->price < $request->discount_price){
						session()->flash("error",__('errors.-5094'));
						return redirect()->back();
					}else{
						$update_pro['price']  =   $request->price;  
					}
				}
				  
			}
			if(@$request->stock || @$request->stock == 0) {
				$update_pro['stock']  =   $request->stock;
				if(@$request->stock > 0) {
					$this->sendNotificationToCustomer($product->id);
				}
			}
			if(@$request->weight || @$request->weight == 0) {
				$update_pro['weight']  =   $request->weight;    
			}
			
			$a = Product::whereId($product->id)->update($update_pro);
			//slug update
			$new_slug = str_slug($request->title[1]);
			if($new_slug != $product->slug){
				$check_slug = Product::where('slug', $new_slug)->first();
				if(@$check_slug) {
					$new_slug = $new_slug.'-'.$product->id;
				}
				Product::whereId($product->id)->update(['slug' => $new_slug]);
				$slug = $new_slug;
			}
			# check sub category changed or not
			$productSubCategory = ProductCategory::where([
				'product_id' => $product->id,
				'level'      => 'S'
			])->first();
			
			
			if(@$request->sub_category && $request->sub_category != $productSubCategory->category_id) {
				# when sub category changed remove all previous variant
				$variant = Variant::where(['category_id' => $request->category,'sub_category_id' => $request->sub_category,'status' => 'A'])->get();
				$v_flag = 0;
				foreach ($variant as $k => $va) {
					if($va->type == 'P' || $va->type == 'S'){
						$v_flag = 1;
					}
				}
				if($v_flag == 1){
					// empty discount price , from date , to_date
					$update_dis['discount_price'] = 0;
					$update_dis['from_date']    = NULL;
					$update_dis['to_date']      = NULL;
					Product::whereId($product->id)->update($update_dis);
				}
				
				ProductVariantDetail::where(['product_id' => $product->id])->delete();
				ProductOtherOption::where(['product_id' => $product->id])->delete();
				ProductVariant::where(['product_id' => $product->id])->delete();

				
			}

			$proId['product_id'] = $product->id;
			$totalOrders = OrderDetail::where(['product_id' => $product->id])->count();
			if($totalOrders == 0) {
				$this->updatedProductToCategory($request, $proId);
			}
			$this->updatedProductToDetails($request, $proId);

			//if(@$request->variant_value_id){
				$this->updatedProductToVariant($request,$proId);
			//}
			// } catch (\Exception $ex) {
			//      DB::rollback();
			//     return response()->json(['error' => $ex->getMessage()], 500);
			// }

			return redirect()->route('add.product.step.four',$slug);
		}
	}

	private function sendNotificationToCustomer($productId, $productVariantId = NULL) {
		$product = Product::with(['productByLanguage', 'defaultImage'])->where(['id' => $productId])->first();
		$productVariant = ProductVariant::with([
			'productVariantDetails.variantByLanguage', 
			'productVariantDetails.variantValueByLanguage'
		])
		->where(['id' => $productVariantId])
		->first();

		$notificationMsg = 'New stock has been added for - '. $product->productByLanguage->title;
		if(@$productVariant) {
			$totVar = count($productVariant->productVariantDetails);
			$notificationMsg .= ' (';
			foreach ($productVariant->productVariantDetails as $key => $value) {
				$notificationMsg .= $value->variantByLanguage->name .': '. $value->variantValueByLanguage->name;
				if(($totVar - 1) > $key) {
					$notificationMsg .= ', ';
				}
			}
			$notificationMsg .= ')';
		}

		$notificationMsg .= '. Hurry on!';

		# for web notification
		$productNotification = ProductNotification::where(['product_id' => $productId])->where('device_type', 'W');
		if(@$productVariantId) {
			$productNotification = $productNotification->where(['variant_id' => $productVariantId]);
		}
		$firebaseIds = $productNotification->pluck('firebase_token')->toArray();
		
		$title = 'Product in stock!';
		$fields = array (
            'registration_ids' => $firebaseIds,
            'data' => array (
                "message"       => $notificationMsg,
                "title"         => $title,
                "image"         => url('firebase-logo.png'),
                "click_action"  => route('product', $product->slug),
            ),
            'notification'      => array (
                "body"          => $notificationMsg,
                "title"         => $title,
                "click_action"  => route('product', $product->slug),
                "icon"          => "url('firebase-logo.png')",
            )
        );
        $fields = json_encode ( $fields );
        
        $headers = array(
            'Authorization: key=' . env('FIREBASE_KEY'),
            'Content-Type: application/json',
        );
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
        $result = curl_exec ( $ch );
        curl_close ( $ch );

        # for android notification
        $productAndroidNotification = ProductNotification::where(['product_id' => $productId])->where('device_type', 'A');
		if(@$productVariantId) {
			$productAndroidNotification = $productAndroidNotification->where(['variant_id' => $productVariantId]);
		}
		$androidFirebaseIds = $productAndroidNotification->pluck('firebase_token')->toArray();
        if(count($androidFirebaseIds)) {
            $msg                      = array();
            $msg['title']             = $title;
            $msg["body"]              = $notificationMsg;
            $msg['link_type']         = "Y";
            $msg['is_linked']         = "Y";
            $msg['image']             = '';
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]           = "10";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'registration_ids' => $androidFirebaseIds,
                'data'             => $msg
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            
            // Closing Curl
            curl_close($ch);
        }

        # for IOS notification
        $productIosNotification = ProductNotification::where(['product_id' => $productId])->where('device_type', 'I');
		if(@$productVariantId) {
			$productIosNotification = $productIosNotification->where(['variant_id' => $productVariantId]);
		}
		$iosFirebaseIds = $productIosNotification->pluck('firebase_token')->toArray();
        if(count($iosFirebaseIds)) {
            $msg                      = array();
            $msg['title']             = $title;
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]            = "10";
            $msg["mutable-content"]   = true;
            $msg["body"]              = $notificationMsg;

            $data['message'] = $notificationMsg;
            $data['type'] = "1";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'category'                  => "CustomSamplePush",
                'content_available'         => true,
                'mutable_content'           => true,
                'registration_ids'          => $iosFirebaseIds,
                'notification'              => $msg,
                'data'                      => $data
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            // Closing Curl
            curl_close($ch);
        }

        # send mail for in stock
        $emails = $productNotification->where('email', '!=', '')->pluck('email')->toArray();
        if(@$emails) {
        	$mailData = [
	        	'emails'  => $emails,
	        	'message' => $notificationMsg,
	        	'product' => $product
	        ];
	        Mail::send(new StockNotification($mailData));	
        }
	}

	/*
	* Method: addProductToCategory
	* Description: It will update product and category into product_categories tables
	* Author: Surajit
	*/
	private function updatedProductToCategory(Request $request, $proId){
		$update_product_cat['category_id']     =   $request->category;
		ProductCategory::where([
			'product_id' => $proId['product_id'],
			'level'      => 'P'
		])->update($update_product_cat);

		$update_product_subcat['category_id']     =   $request->sub_category;
		ProductCategory::where([
			'product_id' => $proId['product_id'],
			'level'      => 'S'
		])->update($update_product_subcat);
	}

	/*
	* Method: updatedProductToDetails
	* Description: It will update product details into product_details tables
	* Author: Surajit 
	*/
	private function updatedProductToDetails(Request $request, $proId){
		foreach ($request->title as $title => $titleVal) {
			$update_product_detail['title']   =   $titleVal;
			ProductDetail::where([
				'product_id'  => $proId['product_id'],
				'language_id' => $title
			])->update($update_product_detail);
		}
		foreach ($request->description as $desc => $descVal) {     
			$update_product_detail1['description']   =   $descVal;
			ProductDetail::where([
				'product_id' => $proId['product_id'],
				'language_id'=> $desc
			])
			->update($update_product_detail1);
		}
	}

	/*
	* Method: updatedProductToVariant
	* Description: It will update product variant into product_other_options tables
	* Author: Surajit 
	*/
	private function updatedProductToVariant(Request $request, $proId){
		ProductVariantOthers::where('product_id', $proId['product_id'])->delete();
		if($request->variant_value_id[0] != null){
			foreach (@$request->variant_value_id as $variant => $variantVal) {
				$var_id = VariantValue::where('id',$variantVal)->first();
				$product_var = new ProductVariantOthers;
				$product_var['product_id']        =   $proId['product_id'];
				$product_var['variant_id']        =   $var_id->variant_id;
				$product_var['variant_value_id']  =   $variantVal;
				$product_var->save();
			}
		}
		
	}

	/*
	* Method: fetchSubcat
	* Description: It will fetch sub categories and brands of category
	* Author: Surajit 
	*/
	public function fetchSubcat(Request $request){
		$response = [
			'jsonrpc'   => '2.0'
		];      
		$value = $request->get('value');
		$data  = Category::select([
			'id',
			'parent_id',
			'status'
		])
		->where([
			'parent_id' =>  $value,
			'status'    =>  'A'
		])
		->with('categoryByLanguage:id,category_id,language_id,title')
		->get();
		$brandData  = Brand::select([
			'id',
			'name',
			'status'
		])
		->where([
			'category_id' => $value,
			'status'      =>  'A'
		])
		->with('brandDetailsByLanguage')
		->get();

		$output = '<option value="">Select Sub Category</option>';
		foreach($data as $row) {
			$output .= '<option value="'.$row->id.'">'.@$row->categoryByLanguage->title.'</option>';
		}

		$brand = '<option value="">Select Brand</option>';
		foreach($brandData as $bn) {
			$brand .= '<option value="'.$bn->id.'">'.@$bn->brandDetailsByLanguage->title.'</option>';
		}
		$response['result']['output'] = $output;
		$response['result']['brand']  = $brand;
		return response()->json($response, 200);
	}

	/**
	*   Method  : statusProduct
	*   Use     : change status of product.
	*   Author  : Surajit
	*/
	public function statusProduct($id){
		$product=Product::find($id);
		if($product->status == "A") {
			Product::where('id',$id)->update(
				[
					'status' => "I"
				]);
		} elseif ($product->status == "I" || $product->status == "W") {
			Product::where('id',$id)->update(
				[
					'status' => "A",
					'seller_status'=> "A"
				]);
		}
		session()->flash("success",__('success_product.-801'));
		return redirect()->back(); 
	}

	/*
	*   Method  : statusProduct by ajax
	*   Use     : change status of product.
	*   Author  : Jayatri
	*/
	public function updateStatusProduct(Request $request){
		$product=Product::with('productByLanguage')->where('id',@$request->id)->first();

		if(@$product){
			if($product->status == "A") {
				$inc = CartDetail::where(['product_id'=>$product->id])->first();
				if($inc){
					return 0;	
				}
				Product::where('id',@$request->id)->update([
					'status' => "I",
					'seller_status'=> "I"
				]);
			} elseif ($product->status == "W") {
				Product::where('id',@$request->id)->update([
					'status' => "A",
					'seller_status'=> "A"
				]);
				try{
					$product=Product::with('productByLanguage')->where('id',@$request->id)->first();
					$merchant = Merchant::where('id',$id)->first();
	                $mailDetails = new \StdClass();
	                $mailDetails->to    = @$merchant->email;
	                $mailDetails->fname = @$merchant->fname;
	                $mailDetails->id    = @$merchant->id;
	                $mailDetails->user = @$merchant;
	                $mailDetails->product = @$product;
	                Mail::send(new sendApproveProductMail($mailDetails));
	            }catch(\Exception $e){

	            }
			}
			elseif ($product->status == "I") {

				
				Product::where('id',@$request->id)->update([
					'status' => "A",
					'seller_status'=> "A"
				]);
				try{
					$product=Product::with('productByLanguage')->where('id',@$request->id)->first();
					$merchant = Merchant::where('id',$id)->first();
	                $mailDetails = new \StdClass();
	                $mailDetails->to    = @$merchant->email;
	                $mailDetails->fname = @$merchant->fname;
	                $mailDetails->id    = @$merchant->id;
	                $mailDetails->user = @$merchant;
	                $mailDetails->product = @$product;
	                Mail::send(new sendApproveProductMail($mailDetails));
	            }catch(\Exception $e){

	            }
			}
			$product=Product::where('id',@$request->id)->first();
			return response()->json($product);
		}else{
			session()->flash("error",__('success_product.-5055')['meaning']);
			return 0;
		} 
	}

	/*
	* Method: removeProduct
	* Description: It will delete from products, product_categories, product_details, product_images & product_other_options table
	* Author: Surajit 
	*/
	public function removeProduct($id){
		ProductDetail::where('product_id',$id)->delete();
		ProductCategory::where('product_id',$id)->delete();
		ProductVariantOthers::where('product_id',$id)->delete();

		$productImg = ProductImage::where('product_id',$id)->get();
		foreach($productImg as $pro){
			$productImage = 'storage/app/public/products/'.$pro->image; // get previous image from folder
			if (File::exists($productImage)) { // unlink or remove previous image from folder
				unlink($productImage);
			}
		}
		ProductImage::where('product_id',$id)->delete();
		//delete form product or change status
		$product = Product::whereId($id)->first();
		Product::whereId($product->id)->update(['status' => 'D']);
		session()->flash("success",__('success_product.-802'));
		return redirect()->back();
	}
	/*
	* Method: deleteProduct
	* Description: It will update the status D
	* Author: Jayatri 
	*/
	public function deleteProduct(Request $request){
		$id = @$request->id;
		//delete form product or change status
		$product = Product::whereId($id)->first();
		if(@$product){
			$is_exist_in_cart = CartDetail::where(['product_id'=>$id])->first();
			if(@$is_exist_in_cart){
				$data['output'] = 0;
				$data['message'] = "Product is associated with cart!you cannot delete this product!";
				return $data;
			}else{
				Product::whereId($product->id)->update(['status' => 'D']);	
				$data['output'] = 1;
				$data['message'] = "You have successfully deleted product!";
			}
			$data['output'] = 1;
			$data['message'] = "You have successfully deleted product!";
			return $data;
		}else{
			$data['output'] = 0;
			$data['message'] = "Error!You have not deleted product!";
			return $data;
		}
	}

	/*
	* Method: setDiscount
	* Description: Get the product details in set discount page
	* Author: Surajit
	*/
	public function setDiscount($slug){
		if(!$slug){
			return redirect()->back();
		}
		$product     = Product::where('slug', $slug)->first();
		$data['product_id']    = $product->id;
		$data['slug']          = $slug;
		$data['fetch_data']    = ProductVariant::where('product_id',$product->id)
		->with('productVariantDetails.variantValueName.variantValueByLanguage','productVariantDetails.variantByLanguage','productVariantDetails.getVariant')
		->get();
		return view("admin.modules.products.product_discount",$data);
	}

	/*
	* Method: storeDiscount
	* Description: It will update discount into product_variants table
	* Author: Surajit
	*/
	public function storeDiscount(Request $request){
		//dd($request->all());
		$product = Product::select('id','discount_price','price')->where('id', $request->product_id)->first();

		foreach ($request->discount_price as $product_var_id => $discountPrice) {
			if($request->price[$product_var_id] == $product->price && $discountPrice != null) {
				Product::whereId($request->product_id)
				->update([
					'discount_price' => $discountPrice,
					'from_date' 	 => $request->from_date[$product_var_id],
					'to_date' 		 => $request->to_date[$product_var_id]
				]);
			}
			// the price of available discount_price pair
			if($discountPrice > 0 && $request->to_date[$product_var_id] >= date('Y-m-d')) {					
				Product::whereId($request->product_id)
				->update([
					'pairing_price' => $request->price[$product_var_id]
				]);
			}

			ProductVariant::whereId($product_var_id)
			->update([
				'discount_price' => $discountPrice,
				'from_date' 	 => $request->from_date[$product_var_id],
				'to_date' 		 => $request->to_date[$product_var_id]
			]);
		}
		session()->flash("success",__('success_product.-4002'));
		return redirect()->route('manage.product');
	}

	
	/**
	* Method: resizePreviousProductImages
	* Description: This method is used to resize all previous images.
	* Author: Sanjoy
	*/
	public function resizePreviousProductImages() {
		$images = ProductImage::get();
		foreach ($images as $key => $value) {
			$imgName = $value->image;
			if(file_exists('storage/app/public/products/' . $imgName)) {
				$img = Image::make('storage/app/public/products/' . $imgName);

				# resize start
				if(!is_dir('storage/app/public/products/600')) {
					mkdir('storage/app/public/products/600');
				}
				if(!is_dir('storage/app/public/products/300')) {
					mkdir('storage/app/public/products/300');
				}
				if(!is_dir('storage/app/public/products/80')) {
					mkdir('storage/app/public/products/80');
				}
				# resize mid
				$img->resize(600, null, function($constraint) {
					$constraint->aspectRatio();
				})->save('storage/app/public/products/600/' . $imgName, 60);

				# resize small
				$img->resize(300, null, function($constraint) {
					$constraint->aspectRatio();
				})->save('storage/app/public/products/300/' . $imgName, 60);
				
				# resize thumb
				$img->resize(80, null, function($constraint) {
					$constraint->aspectRatio();
				})->save('storage/app/public/products/80/' . $imgName, 60);
			}
		}
	}
	public function setFeaturedProduct(Request $request){
        try{
            $id = $request->data['id'];
            $product = Product::with('productByLanguage')->whereId($id)->first();
            if(@$product){
            	if(@$product->is_featured == 'Y'){
            		$update['is_featured'] = 'N'; 	
            		$data['message'] = "You have successfully removed ".$product->productByLanguage->title."  from featured product!";
            	}else{
            		$update['is_featured'] = 'Y'; 	
            		$data['message'] = "You have successfully set ".$product->productByLanguage->title." as featured product!";
            	}
                Product::where('id',$id)->update($update); 
                $data['status'] = 1;
                $product = Product::whereId($id)->first();
                
                return response()->json($data,200);
            }else{
            	$data['status'] = 1;
            	$data['message'] = "Product is either deleted or not found from given id!";
                return response()->json($data,200);
            }
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
            return response()->json($data,200);
        }
    }
}
