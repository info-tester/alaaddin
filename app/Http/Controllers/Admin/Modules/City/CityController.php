<?php

namespace App\Http\Controllers\Admin\Modules\City;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\OrderMaster;
use App\Merchant;
use App\User;
use App\Models\UserAddressBook;
use App\Customer;
use Validator;
use Auth;
use App\Models\Language;
use App\Models\Admin;
use App\Models\CitiDetails;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;

class CityController extends Controller
{
    protected $cities,$language,$countries;
    public function __construct(
        Request $request
    )
    {
        $this->middleware('admin.auth:admin');
    }
    public function manageCity(Request $request){
        $city = City::where('status','!=','D')->with(['state']);
        $state = State::orderBy('name','asc')->get();
        if(@$request->all()){
            $pp = $city;
            $pp = $pp->count();
            
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

            if(@$request->columns['1']['search']['value']){
				$city = $city->where(function($query) use($request){
                    $query = $query->where('name','LIKE',"%".@$request->columns['1']['search']['value']."%");
                });
            }
            if(@$request->columns['2']['search']['value']){
                $city = $city->where(function($query) use($request){
                    $query = $query->where('state_id',@$request->columns['2']['search']['value']);
                });
            }
            $ppp = $city;
            $ppp = $ppp->count();
            
            //sorting
            if($columnIndex == 1) {
                $city1 = $city->get();
                if($columnSortOrder == 'asc') {
                    $city2 = $city1->sortBy(function($city, $key) {
                        return $city['state']['name'];
                    });
                    $city = $city2->slice($request->start, $request->length)->values()->all();
                }
                else
                {
                    $city2 = $city1->sortByDesc(function($city, $key) {
                        return $city['state']['name'];
                    });

                    $city = $city2->slice($request->start, $request->length)->values()->all();
                }
            }
            if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $city = $city->orderBy('name','asc');
                } else {
                    $city = $city->orderBy('name','desc');
                }

                $city = $city->skip($request->start)->take($request->length)->get();
            }
            $data['aaData'] = $city;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $pp;
            $data["iTotalDisplayRecords"] = $ppp;
            return response()->json($data);
        }else{
            $city = $city->orderBy('id','desc')->get();
            return view('admin.modules.city.list_city')->with([
                'city' =>  @$city,
                'state' =>  @$state
            ]);
        }

    }
    public function adminchangeStatusOfCity(Request $request){
        try{
            $id = @$request->data['id'];
            $city = City::where('id',$id)->whereNotIn('status',['D'])->first();
            if($city->status == 'A'){
                $ischeck = User::where(['city'=>$request->id])->whereIn('status',['I','A','U'])->first();
                if($ischeck){
                    $data['meaning'] = " City status has not been changed! ".@$city->name." is associated with customer!So you cannot inactive this city!";
                    $data['output'] = 0;
                    return response()->json($data,200);
                }
                $ischeckm = Merchant::where(['city'=>$request->id])->whereIn('status',['I','A','U'])->first();
                if($ischeckm){
                    $data['meaning'] = " City status has not been changed! ".@$city->name." is associated with merchant ! So you cannot inactive this city!";
                    $data['output'] = 0;
                    return response()->json($data,200);
                }
                $uarr = User::whereIn('status',['I','A','U'])->pluck('id')->toArray();
                $ischeckingv = UserAddressBook::where(['city'=>$request->id])->whereIn('user_id',$uarr)->first();
                if($ischeckingv){
                    $data['meaning'] = " City status has not been changed! ".@$city->name." is associated with customer!So you cannot inactive this city!";
                    $data['output'] = 0;
                    return response()->json($data,200);
                }
                City::where('id',$id)->whereNotIn('status',['D'])->update(['status'=>'I']);
                $data['meaning'] = @$city->name." is inactivated!";
                $data['output'] = 1;
                return response()->json($data,200);
            }
            if($city->status == 'I'){
                City::where('id',$id)->whereNotIn('status',['D'])->update(['status'=>'A']);    
                $data['meaning'] = @$city->name." is activated!";
                $data['output'] = 1;
                return response()->json($data,200);
            }
            $data['meaning'] ="";
            return response()->json($data,200);
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            $data['meaning'] =$e->getMessage();
            return response()->json($data,200);
        }
        // if(@$request->all()){
        //     $cities =$this->cities->where('id',@$request->id)->first();
        //     if(@$cities){
        //         $city_details =$this->city_details->where('city_id',@$request->id)->get();
        //         if(@$city_details){
        //             $status = $cities->status;
        //             if($status == 'A'){
        //                 $ischeck = User::where(['city'=>$request->id])->whereIn('status',['I','A','U'])->first();
        //                 if($ischeck){
        //                     return 0;
        //                 }
        //                 $ischeckm = Merchant::where(['city'=>$request->id])->whereIn('status',['I','A','U'])->first();
        //                 if($ischeckm){
        //                     return 0;
        //                 }
        //                 $uarr = User::whereIn('status',['I','A','U'])->pluck('id')->toArray();
        //                 $ischeckingv = UserAddressBook::where(['city'=>$request->id])->whereIn('user_id',$uarr)->first();
        //                 if($ischeckingv){
        //                     return 0;
        //                 }
        //                 $update['status'] = 'I';
        //                 $this->cities->where('id',@$request->id)->update($update);
        //                 $this->city_details->where('city_id',@$request->id)->update($update);
        //                 return 1;
        //             }else if($status == 'I'){
        //                 $update['status'] = 'A';
        //                 $this->cities->where('id',@$request->id)->update($update);
        //                 $this->city_details->where('city_id',@$request->id)->update($update);
        //                 return 1;
        //             }else{
        //                 return 0;
        //             }
        //         }else{
        //             return 1;
        //         }    
        //     }else{
        //         return 1;
        //     }
        // }else{
        //     return 1;
        // }
    }
}