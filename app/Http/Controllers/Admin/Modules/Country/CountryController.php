<?php

namespace App\Http\Controllers\Admin\Modules\Country;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use Validator;
use Auth;
use App\Models\Language;
use App\Models\Admin;
use App\Models\CountryDetails;
use App\Models\UserAddressBook;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Repositories\LanguageRepository;
use App\Repositories\CountryRepository;
use App\Repositories\CountryDetailsRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\OrderMasterRepository;

use Illuminate\Support\Facades\Hash;

class CountryController extends Controller
{
	protected $cities,$language,$countries;
    public function __construct(LanguageRepository $language,
        MerchantRepository $merchants,
        OrderMasterRepository $order_master,
        CountryRepository $countries,
        CountryDetailsRepository $country_details,
        Request $request
    )
    {
        $this->middleware('admin.auth:admin');
        $this->checkLoginAccess();
        // $this->cities          =   $cities;
        $this->language        =   $language;
        $this->countries       =   $countries;
        $this->country_details =   $country_details;
        $this->merchants       =   $merchants;
        $this->order_master       =   $order_master;
        // dd(auth::guard('admin')->user()->type);
    }
    /*
    * Method        : addCity
    * Description   : This method is used to add City
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function addCountry(Request $request){
        $this->checkLoginAccess();
        $this->checkMenuAccess();
        $language = $this->language->get();
    
        if(@$request->all()){
            $update['country_name']    = " ";
            $country = $this->countries->create($update);        
            foreach(@$request->name as $key=>$val){
                $new['name'] = $val;
                $new['language_id'] = $language[$key]->id;
                $new['country_id'] = $country->id;
                $new['country_code'] = $request->country_code;
                $country_detail = $this->country_details->create($new);
            }
            if(@$country){
                session()->flash("success",__('success.-4061'));
            }else{
                session()->flash("error",__('errors.-5059'));
            }
            return redirect()->route('admin.list.country');
        }else{
            return view('admin.modules.country.add_country')->with([
                'language'=>@$language
            ]);
        }
    }


    /*
    * Method        : editCountry
    * Description   : This method is used to add customer
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */

    public function editCountry(Request $request,$id){
        $this->checkLoginAccess();        
        $language = $this->language->get();    
        $country = $this->countries->with('countryDetails')->where('id',@$id)->first();
        if(@$country){
            return view('admin.modules.country.edit_country')->with(['country'=>@$country,'language'=>@$language]);
        }else{
            return redirect()->back();
        }
    }


    /*
    * Method        : updateCountry
    * Description   : This method is used to update city
    * Author        : Jayatri
    * Date          : 15/01/2020
    */

    public function updateCountry(Request $request,$id){
    	// dd($id);
        $this->checkLoginAccess();
        $language = $this->language->get();    
        $country = $this->countries->where('id',@$id)->first();
        if(@$country){
            foreach(@$request->name as $key=>$val){
                $new['name'] = $val;
                $new['country_code'] = $request->country_code;
                $country_detail = $this->country_details->where(['country_id'=>@$id,'language_id'=>@$language[$key]->id])->update($new);
            }
            session()->flash("success",__('success.-4062'));
            return redirect()->route('admin.list.country');
        }else{
            return redirect()->back();
        }
    }
    /*
    * Method        : manageCity
    * Description   : This method is used to manage city
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function manageCountry(Request $request){
        $this->checkLoginAccess();
        $this->checkMenuAccess();
        // dd("kjfdkd");
        $country = $this->countries->with('countryDetailsBylanguage')->where('status','!=','D');

        // dd($city->get());
        if(@$request->all()){
            $country = $country->orderBy('id','desc')->get();
            return view('admin.modules.country.list_country')->with([
                'country' =>  @$country
            ]);
        }else{
            $country = $country->orderBy('id','desc')->get();
            // dd($country);
            // dd($city);
            return view('admin.modules.country.list_country')->with([
                'country' =>  @$country
            ]);
        }

    }
    /*
    * Method        : checkDuplicateCityName
    * Description   : This method is used to checkDuplicateCityName
    * Author        : Jayatri
    * Date 			: 05.02.2020
    */
    public function checkDuplicateCountryName(Request $request){
        $this->checkLoginAccess();
        $country = $this->countries->where('name',@$request->name)->first();
        if(@$country){
         return 0;
     }else{
         return 1;
     }
 }

    /*
    * Method        : deleteCountry
    * Description   : This method is used to delete country
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function deleteCountry(Request $request,$id){
        $this->checkLoginAccess();
        $this->checkMenuAccess();
        $country = $this->countries->where('id',@$id)->first();
        $flag = 0;
        if(@$country){
            $merchant = $this->merchants->where('country',$id)->count();
            $order = $this->order_master->where('shipping_country',$id)->count();
            $orderB = $this->order_master->where('billing_country',$id)->count();
            $addb = UserAddressBook::where('country',$id)->count();
            $usr = User::where('country',$id)->count();
            // $orderB = $this->order_master->where('billing_country',$id)->count();
            // $orderB = $this->order_master->where('billing_country',$id)->count();
            if(@$merchant>0){
                $flag = 1;
            }elseif($order > 0){
                $flag = 1;
            }elseif($orderB > 0){
                $flag = 1;
            }elseif($addb > 0){
                $flag = 1;
            }elseif($usr > 0){
                $flag = 1;
            }
            if($flag == 0){
                $update['status'] = 'D';
                $this->countries->where('id',@$id)->update($update);
                session()->flash("success",__('success.-4063'));
            }else{
               session()->flash("error",__('errors.-5061'));
            }
        	
          
      }else{
          session()->flash("error",__('errors.-5061'));
      }
      return redirect()->back();
    }
    /*
    * Method        : showHideCountry
    * Description   : This method is used to show Hide Country
    * Author        : Jayatri
    * Date          : 12/04/2020
    */
    public function showHideCountry(Request $request){
        if(@$request->all()){
            $country =$this->countries->where('id',@$request->id)->first();
            if(@$country){
                $country_details =$this->country_details->where('country_id',@$request->id)->get();
                if(@$country_details){
                    $status = $country->status;
                    if($status == 'A'){
                        $update['status'] = 'I';
                        $this->countries->where('id',@$request->id)->update($update);
                        $this->country_details->where('country_id',@$request->id)->update($update);
                        return 1;
                    }else if($status == 'I'){
                        $update['status'] = 'A';
                        $this->countries->where('id',@$request->id)->update($update);
                        $this->country_details->where('country_id',@$request->id)->update($update);
                        return 1;
                    }else{
                        return 0;
                    }
                }else{
                    return 1;
                }    
            }else{
                return 1;
            }
        }else{
            return 1;
        }
    }
    /*
    * Method        : cityDelete
    * Description   : This method is used to delete city by ajax
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    public function countryDelete(Request $request){
        // dd("nn");/
        // $this->checkLoginAccess();
        // $this->checkMenuAccess();
        $id = @$request->id;
        $flag = 0;
        $country = $this->countries->where('id',@$id)->first();
        if(@$country){   
            $merchant = $this->merchants->where('country',$id)->count();
            if(@$merchant>0){
                $flag = 1;
            }
            if($flag == 0){
                $update['status'] = 'D';
                $this->countries->where('id',@$id)->update($update);
                return 1;
            }else{
               return 0;
            }
        	
          // session()->flash("success",__('success.-4051'));
          
      }else{
          // session()->flash("error",__('errors.-5047'));
        return 0;
      }
      // return redirect()->back();
    } 
    /*
    * Method        : checkLoginAccess
    * Description   : This method is used to check Login Access
    * Author        : Jayatri
    * Date          : 15/01/2020
    */
    private function checkLoginAccess(){
        if(!@Auth::guard('admin')->user()){
            return redirect()->route('admin.login');
        }
    }
    private function checkMenuAccess(){
    	if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array();
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id);
                    }

                }
                //dd($permission);
                if(in_array(3, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });
        }
    }
}
