<?php

namespace App\Http\Controllers\Admin\Modules\Coupon;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Coupon;
use App\Models\CouponToMerchant;
use App\Merchant;
use Validator;
use Auth;
use App\Models\Admin;
use App\User;
class CouponController extends Controller
{
    public function __construct(
        Request $request
    )
    {
        $this->middleware('admin.auth:admin');
        
    }
    /*
    * Method        : manageCoupon
    * Description   : This method is used show manage coupon page
    * Author        : Argha
    * Date 			: 2020-09-03
    */

    public function manageCoupon(Request $request)
    {
        if($request->all()){
            $coupons = Coupon::with(['getMerchants.merchantInfo']);
            $dd = $coupons;
            $dd = $dd->count();
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

            if(@$request->columns['1']['search']['value']){
				$coupons = $coupons->where(function($query) use($request){
                    $query->Where('code','like','%'.@$request->columns['1']['search']['value'].'%');
                });
			}

            if(@$request->columns['2']['search']['value']){
				$coupons = $coupons->where(function($query) use($request){
                    $query->Where('applied_for',@$request->columns['2']['search']['value']);
                });
            }
            
            if(@$request->columns['3']['search']['value']){
				$coupons = $coupons->where(function($query) use($request){
                    $query->Where('applied_on',@$request->columns['3']['search']['value']);
                });
			}
             //sorting
             $ddd = $coupons;
             $ddd = $ddd->count();
             if($columnIndex == 0) {
                if($columnSortOrder == 'asc') {
                    $coupons = $coupons->orderBy('code','asc');
                } else {
                    $coupons = $coupons->orderBy('code','desc');
                }
            }
            if($columnIndex == 1) {
                if($columnSortOrder == 'asc') {
                    $coupons = $coupons->orderBy('start_date','asc');
                } else {
                    $coupons = $coupons->orderBy('start_date','desc');
                }
            }
            if($columnIndex == 2) {
                if($columnSortOrder == 'asc') {
                    $coupons = $coupons->orderBy('end_date','asc');
                } else {
                    $coupons = $coupons->orderBy('end_date','desc');
                }
            }
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $coupons = $coupons->orderBy('no_of_times','asc');
                } else {
                    $coupons = $coupons->orderBy('no_of_times','desc');
                }
            }
            if($columnIndex == 4) {
                if($columnSortOrder == 'asc') {
                    $coupons = $coupons->orderBy('used_times','asc');
                } else {
                    $coupons = $coupons->orderBy('used_times','desc');
                }
            }
            if($columnIndex == 5) {
                if($columnSortOrder == 'asc') {
                    $coupons = $coupons->orderBy('discount','asc');
                } else {
                    $coupons = $coupons->orderBy('discount','desc');
                }
            }
            if($columnIndex == 6) {
                if($columnSortOrder == 'asc') {
                    $coupons = $coupons->orderBy('discount_type','asc');
                } else {
                    $coupons = $coupons->orderBy('discount_type','desc');
                }
            }
            if($columnIndex == 7) {
                if($columnSortOrder == 'asc') {
                    $coupons = $coupons->orderBy('applied_on','asc');
                } else {
                    $coupons = $coupons->orderBy('applied_on','desc');
                }
            }
            if($columnIndex == 8) {
                if($columnSortOrder == 'asc') {
                    $coupons = $coupons->orderBy('applied_for','asc');
                } else {
                    $coupons = $coupons->orderBy('applied_for','desc');
                }
            }
            if($columnIndex == 9) {
                if($columnSortOrder == 'asc') {
                    $coupons = $coupons->orderBy('coupon_bearer','asc');
                } else {
                    $coupons = $coupons->orderBy('coupon_bearer','desc');
                }
            }
            
            $coupons = $coupons->skip($request->start)->take($request->length)->get();
            $data['aaData'] = $coupons;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $dd;
            $data["iTotalDisplayRecords"] = $ddd;
            return response()->json($data);
        }else{
            return view('admin.modules.coupon.manage_coupon');
        }
        
    }

    /*
    * Method        : manageCoupon
    * Description   : This method is used show manage coupon page
    * Author        : Argha
    * Date 			: 2020-09-03
    */
    public function addCoupon()
    {
        $data['merchants'] = Merchant::where('status','A')->get();
        return view('admin.modules.coupon.add_coupon')->with($data);
    }

    /*
    * Method        : insertCoupon
    * Description   : This method is used to insert coupon details
    * Author        : Argha
    * Date 			: 2020-09-04
    */

    public function insertCoupon(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'code'                         =>'required',
            'start_date'                   =>'required',
            'end_date'                     =>'required',
            'no_of_times'                  =>'required',
            'discount_type'                =>'required',
            'discount'                     =>'required',
            'applied_on'                   =>'required',
            //'coupon_bearer'                =>'required',
        ]);
        
        $insCoupon['code'] = $request->code;
        $insCoupon['start_date'] = date('Y-m-d H:i:s',strtotime($request->start_date));
        $insCoupon['end_date'] = date('Y-m-d H:i:s',strtotime($request->end_date));
        $insCoupon['no_of_times'] = $request->no_of_times;
        $insCoupon['discount'] = $request->discount;
        $insCoupon['discount_type'] = $request->discount_type;
        $insCoupon['applied_for'] = @$request->applied_for;
        $insCoupon['applied_on'] = @$request->applied_on;
        $insCoupon['coupon_bearer'] = @$request->coupon_bearer;
        
        $coupon = Coupon::create($insCoupon);
        if($request->applied_for == 'M'){
            foreach ($request->merchants as $mer_id) {
                $insapplied['coupon_id'] = $coupon->id;
                $insapplied['merchant_id'] = $mer_id;

                CouponToMerchant::create($insapplied);
            }
        }

        if(@$coupon){
            session()->flash("success",__('success.-4090'));
        }else{
            session()->flash("error",__('errors.-5080'));
        }
        return redirect()->route('admin.manage.coupon');
    }

    /*
    * Method        : editCoupon
    * Description   : This method is used to edit coupon details
    * Author        : Argha
    * Date 			: 2020-09-04
    */

    public function editCoupon($id)
    {
        $data['merchants'] = Merchant::where('status','A')->get();
        $data['coupon']    = Coupon::with(['getMerchants'])->where('id',$id)->first();
        return view('admin.modules.coupon.edit_coupon')->with($data);
    }

    /*
    * Method        : editCoupon
    * Description   : This method is used to edit coupon details
    * Author        : Argha
    * Date 			: 2020-09-04
    */

    public function updateCoupon(Request $request)
    {
        $request->validate([
            'code'                         =>'required',
            'start_date'                   =>'required',
            'end_date'                     =>'required',
            'no_of_times'                  =>'required',
            'discount_type'                =>'required',
            'discount'                     =>'required',
            'applied_on'                   =>'required',
            //'coupon_bearer'                =>'required',
        ]);
        
        $upCoupon['code'] = $request->code;
        $upCoupon['start_date'] = date('Y-m-d H:i:s',strtotime($request->start_date));
        $upCoupon['end_date'] = date('Y-m-d H:i:s',strtotime($request->end_date));
        $upCoupon['no_of_times'] = $request->no_of_times;
        $upCoupon['discount'] = $request->discount;
        $upCoupon['discount_type'] = $request->discount_type;
        $upCoupon['applied_for'] = @$request->applied_for;
        $upCoupon['applied_on'] = @$request->applied_on;
        $upCoupon['coupon_bearer'] = @$request->coupon_bearer;

        $coupon = Coupon::where('id',$request->id)->update($upCoupon);
        if($request->applied_for == 'M'){
            $checkcoupon = CouponToMerchant::where('coupon_id',$request->id)->first();
            if($checkcoupon != null){
                CouponToMerchant::where('coupon_id',$request->id)->delete();
            }
            foreach ($request->merchants as $mer_id) {
                $insapplied['coupon_id'] = $request->id;
                $insapplied['merchant_id'] = $mer_id;

                CouponToMerchant::create($insapplied);
            }
        }

        if(@$coupon){
            session()->flash("success",__('success.-4091'));
        }else{
            session()->flash("error",__('errors.-5081'));
        }
        return redirect()->route('admin.manage.coupon');
    }

    /*
    * Method        : deleteCoupon
    * Description   : This method is used to delete coupon
    * Author        : Argha
    * Date 			: 2020-09-04
    */
    public function deleteCoupon(Request $request) {
        $id = $request['data']['id'];
        $deleteCoupon = Coupon::where('id',$id)->delete();
        CouponToMerchant::where('coupon_id',$id)->delete();
        if($deleteCoupon){
            return response()->json(1);
        }else{
            return response()->json(0);
        }
    }

    /*
    * Method        : checkCouponCode
    * Description   : This method is used to check duplicate coupon code
    * Author        : Argha
    * Date 			: 2020-10-21
    */
    public function checkCouponCode(Request $request)
    {
        $code = $request['data']['coupon_code'];
        $id   = @$request['data']['id'];
        $response['jsonrpc'] = '2.0'; 
        if($id){
           $checkCoupon =  Coupon::where('code',$code)->where('id','!=',$id)->first();
           if($checkCoupon != null){
                $response['status'] = 'false';
           }else{
                $response['status'] = 'true';
           }
        }else{
            $checkCoupon =  Coupon::where('code',$code)->first();
           if($checkCoupon != null){
                $response['status'] = 'false';
           }else{
                $response['status'] = 'true';
           }
        }

        return response()->json($response);
    }
}
