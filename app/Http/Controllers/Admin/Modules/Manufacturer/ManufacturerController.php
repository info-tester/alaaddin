<?php

namespace App\Http\Controllers\Admin\Modules\Manufacturer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryDetail;
use App\Models\Language;
use App\Models\Admin;
use App\Models\Brand;
use App\Models\BrandDetail;
use App\Models\Product;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class ManufacturerController extends Controller
{
    public function __construct()
    {
        $this->checkLoginAccess();
        $this->middleware('admin.auth:admin');
    }
    
    /*
    * Method        : index
    * Description   : This method is used to show list of product manufacturer from brand & brand details.
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function index(Request $request){
        $this->checkLoginAccess();
        $this->checkAccess();
    	$category = Category::with('categoryDetailsByLanguage')->where(['parent_id'=>0])->where('status', '!=', 'D')->get();
    	$brands = Brand::with(['brandDetailsByLanguage', 'categoryByBrand'])->where('category_id','!=',0)->where('status', '!=', 'D');
        $bb = $brands;
        $bb = $bb->count();
    	if($request->all()){
            $columnIndex = $request->order[0]['column']; // Column index
            $columnName = $request->columns[$columnIndex]['data']; // Column name
            $columnSortOrder = $request->order[0]['dir']; // asc or desc

    		if(@$request->columns['1']['search']['value']){
                $brands = $brands->where(function($q1) use($request) {
                	$q1 = $q1->where('category_id', @$request->columns['1']['search']['value']);
                });
            }

            $bbb = $brands;
            $bbb = $bbb->count();

            // sorting
            if($columnIndex == 3) {
                if($columnSortOrder == 'asc') {
                    $brands = $brands->orderBy('status','asc');
                } else {
                    $brands = $brands->orderBy('status','desc');
                }
                $brands = $brands->skip($request->start)->take($request->length)->get();
            }
            if($columnIndex == 1) {
                $brands1 = $brands->get();
                if($columnSortOrder == 'asc') {
                    $brands2 = $brands1->sortBy(function($brands, $key) {
                        return $brands['categoryByBrand']['title'];
                    });
                    $brands = $brands2->slice($request->start, $request->length)->values()->all();
                } else {
                    $brands2 = $brands1->sortByDesc(function($brands, $key) {
                        return $brands['categoryByBrand']['title'];
                    });
                    $brands = $brands2->slice($request->start, $request->length)->values()->all();
                }
            }

            if($columnIndex == 2) {
                $brands1 = $brands->get();
                if($columnSortOrder == 'asc') {
                    $brands2 = $brands1->sortBy(function($brands, $key) {
                        return $brands['brandDetailsByLanguage']['title'];
                    });
                    $brands = $brands2->slice($request->start, $request->length)->values()->all();
                } else {
                    $brands2 = $brands1->sortByDesc(function($brands, $key) {
                        return $brands['brandDetailsByLanguage']['title'];
                    });
                    $brands = $brands2->slice($request->start, $request->length)->values()->all();
                }
            }
            $data['aaData'] = $brands;
            $data["draw"] = intval($request->draw);
            $data['iTotalRecords'] = $bb;
            $data["iTotalDisplayRecords"] = @$bbb;
            return response()->json($data);
    	} else {
            $brands = $brands->orderBy('id','desc')->get();
            return view('admin.modules.product_settings.manufacturer.list_manufacturer')->with([
                'category'=>@$category,
                'brands'=>@$brands
            ]);
        }
    }
    
    /*
    * Method        : addManufacturer
    * Description   : This method is used to add product manufacturer 
    * Author        : Jayatri
    * Date 			: 15/01/2020
    */
    public function addManufacturer(Request $request){
        $this->checkLoginAccess();
        $this->checkAccess();
    	$category = Category::with('categoryDetailsByLanguage')->where(['parent_id'=>0])->where('status', '!=', 'D')->get();
    	$language = Language::where('status','A')->get();
    	if(@$request->all()){
    		$request->validate([
                'category'=>'required',
                'brand_logo'=>'image|mimes:jpeg,jpg,png',
            ],[
                'category.required'=>'Category field is required',
                'brand_logo.image'=>'Please upload  brand logo  with extension jpg/jpeg/png'
            ]);
            DB::transaction(function() use($request,$language,$category){
            $category_id = @$request->category;
            settype($category_id,"integer");
            $brand['category_id'] = $category_id;
            if(@$request->brand_logo){
                $pic   = $request->brand_logo;
                $name = time().'.'.$pic->getClientOriginalExtension();
                $pic->move('storage/app/public/brand_logo/', $name);
                $brand['logo'] = $name;
            }
            $chk_brand_name = Brand::where('name',$request->brand_name[0])->first();
            if(@$chk_brand_name){
            	$brand['name'] = $request->brand_name[0]."_".time();
            }else{
            	$brand['name'] = $request->brand_name[0];
            }
            $brands = Brand::create($brand);
            foreach(@$request->brand_name as $key=>$val){
            	$new['title'] = $val;
            	$new['language_id'] = $language[$key]->id;
            	$new['barnd_id'] = $brands->id;
            	$brandDetails = BrandDetail::create($new);
            }
            if(@$brands){
            	session()->flash("success",__('success.-4006'));
            	
            }else{
            	session()->flash("error",__('errors.-5007'));
            	// return redirect()->route('admin.list.manufacturer');
            }
            });
            return redirect()->route('admin.list.manufacturer');
    	}else{
    		return view('admin.modules.product_settings.manufacturer.add_manufacturer')->with([
    		'category'=>@$category,
    		'language'=>@$language,
    		]);
    	}
    }
    /*
    * Method        : editManufacturer
    * Description   : This method is used to edit product category.
    * Author        : Jayatri
    * Date 			: 19/01/2020
    */
    public function editManufacturer(Request $request,$id){
        $this->checkLoginAccess();
        $this->checkAccess();
    	$category = Category::with('categoryDetailsByLanguage')->where(['parent_id'=>0])->where('status', '!=', 'D')->get();
    	$language = Language::where('status','A')->get();
    	$brand = Brand::with('brandDetails')->where('id',$id)->first();
    	
    	if(@$brand){
    		$brandDetails = BrandDetail::where('barnd_id',$brand->id)->get();
    		if(@$brandDetails){
	    		if($request->all()){

	    			$request->validate([
		                'brand_logo'=>'image|mimes:jpeg,jpg,png',
		            ],[
		                'brand_logo.image'=>'Please upload  brand logo  with extension jpg/jpeg/png'
		            ]);
                    DB::transaction(function() use($request,$language,$category,$brand,$id){
    		    		// if(@$request->category){
    		    		// 	$updateManufacturer['category_id'] = @$request->category;
    		    		// }
    		    		if(@$request->brand_logo){
    		                $pic   = $request->brand_logo;
    		                $name = time().'.'.$pic->getClientOriginalExtension();
    		                $pic->move('storage/app/public/brand_logo/', $name);
    		                $updateManufacturer['logo'] = $name;
                            Brand::where('id',$id)->update($updateManufacturer);
    		            }
    	            	
    	            	if(@$request->brand_name){
    	            		foreach(@$request->brand_name as $key=>$val){
    			            	$update['title'] = $val;
    		            		BrandDetail::where(['barnd_id'=>$brand->id,'language_id'=>$language[$key]->id])->update($update);
                			}
    	            	}
    	            	session()->flash("success",__('success.-4007'));
                    });
	            	return redirect()->route('admin.list.manufacturer');
	    		}else{

	    			//view page without request
	    			return view('admin.modules.product_settings.manufacturer.edit_manufacturer')->with([
		    		'category'=>@$category,
		    		'language'=>@$language,
		    		'brand'=>@$brand
		    		]);
	    		}
	    	}else{
	    		session()->flash("error",__('errors.-5007'));
        		return redirect()->route('admin.list.manufacturer');
	    	}
    	}else{
    		//unauthorized access
    		session()->flash("error",__('errors.-5007'));
        	return redirect()->route('admin.list.manufacturer');

    	}
    }
    /*
    * Method        : checkManufacturerName
    * Description   : This method is used to check Manufacturer Name.
    * Author        : Jayatri
    * Date          : 19/01/2020
    */
    public function checkManufacturerName(Request $request){
            $brand = Brand::with(['brandDetailsByLanguage'])->where(['category_id'=>@$request->caegory_id]);
            
            $brand = $brand->whereHas('brandDetailsByLanguage',function($q1) use($request) {
                $q1 = $q1->where('title', @$request->name);
            });

            $brand = $brand->first();    

            if(@$brand){
                return 0;
            }else{
                return 1;
            }
    }
    /*
    * Method        : deleteManufacturer
    * Description   : This method is used to delete manufacturer.
    * Author        : Jayatri
    * Date 			: 19/01/2020
    */
    public function deleteManufacturer($id){
        $this->checkLoginAccess();
        $this->checkAccess();
    	$brand = Brand::where('id',$id)->first();
        $flag = 0;
    	if(@$brand){
			$brandDetails = BrandDetail::where('barnd_id',$brand->id)->get();
			if(@$brandDetails){
                $product = Product::where('brand_id',$brand->id)->where('status', '!=', 'D')->first();
                if(@$product){
                    $flag = 1;
                }
                if($flag == 0){
                    DB::transaction(function() use($id,$brand){
                        if(@$brand->logo){
                            $image_path = '/storage/app/public/brand_logo/'.$brand->logo; 
                            if (file_exists(@$image_path)){
                                unlink(@$image_path);
                            }
                        }
                        BrandDetail::where('barnd_id',$brand->id)->delete();
                        Brand::where('id',$id)->delete();
                    });
                    session()->flash("success",__('success.-4008'));
                    return redirect()->route('admin.list.manufacturer');
                }else{
                    //if product is associated with this brand/manufacturer

                    session()->flash("error",__('errors.-5008'));
                    return redirect()->route('admin.list.manufacturer');
                }
			}else{
				session()->flash("error",__('errors.-5008'));
        		return redirect()->route('admin.list.manufacturer');
			}
    	}else{
    		session()->flash("error",__('errors.-5008'));
        	return redirect()->route('admin.list.manufacturer');
    	}
    	return redirect()->back();
    }

    /*
    * Method        : deleteManufacturerDetails
    * Description   : This method is used to delete manufacturer details by ajax.
    * Author        : Jayatri
    * Date          : 19/01/2020
    */
    public function deleteManufacturerDetails(Request $request){
        // $this->checkLoginAccess();
        // $this->checkAccess();
        $id = @$request->id;
        $brand = Brand::where('id',$id)->first();
        $flag = 0;
        if(@$brand){
            $brandDetails = BrandDetail::where('barnd_id',$brand->id)->get();
            if(@$brandDetails){
                $product = Product::where('brand_id',$brand->id)->where('status', '!=', 'D')->first();
                if(@$product){
                    $flag = 1;
                }
                if($flag == 0){
                    DB::transaction(function() use($id,$brand){
                        if(@$brand->logo){
                            $image_path = '/storage/app/public/brand_logo/'.$brand->logo; 
                            if (file_exists(@$image_path)){
                                unlink(@$image_path);
                            }
                        }
                        // BrandDetail::where('barnd_id',$brand->id)->delete();
                        Brand::where('id',$id)->update(['status' => 'D']);
                    });
                    $data['result'] = 1;
                    return $data;
                }else{
                    //if product is associated with manufacturer
                    $data['product_count'] = Product::where('brand_id',$brand->id)->where('status', '!=', 'D')->count();
                    $data['result'] = 0;
                    return $data;
                }
            }else{
                //error for no brand details
                $data['product_count'] = 0;
                $data['result'] = 0;
                return $data;
            }
        }else{
            //error for no brand
            $data['product_count'] = 0;
            $data['result'] = 0;
            return $data;
        }
    }
    private function checkAccess(){
        if(Auth::guard('admin')->user()->type == "A"){

        }else{
            $this->middleware('admin.auth:admin');
            $this->middleware(function ($request, $next) {
                $this->admin = Admin::with('sidebarAccess.menuDetails')
                ->where('id', auth::guard('admin')->user()->id)
                ->first();
                $permission = array(); 
                if($this->admin->sidebarAccess !=null)
                {
                    foreach($this->admin->sidebarAccess as $row)
                    {
                        array_push($permission, @$row->menu_id); 
                    }
                    
                }  
                //dd($permission);
                if(in_array(2, $permission))
                {
                    return $next($request);
                }
                else
                {
                    return redirect()->back();
                }
            });  
        }
    }
    private function checkLoginAccess(){
        if(!@Auth::guard('admin')->user()){
            return redirect()->route('admin.login');
        }
    }
}
