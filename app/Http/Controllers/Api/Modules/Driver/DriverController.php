<?php

namespace App\Http\Controllers\Api\Modules\Driver;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Merchant;
use App\Driver;
use App\Models\UserRegId;
use App\Models\OrderSeller;
use App\Models\OrderDetail;
use App\Models\OrderMaster;
use App\Models\Notification;
use App\Models\UserReward;
use App\Models\Language;
use App\Models\Setting;
use App\Models\OrderStatusTiming;

use App\Models\CountryDetails;

use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\ShippingCostRepository;
// use App\Repositories\OrderMasterRepository;
// use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\UserRepository;
use App\Repositories\DriverRepository;
use App\Repositories\OrderImageRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductRepository;
use App\Models\CitiDetails;
use App\Repositories\CityRepository;
use App\Repositories\CitiDetailsRepository;
use App\Repositories\SettingRepository;
use App\Repositories\CountryDetailsRepository;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;
use App\Mail\SendMailDriverNotifydetails;//notification for external order (driver assigned)
use App\Mail\SendOrderAceptedMail;//notification for internal order (driver assigned)
use App\Mail\SendMailExternalOrderdetails;
use App\Mail\SendOrderDetailsMerchantMail;
use App\Mail\OrderMail;
// use Illuminate\Support\Facades\Hash;
use Mail;
use JWTFactory;
use JWTAuth;
use Auth;

# use whatsapp controller
use App\Http\Controllers\Modules\WhatsApp\WhatsAppController;

class DriverController extends Controller
{
    protected $order_master,$order_details;
    #whatsapp send message
    protected $whatsApp;
    public function __construct( OrderMasterRepository $order_master,
        OrderDetailRepository $order_details,
        MerchantRepository $merchants,
        UserRepository $customers,
        DriverRepository $drivers,
        OrderSellerRepository $order_sellers,
        CityRepository $city,
        CitiDetailsRepository $city_details,
        ShippingCostRepository $shipping_cost,
        OrderImageRepository $order_image,
        ProductVariantRepository $product_variant,
        ProductRepository $product,
        SettingRepository $settings,
        WhatsAppController $whatsApp,
        Request $request
   )
    {
        $this->order_master             =   $order_master;
        $this->order_details            =   $order_details;
        $this->order_sellers            =   $order_sellers;
        $this->drivers                  =   $drivers;
        $this->merchants                =   $merchants;
        $this->customers                =   $customers;
        $this->city                     =   $city;
        $this->city_details             =   $city_details;
        $this->shipping_cost            =   $shipping_cost;
        $this->order_image              =   $order_image;
        $this->product_variant          =   $product_variant;
        $this->product                  =   $product;
        $this->settings                 =   $settings;
        $this->whatsApp                 =   $whatsApp;

        # set guard for driver guard
        \Config::set('auth.defaults.guard', 'driver');
    }

    /**
    *@ Method: dashboard
    *@ Description: driver dashboard
    *@ Author: Surajit/Jayatri(changes)
    */
    public function dashboard(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        // try {
            $user = JWTAuth::toUser();
            if($user->status == 'A'){
                $driverAssignedOrders1 = $driverAssignedOrders = OrderSeller::with([
                    'orderMasterTab',
                    'orderMasterTab.getCityNameByLanguage:id,city_id,name,language_id,created_at,updated_at'
                ])
                ->select('id','seller_id','order_master_id','subtotal','shipping_price',\DB::raw('(order_total - shipping_price) order_total'));

                $driverAssignedOrders = $driverAssignedOrders->whereHas('orderMasterTab', function($q){
                    $q->where('is_final_driver', 'N');
                });
                $driverAssignedOrders = $driverAssignedOrders->whereIn('status', ['RP','DA']);
                $driverAssignedOrders = $driverAssignedOrders->where('driver_id', $user->id);
                $driverAssignedOrders = $driverAssignedOrders->count();


                $driverAssignedOrders1 = OrderSeller::with([
                    'orderMasterTab',
                    'orderMasterTab.getCityNameByLanguage:id,city_id,name,language_id,created_at,updated_at',
                    'orderDetails:id,order_master_id,seller_id,driver_id,status,updated_at'])
                ->select('id','seller_id','order_master_id','subtotal','shipping_price',\DB::raw('(order_total - shipping_price) order_total'));
                
                $driverAssignedOrders1 = $driverAssignedOrders1->whereHas('orderMasterTab', function($q){
                    $q->where('is_final_driver', 'Y');
                });
                $driverAssignedOrders1 = $driverAssignedOrders1->whereIn('status', ['RP','DA']);
                $driverAssignedOrders1 = $driverAssignedOrders1->where('driver_id', $user->id);
                $driverAssignedOrders1 = $driverAssignedOrders1->groupBy('order_master_id');
                $driverAssignedOrders1 = $driverAssignedOrders1->get();
                $driverAssignedOrders1 = $driverAssignedOrders1->count();

                $deliveredOrders1 = $deliveredOrders = OrderSeller::with(['orderMasterTab',
                    'orderMasterTab.getCityNameByLanguage:id,city_id,name,language_id,created_at,updated_at',
                    'orderDetails:id,order_master_id,seller_id,driver_id,status,updated_at'
                ])
                ->select('id','seller_id','order_master_id','subtotal','shipping_price',\DB::raw('(order_total - shipping_price) order_total'));
                

                # delivered order count
                $deliveredOrders = $deliveredOrders->whereHas('orderMasterTab', function($q){
                    $q->whereNotIn('status', ['','D','I','OC','F']);
                    $q->where('is_final_driver', 'N');
                });
                $deliveredOrders = $deliveredOrders->where('driver_id', $user->id);
                $deliveredOrders1 = $deliveredOrders1->where('status', 'OD');
                $deliveredOrders = $deliveredOrders->count();

                # count order which assigned for final driver.
                $deliveredOrders1 = OrderSeller::with([
                    'orderMasterTab',
                    'orderMasterTab.getCityNameByLanguage:id,city_id,name,language_id,created_at,updated_at',
                    'orderDetails:id,order_master_id,seller_id,driver_id,status,updated_at'
                ])
                ->select('id','seller_id','order_master_id','subtotal','shipping_price',\DB::raw('(order_total - shipping_price) order_total'));
                $deliveredOrders1 = $deliveredOrders1->whereHas('orderMasterTab', function($q){
                    $q->where('is_final_driver', 'Y');
                });
                $deliveredOrders1 = $deliveredOrders1->where('status', 'OD');
                $deliveredOrders1 = $deliveredOrders1->where('driver_id', $user->id);
                $deliveredOrders1 = $deliveredOrders1->groupBy('order_master_id');
                $deliveredOrders1 = $deliveredOrders1->get();
                $deliveredOrders1 = $deliveredOrders1->count();
                
                $orders = OrderSeller::with([
                    'orderMasterTab',
                    'orderMasterTab.getPreferredTime',
                    'orderMasterTab.getCityNameByLanguage:id,city_id,name,language_id,created_at,updated_at',
                    'orderDetails:id,order_master_id,seller_id,driver_id,status,updated_at',
                    'orderMerchant:id,fname,lname,city,city_id,country',
                    'orderMerchant.merchantCityDetails'
                ])
                ->select('id','seller_id','order_master_id','subtotal','shipping_price',\DB::raw('(order_total - shipping_price) order_total'));
                
                /*$orders = $orders->whereHas('orderDetails', function($q) use ($user){
                    $q->whereIn('status', ['DA','RP']);
                });*/
                $orders = $orders->whereHas('orderMasterTab', function($q){
                    $q->whereNotIn('status', ['I','','OC','D','F']);
                });
                $orders = $orders->where('driver_id',$user->id);
                $orders = $orders->whereIn('status', ['DA','RP']);
                $orders = $orders->orderBy('id','desc')->take(2)->get();
                
                $response['result']['user'] = $user;
                $response['result']['total_driver_assigned'] = $driverAssignedOrders + $driverAssignedOrders1;
                $response['result']['total_delivered'] = $deliveredOrders + $deliveredOrders1;
                $response['result']['orders'] = $orders;
            }
            if($user->status == 'I'){
                $response['error'] = __('errors.-5068');
            }
            if($user->status == 'D'){
                $response['error'] = __('errors.-5067');
            }
            return response()->json($response);
       /* }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] = $th->getMessage();
            return response()->json($response);
        }*/
    }

    /**
    *@ Method: orderList
    *@ Description: driver order listing
    *@ Author: Surajit/Jayatri(changes)
    */
    public function orderList(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $status = $reqData['params']['order_status'];
        
        if(@$reqData['params']['from_date']){
            $from_date = $reqData['params']['from_date'];
        }
        if(@$reqData['params']['to_date']){
            $to_date = $reqData['params']['to_date'];    
        }
        if(@$reqData['params']['keyword']){
            $keyword = $reqData['params']['keyword'];    
        } else {
            $keyword = "";
        }
        if(@$reqData['params']['city_saerch']){
            $city_saerch = $reqData['params']['city_saerch'];    
        } else {
            $city_saerch = "";
        }
        try {
            #get user details by token 
            $user = JWTAuth::toUser();
            #if user is not deleted/inactive (at the time of order list seen by user) it should go to log
            if($user->status == 'A'){
                #merchant specific order list for driver app
                $orders = OrderSeller::with([
                    'orderImages',
                    'orderMasterTab',
                    'orderMasterTab.getPreferredTime',
                    'orderMasterTab.getCityNameByLanguage:id,city_id,name,language_id,created_at,updated_at',
                    'orderMasterTab.statusTiming',
                    'orderMasterTab.getpickupCountryDetails',
                    'orderMasterTab.getpickupCityDetails',
                    'orderMerchant:id,fname,lname,city,city_id,country',
                    'orderMerchant.merchantCityDetails',
                    'orderDetails'
                ])->select('id','seller_id', 'delay_in_minutes', 'driver_pickup_date', 'order_master_id','subtotal','shipping_price', 'updated_at', \DB::raw('(order_total - shipping_price) order_total'));
                
                $orders = $orders->whereHas('orderMasterTab', function($q){
                    $q->whereNotIn('status', ['','D','I','OC','F']);
                });
                
                # if new order list else others(DA,OP,OD)
                if($status == 'N'){
                    #Either driver id is 0 and drivers order(personally)
                    $orders = $orders->whereIn('driver_id', [0, $user->id]);
                    $orders = $orders->where('status', 'N');
                } else {
                    # status(DA/OP/OD)
                    $orders = $orders->where('driver_id', $user->id);
                }
                if($status == 'N' || $status == 'OD'){
                    $orders = $orders->where('status', $status);
                } else if($status == 'OP'){
                    $orders = $orders->where('status', 'OP');
                } else if($status == 'DA'){
                    $orders = $orders->whereIn('status',['RP','DA']);
                    $orders = $orders->where('driver_id',$user->id);   
                } else if($status == 'PP' || $status == 'PC'){
                    $orders = $orders->whereIn('status',['PP','PC']);
                    $orders = $orders->where('driver_id',$user->id);
                } else{
                    $orders = $orders->where('status',$status);
                }
                if($status == 'OD'){
                    if(@$from_date && @$to_date){
                        $orders = $orders->whereBetween('updated_at',[$from_date." 00:00:00", $to_date." 23:59:59"]);        
                    }
                }
                if($status == 'DA' || $status == 'OP' || $status == 'OD' || $status == 'RP'){
                    $orders = $orders->where('driver_id', $user->id);
                    $orders = $orders->where('status', $status);
                }
                
                if(@$reqData['params']['keyword']){
                    $orders = $orders->whereHas('orderMasterTab', function($q) use($keyword){
                        $q->where('order_no','like','%'.$keyword.'%');
                    });
                }
                if(@$reqData['params']['city_saerch']){
                    $orders = $orders->where(function($q1) use($city_saerch) {
                        $q1->whereHas('orderMasterTab', function($q) use($city_saerch){
                            $q->where('shipping_city','like','%'.$city_saerch.'%');
                        })->orWhereHas('orderMasterTab.getCityNameByLanguage', function ($query1) use ($city_saerch) {
                            $query1->where('name', 'like', '%'.$city_saerch.'%');
                        });
                    });
                }
                if(@$reqData['params']['page']) {
                    $page = @$reqData['params']['page']?$reqData['params']['page']:0; 
                    $limit = 10;
                    if($page == 1) {
                        $start = 0;
                    } else {
                        $start = (($page -1) * $limit);
                    }
                    $response['result']['orders'] = $orders->orderBy('updated_at','desc')->skip($start)->take($limit)->get();
                } else {
                    $response['result']['orders'] = $orders->orderBy('updated_at','desc')->get();
                }

                # for total etc
                $orders = OrderSeller::with([
                    'orderMasterTab',
                    'orderMasterTab.getCityNameByLanguage:id,city_id,name,language_id,created_at,updated_at'
                ])->select('id','seller_id','order_master_id','subtotal','shipping_price',\DB::raw('(order_total - shipping_price) order_total'));

                $orders = $orders->whereHas('orderMasterTab', function($q){
                    $q->whereNotIn('status', ['','D','I','OC','F']);
                });
                
                # if new order list else others(DA,OP,OD)
                if($status == 'N'){
                    #Either driver id is 0 and drivers order(personally)
                    $orders = $orders->whereIn('driver_id', [0, $user->id]);
                    $orders = $orders->where('status', 'N');
                } else {
                    # status(DA/OP/OD)
                    $orders = $orders->where('driver_id', $user->id);
                }
                if($status == 'N' || $status == 'OD'){
                    $orders = $orders->where('status', $status);
                } else if($status == 'OP'){
                    $orders = $orders->where('status', 'OP');
                } else if($status == 'DA'){
                    $orders = $orders->whereIn('status',['RP','DA']);
                    $orders = $orders->where('driver_id',$user->id);   
                } else if($status == 'PP' || $status == 'PC'){
                    $orders = $orders->whereIn('status',['PP','PC']);
                    $orders = $orders->where('driver_id',$user->id);
                } else {
                    $orders = $orders->where('status',$status);
                }
                if($status == 'OD'){
                    if(@$from_date && @$to_date){
                        $orders = $orders->whereHas('orderMasterTab', function($q) use($from_date, $to_date) {
                            $q = $q->whereBetween('last_status_date',[$from_date." 00:00:00", $to_date." 23:59:59"]);        
                        });
                    }
                }
                if($status == 'DA' || $status == 'OP' || $status == 'OD' || $status == 'RP'){
                    $orders = $orders->where('driver_id', $user->id);
                    $orders = $orders->where('status', $status);
                }
                
                if(@$reqData['params']['keyword']){
                    $orders = $orders->whereHas('orderMasterTab', function($q) use($keyword){
                        $q->where('order_no','like','%'.$keyword.'%');
                    });
                }
                if(@$reqData['params']['city_saerch']){
                    $orders = $orders->where(function($q1) use($city_saerch) {
                        $q1->whereHas('orderMasterTab', function($q) use($city_saerch){
                            $q->where('shipping_city','like','%'.$city_saerch.'%');
                        })->orWhereHas('orderMasterTab.getCityNameByLanguage', function ($query1) use ($city_saerch) {
                            $query1->where('name', 'like', '%'.$city_saerch.'%');
                        });
                    });
                }
                $orders = $orders->orderBy('updated_at','desc')->get();

                $codSubtotal = $orders->sum(function($orderSeller) {
                    return $orderSeller->orderMasterTab->payment_method == 'C'?$orderSeller->order_total:0;
                }); 
                $onlineSubtotal = $orders->sum(function($orderSeller) {
                    return $orderSeller->orderMasterTab->payment_method == 'O'?$orderSeller->order_total:0;
                });

                if($status == 'OP' || $status == 'OD') {
                    # shipping
                    $codShipping = $orders->groupBy('order_master_id')->sum(function($orderSeller) {
                        return $orderSeller[0]->orderMasterTab->payment_method == 'C'?$orderSeller[0]->orderMasterTab->shipping_price:0;
                    }); 
                    $onlineShipping = $orders->groupBy('order_master_id')->sum(function($orderSeller) {
                        return $orderSeller[0]->orderMasterTab->payment_method == 'O'?$orderSeller[0]->orderMasterTab->shipping_price:0;
                    });
                } else {
                    # shipping
                    $codShipping = $orders->sum(function($orderSeller) {
                        return $orderSeller->orderMasterTab->payment_method == 'C'?$orderSeller->orderMasterTab->shipping_price:0;
                    }); 
                    $onlineShipping = $orders->sum(function($orderSeller) {
                        return $orderSeller->orderMasterTab->payment_method == 'O'?$orderSeller->orderMasterTab->shipping_price:0;
                    });
                }
                
                //start cod
                $response['result']['cod_tot_shipping_cost'] = $codShipping;
                $response['result']['cod_sub_total'] = $codSubtotal;
                
                //start online
                $response['result']['online_total_shipping_cost'] = $onlineShipping;
                $response['result']['online_sub_total'] = $onlineSubtotal;
            } else if($user->status == 'I') {
                $response['error'] = __('errors.-5068');
            } else {
                $response['error'] = __('errors.-5067');
            }
            return response()->json($response);
        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        } catch (\Exception $th) {
            $response['error'] = $th->getMessage();
            return response()->json($response);
        }
    }
    
    /**
    *@ Method: sendMailDriverAssignEntireOrder
    *@ Description: to send mail to merchant customer admin order notification mail that order is send.
    *@ Author: Jayatri
    */
    private function sendMailDriverAssignEntireOrder($order){
        if(@$order->status == 'DA' && @$order->order_type == 'E'){
            $details = $this->order_details->where('order_master_id',@$order->id)->first();
            $merchant = $this->merchants->where(['id'=>@$details->seller_id])->first();
            // start
            // $update_ord_sellers
            $orderMasterDetails                 =   new \stdClass();
            $orderMasterDetails->order_no       =   @$order->order_no;
            $orderMasterDetails->order_type     =   @$order->order_type;
            $orderMasterDetails->payment_method =   @$order->payment_method;
            $orderMasterDetails->order_total    =   @$order->order_total;
            $orderMasterDetails->delivery_date    =   @$order->delivery_date;
            $orderMasterDetails->delivery_time    =   @$order->delivery_time;
            $orderMasterDetails->shipping_fname     =   @$order->shipping_fname ;
            $orderMasterDetails->shipping_lname     =   @$order->shipping_lname ;
            $orderMasterDetails->shipping_email     =   @$order->shipping_email ;
            $orderMasterDetails->shipping_phone     =   @$order->shipping_phone ;
            $orderMasterDetails->shipping_country     =   @$order->shipping_country ;
            $orderMasterDetails->shipping_city     =   @$order->shipping_city ;
            $orderMasterDetails->shipping_state     =   @$order->shipping_state ;
            $orderMasterDetails->shipping_street     =   @$order->shipping_street ;
            $orderMasterDetails->shipping_avenue     =   @$order->shipping_avenue ;
            $orderMasterDetails->shipping_building_no     =   @$order->shipping_building_no ;
            $orderMasterDetails->shipping_zip     =   @$order->shipping_zip ;
            $orderMasterDetails->shipping_postal_code     =   @$order->shipping_postal_code ;
            $orderMasterDetails->shipping_more_address     =   @$order->shipping_more_address ;
            $orderMasterDetails->shipping_price     =   @$order->shipping_price ;
            $orderMasterDetails->subtotal     =   @$order->subtotal ;
            $orderMasterDetails->status     =   @$order->status ;
            $orderMasterDetails->created_at     =   @$order->created_at ;
            $orderMasterDetails->updated_at     =   @$order->updated_at ;
            $orderMasterDetails->email     =   $merchant->email;
            $orderMasterDetails->email1     =   $merchant->email1;
            $data['orderMasterDetails'] = $orderMasterDetails; 
            $setting = $this->settings->where('id','!=',0)->first();
            $count = 0;
            if(@$merchant->email1){
                $data['bcc'] [$count] = $merchant->email1;
                $count++; 
            }
            if(@$merchant->email2){
                $data['bcc'] [$count] = $merchant->email2;
                $count++; 
            }
            if(@$merchant->email3){
                $data['bcc'] [$count] = $merchant->email3; 
                $count++;
                $count++;
            }
            if(@$setting->email_2){
                $data['bcc'] [$count] = $setting->email_2; 
                $count++;
            }
            if(@$setting->email_3){
                $data['bcc'] [$count] = $setting->email_3; 
                $count++;
            }

            Mail::send(new SendMailDriverNotifydetails($data));
                    
        }else if(@$order->order_type == 'I' && @$order->status == 'DA'){
            //sent mail to merchant admin order notification mail
            $data = [];
            $data['order'] = $order = $this->order_master->where('order_no',@$order->order_no)
            ->with('shippingAddress.getCountry','billingAddress.getCountry','getCountry')->first();
            $data['orderDetails'] = $this->order_details->where(['order_master_id' => $order->id])
            ->with('sellerDetails','defaultImage','productByLanguage','productVariantDetails')
            ->get();
            $contactList = [];
            $i=0;
            $user = $this->customers->whereId($order->user_id)->first();
            $data['fname'] = @$user->fname;
            $data['lname'] = @$user->lname;  
            $data['email'] = @$user->email;
            $contactList[$i] = @$user->email;
            $data['orderNo'] = @$order->order_no;
            $data['language_id'] = getLanguage()->id;
            $data['bcc'] = $contactList;
            $data['sub_total'] =  $this->order_details->where(['order_master_id' => @$order->id])->sum('sub_total');
            $data['total'] =  $this->order_details->where(['order_master_id' => @$order->id])->sum('total');
            $dtls_mail = $this->order_details->where(['order_master_id' => @$order->id])->get();
            foreach(@$dtls_mail as $contact){
                $data['orderDetails'] = $this->order_details->where(['order_master_id' => $order->id,'seller_id'=>@$contact->seller_id])
                ->with('sellerDetails','defaultImage','productByLanguage','productVariantDetails')
                ->get();
                $data['sub_total'] =  $this->order_details->where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
                $data['total'] =  $this->order_details->where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
                    $i++;
                    $contactList[$i] = $contact->sellerDetails->email;

                    if($contact->sellerDetails->email1) {
                        $contactListEmail1[$i] = $contact->sellerDetails->email1;
                    }
                    if($contact->sellerDetails->email2) {
                        $contactListEmail2[$i-1] = $contact->sellerDetails->email2;
                    }
                    if($contact->sellerDetails->email3) {
                        $contactListEmail3[$i-1] = $contact->sellerDetails->email3;
                    }
                $contactList = array_unique($contactList);
                if(@$contactListEmail1 != '') {
                    $contactListEmail1 = array_unique($contactListEmail1);
                    $contactList = array_merge($contactList, $contactListEmail1);
                }
                if(@$contactListEmail2 != '') {
                    $contactListEmail2 = array_unique($contactListEmail2);
                    $contactList = array_merge($contactList, $contactListEmail2);
                }
                if(@$contactListEmail3 != '') {
                    $contactListEmail3 = array_unique($contactListEmail3);
                    $contactList = array_merge($contactList, $contactListEmail3);
                }
                $data['bcc'] = $contactList;
                Mail::send(new SendOrderAceptedMail($data));
            }
        }
    }

    /**
    *@ Method: orderAssign
    *@ Description: driver order assign
    *@ Author: Surajit
    */
    public function orderAssign(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $id = $reqData['params']['order_id'];
        $seller_id = $reqData['params']['seller_id'];
        try {
            $user = JWTAuth::toUser();
            if($user->status == 'A'){
                $orderSeller = OrderSeller::where(['order_master_id'=>$id,'seller_id'=>$seller_id])->where('driver_id', '0')->first();
                $master_order = $this->order_master->where(['id'=>$id])->first();
                
                # insert to order timing table
                $this->insertOrderStatusTiming($master_order, 'DA');

                if(@$orderSeller) {
                    $this->order_details->where(['order_master_id'=>$id,'seller_id'=>$seller_id,'driver_id'=>0])->update(['driver_id' => $user->id,'status' => 'DA']);
                    OrderSeller::where(['order_master_id'=>$id,'seller_id'=>$seller_id])->update(['driver_status_m'=>'DA', 'status' => 'DA', 'driver_id' => $user->id]);
                    //check all orders are assigned in order_details table or not
                    $orders = $this->order_details->select('id','order_master_id','quantity','total','status','created_at')->where(['order_master_id'=>$id,'seller_id'=>$seller_id])->first();
                    
                    $orderAssignCount = $this->order_details->select('id','order_master_id','quantity','total','status','created_at')
                    ->where(['order_master_id' => $orders->order_master_id,'status' => 'N'])->count();

                    //if driver assigned for all orders in order_details table then change order_master table status to DA 
                    if($orderAssignCount == 0) {
                        $this->order_master->whereId($orders->order_master_id)->update(['status' => 'DA']);
                    }
                    $response['result'] = __('success.-4055');
                } else {
                    // $response['error'] = ERRORS['-33029'];g
                    $response['error'] = __('errors.-5078');
                }
            }else if($user->status == 'I'){
                $response['error'] = __('errors.-5068');
            }else{
                $response['error'] = __('errors.-5067');
            }
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] = $th->getMessage();
            return response()->json($response);
        }
    }
    
    

    /**
    * Note: new feature added. partial pickup
    */
    public function changeStatus(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $order_id = $reqData['params']['order_id'];
        $seller_id = @$reqData['params']['seller_id'];
        $status = $reqData['params']['change_status'];
        try {
            $user = JWTAuth::toUser();
            if($user->status == 'A'){
                $userId = $user->id;
                $orderDetail = $this->order_details->where(['order_master_id' => $order_id])
                ->where('driver_id',$userId);
                if(@$seller_id) {
                    $orderDetail = $orderDetail->where(['seller_id' => $seller_id]);
                }
                $orderDetail = $orderDetail->first();

                if(@$orderDetail){
                    
                    # update status in order details table
                    $ord_m_t = $order = $this->order_master->where(['id'=>@$order_id])->first();
                    
                    # insert to order timing table
                    $this->insertOrderStatusTiming($order, $status);

                    if($status == 'OP') {
                        # calculate delayInMins
                        if(@$order->order_type == 'E') {
                            if(@$order->external_order_type == 'S') {
                                $now = time(); // or your date as well
                                $your_date = strtotime($order->delivery_date .' '.$order->delivery_time);
                                $datediff = $now - $your_date;
                                $delayInMins = 0;
                                if($datediff > 0) {
                                    $delayInMins = round($datediff / 60);
                                }
                                OrderSeller::where(['order_master_id' => $order->id])->update([
                                    'delay_in_minutes'   => $delayInMins, 
                                    'driver_pickup_date' => date('Y-m-d H:i:s')
                                ]);
                                OrderMaster::where(['id' => $order->id])->update([
                                    'delay_in_minutes'   => $delayInMins, 
                                    // 'driver_pickup_date' => date('Y-m-d H:i:s')
                                ]);
                            } else if(@$order->external_order_type == 'L') {
                                $order = OrderMaster::with(['getPreferredTime'])->where(['id' => $order->id])->first();
                                $now = time(); // or your date as well
                                $your_date = strtotime($order->preferred_delivery_date .' '.$order->getPreferredTime->to_time);
                                $datediff = $now - $your_date;
                                $delayInMins = 0;
                                if($datediff > 0) {
                                    $delayInMins = round($datediff / 60);
                                }
                                OrderSeller::where(['order_master_id' => $order->id])->update([
                                    'delay_in_minutes'   => $delayInMins, 
                                    // 'driver_pickup_date' => date('Y-m-d H:i:s')
                                ]);

                                OrderMaster::where(['id' => $order->id])->update([
                                    'delay_in_minutes'   => $delayInMins, 
                                    'driver_pickup_date' => date('Y-m-d H:i:s')
                                ]);
                            }
                        }
                    }
                    # checking partial pickup or not
                    if($ord_m_t->is_more_seller == 'Y' && $ord_m_t->is_final_driver == 'N' && $status == 'OP') {
                        $update['status'] = 'PP';
                        $this->order_details->where(['order_master_id'=>$order_id,'seller_id'=>$seller_id])->where('driver_id',$userId)->update($update);
                        $update['last_status_date'] = date('Y-m-d H:i:s');
                        $this->order_master->where(['id'=>@$order_id])->update($update);
                        OrderSeller::where(['order_master_id'=>$order_id,'seller_id'=>$seller_id])->update(['status' => 'PP']);
                    } else if($ord_m_t->is_more_seller == 'Y' && $ord_m_t->is_final_driver == 'Y') {
                        $update['status'] = $status;
                        $this->order_details->where(['order_master_id'=>$order_id])->where('driver_id',$userId)->update($update);
                        OrderSeller::where(['order_master_id'=>$order_id])->update($update);
                    } else {
                        $update['status'] = $status;
                        $this->order_details->where(['order_master_id'=>$order_id,'seller_id'=>$seller_id])->where('driver_id',$userId)->update($update);
                        OrderSeller::where(['order_master_id'=>$order_id,'seller_id'=>$seller_id])->update($update);
                    }
                    
                    $response['result'] = __('success.-4003');
                    //start
                    #checking for order status for each product of any order
                    $details_chk = $this->order_details->where(['order_master_id'=>$order_id])->get();
                    $flag = 0;
                    if($ord_m_t->is_more_seller == 'Y' && $ord_m_t->is_final_driver == 'N' && $status == 'OP') {
                        $status1 = 'PP';
                    } else {
                        $status1 = $status;
                    }
                    if(@$details_chk){
                        foreach(@$details_chk as $dtc){
                            if($dtc->status != $status1){
                                $flag = 1;
                            }   
                        }
                    }
                    #update whole order status in order master table if all are in same status
                    if($flag == 0) {
                        $update = [];
                        // $this->sendNotification(@$master_order);
                        # when whole status changed to PP we need to change to PC
                        if($ord_m_t->is_more_seller == 'Y' && $ord_m_t->is_final_driver == 'N' && $status == 'OP') {
                            $update['status'] = 'PC'; // partial pickup completed
                        } else {
                            $update['status'] = $status;
                        }
                        # update partial or not
                        OrderSeller::where(['order_master_id'=>$order_id])->update($update);
                        $this->order_details->where(['order_master_id'=>$order_id])->update($update);

                        $update['last_status_date'] = date('Y-m-d H:i:s');
                        $this->order_master->where(['id'=>@$order_id])->update($update);
                        $ord_m_t = OrderMaster::where(['id'=>@$order_id])->first();

                        # for delivered external order
                        if(@$ord_m_t->order_type == 'E' && @$ord_m_t->status == 'OD') {
                            $this->setCommissionExternalOrder($order_id,$ord_m_t);
                            $response['result'] = __('success.-4003');
                        } else if(@$ord_m_t->order_type == 'I' && @$ord_m_t->status == 'OD') {
                            $chk_comm = $this->setCommision($ord_m_t);
                            if($chk_comm == 1){
                                if(@$master_order->order_type == 'I'){
                                    // $this->sendNotificationCustomer(@$master_order->id,@$master_order->user_id);    
                                }
                                $response['result'] = __('success.-4003');
                            } else {
                                $response['error'] = __('errors.-5010');    
                            }
                        }
                    }
                    $response['result'] = __('success.-4003');
                } else {
                    $response['error'] = __('errors.-5010');
                }
            } else if($user->status == 'I') {
                $response['error'] = __('errors.-5068');
            } else {
                $response['error'] = __('errors.-5067');
            }
            $master_user = $this->order_master->with(['orderMerchants.orderSeller'])->where(['id'=>@$order_id])->first();
            $sendWhatsApp = 0;
            $sendWhatsAppExternal = 0;
            foreach ($master_user->orderMerchants as $key => $value) {
                if($value->orderSeller->notify_customer_by_whatsapp_internal == 'Y') {
                    $sendWhatsApp += 1;
                }

                if($value->orderSeller->notify_customer_by_whatsapp_external == 'Y') {
                    $sendWhatsAppExternal += 1;
                }
            }
            if(@$master_user){
                if(($master_user->status == 'OP' && $master_user->order_type == 'I' ) || $master_user->status == 'OD'){
                    # send whatsapp message to customer
                    if(@$sendWhatsApp > 0) {
                        $this->whatsApp->sendMessage($order_id);
                    }
                    //$this->whatsApp->sendMessageMerchants($order_id);
                    $this->sendCustomerAndSellerMail($order_id);
                } else if(($master_user->status == 'OP' && $master_user->order_type == 'E' ) || $master_user->status == 'OD'){
                    # send whatsapp message to customer
                    if(@$sendWhatsAppExternal > 0) {
                        //$this->whatsApp->sendMessage($order_id);
                    }
                    //$this->whatsApp->sendMessageMerchants($order_id);
                    $this->sendMailForExternalOrder($master_user);
                }
            }
            // $response['result']['language_details'] = $language;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] = $th->getMessage();
            return response()->json($response);
        }
    }

    private function sendMailForExternalOrder($order = []) {
        if(@$order->order_type == 'E') {
            $details = $this->order_details->where('order_master_id',@$order->id)->first();
            $merchant = $this->merchants->where(['id'=>@$details->seller_id])->first();
            $country_nm_txt = CountryDetails::where(['country_id'=>@$order->shipping_country,'language_id'=>1])->first();
            // start
            // $update_ord_sellers
            $orderMasterDetails                         = new \stdClass();
            $orderMasterDetails->order_no               = @$order->order_no;
            $orderMasterDetails->order_type             = @$order->order_type;
            $orderMasterDetails->payment_method         = @$order->payment_method;
            $orderMasterDetails->order_total            = @$order->order_total;
            $orderMasterDetails->product_total_weight   = @$details->weight;
            $orderMasterDetails->invoice_no             = @$order->invoice_no;
            $orderMasterDetails->invoice_details        = @$order->invoice_details;
            $orderMasterDetails->delivery_date          = @$order->delivery_date;
            $orderMasterDetails->delivery_time          = @$order->delivery_time;
            $orderMasterDetails->shipping_fname         = @$order->shipping_fname ;
            $orderMasterDetails->shipping_lname         = @$order->shipping_lname ;
            $orderMasterDetails->shipping_email         = @$order->shipping_email ;
            $orderMasterDetails->shipping_phone         = @$order->shipping_phone ;
            $orderMasterDetails->shipping_country       = @$country_nm_txt->name;
            $orderMasterDetails->shipping_city          = @$order->shipping_city ;
            $orderMasterDetails->shipping_state         = @$order->shipping_state ;
            $orderMasterDetails->shipping_street        = @$order->shipping_street ;
            $orderMasterDetails->shipping_avenue        = @$order->shipping_avenue ;
            $orderMasterDetails->shipping_building      = @$order->shipping_building;
            $orderMasterDetails->shipping_block         = @$order->shipping_block;
            $orderMasterDetails->shipping_zip           = @$order->shipping_zip ;
            $orderMasterDetails->shipping_address       = @$order->shipping_address ;
            $orderMasterDetails->shipping_price         = @$order->shipping_price ;
            $orderMasterDetails->subtotal               = @$order->subtotal ;
            $orderMasterDetails->status                 = @$order->status ;
            $orderMasterDetails->created_at             = @$order->created_at ;
            $orderMasterDetails->updated_at             = @$order->updated_at ;
            $orderMasterDetails->email                  = @$order->shipping_email;
            $orderMasterDetails->subject                = "Order details!";
            $data['orderMasterDetails']                 = $orderMasterDetails;
            $setting = $this->settings->where('id','!=',0)->first();
            $count = 0;
            $contactList = [];
            #sent to admin 
            if(@$order->status == 'N'){
                $count = 0;
                $orderMasterDetails->email   = $setting->email_1;
                $orderMasterDetails->subject = "New External order placed! Order No: ".@$order->order_no."";
                if(@$setting->email_2){
                    $contactList[$count] = $setting->email_2; 
                    $count++;
                }
                if(@$setting->email_3){
                    $contactList[$count] = $setting->email_3;
                    $count++;
                }

                $contactList = array_unique($contactList);
                $data['bcc'] = $contactList;
                if(@$data['orderMasterDetails']->email) {
                    Mail::send(new SendMailDriverNotifydetails($data));
                }
            }
            #sent to merchant
            if(@$order->status == 'DA'){
                $count = 0;
                $orderMasterDetails->email   = @$merchant->email;
                $orderMasterDetails->subject = "Order is accepted! External order details! Order No : ".@$order->order_no;
                if(@$merchant->email1){
                    $contactList[$count] = @$merchant->email1;
                    $count++;
                }
                if(@$merchant->email2){
                    $contactList[$count] = @$merchant->email2;
                    $count++;
                }
                if(@$merchant->email3){
                    $contactList[$count] = @$merchant->email3;
                    $count++;
                }
                if(@$merchant->email){
                    $contactList[$count] = @$merchant->email;
                    $count++;
                }

                $contactList = array_unique($contactList);
                $data['bcc'] = $contactList;
                if(@$data['orderMasterDetails']->email) {
                    Mail::send(new SendMailDriverNotifydetails($data));
                }
            }
            # send merchant on order picked up
            if(@$order->status == 'OP'){
                $count = 0;
                $orderMasterDetails->email   = @$merchant->email;
                $orderMasterDetails->subject = "Your order is picked up! Order No:".@$order->order_no." ! Order Details!";
                if(@$merchant->email1){
                    $contactList[$count] = @$merchant->email1;
                    $count++;
                }
                if(@$merchant->email2){
                    $contactList[$count] = @$merchant->email2;
                    $count++;
                }
                if(@$merchant->email3){
                    $contactList[$count] = @$merchant->email3;
                    $count++;
                }
                if(@$merchant->email){
                    $contactList[$count] = @$merchant->email;
                    $count++;
                }

                $contactList = array_unique($contactList);
                $data['driver_rating_link'] = url('driver-review/' . @$order->order_no . '/' . @$merchant->id . '/' .@$order->verify_code);
                $data['bcc'] = $contactList;
                if(@$data['orderMasterDetails']->email) {
                    Mail::send(new SendMailDriverNotifydetails($data));
                }
            }

            #send to customer
            if(@$order->status == 'OD'){
                $count = 0;
                if(@$merchant->notify_customer_by_email_external == 'Y') {
                    $orderMasterDetails->email   = @$order->email;
                    $orderMasterDetails->subject = "Your order is delivered successfully! External order details! Order no: ".@$order->order_no;

                    $contactList = array_unique($contactList);
                    $data['bcc'] = $contactList;
                    if(@$data['orderMasterDetails']->email) {
                        // $data['driver_rating_link'] = url('driver-review/' . @$order->order_no . '/' . @$order->user_id . '/' .@$order->verify_code);
                        Mail::send(new SendMailDriverNotifydetails($data));
                    }
                }

                # notify merchant on external order delivered
                if(@$merchant->notify_merchant_on_delivery_external == 'Y') {
                    $orderMasterDetails->email   = @$merchant->email;
                    $orderMasterDetails->subject = "Order has been delivered successfully! External order details! Order no: ".@$order->order_no;

                    $contactList = array_unique($contactList);
                    $data['bcc'] = $contactList;
                    if(@$data['orderMasterDetails']->email) {
                        $data['driver_rating_link'] = '';
                        Mail::send(new SendMailDriverNotifydetails($data));
                    }
                }
            }
        }
    }

    /**
    * Method: insertOrderStatusTiming
    * Description: This method is used to insert time in to order status timing table
    * Author: Sanjoy
    */
    private function insertOrderStatusTiming($order, $status) {
        $prevStatus = $order->status;

        $prevOrderStatus = OrderStatusTiming::where(['order_id' => $order->id, 'status' => $prevStatus])->first();
        # check same status is already exist or not
        $currentOrderStatus = OrderStatusTiming::where(['order_id' => $order->id, 'status' => $status])->first();
        # if already inserted in the table then only need to update end_time and duration_in_minutes
        if(@$currentOrderStatus) {
            if(@$prevOrderStatus) {
                # update  end_time and duration_in_minutes for previous status
                $durationInMinutes = (time() - strtotime($prevOrderStatus->start_time));
                OrderStatusTiming::where(['order_id' => $order->id, 'status' => $prevStatus])
                ->update([
                    'end_time'              => date('Y-m-d H:i:s'),
                    'duration_in_minutes'   => $durationInMinutes + $prevOrderStatus->duration_in_minutes
                ]);
            }
            
            $durationInMinutes = (time() - strtotime($currentOrderStatus->start_time));
            OrderStatusTiming::where(['order_id' => $order->id, 'status' => $status])
            ->update([
                'start_time'              => date('Y-m-d H:i:s'),
                
            ]);                
        } else {
            if(@$prevOrderStatus) {
                $durationInMinutes = (time() - strtotime($prevOrderStatus->start_time));
                # update end_date for previous status
                OrderStatusTiming::where(['order_id' => $order->id, 'status' => $prevStatus])
                ->update([
                    'end_time'              => date('Y-m-d H:i:s'),
                    'duration_in_minutes'   => $durationInMinutes + $prevOrderStatus->duration_in_minutes
                ]);
            }
            OrderStatusTiming::create([
                'order_id'            => $order->id,
                'status'              => $status,
                'start_time'          => date('Y-m-d H:i:s')
            ]);
        }
    }

    /**
    * This method is newly added here..
    */
    private function sendCustomerAndSellerMail($orderId) {
        $data = [];
        $data['order'] = $order = OrderMaster::where('id', $orderId)
        ->with([
            'shippingAddress.getCountry',
            'billingAddress.getCountry',
            'getCountry'
        ])
        ->first();

        $data['orderDetails'] = OrderDetail::where(['order_master_id' => $orderId])
        ->with([
            'sellerDetails',
            'defaultImage',
            'productByLanguage',
            'productVariantDetails'
        ])
        ->get();
        
        $contactList = [];
        $i=0;
        $user = User::whereId($order->user_id)->first();
        $data['fname']          = $user->fname;
        $data['lname']          = $user->lname;  
        $data['email']          = $user->email;
        $data['user_type']      = $user->user_type;
        $data['shipping_price'] = @$order->shipping_price;
        $data['orderNo']        = @$order->order_no;
        $data['language_id']    = getLanguage()->id;
        $data['sub_total']      = @$order->subtotal;
        $data['order_total']    = @$order->order_total;
        $data['total_discount'] = @$order->total_discount;

        $contactList[$i]        = @$order->shipping_email;
        if(@$order->status == 'N'){
            $data['subject'] = "Your order has been placed successfully! Order no: ".@$order->order_no;
        } else if(@$order->status == 'DA') {
            $data['subject'] = "Your order is accepted! Order No:".@$order->order_no." ! Order Details!";
        } else if(@$order->status == 'RP') {
            $data['subject'] = "Your order is ready for pick up! Order No:".@$order->order_no." ! Order Details!";
        } else if(@$order->status == 'OP') {
            $data['subject'] = "Your order is picked up! Order No:".@$order->order_no." ! Order Details!";
        } else if(@$order->status == 'OD') {
            $data['subject'] = "Your order is delivered successfully! Order No:".@$order->order_no." ! Order Details!";
            # driver rating link
            $data['driver_rating_link'] = url('driver-review/' . @$order->order_no . '/' . @$order->user_id . '/' .@$order->verify_code);
        }

        #send mail admin to notify for new order only as client requirement (internal order)
        $i=0;
        if(@$order->status =='N'){
            $setting = Setting::first();
            if(@$setting->email_1) {
                $contactList[$i] = $setting->email_1;
                $i++;
            }
            if(@$setting->email_2) {
                $contactList[$i] = $setting->email_2;
                $i++;
            }
            if(@$setting->email_3) {
                $contactList[$i] = $setting->email_3;
                $i++;
            }
        }
        $contactList = array_unique($contactList);
        $data['bcc'] = $contactList;

        #send to customer mail(order status can be new/driver assigned)
        if(@$order->status =='N' || @$order->status =='OD'){
            Mail::queue(new OrderMail($data));
        }
        
        
        # send mail order details to seller mail
        $contactList = [];
        $data['bcc'] = $contactList;
        $dtls_mail = OrderDetail::with([
            'sellerDetails'
        ])
        ->where(['order_master_id' => $order->id])
        ->groupBy('seller_id')
        ->get();
        
        foreach(@$dtls_mail as $contact) {
            $data['driver_rating_link'] = '';
            $contactList = [];
            $data['bcc'] = [];
            $contactListEmail3 = [];
            $i = 0;
            //start
            $data['user_type'] = 'C';
            $data['hide_customer_info'] = $contact->sellerDetails->hide_customer_info;

            $data['orderDetails'] = OrderDetail::where([
                'order_master_id' => $order->id,
                'seller_id'       => @$contact->seller_id
            ])
            ->with([
                'sellerDetails',
                'defaultImage',
                'productByLanguage',
                'productVariantDetails'
            ])
            ->get();
            
            $data['sub_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
            $data['order_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
            $data['total_discount'] = OrderSeller::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total_discount');
            $data['shipping_price'] = "NA";
            if(@$order->status == 'N') {
                $data['subject'] = "New Order Placed!Order No : ".@$order->order_no." ! Order Details!";
            } else if(@$order->status =='DA') {
                $data['subject'] = "Order is Accepted! Order No : ".@$order->order_no." ! Order Details!";    
            } else if(@$order->status =='RP') {
                $data['subject'] = "Order is ready for pickup! Order No : ".@$order->order_no." ! Order Details!";    
            } else if(@$order->status =='OP') {
                $data['subject'] = "Order is picked up! Order No : ".@$order->order_no." ! Order Details!";
                # driver rating link
                if(@$contact->driver_id) {
                    $data['driver_rating_link'] = url('merchant/driver-review/' . @$order->order_no . '/' . $contact->seller_id .'/' . @$contact->getOrderSeller->verify_code);
                }
            } else if(@$order->status =='OD') {
                $data['subject'] = "Order is Delivered! Order No : ".@$order->order_no." ! Order Details!";
            }
            
            // Fill the Bcc email address
            $contactList[$i] = $contact->sellerDetails->email;
            $i++;
            #send mail to merchant if status driver assigned(client requirement)
            // if(@$order->status =='DA'){
            //     if($contact->sellerDetails->email1) {
            //         $contactListEmail1[$i] = $contact->sellerDetails->email1;
            //         $i++;
            //     }
            //     if($contact->sellerDetails->email2) {
            //         $contactListEmail2[$i] = $contact->sellerDetails->email2;
            //         $i++;
            //     }
            //     if($contact->sellerDetails->email3) {
            //         $contactListEmail3[$i] = $contact->sellerDetails->email3;
            //         $i++;
            //     }    
            // }
            $contactList = array_unique($contactList);
            if(@$contactListEmail1 != '') {
                $contactListEmail1 = array_unique($contactListEmail1);
                $contactList = array_merge($contactList, $contactListEmail1);
            }
            if(@$contactListEmail2 != '') {
                $contactListEmail2 = array_unique($contactListEmail2);
                $contactList = array_merge($contactList, $contactListEmail2);
            }
            if(@$contactListEmail3 != '') {
                $contactListEmail3 = array_unique($contactListEmail3);
                $contactList = array_merge($contactList, $contactListEmail3);
            }
            $data['bcc'] = $contactList;
            #send web notification to merchant only for update merchant dashboard
            Mail::queue(new SendOrderDetailsMerchantMail($data));
        }
    }

    private function setCommissionExternalOrder($order_master_id,$order){
        $details = $this->order_details->where('order_master_id',@$order_master_id)->first();
        $total_earning = $this->order_details->where(['seller_id'=>@$details->seller_id,'order_master_id'=>@$order_master_id])->sum('sub_total');
        $merchant = Merchant::where(['id'=>@$details->seller_id])->first();
        $updateM['total_earning'] = $total_earning+@$merchant->total_earning;    
        // $updateM['total_commission'] = ($total_earning * (@$merchant->commission/100));
        $updateM['total_due'] = (@$merchant->total_due)+$total_earning;
        $this->merchants->where(['id'=>@$details->seller_id])->update($updateM);  
        OrderSeller::where(['order_master_id'=>@$order_master_id,'seller_id'=>$details->seller_id])->update(['earning' =>$total_earning]);            
        $price = $order->subtotal;
    }

    //internal order
    private function setCommision($orderChk) {   
        $details = OrderDetail::where('order_master_id',@$orderChk->id)->get();
        //set comission
        $ord_s = OrderSeller::where('order_master_id',@$orderChk->id)->get();
        if(@$ord_s){
            foreach(@$ord_s as $os){
                $total_earn = OrderSeller::where(['order_master_id'=>@$orderChk->id,'seller_id'=>$os->seller_id])->sum('order_total');
                $shippingPrice = $this->order_sellers->where(['order_master_id'=>@$orderChk->id,'seller_id'=>$os->seller_id])->sum('shipping_price');
                $total_earn = $total_earn - $shippingPrice;
                $total_commission = OrderSeller::where(['order_master_id'=>@$orderChk->id,'seller_id'=>$os->seller_id])->sum('total_commission');
                $merchant = Merchant::where('id',@$os->seller_id)->first();
                $individual_earning = (@$os->subtotal - @$os->total_discount)- @$os->total_commission;

                $toal_earn = ($total_earn - $total_commission)+$merchant->total_earning;
                $total_due = ($total_earn - $total_commission) + $merchant->total_due;
                $total_commission = $total_commission + $merchant->total_commission;
                
                $merchant_update['total_earning'] = $toal_earn;
                $merchant_update['total_due'] = $total_due;
                $merchant_update['total_commission'] = $total_commission;
                Merchant::where('id',$os->seller_id)->update($merchant_update); 
                OrderSeller::where('id', $os->id)->update(['earning' => $individual_earning]);
            }
            $ord_comm = OrderSeller::where(['order_master_id'=>@$orderChk->id])->sum('total_commission');
            
            $update['total_commission'] = $ord_comm;
            $update['delivery_date'] = now();
            $this->order_master->where('id',@$orderChk->id)->update($update);
            $this->creaditLoyaltyPoint($orderChk);
            return 1;
        }     
    }

    /**
    *@ Method: orderDetails
    *@ Description: driver order details
    *@ Author: Surajit/Jayatri
    */
    public function orderDetails(Request $request, $orderId = null){
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $id = $orderId;
        try {
            $user = JWTAuth::toUser();
            if($user->status == 'A'){
            $userId = $user->id;
            $orderMaster = $this->order_master->with([
                'getOrderAllImages',
                'getCountry:id,country_id,name,language_id',
                'getCityNameByLanguage:id,city_id,name,language_id',
                'getBillingCountry:name,id,country_id,language_id',
                'getBillingCityNameByLanguage:name,id,city_id,language_id',
                'statusTiming',
                'getpickupCountryDetails',
                'getpickupCityDetails',
                'getPreferredTime'
            ])->whereId($id)->first();
            if(@$orderMaster){
                # get no of item.
                $no_of_items = $this->order_details->where(['order_master_id'=>$orderMaster->id]);
                $no_of_items = $no_of_items->where(function($q1) use($user) {
                    $q1 = $q1->where('driver_id', $user->id)
                        ->orWhere("driver_id", 0);
                });
                $no_of_items =$no_of_items->count();

                # subtotal
                $sub_total = $this->order_details->where(['order_master_id'=>$orderMaster->id]);
                $sub_total = $sub_total->where(function($q1) use($user) {
                    $q1 = $q1->where('driver_id', $user->id)
                        ->orWhere("driver_id", 0);
                });
                $sub_total = $sub_total->sum('total');
                $order_sellers = OrderSeller::with([
                    'orderMerchant.CountryDtls:id,country_id,language_id,name',
                    'orderMerchant.merchantCityDetails:id,city_id,name,language_id',
                    'orderDetails:id,order_master_id,product_id,product_variant_id,variants,seller_id,driver_id,quantity,weight,original_price,discounted_price,sub_total,total,status,updated_at',
                    'orderDetails.productDetails:id,user_id,brand_id',
                    'orderDetails.productDetails.productByLanguage:id,product_id,language_id,title,description',
                    'orderDetails.productDetails.defaultImage:id,image,product_id,is_default',
                    'orderDetails.productDetails.productVariants',
                    'orderDetails.productVariantDetails'
                ])
                ->where(['order_master_id' => $orderMaster->id])
                ->select('id','seller_id', 'delay_in_minutes', 'driver_pickup_date', 'order_master_id');

                $order_sellers = $order_sellers->whereHas('orderDetails', function($q) use ($userId){
                    $q->where('driver_id',$userId)
                    ->orWhere('driver_id',0);
                });
                $order_sellers = $order_sellers->whereHas('orderDetails', function($q){
                    $q->whereNotIn('status', ['','D','I','OC','F']);
                });
                $order_sellers = $order_sellers->get();
                $dnote = "";
                $order_driv = $this->order_details->where(['order_master_id'=>$orderId,'driver_id'=>$user->id])->groupBy('driver_id')->get();
                foreach($order_driv as $orddrr){
                    $dnote = $dnote." ".$orddrr->driver_notes." ";
                }
                
                $response['result']['d_notes'] = $dnote;
                $response['result']['orders'] = $orderMaster;
                $response['result']['details'] = $order_sellers;
                $response['result']['no_of_items'] = $no_of_items;
                $response['result']['sub_total'] = $sub_total;
            }
        }else if($user->status == 'I'){
            $response['error'] = __('errors.-5068');
        }
        else{
            $response['error'] = __('errors.-5067');
        }
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] = $th->getMessage();
            return response()->json($response);
        }
    }

    /**
    *@ Method: changePassword
    *@ Description: driver changePassword
    *@ Author: Jayatri
    */
    public function changePassword(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $nw_password = $reqData['params']['nw_password'];
        $confirm_password = $reqData['params']['confirm_password'];
        $old_password = $reqData['params']['old_password'];
        try {
            $user = JWTAuth::toUser();
            if($user->status == 'A'){
            // $user = auth()->user(); // this is not working
                $userId = $user->id;
                $dPassword = $user->password;
                if(\Hash::check($old_password,$user->password)){
                    $password = @$nw_password;
                    $new['password'] = \Hash::make($password);    
                    Driver::where('id',$userId)->update($new);
                    $response['result'] = __('success.-4064');
                }else{
                    $response['error'] = __('errors.-5063');
                }
            }else if($user->status == 'I'){
                $response['error'] = __('errors.-5068');
            }
            else{
                $response['error'] = __('errors.-5067');
            }
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] = $th->getMessage();
            return response()->json($response);
        }
    }

    /**
    *@ Method: changePaymentMethod
    *@ Description: driver changePaymentMethod
    *@ Author: Jayatri
    */
    public function changePaymentMethod(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $user = JWTAuth::toUser();
            if($user->status == 'A'){


                $userId = $user->id;
                $reqData = $request->json()->all();
                $order_id = $reqData['params']['order_id'];
                $order = $this->order_master->where('id',$order_id)->first();
                if(@$order){
                    if(@$order ->order_type == 'I'){
                        if(@$order->payment_method == 'C'){
                            //change cash on delivery
                            $update['payment_method'] = 'O';
                            $this->order_master->where('id',$order_id)->update($update);
                            $response['result'] = __('success.-4065');
                        }else if(@$order->payment_method == 'O'){
                            //cannot change as it is onlines
                            $response['error'] = __('errors.-5064');
                        }
                    }else if(@$order->order_type == 'E'){
                        if(@$reqData['params']['payment_method'] && @$reqData['params']['payment_method'] != null){
                            $update['payment_method'] = @$reqData['params']['payment_method'];
                            $this->order_master->where('id',$order_id)->update($update);
                            $response['result'] = __('success.-4065');
                        }else{
                            if(@$order->payment_method == 'C'){
                                $update['payment_method'] = 'O';
                                $this->order_master->where('id',$order_id)->update($update);
                                $response['result'] = __('success.-4065');
                            }else if(@$order->payment_method == 'O'){
                                $update['payment_method'] = 'C';
                                $this->order_master->where('id',$order_id)->update($update);
                                $response['result'] = __('success.-4065');
                            }
                        }
                    }
                }else{
                    $response['error'] = __('errors.-5063');
                }
            }
            else if($user->status == 'I'){
                $response['error'] = __('errors.-5068');
            }
            else{
                $response['error'] = __('errors.-5067');
            }
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] = $th->getMessage();
            return response()->json($response);
        }    
    }

    public function addNotes(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $user = JWTAuth::toUser();
            if($user->status == 'A'){
             $userId = $user->id;
            $reqData = $request->json()->all();
            $order_id = $reqData['params']['order_id'];
            // $seller_id = $reqData['params']['seller_id'];
             $notes = $reqData['params']['notes'];
                
                 $order = $this->order_master->where('id',$order_id)->first();
                if(@$order){
                    $admin_notes= " ";
                    $admin_notes = "<br/>".$user->fname."  ".$user->lname." : ".$notes." <br/>";
                    $dt_driver_dttt = $this->order_details->with('driverDetails')->where(['order_master_id'=>$order->id])->get();
                    foreach(@$dt_driver_dttt as $key=>$t){

                        if($t->driver_notes != null || $t->driver_notes != ""){
                            if($t->driver_id != 0){
                                if($t->driver_id != $user->id){
                                    $admin_notes = "<br/>".$admin_notes."<br/>".$t->driverDetails->fname." ".$t->driverDetails->lname." : ".$t->driver_notes."<br/>";    
                                }
                                
                            }
                        }
                    }
                    $update['driver_notes'] = nl2br($admin_notes);
                    $this->order_master->where('id',$order_id)->update($update);  
                    $ord_dttls = $this->order_details->where('order_master_id',$order_id)->first();
                    if(@$ord_dttls){
                         $order_sellers = OrderSeller::with(['orderMerchant.CountryDtls:id,country_id,language_id,name','orderMerchant.merchantCityDetails:id,city_id,name,language_id','orderDetails:id,order_master_id,product_id,product_variant_id,variants,seller_id,driver_id,quantity,weight,original_price,sub_total,total,status,updated_at','orderDetails.productDetails:id,user_id,brand_id','orderDetails.productDetails.productByLanguage:id,product_id,language_id,title,description','orderDetails.productDetails.defaultImage:id,image,product_id,is_default','orderDetails.productDetails.productVariants','orderDetails.productVariantDetails'])->where(['order_master_id'=>$order_id])->select('id','seller_id','order_master_id' );
                        $order_sellers = $order_sellers->whereHas('orderDetails', function($q) use ($userId){
                            $q->where('driver_id',$userId);
                        });
                        $order_sellers = $order_sellers->whereHas('orderDetails', function($q){
                            $q->whereNotIn('status', ['','D','I','OC']);
                        });
                        $order_sellers = $order_sellers->get();
                            foreach(@$order_sellers as $ord){
                                    $update_dtls['driver_notes'] = nl2br($notes);
                                    $this->order_details->where(['order_master_id'=>$order_id,'driver_id'=>$user->id])->update($update_dtls);
                            }
                    }
                    
                    $response['result'] = __('success.-4066');
                    // session()->flash("success",__('success.-4058'));                            
                }else{
                    $response['error'] = __('errors.-5066');
                    // session()->flash("error",__('errors.-5056'));        
                }
            }else if($user->status == 'I'){
                $response['error'] = __('errors.-5068');
            }
            else{
                $response['error'] = __('errors.-5067');
            }
            //end
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] = $th->getMessage();
            return response()->json($response);
        }
    }

    public function getLanguageDetails(Request $request){
        $response = ['jsonrpc'=>'2.0'];
            $reqData = $request->json()->all();
            $lang_prefix = $reqData['params']['lang_prefix'];
        try {
            
            $user = JWTAuth::toUser();
            if($user->status == 'A'){
                $userId = $user->id;
                $language = Language::where('prefix',$lang_prefix)->select('id','prefix','name','status')->first();
                $response['result']['language_details'] = $language;
            }else if($user->status == 'I'){
                $response['error'] = __('errors.-5068');
            }else{
                $response['error'] = __('errors.-5067');
            }
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] = $th->getMessage();
            return response()->json($response);
        }
    }
    //not in use below function
    public function driverProfile(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        // $reqData = $request->json()->all();
        // $id = $reqData['params']['order_id'];
        try {
                $user = JWTAuth::toUser();
                if($user->status == 'A'){


                $userId = $user->id;
                $driver = Driver::where('id',$userId)->first();
                $response['result']['user'] = $user;
                $response['result']['driver'] = $driver;

                // $response['result']['orders']['order_items'] = count($orderItems);
                // $response['result']['orders']['order_seller_products'] = $orderSellerProducts;
            // } else {
            //     $response['error'] = ERRORS['-33029'];
            // }
            }else{
                $response['error'] = __('errors.-32700');
            }
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] = $th->getMessage();
            return response()->json($response);
        }
    }

    public function getAllCity(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            // $user = JWTAuth::toUser();
            $reqData = $request->json()->all();
            // $cityName = $reqData['params']['city_name'];
            $cities = CitiDetails::where('language_id',getLanguage()->id)->select('name');
            $cities = $cities->where(function($q1) use($request) {
                $q1 = $q1->whereNotIn('status',['','D']);
            });
            $cities = $cities->orderBy('name')->groupBy('name')->get();  
            $out_city = OrderMaster::where('shipping_country','!=',134)->select('shipping_city')->groupBy('shipping_city')->get(); 
            $cityNm= [];
            $count = 0;
            foreach(@$cities as $ci){
                $cityNm[$count]['name'] = $ci->name; 
                $count++;
            }
            foreach(@$out_city as $out_c){
                $cityNm[$count]['name'] = $out_c->shipping_city; 
                $count++;
            }         
            
            
            // $cities = array_merge($cities, $out_city);
            // $order = OrderMaster::with('')
            // $cities = 
            // $cities = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
            // $cities = $cities->whereHas('cityDetailsByLanguage',function($query) use($cityName) {
            //     $query->where('name', 'LIKE', "%{$cityName}%");
            // });
            
            $response['result']['cities'] = $cityNm;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] = $th->getMessage();
            return response()->json($response);
        }    
    }

    public function getCity(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = JWTAuth::toUser();
            $reqData = $request->json()->all();
            $cityName = $reqData['params']['city_name'];
            $cities = CitiDetails::where('language_id',getLanguage()->id)->select('city_id','name');
            $cities = $cities->where(function($q1) use($request) {
                    $q1 = $q1->whereNotIn('status',['','D']);
                });
            // $cities = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
            // $cities = $cities->whereHas('cityDetailsByLanguage',function($query) use($cityName) {
            //     $query->where('name', 'LIKE', "%{$cityName}%");
            // });
            $cities = $cities->orderBy('name')->get();            
            $response['result']['cities'] = $cities;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] = $th->getMessage();
            return response()->json($response);
        }    
    }

    private function sendNotificationCustomer($order_id,$userId){
        $androidDrivers = User::where(['status' => 'A', 'device_type' => 'A'])->pluck('firebase_reg_no');

        $iosDrivers = User::where(['id'=>@$userId,'status' => 'A', 'device_type' => 'I'])->pluck('firebase_reg_no');
        $order_dtt = OrderMaster::where('id',$order_id)->first();
        # send notification to android drivers
        if(count($androidDrivers)) {
            $msg                      = array();
            if(@$order_dtt->status == 'N'){
                $msg['title']             = " Order No : ".@$order_dtt->order_no;
                $msg["body"]              = 'Your have successfully placed the order! ';
            }
            if(@$order_dtt->status == 'DA'){
                $msg['title']             = ''." "." Order No : ".@$order_dtt->order_no;
                $msg["body"]              = 'Your order is out for delivery! ';
            }
            if(@$order_dtt->status == 'RP'){
                $msg['title']             = " "." Order No : ".@$order_dtt->order_no;
                $msg["body"]              = 'Your order status is ready to be picked up! ';
            }
            if(@$order_dtt->status == 'OP'){
                $msg['title']             = ''." "." Order No : ".@$order_dtt->order_no;
                $msg["body"]              = 'Your order is picked up! ';
            }
            if(@$order_dtt->status == 'OD'){
                $msg['title']             = ''." "." Order No : ".@$order_dtt->order_no;
                $msg["body"]              = 'Your order Delivered! ';
            }
            

            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            $msg['link_type']         = "Y";
            $msg['is_linked']         = "Y";
            $msg['image']             = '';
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]           = "10";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'registration_ids' => $androidDrivers,
                'data'             => $msg
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            
            // Closing Curl
            curl_close($ch);
        }
        

        # send notification to ios drivers
        if(count($iosDrivers)) {
            $msg                      = array();
            $msg['title']             = "New Order Placed765";
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]            = "10";
            $msg["mutable-content"]   = true;
            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            $msg["body"]              = 'Recently customer placed an order!';

            $data['message'] = "New Order Placed";
            $data['type'] = "1";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'category'                  => "CustomSamplePush",
                'content_available'         => true,
                'mutable_content'           => true,
                'registration_ids'          => $iosDrivers,
                'notification'              => $msg,
                'data'                      => $data
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            // Closing Curl
            curl_close($ch);
        }
    }         
    /**
    * Method: creaditLoyaltyPoint
    * Description: This method is used to credit loyalty point
    * Author: Sanjoy
    */
    public function creaditLoyaltyPoint($order) {
        $setting = Setting::first();
        $loyaltyPoint = $order->order_total*$setting->one_kwd_to_point;
        $totalOrder = $loyaltyPoint*$setting->one_point_to_kwd;
        $this->order_master->whereId($order->id)->increment('loyalty_point_received', $loyaltyPoint);
        User::whereId($order->user_id)->increment('loyalty_balance', $loyaltyPoint);
        User::whereId($order->user_id)->increment('loyalty_total', $loyaltyPoint);

        $addUserReward = new UserReward;
        $addUserReward['user_id']        = $order->user_id;
        $addUserReward['order_id']       = $order->id;
        $addUserReward['earning_points'] = $loyaltyPoint;
        $addUserReward['credit']         = '1';
        $addUserReward->save();
    }
}