<?php

namespace App\Http\Controllers\Api\Modules\PaymentKeyTable;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PaymentKeyTable;
use Illuminate\Validation\Rule;
use JWTFactory;
use JWTAuth;
use Validator;
use Mail;
use Auth;

class PaymentKeyTableController extends Controller
{
    /**
    * @OA\Get(
    * path="/api/show-razorpaykeyandsecretkey",
    * summary="showpaymentkeytable",
    * description="showpaymentkeytable",
    * operationId="showpaymentkeytable",
    * tags={"Alaaddin: showpaymentkeytable"},
    * security={{"bearer_token":{}}},
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function showpaymentkeytable(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $PaymentKeyTable = PaymentKeyTable::first();  
            $response['result']['RAZORPAY_KEY'] = $PaymentKeyTable->RAZORPAY_KEY;
            $response['result']['RAZORPAY_SECRET'] = $PaymentKeyTable->RAZORPAY_SECRET;
            return response()->json($response);
        }
        catch (\Exception $e) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            \Log::error($e->getMessage());
            return response()->json($response);
        }
    }
}
