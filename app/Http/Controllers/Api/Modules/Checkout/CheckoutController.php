<?php

namespace App\Http\Controllers\Api\Modules\Checkout;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// repository
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductVariantDetailRepository;
use App\Repositories\ProductRepository;
use App\Repositories\UserAddressBookRepository;
use App\Repositories\MerchantRepository;

use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\CountryRepository;

use App\Models\Setting;
use App\Models\OrderMaster;
use App\Models\OrderDetail;
use App\Models\ShippingCost;
use App\Models\Product;
use App\Merchant;
use App\User;
use App\Models\Country;
use App\Models\City;
use App\Models\State;
use App\Models\CitiDetails;
use App\Models\UserAddressBook;
use App\Models\ZoneDetail;
use App\Models\ZoneRateDetail;

use JWTFactory;
use JWTAuth;
use Validator;
use Mail;

class CheckoutController extends Controller
{
	protected $cart, $cartDetails, $product, $productVariant, $productVariantDetail, $addressBook, $orderMaster, $orderDetails, $orderSeller, $merchant, $country, $order;
	protected $userId = NULL;
	protected $deviceId = NULL;
	protected $device = NULL;
    /**
	* __construct
	* This method is used to initilize the instance
	* Author: Sanjoy
	*/
	public function __construct(CartMasterRepository $cart,
								CartDetailRepository $cartDetails,
								ProductVariantRepository $productVariant,
								ProductRepository $product,
								ProductVariantDetailRepository $productVariantDetail,
								UserAddressBookRepository $addressBook,
								OrderMasterRepository $orderMaster,
								OrderDetailRepository $orderDetails,
								OrderSellerRepository $orderSeller,
								MerchantRepository $merchant,
								CountryRepository $country
							)
	{
		$this->cart         			= $cart;
		$this->cartDetails  			= $cartDetails;
		$this->productVariant  			= $productVariant;
		$this->productVariantDetail  	= $productVariantDetail;
		$this->product  				= $product;
		$this->addressBook  			= $addressBook;
		$this->orderMaster  			= $orderMaster;
		$this->order 		 			= $orderMaster;
		$this->orderDetails  			= $orderDetails;
		$this->merchant         		= $merchant;
		$this->orderSeller         		= $orderSeller;
		$this->country         			= $country;
	}

    /**
    * Method: getCheckoutData
    * Description: This method is used to get all data to show in checkout page
    * Author: Sanjoy
    */
    /**
    * @OA\Get(
    * path="/api/checkout/{deviceId}",
    * summary="Checkout",
    * description="Checkout",
    * operationId="authLogin",
    * tags={"Alaaddin: Checkout"},
    * security={{"bearer_token":{}}},
  	*      @OA\Parameter(
    *         name="deviceId",
    *         in="path",
    *         description="device id",
    *         required=false,
    *      ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function getCheckoutData(Request $request, $deviceId = NULL) {
    	$response = [
			'jsonrpc' => '2.0'
		];
		try {
			# if no token or device id provided.
			if(!@$deviceId && !@getUserByToken()->id) {
				$response['error']['message'] = __('customer_api.invalid_request');
				return response()->json($response);
			}

			if(@getUserByToken()->id) {
				$this->userId = getUserByToken()->id;
			}
			if(@$reqData['device_id']) {
				$this->deviceId = $reqData['device_id'];
			}
			$setting = Setting::first();
			$response['result']['cart'] = $this->cart;
			if(@getUserByToken()->id){
	            $response['result']['cart'] = $response['result']['cart']->where('user_id', getUserByToken()->id);
			}
			else if(@$deviceId) {
                $response['result']['cart'] = $response['result']['cart']->where('session_id', $deviceId);
			}
			$response['result']['cart'] = $cartMasterData = $response['result']['cart']->first();

			# cart details seller specific
			if(@$cartMasterData) {
				$insurancePrice =  (@$cartMasterData->total * @$setting->insurance) / 100;
				// $response['result']['cart']->insurance_price = (string) number_format(@$insurancePrice,2,'.','');
				$cartDetails = $this->cartDetails->where(['cart_master_id' => $cartMasterData->id])->get();
				@$loadingPrice = 0;
				foreach ($cartDetails as $key => $value) {
					@$loadingPrice += $value->weight * @$setting->loading_unloading_price;
				}
				// $totalWeight = $cartDetails->sum('weight') * $cartDetails->sum('quantity');
				// $loadingPrice =  $totalWeight * @$setting->loading_unloading_price;
				// $response['result']['cart']->loading_price = (string) number_format(@$loadingPrice,2,'.','');
				$response['result']['cart']->subtotal = $response['result']['cart']->total+$response['result']['cart']->insurance_price+$response['result']['cart']->loading_price+$response['result']['cart']->total_seller_commission;
				$cartMasterId = $cartMasterData->id;
				$response['result']['cart']->subtotal = (string) number_format($response['result']['cart']->subtotal,2,'.','');
				$response['result']['sellers'] = $this->cartDetails->with(['sellerInfo.products.productByLanguage:product_id,language_id,title,description'])
					->with(['sellerInfo.products.defaultImage:product_id,image'])
					->with(['sellerInfo.products.productVariantDetails'])
					->with(['sellerInfo.products.getProduct.productUnitMasters'])
					->with(['sellerInfo.products' => function($query) use($cartMasterId){
						$query->where('cart_master_id', $cartMasterId);
					}])
					->with(['sellerInfo:id,fname,lname,slug,payment_mode,international_order,country'])
					->whereHas('sellerInfo.products', function($q) use($cartMasterId){
						$q->where('cart_master_id', $cartMasterId);
					})
					->where(['cart_master_id' => $cartMasterData->id])
					->groupBy('seller_id')
					->get();
				
				// $checkOutsideMerchant = 'N';
				// foreach ($response['result']['sellers']as $sell) {
				// 	if(@$sell->sellerInfo->country != 134){
				// 		$checkOutsideMerchant = 'Y';
				// 	}
				// }
				# if user login then send save address
				if(@$this->userId){
					$response['result']['addresses'] = $this->addressBook->with(['getCountry:id,country_id,name','city_details','state_details'])
																		->where(['user_id' => $this->userId])
																		->get();
				}

				$response['result']['countries'] = $this->country->with('countryDetailsBylanguage')->where('status','!=','D')->get();
				// $response['result']['merchantOutsideKuwait'] = $checkOutsideMerchant;
				$response['result']['cities'] = City::where(['status' => 'A'])->get();
				$response['result']['state'] = State::get();
			}
			return response()->json($response);
		} catch(\Exception $e) {
			// $response['error']['message'] = $e->getMessage();
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
			return response()->json($response);
		}
    }

    /**
    * Method: submitCheckout
    * Description: This method is used to save user information for guest user and save shipping and billing address.
    * Author: Sanjoy
    */
    /**
    * @OA\Post(
    * path="/api/checkout",
    * summary="Checkout",
    * description="Checkout",
    * operationId="authLogin",
    * tags={"Alaaddin: Checkout"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Checkout",
    *    @OA\JsonContent(
    *       required={"fname","lname","phone","shipping_address_id","shipping_fname","shipping_lname","shipping_phone","shipping_country","shipping_city","shipping_state","shipping_postal_code","location","lat","lng","shipping_more_address","billing_address_id","billing_fname","billing_lname","billing_phone","billing_country","billing_city","billing_state","billing_postal_code","billing_more_address","shipping_nearest_landmark","billing_nearest_landmark"},
    *           @OA\Property(property="params", type="object",
    
    
    *           @OA\Property(property="fname", type="string",  example="Test"),
    *           @OA\Property(property="lname", type="string",  example="testlastname"),
    *           @OA\Property(property="phone", type="integer", example="1234323433"),
    *           @OA\Property(property="shipping_address_id", type="integer", format="shipping_address_id", example=""),
    *           @OA\Property(property="shipping_nearest_landmark", type="string", example="abc sdfs"),
    *           @OA\Property(property="billing_nearest_landmark", type="string", example="nsdjhf neavdahgsvdhgrest"),
    *           @OA\Property(property="shipping_fname", type="string",  example="shipping_fname"),
    *           @OA\Property(property="shipping_lname", type="string",  example="shipping_lname"),
    *           @OA\Property(property="shipping_phone", type="string",  example="9089876765"),
    *           @OA\Property(property="shipping_country", type="string", example="101"),
    *           @OA\Property(property="shipping_state", type="string", example="24"),
    *           @OA\Property(property="shipping_city", type="string", example="601"),
    *           @OA\Property(property="shipping_postal_code", type="string", example="700137"),
    *           @OA\Property(property="location", type="string", format="location", example="location"),
    *           @OA\Property(property="lat", type="string", example="lat"),
    *           @OA\Property(property="lng", type="string", example="lng"),
    *           @OA\Property(property="shipping_more_address", type="string", example="shipping_more_address"),
    *           @OA\Property(property="billing_address_id", type="integer", example=""),
    *           @OA\Property(property="billing_fname", type="string", example="billing_fname"),
    *           @OA\Property(property="billing_lname", type="string", example="billing_lname"),
    *           @OA\Property(property="billing_phone", type="string", example="1234567898"),
    *           @OA\Property(property="billing_country", type="string", example="101"),
    *           @OA\Property(property="billing_state", type="string", example="24"),
    *           @OA\Property(property="billing_city", type="string", example="600"),
    *           @OA\Property(property="billing_postal_code", type="string", example="700138"),
    *           @OA\Property(property="billing_more_address", type="string", example="billing_more_address"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function submitCheckout(Request $request) {
    	$response = [
    		'jsonrpc' => '2.0'
    	];
    	// try{
    	$cart = [];
		$reqData = $request->json('params');
		$reqdata = $request->json()->all();
				
	            $vUserAddressBook = UserAddressBook::where(['id'=>@$reqdata['params']['shipping_address_id']])->first();
	            if(!@$vUserAddressBook){
		            if(@$reqdata['params']['shipping_address_id'] == "" || @$reqdata['params']['shipping_address_id'] == null || !@$reqdata['params']['shipping_address_id'] || @$reqdata['params']['shipping_address_id'] == "0"){
		            	$validator = Validator::make($reqdata['params'],[
			                'shipping_fname'   => 'required',
			                'shipping_lname'   => 'required',
			                'shipping_phone'   => 'required|numeric|digits_between:10,12',
			                'shipping_country'   => 'required|numeric',
			                'shipping_state'   => 'required|numeric',
			                'shipping_city'   => 'required|numeric',
			                // 'shipping_nearest_landmark'   => 'required',
			            ],
			            [
			                'shipping_fname.required'=>'shipping_fname required',
			                'shipping_lname.required'=>'shipping_lname required',
			                'shipping_phone.required'=>'shipping_phone required',
			                'shipping_country.required'=>'shipping_country required',
			                'shipping_state.required'=>'shipping_state required',
			                'shipping_city.required'=>'shipping_city required',
			                'shipping_country.numeric'=>'provide shipping_country id',
			                'shipping_state.numeric'=>'provide shipping_state id',
			                'shipping_city.numeric'=>'provide shipping_city id',
			                // 'shipping_nearest_landmark.required'=>' shipping_nearest_landmark required',
			            ]);
			            if($validator->fails()){
			                 $response['error'] =__('errors.-5001');
			                $response['error']['message'] = "Error";
			                $response['error']['meaning'] = $validator->errors()->first();
			                return response()->json($response);
			            }
						if(@$reqdata['params']['shipping_country']){
							$iscountryV = Country::where('id',@$reqdata['params']['shipping_country'])->first();
				            if(!@$iscountryV){
				                $response['error'] =__('errors.-5001');
				                $response['error']['meaning'] = "You have provided wrong shipping_country id: ".@$reqdata['params']['shipping_country'];
				                return response()->json($response);    
				            }
			        	}
			            if(@$reqdata['params']['shipping_state']){
				            $isstatev = State::where('id',@$reqdata['params']['shipping_state'])->first();
				            if(!@$isstatev){
				                $response['error'] =__('errors.-5001');
				                $response['error']['meaning'] = "You have provided wrong shipping_state id: ".@$reqdata['params']['shipping_state'];
				                return response()->json($response);    
				            }
			            }
			            if(@$reqdata['params']['shipping_city']){
				            $iscity = City::where(['id'=>@$reqdata['params']['shipping_city']])->first();
				            if(!@$iscity){
				                $response['error'] =__('errors.-5001');
				                $response['error']['meaning'] = "You have provided wrong shipping_city id: ".@$reqdata['params']['shipping_city']." or wrong combination of shipping_city shipping_city id!";
				                return response()->json($response);    
				            }
			        	}
		            }else{
		            	$vUserAddressBook = UserAddressBook::where(['id'=>@$reqdata['params']['shipping_address_id']])->first();
	    				if(!@$vUserAddressBook){
	    					$response['error'] =__('errors.-5001');
			                $response['error']['meaning'] = "You have provided wrong shipping_address_id: ".@$reqdata['params']['shipping_address_id'];
				                return response()->json($response);    
	    				}
		            }
	            }
	            if(@$reqdata['params']['billing_nearest_landmark'] == "" || @$reqdata['params']['billing_nearest_landmark'] == null || !@$reqdata['params']['billing_nearest_landmark']){
	            	$vUserAddressBook = UserAddressBook::where(['id'=>@$reqdata['params']['shipping_address_id']])->first();
	            	if(@$vUserAddressBook){
	            		$reqdata['params']['billing_nearest_landmark'] = @$vUserAddressBook->location;
	            	}
	            }
	            if(@$reqdata['params']['billing_address_id'] == "" || @$reqdata['params']['billing_address_id'] == null || !@$reqdata['params']['billing_address_id'] || @$reqdata['params']['billing_address_id'] == "0"){
	            	$validator = Validator::make($reqdata['params'],[
		                'billing_fname'   => 'required',
		                'billing_lname'   => 'required',
		                'billing_phone'   => 'required|numeric|digits_between:10,12',
		                'billing_country'   => 'required|numeric',
		                'billing_state'   => 'required|numeric',
		                'billing_city'   => 'required|numeric',
		                // 'billing_nearest_landmark'   => 'required',
		            ],
		            [
		                'billing_fname.required'=>'billing_fname required',
		                'billing_lname.required'=>'billing_lname required',
		                'billing_phone.required'=>'billing_phone required',
		                'billing_country.required'=>'billing_country required',
		                'billing_state.required'=>'billing_state required',
		                'billing_city.required'=>'billing_city required',
		                'billing_country.numeric'=>'provide billing_country id',
		                'billing_state.numeric'=>'provide billing_state id',
		                'billing_city.numeric'=>'provide billing_city id',
		                // 'billing_nearest_landmark.required'=>' billing_nearest_landmark required',
		            ]);
		            if($validator->fails()){
		                 $response['error'] =__('errors.-5001');
		                $response['error']['message'] = "Error";
		                $response['error']['meaning'] = $validator->errors()->first();
		                return response()->json($response);
		            }
					if(@$reqdata['params']['billing_country']){
						$iscountryV = Country::where('id',@$reqdata['params']['billing_country'])->first();
			            if(!@$iscountryV){
			                $response['error'] =__('errors.-5001');
			                $response['error']['meaning'] = "You have provided wrong billing_country id: ".@$reqdata['params']['billing_country'];
			                return response()->json($response);    
			            }
		        	}
		            if(@$reqdata['params']['billing_state']){
			            $isstatev = State::where('id',@$reqdata['params']['billing_state'])->first();
			            if(!@$isstatev){
			                $response['error'] =__('errors.-5001');
			                $response['error']['meaning'] = "You have provided wrong billing_state id: ".@$reqdata['params']['billing_state'];
			                return response()->json($response);    
			            }
		            }
		            if(@$reqdata['params']['billing_city']){
			            $iscity = City::where(['id'=>@$reqdata['params']['billing_city']])->first();
			            if(!@$iscity){
			                $response['error'] =__('errors.-5001');
			                $response['error']['meaning'] = "You have provided wrong billing_city id: ".@$reqdata['params']['billing_city']." or wrong combination of billing_city !";
			                return response()->json($response);    
			            }
		        	}
	            }else{
	            	$vUserAddressBook = UserAddressBook::where(['id'=>@$reqdata['params']['billing_address_id']])->first();
    				if(!@$vUserAddressBook){
    					$response['error'] =__('errors.-5001');
		                $response['error']['meaning'] = "You have provided wrong billing_address_id: ".@$reqdata['params']['billing_address_id'];
			                return response()->json($response);    
    				}
	            }
		        
		# check user request as guest or registered user
		# if no token or device id provided.
		if(!@$reqData['device_id'] && !@getUserByToken()->id) {
			$response['error']['message'] = __('customer_api.invalid_request');
			return response()->json($response);
		}

		if(@getUserByToken()->id) {
			$this->userId = getUserByToken()->id;
			$cart = $this->cart->where(['user_id' => $this->userId])->first();
		}
		if(@$reqData['device_id']) {
			$this->deviceId = $reqData['device_id'];
			$cart = $this->cart->where(['session_id' => $this->deviceId])->first();
		}else{
			$this->deviceId = "A";
		}

		if(!@$cart) {
			$response['error']['message'] = __('customer_api.invalid_request');
			return response()->json($response);
		}
		# assigning device type to device variable
		if(@$request->header('X-device')) {
			$this->device = $request->header('X-device');
		}else{
			$this->device = "A";
		}
		# checking if seller id is out side kuwait
		$sellers = $this->cartDetails->where(['cart_master_id' => $cart->id])->groupBy('seller_id')->pluck('seller_id');
    	if(@$sellers) {
			foreach ($sellers as $value) {
				$merchantId[] = $value;
			}
			$merchantCn = $this->merchant->whereIn('id', $merchantId)->get();
			$mrchCount = $merchantCn->count();
		}
		$userAddress = [];
		$shippingCountry = 0;
		if(@$reqData['shipping_address_id']) {
			$userAddress = UserAddressBook::where(['id' => $reqData['shipping_address_id']])->first();
			if(!@$userAddress){
				$response['error'] = ERRORS['-33000'];
				$response['error']['meaning'] = "You have provided wrong address book id!";
                return response()->json($response);
			}else{

			$shippingCountry = $userAddress->country;

			}
		} else {
			// $shippingCountry = $reqData['shipping_country'];
		}

		// if(@$mrchCount > 0 && @$shippingCountry != '134') {
		// 	$response['error'] = __('success_user.-709');
		// 	return response()->json($response);
		// }
		// else if(@$mrchCount > 0 && @$reqData['shipping_address_id'] == '' && @$reqData['shipping_country'] != '134') {
		// 	$response['error'] = __('success_user.-709');
		// 	return response()->json($response);
		// } else {
			// if(@$mrchCount > 0 || $shippingCountry != '134') {
			// 	$setting = Setting::first();
			// 	if($setting->enable_international_order == 'Y') {
			// 		foreach ($sellers as $value) {
			// 			$merchantId[] = $value;
			// 		}
			// 		$merchantDetail = $this->merchant->whereIn('id', $merchantId)->where('international_order', 'OFF')->get();
			// 		$mrchCn = $merchantDetail->count();
			// 		if(@$mrchCn > 0) {
			// 			$mrs = $merchantDetail->pluck('fname')->toArray();
			// 			$response['error'] = __('success_user.-723', ['mrs' => implode(',', $mrs)]);
			// 			return response()->json($response);
			// 		} else {
			// 			//insert the orders into multiple tables
			// 			return $this->createCheckout($request);
			// 		}
			// 	} else {
			// 		$response['error'] = __('success_user.-711');
			// 		return response()->json($response);
			// 	}
			// } else {
				//insert the orders into multiple tables
				return $this->createCheckout($request);
			// }
		// }
				// }
				// catch(\Exception $e){
				// 	$response['error'] = __('errors.-5001');
    //         $response['error']['meaning'] = $e->getMessage();
    //         return response()->json($response);
				// }
    }

	 /**
    * Method: submitCheckout
    * Description: This method is used to save user information for guest user and save shipping and billing address.
    * Author: Sanjoy
    */
    public function submitCheckout1(Request $request) {
    	$response = [
    		'jsonrpc' => '2.0'
    	];
    	$cart = [];
		$reqData = $request->json('params');
		# check user request as guest or registered user
		# if no token or device id provided.
		if(!@$reqData['device_id'] && !@getUserByToken()->id) {
			$response['error']['message'] = __('customer_api.invalid_request');
			return response()->json($response);
		}

		if(@getUserByToken()->id) {
			$this->userId = getUserByToken()->id;
			$cart = $this->cart->where(['user_id' => $this->userId])->first();
		}
		if(@$reqData['device_id']) {
			$this->deviceId = $reqData['device_id'];
			$cart = $this->cart->where(['session_id' => $this->deviceId])->first();
		}

		if(!@$cart) {
			$response['error']['message'] = __('customer_api.invalid_request');
			return response()->json($response);
		}
		# assigning device type to device variable
		if(@$request->header('X-device')) {
			$this->device = $request->header('X-device');
		}
		else{
			$this->device = 'A';
		}
		# checking if seller id is out side kuwait
		$sellers = $this->cartDetails->where(['cart_master_id' => $cart->id])->groupBy('seller_id')->pluck('seller_id');
    	if(@$sellers) {
			foreach ($sellers as $value) {
				$merchantId[] = $value;
			}
			$merchantCn = $this->merchant->whereIn('id', $merchantId)
			->where('country', '!=', '134')->get();
			$mrchCount = $merchantCn->count();
		}
		$userAddress = [];
		$shippingCountry = 0;
		if(@$reqData['shipping_address_id']) {
			$userAddress = UserAddressBook::where(['id' => $reqData['shipping_address_id']])->first();
			$shippingCountry = $userAddress->country;
		} else {
			$shippingCountry = $reqData['shipping_country'];
		}

		if(@$mrchCount > 0 && @$shippingCountry != '134') {
			$response['error'] = __('success_user.-709');
			return response()->json($response);
		}
		else if(@$mrchCount > 0 && @$reqData['shipping_address_id'] == '' && @$reqData['shipping_country'] != '134') {
			$response['error'] = __('success_user.-709');
			return response()->json($response);
		} else {
			if(@$mrchCount > 0 || $shippingCountry != '134') {
				$setting = Setting::first();
				if($setting->enable_international_order == 'Y') {
					foreach ($sellers as $value) {
						$merchantId[] = $value;
					}
					$merchantDetail = $this->merchant->whereIn('id', $merchantId)->where('international_order', 'OFF')->get();
					$mrchCn = $merchantDetail->count();
					if(@$mrchCn > 0) {
						$mrs = $merchantDetail->pluck('fname')->toArray();
						$response['error'] = __('success_user.-723', ['mrs' => implode(',', $mrs)]);
						return response()->json($response);
					} else {
						//insert the orders into multiple tables
						return $this->createCheckout($request);
					}
				} else {
					$response['error'] = __('success_user.-711');
					return response()->json($response);
				}
			} else {
				//insert the orders into multiple tables
				return $this->createCheckout1($request);
			}
		}
    }
    /**
    * Method: createCheckout
    * Description: This method is used to create checkout
    * Author: Sanjoy
    */
    private function createCheckout($request) {
    	try {
    		$user = [];
    		$cart = [];
    		$reqData = $request->json('params');
    		# check user request as guest or registered user
    		# if no token or device id provided.
			if(!@$reqData['device_id'] && !@getUserByToken()->id) {
				$response['error']['message'] = __('customer_api.invalid_request');
				return response()->json($response);
			}

			if(@getUserByToken()->id) {
				$this->userId = getUserByToken()->id;
				$cart = $this->cart->where(['user_id' => $this->userId])->first();
			}
			if(@$reqData['device_id']) {
				$this->deviceId = $reqData['device_id'];
				$cart = $this->cart->where(['session_id' => $this->deviceId])->first();
			}

			if(!@$cart) {
				$response['error']['message'] = __('customer_api.invalid_request');
				return response()->json($response);
			}

			# insert to user table for guest user.
			if(!@$this->userId) {
				$user = $this->createGuestUser($reqData);
				$response['result']['token'] = JWTAuth::fromUser($user);
			} else {
				$user = User::where(['id' => $this->userId])->first();
			}

			# if request for edit order then delete all order details and order seller
			if(@$reqData['order_id']) {
				$this->deleteOrderData($reqData['order_id']);
			}

			# insert to order master table
			$order = $this->insertOrderMaster($reqData, $user);

			# insert to order details table
			$this->insertOrderDetails($cart, $order->id);

			# checking for product availibility
			$orderDetail = OrderDetail::where(['order_master_id' => $order->id])->first();
			if(!@$orderDetail) {
				$this->deleteOrderData($order->id);
				OrderMaster::where(['id' => $order->id])->delete();
				$response['result']['out_of_stock'] = __('customer_api.product_out_of_stock');
				return response()->json($response);
			}

			$this->updateOrderMasterPrice($order->id);

			# calculate shipping price
			// $shipping = $this->calculateShippingPrice($order);

			# insert to order seller table
			$sellerProducts = $this->insertOrderSeller($order->id);
			#customer
            // if(@$order->order_type == 'I'){
            //     $this->sendNotificationCustomer(@$order->id,@$order->user_id);
            // }

            # update is_more_merchant or not
			if($sellerProducts > 1) {
				$update['is_more_seller'] = 'Y';
				$this->orderMaster->whereId($order->id)->update($update);
			}
			$updateTotItem['total_item'] = OrderDetail::where(['order_master_id'=>$order->id])->count();
			$this->orderMaster->whereId($order->id)->update($updateTotItem);

			$response['result']['order'] = $this->orderMaster->with(['orderMasterDetails'])->where(['id' => $order->id])->first();

			return response()->json($response);
    	} catch(\Exception $e) {
    		// $response['error']['message'] = $e->getMessage();
    		$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
    		return response()->json($response);
    	}
    }

	private function createCheckout1($request) {
    	// try {
    		$user = [];
    		$cart = [];
    		$reqData = $request->json('params');
    		# check user request as guest or registered user
    		# if no token or device id provided.
			if(!@$reqData['device_id'] && !@getUserByToken()->id) {
				$response['error']['message'] = __('customer_api.invalid_request');
				return response()->json($response);
			}

			if(@getUserByToken()->id) {
				$this->userId = getUserByToken()->id;
				$cart = $this->cart->where(['user_id' => $this->userId])->first();
			}
			if(@$reqData['device_id']) {
				$this->deviceId = $reqData['device_id'];
				$cart = $this->cart->where(['session_id' => $this->deviceId])->first();
			}
			
			if(!@$cart) {
				$response['error']['message'] = __('customer_api.invalid_request');
				return response()->json($response);
			}

			# insert to user table for guest user.
			if(!@$this->userId) {
				$user = $this->createGuestUser($reqData);
				$response['result']['token'] = JWTAuth::fromUser($user);
			} else {
				$user = User::where(['id' => $this->userId])->first();
			}

			# if request for edit order then delete all order details and order seller
			if(@$reqData['order_id']) {
				$this->deleteOrderData($reqData['order_id']);
			}

			# insert to order master table
			$order = $this->insertOrderMaster($reqData, $user);
			# insert to order details table
			$this->insertOrderDetails1($cart, $order->id);

			# checking for product availibility
			$orderDetail = OrderDetail::where(['order_master_id' => $order->id])->first();
			if(!@$orderDetail) {
				$this->deleteOrderData($order->id);
				OrderMaster::where(['id' => $order->id])->delete();
				$response['result']['out_of_stock'] = __('customer_api.product_out_of_stock');
				return response()->json($response);
			}

			$this->updateOrderMasterPrice($order->id);

			# calculate shipping price
			// $shipping = $this->calculateShippingPrice($order);

			# insert to order seller table
			$sellerProducts = $this->insertOrderSeller($order->id);
			#customer
            // if(@$order->order_type == 'I'){
            //     $this->sendNotificationCustomer(@$order->id,@$order->user_id);
            // }

            # update is_more_merchant or not
			if($sellerProducts > 1) {
				$update['is_more_seller'] = 'Y';
				$this->orderMaster->whereId($order->id)->update($update);
			}
			$response['result']['order'] = $this->orderMaster->with(['orderMasterDetails'])->where(['id' => $order->id])->first();

			return response()->json($response);
    	/*} catch(\Exception $e) {
    		$response['error']['message'] = $e->getMessage();
    		return response()->json($response);
    	}*/
    }
    /**
    * Method: deleteOrderData
    * Description: This method is used to delete order details and order seller
    * Author: Sanjoy
    */
    private function deleteOrderData($orderId) {
    	$this->orderDetails->where(['order_master_id' => $orderId])->delete();
    	$this->orderSeller->where(['order_master_id' => $orderId])->delete();
    }

    /**
    * Method: createGuestUser
    * Description: This method is used to create guest user
    * Author: Sanjoy
    */
    private function createGuestUser($request) {
    	$user = User::where(['email' => $request['email'], 'user_type' => 'G'])->first();
    	if(@$user) {
    		return $user;
    	} else {
    		$insert['fname']     = $request['fname'];
			$insert['lname']     = $request['lname'];
			$insert['email']     = @$request['email'];
			$insert['user_type'] = 'G'; // guest
			return $user = User::create($insert);
    	}
    }

    /**
	* Method: insertOrderMaster
	* Description: This method is used to insert order master table.
	* Author: Sanjoy
	*/
	private function insertOrderMaster($request, $user) {
		# creating order if request for new order
		if(!@$request['order_id']) {
			// $request['fname'] = @$user->fname;
			// $request['lname'] = @$user->lname;
			// $request['email'] = @$user->email;
			// $request['phone'] = @$user->phone;
			if(@$request['fname']){
			$orderData = $this->orderMaster->create([
				'user_id' 			=> $user->id,
				'fname' 			=> @$request['fname'],
				'lname' 			=> @$request['lname'],
				'email' 			=> @$request['email'],
				'phone' 			=> @$request['phone'],
				'user_id' 			=> $user->id,
				'order_type' 		=> 'I',
				'status' 			=> 'I',
				'verify_code'		=> time().rand(111, 999),
				'payment_method' 	=> @$request['payment_method'],
				'order_from'		=> !empty($this->device) ? $this->device : 'A',
				'expected_delivery_date'		=> date('Y-m-d', strtotime("+7 days")),
			]);
		}else{
			$orderData = $this->orderMaster->create([
				'user_id' 			=> $user->id,
				'fname' 			=> @$user->fname,
				'lname' 			=> @$user->lname,
				'email' 			=> @$user->email,
				'phone' 			=> @$user->phone,
				'user_id' 			=> @$user->id,
				'order_type' 		=> 'I',
				'status' 			=> 'I',
				'verify_code'		=> time().rand(111, 999),
				'payment_method' 	=> @$request['payment_method'],
				'order_from'		=> !empty($this->device) ? $this->device : 'A',
				'expected_delivery_date'		=> date('Y-m-d', strtotime("+7 days")),
			]);
		}
			# generate order number
			$ordId = sprintf('%08d', $orderData->id);
			$orderNumber = "ORDALD".$ordId;
			$this->orderMaster->whereId($orderData->id)->update(['order_no' => $orderNumber]);
		} else {
			$this->orderMaster->whereId($request['order_id'])->update(['user_id' => $user->id]);
			$orderData = $this->orderMaster->where('id', $request['order_id'])->first();
		}
		
		# insert shipping data
		$this->insertShippingData($request, $orderData->id);

		# insert billing data
		$this->insertBillingData($request, $orderData->id);
		return $this->orderMaster->whereId($orderData->id)->first();
	}

	/**
	* Method: insertOrderDetails
	* Description: This method is used to insert to order details table.
	* Author: Sanjoy
	*/
	private function insertOrderDetails($cart, $orderId) {
		$cartDetails = $this->cartDetails->where(['cart_master_id' => $cart->id])->get();
		foreach ($cartDetails as $row) {
			$orderDetails = $this->checkAndInsertToDetails($row, $orderId);
			// if(!@$orderDetails) {
			// 	continue;
			// }
			$p = Product::where(['id'=>$row['product_id']])->first();
			$orderDet = array();
			$orderDet['order_master_id']   	=   $orderId;
			$orderDet['product_id']   		=   $row['product_id'];
			$orderDet['weight']   		=   $row['weight'];
			$orderDet['product_unit_master_id']   		=   $p->unit_master_id;
			$orderDet['seller_id']   		=   $row['seller_id'];
			$orderDet['quantity']   		=   $row['quantity'];
			$orderDet['status']   			=   'N';
			$orderDet['product_note']   	=   @$row['product_note'];
			$orderDet['seller_commission']   	=   @$row['seller_commission'];
			$orderDet['sub_total']   	=   @$row['subtotal'];
			$orderDet['total']   	=   @$row['total'];
			$orderDet['loading_price']   	=   @$row['loading_price'];
			$orderDet['insurance_price']   	=   @$row['insurance_price'];
			$orderDet['payable_amount']   	=   @$row['payable_amount'];
			if($row['product_variant_id']) {
				$orderDet['product_variant_id'] =   $row['product_variant_id'];
				$orderDet['variants']   		=   $row['variants'];
			}
			$orderDet['expected_delivery_date']   	=   date('Y-m-d', strtotime("+7 days"));
			$orderDet['expected_delivery_time']   	=   date('H:i:s');
			$orderDetailsIns = $this->orderDetails->where(['id' => $orderDetails->id])->update($orderDet);
		}
	}

	private function insertOrderDetails1($cart, $orderId) {
		$cartDetails = $this->cartDetails->where(['cart_master_id' => $cart->id])->get();
		//dd($cartDetails);
		foreach ($cartDetails as $row) {
			$orderDetails = $this->checkAndInsertToDetails1($row, $orderId);
			if(!@$orderDetails) {
				continue;
			}
			$orderDet = array();
			$orderDet['order_master_id']   	=   $orderId;
			$orderDet['product_id']   		=   $row['product_id'];
			$orderDet['seller_id']   		=   $row['seller_id'];
			$orderDet['quantity']   		=   $row['quantity'];
			$orderDet['status']   			=   'N';
			$orderDet['product_note']   	=   @$row['product_note'];
			if($row['product_variant_id']) {
				$orderDet['product_variant_id'] =   $row['product_variant_id'];
				$orderDet['variants']   		=   $row['variants'];
			}
			$orderDetailsIns = $this->orderDetails->where(['id' => $orderDetails->id])->update($orderDet);
		}
	}

	private function checkAndInsertToDetails1($cartDetails, $orderId) {
		$product = $this->product->whereId($cartDetails->product_id)->first();
		if(@$cartDetails->product_variant_id) {
			$productVariant = $this->checkStockAndVariant($cartDetails->product_variant_id);
			if(!@$productVariant || @$productVariant->stock >= $cartDetails->quantity) {
				return 0;
			}
		} else {
			if(@$product->stock < $cartDetails->quantity) {
				return 0;
			}
		}
		$originalPrice = 0;
		$discountedPrice = 0;
		$total_discount = 0;
		$subtotal = 0;
		$total = 0;
		$weight = 0;
		if(@$productVariant->price != 0) {
			
			#if discount price is available then calculate this.
			$subtotal = $total = $productVariant->price * $cartDetails->quantity;
			$originalPrice = $productVariant->price;
			if($productVariant->discount_price != 0 && date('Y-m-d')>=$productVariant->from_date && date('Y-m-d')<=$productVariant->to_date) {
				$total_discount = ($productVariant->price - $productVariant->discount_price) * $cartDetails->quantity;
				$discountedPrice = $productVariant->discount_price;
			} else {
				$total_discount = 0.000;
			}
			$total = $subtotal - $total_discount;
			$weight = $productVariant->weight * $cartDetails->quantity;
		} else {
			
			$originalPrice = $product->price;
			$discountedPrice = $product->discount_price;
			if($product->discount_price != 0 && date('Y-m-d')>=$product->from_date && date('Y-m-d')<=$product->to_date) {
				$total_discount = ($product->price - $product->discount_price) * $cartDetails->quantity;
			} else {
				$total_discount = 0.000;
			}
			$subtotal 	= $product->price * $cartDetails->quantity;
			$total 		= $subtotal - $total_discount;	
			$weight 	= $product->weight * $cartDetails->quantity;
		}
		//dump($originalPrice,$discountedPrice,$subtotal,$total,$weight,$orderId);
		return $this->orderDetails->create([
			'original_price'	=> $originalPrice,
			'discounted_price' 	=> $discountedPrice,
			'sub_total'			=> $subtotal,
			'total'				=> $total,
			'weight'			=> $weight,
			'order_master_id'	=> $orderId
		]);
	}
	/**
	* Method: updateOrderDetails
	* Description: This method is used to check stock and then update order details 
	* Author: Sanjoy
	*/
	private function checkAndInsertToDetails($cartDetails, $orderId) {
		$product = $this->product->whereId($cartDetails->product_id)->first();
		$setting = Setting::first();
		// if(@$cartDetails->product_variant_id) {
		// 	$productVariant = $this->checkStockAndVariant($cartDetails->product_variant_id);
		// 	if(!@$productVariant || @$productVariant->stock >= $cartDetails->quantity) {
		// 		return 0;
		// 	}
		// } else {
		// 	if(@$product->stock < $cartDetails->quantity) {
		// 		return 0;
		// 	}
		// }
		$originalPrice = 0;
		$discountedPrice = 0;
		$total_discount = 0;
		$subtotal = 0;
		$total = 0;
		$weight = 0;
		// if(@$productVariant->price != 0) {
			
		// 	#if discount price is available then calculate this.
		// 	$subtotal = $total = $productVariant->price * $cartDetails->quantity;
		// 	$originalPrice = $productVariant->price;
		// 	if($productVariant->discount_price != 0  && date('Y-m-d')>=$productVariant->from_date && date('Y-m-d')<=$productVariant->to_date) {
		// 		$total_discount = ($productVariant->price - $productVariant->discount_price) * $cartDetails->quantity;
		// 		$discountedPrice = $productVariant->discount_price;
		// 	} else {
		// 		$total_discount = 0.000;
		// 	}
		// 	$total = $subtotal - $total_discount;
		// 	$weight = $productVariant->weight * $cartDetails->quantity;
		// } else {
			
			$originalPrice = $product->price;
			$discountedPrice = $product->discount_price;
			// if($product->discount_price != 0 && date('Y-m-d')>=$product->from_date && date('Y-m-d')<=$product->to_date) {
			if($product->discount_price != 0) {
				$total_discount = ($product->price - $product->discount_price) * $cartDetails->quantity;
			} else {
				$total_discount = 0.000;
			}
			$subtotal 	= $product->price * $cartDetails->quantity;
			$total 		= $subtotal - $total_discount;	
			$weight 	=  $cartDetails->quantity;
			$setting = Setting::first();
			$loadingPrice =  $weight * @$setting->loading_unloading_price;
			$insurance_price =  $total * @$setting->insurance;
			$seller = Merchant::whereId($product->user_id)->first();
			// if(!empty($seller)){
				// if(@$seller->commission > 0)
				$totalCom = ($total * @$setting->seller_commission) / 100;
			// }
			$payable_amount =  $total +$insurance_price+$loadingPrice+$totalCom;
			$adminCom = ($total * @$seller->commission) / 100;
		// }
		return $this->orderDetails->create([
			'original_price'	=> $originalPrice,
			'discounted_price' 	=> $discountedPrice,
			'sub_total'			=> $subtotal,
			'total'				=> $total,
			'weight'			=> $weight,
			'order_master_id'	=> $orderId,
			'loading_price' 	=> $loadingPrice,
			'insurance_price' 	=> $insurance_price,
			'payable_amount' 	=> $payable_amount,
			'admin_commission'	=> @$adminCom,
		]);
	}

	
	/**
	* Method: calculateShippingPrice
	* Description: This method is used to calculate shipping price
	* Author: Sanjoy
	*/
	private function calculateShippingPrice1($orderMaster) {
		$sellers = $this->orderDetails->where(['order_master_id' => $orderMaster->id])->pluck('seller_id')->toArray();

		$uniqueSellers = array_unique($sellers);
		$setting = Setting::first();
		$insideShippingCost = 0;
		$outsideShippingCost = 0;
		$merHgCst 	= [];
		foreach ($uniqueSellers as $key => $value) {
			$merchant = Merchant::where(['id' => $value])->first();
			if($merchant->country == 134 && $orderMaster->shipping_country == 134) {
				$merHgCst[] = @$merchant->inside_shipping_cost;
				
			} else {
				$details = $this->orderDetails->where(['order_master_id' => $orderMaster->id, 'seller_id' => $value])->get();
				$weight = $details->sum('weight');

				$infinityChk = ShippingCost::where('infinity_weight', 'Y')->first();
				if($infinityChk && @$weight >= $infinityChk->from_weight) {
					$sCost = $infinityChk;
				} else {
					$sCost = ShippingCost::where(function($where) use ($weight) {
						$where->where(function($where1) use ($weight) {
							$where1->where('from_weight', '<=', @$weight)
							->where('to_weight', '>', @$weight);

						})
						->orWhere(function($where2) use ($weight) {
							$where2->where('from_weight', '<', @$weight)
							->where('to_weight', '>=', @$weight);
						});
					})
					->first();
				}

				$outsideShippingCost += @$sCost->internal_order_rate;
			}
		}
		if(count($merHgCst)) {
			$insideShippingCost = max($merHgCst);
		}
		$orderDet = $this->orderMaster->where(['id' => $orderMaster->id])->first();
		$this->orderMaster->where(['id' => $orderMaster->id])->update(['shipping_price' => 0, 'order_total' => $orderDet->order_total - $orderDet->shipping_price]);

		
		$orderDet = $this->orderMaster->where(['id' => $orderMaster->id])->first();
		$this->orderMaster->where(['id' => $orderMaster->id])->update(['shipping_price' => $outsideShippingCost + $insideShippingCost, 'order_total' => $orderDet->order_total + $outsideShippingCost + $insideShippingCost]);
	}

	/**
	* Method: calculateShippingPrice
	* Description: This method is used to calculate shipping price
	* Author: Sanjoy
	*/
	private function calculateShippingPrice($orderMaster) {
		$sellers = $this->orderDetails->where(['order_master_id' => $orderMaster->id])->pluck('seller_id')->toArray();

		$uniqueSellers = array_unique($sellers);
		$setting = Setting::first();
		$insideShippingCost = 0;
		$outsideShippingCost = 0;
		$merHgCst 	= [];
		foreach ($uniqueSellers as $key => $value) {
			$merchant = Merchant::where(['id' => $value])->first();
			if($merchant->country == 134 && $orderMaster->shipping_country == 134) {
				# if city delivery applicable for merchant
				if($merchant->applicable_city_delivery == 'Y') {
					$cityDelivery = City::where(['id' => $orderMaster->shipping_city_id])->first();
					if(@$cityDelivery->delivery_fees > $merchant->inside_shipping_cost) {
						$merHgCst[] = @$cityDelivery->delivery_fees;
					} else {
						$merHgCst[] = @$merchant->inside_shipping_cost;
					}
				} else {
					$merHgCst[] = @$merchant->inside_shipping_cost;
				}
			} else {
				$details = $this->orderDetails->where(['order_master_id' => $orderMaster->id, 'seller_id' => $value])->get();
				$weight = $details->sum('weight');
				# if seller from outside kuwait and buyer from kuwait
				if($orderMaster->shipping_country == 134) {
					$zone = ZoneDetail::where(['country_id' => $merchant->country])->first();
				} else {
					$zone = ZoneDetail::where(['country_id' => $orderMaster->shipping_country])->first();
				}
				$infinityChk = ZoneRateDetail::where(['zone_id' => $zone->zone_master_id])->where('infinity_weight', 'Y')->first();
				if($infinityChk && @$weight >= $infinityChk->from_weight) {
					$sCost = $infinityChk;
				} else {
					$sCost = ZoneRateDetail::where(['zone_id' => $zone->zone_master_id])
					->where(function($where) use ($weight) {
						$where->where(function($where1) use ($weight) {
							$where1->where('from_weight', '<=', @$weight)
							->where('to_weight', '>', @$weight);
						})
						->orWhere(function($where2) use ($weight) {
							$where2->where('from_weight', '<', @$weight)
							->where('to_weight', '>=', @$weight);
						});
					})
					->first();
				}
				$outsideShippingCost += @$sCost->internal_outside_kuwait;
			}
		}
		if(count($merHgCst)) {
			$insideShippingCost = max($merHgCst);
		}
		$orderDet = $this->order->where(['id' => $orderMaster->id])->first();
		
		$this->order->where(['id' => $orderMaster->id])->update([
			'shipping_price' 	=> 0, 
			'order_total' 		=> $orderDet->order_total - $orderDet->shipping_price
		]);

		$orderDet = $this->order->where(['id' => $orderMaster->id])->first();
		
		$this->order->where(['id' => $orderMaster->id])
		->update([
			'shipping_price' 	=> $outsideShippingCost + $insideShippingCost, 
			'order_total' 		=> $orderDet->order_total + $outsideShippingCost + $insideShippingCost
		]);
	}

	/**
	* Method: checkStockAndVariant
	* Description: This method is used to check variant stock and correct variand ID.
	* Author: Sanjoy
	*/
	private function checkStockAndVariant($proVarId) {
		$variants = $this->productVariant->whereId($proVarId)->first();
		if($variants && $variants->stock_quantity) {
			return $variants;
		}
		return '';
	}

	/**
	* Method: updateOrderMasterPrice
	* Description: This method is used to update order master price
	* Author: Sanjoy
	*/
	private function updateOrderMasterPrice($orderMasterId) {
		$setting = Setting::first();
		$order = $this->orderMaster->whereId($orderMasterId)->first();
		$orderDetails = $this->orderDetails->where('order_master_id', $orderMasterId);
		$discount 		= $orderDetails->sum('discounted_price');
		$subTotal 		= $orderDetails->sum('sub_total');
		$total_product_price 	= $orderDetails->sum('total');
		$admin_commission 	= $orderDetails->sum('admin_commission');
		
		$totalDiscount 	= $subTotal - $total_product_price;
		$totalWeight 	= $orderDetails->sum('weight');
		$orderDetails1 = $orderDetails->get();
		$loadingPrice = @$orderDetails->sum('loading_price');
		$insurance_price = @$orderDetails->sum('insurance_price');
		$payable_amount = @$orderDetails->sum('payable_amount');
		// $totalWeights   = $totalWeight * $orderDetails->sum('quantity');
		// $loadingPrice   =  $totalWeights * @$setting->loading_unloading_price;
		// $insurancePrice =  (@$orderDetails->sum('total') * @$setting->insurance) / 100;
		$totalSellerCcommission = $orderDetails->sum('seller_commission');
		// $adminCom = (@$total_product_price * @$setting->commission) / 100;
		$orderTotal 	= $total_product_price+$order->shipping_price  + $insurance_price + $loadingPrice + $totalSellerCcommission;
		return $this->orderMaster->whereId($orderMasterId)->update([
			'total_discount'		=> $totalDiscount,
			'subtotal' 				=> $subTotal,
			'total_product_price' 				=> $total_product_price,
			'order_total'			=> $total_product_price+$order->shipping_price  + $insurance_price + $loadingPrice + $totalSellerCcommission,
			'product_total_weight'	=> $totalWeight,
			'payable_amount'       => $payable_amount,
			'insurance_price'       => $insurance_price,
			'loading_price'         => $loadingPrice,
			'total_seller_commission' =>$totalSellerCcommission,
			'admin_commission' => @$admin_commission,
		]);
	}

	/**
	* Method: insertShippingData
	* Description: This method is used to insert shipping data to table
	* Author: Sanjoy
	*/
	private function insertShippingData($request, $orderId) {
		$userInfo = auth()->user();
		# if selected from address book
		if(@$request['shipping_address_id']) {
			$shippingAddress = $this->addressBook->whereId($request['shipping_address_id'])->first();

			$order['shipping_address_id']   =   $request['shipping_address_id'];
			$order['shipping_fname']     	=   $shippingAddress->shipping_fname;
			$order['shipping_lname']     	=   $shippingAddress->shipping_lname;
			$order['shipping_email']     	=   $shippingAddress->email;
			$order['shipping_phone']     	=   $shippingAddress->phone;
			$order['shipping_country']   	=   $shippingAddress->country;
			$order['shipping_city']      	=   $shippingAddress->city;
			// $order['shipping_street']    	=   $shippingAddress->street;
			$order['shipping_state']    	=   $shippingAddress->state;
			// $order['shipping_block']    	=   $shippingAddress->block;
			// $order['shipping_building']  	=   $shippingAddress->building;
			$order['shipping_postal_code']  =   $shippingAddress->postal_code;
			$order['shipping_more_address'] =   $shippingAddress->more_address;
			$order['shipping_street'] =   $shippingAddress->location;
			$order['billing_street'] =   $shippingAddress->location;
			$order['billing_more_address'] =   $shippingAddress->location;
			// if($shippingAddress->country == 134) {
			// 	$order['shipping_city_id']  =   $shippingAddress->city_id;	
			// }
			$order['location']				= @$shippingAddress->location;
			$order['lat']					= @$shippingAddress->lat;
			$order['lng']					= @$shippingAddress->lng;
		} else {
			# if enter manually
			$order['shipping_fname']     	= @$request['shipping_fname'];
			$order['shipping_lname']     	= @$request['shipping_lname'];
			$order['shipping_email']     	= @$request['shipping_email'];
			$order['shipping_phone']     	= @$request['shipping_phone'];
			$order['shipping_street']     	= @$request['shipping_nearest_landmark'];
			$order['shipping_country']   	= @$request['shipping_country'];
			
			$order['shipping_more_address'] = @$request['shipping_more_address'];

			// location, lat, lng
			$order['location']				= @$request['location'];
			$order['lat']					= @$request['lat'];
			$order['lng']					= @$request['lng'];

			if(@$request['shipping_city']) {
				$order['shipping_city']      	=   @$request['shipping_city'];
			}
			if(@$request['shipping_state']) {
				$order['shipping_state']      	=   @$request['shipping_state'];
			} 
			// if(@$request['shipping_city_id']) {
			// 	$order['shipping_city_id']      =   @$request['shipping_city_id'];
			// 	$order['shipping_city']      	=   @$request['shipping_city'];				
			// }
			
			if(@$request['shipping_postal_code']) {
				$order['shipping_postal_code']  =   @$request['shipping_postal_code'];
			}
				$ord = $this->orderMaster->where(['id' => $orderId])->first();
				$userAdd['user_id']             =   @$ord->user_id;
                $userAdd['shipping_fname']      =   @$request['shipping_fname'];
                $userAdd['shipping_lname']      =   @$request['shipping_lname'];

                $userAdd['email']               =   $userInfo->email;
                $userAdd['phone']               =   @$request['shipping_phone'];
                $userAdd['country']             =   @$request['shipping_country'];
                $userAdd['more_address']        =   @$request['shipping_more_address'];
                $userAdd['location']            =   @$request['shipping_nearest_landmark'];
                $userAdd['lat']                 =   @$request['lat'];
                $userAdd['lng']                 =   @$request['lng'];
                $userAdd['type_of_address']     =   "H";
                $userAdd['city']            =   @$request['shipping_city'];
                $userAdd['state']         =   @$request['shipping_state'];
                $userAdd['postal_code']         =   @$request['shipping_postal_code'];
            	$userAdd['is_default'] = 'N';
                $userAddress = UserAddressBook::create($userAdd);
		}
		# updating to order master table
		$this->orderMaster->where(['id' => $orderId])->update($order);
	}

	/**
	* Method: saveAddressBook
	* Description: This method is used to save address to address book.
	*/
	public function saveAddressBook($reqData) {
		// $totAddress = $this->addressBook->where(['user_id' => @$this->userId])->count();
		// $isDefault = 'N';
		// if($totAddress == 0) {
		// 	$isDefault = 'Y';
		// }
		// $this->addressBook->create([
		// 	'user_id'		 => @$this->userId,
		// 	'shipping_fname' => $reqData['shipping_fname'],
		// 	'shipping_lname' => $reqData['shipping_lname'],
		// 	'email'			 => " ",
		// 	'phone'			 => $reqData['shipping_phone'],
		// 	'location'		 => $reqData['location'],
		// 	'lat'			 => $reqData['lat'],
		// 	'lng'			 => $reqData['lng'],
		// 	'country'		 => $reqData['shipping_country'],
		// 	'street'		 => $reqData['shipping_street'],
		// 	'block'			 => $reqData['shipping_block'],
		// 	'building'	     => $reqData['shipping_building'],
		// 	'more_address'	 => $reqData['shipping_more_address'],
		// 	// 'city_id'		 => @$reqData['shipping_city_id'],
		// 	'city'			 => $reqData['shipping_city'],
		// 	'state'			 => $reqData['shipping_state'],
		// 	'postal_code' 	 => @$reqData['shipping_postal_code'],
		// 	'is_default'	 => $isDefault
		// ]);
				
	}

	/**
	* Method: insertBillingData
	* Description: This method is used to insert billing data to table
	* Author: Sanjoy
	*/
	private function insertBillingData($request, $orderId) {
		# if selected from address book
		if(@$request['billing_address_id']) {
			$billingAddress = $this->addressBook->whereId($request['billing_address_id'])->first();

			$order['billing_address_id']   	= $request['billing_address_id'];
			$order['billing_fname']     	= $billingAddress->shipping_fname;
			$order['billing_lname']     	= $billingAddress->shipping_lname;
			$order['billing_email']     	= $billingAddress->email;
			$order['billing_phone']     	= $billingAddress->phone;
			$order['billing_country']   	= $billingAddress->country;
			$order['billing_city']      	= $billingAddress->city;
			$order['billing_state']    	    = $billingAddress->state;
			$order['billing_street']    		= $billingAddress->location;
			$order['billing_block']    		= $billingAddress->block;
			$order['billing_building']  	= $billingAddress->building;
			$order['billing_postal_code']  	= $billingAddress->postal_code;
			$order['billing_more_address'] 	= $billingAddress->more_address;
			// if($billingAddress->country == 134) {
			// 	$order['billing_city_id']   = $billingAddress->city_id;	
			// }
		} else {
			# if enter manually
			$order['billing_fname']     	= @$request['billing_fname'];
			$order['billing_lname']     	= @$request['billing_lname'];
			$order['billing_email']     	= @$request['billing_email'];
			$order['billing_phone']     	= @$request['billing_phone'];
			$order['billing_country']   	= @$request['billing_country'];
			if(@$request['billing_nearest_landmark']){
				$order['billing_street']    	= @$request['billing_nearest_landmark'];
			}
			$order['billing_block']    		= @$request['billing_block'];
			$order['billing_building']  	= @$request['billing_building'];
			if(@$request['billing_more_address']){
				$order['billing_more_address'] 	= @$request['billing_more_address'];
			}

			if(@$request['billing_city']) {
				$order['billing_city']      	=   @$request['billing_city'];
			} 
			if(@$request['billing_state']) {
				$order['billing_state']      	=   @$request['billing_state'];
			} 
			// if(@$request['billing_city_id']) {
			// 	$order['billing_city_id']      	=   @$request['billing_city_id'];		
			// 	$order['billing_city']      	=   @$request['billing_city'];				
			// }
			
			if(@$request['billing_postal_code']) {
				$order['billing_postal_code']  =   @$request['billing_postal_code'];
			}
		}
		# updating to order master table
		$this->orderMaster->where(['id' => $orderId])->update($order);
	}

	/**
	* Method: insertOrderSeller
	* Description: This method is used to insert data to order_seller table
	* Author: Sanjoy
	*/
	private function insertOrderSeller($orderId) {
		$sellerProducts = $this->orderDetails->where([
			'order_master_id' => $orderId
		])
		->groupBy('seller_id')->get();

		foreach ($sellerProducts as $sp) {
			$totalWeight = $this->orderDetails->where([
				'order_master_id' => $orderId,
				'seller_id' 	  => $sp->seller_id
			])->sum('weight');

			$subTotal = $this->orderDetails->where([
				'order_master_id' => $orderId,
				'seller_id' 	  => $sp->seller_id
			])->sum('sub_total');

			$total = $this->orderDetails->where([
				'order_master_id' => $orderId,
				'seller_id' 	  => $sp->seller_id
			])->sum('total');
			
			$admin_commission = $this->orderDetails->where([
				'order_master_id' => $orderId,
				'seller_id' 	  => $sp->seller_id
			])->sum('admin_commission');
			
			
			
			$loading_price = $this->orderDetails->where([
				'order_master_id' => $orderId,
				'seller_id' 	  => $sp->seller_id
			])->sum('loading_price');

			$insurance_price = $this->orderDetails->where([
				'order_master_id' => $orderId,
				'seller_id' 	  => $sp->seller_id
			])->sum('insurance_price');

			$payable_amount = $this->orderDetails->where([
				'order_master_id' => $orderId,
				'seller_id' 	  => $sp->seller_id
			])->sum('payable_amount');


			$seller_commission = $this->orderDetails->where([
				'order_master_id' => $orderId,
				'seller_id' 	  => $sp->seller_id
			])->sum('seller_commission');

			$payable_amount = $this->orderDetails->where([
				'order_master_id' => $orderId,
				'seller_id' 	  => $sp->seller_id
			])->sum('payable_amount');

			$earning = $payable_amount - $insurance_price - $admin_commission;

			$totalDiscount = $subTotal - $total;
			$orderSel['seller_id']   		= $sp->seller_id;
			$orderSel['order_master_id']   	= $orderId;
			$orderSel['total_weight']   	= $totalWeight;
			// $orderSel['total_weight']   	= $totalWeight;
			$orderSel['subtotal']   		= $subTotal;
			$orderSel['total_discount']   	= $totalDiscount;
			$orderSel['order_total']   		= $payable_amount;
			$orderSel['total_commission']   		= $admin_commission;
			$orderSel['admin_commission']   		= $admin_commission;
			$orderSel['seller_commission']   		= $seller_commission;
			$orderSel['payable_amount']   		= $payable_amount;
			$orderSel['earning']   		= $earning;
			$orderSel['status']				= 'INP';
			$orderSel['verify_code']		= time().rand(111, 999);
			$orderSel['loading_price']		= $loading_price;
			$orderSel['insurance_price']		= $insurance_price;
			$orderSelD = $this->orderSeller->create($orderSel);
			
			# no need to calculate shipping price for internal order for seller
			// $this->calculateSellerShippingPrice($order, $orderSelD->id, $total);
		}
		return $sellerProducts->count();
	}
	
	private function sendNotificationCustomer($order_id,$userId){
        $androidDrivers = User::where(['status' => 'A', 'device_type' => 'A'])->pluck('firebase_reg_no');

        $iosDrivers = User::where(['id'=>@$userId,'status' => 'A', 'device_type' => 'I'])->pluck('firebase_reg_no');
        $order_dtt = OrderMaster::where('id',$order_id)->first();
        # send notification to android drivers
        if(count($androidDrivers)) {
            $msg = array();
            if(@$order_dtt->status == 'N'){
                $msg['title']             = " Order No : ".@$order_dtt->order_no;
                $msg["body"]              = 'Your have successfully placed the order! ';
            }
            if(@$order_dtt->status == 'DA'){
                $msg['title']             = ''." "." Order No : ".@$order_dtt->order_no;
            	$msg["body"]              = 'Your order is out for delivery! ';
            }
            if(@$order_dtt->status == 'RP'){
                $msg['title']             = " "." Order No : ".@$order_dtt->order_no;
            	$msg["body"]              = 'Your order status is ready to be picked up! ';
            }
            if(@$order_dtt->status == 'OP'){
                $msg['title']             = ''." "." Order No : ".@$order_dtt->order_no;
                $msg["body"]              = 'Your order is picked up! ';
            }
            if(@$order_dtt->status == 'OD'){
                $msg['title']             = ''." "." Order No : ".@$order_dtt->order_no;
            	$msg["body"]              = 'Your order Delivered! ';
            }
            $msg['link_type']         = "Y";
            $msg['is_linked']         = "Y";
            $msg['image']             = '';
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]            = "10";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'registration_ids' => $androidDrivers,
                'data'             => $msg
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            
            // Closing Curl
            curl_close($ch);
        }
        
        # send notification to ios drivers
        if(count($iosDrivers)) {
            $msg                      = array();
            $msg['title']             = "New Order Placed765";
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]            = "10";
            $msg["mutable-content"]   = true;
            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            $msg["body"]              = 'Recently customer placed an order! 65756';

            $data['message'] = "New Order Placed";
            $data['type'] = "1";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'category'                  => "CustomSamplePush",
                'content_available'         => true,
                'mutable_content'           => true,
                'registration_ids'          => $iosDrivers,
                'notification'              => $msg,
                'data'                      => $data
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            // Closing Curl
            curl_close($ch);
        }
    }
}
