<?php

namespace App\Http\Controllers\Api\Modules\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Variant;
use App\Models\Brand;
use App\Models\ContentDetails;
use App\Merchant;
use App\Models\BannerContent;
use App\Repositories\BrandRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\UserFavoriteRepository;
use App\Repositories\CartMasterRepository;

use JWTFactory;
use JWTAuth;
use Validator;
use Session;
use Config;
class SearchController extends Controller
{
    protected $brands, $products, $userFavorite, $cart;

    public function __construct(BrandRepository $brands,
        ProductRepository $products,
        CategoryRepository $category,
        UserFavoriteRepository $userFavorite,
        CartMasterRepository $cart
    )
    {
        $this->brands           = $brands;
        $this->products         = $products;
        $this->category         = $category;
        $this->userFavorite     = $userFavorite;
        $this->cart             = $cart;
    }

    /**
    * Method: index
    * Description: This method is used to change user password
    * Author: Surajit 
    */
    
    public function index(Request $request,$deviceId = NULL) {
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        if(!empty($reqData['params']['category_id'])){
            $catIds = explode(',', $reqData['params']['category_id']);
        }
        $category_json = Setting::select('category_json')->first();
        $category_json = json_decode($category_json->category_json);
        $product = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','avg_review','total_no_reviews')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description'])
        ->with(['wishlist'])
        ->selectRaw('id, price, discount_price,  round(((price - discount_price) / price) * 100) as percentage')
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N']);
        if(!empty(@$catIds)){
            $product = $product->whereHas('productCategory', function($q) use ($catIds){
                $q->whereIn('category_id',$catIds);
            });
        }
        $product = $product->orderBy('id', 'DESC')
        ->take(8)
        ->get();

        $featuredProduct = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname'])
        ->with(['wishlist'])
        ->selectRaw('id, price, discount_price,  round(((price - discount_price) / price) * 100) as percentage')
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N', 'is_featured' => 'Y']);
        if(!empty(@$catIds)){
            $featuredProduct = $featuredProduct->whereHas('productCategory', function($q) use ($catIds){
                $q->whereIn('category_id',$catIds);
            });
        }
       $featuredProduct = $featuredProduct->inRandomOrder()
        ->take(8)
        ->get();

        $bestDealProduct = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname'])
        ->with(['wishlist'])
        ->selectRaw('id, price, discount_price,  round(((price - discount_price) / price) * 100) as percentage')
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N']);
        if(!empty(@$catIds)){
            $bestDealProduct = $bestDealProduct->whereHas('productCategory', function($q) use ($catIds){
                $q->whereIn('category_id',$catIds);
            });
        }
        $bestDealProduct = $bestDealProduct->orderBy('times_of_sold', 'DESC')
        ->take(8)
        ->get();

        $merchant  = Merchant::where('status', 'A')
        ->select('id','slug','fname','lname','company_name','image')
        ->where('hide_merchant', 'N')
        ->where('status', 'A')
        ->where('premium_merchant', 'Y')
        ->get();

        $allBanners = BannerContent::where('language_id', getLanguage()->id)->get();
        $annoucement = ContentDetails::where(['content_id'=>4,'language_id'=>getLanguage()->id])->select('id','content_id','language_id','title')->first();

        $response['result']['cart_item'] = $this->cart->select('total_item');
        if(@$deviceId) {
            $response['result']['cart_item'] = $response['result']['cart_item']->where(['session_id' => $deviceId]);
        }
        if(@getUserByToken()->id) {
            $response['result']['cart_item'] = $response['result']['cart_item']->where(['user_id' => @getUserByToken()->id]);
        }
        $response['result']['cart_item'] = $response['result']['cart_item']->first();

        $response['result']['annoucement'] = $annoucement;
        $response['result']['category_json'] = $category_json;
        $response['result']['product']['featured_product'] = $featuredProduct;
        $response['result']['product']['new'] = $product;
        $response['result']['product']['best_deal_product'] = $bestDealProduct;
        $response['result']['product']['seller'] = $merchant;
        $response['result']['product']['all_banners'] = $allBanners;
        return response()->json($response);
    }
    /**
    * Method: index1
    * Description: This method is used for new home page
    * Author: Argha
    * Date:2021-04-28 
    */
    public function index1($deviceId = NULL) {
        $response = ['jsonrpc'=>'2.0'];
        $user_id = @getUserByToken()->id;
        //dd(Auth()->user());
        $category_json = Setting::select('category_json')->first();
        $category_json = json_decode($category_json->category_json);
        $product = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','avg_review','total_no_reviews')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description'])
        ->with(['wishlist'])
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N','is_new' =>'Y'])
        ->orderBy('id', 'DESC')
        ->take(8)
        ->get();

        $featuredProduct = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname'])
        ->with(['wishlist'])
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N', 'is_featured' => 'Y'])
        ->orderBy('id', 'DESC')
        ->take(8)
        ->get();

        $offeredProduct = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','is_new','in_offers')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname'])
        ->with(['wishlist'])
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N', 'in_offers' => 'Y'])
        ->orderBy('id', 'DESC')
        ->take(8)
        ->get();

        $bestDealProduct = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','is_new','in_offers')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname'])
        ->with(['wishlist'])
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N'])
        ->orderBy('times_of_sold', 'DESC')
        ->take(8)
        ->get();

        $merchant  = Merchant::where('status', 'A')
        ->select('id','slug','fname','lname','company_name','image')
        ->where('hide_merchant', 'N')
        ->where('status', 'A')
        ->where('premium_merchant', 'Y')
        ->get();

        $allBanners = BannerContent::where('language_id', getLanguage()->id)->get();
        $annoucement = ContentDetails::where(['content_id'=>4,'language_id'=>getLanguage()->id])->select('id','content_id','language_id','title')->first();

        $response['result']['cart_item'] = $this->cart->select('total_item');
        if(@$deviceId) {
            $response['result']['cart_item'] = $response['result']['cart_item']->where(['session_id' => $deviceId]);
        }
        if(@getUserByToken()->id) {
            $response['result']['cart_item'] = $response['result']['cart_item']->where(['user_id' => @getUserByToken()->id]);
        }
        $response['result']['cart_item'] = $response['result']['cart_item']->first();

        $response['result']['annoucement'] = $annoucement;
        $response['result']['category_json'] = $category_json;
        $response['result']['product']['featured_product'] = $featuredProduct;
        $response['result']['product']['new'] = $product;
        $response['result']['product']['offered'] = $offeredProduct;
        $response['result']['product']['best_deal_product'] = $bestDealProduct;
        $response['result']['product']['seller'] = $merchant;
        $response['result']['product']['all_banners'] = $allBanners;
        return response()->json($response);
    }
    /**
    * Method: showProductByFeature
    * Description: This method is used show product list depending on featured/new/offers/best deals
    * Author: Argha
    * Date:2021-04-28 
    */
    public function showProductByFeature(Request $request)
    {
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $type = $reqData['params']['type'];
        $count = @$reqData['params']['count'];
        $skip = $count*10;
        $take = 10;
        // F => Featured , N => New , O => Offers , B => Best Deals
        if($type == 'F'){

            $data = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','is_new','in_offers')
            ->with(['defaultImage:product_id,image', 
                'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname'])
            ->with(['wishlist'])
            ->whereHas('productMarchant', function($q) {
                $q->where('hide_merchant', 'N');
            })
            ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N', 'is_featured' => 'Y'])
            ->orderBy('id', 'DESC')
            ->skip($skip)
            ->take($take)
            ->get();
        }elseif($type == 'N'){

            $data = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','avg_review','total_no_reviews','is_new','in_offers')
            ->with(['defaultImage:product_id,image', 
                'productByLanguage:language_id,product_id,title,description'])
            ->with(['wishlist'])
            ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N','is_new' =>'Y'])
            ->orderBy('id', 'DESC')
            ->skip($skip)
            ->take($take)
            ->get();
        }elseif($type == 'O'){

            $data = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','avg_review','total_no_reviews','is_new','in_offers')
            ->with(['defaultImage:product_id,image', 
                'productByLanguage:language_id,product_id,title,description'])
            ->with(['wishlist'])
            ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N','in_offers' =>'Y'])
            ->orderBy('id', 'DESC')
            ->skip($skip)
            ->take($take)
            ->get();
        }elseif($type == 'B'){
            $data = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','is_new','in_offers')
            ->with(['defaultImage:product_id,image', 
                'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname'])
            ->with(['wishlist'])
            ->whereHas('productMarchant', function($q) {
                $q->where('hide_merchant', 'N');
            })
            ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N'])
            ->where('times_of_sold','>', 0)
            ->orderBy('times_of_sold', 'DESC')
            ->skip($skip)
            ->take($take)
            ->get();
        }else{
            $data = array();
        }
        $response['result']['products'] = $data;
        return response()->json($response);
    }
    /**
	*@ Method: getCategories
	*@ Description: fetch category and subcategories of keyword
	*@ Author: Surajit
    */
    
    public function getCategories(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $reqData = $request->json()->all();
            $keyword = $reqData['params']['keyword'];
            $parentCategory = Category::with([
                'categoryDetails',
                'subCategories',
                'subCategories.categoryDetails'
            ])->where(['parent_id' => 0,'status' => 'A']);
            if(@$keyword)
            {
                $parentCategory = $parentCategory->whereHas('categoryDetails', function($q1) use ($keyword){
                    $q1->whereRaw("UPPER(title) LIKE '%". strtoupper($keyword)."%'");
                });
            }
            $subCategory = Category::with([
                'categoryDetails',
                'parentCatDetails'
            ])->where('parent_id',"!=",0)->where(['status' => 'A']);
            if(@$keyword)
            {
                $subCategory = $subCategory->whereHas('categoryDetails', function($q2) use ($keyword){
                    $q2->whereRaw("UPPER(title) LIKE '%". strtoupper($keyword)."%'");
                });
            }
            $parentCategory = $parentCategory->get();
            $subCategory = $subCategory->get();
            $response['result']['parent_categories'] = $parentCategory;
            $response['result']['searched_sub_categories'] = $subCategory;
            // else {
            //     $response['error']['message'] = ERRORS['-33029'];    
            // }
            return response()->json($response);

        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] = $th->getMessage();
            return response()->json($response);
        }
    }

    /**
    * Method: changeLanguage
    * Description: This method is used to change language
    */
    public function changeLanguage(Request $request) {
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $lang = $reqData['params']['lang'];

        if (array_key_exists($lang, Config::get('languages'))) {            
            Session::put('applocale', $lang);
            Config::set('app.locale', $lang);
        }
        $response['result']['status'] = 'SUCCESS';
        $response['result']['lang'] = $lang;
        return response()->json($response);
    }



    /**
    * Method: searchProducts
    * Description: This method is used to search
    * Author: Surajit
    */
    
    public function searchProducts(Request $request) {
        $reqData = $request->json()->all();
        $response = [
            'jsonrpc'   => '2.0'
        ];
        // return json_encode($request->json()->all());
        $data = [];
        $data['products'] = $this->products->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date')->with([
            // 'productVariants.productVariantDetails.variantValueByLanguage', 
            'productParentCategory.category',
            'defaultImage',
            // 'productOtherVariants',
            'productByLanguage',
            'productMarchant:id,fname,lname',
            'wishlist'
        ])
        ->selectRaw('id, price, discount_price,  round(((price - discount_price) / price) * 100) as percentage')
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        });

        // if params is category then all variants of this category
        if(@$reqData['params']['category']) {
            $category_id = explode(",",@$reqData['params']['category']);
            $cat = $this->category->whereIn('id',@$category_id)->with('categoryByLanguage','categories.categoryByLanguage')->get();
            // $response['result']['category_details'] = $cat;
            if($cat) {
                $data['products'] = $data['products']->whereHas('productCategory', function($query) use($category_id) {
                    $query->whereIn('category_id',@$category_id);
                    $query->where('level', 'P');
                });
            }
        }

        if(@$reqData['params']['brands']) {
            $brands = explode(',', $reqData['params']['brands']);
            $data['products'] = $data['products']->whereIn('brand_id', $brands);
        }

        if(@$reqData['params']['product']) {
            $keyword = $reqData['params']['product'];
            $data['products'] = $data['products']->where(function($q1) use($keyword) {
                $q1->where('product_code', 'LIKE', '%' . $keyword . '%')
                ->orWhereHas('productByLanguage', function($q) use($keyword) {
                    $q->where('title', 'LIKE', '%'. $keyword .'%');
                });
            });
        }

        if(@$reqData['params']['variant']) {
            $variants = $reqData['params']['variant'];
            $data['products'] = $data['products']->whereHas('productVariants', function($q) use($variants) {
                foreach ($variants as $value) {
                    // dd($value['variant_id'],$value['value_id']);
                    $variantVal = explode(',', $value['value_id']);
                    if($value) {
                        $q->whereHas('productVariantDetails', function($q1) use($value, $variantVal) {
                            $q1->where('variant_id', $value['variant_id'])
                            ->whereIn('variant_value_id', $variantVal);
                        });
                    }
                }
            });

        }

        if(@$reqData['params']['other_variant']) {
            foreach ($reqData['params']['other_variant'] as $value) {
                $variantVal = explode(',', $value['value_id']);
                if($value) {
                    $data['products'] = $data['products']->whereHas('productOtherVariants', function($q1) use($value, $variantVal) {
                        $q1 = $q1->where('variant_id', $value['variant_id'])
                        ->whereIn('variant_value_id', $variantVal);
                    });
                }
            }
        }

        $minPrice = $data['products']->min('price');
        $maxPrice = $data['products']->max('price');
        if($minPrice) {
            $response['result']['min_price'] = $minPrice;
        } else {
            $response['result']['min_price'] = "0.00";
        }
        if($maxPrice) {
            $response['result']['max_price'] = $maxPrice;
        } else {
            $response['result']['max_price'] = "0.00";
        }
        
        if(@$reqData['params']['price']) {
            $price = explode(',', $reqData['params']['price']);
            $data['products'] = $data['products']->whereBetween('price', $price);
        }

        $data['products'] = $data['products']->where(['status' => 'A', 'seller_status' => 'A', 'product_hide' => 'N']);
        if(@$reqData['params']['order_by'] == 'H') {
            $data['products'] = $data['products']->orderBy('price', 'DESC')->get();    
        } else if(@$reqData['params']['order_by'] == 'L') {
            $data['products'] = $data['products']->orderBy('price', 'ASC')->get();    
        } else if(@$reqData['params']['order_by'] == 'N') {
            $data['products'] = $data['products']->orderBy('id', 'DESC')->get();    
        } else if(@$reqData['params']['order_by'] == 'A') {
            $data['products'] = $data['products']->whereHas('productByLanguage', function($query1) {
                $query1->orderBy('title', 'ASC');
            })->get();
        } else {
            $data['products'] = $data['products']->orderBy('id', 'DESC')->get();
        }


        $allCategory = Category::where(['parent_id' => 0, 'status' => 'A'])->with('categoryByLanguage','categories.categoryByLanguage')->get();

        $response['result']['products'] = $data['products'];
        $response['result']['all_categories'] = $allCategory;
        
        return response()->json($response);
    }

     /**
    * Method: allProducts
    * Description: This method is used to search all products
    * Author: Argha
    * Date:2021-05-05
    */
    
     public function allProducts(Request $request) {
        $reqData = $request->json()->all();
        $response = [
            'jsonrpc'   => '2.0'
        ];
        // return json_encode($request->json()->all());
        $data = [];
        $count = @$reqData['params']['count'];
        $skip = $count*10;
        $take = 10;
        $data['products'] = $this->products->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date')->with([
            // 'productVariants.productVariantDetails.variantValueByLanguage', 
            // 'productCategory',
            'defaultImage',
            // 'productOtherVariants',
            'productByLanguage',
            'productMarchant:id,fname,lname',
            'wishlist'
        ])
        ->selectRaw('id, price, discount_price,  round(((price - discount_price) / price) * 100) as percentage')
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        });

        // if params is category then all variants of this category
        if(@$reqData['params']['sub_category']) {
            $category = $this->category->with(['subCategories'])->where(['slug' => urldecode($reqData['params']['sub_category'])])->with('categoryByLanguage','parentCatDetails')->first();
            $response['result']['sub_category_details'] = $category;

            $data['products'] = $data['products']->whereHas('productCategory', function($query) use($category) {
                $query->where('category_id', $category->id);
                $query->where('level', 'S');
            });
        } elseif(@$reqData['params']['category']) {
            $cat = $this->category->where(['slug' => $reqData['params']['category']])->with('categoryByLanguage','categories.categoryByLanguage')->first();
            $response['result']['category_details'] = $cat;
            if($cat) {
                $data['products'] = $data['products']->whereHas('productCategory', function($query) use($cat) {
                    $query->where('category_id', $cat->id);
                    $query->where('level', 'P');
                });
            }
        }

        if(@$reqData['params']['brands']) {
            $brands = explode(',', $reqData['params']['brands']);
            $data['products'] = $data['products']->whereIn('brand_id', $brands);
        }

        if(@$reqData['params']['product']) {
            $keyword = $reqData['params']['product'];
            $data['products'] = $data['products']->where(function($q1) use($keyword) {
                $q1->where('product_code', 'LIKE', '%' . $keyword . '%')
                ->orWhereHas('productByLanguage', function($q) use($keyword) {
                    $q->where('title', 'LIKE', '%'. $keyword .'%');
                });
            });
        }

        if(@$reqData['params']['variant']) {
            $variants = $reqData['params']['variant'];
            $data['products'] = $data['products']->whereHas('productVariants', function($q) use($variants) {
                foreach ($variants as $value) {
                    // dd($value['variant_id'],$value['value_id']);
                    $variantVal = explode(',', $value['value_id']);
                    if($value) {
                        $q->whereHas('productVariantDetails', function($q1) use($value, $variantVal) {
                            $q1->where('variant_id', $value['variant_id'])
                            ->whereIn('variant_value_id', $variantVal);
                        });
                    }
                }
            });

        }

        if(@$reqData['params']['other_variant']) {
            foreach ($reqData['params']['other_variant'] as $value) {
                $variantVal = explode(',', $value['value_id']);
                if($value) {
                    $data['products'] = $data['products']->whereHas('productOtherVariants', function($q1) use($value, $variantVal) {
                        $q1 = $q1->where('variant_id', $value['variant_id'])
                        ->whereIn('variant_value_id', $variantVal);
                    });
                }
            }
        }

        $minPrice = $data['products']->min('price');
        $maxPrice = $data['products']->max('price');
        $count = $data['products']->count();
        if($minPrice) {
            $response['result']['min_price'] = $minPrice;
        } else {
            $response['result']['min_price'] = "0.00";
        }
        if($maxPrice) {
            $response['result']['max_price'] = $maxPrice;
        } else {
            $response['result']['max_price'] = "0.00";
        }
        
        if(@$reqData['params']['price']) {
            $price = explode(',', $reqData['params']['price']);
            $data['products'] = $data['products']->whereBetween('price', $price);
        }

        $data['products'] = $data['products']->where(['status' => 'A', 'seller_status' => 'A', 'product_hide' => 'N']);
        if(@$reqData['params']['order_by'] == 'H') {

            $data['products'] = $data['products']->orderBy('id','desc')->skip($skip)->take($take)->get();    
        } else if(@$reqData['params']['order_by'] == 'L') {
            $data['products'] = $data['products']->orderBy('id','desc')->skip($skip)->take($take)->get();    
        } else if(@$reqData['params']['order_by'] == 'N') {
            $data['products'] = $data['products']->orderBy('id','desc')->skip($skip)->take($take)->get();    
        } else if(@$reqData['params']['order_by'] == 'A') {
            $data['products'] = $data['products']->whereHas('productByLanguage', function($query1) {
                $query1->orderBy('title', 'ASC');
            })->orderBy('id','desc')->skip($skip)->take($take)->get();
        } else {
            //dump('ok');
            $data['products'] = $data['products']->orderBy('id','desc')->skip($skip)->take($take)->get();
        }


        $allCategory = Category::where(['parent_id' => 0, 'status' => 'A'])->with('categoryByLanguage','categories.categoryByLanguage')->get();

        $response['result']['products'] = $data['products'];
        $response['result']['all_categories'] = $allCategory;
        $response['result']['total_count'] = $count;
        
        return response()->json($response);
    }

    /**
    * Method: getFilterVal
    * Description: This method is used to get values in filter
    * Author: Surajit
    */
     
     public function getFilterVal(Request $request) {
        $reqData = $request->json()->all();
        $response = [
            'jsonrpc'   => '2.0'
        ];

        //get brand and category details
        if(@$reqData['params']['cat_slug']) {
            $response['result']['category'] = $category =  $this->category->where(['slug' => @$reqData['params']['cat_slug']])->with('categoryByLanguage','categories.categoryByLanguage')->first();
            $response['result']['brands'] = $this->brands->with(['brandDetailsByLanguage'])
            ->withCount('productsByBrand')
            ->whereHas('productsByBrand', function($query) {
                $query->where('status', 'A');
                $query->where('product_hide', 'N');
            }, '>=', 1)
            ->findWhere(['status' => 'A', 'category_id' => @$category->id]);
        }

        //get variants and sub_category details
        if(@$reqData['params']['sub_cat_slug']) {
            $response['result']['sub_category'] = $subCategory = $this->category->where(['slug' => @$reqData['params']['sub_cat_slug']])->with('categoryByLanguage','parentCatDetails')->first();
            $response['result']['variants'] = Variant::with([
                'variantByLanguage', 
                'variantValues.variantValueByLanguage'])
            ->where('sub_category_id', $subCategory->id)
            ->where('status', 'A')
            ->whereIn('type', ['P', 'S'])
            ->get();

            $response['result']['other_variants'] = Variant::with([
                'variantByLanguage', 
                'variantValues.variantValueByLanguage'
            ])
            ->where('sub_category_id', $subCategory->id)
            ->where('status', 'A')
            ->whereIn('type', ['SH', 'I'])
            ->get();
        }

        $response['result']['min_price'] = @$reqData['params']['min_price'];
        $response['result']['max_price'] = @$reqData['params']['max_price'];
        
        return response()->json($response);
    }

    /**
    *
    */
    public function getParentCategory($subCatId) {
        $response = [
            'jsonrpc'   => '2.0'
        ];
        $response['result']['category'] = $this->category->where(['status' => 'A'])
        ->where('slug', $subCatId)
        ->with(['categoryByLanguage'])
        ->withCount(['productBySubCategory' => function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        }])
        ->first();

        $response['result']['parent_category'] = $this->category->where(['status' => 'A'])
        ->where('id', $response['result']['category']->parent_id)
        ->with(['categoryByLanguage'])
        ->withCount(['productByCategory' => function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        }])
        ->first();
        return response()->json($response, 200);
    }

    /**
    * 
    */
    public function getSubCategories($slug) {
        $response = [
            'jsonrpc'   => '2.0'
        ];

        // parent category details
        $response['result']['parent_category'] = Category::with(['categoryByLanguage'])
        ->withCount(['productBySubCategory' => function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        }])
        ->where(['slug' => $slug])->first();
                            // return $response['result']['parent_category']->id;

        // all sub category details 
        $response['result']['sub_categories'] = $this->category->where(['status' => 'A'])
        ->where('parent_id', @$response['result']['parent_category']->id)
        ->with(['categoryByLanguage'])
        ->withCount(['productBySubCategory' => function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        }])
        ->whereHas( 'productBySubCategory', function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        })
        ->get();
        return response()->json($response, 200);
    }

    /**
    * Method: appleShare
    * Description: This method is used to share on apple device

    */
    public function appleShare() {
        $response = [
            'applinks' => [
                'apps' => [],
                'details' => [
                    [
                        'appID' => 'LPG8GHL26Q.com',
                        'paths' => ['*'],
                    ]
                ]
            ]
        ];

        return response()->json($response);
    }
}
