<?php

namespace App\Http\Controllers\Api\Modules\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Variant;
use App\Models\OrderDetail;
use App\Models\OrderSeller;
use App\Models\Brand;
use App\Models\ContentDetails;
use App\Merchant;
use App\Models\BannerContent;
use App\Repositories\BrandRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\UserFavoriteRepository;
use App\Repositories\CartMasterRepository;

use JWTFactory;
use JWTAuth;
use Validator;
use Session;
use Config;
class SearchController extends Controller
{
    protected $brands, $products, $userFavorite, $cart;

    public function __construct(BrandRepository $brands,
        OrderDetailRepository $order_details,
        OrderMasterRepository $order_master,
        OrderSellerRepository $orderSeller,
        ProductRepository $products,
        CategoryRepository $category,
        UserFavoriteRepository $userFavorite,
        CartMasterRepository $cart
    )
    {
        $this->brands           = $brands;
        $this->products         = $products;
        $this->category         = $category;
        $this->userFavorite     = $userFavorite;
        $this->cart             = $cart;
        $this->order             =   $order_master;
        $this->order_details            =   $order_details;
    }

    /**
    * Method: index
    * Description: This method is used to change user password
    * Author: Surajit 
    */
    /**
    * @OA\Post(
    * path="/api/home/{deviceId?}",
    * summary="Home Page",
    * description="Home Page",
    * operationId="authLogin",
    * tags={"Alaaddin: Home Page"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    description="Home Page",
    *    @OA\JsonContent(
    *       required={"category_id"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="category_id", type="string", format="category_id", example="1,2,3"),
    *           @OA\Property(property="user_id", type="string", format="user_id", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function index(Request $request,$deviceId = NULL) {
        try{
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();

        if(@$reqData['params']['user_id']) {
            $userId = $reqData['params']['user_id'];
        }else{
            $userId = 0;
        }
        if(@request()->header('Authorization')) {
            $response['result']['user_id'] = @getUserByToken()->id;
            $userId = @getUserByToken()->id;
        }
        if(!empty($reqData['params']['category_id'])){
            $catIds = explode(',', $reqData['params']['category_id']);
        }
        
        $category_json = Setting::select('category_json')->first();
        $category_json = json_decode($category_json->category_json);
        $product = Product::with(['show_cart_detail.cartMasterTable','productUnitMasters','productCategory.Category:id,slug,parent_id,picture,status,add_in_menu,is_popular','productCategory.categoryByLanguage','getProductCategory.Category'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','avg_review','total_no_reviews','unit_master_id')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description','wishlist','wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }])
        ->withCount(['show_cart_detail As is_already_added_in_cart'])
        ->selectRaw('id, price, discount_price,product_price,round(((price - product_price) / price) * 100) As percentage,total_no_reviews,avg_review,unit_master_id')
        ->addSelect(\DB::raw('product_price AS discount_price'))
        ->whereHas('productCategory.Category', function($q) {
            $q->where('status', 'A');
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N']);
        if(!empty(@$catIds)){
            $product = $product->whereHas('productCategory', function($q) use ($catIds){
                $q->whereIn('category_id',$catIds);
            });
        }
        $product = $product->orderBy('id', 'DESC')
        ->take(8)
        ->get();

        $featuredProduct = Product::with(['productUnitMasters','productCategory.Category','productCategory.categoryByLanguage','getProductCategory.Category'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','unit_master_id')
        ->with(['show_cart_detail','defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname',
            'wishlist',
            'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }])
        ->withCount(['show_cart_detail As is_already_added_in_cart'])
        ->selectRaw('id, price, discount_price,product_price,round(((price - product_price) / price) * 100) As percentage,total_no_reviews,avg_review,unit_master_id')
        ->whereHas('productCategory.Category', function($q) {
            $q->where('status', 'A');
        })
        ->addSelect(\DB::raw('product_price AS discount_price'))
        // ->with(['wishlist'])
        // ->selectRaw('id, price, discount_price,  round(((price - discount_price) / price) * 100) as percentage')
        // ->selectRaw('id, price, discount_price,product_price')
        
        // ->addSelect(\DB::raw('IF(discount_price=0.00, price, round(((price - discount_price) / price) * 100) ) AS percentage'))
        // ->addSelect(\DB::raw('IF(discount_price=0.00, price, discount_price ) AS discount_price'))
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        })->whereHas('productCategory.Category', function($q) {
            $q->where('status', 'A');
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N', 'is_featured' => 'Y']);
        if(!empty(@$catIds)){
            $featuredProduct = $featuredProduct->whereHas('productCategory', function($q) use ($catIds){
                $q->whereIn('category_id',$catIds);
            });
        }
       $featuredProduct = $featuredProduct->inRandomOrder()
        ->take(8)
        ->get();

        $bestDealProduct = Product::with(['productUnitMasters','productCategory.Category','productCategory.categoryByLanguage','getProductCategory.Category','getProductCategory.Category'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','unit_master_id')
        ->with(['show_cart_detail','defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname',
            'wishlist',
            'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }])
        ->withCount(['show_cart_detail As is_already_added_in_cart'])
        ->selectRaw('id, price, discount_price,product_price,round(((price - product_price) / price) * 100) As percentage,total_no_reviews,avg_review,unit_master_id')
        ->addSelect(\DB::raw('product_price AS discount_price'))
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        })->whereHas('productCategory.Category', function($q) {
            $q->where('status', 'A');
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N']);
        if(!empty(@$catIds)){
            $bestDealProduct = $bestDealProduct->whereHas('productCategory', function($q) use ($catIds){
                $q->whereIn('category_id',$catIds);
            });
        }
        $bestDealProduct = $bestDealProduct->orderBy('times_of_sold', 'DESC')
        ->take(8)
        ->get();

        $merchant  = Merchant::where('status', 'A')
        ->select('id','slug','fname','lname','company_name','image')
        ->where('hide_merchant', 'N')
        ->where('status', 'A')
        ->where('premium_merchant', 'Y')
        ->get();

        $allBanners = BannerContent::where('language_id', getLanguage()->id)->get();
        $annoucement = ContentDetails::where(['content_id'=>4,'language_id'=>getLanguage()->id])->select('id','content_id','language_id','title')->first();

        $response['result']['cart_item'] = $this->cart->select('total_item');
        if(@$deviceId) {
            $response['result']['cart_item'] = $response['result']['cart_item']->where(['session_id' => $deviceId]);
        }
        if(@getUserByToken()->id) {
            $response['result']['cart_item'] = $response['result']['cart_item']->where(['user_id' => @getUserByToken()->id]);
        }
        $response['result']['cart_item'] = $response['result']['cart_item']->first();

        $response['result']['annoucement'] = $annoucement;
        $response['result']['category_json'] = $category_json;
        $response['result']['product']['featured_product'] = $featuredProduct;
        $response['result']['product']['new'] = $product;
        $response['result']['product']['best_deal_product'] = $bestDealProduct;
        $response['result']['product']['seller'] = $merchant;
        $response['result']['product']['all_banners'] = $allBanners;
        return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/see-all/{deviceId?}",
    * summary="See All",
    * description="See All",
    * operationId="seeAll",
    * tags={"Alaaddin: See All"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    description="See All (see all featured/see all best deal product/see all new products",
    *    @OA\JsonContent(
    *       required={"category_id"},
    *           @OA\Property(property="params", type="object",
    *           @OA\Property(property="category_id", type="string", format="category_id", example="1,2,3"),
    *           @OA\Property(property="user_id", type="string", format="user_id", example="1"),
    *           @OA\Property(property="page", type="string", format="page", example="0"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function seeAll(Request $request,$deviceId = NULL) {
        try{
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();

        if(@$reqData['params']['user_id']) {
            $userId = $reqData['params']['user_id'];
        }else{
            $userId = 0;
        }
        $validator = Validator::make($reqData['params'],[
                'page'    => 'required|numeric',
                'user_id'    => 'required|numeric'
            ],
            [
                'user_id.required'=>'user_id required',
                'user_id.numeric'=>'user_id required',
                'page.required'=>'Page required',
                'page.number'=>'Page required'
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
        if(@request()->header('Authorization')) {
            $response['result']['user_id'] = @getUserByToken()->id;
            $userId = @getUserByToken()->id;
        }
        if(!empty($reqData['params']['category_id'])){
            $catIds = explode(',', $reqData['params']['category_id']);
        }
        
        $category_json = Setting::select('category_json')->first();
        $category_json = json_decode($category_json->category_json);
        $product = Product::with(['productUnitMasters','productCategory.Category:id,slug,parent_id,picture,status,add_in_menu,is_popular','productCategory.categoryByLanguage','getProductCategory.Category'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','avg_review','total_no_reviews','unit_master_id')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description','wishlist','wishlistfavourite'=> function($query) use($userId) {
            $query->where(function($q) use($userId) {
                $q->where(['user_id'=>$userId]);
            });
        }])
        ->selectRaw('id, price, discount_price,product_price,round(((price - product_price) / price) * 100) As percentage,total_no_reviews,avg_review,unit_master_id')
        ->addSelect(\DB::raw('product_price AS discount_price'))
        ->whereHas('productCategory.Category', function($query) {
            $query->whereIn('status',['A']);
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N']);
        if(!empty(@$catIds)){
            $product = $product->whereHas('productCategory', function($q) use ($catIds){
                $q->whereIn('category_id',$catIds);
            });
        }
        
            $page = @$reqData['params']['page']?$reqData['params']['page']:0; 
            $limit = 20;
            if($page == 1) {
                $start = 0;
            } else {
                $start = (($page -1) * $limit);
            }
            $response['result']['new_total'] =$product->count();
            $response['result']['new_page_count'] =ceil($product->count() /$limit);
            $response['result']['new_per_page'] =$limit;
            $product = $product->orderBy('id','desc')->skip($start)->take($limit)->get();
            $response['result']['product']['new'] = $product;
        
        
        // } else {
            // $product = $product->orderBy('id','desc')->get();
        // }
        
        $featuredProduct = Product::with(['productUnitMasters','productCategory.Category','productCategory.categoryByLanguage','getProductCategory.Category'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','unit_master_id')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname',
            'wishlist',
            'wishlistfavourite'=> function($query) use($userId) {
            $query->where(function($q) use($userId) {
                $q->where(['user_id'=>$userId]);
            });
        }])
        ->selectRaw('id, price, discount_price,product_price,round(((price - product_price) / price) * 100) As percentage,total_no_reviews,avg_review,unit_master_id')
        ->addSelect(\DB::raw('product_price AS discount_price'))
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        })
        ->whereHas('productCategory.Category', function($query) {
            $query->whereIn('status',['A']);
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N', 'is_featured' => 'Y']);
        if(!empty(@$catIds)){
            $featuredProduct = $featuredProduct->whereHas('productCategory', function($q) use ($catIds){
                $q->whereIn('category_id',$catIds);
            });
        }
        // if(@$reqData['params']['page']) {
            $page = @$reqData['params']['page']?$reqData['params']['page']:0; 
            $limit = 20;
            if($page == 1) {
                $start = 0;
            } else {
                $start = (($page -1) * $limit);
            }
            $response['result']['featuredProduct_total'] =$featuredProduct->count();
            $response['result']['featuredProduct_page_count'] =ceil($featuredProduct->count() /$limit);
            $response['result']['featuredProduct_per_page'] =$limit;
            $featuredProduct = $featuredProduct->inRandomOrder()->orderBy('id','desc')->skip($start)->take($limit)->get();
        // } else {
            // $featuredProduct = $featuredProduct->orderBy('id','desc')->get();
        // }

        $bestDealProduct = Product::with(['productUnitMasters','productCategory.Category','productCategory.categoryByLanguage','getProductCategory.Category','getProductCategory.Category'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','unit_master_id')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname',
            'wishlist',
            'wishlistfavourite'=> function($query) use($userId) {
            $query->where(function($q) use($userId) {
                $q->where(['user_id'=>$userId]);
            });
        }])
        ->selectRaw('id, price, discount_price,product_price,round(((price - product_price) / price) * 100) As percentage,total_no_reviews,avg_review,unit_master_id')
        ->addSelect(\DB::raw('product_price AS discount_price'))
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        })
        ->whereHas('productCategory.Category', function($query) {
            $query->whereIn('status',['A']);
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N']);
        if(!empty(@$catIds)){
            $bestDealProduct = $bestDealProduct->whereHas('productCategory', function($q) use ($catIds){
                $q->whereIn('category_id',$catIds);
            });
        }
        // if(@$reqData['params']['page']) {
            $page = @$reqData['params']['page']?$reqData['params']['page']:0; 
            $limit = 20;
            if($page == 1) {
                $start = 0;
            } else {
                $start = (($page -1) * $limit);
            }
            $response['result']['bestDealProduct_total'] =$bestDealProduct->count();
            $response['result']['bestDealProduct_page_count'] =ceil($bestDealProduct->count() /$limit);
            $response['result']['bestDealProduct_per_page'] =$limit;
            $bestDealProduct = $bestDealProduct->inRandomOrder()->orderBy('id','desc')->skip($start)->take($limit)->get();

        // } else {
        //     $bestDealProduct = $bestDealProduct->inRandomOrder()->orderBy('id','desc')->get();
        // }

        $merchant  = Merchant::where('status', 'A')
        ->select('id','slug','fname','lname','company_name','image')
        ->where('hide_merchant', 'N')
        ->where('status', 'A')
        ->where('premium_merchant', 'Y')
        ->get();

        $allBanners = BannerContent::where('language_id', getLanguage()->id)->get();
        $annoucement = ContentDetails::where(['content_id'=>4,'language_id'=>getLanguage()->id])->select('id','content_id','language_id','title')->first();

        $response['result']['cart_item'] = $this->cart->select('total_item');
        if(@$deviceId) {
            $response['result']['cart_item'] = $response['result']['cart_item']->where(['session_id' => $deviceId]);
        }
        if(@getUserByToken()->id) {
            $response['result']['cart_item'] = $response['result']['cart_item']->where(['user_id' => @getUserByToken()->id]);
        }
        $response['result']['cart_item'] = $response['result']['cart_item']->first();

        $response['result']['annoucement'] = $annoucement;
        $response['result']['category_json'] = $category_json;
        $response['result']['product']['featured_product'] = $featuredProduct;
        $response['result']['product']['new'] = $product;
        $response['result']['product']['best_deal_product'] = $bestDealProduct;
        $response['result']['product']['seller'] = $merchant;
        $response['result']['product']['all_banners'] = $allBanners;
        return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/see-all-reviews-of-product",
    * summary="See All product reviews",
    * description="See All product reviews",
    * operationId="seeAllProductReviews",
    * tags={"Alaaddin: See All product reviews"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    description="See All product reviews",
    *    @OA\JsonContent(
    *       required={"product_id"},
    *           @OA\Property(property="params", type="object",
    *           @OA\Property(property="product_id", type="string", example="1"),
    *           @OA\Property(property="page", type="string", format="page", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function seeAllProductReviews(Request $request,$deviceId = NULL) {
        try{
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();

        if(@$reqData['params']['user_id']) {
            $userId = $reqData['params']['user_id'];
        }else{
            $userId = 0;
        }
        if(@request()->header('Authorization')) {
            $response['result']['user_id'] = @getUserByToken()->id;
            $userId = @getUserByToken()->id;
        }
        $product = Product::where(['id'=>$reqData['params']['product_id']])->first();
        if(!@$product){
            $response['error'] = __('errors.-5099');
            $response['error']['meaning'] = " You have provided wrong id! ";
            return response()->json($response);
        }
        $reviews=OrderDetail::with(['orderMaster.customerDetails'])->where(['product_id'=>$reqData['params']['product_id']])->where('rate','>',0);
        $page = @$reqData['params']['page']?$reqData['params']['page']:0; 
        $limit = 20;
        if($page == 1) {
            $start = 0;
        } else {
            $start = (($page -1) * $limit);
        }
        $response['result']['new_total'] =$reviews->count();
        $response['result']['new_page_count'] =ceil($reviews->count() /$limit);
        $response['result']['new_per_page'] =$limit;
        $response['result']['product'] =@$product;
        $response['result']['avg_review'] =@$product->avg_review;
        $response['result']['total_no_reviews'] =@$product->total_no_reviews;
        
        $reviews = $reviews->orderBy('id','desc')->skip($start)->take($limit)->get();
        $response['result']['reviews'] = @$reviews;

        return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    
    /**
    * Method: index1
    * Description: This method is used for new home page
    * Author: Argha
    * Date:2021-04-28 
    */
    public function index1($deviceId = NULL) {
        $response = ['jsonrpc'=>'2.0'];
        $user_id = @getUserByToken()->id;
        //dd(Auth()->user());
        $category_json = Setting::select('category_json')->first();
        $category_json = json_decode($category_json->category_json);
        $product = Product::with(['productUnitMasters','productCategory.Category','productCategory.categoryByLanguage'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','avg_review','total_no_reviews','unit_master_idf')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description',
            'wishlist',
            'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }])
        // ->with(['wishlist'])
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N','is_new' =>'Y'])
        ->orderBy('id', 'DESC')
        ->take(8)
        ->get();

        $featuredProduct = Product::with(['productUnitMasters','productCategory.Category','productCategory.categoryByLanguage'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','unit_master_id')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname',
            'wishlist',
            'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }])
        // ->with(['wishlist'])
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N', 'is_featured' => 'Y'])
        ->orderBy('id', 'DESC')
        ->take(8)
        ->get();

        $offeredProduct = Product::with(['productUnitMasters','productCategory.Category','productCategory.categoryByLanguage'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','is_new','in_offers','unit_master_id')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname',
            'wishlist',
            'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }])
        // ->with(['wishlist'])
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N', 'in_offers' => 'Y'])
        ->orderBy('id', 'DESC')
        ->take(8)
        ->get();

        $bestDealProduct = Product::with(['productUnitMasters','productCategory.Category','productCategory.categoryByLanguage'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','is_new','in_offers','unit_master_id')
        ->with(['defaultImage:product_id,image', 
            'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname',
            'wishlist',
            'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }])
        // ->with(['wishlist'])
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        })
        ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N'])
        ->orderBy('times_of_sold', 'DESC')
        ->take(8)
        ->get();

        $merchant  = Merchant::where('status', 'A')
        ->select('id','slug','fname','lname','company_name','image')
        ->where('hide_merchant', 'N')
        ->where('status', 'A')
        ->where('premium_merchant', 'Y')
        ->get();

        $allBanners = BannerContent::where('language_id', getLanguage()->id)->get();
        $annoucement = ContentDetails::where(['content_id'=>4,'language_id'=>getLanguage()->id])->select('id','content_id','language_id','title')->first();

        $response['result']['cart_item'] = $this->cart->select('total_item');
        if(@$deviceId) {
            $response['result']['cart_item'] = $response['result']['cart_item']->where(['session_id' => $deviceId]);
        }
        if(@getUserByToken()->id) {
            $response['result']['cart_item'] = $response['result']['cart_item']->where(['user_id' => @getUserByToken()->id]);
        }
        $response['result']['cart_item'] = $response['result']['cart_item']->first();

        $response['result']['annoucement'] = $annoucement;
        $response['result']['category_json'] = $category_json;
        $response['result']['product']['featured_product'] = $featuredProduct;
        $response['result']['product']['new'] = $product;
        $response['result']['product']['offered'] = $offeredProduct;
        $response['result']['product']['best_deal_product'] = $bestDealProduct;
        $response['result']['product']['seller'] = $merchant;
        $response['result']['product']['all_banners'] = $allBanners;
        return response()->json($response);
    }
    /**
    * Method: showProductByFeature
    * Description: This method is used show product list depending on featured/new/offers/best deals
    * Author: Argha
    * Date:2021-04-28 
    */
    public function showProductByFeature(Request $request)
    {
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $type = $reqData['params']['type'];
        $count = @$reqData['params']['count'];
        $skip = $count*10;
        $take = 10;
        // F => Featured , N => New , O => Offers , B => Best Deals
        if($type == 'F'){

            $data = Product::with(['productUnitMasters','productCategory.Category','productCategory.categoryByLanguage'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','is_new','in_offers')
            ->with(['defaultImage:product_id,image', 
                'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname',
                'wishlist',
                'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }])
            // ->with(['wishlist'])
            ->whereHas('productMarchant', function($q) {
                $q->where('hide_merchant', 'N');
            })
            ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N', 'is_featured' => 'Y'])
            ->orderBy('id', 'DESC')
            ->skip($skip)
            ->take($take)
            ->get();
        }elseif($type == 'N'){

            $data = Product::with(['productUnitMasters','productCategory.Category','productCategory.categoryByLanguage'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','avg_review','total_no_reviews','is_new','in_offers')
            ->with(['defaultImage:product_id,image', 
                'productByLanguage:language_id,product_id,title,description',
                'wishlist',
                'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }])
            // ->with(['wishlist'])
            ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N','is_new' =>'Y'])
            ->orderBy('id', 'DESC')
            ->skip($skip)
            ->take($take)
            ->get();
        }elseif($type == 'O'){

            $data = Product::with(['productUnitMasters','productCategory.Category','productCategory.categoryByLanguage'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','avg_review','total_no_reviews','is_new','in_offers')
            ->with(['defaultImage:product_id,image', 
                'productByLanguage:language_id,product_id,title,description',
                'wishlist',
                'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }])
            // ->with(['wishlist'])
            ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N','in_offers' =>'Y'])
            ->orderBy('id', 'DESC')
            ->skip($skip)
            ->take($take)
            ->get();
        }elseif($type == 'B'){
            $data = Product::with(['productUnitMasters','productCategory.Category','productCategory.categoryByLanguage'])->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','is_new','in_offers')
            ->with(['defaultImage:product_id,image', 
                'productByLanguage:language_id,product_id,title,description', 'productMarchant:id,fname,lname',
                'wishlist',
                'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }])
            // ->with(['wishlist'])
            ->whereHas('productMarchant', function($q) {
                $q->where('hide_merchant', 'N');
            })
            ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N'])
            ->where('times_of_sold','>', 0)
            ->orderBy('times_of_sold', 'DESC')
            ->skip($skip)
            ->take($take)
            ->get();
        }else{
            $data = array();
        }
        $response['result']['products'] = $data;
        return response()->json($response);
    }
    /**
    *@ Method: getCategories
    *@ Description: fetch category and subcategories of keyword
    *@ Author: Surajit
    */
    /**
    * @OA\Post(
    * path="/api/get-categories",
    * summary="Get Categories",
    * description="Get Categories",
    * operationId="authLogin",
    * tags={"Alaaddin: Get Categories"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant credentials",
    *    @OA\JsonContent(
    *       required={"keyword"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="keyword", type="string", format="keyword", example="keywords"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function getCategories(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $reqData = $request->json()->all();
            $keyword = $reqData['params']['keyword'];
            $parentCategory = Category::with([
                'categoryDetails',
                'subCategories',
                'subCategories.categoryDetails'
            ])->where(['parent_id' => 0,'status' => 'A']);
            if(@$keyword)
            {
                $parentCategory = $parentCategory->whereHas('categoryDetails', function($q1) use ($keyword){
                    $q1->whereRaw("UPPER(title) LIKE '%". strtoupper($keyword)."%'");
                });
            }
            $subCategory = Category::with([
                'categoryDetails',
                'parentCatDetails'
            ])->where('parent_id',"!=",0)->where(['status' => 'A']);
            if(@$keyword)
            {
                $subCategory = $subCategory->whereHas('categoryDetails', function($q2) use ($keyword){
                    $q2->whereRaw("UPPER(title) LIKE '%". strtoupper($keyword)."%'");
                });
            }
            $parentCategory = $parentCategory->get();
            $subCategory = $subCategory->get();
            $response['result']['parent_categories'] = $parentCategory;
            $response['result']['searched_sub_categories'] = $subCategory;
            // else {
            //     $response['error']['message'] = ERRORS['-33029'];    
            // }
            return response()->json($response);

        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            // $response['error'] = $th->getMessage();
            return response()->json($response);
        }
    }

    /**
    * Method: changeLanguage
    * Description: This method is used to change language
    */
    public function changeLanguage(Request $request) {
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $lang = $reqData['params']['lang'];

        if (array_key_exists($lang, Config::get('languages'))) {            
            Session::put('applocale', $lang);
            Config::set('app.locale', $lang);
        }
        $response['result']['status'] = 'SUCCESS';
        $response['result']['lang'] = $lang;
        return response()->json($response);
    }



    /**
    * Method: searchProducts
    * Description: This method is used to search
    * Author: Surajit
    */
    /**
    * @OA\post(
    * path="/api/search",
    * summary="Search Product",
    * description="Search Product",
    * operationId="searchProducts",
    * tags={"Alaaddin: Search Product"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    description="Search Product",
    *    @OA\JsonContent(
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="category", type="string", format="category", example=""),
    *               @OA\Property(property="product", type="string", format="product", example=""),
    *               @OA\Property(property="price", type="string", format="price", example="0,1000"),
    *               @OA\Property(property="order_by", type="string", format="order_by", example="H"),
    *               @OA\Property(property="user_id", type="string", format="user_id", example="1"),
    *               @OA\Property(property="discounted", type="string", example="Y"),
    *               @OA\Property(property="sortbyratings", type="string", example="Y"),
    *               @OA\Property(property="page", type="string", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Sorry,  Please try again",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry,  Please try again")
    *        )
    *     )
    * )
    */
    public function searchProducts(Request $request) {
        try{
        $reqData = $request->json()->all();
        $response = [
            'jsonrpc'   => '2.0'
        ];
        $reqdata = $request->json()->all();
            
            $validator = Validator::make($reqdata['params'],[
                'page'    => 'required|numeric',
                'user_id'    => 'required|numeric'
            ],
            [
                'user_id.required'=>'user_id required',
                'user_id.numeric'=>'user_id required ',
                'page.required'=>'Page required',
                'page.number'=>'Page required'
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }

        if(@$reqData['params']['user_id']) {
            $userId = $reqData['params']['user_id'];
        }else{
            $userId = 0;
        }
        if(@request()->header('Authorization')) {
            $response['result']['user_id'] = @getUserByToken()->id;
            $userId = @getUserByToken()->id;
        }

        // return json_encode($request->json()->all());
        $data = [];
        $data['products'] = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','unit_master_id')->with([
            // 'productVariants.productVariantDetails.variantValueByLanguage', 
            // 'productParentCategory.category',
            'productUnitMasters',
            'getProductCategory.Category',
            'defaultImage',
            'show_cart_detail',
            // 'productOtherVariants',
            'productByLanguage',
            'productMarchant.state_details',
            'productMarchant.city_details',
            'productMarchant.Country',
            'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    },
            'wishlist'
        ])
        ->withCount(['show_cart_detail As is_already_added_in_cart'])
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        });
         $data['products'] = $data['products']->whereHas('productCategory.Category', function($query) {
            $query->whereIn('status',['A']);
        });
        // if params is category then all variants of this category
        if(@$reqData['params']['category']) {
            $category_id = explode(",",@$reqData['params']['category']);
            $cat = $this->category->whereIn('id',@$category_id)->with('categoryByLanguage','categories.categoryByLanguage')->get();
            // $response['result']['category_details'] = $cat;
            if($cat) {
                $data['products'] = $data['products']->whereHas('productCategory', function($query) use($category_id) {
                    $query->whereIn('category_id',@$category_id);
                    $query->where('level', 'P');
                });
            }
        }

        if(@$reqData['params']['brands']) {
            $brands = explode(',', $reqData['params']['brands']);
            $data['products'] = $data['products']->whereIn('brand_id', $brands);
        }
        if(@$reqData['params']['discounted'] == 'Y') {   
            $data['products'] = $data['products']->where('discount_price','>',0);
        }

        if(@$reqData['params']['product']) {
            $keyword = $reqData['params']['product'];
            $data['products'] = $data['products']->where(function($q1) use($keyword) {
                $q1->where('product_code', 'LIKE', '%' . $keyword . '%')
                ->orWhereHas('productByLanguage', function($q) use($keyword) {
                    $q->where('title', 'LIKE', '%'. $keyword .'%');
                })->orWhereHas('productMarchant', function($q) use($keyword) {
                    $q->where('fname', 'LIKE', '%'. $keyword .'%')
                    ->orWhere('lname', 'LIKE', '%'. $keyword .'%')
                    ->orWhere('phone', 'LIKE', '%'. $keyword .'%')
                    ->orWhere('email', 'LIKE', '%'. $keyword .'%')
                    ->orWhere('company_type', 'LIKE', '%'. $keyword .'%')
                    ->orWhere('company_details', 'LIKE', '%'. $keyword .'%')
                    ->orWhere('zipcode', 'LIKE', '%'. $keyword .'%')
                    ->orWhere('address', 'LIKE', '%'. $keyword .'%')
                    ->orWhere('company_name', 'LIKE', '%'. $keyword .'%');
                })->orWhereHas('productMarchant.state_details', function($q) use($keyword) {
                    $q->where('name', 'LIKE', '%'. $keyword .'%');
                })->orWhereHas('productMarchant.city_details', function($q) use($keyword) {
                    $q->where('name', 'LIKE', '%'. $keyword .'%');
                });
            });
        }

        if(@$reqData['params']['variant']) {
            $variants = $reqData['params']['variant'];
            $data['products'] = $data['products']->whereHas('productVariants', function($q) use($variants) {
                foreach ($variants as $value) {
                    // dd($value['variant_id'],$value['value_id']);
                    $variantVal = explode(',', $value['value_id']);
                    if($value) {
                        $q->whereHas('productVariantDetails', function($q1) use($value, $variantVal) {
                            $q1->where('variant_id', $value['variant_id'])
                            ->whereIn('variant_value_id', $variantVal);
                        });
                    }
                }
            });

        }

        if(@$reqData['params']['other_variant']) {
            foreach ($reqData['params']['other_variant'] as $value) {
                $variantVal = explode(',', $value['value_id']);
                if($value) {
                    $data['products'] = $data['products']->whereHas('productOtherVariants', function($q1) use($value, $variantVal) {
                        $q1 = $q1->where('variant_id', $value['variant_id'])
                        ->whereIn('variant_value_id', $variantVal);
                    });
                }
            }
        }

        $minPrice = $data['products']->min('product_price');
        $maxPrice = $data['products']->max('product_price');
        if($minPrice) {
            $response['result']['min_price'] = $minPrice;
        } else {
            $response['result']['min_price'] = "0.00";
        }
        if($maxPrice) {
            $response['result']['max_price'] = $maxPrice;
        } else {
            $response['result']['max_price'] = "0.00";
        }
        
        if(@$reqData['params']['price']) {
            $price = explode(',', $reqData['params']['price']);
            $data['products'] = $data['products']->whereBetween('product_price', $price);
        }

        $data['products'] = $data['products']->where(['status' => 'A', 'seller_status' => 'A', 'product_hide' => 'N'])->selectRaw('id, price, discount_price,product_price,round(((price - product_price) / price) * 100) as percentage,total_no_reviews,avg_review,unit_master_id')
        ->addSelect(\DB::raw('product_price AS discount_price'));
        
        // if(@$reqData['params']['page']){
                
                
            // }else{

            //     $data['products'] = $data['products']->get();
            // }
        if(@$reqData['params']['sortbyratings'] == 'Y') {
            $data['products'] = $data['products']->orderBy('avg_review', 'desc');    
        }
        if(@$reqData['params']['order_by'] == 'H') {
            $data['products'] = $data['products']->orderBy('product_price', 'DESC');    
        } else if(@$reqData['params']['order_by'] == 'L') {
            $data['products'] = $data['products']->orderBy('product_price', 'ASC');    
        } else if(@$reqData['params']['order_by'] == 'N') {
            $data['products'] = $data['products']->orderBy('id', 'DESC');    
        } else if(@$reqData['params']['order_by'] == 'A') {
            $data['products'] = $data['products']->whereHas('productByLanguage', function($query1) {
                $query1->orderBy('title', 'ASC');
            });
        } else {
            $data['products'] = $data['products']->orderBy('id', 'DESC');
        }
        
                $page = @$reqData['params']['page']?$reqData['params']['page']:1; 
                $limit = 25;
                if($page == 1) {
                    $start = 0;
                } else {
                    $start = (($page -1) * $limit);
                }
                $response['result']['products_total'] =$data['products']->count();
                $response['result']['products_page_count'] =ceil($data['products']->count() /$limit);
                $response['result']['products_per_page'] =$limit;
                $data['products'] = $data['products']->orderBy('id','desc')->skip($start)->take($limit)->get();
            
            

            

        $allCategory = Category::where(['parent_id' => 0, 'status' => 'A'])->with('categoryByLanguage','categories.categoryByLanguage')->get();

        $response['result']['products'] = $data['products'];
        $response['result']['all_categories'] = $allCategory;
        
        return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }

     /**
    * Method: allProducts
    * Description: This method is used to search all products
    * Author: Argha
    * Date:2021-05-05
    */
     /**
    * @OA\post(
    * path="/api/all-products",
    * summary="All Product",
    * description="All Product",
    * operationId="authLogin",
    * tags={"Alaaddin: All Product"},
    * @OA\RequestBody(
    *    description="All Product",
    *    @OA\JsonContent(
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="sub_category", type="string", format="sub_category", example=""),
    *               @OA\Property(property="category", type="string", format="category", example=""),
    *               @OA\Property(property="product", type="string", format="product", example=""),
    *               @OA\Property(property="price", type="string", format="price", example="0,1000"),
    *               @OA\Property(property="order_by", type="string", format="order_by", example="H"),
    *               @OA\Property(property="count", type="string", format="count", example="0"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
     public function allProducts(Request $request) {
        $reqData = $request->json()->all();
        $response = [
            'jsonrpc'   => '2.0'
        ];
        $userId =0;
        if(@getUserByToken()->id){
            $userId =@getUserByToken()->id;
        }else{
            $userId =0;
        }
        // return json_encode($request->json()->all());
        $data = [];
        $count = @$reqData['params']['count'];
        $skip = $count*10;
        $take = 10;
        $data['products'] = $this->products->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date','unit_master_id')->with([
            // 'productVariants.productVariantDetails.variantValueByLanguage', 
            // 'productCategory',
            'show_cart_detail',
            'defaultImage',
            'productUnitMasters',
            // 'productOtherVariants',
            'productByLanguage',
            'productMarchant:id,fname,lname',
            'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    },
            'wishlist'
        ])
        ->withCount(['show_cart_detail As is_already_added_in_cart'])
        ->selectRaw('id, price, discount_price,  round(((price - product_price) / price) * 100) as percentage,total_no_reviews,avg_review')
        ->addSelect(\DB::raw('product_price AS discount_price'))
        ->whereHas('productMarchant', function($q) {
            $q->where('hide_merchant', 'N');
        });

        // if params is category then all variants of this category
        if(@$reqData['params']['sub_category']) {
            $category = $this->category->with(['subCategories'])->where(['slug' => urldecode($reqData['params']['sub_category'])])->with('categoryByLanguage','parentCatDetails')->first();
            $response['result']['sub_category_details'] = $category;

            $data['products'] = $data['products']->whereHas('productCategory', function($query) use($category) {
                $query->where('category_id', $category->id);
                $query->where('level', 'S');
            });
        } elseif(@$reqData['params']['category']) {
            $cat = $this->category->where(['slug' => $reqData['params']['category']])->with('categoryByLanguage','categories.categoryByLanguage')->first();
            $response['result']['category_details'] = $cat;
            if($cat) {
                $data['products'] = $data['products']->whereHas('productCategory', function($query) use($cat) {
                    $query->where('category_id', $cat->id);
                    $query->where('level', 'P');
                });
            }
        }

        if(@$reqData['params']['brands']) {
            $brands = explode(',', $reqData['params']['brands']);
            $data['products'] = $data['products']->whereIn('brand_id', $brands);
        }

        if(@$reqData['params']['product']) {
            $keyword = $reqData['params']['product'];
            $data['products'] = $data['products']->where(function($q1) use($keyword) {
                $q1->where('product_code', 'LIKE', '%' . $keyword . '%')
                ->orWhereHas('productByLanguage', function($q) use($keyword) {
                    $q->where('title', 'LIKE', '%'. $keyword .'%');
                });
            });
        }

        if(@$reqData['params']['variant']) {
            $variants = $reqData['params']['variant'];
            $data['products'] = $data['products']->whereHas('productVariants', function($q) use($variants) {
                foreach ($variants as $value) {
                    // dd($value['variant_id'],$value['value_id']);
                    $variantVal = explode(',', $value['value_id']);
                    if($value) {
                        $q->whereHas('productVariantDetails', function($q1) use($value, $variantVal) {
                            $q1->where('variant_id', $value['variant_id'])
                            ->whereIn('variant_value_id', $variantVal);
                        });
                    }
                }
            });

        }

        if(@$reqData['params']['other_variant']) {
            foreach ($reqData['params']['other_variant'] as $value) {
                $variantVal = explode(',', $value['value_id']);
                if($value) {
                    $data['products'] = $data['products']->whereHas('productOtherVariants', function($q1) use($value, $variantVal) {
                        $q1 = $q1->where('variant_id', $value['variant_id'])
                        ->whereIn('variant_value_id', $variantVal);
                    });
                }
            }
        }

        $minPrice = $data['products']->min('price');
        $maxPrice = $data['products']->max('price');
        $count = $data['products']->count();
        if($minPrice) {
            $response['result']['min_price'] = $minPrice;
        } else {
            $response['result']['min_price'] = "0.00";
        }
        if($maxPrice) {
            $response['result']['max_price'] = $maxPrice;
        } else {
            $response['result']['max_price'] = "0.00";
        }
        
        if(@$reqData['params']['price']) {
            $price = explode(',', $reqData['params']['price']);
            $data['products'] = $data['products']->whereBetween('price', $price);
        }

        $data['products'] = $data['products']->where(['status' => 'A', 'seller_status' => 'A', 'product_hide' => 'N']);
        if(@$reqData['params']['order_by'] == 'H') {

            $data['products'] = $data['products']->orderBy('id','desc')->skip($skip)->take($take)->get();    
        } else if(@$reqData['params']['order_by'] == 'L') {
            $data['products'] = $data['products']->orderBy('id','desc')->skip($skip)->take($take)->get();    
        } else if(@$reqData['params']['order_by'] == 'N') {
            $data['products'] = $data['products']->orderBy('id','desc')->skip($skip)->take($take)->get();    
        } else if(@$reqData['params']['order_by'] == 'A') {
            $data['products'] = $data['products']->whereHas('productByLanguage', function($query1) {
                $query1->orderBy('title', 'ASC');
            })->orderBy('id','desc')->skip($skip)->take($take)->get();
        } else {
            //dump('ok');
            $data['products'] = $data['products']->orderBy('id','desc')->skip($skip)->take($take)->get();
        }


        $allCategory = Category::where(['parent_id' => 0, 'status' => 'A'])->with('categoryByLanguage','categories.categoryByLanguage')->get();

        $response['result']['products'] = $data['products'];
        $response['result']['all_categories'] = $allCategory;
        $response['result']['total_count'] = $count;
        
        return response()->json($response);
    }

    /**
    * Method: getFilterVal
    * Description: This method is used to get values in filter
    * Author: Surajit
    */
     /**
    * @OA\post(
    * path="/api/filter",
    * summary="Filter Value",
    * description="Filter Value",
    * operationId="authLogin",
    * tags={"Alaaddin: Filter Value"},
    * @OA\RequestBody(
    *    description="Filter Value",
    *    @OA\JsonContent(
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="cat_slug", type="string", format="cat_slug", example=""),
    *               @OA\Property(property="sub_cat_slug", type="string", format="sub_cat_slug", example=""),
    *               @OA\Property(property="min_price", type="string", format="min_price", example="0"),
    *               @OA\Property(property="max_price", type="string", format="max_price", example="1000"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
     public function getFilterVal(Request $request) {
        $reqData = $request->json()->all();
        $response = [
            'jsonrpc'   => '2.0'
        ];

        //get brand and category details
        if(@$reqData['params']['cat_slug']) {
            $response['result']['category'] = $category =  $this->category->where(['slug' => @$reqData['params']['cat_slug']])->with('categoryByLanguage','categories.categoryByLanguage')->first();
            $response['result']['brands'] = $this->brands->with(['brandDetailsByLanguage'])
            ->withCount('productsByBrand')
            ->whereHas('productsByBrand', function($query) {
                $query->where('status', 'A');
                $query->where('product_hide', 'N');
            }, '>=', 1)
            ->findWhere(['status' => 'A', 'category_id' => @$category->id]);
        }

        //get variants and sub_category details
        if(@$reqData['params']['sub_cat_slug']) {
            $response['result']['sub_category'] = $subCategory = $this->category->where(['slug' => @$reqData['params']['sub_cat_slug']])->with('categoryByLanguage','parentCatDetails')->first();
            $response['result']['variants'] = Variant::with([
                'variantByLanguage', 
                'variantValues.variantValueByLanguage'])
            ->where('sub_category_id', $subCategory->id)
            ->where('status', 'A')
            ->whereIn('type', ['P', 'S'])
            ->get();

            $response['result']['other_variants'] = Variant::with([
                'variantByLanguage', 
                'variantValues.variantValueByLanguage'
            ])
            ->where('sub_category_id', $subCategory->id)
            ->where('status', 'A')
            ->whereIn('type', ['SH', 'I'])
            ->get();
        }

        $response['result']['min_price'] = @$reqData['params']['min_price'];
        $response['result']['max_price'] = @$reqData['params']['max_price'];
        
        return response()->json($response);
    }

    /**
    *
    */
    public function getParentCategory($subCatId) {
        $response = [
            'jsonrpc'   => '2.0'
        ];
        $response['result']['category'] = $this->category->where(['status' => 'A'])
        ->where('slug', $subCatId)
        ->with(['categoryByLanguage'])
        ->withCount(['productBySubCategory' => function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        }])
        ->first();

        $response['result']['parent_category'] = $this->category->where(['status' => 'A'])
        ->where('id', $response['result']['category']->parent_id)
        ->with(['categoryByLanguage'])
        ->withCount(['productByCategory' => function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        }])
        ->first();
        return response()->json($response, 200);
    }

    /**
    * 
    */
    public function getSubCategories($slug) {
        $response = [
            'jsonrpc'   => '2.0'
        ];

        // parent category details
        $response['result']['parent_category'] = Category::with(['categoryByLanguage'])
        ->withCount(['productBySubCategory' => function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        }])
        ->where(['slug' => $slug])->first();
                            // return $response['result']['parent_category']->id;

        // all sub category details 
        $response['result']['sub_categories'] = $this->category->where(['status' => 'A'])
        ->where('parent_id', @$response['result']['parent_category']->id)
        ->with(['categoryByLanguage'])
        ->withCount(['productBySubCategory' => function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        }])
        ->whereHas( 'productBySubCategory', function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        })
        ->get();
        return response()->json($response, 200);
    }

    /**
    * Method: appleShare
    * Description: This method is used to share on apple device

    */
    public function appleShare() {
        $response = [
            'applinks' => [
                'apps' => [],
                'details' => [
                    [
                        'appID' => '',
                        'paths' => ['*'],
                    ]
                ]
            ]
        ];

        return response()->json($response);
    }
    /**
    * @OA\Post(
    * path="/api/getOrderProductSpecificDetails",
    * summary="getOrderProductSpecificDetails",
    * description="getOrderProductSpecificDetails",
    * operationId="getOrderProductSpecificDetails",
    * tags={"Alaaddin: getOrderProductSpecificDetails"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    description="getOrderProductSpecificDetails",
    *    @OA\JsonContent(
    *       required={"order_master_id","product_id"},
    *           @OA\Property(property="params", type="object",
    *           @OA\Property(property="order_master_id", type="string", example="1"),
    *           @OA\Property(property="product_id", type="string", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function getOrderProductSpecificDetails(Request $request) {

        $response = ['jsonrpc'=>'2.0'];
        
        $reqData = $request->json()->all();

        if(@request()->header('Authorization')) {
            $response['result']['user_id'] = @getUserByToken()->id;
            $userId = @getUserByToken()->id;
        }
        $orderId = @$reqData['params']['order_master_id'];
        $orderdetails = OrderDetail::with(['productDetails','productCategoriesDetails','getOrderDetailsUnitMaster','sellerDetails','productByLanguage','defaultImage','orderMaster','getOrderSellerDetail'])->where(['product_id'=>@$reqData['params']['product_id'],'order_master_id'=>@$reqData['params']['order_master_id']])->first();

        if(!@$orderdetails){

            $response['error']['meaning'] = " You have provided wrong combination of order master id and product_id ";

            return response()->json($response);
        }
        $order = $this->order->with(['orderMasterDetails.getOrderDetailsUnitMaster','getCountry','shipping_city_details','billing_city_details','shipping_state_details','billing_state_details','getCityNameByLanguage:id,city_id,language_id,name','getBillingCountry:id,country_id,language_id,name','getBillingCityNameByLanguage:id,city_id,language_id,name'])->where('id',$orderId)->first();
            $no_of_items = $this->order_details->where(['order_master_id'=>$orderId])->count();
            $order_sellers = OrderSeller::with(['orderDetails.getOrderDetailsUnitMaster','orderDetails.productDetails.productUnitMasters','orderMerchant','orderMerchant.merchantCityDetails','orderDetails','orderDetails.productDetails','orderDetails.productDetails.productByLanguage:id,product_id,language_id,title,description','orderDetails.productDetails.defaultImage:id,image,product_id,is_default','orderDetails.productDetails.productVariants','orderDetails.productVariantDetails'])->where(['order_master_id'=>$orderId]);
            $order_sellers = $order_sellers->whereHas('orderDetails', function($q) use($orderdetails){
                $q->where('id',$orderdetails->id);
            });
            $order_sellers = $order_sellers->first();
            $response['result']['seller'] = $order_sellers;
            $response['result']['no_of_items'] = $no_of_items;
            $response['result']['order'] = $order;
        // $order = OrderSeller::with(['orderMasterTab','getCountry','shipping_city_details','billing_city_details','shipping_state_details','billing_state_details','getCityNameByLanguage:id,city_id,language_id,name','getBillingCountry:id,country_id,language_id,name','getBillingCityNameByLanguage:id,city_id,language_id,name'])->where('order_master_id',$reqData['params']['order_master_id'])->first();
        
        // $order_sellers = OrderSeller::with(['orderDetails.getOrderDetailsUnitMaster','orderDetails.productDetails.productUnitMasters','orderMerchant','orderMerchant.merchantCityDetails','orderDetails','orderDetails.productDetails','orderDetails.productDetails.productByLanguage:id,product_id,language_id,title,description','orderDetails.productDetails.defaultImage:id,image,product_id,is_default','orderDetails.productDetails.productVariants','orderDetails.productVariantDetails'])->where(['order_master_id'=>$reqData['params']['order_master_id']]);
        // $order_sellers = $order_sellers->whereHas('orderDetails', function($q) use($orderdetails){
        //     $q->where('id', $orderdetails->id);
        // });
        // $order_sellers = $order_sellers->first();

        // $response['result']['order_details'] ="";
        
        $response['result']['order_details'] = @$orderdetails;
        
        return response()->json($response);
    }
}