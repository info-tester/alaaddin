<?php

namespace App\Http\Controllers\Api\Modules\Merchant\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\User;
use Validator;
use Auth;
use App\Models\Language;
use App\Models\OrderDetail;
use App\Models\Withdraw;
use App\Admin;
use App\Merchant;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\ShippingCostRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\UserRepository;
use App\Repositories\ProductRepository;
use App\Repositories\WithdrawRepository;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;
use Excel;
use Response;
use App\Mail\SendMailWithDrawlRequest;
class FinanceController extends Controller
{
	protected $order_master,$order_details,$shipping_cost,$customers;
	public function __construct(OrderMasterRepository $order_master,
		ShippingCostRepository $shipping_cost,
		OrderDetailRepository $order_details,
		ProductVariantRepository $product_variant,
		ProductRepository $product,
		UserRepository $customers,
		WithdrawRepository $withdraw,
		Request $request)
	{
		auth()->shouldUse('merchant');
		$this->order_master            =   $order_master;
		$this->order_details           =   $order_details;
		$this->shipping_cost           =   $shipping_cost;
		$this->customers           	   =   $customers;
		$this->product_variant         =   $product_variant;
		$this->product         		   =   $product;
		$this->withdraw           	   =   $withdraw;
	}
	/**
    * @OA\Post(
    * path="/api/merchant-earning",
    * summary="Merchant Earning",
    * description="Merchant Earning",
    * operationId="earningReport",
    * tags={"Alaaddin: Merchant Earning"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant Earning",
    *    @OA\JsonContent(
    *       required={"page"},
    *           @OA\Property(property="params", type="object",
    *         	@OA\Property(property="keyword", type="string", example="ORDALD00000024"),
    *           @OA\Property(property="from_date", type="string", example="2022-08-01"),
    *           @OA\Property(property="to_date", type="string", example="2022-09-01"),
    *         	@OA\Property(property="page", type="string", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
	public function earningReport(Request $request){
		$response = ['jsonrpc'=>'2.0'];
		try 
		{
			$user = auth()->user();
			if(@$user->status == 'I'){
                $response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
            }
            if(@$user->status == 'D'){
                $response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
            }
			$reqdata = $request->json()->all();

            $validator = Validator::make($reqdata['params'],[
                'page'    => 'required|numeric',
            ],
            [
                'page.required'=>'page required',
                'page.numeric'=>'page should be number'
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
           	
			$orders 	= OrderDetail::with(['orderMaster:id,order_no','getOrderDetailsUnitMaster:id,unit_name','sellerDetails:id,fname,lname','productByLanguage:product_id,title','defaultImage:image,product_id'])->where(['seller_id'=>@$user->id,'status'=>'OD']);
			
			if(!empty(@$request['params']['keyword'])){
				$orders = $orders->whereHas('orderMaster',function($query) use($request){
					$query->Where('shipping_fname','like','%'.@$request['params']['keyword'].'%')
					->orWhere('shipping_lname','like','%'.@$request['params']['keyword'].'%')
					->orWhere('order_no',@$request['params']['keyword'])
					->orWhere('shipping_phone','like','%'.@$request['params']['keyword'].'%')
					->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".@$request['params']['keyword']."%");
				});
			}
            if(!empty(@$request['params']['from_date']) && !empty(@$request['params']['to_date'])){
                $orders = $orders->where(function($query) use($request){
                    $query->whereBetween('delivery_date',[date('Y-m-d',strtotime($request['params']['from_date'])),date('Y-m-d',strtotime($request['params']['to_date']))]);
                });
            }
            else if(!empty(@$request['params']['from_date'])){
                $orders = $orders->where(function($query) use($request){
                    $query->Where('delivery_date','>=',date('Y-m-d',strtotime($request['params']['from_date'])));
                });
            }
            else if(!empty(@$request['params']['to_date'])){
                $orders = $orders->where(function($query) use($request){
                    $query->Where('delivery_date','<=',date('Y-m-d',strtotime($request['params']['to_date'])));
                });
            }
            if(!empty(@$request['params']['status'])){
                $orders = $orders->where(function($query) use($request){
                    $query->Where('status',$request['params']['status']);
                });
            }
            $page = @$request['params']['page']?$request['params']['page']:1;
            $limit = 10;
            if($page == 1) {
                $start = 0;
            } else {
                $start = (($page -1) * $limit);
            }
            $response['result']['orders_total'] =$orders->count();
            $response['result']['orders_page_count'] =ceil($orders->count() /$limit);
            $response['result']['orders_per_page'] =$limit;
			$orders = $orders->select('id','order_master_id','product_id','product_unit_master_id','seller_id','quantity','weight','status','delivery_date','delivery_time',\DB::raw('(payable_amount - admin_commission - insurance_price) earning_from_this_product'))->orderBy('delivery_date','desc')->skip($start)->take($limit)->get();
            $merchant = Merchant::where(['id'=>$user->id])->select('id','fname','lname','email','phone','image','company_name','total_due','total_paid','net_earning')->first();
			$response['result']['seller'] = @$merchant;
			$response['result']['orders'] = @$orders;
			return response()->json($response);
		}
		catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
			// $response['error'] = $e->getMessage();
			$response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
			return response()->json($response,401);
		}
		catch (\Exception $th) {
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
             \Log::error($th->getMessage());
			// $response['error'] = $th->getMessage();
			return response()->json($response);
		}
	}
	/**
    * @OA\Post(
    * path="/api/merchant-withdraw-list",
    * summary="Merchant Withdraw List",
    * description="Merchant Withdraw List",
    * operationId="merchantWithdrawlist",
    * tags={"Alaaddin: Merchant Withdraw List"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant Withdraw List",
    *    @OA\JsonContent(
    *       required={"page"},
    *           @OA\Property(property="params", type="object",
    *         	@OA\Property(property="page", type="string", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Please try again",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Please try again")
    *        )
    *     )
    * )
    */
	public function merchantWithdrawlist(Request $request){
		$response = ['jsonrpc'=>'2.0'];
		try 
		{
			$user = auth()->user(); 
			if(@$user->status == 'I'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
			if(@$user->status == 'D'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
			$merchant = auth()->user(); 
			
			$reqdata = $request->json()->all();
			$validator = Validator::make($reqdata['params'],[
                'page'    => 'required|numeric'
            ],
            [
                'page.required'=>'Page required',
                'page.number'=>'Page required'
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
			$withdraws = Withdraw::where('seller_id',@$user->id)->whereNotIn('status',['D'])->select('id','seller_id','account_no','account_name','bank_name','iban_number','branch_name','amount','balance','payment_method','ifsc_code','request_date','payment_date','status','created_at');
			
			$page = @$request['params']['page']?$request['params']['page']:1;
            
            $limit = 1;
            
            if($page == 1) {
                $start = 0;
            } else {
                $start = (($page -1) * $limit);
            }

            $response['result']['withdraws_total'] =$withdraws->count();
            $response['result']['withdraws_page_count'] =ceil($withdraws->count() /$limit);
            $response['result']['withdraws_per_page'] =$limit;
			$withdraws = $withdraws->orderBy('id','desc')->skip($start)->take($limit)->get();
			$response['result']['withdraws'] = @$withdraws;
			$response['result']['total_due'] = @$merchant->total_due;
			return response()->json($response);
		}
		catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
			// $response['error'] = $e->getMessage();
			$response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
			return response()->json($response);
		}
		catch (\Exception $th) {
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
			// $response['error'] = $th->getMessage();
			return response()->json($response);
		}
	}
	/**
    * @OA\Get(
    * path="/api/get-merchant-due",
    * summary="Get Merchant Due",
    * description="Get Merchant Due",
    * operationId="getMerchantDue",
    * tags={"Alaaddin: Get Merchant Due"},
    * security={{"bearer_token":{}}},
    * @OA\Response(
    *    response=422,
    *    description="",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Please try again")
    *        )
    *     )
    * )
    */
	public function getMerchantDue(Request $request){
		$response = ['jsonrpc'=>'2.0'];
		try 
		{
			$m = auth()->user();
			$response['result']['total_due'] = @$m->total_due;
			return response()->json($response);
		}
		catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
			$response['error'] =__('errors.-33085');
			$response['error']['meaning'] = $e->getMessage();
			return response()->json($response);
		}
		catch (\Exception $th) {
			$response['error'] =__('errors.-5001');
			$response['error']['meaning'] = $th->getMessage();
			\Log::error($th->getMessage());
			return response()->json($response);
		}
	}
	/**
    * @OA\Post(
    * path="/api/show-merchant-due",
    * summary="Show Merchant Due",
    * description="Show Merchant Due",
    * operationId="showMerchantDue",
    * tags={"Alaaddin: Show Merchant Due"},
    * security={{"bearer_token":{}}},
    * @OA\Response(
    *    response=422,
    *    description="",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Please try again")
    *        )
    *     )
    * )
    */
	public function showMerchantDue(Request $request){
		$response = ['jsonrpc'=>'2.0'];
		try 
		{
			$merchant = auth()->user();
			$response['result']['total_due'] = @$merchant->total_due;
			return response()->json($response);
		}
		catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
			$response['error'] =__('errors.-33085');
			$response['error']['meaning'] = $e->getMessage();
			return response()->json($response,401);
		}
		catch (\Exception $th) {
			$response['error'] =__('errors.-5001');
			$response['error']['meaning'] = $th->getMessage();
			\Log::error("show-merchant-due");
            \Log::error($th->getMessage());
			return response()->json($response);
		}
	}
	/**
    * @OA\Post(
    * path="/api/merchant-withdraw-request",
    * summary="Merchant Withdraw Request",
    * description="Merchant Withdraw Request",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Withdraw Request"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant Withdraw Request paymenth method Bank ",
    *    @OA\JsonContent(
    *       required={"amount","account_no","account_name","bank_name","branch_name","ifsc_code"},
    *           @OA\Property(property="params", type="object",
    *         	@OA\Property(property="amount", type="string", format="amount", example="400"),
    *         	@OA\Property(property="account_no", type="string", format="amount", example="1234343434"),
    *         	@OA\Property(property="account_name", type="string", format="amount", example="YOUR NAME"),
    *         	@OA\Property(property="bank_name", type="string", format="amount", example="BANK NAME"),
    *         	@OA\Property(property="branch_name", type="string", format="amount", example="BRANCH NAME"),
    *         	@OA\Property(property="ifsc_code", type="string", format="amount", example="IFSC CODE"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
	public function requestWithdraw(Request $request){
		$response = ['jsonrpc'=>'2.0'];
		try{
			$reqData = $request->json()->all();
            $user = auth()->user();
            if(@$user->total_due <= 0){
                $response['error'] = __('errors.-5042');
                $response['error']['meaning'] = "Your Due  amount : ".$user->total_due."  So you cannot withdraw!";
                return response()->json($response);
            }
            
			$validator = Validator::make(@$reqData['params'], [
                'amount'    => 'required|numeric|min:1',
                'account_no'    => 'required|numeric',
                'account_name'    => 'required',
                'bank_name'    => 'required',
                'branch_name'    => 'required',
                'ifsc_code'    => 'required'
            ],[
                'amount.required'=>'Amount is required',
                'amount.numeric'=>'Amount should be numeric',
                'amount.min'=>'Amount is required and should br greater than 0',
                'account_no.required'=>'Account number is required',
                'account_no.numeric'=>'Account number should be numeric',
                'account_name.required'=>'Account name is required',
                'bank_name.required'=>'Bank name is required',
                'branch_name.required'=>'Branch name is required',
                'ifsc_code.required'=>'Ifsc code is required'
            ]);
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                
                $response['error'] =__('errors.-5001');
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
			$user = auth()->user();
			if(@$user->status == 'I'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
			if(@$user->status == 'D'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
            if(@$user->total_due <= 0){
                $response['error'] = __('errors.-5042');
                $response['error']['meaning'] = "Your Due  amount : ".$user->total_due."  So you cannot withdraw!";
                return response()->json($response);
            }
            
			$checkDup = $this->checkDuplicate(@$user->id);
			if($checkDup == 1){
				if(@$user->total_due >= @$reqData['params']['amount']){
					
					$new['seller_id'] = @$user->id;
					$new['bank_name'] = $reqData['params']['bank_name'];
					$new['account_name'] = $reqData['params']['account_name'];
					$new['account_no'] = $reqData['params']['account_no'];
					$new['ifsc_code'] = $reqData['params']['ifsc_code'];
					$new['iban_number'] = $reqData['params']['ifsc_code'];
					$new['payment_method'] = "Bank";
					$new['branch_name'] = $reqData['params']['branch_name'];
					$new['request_date'] = now();
					$new['amount'] = $reqData['params']['amount'];
					$new['balance'] = @$user->total_due;
					$new['status'] = 'N';
					$withdraw = $this->withdraw->create($new);
					if(@$withdraw){
						$response['result'] =  __('success.-4045');
                        $response['result']['meaning'] = "You have successfully requested the withdrawl!";
					}else{
						$response['error'] = __('errors.-5042');
						$response['error']['meaning'] = " You have not requested the withdrawl! ";
                        return response()->json($response);
					}
					$data['merchant_name'] 		= 	@$user->fname." ".@$user->lname;
					$data['merchant_email'] 	=	@$user->email;
					$data['created_at'] 		=	now();
					$data['merchant_phone'] 	=	@$user->phone;
					$data['account_no'] 		=	@$user->account_number;
					$data['account_name'] 		=	@$user->account_name;
					$data['bank_name'] 			=	@$user->bank_name;
					$data['iban_number'] 		=	@$user->ifsc_number;
					$data['branch_name'] 		=	@$user->branch_name;
					$data['amount'] 			=	@$reqData['params']['amount'];
					$data['balance'] 			=	@$user->total_due;
					$data['request_id'] 		=	@$withdraw->id;
					$data['balance'] 			=	@$user->total_due;
					
					if(@$withdraw->status == 'N'){
						$data['wstatus'] 		=	"New";	
					}

					$admin_notify = Admin::where('type','A')->first();
					if(@$admin_notify){
						if(@$admin_notify->email){
							$contactList[0] = $admin_notify->email;    	
						}
						$contactListEmail3 = array_unique($contactList);
						$data['bcc'] = $contactListEmail3;
					}
					try{
						Mail::send(new SendMailWithDrawlRequest($data));
					}catch(\Exception $e){
                        \Log::error($e->getMessage());
					}
					
					$response['result'] =  __('success.-4045');	
                    $response['result']['meaning'] =  "You have successfully requested the withdrawl!"; 
					return response()->json($response);
				}else{
					$response['error'] = __('errors.-5042');
					$response['error']['meaning'] = "Your Due  amount : ".$user->total_due."  So you cannot withdraw!";
					// return Response::json($response);
					return response()->json($response);
				}
			}
			else{
				$response['error'] = __('errors.-5096');
                $response['error']['meaning'] = "Previously you have already requested a withdrawl and until admin approve/reject that request you cannot request another!";
            	// return Response::json($response);
            	return response()->json($response);
			}
		}
		catch(\Exception $e){
			// $response['error']['message'] = $e->getMessage();
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            \Log::error("merchant-withdraw-request");
            \Log::error($e->getMessage());
            // return Response::json($response);
            return response()->json($response);
		}
		
		return response()->json($response);
	}
	/**
    * @OA\Post(
    * path="/api/merchant-delete-request",
    * summary="Merchant Request Delete",
    * description="Merchant Request Delete",
    * operationId="requestdelete",
    * tags={"Alaaddin: Merchant Request Delete"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant  Request delete",
    *    @OA\JsonContent(
    *       required={"amount"},
    *           @OA\Property(property="params", type="object",
    *         	@OA\Property(property="id", type="string", example="25"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
	public function requestdelete(Request $request){
		$response = ['jsonrpc'=>'2.0'];
		try{
			$reqData = $request->json()->all();
			$validator = Validator::make(@$reqData['params'], [
                'id'    => 'required|numeric',
            ]);
            if($validator->fails()){
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
			$user = auth()->user();
			$w = $this->withdraw->where(['id'=>$reqData['params']['id']])->whereIn('status',['N'])->first();
			if(@$w){
				$this->withdraw->where(['id'=>$reqData['params']['id']])->whereIn('status',['N'])->update(['status'=>'D']);
				$response['result'] =  __('success.-4045');	
				$response['result']['meaning'] =  " Request  deleted successfully ";
			}else{
				$response['error'] = __('errors.-5042');
				$response['error']['meaning'] = " Request deleted ";	
			}
			return Response::json($response);
		}
		catch(\Exception $e){
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
			// $response['error']['message'] = $e->getMessage();
			\Log::error("merchant-delete-request");
            \Log::error($e->getMessage());
            return Response::json($response);
		}
		
		return response()->json($response);
	}
	public function checkDuplicate($sellerIds){
		$order = $this->withdraw->where(['seller_id'=>@$sellerIds,'status'=>'N'])->first();
		if(!empty(@$order)){
			return 0;
		}else{
			return 1;
		}
	}
}
