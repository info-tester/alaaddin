<?php

namespace App\Http\Controllers\Api\Modules\Merchant\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
#models
use App\User;
use App\Merchant;
use App\Models\UserRegId;
use App\Models\Setting;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Hash;
use Auth;

class GoogleSignupLoginController extends Controller
{
    /**
    * @OA\Post(
    * path="/api/merchant-login-via-google-step-1",
    * summary="merchant Login Via Google Step 1",
    * description="merchant Google Login Or Sign Up",
    * operationId="merchantLoginViaGoogleStep1",
    * tags={" Alaaddin : merchant Login Via Google Step 1"},
    * @OA\RequestBody(
    *    required=true,
    *    description=" merchant Login Via Google Step 1 ",
    *    @OA\JsonContent(
    *       required={"name","phone","email","google_id"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="name", type="string", format="name", example="Fname Lname"),
    *               @OA\Property(property="email", type="string", format="email", example="shfa@gmail.com"),
    *               @OA\Property(property="phone", type="string", format="email", example="1234567898"),
    *               @OA\Property(property="image", type="string", format="image", example="https://lh3.googleusercontent.com/a/AATXAJwjjdObnv4weZNG8W1OVKJnJRaFD-xyBQPbG3Vf=s96-c"),
    *               @OA\Property(property="google_id", type="string", format="google_id", example="435352352523532352"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description=" merchant Login Via Google Step 1 ",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry Please try again")
    *        )
    *     )
    * )
    */
    public function merchantLoginViaGoogleStep1(Request $request) {
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            auth()->shouldUse('merchant');
            $reqData = $request->json()->all();
            #if google id or email is null then 
            if(!@$reqData['params']['google_id'] || !@$reqData['params']['email']) {
                $response['error'] = ERRORS['-33029'];
                return response()->json($response);
            }
            if(@$reqData['params']['email'] == "seller@gmail.com"){
                    
                $otp = 100000;
                
            }else{

                $otp = mt_rand(100000, 999999);

            }
            $merchant = Merchant::where(['google_id' => $reqData['params']['google_id']])->where('status', '!=', 'D')->first();
            
            #if previously registered using google then login the user.
            if(@$merchant) {
                if($merchant->status == 'A' || $merchant->status == 'U') {
                    
                    Merchant::where('id', $merchant->id)->update(['login_via_otp'=>$otp,'is_phone_verified'=>'N']);
                    $merchant = Merchant::where(['google_id' => $reqData['params']['google_id']])->where('status', '!=', 'D')->first();
                    $phnno = $merchant->phone;
                    $message = "Mobile verify OTP is ".$otp." - ALLAAD";
                    $smsr = sendSms($phnno,$message);
                    $response['result'] = __('success.-4074');
                    $response['result']['login_via_otp'] = $otp;
                    $response['result']['phone'] = $merchant->phone;
                    $response['result']['is_phone_verified'] = $merchant->is_phone_verified;

                    // $is_phone = Merchant::where('phone', $reqData['params']['phone'])->where('status', '!=', 'D')->count();
                    // if($is_phone>0){
                    //     $response['error'] = ERRORS['-33088'];
                    //     return response()->json($response);
                    // }
                    // $response['error'] = ERRORS['-33000'];
                    
                    return response()->json($response);
                } else {
                    $response['error'] = ERRORS['-33086'];
                    return response()->json($response);
                }
            } else {
                # if not matched with google id then check using email.
                $merchant = Merchant::where(['email' => $reqData['params']['email']])->where('status', '!=', 'D')->first();
                if(@$merchant) {
                    if($merchant->status == 'A' || $merchant->status == 'U') {

                        if(@$merchant->is_phone_verified){

                            Merchant::where('id', $merchant->id)->update(['login_via_otp'=>$otp,'is_phone_verified'=>'N']);

                            $merchant = Merchant::where(['email' => $reqData['params']['email']])->where('status', '!=', 'D')->first();
                            
                            $phnno = $merchant->phone;
                            $message = "Mobile verify OTP is ".$otp." - ALLAAD";
                            $smsr = sendSms($phnno,$message);
                            $response['result'] = __('success.-4074');
                            $response['result']['login_via_otp'] = $otp;
                            $response['result']['phone'] = $merchant->phone;
                            $response['result']['is_phone_verified'] = $merchant->is_phone_verified;
                        }
                        


                        $is_phone = Merchant::where('phone', $reqData['params']['phone'])->where('status', '!=', 'D')->count();
                        if($is_phone>0){
                            $response['error'] = ERRORS['-33088'];
                            return response()->json($response);
                        }
                        $response['error'] = ERRORS['-33000'];
                        return response()->json($response);
                    } else if($merchant->status == 'AA') {
                        $response['error'] = ERRORS['-33089'];
                        return response()->json($response);
                    } else if($merchant->status == 'I') {
                        $response['error'] = ERRORS['-33003'];
                        return response()->json($response);
                    } else if($merchant->status == 'D') {
                        $response['error'] = ERRORS['-33005'];
                        return response()->json($response);
                    }
                } else {
                    $validator = Validator::make($reqData['params'],[
                        'google_id'    => 'required',
                        'name'    => 'required|string|max:255',
                        'email'    => 'required|string|email|max:255|regex:/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/',
                        'phone'   => 'required|numeric|digits_between:10,12'
                    ],
                    [
                        'google_id.required'=>'google_id is required',
                        'name.required'=>'name is required',
                        'name.string'=>'name should be string',
                        'name.max'=>'name should be 255 charecters',
                        'email.required'=>'Please provide valid email id',
                        'email.email'=>'Please provide valid email-id',
                        'email.regex'=>'Please sprovide valid email-id',
                        'email.max'=>'email should be 255 charecters',
                        'phone.required'=>'phone number is required',
                        'phone.numeric'=>'phone number should be number',
                        'phone.digits_between'=>'phone number should be 10 digits to maximum 12 digits'
                    ]);
                    if($validator->fails()){
                        $response['error']['message'] = 'Error';
                        $response['error']['meaning'] = $validator->errors()->first();
                        return response()->json($response);
                    }
                    $is_email = Merchant::where('email', $reqData['params']['email'])->where('status', '!=', 'D')->count();
                    if($is_email>0){
                        $response['error'] = ERRORS['-33000'];
                        return response()->json($response);
                    }
                    $is_phone = Merchant::where('phone', $reqData['params']['phone'])->where('status', '!=', 'D')->count();
                    if($is_phone>0){
                        $response['error'] = ERRORS['-33088'];
                        return response()->json($response);
                    }
                    # new user, need to register
                    $name = explode(' ', $reqData['params']['name']);
                    if(@$reqData['params']['image']) {
                        $image = rand(11111,99999).time().'.jpg';
                        # save file to folder
                        $ch = curl_init();
                        curl_setopt ($ch, CURLOPT_URL, $reqData['params']['image']);
                        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
                        $data = curl_exec($ch);
                        curl_close($ch);
                        $file = fopen('storage/app/public/customer/profile_pics/'.$image, 'w+');
                        fputs($file, $data);
                        fclose($file);
                    }
                    $setting = Setting::first();
                    $params = [
                        'fname'         => @$name[0],
                        'lname'         => @$name[1],
                        'email'         => $reqData['params']['email'],
                        'phone'         => $reqData['params']['phone'],
                        'google_id'     => $reqData['params']['google_id'],
                        'login_via_otp'     => $otp,
                        'is_phone_verified'     => 'N',
                        'status'        => 'U',
                        'commission'        => @$setting->commission,
                        'image'         =>  @$image,
                        'country'       => '101'
                    ];
                    $merchant = Merchant::create($params);
                    $fname = str_slug(@$merchant->fname);
                    $lname = str_slug(@$merchant->lname);
                    $slug = $fname.'-'.$lname;
                    $slug = $slug.'-'.$merchant->id;
                    Merchant::whereId($merchant->id)->update(['slug' => $slug]);
                    
                    if($merchant->status == 'A') {
                        
                        $phnno = $merchant->phone;
                        $message = "Mobile verify OTP is ".$otp." - ALLAAD";
                        $smsr = sendSms($phnno,$message);

                        $response['result'] = __('success.-4074');
                        $response['result']['login_via_otp'] = $otp;
                        $response['result']['phone'] = $merchant->phone;
                        $response['result']['is_phone_verified'] = $merchant->is_phone_verified;
                        $response['result']['token'] = JWTAuth::fromUser($merchant);
                    } else if($merchant->status == 'U') {
                        
                        $phnno = $merchant->phone;
                        $message = "Mobile verify OTP is ".$otp." - ALLAAD";
                        $smsr = sendSms($phnno,$message);

                        $response['result'] = __('success.-4074');
                        $response['result']['login_via_otp'] = $otp;
                        $response['result']['phone'] = $merchant->phone;
                        $response['result']['is_phone_verified'] = $merchant->is_phone_verified;
                        return response()->json($response);
                    } else if($merchant->status == 'AA') {
                        $response['error'] = ERRORS['-33089'];
                        return response()->json($response);
                    } else if($merchant->status == 'I') {
                        $response['error'] = ERRORS['-33003'];
                        return response()->json($response);
                    } else if($merchant->status == 'D') {
                        $response['error'] = ERRORS['-33005'];
                        return response()->json($response);
                    }
                    
                }
            }
            $merchant = Merchant::where('id',@$merchant->id)->first();
            
            return response()->json($response);
        } catch (\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/check-merchant-info-by-google-id",
    * summary="check-merchant-info-by-google-id",
    * description="check merchant info by google id",
    * operationId="checkmerchantinfobygoogleid",
    * tags={" Alaaddin : check merchant info by google id"},
    * @OA\RequestBody(
    *    required=true,
    *    description=" check merchant info by google id ",
    *    @OA\JsonContent(
    *       required={"google_id"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="google_id", type="string", format="google_id", example="435352352523532352"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description=" check-merchant-info-by-google-id ",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, Please try again")
    *        )
    *     )
    * )
    */
    public function checkmerchantinfobygoogleid(Request $request) {
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            auth()->shouldUse('merchant');
            $reqData = $request->json()->all();
            
            if(!@$reqData['params']['google_id']) {
                $response['error'] = ERRORS['-33029'];
                return response()->json($response);
            }
            $merchant = Merchant::where(['google_id' => $reqData['params']['google_id']])->where('status', '!=', 'D')->first();

            if(@$merchant) {
                if(@$merchant->status == 'AA'){
                    $response['error'] = ERRORS['-33089'];
                    return response()->json($response);
                }
                if(@$merchant->status == 'I'){
                    $response['error'] = ERRORS['-33039'];
                    $response['error']['meaning'] = "Admin inactive you! ";  
                    return response()->json($response);  
                }else{
                    
                    if(@$merchant->is_phone_verified == 'Y'){
                        $response['result']['is_phone_verified'] = $merchant->is_phone_verified;  
                        $response['result']['merchant'] = $merchant;    
                        $response['result']['token'] = JWTAuth::fromUser($merchant);
                        return response()->json($response);
                    }else{
                        $response['result']['is_phone_verified'] = $merchant->is_phone_verified;  
                        return response()->json($response);
                    }
                }
                
            }else{
                $response['result']['is_phone_verified'] = 'N';
                return response()->json($response);
                
            }
            return response()->json($response);
        } catch (\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/merchant-otp-verification",
    * summary=" merchant-otp-verification",
    * description="merchant-otp-verification",
    * operationId="MerchantOtpVerification",
    * tags={"Alaaddin: merchant-otp-verification"},
    * @OA\RequestBody(
    *    required=true,
    *    description="merchant-otp-verification",
    *    @OA\JsonContent(
    *       required={"phone","login_via_otp"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="phone", type="string", example="1234567898"),
    *           @OA\Property(property="login_via_otp", type="integer", example="111111"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry,")
    *        )
    *     )
    * )
    */
    public function MerchantOtpVerification(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            auth()->shouldUse('merchant');
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'], [
                'phone' => 'required',
                'login_via_otp' => 'required|max:6'
            ],[
                'phone.required' => 'Please provide valid email-id or phone number',
                'login_via_otp.required' => 'Please provide login_via_otp'
            ]);
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $userData = Merchant::select('id','email','fname','lname','status', 'is_email_verified','login_via_otp')
            ->where(['login_via_otp' => $reqdata['params']['login_via_otp']])
            ->whereNotIn('status',['D']);
            $userData = $userData->where(function($q) use ($reqdata){
                $q->where('phone',$reqdata['params']['phone'])->orWhere('phone',$reqdata['params']['phone']);
            });
            $userData = $userData->first();
            
            if(@$userData){
                if(@$userData->status == 'U'){
                    Merchant::where(['id'=>$userData->id])->update(['is_phone_verified' => 'Y', 'login_via_otp' => NULL,'status'=>'AA']);
                }else{
                    if(is_numeric($reqdata['params']['phone'])){
                        Merchant::where(['id'=>$userData->id])->update(['is_phone_verified' => 'Y', 'login_via_otp' => NULL]);
                    }else{
                        Merchant::where(['id'=>$userData->id])->update(['is_email_verified' => 'Y', 'login_via_otp' => NULL]);
                    }
                }
                $userData = Merchant::where(['id'=>$userData->id])->select('is_phone_verified','id','firebaseToken_id','driver_id','slug','fname','lname','phone','email','google_id','login_via_otp','tmp_email','email1','email2','email3','image','cover_pic','company_name','company_type','company_details','commission','shipping_cost','inside_shipping_cost','international_order','premium_merchant','address','location','lat','lng','address_note','building_number','avenue','city','city_id','state','zipcode','country','bank_name','account_name','account_number','ifsc_number','branch_name','payment_mode','total_earning','total_seller_commission','total_insurance_price','total_loading_inloading_price','total_admin_commission','total_product_price','invoice_in','invoice_out','total_commission','total_paid','total_due','email_vcode','is_email_verified','email_verified_at','status','hide_merchant','rate','total_no_review','total_review_rate','remember_token','show_order_processed','gross_earning','net_earning','commission_paid','applicable_city_delivery','applicable_city_delivery_external','minimum_commission','hide_customer_info','notify_merchant_on_delivery_internal','notify_merchant_on_delivery_external','notify_customer_by_email_internal','notify_customer_by_email_external','notify_customer_by_whatsapp_internal','notify_customer_by_whatsapp_external','created_at','updated_at','password')->first();
                if(@$userData->status == 'AA'){
                    $response['error'] = ERRORS['-33089'];
                    return response()->json($response);
                }
                $response['result']['userdata'] = $userData;
                $token = JWTAuth::fromUser($userData);
                $response['result']['token'] = $token;

                return response()->json($response);
            } else {
                $response['error'] = ERRORS['-33039'];
                return response()->json($response);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            $response['error'] = ERRORS['-33009'];
            $response['error']['message'] = $e->getMessage();
            return Response::json($response);
        }
    }
}
