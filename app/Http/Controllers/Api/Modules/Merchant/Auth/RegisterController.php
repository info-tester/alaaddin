<?php

namespace App\Http\Controllers\Api\Modules\Merchant\Auth;

use App\Merchant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use App\Models\Country;
use App\Models\City;
use App\Models\CitiDetails;
use App\Repositories\CityRepository;
use App\Repositories\CitiDetailsRepository;
use App\Models\Customer;
use App\Models\Setting;
use App\Mail\VerificationMail;
use App\Mail\SendMailOtp;
use JWTFactory;
use JWTAuth;
use Response;
use Mail;
use Session;
class RegisterController extends Controller
{
    public function __construct(CityRepository $city,
        CitiDetailsRepository $city_details)
    {
        $this->city                     =   $city;
        $this->city_details             =   $city_details;
    }
    /**
    * @OA\Post(
    * path="/api/merchant-register",
    * summary="Merchant Sign in",
    * description="Register by merchant details",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Register"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant credentials",
    *    @OA\JsonContent(
    *       required={"fname","lname","phone","email","company_name","address","city","state","zipcode","avenue","building_number","address_note","payment_method","country","password"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="fname", type="string", format="name", example="user fname"),
    *               @OA\Property(property="lname", type="string", format="name", example="user lname"),
    *               @OA\Property(property="phone", type="integer", format="phone", example="1234567890"),
    *               @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *               @OA\Property(property="company_name", type="string", format="company_name", example="Company Name"),
    *               @OA\Property(property="address", type="string", format="address", example="Address"),
    *               @OA\Property(property="city", type="integer", format="city", example="1"),
    *               @OA\Property(property="state", type="integer", format="state", example="1"),
    *               @OA\Property(property="zipcode", type="integer", format="state", example="700064"),
    *               @OA\Property(property="avenue", type="string", format="avenue", example="Avenue"),
    *               @OA\Property(property="building_number", type="string", format="building_number", example="Building Number"),
    *               @OA\Property(property="address_note", type="string", format="address_note", example="Address Note"),
    *               @OA\Property(property="payment_method", type="string", format="payment_method", example="C"),
    *               @OA\Property(property="country", type="integer", format="country", example="101"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function register(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'],[
                'fname'         => 'required|max:255',
                'lname'         => 'required|max:255',
                'phone'         => 'required|numeric',
                'email'         => 'required|email|max:255',
                'company_name'  => 'required'
                // 'address'       => 'required',
                // 'city'          => 'required',
                // 'state'         => 'required',
                // 'zipcode'       => 'required',
                // 'avenue'        => 'required',
                // 'building_number'=> 'required',
                // 'address_note'  => 'required',
                // 'payment_method'  => 'required|in:A,O,C',
                // 'country'       => 'required',
                // 'password'      => 'required|min:6|confirmed',
            ]);
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $is_email = Merchant::where('email', @$reqdata['params']['email'])->where('status', '!=', 'D')->count();
            if($is_email>0){
                $response['error'] = ERRORS['-33000'];
                return response()->json($response);
            }
            $is_phone = Merchant::where('phone', @$reqdata['params']['phone'])->where('status', '!=', 'D')->count();
            if($is_phone>0){
                $response['error'] = ERRORS['-33088'];
                return response()->json($response);
            }
            $otp = mt_rand(100000,999999);
            // dd($name);
            if(@$reqdata['params']['image']) {
                $image     = @$reqdata['params']['image']; 
                $imgName   = time().".".$image->getClientOriginalExtension();
                $image->move('storage/app/public/profile_pics', $imgName);
            } else {
                $imgName = '';
            }
            $setting = Setting::first();
            $merchant =  Merchant::create([
                'fname'         => @$reqdata['params']['fname'],
                'lname'         => @$reqdata['params']['lname'],
                'email'         => @$reqdata['params']['email'],
                'phone'         => @$reqdata['params']['phone'],
                'company_name'  => @$reqdata['params']['company_name'],
                'address'       => @$reqdata['params']['address'],
                'city'          => @$reqdata['params']['city'],
                'city_id'       => @$reqdata['params']['city'],
                'state'         => @$reqdata['params']['state'],
                'zipcode'       => @$reqdata['params']['zipcode'],
                'avenue'        => @$reqdata['params']['avenue'],
                'building_number'=> @$reqdata['params']['building_number'],
                'address_note'  => @$reqdata['params']['address_note'],
                // 'country'       => @$reqdata['params']['country'],
                'payment_mode' => @$reqdata['params']['payment_method'],
                'image'         => $imgName,
                'email_vcode'   => @$otp,
                'login_via_otp'   => @$otp,
                'location'      => @$reqdata['params']['location'],
                'lat'           => @$reqdata['params']['lat'],
                'lng'           => @$reqdata['params']['lng'],
                'commission'    => @$setting->commission,
            ]);
            if(!empty(@$merchant)){
                $fname = str_slug(@$reqdata['params']['fname']);
                $lname = str_slug(@$reqdata['params']['lname']);
                $slug = $fname.'-'.$lname;
                $slug = $slug.'-'.$merchant->id;
                Merchant::whereId($merchant->id)->update(['slug' => $slug]);
                $merchant['mailBody'] = 'registered';
                $phnno = $reqdata['params']['phone'];
                $message = "Mobile verify OTP is ".@$otp." - ALLAAD";
                $smsr = sendSms($phnno,$message);
                try{
                    // $this->sendVerificationMail($merchant);
                }catch(\Exception $e){
                    
                }
            }
            //mail send end
            $response['result'] = __('success.-4074');
            $response['result']['meaning'] = "Otp has been send to ".@$reqdata['params']['phone'];
            $response['result']['email'] = $reqdata['params']['email'];
            $response['result']['otp'] = $otp;
            $response['result']['merchant'] = $merchant;
            $response['result']['commission'] = @$setting->commission;
            $response['result']['sendsms'] = @$smsr;
            return response()->json($response);
        } catch (\Exception $e) {
            // $response['error']['message'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            \Log::error($e->getMessage());
            return Response::json($response);
        }
    }
    public function sendVerificationMail($reqData)  {        
        Mail::send(new VerificationMail($reqData));
    }
    /**
    * @OA\Post(
    * path="/api/merchant-email-verify",
    * summary="Merchant Email Verify",
    * description="Verify by merchant email",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Email Verify"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant credentials",
    *    @OA\JsonContent(
    *       required={"email","vcode"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *           @OA\Property(property="vcode", type="integer", format="vcode", example="111111"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function emailVerify(Request $request){
        // $response = [
        //     'jsonrpc' => '2.0'
        // ];
        // try {
        //     $reqdata = $request->json()->all();
        //     $validator = Validator::make($reqdata['params'], [
        //         'email' => 'required|email',
        //         'vcode' => 'required|max:6'
        //     ]);
        //     if($validator->fails()){
        //         // $response['error'] = $validator->errors();
        //         $response['error']['message'] = 'Error';
        //         $response['error']['meaning'] = $validator->errors()->first();
        //         return response()->json($response);
        //     }
        //     $userData = Merchant::select('id','email','fname','lname','status', 'is_email_verified')
        //     ->where(['email'=> $reqdata['params']['email'],'email_vcode' => $reqdata['params']['vcode']])
        //     ->where('status', '!=', 'D')
        //     ->first();
        //     if(@$userData){
        //         Merchant::where(['email'=>$reqdata['params']['email'],'email_vcode' => $reqdata['params']['vcode']])
        //         ->where('status', '!=', 'D')
        //         ->update(['is_email_verified' => 'Y', 'email_vcode' => NULL,'status'=>'AA']);

        //         $userData = Merchant::select('id','email','fname','lname','status', 'is_email_verified')
        //         ->where(['email'=> $reqdata['params']['email']])
        //         ->where('status', 'AA')
        //         ->first();
        //         $response['result']['userdata'] = $userData;
        //         return response()->json($response);
        //     } else {
        //         $response['error'] = ERRORS['-33039'];
        //         return response()->json($response);
        //     }
        // } catch (\Exception $e) {
        //     $response['error']['message'] = $e->getMessage();
        //     return Response::json($response);
        // }
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'], [
                'email' => 'required',
                'vcode' => 'required|max:6'
            ],[
                'email.required' => 'Please provide valid email-id or phone number',
                'vcode.required' => 'Please provide login_via_otp'
            ]);
            if($validator->fails()){
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $userData = Merchant::select('id','email','fname','lname','status', 'is_email_verified','login_via_otp')
            ->where(['email'=>$reqdata['params']['email'],'login_via_otp' => $reqdata['params']['vcode']])
            ->orWhere(['phone'=>$reqdata['params']['email'],'login_via_otp' => $reqdata['params']['vcode']])
            ->where('status', '!=', 'D')
            ->first();
            if(@$userData){
                if(@$userData->status == 'U'){
                    Merchant::where(['email'=>$reqdata['params']['email'],'login_via_otp' => $reqdata['params']['vcode']])->orWhere(['phone'=> $reqdata['params']['email'],'login_via_otp' => $reqdata['params']['vcode']])
                ->where('status', '!=', 'D')
                ->update(['is_email_verified' => 'Y', 'login_via_otp' => NULL,'status'=>'AA']);
                }else{
                    Merchant::where(['email'=>$reqdata['params']['email'],'login_via_otp' => $reqdata['params']['vcode']])->orWhere(['phone'=> $reqdata['params']['email'],'login_via_otp' => $reqdata['params']['vcode']])
                ->where('status', '!=', 'D')
                ->update(['is_email_verified' => 'Y', 'login_via_otp' => NULL]);
                }
                

                $userData = Merchant::where(['email'=> $reqdata['params']['email']])
                ->orWhere(['phone'=> $reqdata['params']['email']])
                ->where('status', 'AA')
                ->first();
                if(@$userData->status == 'AA'){
                    $response['error'] = ERRORS['-33089'];
                    return response()->json($response);
                }
                $response['result']['userdata'] = $userData;
                $token = JWTAuth::fromUser($userData);
                $response['result']['token'] = $token;

                return response()->json($response);
            } else {
                $response['error'] = ERRORS['-33039'];
                return response()->json($response);
            }
        } catch (\Exception $e) {
            
            $response['error'] = ERRORS['-33009'];
            $response['error']['message'] = $e->getMessage();
            \Log::error($e->getMessage());
            return Response::json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/merchant-resend-code",
    * summary="Merchant resend email verify code",
    * description="Verify code resend email",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant verify code resend"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant credentials",
    *    @OA\JsonContent(
    *       required={"email"},
    *           @OA\Property(property="params", type="object",
    *           @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function resendVcode(Request $request)
    {
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqdata = $request->json()->all();
            if(@$reqdata['params']['email']==null) {
                $response['error'] = ERRORS['-33008'];
                return Response::json($response);
            }
            $getVcode = Merchant::where(['email'=>$reqdata['params']['email']])->whereIn('status', ['A','I','U','AA'])->first();

            if(@$getVcode) {
                if(@$getVcode->email_vcode) {
                    $otp = $getVcode->email_vcode;
                } else {
                    $otp = mt_rand(100000, 999999);
                }
                Merchant::where('email', $getVcode->email)->update(['email_vcode' => $otp]);
                $merchant = Merchant::where('email', $getVcode->email)->first();
                $merchant['mailBody'] = 'registered';
                $this->sendVerificationMail($merchant);
                $response['result'] = ERRORS['-33079'];
                $response['result']['vcode']   = $otp;
            } else {
                $response['error'] = ERRORS['-33051'];
            }
            return Response::json($response);
        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $exception) {
            \Log::error($exception->getMessage());
            $response['error'] = ERRORS['-33012'];
            $response['error']['meaning'] = $exception->getMessage();
            return Response::json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/merchant-otp-verification-by-login",
    * summary="Merchant MerchantOtpVerifyByLogin",
    * description="MerchantOtpVerifyByLogin",
    * operationId="MerchantOtpVerifyByLogin",
    * tags={"Alaaddin: Merchant MerchantOtpVerifyByLogin"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant MerchantOtpVerifyByLogin",
    *    @OA\JsonContent(
    *       required={"email_or_phone","login_via_otp"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="email_or_phone", type="string", example="1234567898"),
    *           @OA\Property(property="login_via_otp", type="integer", example="111111"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function MerchantOtpVerifyByLogin(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'], [
                'email_or_phone' => 'required',
                'login_via_otp' => 'required|max:6'
            ],[
                'email_or_phone.required' => 'Please provide valid email-id or phone number',
                'login_via_otp.required' => 'Please provide login_via_otp'
            ]);
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $userData = Merchant::select('id','email','fname','lname','status', 'is_email_verified','login_via_otp')
            ->where(['login_via_otp' => $reqdata['params']['login_via_otp']])
            ->whereNotIn('status',['D']);
            $userData = $userData->where(function($q) use ($reqdata){
                $q->where('email',$reqdata['params']['email_or_phone'])->orWhere('phone',$reqdata['params']['email_or_phone']);
            });
            $userData = $userData->first();
            
            if(@$userData){
                if(@$userData->status == 'U'){
                    if(is_numeric($reqdata['params']['email_or_phone'])){
                        Merchant::where(['id'=>$userData->id])->update(['is_phone_verified' => 'Y', 'login_via_otp' => NULL,'status'=>'AA']);
                    }else{
                        Merchant::where(['id'=>$userData->id])->update(['is_email_verified' => 'Y', 'login_via_otp' => NULL,'status'=>'AA']);
                    }
                    
                }else{
                    if(is_numeric($reqdata['params']['email_or_phone'])){
                        Merchant::where(['id'=>$userData->id])->update(['is_phone_verified' => 'Y', 'login_via_otp' => NULL]);
                    }else{
                        Merchant::where(['id'=>$userData->id])->update(['is_email_verified' => 'Y', 'login_via_otp' => NULL]);
                    }
                }
                $userData = Merchant::where(['id'=>$userData->id])->select('id','firebaseToken_id','driver_id','slug','fname','lname','phone','email','google_id','login_via_otp','tmp_email','email1','email2','email3','image','cover_pic','company_name','company_type','company_details','commission','shipping_cost','inside_shipping_cost','international_order','premium_merchant','address','location','lat','lng','address_note','building_number','avenue','city','city_id','state','zipcode','country','bank_name','account_name','account_number','ifsc_number','branch_name','payment_mode','total_earning','total_seller_commission','total_insurance_price','total_loading_inloading_price','total_admin_commission','total_product_price','invoice_in','invoice_out','total_commission','total_paid','total_due','email_vcode','is_email_verified','email_verified_at','status','hide_merchant','rate','total_no_review','total_review_rate','remember_token','show_order_processed','gross_earning','net_earning','commission_paid','applicable_city_delivery','applicable_city_delivery_external','minimum_commission','hide_customer_info','notify_merchant_on_delivery_internal','notify_merchant_on_delivery_external','notify_customer_by_email_internal','notify_customer_by_email_external','notify_customer_by_whatsapp_internal','notify_customer_by_whatsapp_external','created_at','updated_at','password')->first();
                if(@$userData->status == 'AA'){
                    $response['error'] = ERRORS['-33089'];
                    return response()->json($response);
                }
                $response['result']['userdata'] = $userData;
                $token = JWTAuth::fromUser($userData);
                $response['result']['token'] = $token;

                return response()->json($response);
            } else {
                $response['error'] = ERRORS['-33039'];
                return response()->json($response);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            $response['error'] = ERRORS['-33009'];
            $response['error']['message'] = $e->getMessage();
            return Response::json($response);
        }
    }
}
