<?php
namespace App\Http\Controllers\Api\Modules\Merchant\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\VerificationMail;
use App\Merchant;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Mail;
class ForgotPasswordController extends Controller {
    /**
    * @OA\Post(
    * path="/api/forgot-pass-merchant",
    * summary="Merchant Forgot Password",
    * description="Merchant Forgot by email",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Forgot Password"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant credentials",
    *    @OA\JsonContent(
    *       required={"email"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function forgotPassword(Request $request) {
        $response = ['jsonrpc' => '2.0'];
        // try {
            $reqData = $request->json()->all();
            $rules = ['email' => 'required|email'];
            $validator = Validator::make(@$reqData['params'], $rules);
            if ($validator->fails()) {
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $userdata = Merchant::where(['email' => $reqData['params']['email']])->whereIn('status', ['A','I','U'])->first();
            if ($userdata) {
                $otp = mt_rand(100000, 999999);
                Merchant::where('id', $userdata->id)->update(['email_vcode' => $otp]);
                $userdata = Merchant::where('id', $userdata->id)->first();
                $userdata['mailBody'] = 'forget_password';
                Mail::send(new VerificationMail($userdata));
                $response['result'] = __('success.-4075');
                $response['result']['vcode'] = $otp;
                $response['result']['req'] = $reqData;
            } else {
                $response['error'] = __('errors.-33018');
                $response['error']['message'] = "Error";
                $response['error']['meaning'] = "This email-id is not registered with us!";
            }
            return Response::json($response);
        // } catch(\Exception $e) {
        //     $response['error']['message'] = $e->getMessage();
        //     return Response::json($response);
        // }
    }
    /**
    * @OA\Post(
    * path="/api/update-pass-merchant",
    * summary="Merchant Update Password",
    * description="Merchant Update by otp,email,password",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Update Password"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant credentials",
    *    @OA\JsonContent(
    *       required={"otp","email","password"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="otp", type="integer", format="email", example="111111"),
    *           @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *           @OA\Property(property="password", type="string", format="password", example="password"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function passwordUpdate(Request $request) {
        $response = ['jsonrpc' => '2.0'];
        try {
            $reqData = $request->json()->all();
            $rules = [
                'otp' => "required|numeric",
                'email' => 'required|email',
                'password' => 'required'
            ];
            $validator = Validator::make(@$reqData['params'], $rules);
            if ($validator->fails()) {
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $user_details = Merchant::where(["email_vcode" => $reqData['params']['otp']])->where(["email" => $reqData['params']['email']])->whereIn('status', ['A','I','U'])->first();
            if (@$user_details) {
                $update_password['email_vcode'] = null;
                $update_password['password'] = \Hash::make($reqData['params']['password']);
                $update_user_password = Merchant::where('id', $user_details->id)->update($update_password);
                // $response['result'] = ERRORS['-33042'];
                $response['result'] = __('success.-4074');
            } else {
                // $response['error'] = ERRORS['-33029'];
                $response['error'] = __('errors.-33018');
            }
            return Response::json($response);
        }
        catch(\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return Response::json($response);
        }
    }
}
