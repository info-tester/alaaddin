<?php

namespace App\Http\Controllers\Api\Modules\Merchant\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Models\CartMaster;
use App\Models\CartDetail;
use App\Models\UserRegId;
use App\Mail\SendMailOtp;
use Mail;
#models
use App\Driver;
use App\User;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Hash;
use Auth;
use App\Merchant;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class LoginController extends Controller
{
    protected $cartMaster, $cartMaster1, $cartDetails, $cartData, $sessionId;

    public function __construct(
        Request $request,
        CartMasterRepository $cartMaster,
        CartMasterRepository $cartMaster1,
        CartDetailRepository $cartDetails
    )
    {
        $this->cartMaster = $cartMaster;
        $this->cartMaster1 = $cartMaster1;
        $this->cartDetails = $cartDetails;
    }
    /**
    * @OA\Post(
    * path="/api/merchant/login",
    * summary="Merchant Login",
    * description="Merchant Login by email,password",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Login"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant credentials",
    *    @OA\JsonContent(
    *       required={"email","password"},
    *           @OA\Property(property="params", type="object",
    
    *         	@OA\Property(property="email", type="string", example="1234567890"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function login(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
        	auth()->shouldUse('merchant');
            $reqData = $request->json()->all();
            if(is_numeric(@$reqData['params']['email'])){
                $validator = Validator::make(@$reqData['params'], [
                    'email'    => 'required|numeric|digits_between:10,12'
                    // 'password' => 'required'
                ],[
                    'email.digits_between' => 'The phone must be between 10 and 12 digits.',
                    'email.numeric' => 'The phone must be between 10 and 12 digits.',
                    'email.required' => 'The phone must be between 10 and 12 digits.'
                ]);
                $field = "phone";
            }
            else{
                $validator = Validator::make(@$reqData['params'], [
                    'email'    => 'required|email'
                    // 'password' => 'required'
                ],[
                    'email.required' => 'Please provide email-id',
                    'email.email' => 'Please provide valid email-id',
                    'email.regex' => 'Please provide valid email-id'
                ]);
                $field = "email";
            }
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            if($field == 'phone'){
                $credentials['phone'] = $reqData['params']['email'];
            }
            else{
                $credentials['email'] = $reqData['params']['email'];
            }
            // $credentials['password'] = $reqData['params']['password'];


            $userData = Merchant::whereIn('status', ['A','I','U','AA']);
            $userData = $userData->where(function($q) use ($reqData){
                $q->where('email',$reqData['params']['email'])->orWhere('phone',$reqData['params']['email']);
            });
            $userData = $userData->first();
            if(@$userData){
                if($userData->status == 'AA'){
                    $response['status'] = 'AA';
                    $response['error'] = ERRORS['-33089'];
                    return response()->json($response);
                }
                // if($userData->status == 'U'){
                //     $response['status'] = 'U';
                //     $response['error'] = ERRORS['-33077'];
                //     return response()->json($response);
                // }
                if($userData->status == 'I'){
                    $response['status'] = 'I';
                    $response['error'] = ERRORS['-33090'];
                    return response()->json($response);
                }
                $credentials['status'] = 'A';
                if(!$token = JWTAuth::fromUser($userData)){
                // if(!$token = JWTAuth::attempt($credentials)){
                    $response['error'] = ERRORS['-33009'];
                    return response()->json($response);
                }
                $response['result']['status'] = 'A';
                if(@$reqData['params']['email'] == "seller@gmail.com"){
                    $login_via_otp = 100000;
                }else{
                    

                
                    $login_via_otp = mt_rand(100000, 999999);
                }
                Merchant::where('id', $userData->id)->update(['login_via_otp'=>$login_via_otp]);
                $userData = Merchant::where('id', $userData->id)->first();
                // if(is_numeric($reqData['params']['email'])){
                    $phnno = $userData->phone;
                    $message = "Your login OTP is ".$login_via_otp." - ALLAAD";
                    $smsr = sendSms($phnno,$message);
                // }else{
                // try{
                //     $mailDetails = new \StdClass();
                //     $mailDetails->to    = @$userData->email;
                //     $mailDetails->fname = @$userData->fname;
                //     $mailDetails->id    = @$userData->id;
                //     $mailDetails->login_via_otp = @$userData->login_via_otp;
                //     $mailDetails->user = @$userData;
                //     Mail::send(new SendMailOtp($mailDetails));
                // }catch(\Exception $e){

                // }
                // }
                $response['result'] = __('success.-4074');
                $response['result']['meaning'] = "Otp has been sent to ".$userData->phone." Please check!";
                $response['result']['userdata'] = $this->getAuthUser($token);
                $response['result']['token'] = $token;
                $response['result']['login_via_otp'] = $login_via_otp;
                $response['result']['sendsms'] = @$smsr;
                
                return response()->json($response);
            }else{
                $response['error'] = ERRORS['-33009'];
                $response['error']['meaning'] = "This email does not exist!";
                return response()->json($response);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            $response['error'] = ERRORS['-33009'];
            $response['error']['message'] = $e->getMessage();
            return response()->json($response);
        }
    }
    private function getAuthUser($token)
    {
        $user = JWTAuth::setToken($token)->toUser();
        return  $user;
    }
}
