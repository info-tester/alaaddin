<?php

namespace App\Http\Controllers\Api\Modules\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Variant;
use App\Models\Brand;
use App\Merchant;
use App\Models\BannerContent;
use App\Models\ProductVariant;
use App\Models\ProductVariantDetail;
use App\Repositories\BrandRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\UserFavoriteRepository;
use App\Repositories\ProductOtherOptionRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\OrderDetailRepository;

use JWTFactory;
use JWTAuth;
use Validator;
use Session;
use Config;
class MerchantController extends Controller
{
    protected $brands, $product, $otherOptions, $orderDetails, $merchant, $userFavorite;

    public function __construct(BrandRepository $brands,
        ProductRepository $product,
        CategoryRepository $category,
        ProductOtherOptionRepository $otherOptions,
        UserFavoriteRepository $userFavorite,
        OrderDetailRepository $orderDetails,
        MerchantRepository $merchant,
        ProductCategoryRepository $product_cat
    )
    {
        $this->merchant         = $merchant;
        $this->brands           = $brands;
        $this->product          = $product;
        $this->category         = $category;
        $this->userFavorite     = $userFavorite;
        $this->otherOptions     = $otherOptions;
        $this->product_cat      = $product_cat;
        $this->orderDetails     = $orderDetails;
    }

    /**
    * Method: merchantProfile
    * Description: This method is used to show merchant details
    * Author: Surajit 
    */
     /**
    * @OA\Post(
    * path="/api/merchant-public-profile",
    * summary="Merchant Public Profile",
    * description="Merchant Public Profile",
    * operationId="authLogin",
    * tags={"Alaaddin: Public Profile"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant Public Profile",
    *    @OA\JsonContent(
    *       required={"seller_id"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="seller_id", type="string", format="seller_id", example="0"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function merchantProfile(Request $request) {
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $id = $reqData['params']['seller_id'];
        //$q->orWhere(['slug' => $id, 'id' => $id]);

        $merchant   = Merchant::with(['state_details','city_details'])->select('id', 'slug', 'fname', 'lname','image','cover_pic', 'company_name', 'city', 'country','state', 'rate', 'total_no_review', 'status', 'created_at', 'show_order_processed','company_type','company_details')
        ->where(function($q) use($id) {
            $q->where('id',$id);
            $q->orWhere('slug',$id);
        })
        ->where('status', 'A')
        ->with('Country','merchantCompanyDetailsByLanguage','merchantImage','city_details','state_details')
        ->first();

        if($merchant) {
            $product = $this->product->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price', 'from_date', 'to_date')->selectRaw('id, price, discount_price,product_price,round(((price - product_price) / price) * 100) as percentage,total_no_reviews,avg_review,unit_master_id')
        ->addSelect(\DB::raw('product_price AS discount_price'))
            ->where(['user_id' =>$merchant->id])
            ->with([
                'productUnitMasters', 
                'defaultImage:product_id,image', 
                'productByLanguage:language_id,product_id,title,description'
            ])
            ->with(['wishlist'])
            ->where(['status' => 'A','seller_status' => 'A'])
            ->orderBy('id', 'DESC')
            ->get();
            $merchant->products_count = $product->count();
            $totalProducts = $this->product->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price', 'from_date', 'to_date')
            ->where(['user_id' => $merchant->id])
            ->where([
                'status' => 'A',
                'seller_status' => 'A'
            ])
            ->count();

            $reviews = $this->orderDetails->select('id','seller_id','order_master_id','rate','comment','review_date')
            ->where('seller_id', $merchant->id)
            ->with(['orderMaster','orderMaster.getUserDetails','orderMaster.customerDetails'])
            ->WhereNotNull('rate')->WhereNotNull('comment')->Where('rate','>',0)->take(10)->get();

            if(count($product) > 0) {
                $product_id = $product->pluck('id');
                $product_cat = $this->product_cat->whereIn('product_id', $product_id)->groupBy('category_id')->with('Category')->where('level', 'P')->get();
                $orderProcessed = $this->orderDetails->whereIn('product_id', $product_id)->where('status', 'OD')->count();
            } else {
                $product_cat = '';
                $orderProcessed = '0';
            }

            $response['result']['merchant']['merchant_details'] = $merchant;
            $response['result']['merchant']['reviews'] = $reviews;

            $response['result']['tot_reviews_count'] = $this->orderDetails->select('id','seller_id','order_master_id','rate','comment','review_date')
            ->where('seller_id', $merchant->id)
            ->with(['orderMaster:id,user_id','orderMaster.getUserDetails:id,fname,lname,image','orderMaster.customerDetails'])
            ->Where('rate','>',0)->count();

            if($response['result']['tot_reviews_count'] > 10){
                $response['result']['has_more_reviews'] = 'Y';
            }else{
                $response['result']['has_more_reviews'] = 'N';
            }
            $response['result']['merchant']['merchant_products'] = $product;
            $response['result']['merchant']['total_products'] = count($product);
            $response['result']['merchant']['order_processed'] = $orderProcessed;
            $response['result']['merchant']['categories'] = $product_cat;

        } else {
            $response['error']['message'] = ERRORS['-33029'];
        }
        return response()->json($response);
    }
    /**
    * Method: merchantProfile
    * Description: This method is used to show merchant details
    * Author: Surajit 
    */
     /**
    * @OA\Post(
    * path="/api/see-all-specfic-merchant-related-products",
    * summary="See all specific merchant related products",
    * description="See all specific Merchant related products",
    * operationId="seeAllmerchantrelatedProducts",
    * tags={"Alaaddin: See all specific merchant related products"},
    * @OA\RequestBody(
    *    required=true,
    *    description="See all specific merchant related products",
    *    @OA\JsonContent(
    *       required={"seller_id","page"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="seller_id", type="string", format="seller_id", example="29"),
    *           @OA\Property(property="page", type="string", format="seller_id", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function seeAllmerchantrelatedProducts(Request $request) {
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $id = $reqData['params']['seller_id'];
        //$q->orWhere(['slug' => $id, 'id' => $id]);

        $merchant   = Merchant::with(['state_details','city_details'])->select('id', 'slug', 'fname', 'lname','image','cover_pic', 'company_name', 'city', 'country','state', 'rate', 'total_no_review', 'status', 'created_at', 'show_order_processed','company_type','company_details')
        ->where(function($q) use($id) {
            $q->where('id',$id);
            $q->orWhere('slug',$id);
        })
        ->where('status', 'A')
        ->with('Country','merchantCompanyDetailsByLanguage','merchantImage','city_details','state_details')
        ->first();

        if($merchant) {
            $product = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price', 'from_date', 'to_date','unit_master_id')->selectRaw('id, price, discount_price,product_price,round(((price - product_price) / price) * 100) as percentage,total_no_reviews,avg_review')
        ->addSelect(\DB::raw('product_price AS discount_price'))
            ->where(['user_id' =>$merchant->id])
            ->with([
                'defaultImage:product_id,image', 
                'productByLanguage:language_id,product_id,title,description'
            ])
            ->with(['wishlist'])
            ->where(['status' => 'A','seller_status' => 'A']);
            $page = @$reqData['params']['page']?$reqData['params']['page']:0; 
            $limit = 20;
            if($page == 1) {
                $start = 0;
            } else {
                $start = (($page -1) * $limit);
            }
            $merchant->products_count = $product->count();
            $response['result']['mp_total'] =$product->count();
            $response['result']['mp_count'] =ceil($product->count() /$limit);
            $response['result']['mp_per_page'] =$limit;
            $product = $product->orderBy('id','desc')->skip($start)->take($limit)->get();


            
            $totalProducts = $this->product->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price', 'from_date', 'to_date')
            ->where(['user_id' => $merchant->id])
            ->where([
                'status' => 'A',
                'seller_status' => 'A'
            ])
            ->count();

            $reviews = $this->orderDetails->select('id','seller_id','order_master_id','rate','comment','review_date')
            ->where('seller_id', $merchant->id)
            ->with('orderMaster:id,user_id','orderMaster.getUserDetails:id,fname,lname,image')
            ->WhereNotNull('rate')->WhereNotNull('comment')->get();

            if(count($product) > 0) {
                $product_id = $product->pluck('id');
                $product_cat = $this->product_cat->whereIn('product_id', $product_id)->groupBy('category_id')->with('Category')->where('level', 'P')->get();
                $orderProcessed = $this->orderDetails->whereIn('product_id', $product_id)->where('status', 'OD')->count();
            } else {
                $product_cat = '';
                $orderProcessed = '0';
            }

            $response['result']['merchant']['merchant_details'] = $merchant;
            $response['result']['merchant']['reviews'] = $reviews;
            $response['result']['merchant']['merchant_products'] = $product;
            $response['result']['merchant']['total_products'] = count($product);
            $response['result']['merchant']['order_processed'] = $orderProcessed;
            $response['result']['merchant']['categories'] = $product_cat;
        } else {
            $response['error']['message'] = ERRORS['-33029'];
        }
        return response()->json($response);
    }
}
