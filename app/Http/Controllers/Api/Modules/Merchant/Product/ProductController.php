<?php

namespace App\Http\Controllers\Api\Modules\Merchant\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Setting;
use Illuminate\Support\Facades\Validator;
use Image;
use App\Models\ProductImage;
use App\Models\Category;
use App\Models\ProductCategory;
use App\Models\ProductDetail;
use App\Models\UnitMaster;
use File;
class ProductController extends Controller
{
	public function __construct()
	{
		auth()->shouldUse('merchant');
	}
	/**
    * @OA\Post(
    * path="/api/get-unit-master-and-category",
    * summary="Merchant getUnitMasterAndCategory",
    * description="Merchant getUnitMasterAndCategory",
    * operationId="getUnitMasterAndCategory",
    * tags={"Alaaddin: Merchant getUnitMasterAndCategory"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    description="getUnitMasterAndCategory",
    *    @OA\JsonContent(
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="no_parameter", type="string", example=""),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
	public function getUnitMasterAndCategory(Request $request){
		$response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
		try {
			$response['result']['category'] = Category::with('categoryByLanguage')->where(['status'=>'A'])->get();
			$response['result']['UnitMaster'] = UnitMaster::where(['status'=>'A'])->select('id','unit_name','status')->get();
			return response()->json($response);
		}
		catch (\Exception $th) {
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
			// $response['error'] = $th->getMessage();
			return response()->json($response);
		}
	}
	/**
    * @OA\Post(
    * path="/api/merchant-product-list",
    * summary="Merchant Product List",
    * description="Merchant Product List",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Product List"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    description="Search Product",
    *    @OA\JsonContent(
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="keyword", type="string", format="keyword", example=""),
    *               @OA\Property(property="price", type="string", format="price", example="0,1000"),
    *               @OA\Property(property="status", type="string", format="status", example="A"),
    *               @OA\Property(property="page", type="string", format="status", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
	public function index(Request $request){
		$response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
		try {
			$user = auth()->user();
			if(@$user->status == 'I'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
			if(@$user->status == 'D'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
			$setting = Setting::select('loading_unloading_price')->first();
			$reqdata = $request->json()->all();

            $validator = Validator::make($reqdata['params'],[
                'page'    => 'required|numeric',
            ],
            [
                'page.required'=>'page required',
                'page.numeric'=>'page should be number'
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
			$p1 = $product = Product::where('status', '!=', 'D')
			->with([
				'productByLanguage',
				'defaultImage',
                'images',
				'productMarchant:id,fname,lname',
				'productCategory.Category',
				'productSubCategory.Category.priceVariant',
				'productParentCategory.Category',
				'productSubCategory.Category',
				'productUnitMasters:id,unit_name,status',
				'productVariants'
			])
			->where('user_id', @$user->id);
            if(@$reqData['params']['keyword']) {
                $keyword = $reqData['params']['keyword'];
                $product = $product->where(function($q1) use($keyword) {
                    $q1->where('product_code', 'LIKE', '%' . $keyword . '%')
                    ->orWhereHas('productByLanguage', function($q) use($keyword) {
                        $q->where('title', 'LIKE', '%'. $keyword .'%');
                    });
                });
            }
            if(@$reqData['params']['price']) {
                $price = explode(',', $reqData['params']['price']);
                $product = $product->whereBetween('price', $price);
            }
            if(@$reqData['params']['status']) {
                $product = $product->where('status', @$reqData['params']['status']);
            }
            $page = @$request['params']['page']?$request['params']['page']:1;
            $limit = 10;
            if($page == 1) {
                $start = 0;
            } else {
                $start = (($page -1) * $limit);
            }

            $response['result']['product_total'] =$product->count();
            $response['result']['product_page_count'] =ceil($product->count() /$limit);
            $response['result']['product_per_page'] =$limit;
            $product = $product->orderBy('id','desc')->skip($start)->take($limit)->get();
			$p1 = $p1->count();
			$response['result']['products'] = $product;
			$response['result']['loading_price'] = $setting->loading_unloading_price;
			$response['result']['show_lp'] = "Ton ";
			$minPrice = $product->min('price');
            $maxPrice = $product->max('price');
            if($minPrice) {
                $response['result']['min_price'] = $minPrice;
            } else {
                $response['result']['min_price'] = "0.00";
            }
            if($maxPrice) {
                $response['result']['max_price'] = $maxPrice;
            } else {
                $response['result']['max_price'] = "0.00";
            }
			return response()->json($response);
		}
		catch (\Exception $th) {
			// $response['error'] = $th->getMessage();
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
			return response()->json($response);
		}
	}
	/**
    * @OA\Post(
    * path="/api/merchant-store-product",
    * summary="Merchant store product",
    * description="Merchant store product",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant store product"},
    * security={{"bearer_token":{}}},
    *     @OA\RequestBody(
    *          @OA\MediaType(
    *              mediaType="multipart/form-data",
    *              @OA\Schema(
    *                  @OA\Property(
    *                      property="image[]",
    *                      type="file",
    *                      description="Product Images"
    *                  )
    *              )
    *          )
    *       ),
    *       @OA\Parameter(
    *           name="title",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="category",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="integer"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="unit_master_id",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="integer"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="price",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="integer"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="discount",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="integer"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="description",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
	public function merchantStoreProduct(Request $request){
		$response = ['jsonrpc'=>'2.0'];
		try {
			$user = auth()->user();

			// \Log::error($request->all());

			$validator = Validator::make($request->all(),[
				'category' => 'required|numeric',
				'title'    => 'required|string|max:255',
				'price'    => 'required',
				'unit_master_id'    => 'required|numeric',
				'discount' => 'nullable|numeric|max:99',
				'image.*'  => 'required'
				// 'image.*'  => 'required|mimes:jpeg,png,jpg'
			]);
			
			

			if($validator->fails()){
				$response['error'] =__('errors.-5001');
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
				return response()->json($response);
			}
			if(@$user->status == 'I'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
			if(@$user->status == 'D'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
			if(@$request->image) {		
				foreach($request->image as $key=>$file){
					
					// if($file->getClientOriginalExtension() != "jpeg" || $file->getClientOriginalExtension() != "png" || $file->getClientOriginalExtension() != 'jpg'){
					if(strtolower($file->getClientOriginalExtension()) == 'jpeg'){
						
					}
					else if(strtolower($file->getClientOriginalExtension()) == 'png'){
						
					}
					else if(strtolower($file->getClientOriginalExtension()) == 'jpg'){
						
					}
					else{
						$response['error'] =__('errors.-5001');
						$response['error']['message'] = 'Error';
		                $response['error']['meaning'] = $file->getClientOriginalExtension();
						return response()->json($response);	
					}
				}
			}
			if(!@$user->fname ){
				$response['error'] =__('errors.-5001');
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "Seller can't add products without given the essential details like company details  etc. fname : ".@$user->fname;
				return response()->json($response);
			}
			if(!@$user->lname ){
				$response['error'] =__('errors.-5001');
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "Seller can't add products without given the essential details like company details  etc. lname : ".@$user->lname;
				return response()->json($response);
			}
			if(!@$user->email ){
				$response['error'] =__('errors.-5001');
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "Seller can't add products without given the essential details like company details  etc. email : ".@$user->email;
				return response()->json($response);
			}
			if(!@$user->state ){
				$response['error'] =__('errors.-5001');
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "Seller can't add products without given the essential details like company details  etc. state : ".@$user->state;
				return response()->json($response);
			}
			if(!@$user->country ){
				$response['error'] =__('errors.-5001');
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "Seller can't add products without given the essential details like company details  etc. country : ".@$user->country;
				return response()->json($response);
			}
			if(!@$user->city ){
				$response['error'] =__('errors.-5001');
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "Seller can't add products without given the essential details like company details  etc. city : ".@$user->city;
				return response()->json($response);
			}
			if(!@$user->zipcode){
				$response['error'] =__('errors.-5001');
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "Seller can't add products without given the essential details like company details  etc. zipcode : ".@$user->zipcode;
				return response()->json($response);
			}
			if(!@$user->company_details){
				$response['error'] =__('errors.-5001');
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "Seller can't add products without given the essential details like company details  etc. company_details : ".@$user->company_details;
				return response()->json($response);
			}
			if(!@$user->company_type){
				$response['error'] =__('errors.-5001');
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "Seller can't add products without given the essential details like company details  etc. company_type : ".@$user->company_type;
				return response()->json($response);
			}
			if(!@$user->company_name){
				$response['error'] =__('errors.-5001');
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "Seller can't add products without given the essential details like company details  etc. company_name : ".@$user->company_name;
				return response()->json($response);
			}
			if(!@$user->fname || !@$user->lname || !@$user->email || !@$user->company_name || !@$user->company_type || !@$user->company_details || !@$user->country || !@$user->city || !@$user->state || !@$user->zipcode){
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "Seller can't add products without given the essential details like company details  etc. Company details : ".@$user->company_details;
				return response()->json($response);
			}
			$checkUnitMaster  = UnitMaster::where(['id'=>$request->unit_master_id,'status'=>'A'])->first();

			if(!@$checkUnitMaster){
				$response['error'] =__('errors.-5001');
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "You have provided wrong unit_master_id!";
				return response()->json($response);
			}
			$Category  = Category::where(['id'=>$request->category,'status'=>'A'])->first();

			if(!@$Category){
				$response['error'] =__('errors.-5001');
				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "You have provided wrong category id!";
				return response()->json($response);
			}
			if($validator) {
				$product = new Product;
				$product['user_id']      =   @$user->id;
				$product['status'] =   'W';
				$product['seller_status']=   'W';
				$product['price']  = @$request->price;
				$product['unit_master_id']  = $request->unit_master_id;

				

				$product['weight']  = $checkUnitMaster->weight_in_ton;

				if(!empty(@$request->discount)){
					$disTP = (@$request->price * @$request->discount) / 100;
					$product['discount_price'] = @$request->price - @$disTP;
					$product['product_price'] = $product['discount_price'];
				}else{
					$product['product_price'] = @$request->price;
				}
				
				$product->save();
				if(!empty($product)){
					$slug = str_slug(@$request->title);
					$check_slug = Product::where('slug', $slug)->first();
					if(@$check_slug) {
						$slug = $slug.'-'.$product->id;
					}

					$update['slug'] = $slug;
					$productId = sprintf('%06d', @$product->id);
					$update['product_code'] = "ALD".@$productId;

					Product::whereId(@$product->id)->update($update);
					$proId['product_id'] = $product->id;
					$this->addProductToCategory($request,$proId);
					$this->addProductToDetails($request,$proId);
					if($request->image) {
						$i=0;
						foreach($request->image as $key=>$file){
							$imgName   = $i.time().".".$file->getClientOriginalExtension();
						//for update image and check is defalut
							$pro_is_default = ProductImage::Where([
								'product_id' => $request->product_id,
								'is_default' => 'Y'
							])
							->first();
							if($pro_is_default){
								$is_default="N";
							}else{
								if($i==0){
									$is_default="Y";
								}
								else{
									$is_default="N";
								}
							}

							$file->move('storage/app/public/products', $imgName);

							$img = Image::make('storage/app/public/products/' . $imgName);

						# resize start
							if(!is_dir('storage/app/public/products/600')) {
								mkdir('storage/app/public/products/600');
							}
							if(!is_dir('storage/app/public/products/300')) {
								mkdir('storage/app/public/products/300');
							}
							if(!is_dir('storage/app/public/products/80')) {
								mkdir('storage/app/public/products/80');
							}
						# resize mid
							$img->resize(600, null, function($constraint) {
								$constraint->aspectRatio();
							})->save('storage/app/public/products/600/' . $imgName, 60);

						# resize small
							$img->resize(300, null, function($constraint) {
								$constraint->aspectRatio();
							})->save('storage/app/public/products/300/' . $imgName, 60);

						# resize thumb
							$img->resize(80, null, function($constraint) {
								$constraint->aspectRatio();
							})->save('storage/app/public/products/80/' . $imgName, 60);

							ProductImage::create([
								"product_id"      => @$product->id,
								"image"           => $imgName,
								"is_default"      => $is_default
							]);
							$i++;
							$imgName="";
						}
					}
				}
				$response['result'] = __('success_product.-4001');
				return response()->json($response);
			}
		}
		catch (\Exception $th) {
			// $response['error'] = $th->getMessage();
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
			return response()->json($response);
		}
	}
	private function addProductToCategory(Request $request, $proId){
		$product_cat = new ProductCategory;
		$product_cat['product_id']      =   @$proId['product_id'];
		$product_cat['category_id']     =   @$request->category;
		$product_cat['level']           =   'P';
		$product_cat->save();
	}
	private function addProductToDetails(Request $request, $proId){
		$product_detail = new ProductDetail;
		$product_detail['product_id']      =   $proId['product_id'];
		$product_detail['language_id']     =   1;
		$product_detail['title']           =   @$request->title;
		$product_detail['description']     =   @$request->description;
		$product_detail->save();
	}
	/**
    * @OA\Post(
    * path="/api/merchant-edit-product",
    * summary="Merchant Edit Product Details",
    * description="Merchant Edit Product Details",
    * operationId="merchantEditProduct",
    * tags={"Alaaddin: Merchant Edit Product Details"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant credentials",
    *    @OA\JsonContent(
    *       required={"product_id"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="product_id", type="integer", format="product_id", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
	public function merchantEditProduct(Request $request) {
		$response = ['jsonrpc'=>'2.0'];
		$reqData = $request->json()->all();
		$id = $reqData['params']['product_id'];
		$user = auth()->user();
		$product = Product::where('user_id',@$user->id)->select('id', 
			'slug', 
			'price', 
			'user_id', 
			\DB::raw('product_price AS discount_price'),
			'from_date',
			'total_review',
			'total_no_reviews',
			'product_price',
			'unit_master_id',
			'avg_review',
			'to_date',
			'stock',
			'product_code',
            'weight'
		)
		->with([
			'productParentCategory.Category',
			'images', 
			'productByLanguage',
			'productUnitMasters:id,unit_name,status',
			'productVariants.productVariantDetails'

		])
		->where(function($q) use($id) {
			$q->orWhere(['slug' => $id, 'id' => $id]);
		});
		$product = $product->first();

		$UnitMaster = UnitMaster::where('status','A')->select('id','unit_name','status')->get();

		if(@$product){
			$response['result']['product'] = $product;
			$response['result']['UnitMaster'] = $UnitMaster;
		}
		else
		{
			$response['error'] =__('errors.-5097');
		}
		return response()->json($response);
	}
    /**
    * @OA\Post(
    * path="/api/merchant-update-product",
    * summary="Merchant update product",
    * description="Merchant update product",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant update product"},
    * security={{"bearer_token":{}}},
    *     @OA\RequestBody(
    *          @OA\MediaType(
    *              mediaType="multipart/form-data",
    *              @OA\Schema(
    *                  @OA\Property(
    *                      property="image[]",
    *                      type="file",
    *                      description="Product Images"
    *                  )
    *              )
    *          )
    *       ),
    *       @OA\Parameter(
    *           name="product_id",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="integer"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="title",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="category",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="integer"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="unit_master_id",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="integer"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="price",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="integer"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="discount",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="integer"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="description",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function merchantUpdateProduct(Request $request){
    	$response = ['jsonrpc'=>'2.0'];
    	try {
    		$user = auth()->user();
    		if(@$user->status == 'I'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
			if(@$user->status == 'D'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
    		$validator = Validator::make($request->all(),[
    			'product_id' => 'required|numeric',
    			'category' => 'required|string|max:255',
    			'title'    => 'required|string|max:255',
    			'price'    => 'required',
    			'unit_master_id'    => 'required|numeric',
    			'discount' => 'nullable|numeric|max:99',
    		]);
    		if($validator->fails()){
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
    			return response()->json($response);
    		}
    		$checkUnitMaster  = UnitMaster::where(['id'=>$request->unit_master_id,'status'=>'A'])->first();

			if(!@$checkUnitMaster){

				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "You have provided wrong unit_master_id!";
				return response()->json($response);
			}
			$Category  = Category::where(['id'=>$request->category,'status'=>'A'])->first();

			if(!@$Category){

				$response['error']['message'] = 'Error';
                $response['error']['meaning'] = "You have provided wrong category id!";
				return response()->json($response);
			}
    		if($validator) {
    			$product['user_id']      =   @$user->id;
    			$product['price']  = @$request->price;
    			$product['unit_master_id']  = $request->unit_master_id;
    			if(!empty(@$request->discount)){
    				$disTP = (@$request->price * @$request->discount) / 100;
    				$product['discount_price'] = @$request->price - @$disTP;
    				$product['product_price'] = $product['discount_price'];
    			}
    			else{
    				$product['discount_price'] = 0.00;
    				$product['product_price'] = @$request->price;
    			}
    			
    			$slug = str_slug(@$request->title);
    			$product['slug'] = $slug.'-'.@$request->product_id;
    			Product::where('id',@$request->product_id)->update($product);
    			$product = Product::where('id',@$request->product_id)->first();
    			if(!empty($product)){
    				$proId['product_id'] = $product->id;
    				$this->updatedProductToCategory($request, $proId);
    				$this->updatedProductToDetails($request, $proId);
    				if($request->image) {
    					$i=0;
    					foreach($request->image as $key=>$file){
    						$imgName   = rand().".".$file->getClientOriginalExtension();
							//for update image and check is defalut
							// 
    						// $pro_is_default = ProductImage::Where([
    						// 	'product_id' => @$request->product_id
    						// 	// 'is_default' => 'Y'
    						// ])
    						// ->first();

    						// if($pro_is_default){
    						// 	$is_default="N";
    						// }else{
    							if($key==0){
    								$is_default="Y";
    							}
    							else{
    								$is_default="N";
    							}
    						// }

    						$file->move('storage/app/public/products', $imgName);

    						$img = Image::make('storage/app/public/products/' . $imgName);

						# resize start
    						if(!is_dir('storage/app/public/products/600')) {
    							mkdir('storage/app/public/products/600');
    						}
    						if(!is_dir('storage/app/public/products/300')) {
    							mkdir('storage/app/public/products/300');
    						}
    						if(!is_dir('storage/app/public/products/80')) {
    							mkdir('storage/app/public/products/80');
    						}
						# resize mid
    						$img->resize(600, null, function($constraint) {
    							$constraint->aspectRatio();
    						})->save('storage/app/public/products/600/' . $imgName, 60);

						# resize small
    						$img->resize(300, null, function($constraint) {
    							$constraint->aspectRatio();
    						})->save('storage/app/public/products/300/' . $imgName, 60);

						# resize thumb
    						$img->resize(80, null, function($constraint) {
    							$constraint->aspectRatio();
    						})->save('storage/app/public/products/80/' . $imgName, 60);

    						

    						if($key == 0){

    							// $pro_is_default = ProductImage::Where([
	    						// 	'product_id' => @$request->product_id,
	    						// 	'is_default' => 'Y'
	    						// ])
	    						// ->update(['is_default'=>'N']);

	    						$crProductImage = ProductImage::create([
	    							"product_id"      => @$product->id,
	    							"image"           => $imgName,
	    							"is_default"      => $is_default
	    						]);
	    						$pro_is_default = ProductImage::Where([
	    							'product_id' => @$request->product_id,
	    							'is_default' => 'Y'
	    						])->whereNotIn('id',[$crProductImage->id])
	    						->update(['is_default'=>'N']);

    						}else{
    							$crProductImage = ProductImage::create([
	    							"product_id"      => @$product->id,
	    							"image"           => $imgName,
	    							"is_default"      => $is_default
	    						]);
    						}

    						$i++;
    						$imgName="";
    					}
    				}

    			}
    			$response['result'] = __('success_product.-1000');
    			return response()->json($response);
    		}
    	}
    	catch (\Exception $th) {
    		$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
    		// $response['error'] = $th->getMessage();
    		\Log::error($th->getMessage());
    		return response()->json($response);
    	}
    }
    private function updatedProductToCategory(Request $request, $proId){
    	$update_product_cat['category_id']     =   $request->category;
    	ProductCategory::where([
    		'product_id' => $proId['product_id'],
    		'level'      => 'P'
    	])->update($update_product_cat);
    }
    private function updatedProductToDetails(Request $request, $proId){
    	$product_detail['product_id']      =   $proId['product_id'];
    	$product_detail['language_id']     =   1;
    	$product_detail['title']           =   @$request->title;
    	$product_detail['description']     =   @$request->description;
    	ProductDetail::where(['product_id'  => $proId['product_id']])->update($product_detail);
    }
	/**
    * @OA\Post(
    * path="/api/merchant-product-status/{id}",
    * summary="Merchant Product Status",
    * description="Merchant Product Status",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Product Status"},
    * security={{"bearer_token":{}}},
  	*      @OA\Parameter(
    *         name="id",
    *         in="path",
    *         description="product id",
    *         required=true,
    *      ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
  	public function statusProduct($id){
  		$user = auth()->user();
  		$product=Product::whereId($id)->where('user_id',@$user->id)->first();
  		if(!empty($product)){
  			if($product->status == "A") {
  				Product::where('id',$id)->update(
  					[
  						'status' => "I",
  						'seller_status' => "I"
  					]);
  			} elseif ($product->status == "I" || $product->status == "W") {
  				Product::where('id',$id)->update(
  					[
  						'status' => "A",
  						'seller_status' => "A"
  					]);
  			}
  			$response['result'] = __('success_product.-801');
  		}
  		else
  		{
  			$response['error'] =__('errors.-5097');
  		}
  		return response()->json($response);
  	}
  	/**
    * @OA\Post(
    * path="/api/merchant-product-delete/{id}",
    * summary="Merchant Product delete",
    * description="Merchant Product delete",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Product delete"},
    * security={{"bearer_token":{}}},
  	*      @OA\Parameter(
    *         name="id",
    *         in="path",
    *         description="product id",
    *         required=true,
    *      ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
  	public function deleteProduct($id){
		//delete form product or change status
  		$user = auth()->user();
  		$product=Product::whereId($id)->where('user_id',@$user->id)->first();
  		if(!empty(@$product)){
  			Product::whereId($id)->update(['status' => 'D']);
  			$response['result'] = __('success_product.-802');
  		}
  		else
  		{
  			$response['error'] =__('errors.-5097');
  		}
  		return response()->json($response);
  	}
  	/**
    * @OA\Post(
    * path="/api/merchant-set-default-image-of-product",
    * summary="merchant-set-default-image-of-product",
    * description="merchant-set-default-image-of-product",
    * operationId="merchantSetDefaultImageOfProduct",
    * tags={"Alaaddin: merchantSetDefaultImageOfProduct "},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description=" merchantSetDefaultImageOfProduct ",
    *    @OA\JsonContent(
    *       required={"id"},
    *           @OA\Property(property="params", type="object",
    *           @OA\Property(property="id", type="string", format="id", example="173"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function merchantSetDefaultImageOfProduct(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user();
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'], [
                'id' => 'required|numeric'
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5097');
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $id = $reqdata['params']['id'];
            $product = ProductImage::whereId($id)->first();
	  		if(!empty(@$product)){
	  			ProductImage::whereId($id)->update([
	  				'is_default' => 'Y'
	  			]);
	  			ProductImage::where('id', '!=', $id)
	  			->where('product_id', $product->product_id)
	  			->update([
	  				'is_default' => 'N'
	  			]);
	  			$response['result'] = __('success_product.-3002');
	  		}
	  		else{
	  			$response['error'] =__('errors.-5097');
	  		}
  			return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }  
    }
	/**
    * @OA\Post(
    * path="/api/merchant-product-set-default-image/{id}",
    * summary="Merchant Product Set Default Image",
    * description="Merchant Product Set Default Image",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Product Set Default Image"},
    * security={{"bearer_token":{}}},
  	*      @OA\Parameter(
    *         name="id",
    *         in="path",
    *         description="product image id",
    *         required=true,
    *      ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
  	public function setDefaultImg($id){
  		$product = ProductImage::whereId($id)->first();
  		if(!empty(@$product)){
  			ProductImage::whereId($id)->update([
  				'is_default' => 'Y'
  			]);
  			ProductImage::where('id', '!=', $id)
  			->where('product_id', $product->product_id)
  			->update([
  				'is_default' => 'N'
  			]);
  			$response['result'] = __('success_product.-3002');
  		}
  		else{
  			$response['error'] =__('errors.-5097');
  		}
  		return response()->json($response);
  	}
	/**
    * @OA\Post(
    * path="/api/merchant-product-image-delete/{id}",
    * summary="Merchant Product Delete Image",
    * description="Merchant Product Delete Image",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Product Delete Image"},
    * security={{"bearer_token":{}}},
  	*      @OA\Parameter(
    *         name="id",
    *         in="path",
    *         description="product image id",
    *         required=true,
    *      ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
  	public function removeImg($id){
  		$response = [
  			'jsonrpc'   => '2.0'
  		];

  		
  		$product = ProductImage::whereId($id)->first();

  		if(@$product){

	  		$product_image_count = ProductImage::where(['product_id'=>@$product->product_id])->count();
	  		
	  		if($product_image_count <= 1){
	  			$response['error'] =__('errors.-5097');
	  			$response['error']['meaning'] = "You have only 1 image of this product ! Product must have a image! First upload the other image then you can delete this image!";
	  			$response['error']['message'] = "Error";
	  			return response()->json($response);
	  		}
  		}

  		if(!empty(@$product)){
  			$productImage1 = 'storage/app/public/products/'.$product->image;
  			if (File::exists($productImage1)) {
  				unlink($productImage1);
  			}
  			$productImage2 = 'storage/app/public/products/80/'.$product->image;
  			if (File::exists($productImage2)) {
  				unlink($productImage2);
  			}
  			$productImage3 = 'storage/app/public/products/300/'.$product->image;
  			if (File::exists($productImage3)) {
  				unlink($productImage3);
  			}
  			$productImage4 = 'storage/app/public/products/600/'.$product->image;
  			if (File::exists($productImage4)) {
  				unlink($productImage4);
  			}
  			ProductImage::whereId($id)->delete();

  			$product_image_count = ProductImage::where(['product_id'=>@$product->product_id,'is_default'=>'Y'])->count();
  			if($product_image_count <= 0){
	  			$first_product_image = ProductImage::where(['product_id'=>@$product->product_id])->first();
  				if(@$first_product_image){
  					ProductImage::where(['product_id'=>@$product->product_id])->update(['is_default'=>'Y']);
  				}
	  		}

  			$response['result'] = __('success_product.-3003');
  		}
  		else{
  			$response['error'] =__('errors.-5097');
  		}
  		return response()->json($response);
  	}
  }
