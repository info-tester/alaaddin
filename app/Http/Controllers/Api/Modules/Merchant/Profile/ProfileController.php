<?php

namespace App\Http\Controllers\Api\Modules\Merchant\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\EmailVerifyMail;
use App\User;
use App\Merchant;
use App\Models\Country;
use App\Models\State;
use App\Models\CountryDetails;
use App\Models\UserAddressBook;
use App\Models\UserFavorite;
use App\Models\Product;
use App\Models\City;
use App\Models\CitiDetails;
use App\Models\UserReward;
use App\Models\OrderMaster;
use App\Models\Payment;
use App\Mail\VerificationMail;
use App\Repositories\CityRepository;
use App\Repositories\CitiDetailsRepository;
use App\Models\OrderSeller;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\DriverRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductVariantDetailRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CountryRepository;
use App\Repositories\UserRepository;
use Illuminate\Validation\Rule;
use Exception;
use JWTFactory;
use JWTAuth;
use Validator;
use Mail;
use Auth;
class ProfileController extends Controller
{
    protected $order_master,$order_details;
    public function __construct( OrderMasterRepository $order_master,
        OrderDetailRepository $order_details,
        DriverRepository $drivers,
        OrderSellerRepository $orderSeller,
        ProductVariantRepository $productVariant,
        ProductRepository $product,
        ProductVariantDetailRepository $productVariantDetail,
       MerchantRepository $merchants
    )
    {
        $this->order             =   $order_master;
        $this->order_details            =   $order_details;
        $this->merchants                = $merchants;
        $this->drivers                  =   $drivers;
        $this->orderSeller              = $orderSeller;
        $this->product_variant           = $productVariant;
        $this->productVariantDetail     = $productVariantDetail;
        $this->product                  = $product;
        auth()->shouldUse('merchant');
    }
    /**
    *   @OA\Post(
    *     path="/api/merchant-details",
    *      tags={"Alaaddin: Merchant Details"},  
    *      security={{"bearer_token":{}}},
    *       @OA\Response(
    *           response=200,
    *           description="Success",
    *            @OA\MediaType(
    *               mediaType="application/json",
    *           )
    *       ),
    * )
    */
    public function merchantDetails(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            if(@request()->header('authorization')) {
               $user = \JWTAuth::parseToken()->toUser();
            }
            $country = Country::with('countryDetailsBylanguage')->where('status','A')->get();
            $selerData = Merchant::whereId(@$user->id)->with(['city_details','state_details'])->first();
            $response['result']['merchant'] = $selerData;
            $response['result']['country'] = $country;
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/merchant-update",
    * summary="Merchant Update",
    * description="Register by merchant update",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant update"},
    * security={{"bearer_token":{}}},
    *     @OA\RequestBody(
    *          @OA\MediaType(
    *              mediaType="multipart/form-data",
    *              @OA\Schema(
    *                  @OA\Property(
    *                      property="image",
    *                      type="file",
    *                      description="Upload Image"
    *                  )
    *              )
    *          )
    *       ),
    *       @OA\Parameter(
    *           name="fname",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="lname",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="phone",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="integer"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="email",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="company_name",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="company_type",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="company_details",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="address",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="country",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="city",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="state",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="zipcode",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function updateMerchant(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            // $user = JWTAuth::toUser();
            $reqdata = $request->all();
            $user = auth()->user();
            if(@$user->status == 'I'){
                $response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
            }
            if(@$user->status == 'D'){
                $response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
            }
            $validator = Validator::make($reqdata,[
                'fname'         => 'required|string|max:255',
                'lname'         => 'required|string|max:255',
                'phone'         =>  [
                                        'required',
                                        'numeric',
                                        Rule::unique('merchants')->where(function($query) use ($user)  {
                                            $query->where('status','!=','D')->where('id','!=',@$user->id);
                                        })
                                    ],
                'email'         => [
                                    'required',
                                    'email',
                                    'max:255',
                                    Rule::unique('merchants')->where(function($query) use ($user)  {
                                        $query->where('status','!=','D')->where('id','!=',@$user->id);
                                    })
                                ],
                'company_type'  => 'required',
                'company_name'  => 'required',
                'company_details'  => 'required',
                // 'address'       => 'required',
                'city'          => 'required|numeric',
                'state'         => 'required|numeric',
                'zipcode'       => 'required|numeric',
                // 'avenue'        => 'nullable',
                // 'building_number'=> 'nullable',
                // 'address_note'  => 'nullable',
                // 'payment_method'  => 'nullable|in:A,O,C',
                'country'       => 'nullable',
                // 'password'      => 'nullable|min:6|confirmed',
            ]
            ,
            [
                'company_type.required'=>'Company type required',
                'company_name.required'=>'Company name required',
                'company_details.required'=>'Company details required',
                'city.required'=>'City required',
                'state.required'=>'State required',
                'zipcode.required'=>'Zipcode required',
                // 'avenue.required'=>'Avenue required',
                // 'building_number.required'=>'Building number required',
                // 'address_note.required'=>'Address required',
                // 'payment_method.required'=>'Payment method required',
                'country.required'=>'country required',
                // 'address.required'=>'Address required',
                'fname.required'=>'First name required',
                'fname.string'=>'First name should be string',
                'fname.max'=>'First name should be 255 charecters',
                'lname.required'=>'Last name required',
                'lname.max'=>'Last name maximum 255 charecters',
                'lname.string'=>'Last name should be string',
                'email.required'=>'Please provide valid email id',
                'email.email'=>'Please provide valid email-id',
                'email.regex'=>'Please provide valid email-id',
                'email.max'=>'Email-id should be 255 charecters',
                'phone.required'=>'Phone number is required',
                'phone.numeric'=>'Phone number should be number',
                'phone.digits_between'=>'Phone number should be 10 digits to maximum 12 digits'
            ]
        );
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $iscountryV = Country::where('id',@$reqdata['country'])->first();
            if(!@$iscountryV){
                $response['error'] =__('errors.-5001');
                $response['error']['meaning'] = "You have provided wrong country id: ".@$reqdata['country'];
                return response()->json($response);    
            }
            $isstatev = State::where('id',@$reqdata['state'])->first();
            if(!@$isstatev){
                $response['error'] =__('errors.-5001');
                $response['error']['meaning'] = "You have provided wrong state id: ".@$reqdata['state'];
                return response()->json($response);    
            }
            $iscity = City::where(['id'=>@$reqdata['city']])->first();
            if(!@$iscity){
                $response['error'] =__('errors.-5001');
                $response['error']['meaning'] = "You have provided wrong city id: ".@$reqdata['city']." or wrong combination of state city id!";
                return response()->json($response);    
            }
            $ins = [];
            $ins['fname']     =   @$reqdata['fname'];
            $ins['lname']     =   @$reqdata['lname'];
            $ins['phone']     =   @$reqdata['phone'];
            // alternative email id
            $ins['email1']     =   @$request->email;
            $ins['company_name'] =   @$reqdata['company_name'];
            $ins['company_type'] =   @$reqdata['company_type'];
            $ins['company_details'] =   @$reqdata['company_details'];
            $ins['address']   =   @$reqdata['address'];
            $ins['country']   =   @$reqdata['country'];
            $ins['state']     =   @$reqdata['state'];
            $ins['city']      =   @$reqdata['city'];
            $ins['zipcode']   =   @$reqdata['zipcode'];
            // $ins['avenue']   =   $reqdata['avenue'];
            // $ins['building_number']   =   $reqdata['building_number'];
            // $ins['address_note']   =   $reqdata['address_note'];
            // $ins['payment_method']   =   $reqdata['payment_method'];
            if($request->image) {
                @unlink(storage_path('app/public/customer/profile_pics/' . $user->image));
                $image     = $request->image; 
                $imgName   = time().".".$image->getClientOriginalExtension();
                $image->move('storage/app/public/customer/profile_pics', $imgName);
                $ins['image']     =   $imgName;
            }
            $fname = str_slug(@$reqdata['fname']);
            $lname = str_slug(@$reqdata['lname']);
            $slug = $fname.'-'.$lname;
            $ins['slug'] = $slug.'-'.$user->id;
            Merchant::where('id', $user->id)->update($ins);

            // if(!empty(@$request->email) && @$request->email != $user->email){
            //     $otp = mt_rand(100000,999999);
            //     $userUp = [];
            //     $userUp['tmp_email']       =   $request->email;
            //     $userUp['email_vcode']     =   $otp;
            //     Merchant::whereId(@$user->id)->update($userUp);
            //     $merchant = Merchant::whereId(@$user->id)->first();
            //     $merchant['id']       =   $user->email;
            //     $merchant['email']    =   $request->email;
            //     $merchant['mailBody'] = 'updated';
            //     Mail::send(new VerificationMail($merchant));
            //     $response['otp'] = $otp;
            //     $email_updated = 'Y';
            // }
            // else{
            //     $email_updated = 'N';
            // }
            
            // Change user password.
            if(@$request->new_password) {
                $resultPass = $this->changePassword($request);
            } else {
                // $resultPass = __('success_user.-700');
            }

            if(@$resultPass) {
                $response['error'] = $resultPass;                
            } else {
                $response['result'] = ERRORS['-33017'];
                if(@$email_updated) {
                    $response['result']['email_updated'] = $email_updated;
                }
            }
            $response['result']['email_updated'] = 'N';
            return response()->json($response);

        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    
    public function merchantChangePassword(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $reqdata = $request->all();
            $user = auth()->user();
            // return $request->all();
            $validator = Validator::make($reqdata,[
                'password'      => 'required',
                'new_password' => 'required|min:8|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation' => 'required|min:8'
            ]);
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            if(@$request->new_password) {
                $resultPass = $this->changePassword($request);
            } else {
                // $resultPass = __('success_user.-700');
            }
            if(@$resultPass) {
                $response['error'] = $resultPass;                
            } else {
                $response['result'] = ERRORS['-33091'];
            }
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    private function changePassword($request) {
        $user = auth()->user();
        // dd($request->all());
        if($request->new_password) {
            if($user->password == '') {
                $user_detail = $user;
                $user_detail->password=\Hash::make($request->password_confirmation);
                $user_detail->save();
                // return $passwordSucc = __('success_user.-700');
            } else {
                if(\Hash::check($request->password, $user->password)){
                    $user_detail = $user;
                    $user_detail->password=\Hash::make($request->password_confirmation);
                    $user_detail->save();
                    // return $passwordSucc = __('success_user.-700');
                } else {
                    return $passwordErr = __('success_user.-701');
                }
            }
        }
    }
    /**
    * @OA\Post(
    * path="/api/merchant-update-email-verify",
    * summary="Merchant Update Email Verify",
    * description="Verify by merchant email",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Update Email Verify"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant credentials",
    *    @OA\JsonContent(
    *       required={"email","vcode"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *           @OA\Property(property="vcode", type="integer", format="vcode", example="111111"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function merchantEmailVerify(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqdata = $request->json()->all();
            // print_r($reqdata); exit();
            $validator = Validator::make($reqdata['params'], [
                'email' => 'required|email',
                'vcode' => 'required|max:6'
            ]);
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $userData = auth()->user();
            if(@$userData->tmp_email == $reqdata['params']['email'] && @$userData->email_vcode == $reqdata['params']['vcode']){
                Merchant::whereId($userData->id)
                ->update(['email' => $reqdata['params']['email'], 'email_vcode' => NULL, 'tmp_email' => NULL]);

                $response['result'] =  __('success_user.-700');
                return response()->json($response);
            }else{
                $response['error'] = ERRORS['-33039'];
                return response()->json($response);
            }
        } catch (\Exception $e) {
            $response['error'] = ERRORS['-33039'];
            $response['error']['meaning'] = $e->getMessage();
            \Log::error($e->getMessage());
            return Response::json($response);
        }
    }
    /**
    * @OA\Get(
    * path="/api/merchant-dashboard",
    * summary="Merchant Dashboard",
    * description="Merchant Dashboard",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Dashboard"},
    * security={{"bearer_token":{}}},
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function merchantDashboard(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $user = auth()->user();
            // $user = JWTAuth::toUser();
            @$orderId = $this->orderSeller->where('seller_id',@$user->id)->pluck('order_master_id')->toArray(); 
            @$totalOrder = $this->order->whereIn('id',@$orderId)->whereNotIn('status',['D','I','F'])->count();
            @$activeProduct = $this->product->where('user_id',@$user->id)->where('status','A')->count();
            @$inactiveProduct = $this->product->where('user_id',@$user->id)->where('status','I')->count();
            $orders = $this->order->with([
                'orderSellerInfo'=> function($query) use($user) {
                    $query->where(function($q) use($user) {
                        $q->where('seller_id', $user->id);
                    });
                },
                'shipping_state_details',
                'countryDetails',
                'billing_state_details',
                'shipping_city_details',
                'billing_city_details',
                'getBillingCountry',
                'customerDetails.city_details',
                'customerDetails.state_details',
                'customerDetails.userCountryDetails.countryDetailsBylanguage',
                'customerDetails.userCityDetails',
                'orderMasterDetails'=> function($query) use($user) {
                    $query->where(function($q) use($user) {
                        $q->where('seller_id', $user->id);
                    });
                },
                'orderMasterDetails.getOrderDetailsUnitMaster',
                'orderMasterDetails.productDetails.productByLanguage',
                'orderMasterDetails.productDetails.productVariants',
                'orderMasterDetails.productVariantDetails',
                'orderMasterDetails.sellerDetails.merchantCityDetails',
                'orderMasterDetails.productDetails.defaultImage',
                'orderMasterDetails.productDetails.productMarchant',
                'orderMasterDetails.productDetails',
                'countryDetails.countryDetailsBylanguage',
                'getCityNameByLanguage',
            ])->whereNotIn('status', ['OC','D','I','F']);
            $orders = $orders->whereHas('orderMasterDetails', function($q) use ($request,$user){
                $q->where('seller_id',@$user->id);
            });
            $orders = $orders->whereHas('orderSellerInfo', function($q) use ($request,$user){
                $q->where('seller_id',@$user->id);
            });
            @$orders = $orders->orderBy('id','desc')->first();
            @$orders->order_detail = $this->order_details->where('order_master_id',@$orders->id)->where('seller_id',@$user->id)->with(['getOrderDetailsUnitMaster','productDetails.productByLanguage','productDetails.defaultImage','productDetails.productUnitMasters','productDetails.productMarchant'])->first();
            

            $merchant = Merchant::with(['city_details','state_details','Country:country_id,name,country_code'])->where(['id'=>$user->id])->first();
            $user = Merchant::with(['city_details','state_details','Country:country_id,name,country_code'])->where(['id'=>$user->id])->first();

            if(@$merchant->fname && @$merchant->lname && @$merchant->phone && @$merchant->company_name && @$merchant->email && @$merchant->country && @$merchant->state && @$merchant->city && @$merchant->zipcode){
                $response['result']['is_profile'] = "1";
            }else{
                $response['result']['is_profile'] = "0";  
            }
            $response['result']['merchant'] = $merchant; 
            $response['result']['user'] = $user; 
            $response['result']['total_order'] = @$totalOrder;
            $response['result']['active_product'] = @$activeProduct;
            $response['result']['inactive_product'] = @$inactiveProduct;
            $response['result']['gross_earnings'] = @$user->total_earning+@$user->total_admin_commission;
            $response['result']['total_seller_commission'] = @$user->total_admin_commission;
            $response['result']['net_earnings'] = @$user->total_earning;
            $response['result']['recent_order'] = @$orders;
            
            
            $p = OrderSeller::where(['order_master_id'=>@$orders->id,'seller_id'=>@$user->id])->first();
            if(@$p){
            $response['result']['recent_order']['order_total'] = $p->order_total;
            }
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = __('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] = __('errors.-5001');
            $response['error'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Get(
    * path="/api/merchant-edit-profile",
    * summary="Merchant Edit Profile",
    * description="Merchant Edit Profile",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Edit Profile"},
    * security={{"bearer_token":{}}},
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function editMerchantProfile(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $user = auth()->user();
            $country = Country::with('countryDetailsBylanguage')->where('status','A')->get();
            $selerData = Merchant::whereId(@$user->id)->with(['city_details','state_details'])->first();
            $response['result']['merchant'] = @$selerData;

            
            $response['result']['country'] = $country;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/merchant-update-bank",
    * summary="Merchant Update Bank Info",
    * description="Merchant Update Bank Info",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Update Bank Info"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant credentials",
    *    @OA\JsonContent(
    *       required={"bank_name","account_name","account_number","ifsc_number","branch_name"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="bank_name", type="string", format="bank_name", example=""),
    *           @OA\Property(property="account_name", type="string", format="account_name", example=""),
    *           @OA\Property(property="account_number", type="string", format="account_number", example=""),
    *           @OA\Property(property="ifsc_code", type="string", format="ifsc_code", example=""),
    *           @OA\Property(property="branch_name", type="string", format="branch_name", example=""),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function updateMerchantBank(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user();
            if(@$user->status == 'I'){
                $response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
            }
            if(@$user->status == 'D'){
                $response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
            }
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'], [
                'bank_name' => 'required',
                'account_name' => 'required',
                'account_number' => 'required',
                'ifsc_code' => 'required',
                'branch_name' => 'required',
            ]);
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            if(@$validator)
            {
                $bankInfo['bank_name']      =   @$reqdata['params']['bank_name'];
                $bankInfo['account_name']   =   @$reqdata['params']['account_name'];
                $bankInfo['account_number'] =   @$reqdata['params']['account_number'];
                $bankInfo['ifsc_number']    =   @$reqdata['params']['ifsc_code'];
                $bankInfo['branch_name']    =   @$reqdata['params']['branch_name'];
                Merchant::whereId(@$user->id)->update(@$bankInfo);
                $response['result'] = __('errors.-5095');
                return response()->json($response);
            }
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }  
    }
    /**
    * @OA\Post(
    * path="/api/merchant-reviews",
    * summary="Merchant All Reviews",
    * description="Merchant All Reviews",
    * operationId="MerchantReviews",
    * tags={"Alaaddin: Merchant All Reviews"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant All Reviews",
    *    @OA\JsonContent(
    *       required={"page"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="page", type="string", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function MerchantReviews(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $user = auth()->user();
            if(@$user->status == 'I'){
                $response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
            }
            if(@$user->status == 'D'){
                $response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
            }
            $selerData = Merchant::whereId(@$user->id)->select('rate','total_no_review')->first();

            $reqdata = $request->json()->all();

            $validator = Validator::make($reqdata['params'],[
                'page'    => 'required|numeric',
            ],
            [
                'page.required'=>'page required',
                'page.numeric'=>'page should be number'
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $reviews = $this->order_details->where('seller_id',@$user->id)->where('rate','!=',0)->select('order_master_id','rate','comment','customer_name','review_date')->with(['orderMaster:order_no,id,user_id','orderMaster.customerDetails:id,image']);

            $page = @$request['params']['page']?$request['params']['page']:1;
            
            $limit = 15;
            
            if($page == 1) {
                $start = 0;
            } else {
                $start = (($page -1) * $limit);
            }

            $response['result']['reviews_total'] =$reviews->count();
            $response['result']['reviews_page_count'] =ceil($reviews->count() /$limit);
            $response['result']['reviews_per_page'] =$limit;
            $reviews = $reviews->orderBy('id', 'desc')->skip($start)->take($limit)->get();
            $response['result']['seller'] = @$selerData;
            $response['result']['reviews'] = @$reviews;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    // public function MerchantReviewsOldWithoutPagination(){
    //     $response = ['jsonrpc'=>'2.0'];
    //     try {
    //         $user = auth()->user();
    //         $selerData = Merchant::whereId(@$user->id)->select('rate','total_no_review')->first();

            
            
    //         $reviews = $this->order_details->where('seller_id',@$user->id)->where('rate','!=',0)->select('order_master_id','rate','comment','customer_name','review_date')->with(['orderMaster:order_no,id,user_id','orderMaster.customerDetails:id,image'])->get();

    //         $response['result']['seller'] = @$selerData;
    //         $response['result']['reviews'] = @$reviews;
    //         return response()->json($response);
    //     }
    //     catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
    //         $response['error'] = $e->getMessage();
    //         return response()->json($response);
    //     }
    //     catch (\Exception $th) {
    //         // $response['error'] = $th->getMessage();
    //         $response['error'] =__('errors.-5001');
    //         $response['error']['meaning'] = $th->getMessage();
    //         return response()->json($response);
    //     }
    // }
}
