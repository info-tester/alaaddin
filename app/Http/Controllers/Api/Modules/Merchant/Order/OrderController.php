<?php

namespace App\Http\Controllers\api\modules\merchant\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\Models\OrderMaster;
use App\Models\OrderSeller;
use App\Models\OrderDetail;
use App\Models\Wallet;
use App\Models\WalletDetails;
use App\Models\Product;
use App\Models\Language;
use App\Models\Payment;
use App\Models\CancelRequest;
use App\Models\OrderVehicleInformation;
use App\Models\PaymentKeyTable;
use App\User;
use App\Merchant;
use Validator;
use Auth;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\ShippingCostRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\UserRepository;
use App\Repositories\ProductRepository;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Hash;

use Mail;
use App\Mail\SendMailCustomerdetails;
use App\Mail\SendMailToMerchant;
use App\Mail\VerifyNewEmail;
use App\Mail\SendOrderMail;
use App\Mail\SendMerchantOrderMail;


class OrderController extends Controller
{
	protected $order_master,$order_details,$shipping_cost,$customers;
	public function __construct(OrderMasterRepository $order_master,
		ShippingCostRepository $shipping_cost,
		OrderDetailRepository $order_details,
		ProductVariantRepository $product_variant,
		ProductRepository $product,
		UserRepository $customers,
		Request $request)
	{
		auth()->shouldUse('merchant');
		// $this->middleware('merchant.auth:merchant');
		$this->order_master            =   $order_master;
		$this->order_details           =   $order_details;
		$this->shipping_cost           =   $shipping_cost;
		$this->customers           	   =   $customers;
		$this->product_variant         =   $product_variant;
		$this->product         		   =   $product;

	}
	/**
    * @OA\Post(
    * path="/api/merchant-order-history",
    * summary="Merchant Order History",
    * description="Merchant Order History",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Order History"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Merchant Order History",
    *    @OA\JsonContent(
    *       required={"keyword","from_date","to_date","status"},
    *           @OA\Property(property="params", type="object",
    *         	@OA\Property(property="keyword", type="string", format="keyword", example=""),
    *         	@OA\Property(property="from_date", type="string", format="from_date", example="04/21/2022"),
    *         	@OA\Property(property="to_date", type="string", format="to_date", example="04/21/2022"),
    *         	@OA\Property(property="status", type="string", format="status", example=""),
    *         	@OA\Property(property="page", type="string", format="status", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
	public function index(Request $request){
		$response = ['jsonrpc'=>'2.0'];
		try 
		{
			$user = auth()->user();
			if(@$user->status == 'I'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
			if(@$user->status == 'D'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			} 
			$reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'],[
                'page'    => 'required|numeric'
            ],
            [
                'page.required'=>'Page required',
                'page.number'=>'Page required'
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
			// $customers 	= $this->order_master->with([
			// 	'orderSellerInfo',
			// 	'customerDetails',
			// 	'orderMasterDetails',
			// 	'countryDetails.countryDetailsBylanguage',
			// 	'getCityNameByLanguage'
			// ]);
			// $customers = $customers->whereHas('orderMasterDetails', function($q) use ($request,$user){
			// 	$q->where(['seller_id'=>@$user->id]);
			// });
			// $customers = $customers->get();
			$orders = $this->order_master->with([
				'getCountry',
				'getBillingCountry',
				'shipping_city_details',
				'billing_city_details',
				'shipping_state_details',
				'billing_state_details',
				'orderSellerInfo.orderSeller',
				'customerDetails.userCountryDetails',
				'customerDetails.state_details',
				'customerDetails.city_details',
				'customerDetails',
				'orderMasterDetails.showCancelRequest',
				'orderMasterDetails.getOrderDetailsUnitMaster',
				'orderMasterDetails.productDetails.productMarchant',
				'orderMasterDetails.productDetails.defaultImage',
				'orderMasterDetails.productDetails.productByLanguage',
				'countryDetails.countryDetailsBylanguage',
				'getCityNameByLanguage',
			])->where('status','!=','I');
			$orders = $orders->whereNotIn('status', ['','D','I','F']);
			$orders = $orders->whereHas('orderMasterDetails', function($q) use ($request,$user){
				$q->where('seller_id',@$user->id);
			});
			$orders = $orders->whereHas('orderSellerInfo', function($q) use ($request,$user){
				$q->where('seller_id',@$user->id);
			});
			if(!empty(@$request['params']['keyword'])){
				$orders = $orders->where(function($query) use($request){
					$query->Where('shipping_fname','like','%'.@$request['params']['keyword'].'%')
					->orWhere('shipping_lname','like','%'.@$request['params']['keyword'].'%')
					->orWhere('billing_fname','like','%'.@$request['params']['keyword'].'%')
					->orWhere('billing_lname','like','%'.@$request['params']['keyword'].'%')
					->orWhere('billing_phone','like','%'.@$request['params']['keyword'].'%')
					->orWhere('shipping_phone','like','%'.@$request['params']['keyword'].'%')
					->orWhere('fname','like','%'.@$request['params']['keyword'].'%')
					->orWhere('lname','like','%'.@$request['params']['keyword'].'%')
					->orWhere('order_no','like','%'.@$request['params']['keyword'].'%')
					->orWhere('shipping_phone','like','%'.@$request['params']['keyword'].'%')
					->orWhere(\DB::raw("CONCAT(`billing_fname`, ' ', `billing_lname`)"), 'LIKE', "%".@$request['params']['keyword']."%")
					->orWhere(\DB::raw("CONCAT(`fname`, ' ', `lname`)"), 'LIKE', "%".@$request['params']['keyword']."%")
					->orWhere(\DB::raw("CONCAT(`shipping_fname`, ' ', `shipping_lname`)"), 'LIKE', "%".@$request['params']['keyword']."%");
				});
			}
			if(!empty(@$request['params']['from_date']) && !empty(@$request['params']['to_date']))
			{
				$from_date = date('Y-m-d', strtotime(@$request['params']['from_date']));
				$to_date = date('Y-m-d', strtotime(@$request['params']['to_date']));
				$orders = $orders->whereBetween('created_at',[@$from_date." 00:00:00", @$to_date." 23:59:59"]);
			}
			else if(!empty(@$request['params']['from_date']))
			{
				$from_date = date('Y-m-d', strtotime(@$request['params']['from_date']));
				
				$orders = $orders->where('created_at','>=',@$from_date." 00:00:00");
			}
			else if(!empty(@$request['params']['to_date']))
			{
				$to_date = date('Y-m-d', strtotime(@$request['params']['to_date']));
				$orders = $orders->where('created_at','<=',@$to_date." 23:59:59");
			}
			if(!empty(@$request['params']['status']))
			{
				$orders = $orders->whereHas('orderSellerInfo', function($q) use ($request,$user){
					$q->where('status',@$request['params']['status']);
				});
				// $orders = $orders->where('status',@$request['params']['status']);
			}
			if(@$request['params']['page']){
				$validator = Validator::make($request['params'],[
	                'page'    => 'required|numeric'
	            ],
	            [
	                'page.required'=>'Page required',
	                'page.number'=>'Page required'
	            ]);
	            if($validator->fails()){
	                $response['error'] =__('errors.-5001');
	                $response['error']['message'] = 'Error';
	                $response['error']['meaning'] = $validator->errors()->first();
	                return response()->json($response);
	            }
			}
			$page = @$request['params']['page']?$request['params']['page']:1;
            $limit = 10;
            if($page == 1) {
                $start = 0;
            } else {
                $start = (($page -1) * $limit);
            }

            $response['result']['orders_total'] =$orders->count();
            $response['result']['orders_page_count'] =ceil($orders->count() /$limit);
            $response['result']['orders_per_page'] =$limit;
			$orders = $orders->orderBy('id','desc')->skip($start)->take($limit)->get();
			foreach ($orders as $key => $value) {
				$value->order_detail = $this->order_details->where('order_master_id',@$value->id)->where('seller_id',@$user->id)->with(['getOrderDetailsUnitMaster','productDetails.productByLanguage','productDetails.defaultImage','productDetails.productMarchant','showCancelRequest'])->first();
				// $value->order_details = $this->order_details->where('order_master_id',@$value->id)->where('seller_id',@$user->id)->with(['productDetails.productByLanguage','productDetails.defaultImage'])->get();
			}
			// $response['result']['customers'] = @$customers;
			$response['result']['orders'] = @$orders;
			return response()->json($response);
		}
		catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
			// $response['error'] = $e->getMessage();
			$response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
			return response()->json($response);
		}
		catch (\Exception $th) {
			// $response['error'] = $th->getMessage();
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
			return response()->json($response);
		}
	}
	/**
    * @OA\Get(
    * path="/api/merchant-order-details-{id}",
    * summary="Merchant Order Details",
    * description="Merchant Order Details",
    * operationId="authLogin",
    * tags={"Alaaddin: Merchant Order Details"},
    * security={{"bearer_token":{}}},
    *      @OA\Parameter(
    *         name="id",
    *         in="path",
    *         description="order id",
    *         required=true,
    *      ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
	public function viewOrderDetails($id){
		try 
		{
			$user = auth()->user(); 
			if(@$user->status == 'I'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
			if(@$user->status == 'D'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
			$order = $this->order_master->with([
				'getCountry',
				'getBillingCountry',
				'shipping_city_details',
				'billing_city_details',
				'shipping_state_details',
				'billing_state_details',
				'customerDetails.userCountryDetails',
				'customerDetails.state_details',
				'customerDetails.city_details',
				'customerDetails.userCountryDetails.countryDetailsBylanguage',
				'orderMasterDetails.getOrderDetailsUnitMaster',
				'countryDetails.countryDetailsBylanguage',
				'orderMasterDetails.showCancelRequest',
				'orderMasterDetails.showOrderVehicleInformation',
				'orderMasterDetails.getOrderDetailsUnitMaster',
				'orderMasterDetails.productDetails.productByLanguage',
				'orderMasterDetails.productDetails.productVariants',
				'orderMasterDetails.productVariantDetails',
				'orderMasterDetails.sellerDetails.merchantCityDetails',
				'countryDetails.countryDetailsBylanguage',
				'shippingAddress',
				'billingAddress',
				'showOrderVehicleInformation',
				'getBillingCountry',
				'orderSellerInfo.orderSeller',
				'productVariantDetails',
				'orderMasterDetails.productDetails.defaultImage',
				'orderMasterDetails.productDetails.productMarchant',
				'orderMasterDetails.productDetails',
				'shipping_city_details',
				'billing_city_details',
				'shipping_state_details',
				'billing_state_details'
			])
			->where(['id'=>$id]);
			$order = $order->whereHas('orderMasterDetails', function($q) use ($user) {
				$q->where(['seller_id'=>@$user->id]);
			});
			$order = $order->first();
			if(!@$order){
				$response['error'] =__('errors.-5001');
            	$response['error']['meaning'] = " This order is not assciated with you! ".$id;
            	return response()->json($response);
			}

			if(!empty($order)){
				// $order->order_detail = $this->order_details->where('order_master_id',@$order->id)->where('seller_id',@$user->id)->with(['productDetails.productByLanguage','productDetails.defaultImage'])->first();
				$order->order_details = $this->order_details->where('order_master_id',@$order->id)->where('seller_id',@$user->id)->with(['showCancelRequest','productDetails.productByLanguage','productDetails.defaultImage','productDetails.productUnitMasters','productDetails.productMarchant'])->get();
			}
			$no_of_item = $this->order_details->where(['order_master_id'=>@$id,'seller_id'=>@$user->id])->count();
			$ord_sub_total = $this->order_details->where(['order_master_id'=>@$id,'seller_id'=>@$user->id])->sum('sub_total');
			$ord_total = $this->order_details->where(['order_master_id'=>@$id,'seller_id'=>@$user->id])->sum('total');
			$ord_sel = OrderSeller::where(['order_master_id'=>@$order->id,'seller_id'=>@$user->id])->first();
			$response['result']['order'] = @$order; 
			if(@$order){
				if(@$ord_sel){
					$response['result']['order']['admin_commission'] = $ord_sel->admin_commission;
				}
			}
			$response['result']['total_no_item'] = @$no_of_item;
			$response['result']['ord_sub_total'] = @$ord_sub_total;
			$response['result']['ord_total'] = @$ord_total;
			$response['result']['ord_sel'] = @$ord_sel;
			if(@$order->billing_city_details->name){
				$response['result']['user_tab_cust_cur_city_name'] = @$order->billing_city_details->name;
			}else{
				$response['result']['user_tab_cust_cur_city_name'] = " ";	
			}
			if(@$order->getBillingCountry->name){
				$response['result']['user_tab_cust_cur_country_name'] = @$order->getBillingCountry->name;
			}else{
				$response['result']['user_tab_cust_cur_country_name'] = " ";
			}
			if(@$order->billing_state_details->name){
				$response['result']['user_tab_cust_cur_state_name'] = @$order->billing_state_details->name;
			}else{
				$response['result']['user_tab_cust_cur_state_name'] = " ";
			}
			return response()->json($response);
		}
		catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
			// $response['error'] = $e->getMessage();
			$response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
			return response()->json($response);
		}
		catch (\Exception $th) {
			// $response['error'] = $th->getMessage();
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
			return response()->json($response);
		}
	}
	/**
    * @OA\Post(
    * path="/api/merchant-order-status-change",
    * summary="merchant-order-status-change",
    * operationId="orderStatusChange",
    * tags={"Alaaddin:orderStatusChange   "},
    *      security={{"bearer_token":{}}},
    *       @OA\Parameter(
    *           name="order_details_id",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="order_master_id",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="product_id",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="status",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="cancel_note",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="vehicle_no",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="shipping_date",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
     *       @OA\Parameter(
    *           name="driver_name",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
	 *       @OA\Parameter(
    *           name="driver_mobile_number",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="integer"
    *           )
    *       ),
     *       @OA\Parameter(
    *           name="received_date",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
 	*       @OA\Parameter(
    *           name="description",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),

    *     @OA\RequestBody(
    *          @OA\MediaType(
    *              mediaType="multipart/form-data",
    *              @OA\Schema(
    *                  @OA\Property(
    *                      property="evidence_file",
    *                      type="file",
    *                      description="evidence_file"
    *                  )
    *              )
    *          )
    *      ),
            * @OA\Response(
    *           response=200,
    *           description="Success",
    *            @OA\MediaType(
    *               mediaType="application/json",
    *           )
    *       ),
    *      * @OA\Response(
    *    response=422,
    *    description="Wrong ",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, Please try again")
    *        )
    *     )
    * ) 
    * )
    */
	
	public function orderStatusChange(Request $request){
		try 
		{
			$response = ['jsonrpc'=>'2.0'];
			$reqData = $request->all();
			$reqdata = $reqData;
            $validator = Validator::make($reqdata,[
                'order_details_id'    => 'required|numeric',
                'order_master_id'    => 'required|numeric',
                'product_id'    => 'required|numeric',
                'status'    => 'required|in:OA,RP,OD,OC,OP',
            ],
            [
                'status.required'=>'status required',
                'status.in'=>'Please provide status either OA or RP or OP or OD or OC',
                'order_master_id.numeric'=>'order_master_id required and shoule be a number',
                'product_id.numeric'=>'product_id required and shoule be a number',
                'order_details_id.numeric'=>'order_details_id required and shoule be a number',
                'order_master_id.required'=>'order_master_id required',
                'product_id.required'=>'product_id required',
                'order_details_id.required'=>'order_details_id required'
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
			$id = $reqData['order_details_id'];
			$order_master_id = $reqData['order_master_id'];
			$product_id = $reqData['product_id'];
			$status = $reqData['status'];
			$user = auth()->user(); 
			if(@$user->status == 'I'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}
			if(@$user->status == 'D'){
				$response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
			}

			

			
			$order = OrderDetail::where(['id'=>$id,'seller_id'=>@$user->id,'order_master_id'=>$order_master_id,'product_id'=>$product_id])->whereNotIn('status',['OD','OC'])->first();
			
			if(!@$order){
				$response['error'] = __('errors.-5041');
				$response['error']['meaning'] = " This is either not associated with you or already delivered or cancelled  ";
				return response()->json($response);
			}

			$orderDtl = OrderDetail::where(['id'=>$id,'seller_id'=>@$user->id,'order_master_id'=>$order_master_id,'product_id'=>$product_id])->whereNotIn('status',['OD','OC'])->first();
			if(!@$orderDtl){
				$response['error'] = __('errors.-5041');
				return response()->json($response);
			}
			$ords = OrderSeller::where(['seller_id' => $order->seller_id, 'order_master_id' => $order->order_master_id])->first();
			if(!@$ords){
				$response['error'] = __('errors.-5041');
				return response()->json($response);
			}
			if(@$ords->status == 'CM'){
				$response['error'] = __('errors.-5041');
				$response['error']['meaning'] = "this is already completed!";
				return response()->json($response);
			}
			if(@$orderDtl->status == $status){
				$response['error'] = __('errors.-5041');
				$statusNm = $status;	
				if($status == 'OA'){
					$statusNm = "Accepted";	
				}
				if($status == 'OP'){
					$statusNm = "Picked up";	
				}
				if($status == 'RP'){
					$statusNm = "Ready for pick up";	
				}
				if($status == 'OD'){
					$statusNm = "delivered";	
				}
				if($status == 'OC'){
					$statusNm = "cancelled";	
				}
				if($status == 'N'){
					$statusNm = "New";	
				}
				$response['error']['meaning'] = "You cannot changed to previous status.Previously you have already changed the status to ".$statusNm;
				return response()->json($response);
			}
			if(@$orderDtl->status == 'OP' && $status == 'OA'){
				$response['error'] = __('errors.-5041');
				$statusNm = $status;	
				if($status == 'OA'){
					$statusNm = "Accepted";
				}
				if($status == 'OP'){
					$statusNm = "Picked up";	
				}
				if($status == 'RP'){
					$statusNm = "Ready for pick up";
				}
				if($status == 'OD'){
					$statusNm = "delivered";	
				}
				if($status == 'OC'){
					$statusNm = "cancelled";	
				}
				if($status == 'N'){
					$statusNm = "New";
				}
				$response['error']['meaning'] = "You cannot changed to previous status.Previously you have already changed the status to ".$statusNm;
				return response()->json($response);
			}
			if(@$orderDtl->status == 'OP' && $status == 'RP'){
				$response['error'] = __('errors.-5041');
				$statusNm = $status;	
				if($status == 'OA'){
					$statusNm = "Accepted";
				}
				if($status == 'OP'){
					$statusNm = "Picked up";	
				}
				if($status == 'RP'){
					$statusNm = "Ready for pick up";
				}
				if($status == 'OD'){
					$statusNm = "delivered";	
				}
				if($status == 'OC'){
					$statusNm = "cancelled";	
				}
				if($status == 'N'){
					$statusNm = "New";
				}
				$response['error']['meaning'] = "You cannot changed to previous status.Previously you have already changed the status to ".$statusNm;
				return response()->json($response);
			}
			if(@$orderDtl->status == 'OP' && $status == 'RP'){
				$response['error'] = __('errors.-5041');
				$statusNm = $status;	
				if($status == 'OA'){
					$statusNm = "Accepted";
				}
				if($status == 'OP'){
					$statusNm = "Picked up";	
				}
				if($status == 'RP'){
					$statusNm = "Ready for pick up";	
				}
				if($status == 'OD'){
					$statusNm = "delivered";	
				}
				if($status == 'OC'){
					$statusNm = "cancelled";	
				}
				if($status == 'N'){
					$statusNm = "New";
				}
				$response['error']['meaning'] = "Previously you have already changed the status to ".$statusNm;
				return response()->json($response);
			}
			if(@$orderDtl->status == 'OP' && $status == 'RP'){
				$response['error'] = __('errors.-5041');
				$statusNm = $status;	
				if($status == 'OA'){
					$statusNm = "Accepted";
				}
				if($status == 'OP'){
					$statusNm = "Picked up";	
				}
				if($status == 'RP'){
					$statusNm = "Ready for pick up";	
				}
				if($status == 'OD'){
					$statusNm = "delivered";	
				}
				if($status == 'OC'){
					$statusNm = "cancelled";	
				}
				if($status == 'N'){
					$statusNm = "New";
				}
				$response['error']['meaning'] = "Previously you have already changed the status to ".$statusNm;
				return response()->json($response);
			}
			if(@$order) {
				if($reqData['status'] == 'RP'){
					$chkOrderVehicleInformation = OrderVehicleInformation::where(['order_master_id'=>$order->order_master_id,'order_details_id'=>$reqData['order_details_id'],'product_id'=>$reqData['product_id']])->first();
					if(!@$chkOrderVehicleInformation){
						$validator = Validator::make($reqdata,[
			                'order_details_id'    => 'required|numeric',
			                'order_master_id'    => 'required|numeric',
			                'product_id'    => 'required|numeric',
			                'vehicle_no'    => 'required',
			                'shipping_date'    => 'required',
			                'driver_mobile_number'    => 'required|numeric',
			                'status'    => 'required|in:OA,RP,OD,OC,OP',
			            ],
			            [
			                'vehicle_no.required'=>'vehicle_no required',
			                'shipping_date.required'=>'shipping_date required',
			                'driver_mobile_number.required'=>'driver_mobile_number required',
			                'driver_mobile_number.numeric'=>'driver_mobile_number should be number',
			                'status.required'=>'status required',
			                'status.in'=>'Please provide status either OA or RP or OP or OD or OC',
			                'order_master_id.numeric'=>'order_master_id required and shoule be a number',
			                'product_id.numeric'=>'product_id required and shoule be a number',
			                'order_details_id.numeric'=>'order_details_id required and shoule be a number',
			                'order_master_id.required'=>'order_master_id required',
			                'product_id.required'=>'product_id required',
			                'order_details_id.required'=>'order_details_id required'
			            ]);
			            if($validator->fails()){
			                $response['error'] =__('errors.-5001');
			                $response['error']['message'] = 'Error';
			                $response['error']['meaning'] = $validator->errors()->first();
			                return response()->json($response);
			            }
						$orOrderVehicleInformation['order_master_id']= $order->order_master_id;
						$orOrderVehicleInformation['order_details_id']= $reqData['order_details_id'];
						$orOrderVehicleInformation['product_id']= $reqData['product_id'];
						$orOrderVehicleInformation['vehicle_no']= $reqData['vehicle_no'];
						$orOrderVehicleInformation['driver_name']= @$reqData['driver_name'];
						$orOrderVehicleInformation['driver_mobile_number']= $reqData['driver_mobile_number'];
						$orOrderVehicleInformation['shipping_date']= $reqData['shipping_date'];
						$OrderVehicleInformation = OrderVehicleInformation::create($orOrderVehicleInformation);
					}
				}
				if(@$reqData['status'] == 'OC'){
					$validator = Validator::make($reqdata,[
		                'order_details_id'    => 'required|numeric',
		                'order_master_id'    => 'required|numeric',
		                'product_id'    => 'required|numeric',
		                'cancel_note'    => 'required',
		                'status'    => 'required|in:OA,RP,OD,OC,OP',
		            ],
		            [
		                'cancel_note.required'=>'cancel_note required',
		                'status.required'=>'status required',
		                'status.in'=>'Please provide status either OA or RP or OP or OD or OC',
		                'order_master_id.numeric'=>'order_master_id required and shoule be a number',
		                'product_id.numeric'=>'product_id required and shoule be a number',
		                'order_details_id.numeric'=>'order_details_id required and shoule be a number',
		                'order_master_id.required'=>'order_master_id required',
		                'product_id.required'=>'product_id required',
		                'order_details_id.required'=>'order_details_id required'
		            ]);
		            if($validator->fails()){
		                $response['error'] =__('errors.-5001');
		                $response['error']['message'] = 'Error';
		                $response['error']['meaning'] = $validator->errors()->first();
		                return response()->json($response);
		            }
				}
				if($reqData['status'] == 'OD'){
					$chkOrderVehicleInformation = OrderVehicleInformation::where(['order_master_id'=>$order->order_master_id,'order_details_id'=>$reqData['order_details_id'],'product_id'=>$reqData['product_id']])->first();
					if(@$chkOrderVehicleInformation){
						$validator = Validator::make($reqdata,[
			                'received_date'    => 'required',
			            ],
			            [
			                'received_date.required'=>'received_date required',
			            ]);
			            if($validator->fails()){
			                $response['error'] =__('errors.-5001');
			                $response['error']['message'] = 'Error';
			                $response['error']['meaning'] = $validator->errors()->first();
			                return response()->json($response);
			            }
						
						$orOrderVehicleInformation['received_date']= $reqData['received_date'];
						$orOrderVehicleInformation['description']= @$reqData['description'];
						if(@$request->evidence_file){
			                $evidence_file = $request->evidence_file;
			                $filename = rand() . '-' . rand(1000, 9999) . '.' . $evidence_file->getClientOriginalExtension();
			                $evidence_file->move('storage/app/public/evidence_file/', $filename);
			                $orOrderVehicleInformation['evidence_file']=$filename;
			            }
						$OrderVehicleInformation = OrderVehicleInformation::where(['order_master_id'=>$order->order_master_id,'order_details_id'=>$reqData['order_details_id'],'product_id'=>$reqData['product_id']])->update($orOrderVehicleInformation);
					}
				}
				
				
				if($status == 'OD'){
					$update['delivery_date'] = date('Y-m-d');
					$update['delivery_time'] = date('H:i:s');
				}
				if($status == 'OC'){
					$update['status'] = $status;
					$update['cancelled_on'] = date('Y-m-d');
					$update['cancel_acpt_rej_date'] = date('Y-m-d');
					$update['cancel_note'] = $reqData['cancel_note'];

					$OrderDetailTble = OrderDetail::where(['id'=>$id])->first();
		         	

            		$paymentDtls = Payment::where(['order_id'=>$order->order_master_id,'type'=>'order_payment'])->first();
            		if(!@$paymentDtls){
            			$response['error'] =__('errors.-5001');
		                $response['error']['message'] = 'Error';
		                $response['error']['meaning'] = "not found this id!";
		                return response()->json($response);
            		}
			        $txn = $paymentDtls->txn_id;
			        $URL='https://api.razorpay.com/v1/payments/'.$txn."/refund";
			        $PaymentKeyTable = PaymentKeyTable::first();  
			        if(!@$PaymentKeyTable){
			        	$response['error'] =__('errors.-5001');
		                $response['error']['message'] = 'Error';
		                $response['error']['meaning'] = "Key not found!";
		                return response()->json($response);	
			        }
			        $username=$PaymentKeyTable->RAZORPAY_KEY;
                    $password=$PaymentKeyTable->RAZORPAY_SECRET;
			        $headers = array(
			            "Authorization: Basic ".base64_encode($username.":".$password),
			            'Content-Type: application/json'
			        );
			        $refundFlds['amount'] = round($OrderDetailTble->payable_amount*100);
			        $ch = curl_init();
			        curl_setopt($ch, CURLOPT_URL,$URL);
			        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
			        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			        // Posting fields array in json format
			        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($refundFlds));
			        $result=curl_exec ($ch);
			        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
			        curl_close ($ch);
			        $jdres = json_decode($result);
			        $jeres = json_encode($result);
			        if($status_code != 200){
			        	$response['error'] =__('errors.-5001');
		                $response['error']['message'] = 'Error';
		                $response['error']['meaning'] = @$result;
		                return response()->json($response);
			        }
			        $crCancelRequest['order_master_id']=$order->order_master_id;
		         	$crCancelRequest['order_details_id']=$OrderDetailTble->id;
		         	$crCancelRequest['product_id']=$OrderDetailTble->product_id;
		         	$crCancelRequest['status']="accepted";
		         	$crCancelRequest['cancel_note']=$reqData['cancel_note'];
		         	$crCancelRequest['cancel_date']=date('Y-m-d');
		         	$crCancelRequest['amount']=$OrderDetailTble->payable_amount;

		            $crCancelRequest['refund_response'] = @$jeres;
		            $crCancelRequest['refund_id'] = @$jdres->id;
		         	$cancelrequesttbl = CancelRequest::create($crCancelRequest);
		         	$CancelRequest = CancelRequest::where(['id'=>$cancelrequesttbl->id])->first();
		         	if(!@$CancelRequest){
			        	$response['error'] =__('errors.-5001');
		                $response['error']['message'] = 'Error';
		                $response['error']['meaning'] = "provided wrong cancel_request_id!";
		                return response()->json($response);
			        }
		            
		            $crp['type'] = 'cancel_refund';
		            $crp['order_id'] = $CancelRequest->order_master_id;
		            $crp['cancel_request_id'] = $CancelRequest->id;
		            $crp['txn_id'] = @$jdres->id;
		            $crp['user_id'] = $CancelRequest->get_order_master->customerDetails->id;
		            $crp['response'] = @$jeres;
		            $crp['payment_initated_id'] = @$jdres->id;
		            $crp['amount'] = $CancelRequest->amount;
		            $crp['status'] = 'S';
		            $p = Payment::create($crp);

				}else{
					$update['status'] = $status;
				}
				
				OrderDetail::where('id',$id)->update($update);

				$totalReady = OrderDetail::where(['order_master_id' => $order->order_master_id, 'status' => 'OD', 'seller_id' => $order->seller_id])->whereNotIn('status',['OC'])->count();
				$totalReady1 = OrderDetail::where(['order_master_id' => $order->order_master_id, 'seller_id' => $order->seller_id])->whereNotIn('status',['OC'])->count();

                # updating order seller table if all item is ready for pickup of this seller.
				if($totalReady1 == $totalReady) {
					if($status == 'OD'){
						OrderSeller::where(['seller_id' => $order->seller_id, 'order_master_id' => $order->order_master_id])->update(['status' => 'CM','delivery_date'=>date('Y-m-d'),'delivery_time'=>date('H:i:s')]);
					}
				
					else{
						OrderSeller::where(['seller_id' => $order->seller_id, 'order_master_id' => $order->order_master_id])->update(['status' => 'INP']);
					}
				}
				$totalReady = OrderDetail::where(['order_master_id' => $order->order_master_id, 'status' => 'OC', 'seller_id' => $order->seller_id])->count();
				$totalReady1 = OrderDetail::where(['order_master_id' => $order->order_master_id, 'seller_id' => $order->seller_id])->count();

                # updating order seller table if all item is ready for pickup of this seller.
				if($totalReady1 == $totalReady) {
					if($status == 'OC'){
						OrderSeller::where(['seller_id' => $order->seller_id, 'order_master_id' => $order->order_master_id])->update(['status' => 'OC','delivery_date'=>date('Y-m-d'),'delivery_time'=>date('H:i:s')]);
					}
				}
                # updating order master table if all seller product is ready for pickup
				$totalReadyMaster = OrderDetail::where(['order_master_id' => $order->order_master_id, 'status' => $status])->count();
				$totalReadyMaster1 = OrderDetail::where(['order_master_id' => $order->order_master_id])->count();

				if($totalReadyMaster == $totalReadyMaster1) {
					if($status == 'OD'){
						$updmstrO['status'] = 'CM';
					}
					if($status == 'OC'){
						$tot_oc_cnt = OrderDetail::where(['order_master_id' => $order->order_master_id, 'status' => $status])->count();
						$o_cnt = OrderDetail::where(['order_master_id' => $order->order_master_id])->count();
						if($tot_oc_cnt == $o_cnt){
							$updmstrO['status'] = 'OC';	
						}else{
							$updmstrO['status'] = 'CM';
						}
						
					}
					$updmstrO['delivery_date'] = date('Y-m-d');
					$updmstrO['delivery_time'] = date('H:i:s');
					OrderMaster::where(['id' => $order->order_master_id])->update($updmstrO);
				}else{
					$tot_or_cn = OrderDetail::where(['order_master_id' => $order->order_master_id])->whereIn('status',['OC','OD'])->count();
					$tot_o_cn = OrderDetail::where(['order_master_id' => $order->order_master_id])->count();
					if($tot_or_cn == $tot_o_cn){
						$updmstrO['status'] = 'CM';
						$updmstrO['delivery_date'] = date('Y-m-d');
						$updmstrO['delivery_time'] = date('H:i:s');
						OrderMaster::where(['id' => $order->order_master_id])->update($updmstrO);	
					}

				}
				// if($status == 'OD'){
					$merchant = Merchant::where('id',@$user->id)->first();
				// 	$total_commission = @$order->admin_commission;
				// 	$toal_earn = (@$order->total - $total_commission) + $merchant->total_earning;
	   			//              $total_due = (@$order->total - $total_commission) + $merchant->total_due;
	   			//              $total_commission = $total_commission + $merchant->total_commission;
	                
	   			//              $merchant_update['total_earning'] = $toal_earn;
	   			//              $merchant_update['total_due'] = $total_due;
	   			//              $merchant_update['total_commission'] = $total_commission;
	   			//              Merchant::where('id',@$user->id)->update($merchant_update);
				// }

				$response['result'] = __('success.-4044');
				
				$orderMasterTable = OrderMaster::where(['id' => $order->order_master_id])->first();

				if($status == 'OD'){

					$this->calculateEarningOfO($orderMasterTable,$id);

					$this->sendMail($orderMasterTable,$id);

					// $response['result'] = __('success.-5001');
					$response['result'] = __('success.-4044');
					return response()->json($response);
				}
				else if($status == 'OC'){
					
					$this->cancelAproductOrder($orderMasterTable,$id);
				
					

					// $this->updateWallet($orderMasterTable,$id);
					$this->sendMail($orderMasterTable,$id);
					// $response['result'] = __('success.-5002');
					$response['result'] = __('success.-4044');
					
					return response()->json($response);
				}else{
					$this->sendMail($orderMasterTable,$id);
				}
			} else {
				$response['error'] = __('errors.-5041');
				$response['error']['meaning'] = "Either you have provided wrong id or this is already delivered or cancelled!";
			}
			return response()->json($response);
		}
		catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
			// $response['error'] = $e->getMessage();
			$response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
			return response()->json($response);
		}
		catch (\Exception $th) {
			// $response['error'] = $th->getMessage();
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
			return response()->json($response);
		}
	}
	public function calculateEarningOfO($order,$id){
        $orderDetail = OrderDetail::where(['order_master_id'=>$order->id,'id'=>$id,'status'=>'OD'])->first();
        $merchant = Merchant::where('id',$orderDetail->seller_id)->first();
        $OrderSeller = OrderSeller::where(['order_master_id'=>$order->id,'seller_id'=>$orderDetail->seller_id])->first();
        $merchantUpd['total_commission'] = $merchant->total_commission+$orderDetail->admin_commission;
        $merchantUpd['gross_earning'] = $merchant->gross_earning+$orderDetail->payable_amount-$orderDetail->insurance_price;
        $merchantUpd['net_earning'] = $merchant->net_earning+$orderDetail->payable_amount-$orderDetail->insurance_price-$orderDetail->admin_commission;
        $merchantUpd['commission_paid'] = $merchant->commission_paid+$orderDetail->admin_commission;
        $merchantUpd['total_earning'] = $merchantUpd['gross_earning'];
        $merchantUpd['total_due'] = $merchant->total_due+$orderDetail->payable_amount-$orderDetail->insurance_price-$orderDetail->admin_commission;
        $merchantUpd['total_insurance_price'] = $merchant->total_insurance_price+$orderDetail->insurance_price;
        $merchantUpd['total_loading_inloading_price'] = $merchant->total_loading_inloading_price+$orderDetail->loading_price;
        $merchantUpd['total_admin_commission'] = $merchant->total_admin_commission+$orderDetail->admin_commission;
        $merchantUpd['total_product_price'] = $merchant->total_product_price+$orderDetail->total;
        $merchantUpd['total_seller_commission'] = $merchant->total_seller_commission+$orderDetail->seller_commission;
        Merchant::where('id',$orderDetail->seller_id)->update($merchantUpd);
    }
    public function cancelAproductOrder($order,$id){
    	
    	$order = OrderDetail::where('id',$id)->first();
        $orderM = OrderMaster::where('id',$order->id)->first();

        $totdiscount = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('original_price') - OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('discounted_price');
        $total = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('total');
        $payable_amount = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('payable_amount');
        $sub_total = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('sub_total');
        $insurance_price = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('insurance_price');
        $loading_price = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('loading_price');
        $admin_commission = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('admin_commission');
        $total_seller_commission = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('seller_commission');
        $weight = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('weight');
        
        $updMstr['total_discount'] = $totdiscount;
        $updMstr['total_product_price'] = $total;
        $updMstr['payable_amount'] = $payable_amount;
        $updMstr['order_total'] = $payable_amount;
        $updMstr['subtotal'] = $sub_total;
        $updMstr['insurance_price'] = $insurance_price;
        $updMstr['loading_price'] = $loading_price;
        $updMstr['admin_commission'] = $admin_commission;
        $updMstr['total_seller_commission'] = $total_seller_commission;
        $updMstr['product_total_weight'] = $weight;
        OrderMaster::where(['id'=>$order->order_master_id])->update($updMstr);

        $ords = OrderSeller::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->first();
        $updOrds['loading_price'] = $ords->loading_price-$order->loading_price;
        $updOrds['insurance_price'] = $ords->insurance_price-$order->insurance_price;
        // $updOrds['admin_commission'] = $ords->admin_commission-$order->admin_commission;
        $updOrds['seller_commission'] = $ords->seller_commission-$order->seller_commission;
        $updOrds['payable_amount'] = $ords->payable_amount-$order->payable_amount;
        $updOrds['admin_commission'] = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->whereNotIn('status',['OC'])->sum('admin_commission');
        $updOrds['subtotal'] = $ords->subtotal-$order->sub_total;
        $dis = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->whereNotIn('status',['OC'])->sum('original_price') - OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->whereNotIn('status',['OC'])->sum('discounted_price');

        $updOrds['total_discount'] = $dis;
        $updOrds['order_total'] = $ords->order_total-$order->payable_amount;
        $updOrds['total_commission'] = $ords->total_commission;
        $updOrds['earning'] = $ords->earning - $order->loading_price - $order->seller_commission - ($order->total-$order->admin_commission);
        $weight = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->whereNotIn('status',['OC'])->sum('weight');
        $updOrds['total_weight'] = $weight;
        OrderSeller::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->update($updOrds);
    }
    public function updateWallet($orderM,$orderDetailsId){
    	$order = OrderDetail::where(['id'=>$orderDetailsId])->first();
    	$userInfo = User::where(['id'=>$orderM->user_id])->first();
                $updUser['wallet_amount'] = $userInfo->wallet_amount+$order->payable_amount;
                User::where(['id'=>$orderM->user_id])->update($updUser);

                $wcr['amount'] = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereIn('status',['OC'])->sum('payable_amount');
                $wcr['type'] = 'IN';
                $wcr['description'] = " Refund money of ".getCurrency()." ".$wcr['amount']." for ".$orderM->order_no;
                $wcr['w_date'] = date('Y-m-d');
                $walletHistory = Wallet::where(['user_id'=>$userInfo->id,'order_id'=>$orderM->id])->first();
                
                if(!@$walletHistory){
                    $wcr['user_id'] = $userInfo->id;
                    $wcr['order_id'] = $orderM->id;
                    $wcr['order_no'] = $orderM->order_no;
                    $wallet = Wallet::create($wcr);
                }else{
                    $wallet = Wallet::where(['user_id'=>$userInfo->id,'order_id'=>$orderM->id])->update($wcr);
                }
                $walletHistory = Wallet::where(['user_id'=>$userInfo->id,'order_id'=>$orderM->id])->first();
                $productDtals = Product::with(['productByLanguage'])->where(['id'=>$order->product_id])->first();

                $wdcr['wallet_id'] = $walletHistory->id;
                $wdcr['amount'] = $order->payable_amount;
                $wdcr['type'] = 'IN';
                $wdcr['description'] = " Refund money of ".getCurrency()." ".$wdcr['amount']." for ".@$productDtals->productByLanguage->title." in ".$orderM->order_no;
                $wdcr['w_d_date'] = date('Y-m-d');
                $wdcr['user_id'] = $orderM->user_id;
                $wdcr['order_id'] = $orderM->id;
                $wdcr['product_id'] = $order->product_id;
                $wdcr['order_details_id'] = $order->id;
                $walletdetails = WalletDetails::create($wdcr);
    }
    public function sendMail($orderM,$orderDetailsId){
    	try{
    	$gorderDetails = OrderDetail::where(['id'=>$orderDetailsId])->first();
    	$order = OrderMaster::with(['customerDetails.userCountryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails.getOrderDetailsUnitMaster',
            'orderMasterDetails.productDetails.productByLanguage',
            'orderMasterDetails.productDetails.productVariants',
            'orderMasterDetails.productDetails.productUnitMasters',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails.merchantCityDetails',
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'orderMerchants.orderSeller',
            'orderMerchants'=> function($query) use($gorderDetails) {
            $query->where(function($q) use($gorderDetails) {
                $q->where('seller_id', $gorderDetails->seller_id);
            });
        },
            'getBillingCountry',
            'productVariantDetails',
            'getOrderAllImages',
            'orderMasterDetails.productDetails.defaultImage',
            'orderMasterExtDetails.sellerDetails',
            'orderMasterDetails.driverDetails',
            'orderMasterExtDetails.driverDetails',
            'getCityNameByLanguage',
            'getBillingCityNameByLanguage',
            'paymentData',
            'getpickupCountryDetails',
            'getpickupCityDetails',
            'getPreferredTime'])->where('id',$orderM->id)->first();
            $data['order'] = @$order;
            Mail::send(new SendOrderMail($data));
            foreach(@$order->orderMerchants as $o){

                $order = OrderMaster::with(['customerDetails.userCountryDetails.countryDetailsBylanguage',
                'orderMasterDetails'=> function($query) use($o) {
                    $query->where(function($q) use($o){
                        $q->where('seller_id', $o->seller_id);
                    });
                },
                'orderMerchants'=> function($query) use($o) {
                    $query->where(function($q) use($o){
                        $q->where(['seller_id'=>$o->seller_id]);
                    });
                },
                'orderMerchants.orderSeller',
                'countryDetails.countryDetailsBylanguage',
                'orderMasterDetails.getOrderDetailsUnitMaster',
                'orderMasterDetails.productDetails.productByLanguage',
                'orderMasterDetails.productDetails.productVariants',
                'orderMasterDetails.productDetails.productUnitMasters',
                'orderMasterDetails.productVariantDetails',
                'orderMasterDetails.sellerDetails.merchantCityDetails',
                'countryDetails.countryDetailsBylanguage',
                'shippingAddress',
                'billingAddress',
                'getBillingCountry',
                'productVariantDetails',
                'getOrderAllImages',
                'orderMasterDetails.productDetails.defaultImage',
                'orderMasterExtDetails.sellerDetails',
                'orderMasterDetails.driverDetails',
                'orderMasterExtDetails.driverDetails',
                'getCityNameByLanguage',
                'getBillingCityNameByLanguage',
                'paymentData',
                'getpickupCountryDetails',
                'getpickupCityDetails',
                'getPreferredTime'])->where('id',$orderM->id)->first();
                $data['order'] = @$order;
                Mail::send(new SendMerchantOrderMail($data));    
            }
        }catch(\Exception $e){
        	
        }
    }
}
