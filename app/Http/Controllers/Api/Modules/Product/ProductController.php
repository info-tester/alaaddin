<?php

namespace App\Http\Controllers\Api\Modules\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Variant;
use App\Models\OrderDetail;
use App\Models\Brand;
use App\Models\UserFavorite;
use App\Models\CartDetail;
use App\Models\CartMaster;
use App\Models\ProductNotification;
use App\Models\ProductCategory;
use App\Merchant;
use App\Models\BannerContent;
use App\Models\ProductVariant;
use App\Models\ProductVariantDetail;
use App\Repositories\BrandRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\UserFavoriteRepository;
use App\Repositories\ProductOtherOptionRepository;
use App\Repositories\ProductCategoryRepository;

use JWTFactory;
use JWTAuth;
use Validator;
use Session;
use Config;
use Auth;
class ProductController extends Controller
{
    protected $brands, $products, $otherOptions, $userFavorite;

    public function __construct(BrandRepository $brands,
        ProductRepository $products,
        CategoryRepository $category,
        ProductOtherOptionRepository $otherOptions,
        UserFavoriteRepository $userFavorite,
        ProductCategoryRepository $product_cat
    )
    {
        $this->brands           = $brands;
        $this->products         = $products;
        $this->category         = $category;
        $this->userFavorite     = $userFavorite;
        $this->otherOptions     = $otherOptions;
        $this->product_cat      = $product_cat;
    }

    /**
    * Method: productDetails
    * Description: This method is used to change user password
    * Author: Surajit 
    */
    /**
    * @OA\Post(
    * path="/api/product-details",
    * summary="Product Details",
    * description="Product Details",
    * operationId="productDetails",
    * tags={"Alaaddin: Product Details"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer credentials",
    *    @OA\JsonContent(
    *       required={"product_id"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="product_id", type="integer", format="product_id", example="24"),
    *           @OA\Property(property="user_id", type="integer", format="product_id", example="16"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function productDetails(Request $request) {
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $validator = Validator::make($reqData['params'],[
                'product_id'      => 'required|numeric',
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
        $id = $reqData['params']['product_id'];
        $user = auth()->user();
        if(!@$user){
            $user = User::where(['id'=>$reqData['params']['user_id']])->first();
            if(@$user){
                $userId = $user->id;    
            }else{
                $userId = 0;
            }
            
        }
            if(@$user){
                $userId = $user->id;    
            }else{
                $userId = 0;
            }
        // $response['result']['m'] = $user;

        $product = Product::select('id', 
                'slug', 
                'price', 
                'user_id', 
                'discount_price',
                'from_date',
                'total_review',
                'total_no_reviews',
                'avg_review',
                'unit_master_id',
                'to_date',
                'stock',
                'product_hide',
                'status',
                'product_code'
            )->with([
            'images:product_id,image', 
            'wishlist',
            'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }, 
            'productByLanguage',
            'productMarchant',
            'productUnitMasters:id,unit_name,status',
            'getProductCategory.Category'
            // 'productParentCategory',
            // 'productSubCategory'
        ])
        // ->where(['product_hide' => 'N', 'status' => 'A'])
        ->where(function($q) use($id) {
            $q->orWhere(['slug' => $id, 'id' => $id]);
        })
        // ->whereHas('productMarchant', function($q) {
        //     $q->where('status', 'A');
        //     $q->where('hide_merchant', 'N');
        // })
        ->addSelect(\DB::raw('product_price AS discount_price','unit_master_id'))
            ->addSelect(\DB::raw('round(((price - product_price) / price) * 100) As percentage'));
        
        // if(@$user){
            // $product = $product->whereHas('wishlistfavourite', function($q) use($user){
            //     $q->where('user_id', $user->id);
            // });
            // $product = $product->whereHas('wishlist', function($q) use($user){
            //     $q->where('user_id', $user->id);
            // });
        // }
        $product = $product->first();
        if(!@$product){
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] ="You have send the wrong product id";
            return response()->json($response);
        }
        $response['result']['product']['product_details'] = $product;
        
        if(@$product) {
            $productId = $id;
            $related_product = Product::select('id', 
                'slug', 
                'price', 
                'user_id', 
                'discount_price',
                'from_date',
                'total_review',
                'total_no_reviews',
                'avg_review',
                'unit_master_id',
                'to_date',
                'stock',
                'product_code'
            )
            ->with([
                'productMarchant', 
                'images:product_id,image', 
                'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }, 
                    'wishlist',
                'productByLanguage','getProductCategory.Category','productUnitMasters:id,unit_name,status','defaultImage'
            // 'productParentCategory',
            // 'productSubCategory'
            ])
            ->addSelect(\DB::raw('product_price AS discount_price'))
            ->addSelect(\DB::raw('round(((price - product_price) / price) * 100) As percentage'))
            ->where(['product_hide' => 'N', 'status' => 'A'])
            ->where('id','!=',@$id)
            ->whereHas('productMarchant', function($q) {
                $q->where('status', 'A');
                $q->where('hide_merchant', 'N');
            });
            // if(@$user){
            //     $related_product = $related_product->whereHas('wishlistfavourite', function($q) use($user){
            //         $q->where('user_id', $user->id);
            //     });
            // }
            $related_product = $related_product->take(15)->get();
            $seller_products = Product::with([
                'defaultImage:product_id,image', 
                'productByLanguage',
                'wishlist',
                'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    },'getProductCategory.Category','productUnitMasters:id,unit_name,status'
            // 'productParentCategory',
            // 'productSubCategory'
                /*'productParentCategory.Category',
                'productSubCategory.Category',
                'productVariants.productVariantDetails',*/
                // 'productMarchant'
            ])
            ->addSelect(\DB::raw('product_price AS discount_price'))
            ->addSelect(\DB::raw('round(((price - product_price) / price) * 100) As percentage'))
            ->where(['user_id' => $product->user_id])
            ->where(['status' => 'A', 'product_hide' => 'N', 'seller_status' => 'A']);
            // if(@$user){
            //     $seller_products = $seller_products->whereHas('wishlistfavourite', function($q) use($user){
            //         $q->where('user_id', $user->id);
            //     });
            // }
            $seller_products = $seller_products->orderBy('id','desc')->take(15)->get();

            $seller  = Merchant::with(['State','Country','city_details'])->where(['id' => $product->user_id, 'status' => 'A', 'hide_merchant' => 'N'])
            ->select('id','slug','fname','lname','image','company_name','company_type','company_details','city','state','country','image','rate','total_no_review','total_review_rate')
            ->first();

            $productCount =  $seller_products->pluck('id');
            if(count($productCount) > 0) {
                $product_cat = $this->product_cat->whereIn('product_id', $product)->groupBy('category_id')->with('Category')->where('level', 'P')->get();
            } else {
                $product_cat = '';
            }

            // $user = auth()->user();
            // return response()->json($response);
            $wi = "False";
            if(@$userId){
                $fav = UserFavorite::where(['user_id'=>$userId,'product_id'=>$product->id])->first();
                if($fav){
                    $wi = "True";
                }    
                else{
                    $wi = "False";
                }
            }

            $response['result']['product']['wish'] = $wi;
            $response['result']['product']['product_details'] = $product;
            $response['result']['product']['related_product'] = $related_product;
            $response['result']['seller'] = $seller;
            $response['result']['seller_products'] = $seller_products;
            $response['result']['reviews'] = OrderDetail::with(['orderMaster.customerDetails'])->where(['product_id'=>$product->id])->where('rate','>',0)->orderBy('id','desc')->take(10)->get();
            $response['result']['tot_reviews_count'] = OrderDetail::with(['orderMaster.customerDetails'])->where(['product_id'=>$product->id])->where('rate','>',0)->orderBy('id','desc')->count();
            if(@$response['result']['tot_reviews_count'] > 10){
                $response['result']['has_more_reviews'] = 'Y';
            }else{
                $response['result']['has_more_reviews'] = 'N';
            }
            $cartMasterTable = CartMaster::where(['user_id'=>$userId])->first();
            if(@$cartMasterTable){
                $show_cart_detail = CartDetail::where(['product_id'=>$product->id,'cart_master_id'=>$cartMasterTable->id])->first();    
            }
            if(@$show_cart_detail){
                $response['result']['is_already_added_in_cart'] = 1;    
                // $response['result']['show_cart_detail'] = $show_cart_detail;    
                // $response['result']['cartMasterTable'] = $cartMasterTable;    
            }else{
                $response['result']['is_already_added_in_cart'] = 0;
            }
            return response()->json($response);
        } else {
            // $response['error'] = ERRORS['-33029'];
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] ="You have send the wrong product";
        }
        return response()->json($response);
    }
    /**
    * @OA\Post(
    * path="/api/see-all-related-products",
    * summary="see all related products",
    * description="seeAllRelatedProducts Details",
    * operationId="seeAllRelatedProducts",
    * tags={"Alaaddin: see all related products"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="see all related products",
    *    @OA\JsonContent(
    *       required={"product_id"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="product_id", type="integer", format="product_id", example="24"),
    *           @OA\Property(property="user_id", type="integer", format="product_id", example="16"),
    *           @OA\Property(property="offset", type="integer", format="product_id", example="8"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="see all related products",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry Please try again")
    *        )
    *     )
    * )
    */
    public function seeAllRelatedProducts(Request $request) {
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $id = $reqData['params']['product_id'];
        $user = auth()->user();
        if(!@$user){
            $user = User::where(['id'=>$reqData['params']['user_id']])->first();
            if(@$user){
                $userId = $user->id;    
            }else{
                $userId = 0;
            }
            
        }
            if(@$user){
                $userId = $user->id;    
            }else{
                $userId = 0;
            }
        // $response['result']['m'] = $user;

        $product = Product::select('id', 
                'slug', 
                'price', 
                'user_id', 
                'discount_price',
                'from_date',
                'total_review',
                'total_no_reviews',
                'avg_review',
                'unit_master_id',
                'to_date',
                'stock',
                'product_hide',
                'status',
                'product_code'
            )->with([
            'images:product_id,image', 
            'wishlist',
            'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }, 
            'productByLanguage',
            'productMarchant',
            'productUnitMasters:id,unit_name,status',
            'getProductCategory.Category'
            // 'productParentCategory',
            // 'productSubCategory'
        ])
        ->where(['product_hide' => 'N', 'status' => 'A'])
        ->where(function($q) use($id) {
            $q->orWhere(['slug' => $id, 'id' => $id]);
        })
        ->whereHas('productMarchant', function($q) {
            $q->where('status', 'A');
            $q->where('hide_merchant', 'N');
        })->addSelect(\DB::raw('product_price AS discount_price','unit_master_id'))
            ->addSelect(\DB::raw('round(((price - product_price) / price) * 100) As percentage'));
        
        // if(@$user){
            // $product = $product->whereHas('wishlistfavourite', function($q) use($user){
            //     $q->where('user_id', $user->id);
            // });
            // $product = $product->whereHas('wishlist', function($q) use($user){
            //     $q->where('user_id', $user->id);
            // });
        // }
        $product = $product->first();
        $response['result']['product']['product_details'] = $product;
        
        if(@$product) {
            $productId = $id;
            $offset = @$reqData['params']['offset']?$reqData['params']['offset']:0; 
            $limit = 10;
            if($offset == 1) {
                $start = 0;
            } else {
                $start = (($offset -1) * $limit);
            }
            $related_product = Product::select('id', 
                'slug', 
                'price', 
                'user_id', 
                'discount_price',
                'from_date',
                'total_review',
                'total_no_reviews',
                'avg_review',
                'unit_master_id',
                'to_date',
                'stock',
                'product_code'
            )
            ->with([
                'productMarchant', 
                'images:product_id,image', 
                'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    }, 
                    'wishlist',
                'productByLanguage','getProductCategory.Category','productUnitMasters:id,unit_name,status','defaultImage'
            // 'productParentCategory',
            // 'productSubCategory'
            ])
            ->addSelect(\DB::raw('product_price AS discount_price'))
            ->addSelect(\DB::raw('round(((price - product_price) / price) * 100) As percentage'))
            ->where(['product_hide' => 'N', 'status' => 'A'])
            ->where('id','!=',@$id)
            ->whereHas('productMarchant', function($q) {
                $q->where('status', 'A');
                $q->where('hide_merchant', 'N');
            });

            $response['result']['mp_total'] =$related_product->count();
            $response['result']['mp_count'] =ceil($related_product->count() /$limit);
            $response['result']['mp_per_page'] =$limit;
            $related_product = $related_product->orderBy('id','desc')->skip($start)->take($limit)->get();
            // $related_product = $related_product->take(15)->get();

            $seller_products = Product::with([
                'defaultImage:product_id,image', 
                'productByLanguage',
                'wishlist',
                'wishlistfavourite'=> function($query) use($userId) {
                        $query->where(function($q) use($userId) {
                            $q->where(['user_id'=>$userId]);
                        });
                    },'getProductCategory.Category','productUnitMasters:id,unit_name,status'
            ])
            ->addSelect(\DB::raw('product_price AS discount_price'))
            ->addSelect(\DB::raw('round(((price - product_price) / price) * 100) As percentage'))
            ->where(['user_id' => $product->user_id])
            ->where(['status' => 'A', 'product_hide' => 'N', 'seller_status' => 'A']);
            $seller_products = $seller_products->orderBy('id','desc')->take(15)->get();

            $seller  = Merchant::with(['State','Country','city_details'])->where(['id' => $product->user_id, 'status' => 'A', 'hide_merchant' => 'N'])
            ->select('id','slug','fname','lname','image','company_name','company_type','company_details','city','state','country','image','rate','total_no_review','total_review_rate')
            ->first();

            $productCount =  $seller_products->pluck('id');
            if(count($productCount) > 0) {
                $product_cat = $this->product_cat->whereIn('product_id', $product)->groupBy('category_id')->with('Category')->where('level', 'P')->get();
            } else {
                $product_cat = '';
            }

            // $user = auth()->user();
            // return response()->json($response);
            $wi = "False";
            if(@$userId){
                $fav = UserFavorite::where(['user_id'=>$userId,'product_id'=>$product->id])->first();
                if($fav){
                    $wi = "True";
                }    
                else{
                    $wi = "False";
                }
            }
            $response['result']['related_product'] = $related_product;
            $response['result']['product']['wish'] = $wi;
            $response['result']['product']['product_details'] = $product;
            
            $response['result']['seller'] = $seller;
            $response['result']['seller_products'] = $seller_products;
            
        } else {
            // $response['error'] = ERRORS['-33029'];
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] ="You have send the wrong product";
        }
        return response()->json($response);
    }
    public function productDetails1(Request $request) {
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $id = $reqData['params']['product_id'];
        //$user = auth()->user()->id;
        $user = @getUserByToken();
        //dd(@getUserByToken()->id,$user);
        $product = Product::select('id', 
            'slug', 
            'price', 
            'user_id', 
            'discount_price',
            'from_date',
            'total_review',
            'total_no_reviews',
            'avg_review',
            'to_date',
            'stock',
            'product_code'
        )
        ->with([
            'images:product_id,image', 
            'productByLanguage',
            'wishlistfavourite'=> function($query) use($user) {
                        $query->where(function($q) use($user) {
                            $q->where(['user_id'=>@$user->id]);
                        });
                    },
            // 'wishlist',
            'productVariants.productVariantDetails',
            'productUnitMasters:id,unit_name,status',
            'getProductCategory.Category'
            // 'productParentCategory',
            // 'productSubCategory',
            //'productParentCategory.Category',
            //'productSubCategory.Category',
            // 'productMarchant'
        ])
        ->where(['product_hide' => 'N', 'status' => 'A'])
        ->where(function($q) use($id) {
            $q->orWhere(['slug' => $id, 'id' => $id]);
        })
        ->whereHas('productMarchant', function($q) {
            $q->where('status', 'A');
            $q->where('hide_merchant', 'N');
        });
        
        // if(@$user){
        //     $product = $product->whereHas('wishlistfavourite', function($q) use($user){
        //         $q->where('user_id', $user->id);
        //     });
        //     $product = $product->whereHas('wishlist', function($q) use($user){
        //         $q->where('user_id', $user->id);
        //     });
        // }
        $product = $product->first();
        if(@$product) {
            $productId = $id;
            $product_features = $this->otherOptions->with([
                'productVariantByLanguage.variantByLanguage',
                'productVariantDetails' => function($q) use($id){
                    $q->where('product_id', $id);
                },
                'productVariantDetails.variantValueByLanguage'
            ])
            ->where(['product_id' => $id])
            ->groupBy('variant_id')
            ->get();
            $proParentCat = ProductCategory::where(['product_id' =>  $product->id , 'level' => 'P'])->first();
            $proSubCat = ProductCategory::where(['product_id' =>  $product->id , 'level' => 'S'])->first();
            $productVariants = ProductVariant::where([
                'product_id' => $id
            ])
            ->pluck('id');
            $all_variants = ProductVariantDetail::select('id','product_id','product_variant_id','group_id','variant_id','variant_value_id')
            ->whereIn('product_variant_id', $productVariants)
            ->groupBy('variant_id')
            ->with([
                'variantByLanguage',
                // 'productByLanguage',
                'variantValues' => function($q) use($productVariants) {
                    $q->whereIn('product_variant_id', $productVariants);
                },
                'variantValues.variantValueByLanguage',
                'variantValues.variantValueName'
            ])
            ->with('variantByLanguage')
            ->orderBy('id')
            ->get();
            
            $proParentCatDetails = Category::select('id','slug')->where('id',$proParentCat->category_id)->first();
            $proSubCatDetails = Category::select('id','slug')->where('id',$proSubCat->category_id)->first();

            $seller_products = $this->products->select('id', 
                'slug', 
                'price', 
                'user_id', 
                'discount_price',
                'from_date',
                'to_date'
            )
            ->with([
                'defaultImage:product_id,image', 
                'productByLanguage',
                'wishlist',
                'productParentCategory:id,product_id,category_id', 
                'productSubCategory:id,product_id,category_id',
                /*'productVariants.productVariantDetails',*/
                // 'productMarchant'
            ])
            ->whereHas('productParentCategory', function($q) use($proParentCat){
                $q->where(['category_id' =>  $proParentCat->category_id ]);
            })
            ->whereHas('productSubCategory', function($q) use($proSubCat){
                $q->where(['category_id' =>  $proSubCat->category_id ]);
            })
            ->where('id','!=',$id)
            ->where(['status' => 'A', 'product_hide' => 'N', 'seller_status' => 'A'])
            ->orderBy('id','desc')
            ->limit(10)
            ->get();

            $seller  = Merchant::where(['id' => $product->user_id, 'status' => 'A', 'hide_merchant' => 'N'])
            ->select('id','slug','fname','lname','company_name','image','rate','total_no_review','total_review_rate')
            ->first();

            $productCount =  $seller_products->pluck('id');
            if(count($productCount) > 0) {
                $product_cat = $this->product_cat->whereIn('product_id', $product)->groupBy('category_id')->with('Category')->where('level', 'P')->get();
            } else {
                $product_cat = '';
            }
            $user = @getUserByToken();
            
            $wi = "False";
            if(@$user->id){
                $fav = UserFavorite::where(['user_id'=>$user->id,'product_id'=>$product->id])->first();
                if($fav){
                    $wi = "True";
                }    
                else{
                    $wi = "False";
                }
            }
            
            $response['result']['product']['wish'] = $wi;
            $response['result']['product']['product_details'] = $product;
            $response['result']['product']['product_features'] = $product_features;
            $response['result']['product']['all_variants'] = $all_variants;
            $response['result']['product']['category'] = @$proParentCatDetails;
            $response['result']['product']['subcategory'] = @$proSubCatDetails;
            $response['result']['seller'] = $seller;
            $response['result']['seller_products'] = $seller_products;
        } else {
            // $response['error'] = ERRORS['-33029'];
            $response['error'] =__('errors.-32700');
        }
        return response()->json($response);
    }
    /**
    * Method: productPrice
    * Description: This method is used to get product price depend on variant value combinations
    * Author: Surajit 
    */
    public function productPrice(Request $request) {
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $id = $reqData['params']['product_id'];
        $variant_val = $reqData['params']['value_id'];

        $product = $this->products->select('id', 
            'slug', 
            'price', 
            'user_id', 
            'discount_price',
            'from_date',
            'to_date',
            'product_code'
        )
        ->with([
            'productVariants'
        ])
        ->where(['id' => $id, 'product_hide' => 'N', 'status' => 'A'])
        ->first();
        
        if(@$product) {
            $variant_val = json_decode($variant_val);
            sort($variant_val);
            $variant_val = json_encode($variant_val);
            foreach ($product->productVariants as $value) {
                $variant_val_pro = json_decode($value->variants);
                sort($variant_val_pro);
                $variant_val_pro = json_encode($variant_val_pro);
                
                if($variant_val_pro == $variant_val) {
                    $product = $value;
                    break;
                } else {
                    $product = $value;
                }
            }
            $user = auth()->user();
            $wi = "False";
            if(@$user->id){
                $fav = UserFavorite::where(['user_id'=>$user->id,'product_id',$product->id])->first();
                if($fav){
                    $wi = "True";
                }    
                else{
                    $wi = "False";
                }
            }
            $response['result']['wish'] = $wi;
            $response['result']['product'] = $product;
        } else {
            // $response['error']['message'] = ERRORS['-33029'];
            $response['error']['message'] = __('errors.-32700');
        }
        return response()->json($response);
    }


    /**
    *@ Method: getCategories
    *@ Description: fetch category and subcategories of keyword
    *@ Author: Surajit
    */
    public function getCategories(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $reqData = $request->json()->all();
            $keyword = $reqData['params']['keyword'];
            if(@$keyword)
            {
                $parentCategory = Category::with([
                    'categoryDetails',
                    'subCategories',
                    'subCategories.categoryDetails'
                ])->where(['parent_id' => 0,'status' => 'A']);
                $parentCategory = $parentCategory->whereHas('categoryDetails', function($q1) use ($keyword){
                    $q1->whereRaw("UPPER(title) LIKE '%". strtoupper($keyword)."%'");
                })->get();

                $subCategory = Category::with([
                    'categoryDetails',
                    'parentCatDetails'
                ])->where('parent_id',"!=",0)->where(['status' => 'A']);
                $subCategory = $subCategory->whereHas('categoryDetails', function($q2) use ($keyword){
                    $q2->whereRaw("UPPER(title) LIKE '%". strtoupper($keyword)."%'");
                })->get();


                $response['result']['parent_categories'] = $parentCategory;
                $response['result']['searched_sub_categories'] = $subCategory;
            } else {
                // $response['error']['message'] = ERRORS['-33029'];  
                $response['error']['message'] = __('errors.-32700');  

            }
            return response()->json($response);

        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }

    /**
    * Method: changeLanguage
    * Description: This method is used to change language
    */
    public function changeLanguage(Request $request) {
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $lang = $reqData['params']['lang'];

        if (array_key_exists($lang, Config::get('languages'))) {            
            Session::put('applocale', $lang);
            Config::set('app.locale', $lang);
        }
        $response['result']['status'] = 'SUCCESS';
        $response['result']['lang'] = $lang;
        return response()->json($response);
    }



    /**
    * Method: searchProducts
    * Description: This method is used to search
    * Author: Surajit
    */
    public function searchProducts(Request $request) {
        $reqData = $request->json()->all();
        $response = [
            'jsonrpc'   => '2.0'
        ];
        // return json_encode($request->json()->all());
        $data = [];
        $data['products'] = $this->products->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date')->with([
            // 'productVariants.productVariantDetails.variantValueByLanguage', 
            // 'productCategory',
            'defaultImage',
            // 'productOtherVariants',
            'productByLanguage',
            // 'productMarchant:id,fname,lname',
            'wishlist'
        ]);

        // if params is category then all variants of this category
        if(@$reqData['params']['sub_category']) {
            $category = $this->category->with(['subCategories'])->where(['slug' => urldecode($reqData['params']['sub_category'])])->with('categoryByLanguage','parentCatDetails')->first();
            $response['result']['sub_category_details'] = $category;

            $data['products'] = $data['products']->whereHas('productCategory', function($query) use($category) {
                $query->where('category_id', $category->id);
                $query->where('level', 'S');
            });
        } elseif(@$reqData['params']['category']) {
            $cat = $this->category->where(['slug' => $reqData['params']['category']])->with('categoryByLanguage','categories.categoryByLanguage')->first();
            $response['result']['category_details'] = $cat;
            if($cat) {
                $data['products'] = $data['products']->whereHas('productCategory', function($query) use($cat) {
                    $query->where('category_id', $cat->id);
                    $query->where('level', 'P');
                });
            }
        }

        if(@$reqData['params']['brands']) {
            $data['products'] = $data['products']->whereIn('brand_id', $reqData['params']['brands']);
        }

        if(@$reqData['params']['variant']) {
            $variants = $reqData['params']['variant'];
            $data['products'] = $data['products']->whereHas('productVariants', function($q) use($variants) {
                foreach ($variants as $key => $value) {
                    if($value) {
                        $q->whereHas('productVariantDetails', function($q1) use($key, $value) {
                            $q1->where('variant_id', $key)
                            ->whereIn('variant_value_id', $value);
                        });
                    }
                }
            });

        }

        if(@$reqData['params']['other_variant']) {
            foreach ($reqData['params']['other_variant'] as $key => $value) {
                if($value) {
                    $data['products'] = $data['products']->whereHas('productOtherVariants', function($q1) use($key, $value) {
                        $q1 = $q1->where('variant_id', $key)
                        ->whereIn('variant_value_id', $value);

                    });
                }
            }
        }

        $response['result']['min_price'] = $data['products']->min('price');
        $response['result']['max_price'] = $data['products']->max('price');
        
        if(@$reqData['params']['price']) {
            $data['products'] = $data['products']->whereBetween('price', $reqData['params']['price']);
        }

        $data['products'] = $data['products']->where(['status' => 'A', 'seller_status' => 'A', 'product_hide' => 'N']);
        if(@$reqData['params']['order_by'] == 'H') {
            $data['products'] = $data['products']->orderBy('price', 'DESC')->get();    
        } else if(@$reqData['params']['order_by'] == 'L') {
            $data['products'] = $data['products']->orderBy('price', 'ASC')->get();    
        } else if(@$reqData['params']['order_by'] == 'N') {
            $data['products'] = $data['products']->orderBy('id', 'DESC')->get();    
        } else if(@$reqData['params']['order_by'] == 'A') {
            $data['products'] = $data['products']->whereHas('productByLanguage', function($query1) {
                $query1->orderBy('title', 'ASC');
            })->get();
        } else {
            $data['products'] = $data['products']->orderBy('id', 'DESC')->get();
        }


        $allCategory = Category::where(['parent_id' => 0, 'status' => 'A'])->with('categoryByLanguage','categories.categoryByLanguage')->get();

        $response['result']['products'] = $data['products'];
        $response['result']['all_categories'] = $allCategory;
        
        return response()->json($response);
    }

    /**
    * Method: getFilterVal
    * Description: This method is used to get values in filter
    * Author: Surajit
    */
    public function getFilterVal(Request $request) {
        $reqData = $request->json()->all();
        $response = [
            'jsonrpc'   => '2.0'
        ];

        //get brand and category details
        if(@$reqData['params']['cat_slug']) {
            $response['result']['category'] = $category =  $this->category->where(['slug' => @$reqData['params']['cat_slug']])->with('categoryByLanguage','categories.categoryByLanguage')->first();
            $response['result']['brands'] = $this->brands->with(['brandDetailsByLanguage'])
            ->withCount('productsByBrand')
            ->whereHas('productsByBrand', function($query) {
                $query->where('status', 'A');
                $query->where('product_hide', 'N');
            }, '>=', 1)
            ->findWhere(['status' => 'A', 'category_id' => @$category->id]);
        }

        //get variants and sub_category details
        if(@$reqData['params']['sub_cat_slug']) {
            $response['result']['sub_category'] = $subCategory = $this->category->where(['slug' => @$reqData['params']['sub_cat_slug']])->with('categoryByLanguage','parentCatDetails')->first();
            $response['result']['variants'] = Variant::with([
                'variantByLanguage', 
                'variantValues.variantValueByLanguage'])
            ->where('sub_category_id', $subCategory->id)
            ->where('status', 'A')
            ->whereIn('type', ['P', 'S'])
            ->get();

            $response['result']['other_variants'] = Variant::with([
                'variantByLanguage', 
                'variantValues.variantValueByLanguage'
            ])
            ->where('sub_category_id', $subCategory->id)
            ->where('status', 'A')
            ->whereIn('type', ['SH', 'I'])
            ->get();
        }

        $response['result']['min_price'] = @$reqData['params']['min_price'];
        $response['result']['max_price'] = @$reqData['params']['max_price'];
        
        return response()->json($response);
    }

    /**
    *
    */
    public function getParentCategory($subCatId) {
        $response = [
            'jsonrpc'   => '2.0'
        ];
        $response['result']['category'] = $this->category->where(['status' => 'A'])
        ->where('slug', $subCatId)
        ->with(['categoryByLanguage'])
        ->withCount(['productBySubCategory' => function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        }])
        ->first();

        $response['result']['parent_category'] = $this->category->where(['status' => 'A'])
        ->where('id', $response['result']['category']->parent_id)
        ->with(['categoryByLanguage'])
        ->withCount(['productByCategory' => function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        }])
        ->first();
        return response()->json($response, 200);
    }

    /**
    * 
    */
    public function getSubCategories($slug) {
        $response = [
            'jsonrpc'   => '2.0'
        ];

        // parent category details
        $response['result']['parent_category'] = Category::with(['categoryByLanguage'])
        ->withCount(['productBySubCategory' => function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        }])
        ->where(['slug' => $slug])->first();
                            // return $response['result']['parent_category']->id;

        // all sub category details 
        $response['result']['sub_categories'] = $this->category->where(['status' => 'A'])
        ->where('parent_id', @$response['result']['parent_category']->id)
        ->with(['categoryByLanguage'])
        ->withCount(['productBySubCategory' => function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        }])
        ->whereHas( 'productBySubCategory', function($query) {
            $query->whereHas('activeProducts', function($q) {
                $q->where('status', 'A');
                $q->where('product_hide', 'N');
            });
        })
        ->get();
        return response()->json($response, 200);
    }

    /**
    * Method: updateFirebaseToken
    * Description: This method is used to update firebase token
    * Author: Sanjoy
    */
    public function updateFirebaseToken(Request $request) {
        $params = $request->params;
        $user = getUserByToken();
        if(@$params['variants']) {
            $variant = $this->getVariantId($params);
        }
        $email = '';
        if(@$user) {
            $email = $user->email;
        } else {
            $email = @$params['email'];
        }
        
        $chkToken = ProductNotification::where([
            'firebase_token' => $params['firebase_token'], 
            'product_id'     => $params['product_id'], 
            'email'          => $email
        ])
        ->first();

        if(@$chkToken) {
            return response()->json([
                'jsonrpc' => '2.0',
                'result'  => [
                    'message' => 'Token already exist'
                ]
            ]);
        }
        
        $productNotification = ProductNotification::create([
            'product_id'        => @$params['product_id'],
            'user_id'           => @$user->id,
            'email'             => @$email,
            'variant_id'        => @$variant->id,
            'firebase_token'    => @$params['firebase_token'],
            'device_type'       => @$params['device_type'],
            'device_id'         => @$params['device_id'],

        ]);

        return response()->json([
            'jsonrpc' => '2.0',
            'result'  => $productNotification
        ]);
    }
}
