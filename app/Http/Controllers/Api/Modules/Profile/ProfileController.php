<?php

namespace App\Http\Controllers\Api\Modules\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\EmailVerifyMail;
use App\User;
use App\Models\Country;
use App\Models\CountryDetails;
use App\Models\UserAddressBook;
use App\Models\UserFavorite;
use App\Models\Product;
use App\Models\City;
use App\Models\State;
use App\Models\CancelRequest;
use App\Models\CitiDetails;
use App\Models\UserReward;
use App\Models\OrderMaster;
use App\Models\OrderDetail;
use App\Models\Payment;
use App\Models\PaymentKeyTable;
use App\Mail\VerificationMail;
use App\Repositories\CityRepository;
use App\Repositories\CitiDetailsRepository;
use App\Models\OrderSeller;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\DriverRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductVariantDetailRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CountryRepository;
use App\Repositories\UserRepository;
use Illuminate\Validation\Rule;
use JWTFactory;
use JWTAuth;
use Validator;
use Mail;
use Auth;
use App\Mail\SendOrderMail;
use App\Mail\SendMerchantOrderMail;

class ProfileController extends Controller
{
    protected $order_master,$order_details;
    public function __construct( OrderMasterRepository $order_master,
        OrderDetailRepository $order_details,
        DriverRepository $drivers,
        OrderSellerRepository $orderSeller,
        ProductVariantRepository $productVariant,
        ProductRepository $product,
        ProductVariantDetailRepository $productVariantDetail,
        MerchantRepository $merchants
    )
    {
        $this->order             =   $order_master;
        $this->order_details            =   $order_details;
        $this->merchants                = $merchants;
        $this->drivers                  =   $drivers;
        $this->orderSeller              = $orderSeller;
        $this->product_variant           = $productVariant;
        $this->productVariantDetail     = $productVariantDetail;
        $this->product                  = $product;
    }


    private function calculateMerchantRate($id){
        // return $id;
        $orderdetails = $this->order_details->where('order_master_id',@$id)->get();
        foreach(@$orderdetails as $key=>$dtls){
            $obj = $this->order_details->where(['seller_id'=>@$dtls->seller_id]);
            $count = $obj->where('rate','!=',0)->count();
            $total_rate = $this->order_details->where(['seller_id'=>@$dtls->seller_id])->sum('rate');
            $update['total_no_review'] = $count;
            $update['total_review_rate'] = $total_rate;
            if(@$count != 0){
                $update['rate'] = $total_rate/$count;    
            }
            
            $this->merchants->where(['id'=>@$dtls->seller_id])->update($update);
        }
    }
    private function calculateDriverReview($id){
        $orderdetails = $this->order_details->where('order_master_id',@$id)->get();
        foreach(@$orderdetails as $key=>$dtls){
            $obj = $this->order_details->where(['driver_id'=>@$dtls->driver_id]);
            $count = $obj->where('rate','!=',0)->count();
            $total_rate = $this->order_details->where(['driver_id'=>@$dtls->driver_id])->sum('rate');
            $update['total_no_reviews'] = $count;
            $update['total_review'] = $total_rate;
            if(@$count != 0){
                $update['avg_review'] = $total_rate/$count;
            }
            $this->drivers->where(['id'=>@$dtls->driver_id])->update($update);
        }
    }
    private function calculateProductRate($id){
        $orderdetails = $this->order_details->where('order_master_id',@$id)->get();
        foreach(@$orderdetails as $key=>$dtls){
            // return 1;
            // $this->merchants->where(['id'=>@$dtls->seller_id])->update($update);
            if($dtls->product_id != 0){
                    // dd("gjdnf");
                $product = $this->product->where('id',@$dtls->product_id)->first();    
                $obje = $this->order_details->where(['product_id'=>$dtls->product_id]);
                $count = $obje->where('rate','!=',0)->count();
                $total_rate = $this->order_details->where(['product_id'=>$dtls->product_id])->sum('rate');
                $updater['total_no_reviews'] = $count;
                $updater['total_review'] = $total_rate;
                if($count != 0){
                    $updater['avg_review'] = $total_rate/$count;    
                }

                    // dd($stock_update['stock']);            
                $this->product->where('id',@$dtls->product_id)->update($updater);             
            }
            
        }   
    }
    /*
    *Method:submitReviewRatings
    *Description: submitReviewRatings
    *Author: Jayatri mullick
    */
     /**
    * @OA\Post(
    * path="/api/post-review",
    * summary="Customer Post Review",
    * description="Customer Post Review",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Post Review"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer Post Review",
    *    @OA\JsonContent(
    *       required={"order_master_id","product_id","review_point","comment"},
    *           @OA\Property(property="params", type="object",
    *           @OA\Property(property="order_master_id", type="string", format="order_master_id", example="0"),
    *           @OA\Property(property="product_id", type="string", format="product_id", example="1,2,3"),
    *           @OA\Property(property="review_point", type="string", format="review_point", example="4,5,2"),
    *           @OA\Property(property="comment", type="string", format="comment", example="nice product,good product,bad product"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function submitReviewRatings(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $user = auth()->user();
            if(@$user->status == 'I'){
                $response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
            }
            if(@$user->status == 'D'){
                $response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
            }
            $reqdata = $request->json()->all();
            $order_master_id = @$reqdata['params']['order_master_id']; 
            $product_id = explode(",",@$reqdata['params']['product_id']); 
            // $product_variant_id = explode(",",@$reqdata['params']['product_variant_id']); 
            $review_point = explode(",",@$reqdata['params']['review_point']); 
            $comment = explode(",",@$reqdata['params']['comment']); 
            $orderdetails = $this->order_details->where('order_master_id',@$order_master_id)->get();
            $masterOrderTab = $this->order->where('id',@$order_master_id)->first();
            $total_rate = 0;
            
            foreach(@$product_id as $key=>$dtls){

                $update['rate'] = @$review_point[$key]; 
                $update['comment'] = @$comment[$key];
                $update['customer_name'] = @$masterOrderTab->shipping_fname." ".@$masterOrderTab->shipping_lname;
                $update['customer_photo'] = @$user->image;
                $update['review_date'] = now();
                if(@$product_id[$key] != ""){
                    $this->order_details->where(['order_master_id'=>@$order_master_id,'product_id'=>$product_id[$key]])->update($update);
                }                
            }
            $upm['is_review_done'] = 'Y';
            $this->order->where('id',@$order_master_id)->update($upm);
            $this->calculateMerchantRate($order_master_id);
            $this->calculateProductRate($order_master_id);
            // $this->calculateDriverReview($order_master_id);
            // return redirect()->back();    
            $response['result'] = __('success.-4084');
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    *@ Method: userDetails
    *@ Description: User details
    *@ Author: Surajit
    */
    /**
    * @OA\Post(
    * path="/api/user-details",
    * summary="User Details",
    * description="User Details",
    * operationId="authLogin",
    * tags={"Alaaddin: User Details"},
    * security={{"bearer_token":{}}},
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function userDetails(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            // $user = JWTAuth::toUser();
            $user = auth()->user();
            $country = Country::with('countryDetailsBylanguage')->get();
            $userData = User::whereId(@$user->id)->with(['city_details','state_details','userCountryDetails'])->first();
            $response['result']['user'] = $userData;
            $response['result']['country'] = $country;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }

    /**
    *@ Method: updateUser
    *@ Description: Update user details
    *@ Author: Surajit
    */
    /**
    * @OA\Post(
    * path="/api/user-update",
    * summary="Customer Update",
    * description="Customer update",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer update"},
    * security={{"bearer_token":{}}},
    *     @OA\RequestBody(
    *          @OA\MediaType(
    *              mediaType="multipart/form-data",
    *              @OA\Schema(
    *                  @OA\Property(
    *                      property="image",
    *                      type="file",
    *                      description="Upload Image"
    *                  )
    *              )
    *          )
    *       ),
    *       @OA\Parameter(
    *           name="fname",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="lname",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="phone",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="integer"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="email",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="gender",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string",
    *               enum={"Male","Female","Other"}
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="date_of_birth",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="country",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="city",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="state",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="zipcode",
    *           in="query",
    *           required=false,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function updateUser(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            // $user = JWTAuth::toUser();
            $user = auth()->user();
            $validator = Validator::make($request->all(),[
                'fname'        => 'required|string|max:255',
                'lname'        => 'required|string|max:255',
                'email'         => [
                    'nullable',
                    'email',
                    'regex:/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/',
                    'max:255',
                    Rule::unique('users')->where(function($query) use ($user)  {
                        $query->where('status','!=','D')->where('id','!=',@$user->id);
                    })
                ],
                'phone'  =>  [
                        'required',
                        'numeric',
                        Rule::unique('users')->where(function($query) use ($user)  {
                            $query->where('status','!=','D')->where('id','!=',@$user->id);
                        })
                ],
                // 'address'      => 'required',
                'country'      => 'required|numeric',
                'gender'      => 'required|in:Male,Female,Other',
                'date_of_birth'      => 'required',
                'city'         => 'required|numeric',
                'state'        => 'required|numeric',
                'zipcode'      => 'required|numeric|min:6'
            ],[
                'fname.required'=>'First name required',
                'gender.required'=>'Please select your gender',
                'gender.in'=>'Please select your gender',
                'date_of_birth.required'=>'Please provide your date of birth',
                'fname.string'=>'First name should be string',
                'fname.max'=>'First name should be 255 charecters',
                'lname.required'=>'Last name required',
                'lname.max'=>'Last name maximum 255 charecters',
                'lname.string'=>'Last name should be string',
                'email.required'=>'Please provide valid email id',
                'email.email'=>'Please provide valid email-id',
                'email.regex'=>'Please provide valid email-id',
                'phone.required'=>'Phone number is required',
                'phone.numeric'=>'Phone number should be number',
                'phone.digits_between'=>'Phone number should be 10 digits to maximum 12 digits',
                'country.required'=>'Please select country',
                'country.numeric'=>'Please select country id',
                'city.required'=>'Please select city',
                'city.numeric'=>'Please select city id',
                'state.numeric'=>'Please select state id',
                'state.required'=>'Please select state'
            ]);
            
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $iscountryV = Country::where('id',@$request->country)->first();
            if(!@$iscountryV){
                $response['error'] =__('errors.-5001');
                $response['error']['meaning'] = "You have provided wrong country id: ".@$request->country;
                return response()->json($response);    
            }
            $isstatev = State::where('id',@$request->state)->first();
            if(!@$isstatev){
                $response['error'] =__('errors.-5001');
                $response['error']['meaning'] = "You have provided wrong state id: ".@$request->state;
                return response()->json($response);    
            }
            $iscity = City::where(['id'=>@$request->city])->first();
            if(!@$iscity){
                $response['error'] =__('errors.-5001');
                $response['error']['meaning'] = "You have provided wrong city id: ".@$request->city." or wrong combination of state city id!";
                return response()->json($response);    
            }
            $ins = [];
            $ins['fname']     =   $request->fname;
            $ins['lname']     =   $request->lname;
            $ins['phone']     =   $request->phone;
            // $ins['address']   =   $request->address;
            $ins['country']   =   $request->country;
            $ins['city']      =   $request->city;
            $ins['state']     =   $request->state;
            $ins['zipcode']   =   $request->zipcode;
            if(@$request->gender == 'Male'){
                $ins['gender']    =   "M";    
            }
            if(@$request->gender == 'Female'){
                $ins['gender']    =   "F";    
            }
            if(@$request->gender == 'Other'){
                $ins['gender']    =   "O";    
            }
            
            $ins['date_of_birth'] = @$request->date_of_birth;
            if($request->image) {
                @unlink(storage_path('app/public/customer/profile_pics/' . $user->image));
                $image    = $request->image; 
                $imgName   = rand().".".$image->getClientOriginalExtension();
                $image->move('storage/app/public/customer/profile_pics', $imgName);
                $ins['image']     =   $imgName;
            }
            User::where('id', $user->id)->update($ins);

            if(!empty(@$request->email) && @$request->email != $user->email){
                $otp = mt_rand(100000,999999);
                $userUp = [];
                $userUp['tmp_email']       =   $request->email;
                $userUp['email_vcode']     =   $otp;
                User::whereId($user->id)->update($userUp);

                $mailDetails = new \StdClass();
                $mailDetails->to = $request->email;
                $mailDetails->fname = $request->fname;
                $mailDetails->vcode = $otp;
                $mailDetails->id = @$user->id;
                $mailDetails->mailBody = 'forget_password';

                Mail::send(new EmailVerifyMail($mailDetails));
                $response['otp'] = $otp;
                $email_updated = 'Y';
            }
            else{
                $email_updated = 'N';
            }
            
            // Change user password.
            // if(@$request->new_password) {
                // $resultPass = $this->changePassword($request);
            // } else {
                // $resultPass = __('success_user.-700');
            // }

            if(@$resultPass) {
                $response['error'] = $resultPass;                
            } else {
                $response['result'] = ERRORS['-33017'];
                if(@$email_updated) {
                    $response['result']['email_updated'] = $email_updated;
                }
            }
            return response()->json($response);

        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/user-change-password",
    * summary="Customer Change Password",
    * description="Customer Change Password",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Change Password"},
    * security={{"bearer_token":{}}},
    *       @OA\Parameter(
    *           name="password",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="new_password",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    *       @OA\Parameter(
    *           name="password_confirmation",
    *           in="query",
    *           required=true,
    *           @OA\Schema(
    *               type="string"
    *           )
    *       ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function userChangePassword(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $reqdata = $request->all();
            $user = auth()->user();
            // return $request->all();
            $validator = Validator::make($reqdata,[
                'password'      => 'required',
                'new_password' => 'required|min:8|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation' => 'required|min:8'
            ]);
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            // if(@$request->new_password) {
            //     $resultPass = $this->changePassword($request);
            // } else {
            //     // $resultPass = __('success_user.-700');
            // }
            if(@$resultPass) {
                $response['error'] = $resultPass;                
            } else {
                $response['result'] = ERRORS['-33091'];
            }
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * Method: changePassword
    * Description: This method is used to change user password
    * Author: Surojit/jayatri 
    */
    private function changePassword($request) {
        $user = auth()->user();
        // dd($request->all());
        if($request->new_password) {
            if($user->password == '') {
                $user_detail = $user;
                $user_detail->password=\Hash::make($request->password_confirmation);
                $user_detail->save();
                // return $passwordSucc = __('success_user.-700');
            } else {
                if(\Hash::check($request->password, $user->password)){
                    $user_detail = $user;
                    $user_detail->password=\Hash::make($request->password_confirmation);
                    $user_detail->save();
                    // return $passwordSucc = __('success_user.-700');
                } else {
                    return $passwordErr = __('success_user.-701');
                }
            }
        }
    }

    /**
    *@ Method: userEmailVerify
    *@ Description: For user email verification
    *@ Author: Surajit
    */
    /**
    * @OA\Post(
    * path="/api/user-emailverify",
    * summary="Customer Update Email Verify",
    * description="Verify by merchant email",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Update Email Verify"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer credentials",
    *    @OA\JsonContent(
    *       required={"email","vcode"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *           @OA\Property(property="vcode", type="integer", format="vcode", example="111111"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function userEmailVerify(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqdata = $request->json()->all();
            // print_r($reqdata); exit();
            $validator = Validator::make($reqdata['params'], [
                'email' => 'required|email',
                'vcode' => 'required|max:6'
            ]);
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $userData = auth()->user();
            if(@$userData->tmp_email == $reqdata['params']['email'] && @$userData->email_vcode == $reqdata['params']['vcode']){
                User::whereId($userData->id)
                ->update(['email' => $reqdata['params']['email'], 'email_vcode' => NULL, 'tmp_email' => NULL]);

                $response['result'] =  __('success_user.-700');
                return response()->json($response);
            }else{
                $response['error'] = ERRORS['-33039'];
                return response()->json($response);
            }
        } catch (\Exception $e) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return Response::json($response);
        }
    }
    /**
    * @OA\Get(
    * path="/api/user-dashboard",
    * summary="Customer Dashboard",
    * description="Customer Dashboard",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Dashboard"},
    * security={{"bearer_token":{}}},
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function userDashboard(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $user = auth()->user();
            // $user = JWTAuth::toUser();
            $userDefAddr    = UserAddressBook::where(['user_id' => $user->id, 'is_default' => 'Y'])->with('getCountry','city_details','state_details')->first();
            $order =  $data['order'] = OrderMaster::with('customerDetails','orderMasterDetails.getOrderDetailsUnitMaster','shipping_city_details','billing_city_details','shipping_state_details','billing_state_details','orderMasterDetails.productDetails.productByLanguage','orderMasterDetails.productDetails.defaultImage','orderMasterDetails.productDetails.productVariants','orderMasterDetails.productVariantDetails','orderMasterDetails.productDetails','orderMasterDetails.productDetails.productMarchant','getBillingCountry','getCountry')->whereNotIn('status',['I','F','D'])->where(['user_id'=>$user->id])->orderBy('id','desc')->first();
            // if(@$order){
                $response['result']['order'] = @$order;
            // }else{
            //     $response['result']['order'] = "";
            // }
            
            $response['result']['total_orders'] = OrderMaster::with('customerDetails','orderMasterDetails.getOrderDetailsUnitMaster','orderMasterDetails.productDetails.productByLanguage','orderMasterDetails.productDetails.defaultImage','orderMasterDetails.productDetails.productVariants','orderMasterDetails.productVariantDetails','orderMasterDetails.productDetails','orderMasterDetails.productDetails.productMarchant')->where(['user_id'=>$user->id])->whereNotIn('status',['I','F','D'])->orderBy('id','desc')->count();
            
                $response['result']['recent_orders'] = OrderMaster::with('customerDetails','orderMasterDetails.getOrderDetailsUnitMaster','orderMasterDetails.productDetails.productByLanguage','orderMasterDetails.productDetails.defaultImage','orderMasterDetails.productDetails.productVariants','orderMasterDetails.productVariantDetails','orderMasterDetails.productDetails','orderMasterDetails.productDetails.productMarchant')->whereNotIn('status',['I','F','D'])->where(['user_id'=>$user->id,'id'=>@$order->id])->orderBy('id','desc')->count();
            
            
            $response['result']['wishlist'] = UserFavorite::where(['user_id'=>$user->id])->count();
            $response['result']['user'] = $user;
            // $response['result']['user'] = User::where(['id'=>$user->id])->select('id','fname','lname','phone','email','email1','gender','date_of_birth',)->first();
            $response['result']['userDefAddr'] = $userDefAddr;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Get(
    * path="/api/edit-profile",
    * summary="Customer Edit Profile",
    * description="Customer Edit Profile",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Edit Profile"},
    * security={{"bearer_token":{}}},
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function editProfile(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $user = auth()->user();
            $country = Country::with('countryDetailsBylanguage')->where('status','!=','D')->get();

            $customer = User::where('id',$user->id)->with(['city_details','state_details'])->select('id','fname','lname','phone','email','gender','date_of_birth','login_from','image','tmp_email','address','landmark','city','city_id','state','country','facebook_id','google_id','apple_id','provider','signup_from','loyalty_balance','loyalty_total','loyalty_used','email_verified_at','email_vcode','is_email_verified','status','user_type','firebase_reg_no','device_type','remember_token','tag','created_at','updated_at')->addSelect(\DB::raw('IF(zipcode=NULl," ", zipcode) AS zipcode'))->first();
            $user = User::where('id',$user->id)->with(['city_details','state_details'])->select('id','fname','lname','phone','email','gender','date_of_birth','login_from','image','tmp_email','address','landmark','city','city_id','state','country','facebook_id','google_id','apple_id','provider','signup_from','loyalty_balance','loyalty_total','loyalty_used','email_verified_at','email_vcode','is_email_verified','status','user_type','firebase_reg_no','device_type','remember_token','tag','created_at','updated_at')->addSelect(\DB::raw('IF(zipcode=NULL," ", zipcode ) AS zipcode'))->first();


            $response['result']['user'] = $user;
            if(@$user->gender == 'M'){
                $response['result']['user']['gender'] = "Male";
            }
            if(@$user->gender == 'F'){
                $response['result']['user']['gender'] = "Female";
            }
            if(@$user->gender == 'O'){
                $response['result']['user']['gender'] = "Other";
            }
            
            $response['result']['country'] = $country;

            $response['result']['customer'] = $customer;
            if(@$customer->gender == 'M'){
                $response['result']['customer']['gender'] = "Male";
            }
            if(@$customer->gender == 'F'){
                $response['result']['customer']['gender'] = "Female";
            }
            if(@$customer->gender == 'O'){
                $response['result']['customer']['gender'] = "Other";
            }
            
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/order-history",
    * summary="Customer Order History",
    * description="Customer Order History",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Order History"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer Order History",
    *    @OA\JsonContent(
    *       required={"page"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="page", type="string", format="name", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="please try again",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="please try again")
    *        )
    *     )
    * )
    */
  
    public function orderHistory(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try {
            \DB::connection()->enableQueryLog();
            $user = auth()->user();
            $userId = $user->id;
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'],[
                'page'    => 'required|numeric'
            ],
            [
                'page.required'=>'Page required',
                'page.number'=>'Page required'
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $page = @$reqdata['params']['page']?$reqdata['params']['page']:1;
            $limit = 5;
            if($page == 1) {
                $start = 0;
            } else {
                $start = (($page -1) * $limit);
            }

            
            
            
            $ordersdetails = OrderDetail::with(['showCancelRequest','showOrderVehicleInformation','productDetails.productUnitMasters','productCategoriesDetails','sellerDetails','productByLanguage','defaultImage','orderMaster']);
            $ordersdetails = $ordersdetails->whereHas('orderMaster',function($q1)  use($userId){
                $q1 = $q1->where('user_id',$userId);
            });
            $ordersdetails = $ordersdetails->whereHas('orderMaster',function($q1)  {
                $q1 = $q1->whereNotIn('status',['D','I','','F']);
            })->groupBy('order_master_id');

            $response['result']['orders_total'] =$ordersdetails->count();
            $response['result']['orders_page_count'] =ceil($ordersdetails->count() /$limit);
            $response['result']['orders_per_page'] =$limit;

            
            $ordersdetails = $ordersdetails->orderBy('order_master_id', 'desc')->skip($start)->take($limit)->get();

            $response['result']['ordersdetails'] = $ordersdetails;
            
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }

    // old without pagination
    public function orderHistoryOldWithoutPagination(){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $user = auth()->user();
            $userId = $user->id;
            $orders = $this->order->with(['orderMasterDetails.getOrderDetailsUnitMaster','orderMasterDetails.productDetails.productUnitMasters','orderMasterExtDetails.productDetails.defaultImage','orderMasterExtDetails.productDetails.productByLanguage','orderMasterExtDetails.sellerDetails']);
            $orders = $orders->where(function($q1)  use($userId){
                $q1 = $q1->where('user_id',$userId);
            });
            $orders = $orders->where(function($q1)  {
                $q1 = $q1->whereNotIn('status',['D','I','','F']);
            });
            $orders = $orders->orderBy('id', 'desc')->get();
            $ordersdetails = OrderDetail::with(['productDetails.productUnitMasters','productCategoriesDetails','sellerDetails','productByLanguage','defaultImage','orderMaster'])->groupBy('order_master_id');
            $ordersdetails = $ordersdetails->whereHas('orderMaster',function($q1)  use($userId){
                $q1 = $q1->where('user_id',$userId);
            });
            $ordersdetails = $ordersdetails->whereHas('orderMaster',function($q1)  {
                $q1 = $q1->whereNotIn('status',['D','I','','F']);
            });
            $ordersdetails = $ordersdetails->orderBy('id', 'desc')->get();

            $response['result']['orders'] = $orders;
            $response['result']['ordersdetails'] = $ordersdetails;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Get(
    * path="/api/order-history-details/{id}",
    * summary="Customer Order History Details",
    * description="Customer Order History Details",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Order History Details"},
    * security={{"bearer_token":{}}},
    *      @OA\Parameter(
    *         name="id",
    *         in="path",
    *         description="order id",
    *         required=true,
    *      ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function orderHistoryDetails($orderId){
        $response = ['jsonrpc'=>'2.0'];
        try {
            $user = auth()->user();

            // return $orderId;
            //user id
            // $order = $this->order->with(['orderMasterDetails','orderMasterDetails','orderMasterDetails.productDetails.productByLanguage:id,language_id,product_id,title,description','orderMasterDetails.productDetails.defaultImage:id,image,product_id,is_default','orderMasterDetails.productDetails.images','orderMasterDetails.productDetails.productVariants','orderMasterDetails.productVariantDetails','orderMasterDetails.sellerDetails','orderMasterDetails.driverDetails'])->where('order_no',$order_no)->first();

            $order = $this->order->with(['orderMasterDetails.showCancelRequest','orderMasterDetails.getOrderDetailsUnitMaster',
                'orderMasterDetails.productDetails.productMarchant',
                'orderMasterDetails.productDetails.defaultImage',
                'orderMasterDetails.productDetails.productByLanguage','orderMasterDetails.getOrderDetailsUnitMaster','getCountry','shipping_city_details','billing_city_details','shipping_state_details','billing_state_details','getCityNameByLanguage:id,city_id,language_id,name','getBillingCountry:id,country_id,language_id,name','getBillingCityNameByLanguage:id,city_id,language_id,name'])->where('id',$orderId)->first();
            $no_of_items = $this->order_details->where(['order_master_id'=>$orderId])->count();
            $order_sellers = OrderSeller::with(['orderDetails.showCancelRequest','orderDetails.showOrderVehicleInformation','orderDetails.getOrderDetailsUnitMaster','orderDetails.productDetails.productUnitMasters','orderMerchant','orderMerchant.merchantCityDetails','orderDetails','orderDetails.productDetails','orderDetails.productDetails.productByLanguage:id,product_id,language_id,title,description','orderDetails.productDetails.defaultImage:id,image,product_id,is_default','orderDetails.productDetails.productVariants','orderDetails.productVariantDetails'])->where(['order_master_id'=>$orderId]);
            $order_sellers = $order_sellers->whereHas('orderDetails', function($q){
                $q->whereNotIn('status', ['','D','I']);
            });
            $order_sellers = $order_sellers->get();
            $response['result']['seller'] = $order_sellers;
            $response['result']['no_of_items'] = $no_of_items;
            $response['result']['order'] = $order;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    public function rewardPoints(){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user();
            $user_info = User::where('id',$user->id)->select('id','loyalty_balance','loyalty_total','loyalty_used')->first();

            $rewards    = $this->order->where(['user_id'=>$user->id])->select('order_no','delivery_date','delivery_time','payment_method','status','is_paid','order_type','subtotal','shipping_price','order_total','loyalty_point_received','loyalty_point_used','loyalty_amount','created_at','updated_at');
            $rewards = $rewards->where(function($query)  {
                $query->where('loyalty_point_used', '>', 0)
                ->orWhere('loyalty_point_received', '>', 0);
            })
            ->where('status', '!=', 'OC')
            ->orderBy('id','desc')
            ->get();
            // $rewards = UserReward::with('orderMaster:id,user_id,order_no,created_at,updated_at','user:id,loyalty_balance,loyalty_total,loyalty_used')->where('user_id',$user->id)->get();
            // $rewards = UserReward::where('user_id',$user->id)
            /*
            where(function($query) {
                $query->orWhere('loyalty_point_used', '>', 0);
                $query->orWhere('loyalty_point_received', '>', 0);
            })
            */
            $response['result']['rewards'] = $rewards;
            $response['result']['user_info'] = $user_info;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }    
    }
    /**
    * @OA\Get(
    * path="/api/payment-history",
    * summary="Customer Payment History",
    * description="Customer Payment History",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Payment History"},
    * security={{"bearer_token":{}}},
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function paymentHistory(){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user();
            $payments = Payment::with('orderMasterTab:id,order_no,created_at,updated_at,user_id,payment_method')->where(['user_id'=>$user->id,'payment_mode'=>'O'])->orderBy('id','desc')->get();

            $response['result']['payments'] = $payments;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    public function addFavorite($id){
        // $response = ['jsonrpc'=>'2.0'];

        $status = '';
        $response = [
            'jsonrpc'   => '2.0'
        ];        
        $user = auth()->user();
        $userid = $user->id;
        
        $data = UserFavorite::where('product_id',$id)
        ->where('user_id',$userid)
        ->first();       

        if(@$data)
        {
            UserFavorite::where([
                'product_id'    => $id,
                'user_id'       => $userid
            ])
            ->delete();
            $status = 'Remove';
        } else {
            $favourites = new UserFavorite;
            $favourites->user_id     = $userid;
            $favourites->product_id  = $id;
            $favourites->save();

            $status = 'Add';
        }

        if($status == '') {
            // $response['error']['message'] = 'Fail';
            $response['error'] = __('errors.-5075');
        } else {
            // $response['sucess']['result'] = $status;
            if($status == 'Add') {
                // $response['result'] = $status;
                $response['result']['success'] = __('success.-4079');
            }
            if($status == 'Remove') {
                // $response['result'] = $status;
                $response['result']['success'] = __('success.-4080');
                // $response['result']['success'] = __('errors.-5076');
            }
        }
        return response()->json($response);
    }
    /**
    * @OA\Post(
    * path="/api/my-wish-list",
    * summary="Customer My Favorite",
    * description="Customer My Favorite",
    * operationId="wishList",
    * tags={"Alaaddin: Customer My Favorite"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer Order History",
    *    @OA\JsonContent(
    *       required={"page"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="page", type="string", format="name", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function wishList(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try{
            $reqdata = $request->json()->all();
            $user = auth()->user();
            // $fav = UserFavorite::where('user_id', $user->id)->get();
            // foreach ($fav as $val) {
            //     $favVal[] = $val->product_id;
            // }
            $favProduct = UserFavorite::where('user_id', $user->id)
            ->with(
                'gettProduct.productUnitMasters',
                'gettProduct.defaultImage:product_id,image',
                'gettProduct.productByLanguage',
                // 'gettProduct.productSubCategory',
                // 'gettProduct.productParentCategory',
                'gettProduct.getProductCategory.Category',
                'gettProduct.productCategory',
                'gettProduct.productMarchant'
            );
            $favProduct = $favProduct->whereHas('gettProduct', function($q){
                $q->where(['status'=>'A','seller_status'=>'A']);
            });

            $validator = Validator::make($reqdata['params'],[
                'page'    => 'required|numeric'
            ],
            [
                'page.required'=>'Page required',
                'page.number'=>'Page required'
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $page = $reqdata['params']['page']; 

            $limit = 5;
            if($page == 1) {
                $start = 0;
            } else {
                $start = (($page -1) * $limit);
            }

            $response['result']['favProduct_total'] =$favProduct->count();
            $response['result']['favProduct_page_count'] =ceil($favProduct->count() /$limit);
            $response['result']['favProduct_per_page'] =$limit;

            $favProduct = $favProduct->skip($start)->take($limit)->orderBy('id', 'desc')->get();
            $response['result']['favProduct'] = $favProduct;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    public function wishListOld(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try{
            $user = auth()->user();
            $fav = UserFavorite::where('user_id', $user->id)->get();
            foreach ($fav as $val) {
                $favVal[] = $val->product_id;
            }
            $favProduct = UserFavorite::where('user_id', $user->id)
            ->with(
                'gettProduct.productUnitMasters',
                'gettProduct.defaultImage:product_id,image',
                'gettProduct.productByLanguage',
                // 'gettProduct.productSubCategory',
                // 'gettProduct.productParentCategory',
                'gettProduct.getProductCategory.Category',
                'gettProduct.productCategory',
                'gettProduct.productMarchant'
            );
            $favProduct = $favProduct->whereHas('gettProduct', function($q){
                $q->where(['status'=>'A','seller_status'=>'A']);
            });
            $favProduct = $favProduct->get();
            $response['result']['favProduct'] = $favProduct;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/remove-my-wish-list/{id}",
    * summary="Customer Remove Product In Favorite",
    * description="Customer Remove Product In Favorite",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Remove Product In Favorite"},
    * security={{"bearer_token":{}}},
    *      @OA\Parameter(
    *         name="id",
    *         in="path",
    *         description="product id",
    *         required=true,
    *      ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function removeWishlist($id,Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user();
        //dd($user);
            $status = '';
            $response = [
                'jsonrpc'   => '2.0'
            ];        

            $data = UserFavorite::where('product_id',$id)->where('user_id', $user->id)->first();    
            if(@$data)
            {
                UserFavorite::whereId($data->id)->delete();
                $status = 'Remove';
            }

            if($status == '') {
            // $response['error']['message'] = 'Fail';
                $response['error'] = __('errors.-5073');
                $response['result']['status'] = 'Fail';
            } else {
                $response['result']['status'] = $status;
                if($status == 'Remove') {
                    $response['result']['success'] = __('front_static.wishlist_success');
                }
            }
        // return $response;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }

    public function removeWishlist1($id,Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user();
            $status = '';
            $response = [
                'jsonrpc'   => '2.0'
            ];        

            $data = UserFavorite::whereId($id)->where('user_id', $user->id)->first();    
            if(@$data)
            {
                UserFavorite::whereId($id)->delete();
                $status = 'Remove';
            }

            if($status == '') {
            // $response['error']['message'] = 'Fail';
                $response['error'] = __('errors.-5073');
                $response['result']['status'] = 'Fail';
            } else {
                $response['result']['status'] = $status;
                if($status == 'Remove') {
                    $response['result']['success'] = __('front_static.wishlist_success');
                }
            }
        // return $response;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/user-address-book",
    * summary="Customer Address Book",
    * description="Customer Address Book",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Address Book"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer credentials",
    *    @OA\JsonContent(
    *       required={"type","default"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="type", type="string", format="type", example="O or S or H"),
    *           @OA\Property(property="default", type="string", format="default", example="Y or N"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function userAddressBook(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user();
            // return $user = JWTAuth::toUser();
            $user_address_book = UserAddressBook::with(['getCountry','city_details','state_details'])->where('user_id',$user->id);
            if(!empty(@$request['params']['type'])){
                $user_address_book = $user_address_book->where('type_of_address',@$request['params']['type']);
            }
            if(!empty(@$request['params']['default'])){
                $user_address_book = $user_address_book->where('is_default',@$request['params']['default']);
            }
            $user_address_book = $user_address_book->latest()->get();
            if(@$user_address_book->count() <= 0){
                if(empty(@$request['params']['type'])){
                    $user_address_book = UserAddressBook::with(['getCountry','city_details','state_details'])->where('user_id',$user->id)->orderBy('id','desc')->take(1)->get();
                }
            }
            // with(['getCountry','getCityByLanguage'])->where('user_id',$user->id)->

            $response['result']['user_address_book'] = $user_address_book;

            $user = User::with(['city_details','state_details','userCountryDetails'])->where(['id'=>$user->id])->first();

            $response['result']['user_billing_details'] = $user;
            $response['result']['countries'] = Country::get();

            
            // $countries = Country::with('countryDetailsBylanguage:id,country_id,language_id,name,status')->where('status','!=','D')->select('id','status')->get();
            // $countries = CountryDetails::where('status','!=','D');
            // $countries = $countries->where(function($q1) use($request) {
            //         $q1 = $q1->where('status', '!=','I');
            //     });
            // $countries = $countries->orderBy('name')->get();
            // $response['result']['countries'] = $countries;

            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }    
    }
    /**
    * @OA\Get(
    * path="/api/edit-address-book/{id}",
    * summary="Customer edit address book",
    * description="Customer edit address book",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer edit address book"},
    * security={{"bearer_token":{}}},
    *      @OA\Parameter(
    *         name="id",
    *         in="path",
    *         description="address id",
    *         required=true,
    *      ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function editAddressBook($id){
        $response = ['jsonrpc'=>'2.0'];
        try{
            $user = auth()->user();
            $user_address_book = UserAddressBook::with(['city_details:id,name,country_id,state_id,status','state_details','getCountry:id,country_id,language_id,name,country_code,status'])->where(['id'=>$id,'user_id'=>$user->id])->first();
            if(@$user_address_book){
                $response['result']['user_address_book'] = $user_address_book;
                return response()->json($response);
            }else{
                $response['result']['error'] =__('errors.-5001');
                return response()->json($response);
            }     
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }    
    }
    /**
    * @OA\Post(
    * path="/api/set-default-address-book",
    * summary="Customer set default address book",
    * description="Customer set default address book",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer set default address book"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer credentials",
    *    @OA\JsonContent(
    *       required={"id"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="id", type="integer", format="id", example="0"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function setDefault(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user(); 
            $reqdata = $request->json()->all();
            $id = @$reqdata['params']['id'];
            $chk = UserAddressBook::where(['id'=>$id,'user_id'=>$user->id])->first();
            if(@$chk){
                UserAddressBook::where('user_id',$user->id)->update(['is_default' => 'N']);
                UserAddressBook::where('id',$id)->update(['is_default' => 'Y']);
                $response['result']['success'] = __('success_user.-705');    
            }else{
                $response['result']['error'] = __('errors.-5074');    
            }
        // $response['result']['success'] = __('success_user.-704');
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }   

    }
    /**
    * @OA\Post(
    * path="/api/delete-address-book",
    * summary="Customer delete address book",
    * description="Customer delete address book",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer delete address book"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer credentials",
    *    @OA\JsonContent(
    *       required={"id"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="id", type="integer", format="id", example="0"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function deleteAddressBook(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user(); 
            $reqdata = $request->json()->all();
            $id = @$reqdata['params']['id'];
            $chk = UserAddressBook::where(['id'=>$id,'user_id'=>$user->id])->first();
            if(@$chk){
                UserAddressBook::where('id',$id)->delete();
                $response['result']['success'] = __('success.-4077');    
            }else{
                $response['result']['error'] = __('errors.-5074');    
            }
        // $response['result']['success'] = __('success_user.-704');
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }   

    }
    /**
    * @OA\Post(
    * path="/api/update-address-book",
    * summary="Customer update address book",
    * description="Customer update address book",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer update address book"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer credentials",
    *    @OA\JsonContent(
    *       required={"id","fname","lname","phone","email"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="id", type="integer", format="id", example="0"),
    *               @OA\Property(property="fname", type="string", format="name", example="user fname"),
    *               @OA\Property(property="lname", type="string", format="name", example="user lname"),
    *               @OA\Property(property="phone", type="integer", format="phone", example="1234567890"),
    *               @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *               @OA\Property(property="country", type="integer", format="country", example="101"),
    *               @OA\Property(property="state", type="string", format="state", example="state"),
    *               @OA\Property(property="city", type="string", format="city", example="city"),
    *               @OA\Property(property="postcode", type="string", format="postcode", example="700137"),
    *               @OA\Property(property="landmark", type="string", format="landmark", example="landmark"),
    *               @OA\Property(property="more_address", type="string", format="more_address", example="more_address"),
    *               @OA\Property(property="type_of_address", type="string", format="type_of_address", example="H"),
    *               @OA\Property(property="is_default", type="string", format="is_default", example="Y"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function updateUserAddressBook(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user();
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'], [
                'id' => 'required',
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required|email',
                'phone' => 'required|numeric',
                'country' => 'required|numeric',
                'state' => 'required',
                'city' => 'required',
                'postcode' => 'required',
                'landmark' => 'required'
            ],[
           'id.required'=>'id required',
           'fname.required'=>'First name required',
           'lname.required'=>'Last name required',
           'email.required'=>'Please provide valid email id',
           'email.email'=>'Email-id required',
           'phone.required'=>'Phone number is required',
           'phone.numeric'=>'Phone number should be number',
           'country.required'=>'Country is required',
           'country.numeric'=>'Please send country id only!',
           'state.required'=>'State is required',
           'city.required'=>'City is required',
           'postcode.required'=>'Post code is required',
           'landmark.required'=>'Landmark is required',
        ]);
            $id = @$reqdata['params']['id'];
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $userAddress = UserAddressBook::where('id',$id)->first();
            if(empty($userAddress)){
                $response['result']['error'] =__('errors.-5001');
                $response['result']['error']['meaning'] ="You have sent wrong id :".$id;
                return response()->json($response);
            }
            if(@$validator)
            {
                $userAddData = UserAddressBook::where('user_id', $user->id)->count();
                $userAdd['user_id']             =   $user->id;
                $userAdd['shipping_fname']      =   @$reqdata['params']['fname'];
                $userAdd['shipping_lname']      =   @$reqdata['params']['lname'];
                $userAdd['email']               =   @$reqdata['params']['email'];
                $userAdd['phone']               =   @$reqdata['params']['phone'];
                $userAdd['country']             =   @$reqdata['params']['country'];
                if(@$reqdata['params']['more_address']){
                    $userAdd['more_address']        =   @$reqdata['params']['more_address'];    
                }
                
                $userAdd['location']            =   @$reqdata['params']['landmark'];
                $userAdd['lat']                 =   @$reqdata['params']['lat'];
                $userAdd['lng']                 =   @$reqdata['params']['lng'];
                $userAdd['type_of_address']     =   @$reqdata['params']['type_of_address'];
                if(@$reqdata['params']['is_default']){
                    $userAdd['is_default']          =   @$reqdata['params']['is_default'];
                    if(@$reqdata['params']['is_default'] == 'Y'){

                    }else{

                    }
                }
                if(@$reqdata['params']['state']){
                    $userAdd['state']             =   @$reqdata['params']['state'];
                }
                if(@$reqdata['params']['city']){
                    $userAdd['city']                =   @$reqdata['params']['city'];
                }
                
                if(@$reqdata['params']['postcode']) {
                    $userAdd['postal_code']     =   @$reqdata['params']['postcode'];
                }
                // if(@$reqdata['params']['is_default'] || $userAddData<1) {
                //     $userAdd['is_default'] = 'Y';
                // } else {
                //     $userAdd['is_default'] = 'N';               
                // }
                if(@$reqdata['params']['is_default'] == 'Y') {
                    $userAdd['is_default'] = 'Y';
                } else {
                    $userAdd['is_default'] = 'N';               
                }
                UserAddressBook::where('id',$id)->update($userAdd);
                $userAddress = UserAddressBook::where('id',$id)->first();
                
                if(@$reqdata['params']['is_default'] == 'Y') {
                    UserAddressBook::where('user_id', $user->id)->where('id', '!=', $userAddress->id)->update(['is_default' => 'N']);
                }
                $defaultCount = UserAddressBook::where(['user_id'=> $user->id,'is_default'=>'Y'])->count();
                
                if($defaultCount <= 0){
                    UserAddressBook::where(['user_id'=>$user->id,'id'=>$userAddress->id])->update(['is_default' => 'Y']);
                }
                // session()->flash("success",);
            }
            $response['result']['success'] = __('success_user.-704');
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }    
    }
    // public function updateUser(Request $request){
    //     $response = ['jsonrpc'=>'2.0'];
    //     try {
    //         // $user = JWTAuth::toUser();
    //         $user = auth()->user();

    //         $validator = Validator::make($request->all(),[
    //             'fname'        => 'required|string|max:255',
    //             'lname'        => 'required|string|max:255',
    //             'email'        => 'required',
    //             'phone'        => 'required',
    //             'address'      => 'required',
    //             'country'      => 'required',
    //             'city'         => 'required',
    //             'state'        => 'required',
    //             'zipcode'      => 'required'
    //         ]);
    //         if($validator->fails()){
    //             $response['error'] = $validator->errors();
    //             return response()->json($response);
    //         }
    //         $ins = [];
    //         $ins['fname']     =   $request->fname;
    //         $ins['lname']     =   $request->lname;
    //         $ins['phone']     =   $request->phone;
    //         $ins['address']   =   $request->address;
    //         $ins['country']   =   $request->country;
    //         $ins['city']      =   $request->city;
    //         $ins['state']     =   $request->state;
    //         $ins['zipcode']   =   $request->zipcode;
    //         if($request->image) {
    //             $image     = $request->image; 
    //             $imgName   = time().".".$image->getClientOriginalExtension();
    //             $image->move('storage/app/public/customer/profile_pics', $imgName);
    //             $ins['image']     =   $imgName;
    //         }
    //         User::where('id', $user->id)->update($ins);

    //         if($request->email != $user->email){
    //             $otp = mt_rand(100000,999999);
    //             $userUp = [];
    //             $userUp['tmp_email']       =   $request->email;
    //             $userUp['email_vcode']     =   $otp;
    //             User::whereId($user->id)->update($userUp);

    //             $mailDetails = new \StdClass();
    //             $mailDetails->to = $request->email;
    //             $mailDetails->fname = $request->fname;
    //             $mailDetails->vcode = $otp;

    //             Mail::send(new EmailVerifyMail($mailDetails));
    //             $response['otp'] = $otp;
    //             $email_updated = 'Y';
    //         }

    //         // Change user password.
    //         if(@$request->new_password) {
    //             $resultPass = $this->changePassword($request);
    //         } else {
    //             // $resultPass = __('success_user.-700');
    //         }

    //         if(@$resultPass) {
    //             $response['error'] = $resultPass;                
    //         } else {
    //             $response['result'] = ERRORS['-33017'];
    //             if(@$email_updated) {
    //                 $response['result']['email_updated'] = $email_updated;
    //             }
    //         }
    //         return response()->json($response);

    //     } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
    //         $response['error'] = $e->getMessage();
    //         return response()->json($response);
    //     }
    //     catch (\Exception $th) {
    //         $response['error'] = $th->getMessage();
    //         return response()->json($response);
    //     }
    // }
    public function addAddressBook(){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user();
            $data['city']       = City::get();
            $country = Country::with('countryDetailsBylanguage');
            $country = $country->where(function($q1) {
                $q1 = $q1->where('status', '!=', 'I')
                ->orWhere('status','!=','D');
            });
            $data['country'] = $country->orderBy('id','desc')->get();

            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }   
    }
    /**
    * @OA\Post(
    * path="/api/add-address-book",
    * summary="Customer add address book",
    * description="Customer add address book",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer add address book"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer credentials",
    *    @OA\JsonContent(
    *       required={"fname","lname","phone","email"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="fname", type="string", format="name", example="user fname"),
    *               @OA\Property(property="lname", type="string", format="name", example="user lname"),
    *               @OA\Property(property="phone", type="integer", format="phone", example="1234567890"),
    *               @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *               @OA\Property(property="country", type="integer", format="country", example="101"),
    *               @OA\Property(property="state", type="string", format="state", example="state"),
    *               @OA\Property(property="city", type="string", format="city", example="city"),
    *               @OA\Property(property="postcode", type="string", format="postcode", example="700137"),
    *               @OA\Property(property="landmark", type="string", format="landmark", example="landmark"),
    *               @OA\Property(property="more_address", type="string", format="more_address", example="more_address"),
    *               @OA\Property(property="type_of_address", type="string", format="type_of_address", example="H"),
    *               @OA\Property(property="is_default", type="string", format="is_default", example="Y"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function storeAddressBook(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user();
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'], [
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required|email',
                'phone' => 'required|numeric',
                'country' => 'required|numeric',
                'state' => 'required',
                'city' => 'required',
                'postcode' => 'required',
                'landmark' => 'required',
                'type_of_address' => 'required'
            ]);
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            if(@$validator)
            {
                if(@$reqdata['params']['city_id']) {
                    $city = City::where('status', '!=', 'D');
                    $city = $city->whereHas(function($query) use($reqdata) {
                        $query->where('name', 'LIKE', "%{$reqdata['params']['city_id']}%");
                    })->get();

                    // $city = City::where('name', $request->city_id)->first();
                    if(!$city) {
                        $response['error'] = __('errors.-5001');
                        // session()->flash("error",__('success_user.-722'));
                        // return redirect()->back();      
                    }
                }
                $userAddData = UserAddressBook::where('user_id', $user->id)->count();
                $userAdd['user_id']             =   $user->id;
                $userAdd['shipping_fname']      =   @$reqdata['params']['fname'];
                $userAdd['shipping_lname']      =   @$reqdata['params']['lname'];
                $userAdd['email']               =   @$reqdata['params']['email'];
                $userAdd['phone']               =   @$reqdata['params']['phone'];
                $userAdd['country']             =   @$reqdata['params']['country'];
                if(@$reqdata['params']['more_address']){
                    $userAdd['more_address']        =   @$reqdata['params']['more_address'];
                }
                $userAdd['location']            =   @$reqdata['params']['landmark'];
                $userAdd['lat']                 =   @$reqdata['params']['lat'];
                $userAdd['lng']                 =   @$reqdata['params']['lng'];
                $userAdd['type_of_address']     =   @$reqdata['params']['type_of_address'];
                if(@$reqdata['params']['city']) {
                    $userAdd['city']            =   @$reqdata['params']['city'];
                } 
                if(@$reqdata['params']['state']) {
                    $userAdd['state']         =   @$reqdata['params']['state'];
                }
                
                if(@@$reqdata['params']['postcode']) {
                    $userAdd['postal_code']     =   $reqdata['params']['postcode'];
                }
                // if(@$reqdata['params']['is_default'] || $userAddData<1) {
                //     $userAdd['is_default'] = 'Y';
                // } else {
                //     $userAdd['is_default'] = 'N';               
                // }
                if(@$reqdata['params']['is_default'] == 'Y') {
                    $userAdd['is_default'] = 'Y';
                } else {
                    $userAdd['is_default'] = 'N';               
                }
                $userAddress = UserAddressBook::create($userAdd);
                
                // if(@$reqdata['params']['is_default']) {
                //     UserAddressBook::where('user_id', $user->id)->where('id', '!=', $userAddress->id)->update(['is_default' => 'N']);
                // }
                if(@$reqdata['params']['is_default'] == 'Y') {
                    UserAddressBook::where('user_id', $user->id)->where('id', '!=', $userAddress->id)->update(['is_default' => 'N']);
                }
                $defaultCount = UserAddressBook::where(['user_id'=> $user->id,'is_default'=>'Y'])->count();
                
                if($defaultCount <= 0){
                    UserAddressBook::where(['user_id'=>$user->id,'id'=>$userAddress->id])->update(['is_default' => 'Y']);
                }

                // session()->flash("success",__('success_user.-702'));
                $response['result']['success'] = __('success_user.-702');

            }
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }   
    }
    /**
    * @OA\Get(
    * path="/api/get-all-country",
    * summary="Get Country",
    * description="Get Country",
    * operationId="authLogin",
    * tags={"Alaaddin: Get Country"},
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function getCountry(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            // $user = auth()->user();
            $countries = Country::with('countryDetailsBylanguage:id,country_id,language_id,name,status')->where('status','!=','D')->select('id','status')->get();
            if(@$countries){
               $response['result']['countries'] = $countries;
           }else{
               $response['result']['countries'] = "No result found ";
           }

           return response()->json($response);
       }
       catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
        $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
        return response()->json($response);
    }
    catch (\Exception $th) {
        $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
        return response()->json($response);
    }    
}
    /**
    * @OA\post(
    * path="/api/get-all-city",
    * summary="Get city",
    * description="Get city",
    * operationId="authLogin",
    * tags={"Alaaddin: Get city"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Get city",
    *    @OA\JsonContent(
    *       required={"city_name","state_id"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="city_name", type="string", format="name", example=""),
    *               @OA\Property(property="state_id", type="integer", format="state_id", example=""),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function getAllCity(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            // $user = auth()->user();
            $reqData = $request->json()->all();
            
            
            $cities = City::where('status', '!=', 'D');
            $cities = $cities->where(function($q1) use($request) {
                $q1 = $q1->whereNotIn('status',['','D','I']);
            });
            if(@$reqData['params']['city_name']){
                $cityName = $reqData['params']['city_name'];  
                $cities = $cities->where(function($query) use($cityName) {
                    $query->where('name', 'LIKE', "%{$cityName}%");
                });  
            }
            if(@$reqData['params']['state_id']){
                $cities = $cities->where('state_id',@$reqData['params']['state_id']);
            }
            // $cities = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
            
            $cities = $cities->orderBy('name')->get();            
            $response['result']['cities'] = $cities;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }    
    }
    /*
    *   Method  : addToFev
    *   Use     : addToFev
    *   Author  : Jayatri
    *
    */ 
    /**
    * @OA\Post(
    * path="/api/add-fav/{id}",
    * summary="Customer Add Product In Favorite",
    * description="Customer Add Product In Favorite",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Add Product In Favorite"},
    * security={{"bearer_token":{}}},
    *      @OA\Parameter(
    *         name="id",
    *         in="path",
    *         description="product id",
    *         required=true,
    *      ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function addToFev($id) {
        $response = [
            'jsonrpc' => '2.0'
        ];
        $users       = auth()->user();
        try{
            $user = $users->id;
            // $reqData    = $request->json()->all();
            $product_id = $id;
            if(@$reqData['params']['id']){
                $product_id = $reqData['params']['product_id'];    
            }
            if(!@$users) {
                $response['error']['message'] = __('errors.-5077');
                return response()->json($response);
            }
            #checking if already exist or not.
            $userFav = UserFavorite::where(['product_id' => $id, 'user_id' => @$user])->first();
            
            #if already not added to wishlist the insert to wishlist table
            if(!@$userFav) {
                UserFavorite::create([
                    'user_id'       => @$users->id,
                    'product_id'    => $id
                ]);
                $response['result']['message'] = __('success_site.-15000');
            } else {
                UserFavorite::where([
                    'user_id'       => @$users->id,
                    'product_id'    => $id
                ])->delete();
                $response['result']['message'] = __('success_site.-15001');
            }
            return response()->json($response);
        }catch(\Exception $e){
            $response['error'] = __('errors.-5099');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
    }

    /**
    * @OA\post(
    * path="/api/customer-cancel-order",
    * summary="customerCancelOrder",
    * description="customerCancelOrder",
    * operationId="customerCancelOrder",
    * tags={"Alaaddin: customerCancelOrder"},
    * @OA\RequestBody(
    *    required=true,
    *    description="customerCancelOrder",
    *    @OA\JsonContent(
    *       required={"order_master_id","order_details_id","product_id","cancel_note"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="order_master_id", type="integer", example="1"),
    *               @OA\Property(property="order_details_id", type="integer", example="2"),
    *               @OA\Property(property="product_id", type="integer", example="1"),
    *               @OA\Property(property="cancel_note", type="string", example="This is a cancel note"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry. Please try again")
    *        )
    *     )
    * )
    */
    public function customerCancelOrder(Request $request) {
        try{

            $response = [
                'jsonrpc' => '2.0'
            ];
            
            $reqData    = $request->json()->all();
            $reqdata    = $request->json()->all();

            $validator = Validator::make($reqdata['params'],[
                'order_details_id'    => 'required|numeric',
                'order_master_id'    => 'required|numeric',
                'product_id'    => 'required|numeric',
                'cancel_note'    => 'required'
            ],
            [
                'order_details_id.required'=>'order_details_id required',
                'order_details_id.numeric'=>'order_details_id required and shoule be a number',
                'order_master_id.numeric'=>'order_master_id required and shoule be a number',
                'order_master_id.required'=>'order_master_id required',
                'product_id.numeric'=>'product_id required and shoule be a number',
                'product_id.required'=>'product_id required',
                'cancel_note.required'=>'Cancel note required'
            ]
            );
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $order = OrderDetail::where(['id'=>$reqData['params']['order_details_id'],'order_master_id'=>$reqData['params']['order_master_id'],'product_id'=>$reqData['params']['product_id']])->whereNotIn('status',['OD','OC'])->first();
            
            if(!@$order){
                $response['error'] = __('errors.-5041');
                $response['error']['meaning'] = " This is either not associated with you or already delivered or cancelled ! ";
                return response()->json($response);
            }
            $orderMasterTable = OrderMaster::where(['id' => $order->order_master_id])->first();
            $status ='OC';
            
            $OrderDetailTble = OrderDetail::where(['id'=>$reqData['params']['order_details_id']])->first();
            $paymentDtls = Payment::where(['order_id'=>$order->order_master_id,'type'=>'order_payment'])->first();
            if(!@$paymentDtls){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "not found this id!";
                return response()->json($response);
            }
            $txn = $paymentDtls->txn_id;
            $URL='https://api.razorpay.com/v1/payments/'.$txn."/refund";
            $PaymentKeyTable = PaymentKeyTable::first();  
            if(!@$PaymentKeyTable){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "Key not found!";
                return response()->json($response); 
            }
            $username=$PaymentKeyTable->RAZORPAY_KEY;
            $password=$PaymentKeyTable->RAZORPAY_SECRET;
            $headers = array(
                "Authorization: Basic ".base64_encode($username.":".$password),
                'Content-Type: application/json'
            );
            $refundFlds['amount'] = round($OrderDetailTble->payable_amount*100);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$URL);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($refundFlds));
            $result=curl_exec ($ch);
            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
            curl_close ($ch);
            $jdres = json_decode($result);
            $jeres = json_encode($result);
            // if($status_code != 200){
            //     $response['error'] =__('errors.-5001');
            //     $response['error']['message'] = 'Error';
            //     $response['error']['meaning'] = @$result;
            //     return response()->json($response);
            // }
            $crCancelRequest['order_master_id']=$order->order_master_id;
            $crCancelRequest['order_details_id']=$OrderDetailTble->id;
            $crCancelRequest['product_id']=$OrderDetailTble->product_id;
            $crCancelRequest['status']="accepted";
            $crCancelRequest['cancel_note']=$reqData['params']['cancel_note'];
            $crCancelRequest['cancel_date']=date('Y-m-d');
            $crCancelRequest['amount']=$OrderDetailTble->payable_amount;

            $crCancelRequest['refund_response'] = @$jeres;
            $crCancelRequest['refund_id'] = @$jdres->id;
            $cancelrequesttbl = CancelRequest::create($crCancelRequest);
            $CancelRequest = CancelRequest::where(['id'=>$cancelrequesttbl->id])->first();
            if(!@$CancelRequest){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "provided wrong cancel_request_id!";
                return response()->json($response);
            }
            $crp['type'] = 'cancel_refund';
            $crp['order_id'] = $CancelRequest->order_master_id;
            $crp['cancel_request_id'] = $CancelRequest->id;
            $crp['txn_id'] = @$jdres->id;
            $crp['user_id'] = $CancelRequest->get_order_master->customerDetails->id;
            $crp['response'] = @$jeres;
            $crp['payment_initated_id'] = @$jdres->id;
            $crp['amount'] = $CancelRequest->amount;
            $crp['status'] = 'S';
            $p = Payment::create($crp);

            $update['status'] = 'OC';
            $update['cancelled_on'] = date('Y-m-d');
            $update['cancel_acpt_rej_date'] = date('Y-m-d');
            $update['cancel_note'] = $reqData['params']['cancel_note'];
            OrderDetail::where('id',$reqData['params']['order_details_id'])->update($update);


            $totalReady = OrderDetail::where(['order_master_id' => $order->order_master_id, 'status' => 'OD', 'seller_id' => $order->seller_id])->whereNotIn('status',['OC'])->count();
            $totalReady1 = OrderDetail::where(['order_master_id' => $order->order_master_id, 'seller_id' => $order->seller_id])->whereNotIn('status',['OC'])->count();

            # updating order seller table if all item is ready for pickup of this seller.
            if($totalReady1 == $totalReady) {
                if($status == 'OD'){
                    OrderSeller::where(['seller_id' => $order->seller_id, 'order_master_id' => $order->order_master_id])->update(['status' => 'CM','delivery_date'=>date('Y-m-d'),'delivery_time'=>date('H:i:s')]);
                }else{
                    OrderSeller::where(['seller_id' => $order->seller_id, 'order_master_id' => $order->order_master_id])->update(['status' => 'INP']);
                }
            }
            $totalReady = OrderDetail::where(['order_master_id' => $order->order_master_id, 'status' => 'OC', 'seller_id' => $order->seller_id])->count();
            $totalReady1 = OrderDetail::where(['order_master_id' => $order->order_master_id, 'seller_id' => $order->seller_id])->count();

            # updating order seller table if all item is ready for pickup of this seller.
            if($totalReady1 == $totalReady) {
                if($status == 'OC'){
                    OrderSeller::where(['seller_id' => $order->seller_id, 'order_master_id' => $order->order_master_id])->update(['status' => 'OC','delivery_date'=>date('Y-m-d'),'delivery_time'=>date('H:i:s')]);
                }
            }
            # updating order master table if all seller product is ready for pickup
            $totalReadyMaster = OrderDetail::where(['order_master_id' => $order->order_master_id, 'status' => $status])->count();
            $totalReadyMaster1 = OrderDetail::where(['order_master_id' => $order->order_master_id])->count();

            if($totalReadyMaster == $totalReadyMaster1) {
                if($status == 'OD'){
                    $updmstrO['status'] = 'CM';
                }
                if($status == 'OC'){
                    $updmstrO['status'] = 'CM';
                }
                $updmstrO['delivery_date'] = date('Y-m-d');
                $updmstrO['delivery_time'] = date('H:i:s');
                OrderMaster::where(['id' => $order->order_master_id])->update($updmstrO);
            }

            $this->cancelAproductOrder($orderMasterTable,$reqData['params']['order_details_id']);
            
            $this->sendMail($orderMasterTable,$reqData['params']['order_details_id']);
            
            $response['result'] = __('success.-5009');        
            return response()->json($response);
            
        }catch(\Exception $e){
            $response['error'] = __('errors.-5099');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
    }
    public function cancelAproductOrder($order,$id){
        
        $order = OrderDetail::where('id',$id)->first();
        $orderM = OrderMaster::where('id',$order->id)->first();

        $totdiscount = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('original_price') - OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('discounted_price');
        $total = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('total');
        $payable_amount = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('payable_amount');
        $sub_total = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('sub_total');
        $insurance_price = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('insurance_price');
        $loading_price = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('loading_price');
        $admin_commission = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('admin_commission');
        $total_seller_commission = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('seller_commission');
        $weight = OrderDetail::where(['order_master_id'=>$order->order_master_id])->whereNotIn('status',['OC'])->sum('weight');
        
        $updMstr['total_discount'] = $totdiscount;
        $updMstr['total_product_price'] = $total;
        $updMstr['payable_amount'] = $payable_amount;
        $updMstr['order_total'] = $payable_amount;
        $updMstr['subtotal'] = $sub_total;
        $updMstr['insurance_price'] = $insurance_price;
        $updMstr['loading_price'] = $loading_price;
        $updMstr['admin_commission'] = $admin_commission;
        $updMstr['total_seller_commission'] = $total_seller_commission;
        $updMstr['product_total_weight'] = $weight;
        OrderMaster::where(['id'=>$order->order_master_id])->update($updMstr);

        $ords = OrderSeller::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->first();
        $updOrds['loading_price'] = $ords->loading_price-$order->loading_price;
        $updOrds['insurance_price'] = $ords->insurance_price-$order->insurance_price;
        // $updOrds['admin_commission'] = $ords->admin_commission-$order->admin_commission;
        $updOrds['seller_commission'] = $ords->seller_commission-$order->seller_commission;
        $updOrds['payable_amount'] = $ords->payable_amount-$order->payable_amount;
        $updOrds['admin_commission'] = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->whereNotIn('status',['OC'])->sum('admin_commission');
        $updOrds['subtotal'] = $ords->subtotal-$order->sub_total;
        $dis = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->whereNotIn('status',['OC'])->sum('original_price') - OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->whereNotIn('status',['OC'])->sum('discounted_price');

        $updOrds['total_discount'] = $dis;
        $updOrds['order_total'] = $ords->order_total-$order->payable_amount;
        $updOrds['total_commission'] = $ords->total_commission;
        $updOrds['earning'] = $ords->earning - $order->loading_price - $order->seller_commission - ($order->total-$order->admin_commission);
        $weight = OrderDetail::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->whereNotIn('status',['OC'])->sum('weight');
        $updOrds['total_weight'] = $weight;
        OrderSeller::where(['order_master_id'=>$order->order_master_id,'seller_id'=>$order->seller_id])->update($updOrds);
    }
    public function sendMail($orderM,$orderDetailsId){
        try{
        $gorderDetails = OrderDetail::where(['id'=>$orderDetailsId])->first();
        $order = OrderMaster::with(['customerDetails.userCountryDetails.countryDetailsBylanguage',
            'orderMasterDetails',
            'countryDetails.countryDetailsBylanguage',
            'orderMasterDetails.getOrderDetailsUnitMaster',
            'orderMasterDetails.productDetails.productByLanguage',
            'orderMasterDetails.productDetails.productVariants',
            'orderMasterDetails.productDetails.productUnitMasters',
            'orderMasterDetails.productVariantDetails',
            'orderMasterDetails.sellerDetails.merchantCityDetails',
            'countryDetails.countryDetailsBylanguage',
            'shippingAddress',
            'billingAddress',
            'orderMerchants.orderSeller',
            'orderMerchants'=> function($query) use($gorderDetails) {
            $query->where(function($q) use($gorderDetails) {
                $q->where('seller_id', $gorderDetails->seller_id);
            });
        },
            'getBillingCountry',
            'productVariantDetails',
            'getOrderAllImages',
            'orderMasterDetails.productDetails.defaultImage',
            'orderMasterExtDetails.sellerDetails',
            'orderMasterDetails.driverDetails',
            'orderMasterExtDetails.driverDetails',
            'getCityNameByLanguage',
            'getBillingCityNameByLanguage',
            'paymentData',
            'getpickupCountryDetails',
            'getpickupCityDetails',
            'getPreferredTime'])->where('id',$orderM->id)->first();
            $data['order'] = @$order;
            Mail::send(new SendOrderMail($data));
            foreach(@$order->orderMerchants as $o){

                $order = OrderMaster::with(['customerDetails.userCountryDetails.countryDetailsBylanguage',
                'orderMasterDetails'=> function($query) use($o) {
                    $query->where(function($q) use($o){
                        $q->where('seller_id', $o->seller_id);
                    });
                },
                'orderMerchants'=> function($query) use($o) {
                    $query->where(function($q) use($o){
                        $q->where(['seller_id'=>$o->seller_id]);
                    });
                },
                'orderMerchants.orderSeller',
                'countryDetails.countryDetailsBylanguage',
                'orderMasterDetails.getOrderDetailsUnitMaster',
                'orderMasterDetails.productDetails.productByLanguage',
                'orderMasterDetails.productDetails.productVariants',
                'orderMasterDetails.productDetails.productUnitMasters',
                'orderMasterDetails.productVariantDetails',
                'orderMasterDetails.sellerDetails.merchantCityDetails',
                'countryDetails.countryDetailsBylanguage',
                'shippingAddress',
                'billingAddress',
                'getBillingCountry',
                'productVariantDetails',
                'getOrderAllImages',
                'orderMasterDetails.productDetails.defaultImage',
                'orderMasterExtDetails.sellerDetails',
                'orderMasterDetails.driverDetails',
                'orderMasterExtDetails.driverDetails',
                'getCityNameByLanguage',
                'getBillingCityNameByLanguage',
                'paymentData',
                'getpickupCountryDetails',
                'getpickupCityDetails',
                'getPreferredTime'])->where('id',$orderM->id)->first();
                $data['order'] = @$order;
                Mail::send(new SendMerchantOrderMail($data));    
            }
        }catch(\Exception $e){
            
        }
    }
    /**
    * @OA\post(
    * path="/api/get-all-state",
    * summary="Get state",
    * description="Get state",
    * operationId="authLogin",
    * tags={"Alaaddin: Get state"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Get state",
    *    @OA\JsonContent(
    *       required={"state_name"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="state_name", type="string", format="name", example=""),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function getAllState(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            // $user = auth()->user();
            $reqData = $request->json()->all();
            $state = State::with(['cityDetails'])->orderBy('name','asc');
            if(@$reqData['params']['state_name']){
                $stateName = $reqData['params']['state_name'];  
                $state = $state->where(function($query) use($stateName) {
                    $query->where('name', 'LIKE', "%{$stateName}%");
                });  
            }
            $state = $state->get();            
            $response['result']['state'] = $state;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error'] = $e->getMessage();
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }    
    }
    /**
    * @OA\Post(
    * path="/api/customer-update-bank-details",
    * summary="Customer Update Bank Details",
    * description="Customer Update Bank Details",
    * operationId="updateCustomerBankDetails",
    * tags={"Alaaddin: Customer Update Bank Details"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer Update Bank Details ",
    *    @OA\JsonContent(
    *       required={"bank_name","account_name","account_number","ifsc_number","branch_name"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="bank_name", type="string", format="bank_name", example=""),
    *           @OA\Property(property="account_name", type="string", format="account_name", example=""),
    *           @OA\Property(property="account_number", type="string", format="account_number", example=""),
    *           @OA\Property(property="ifsc_code", type="string", format="ifsc_code", example=""),
    *           @OA\Property(property="branch_name", type="string", format="branch_name", example=""),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function updateCustomerBankDetails(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user();
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'], [
                'bank_name' => 'required',
                'account_name' => 'required',
                'account_number' => 'required',
                'ifsc_code' => 'required',
                'branch_name' => 'required',
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5097');
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            
            $bankInfo['bank_name']      =   $reqdata['params']['bank_name'];
            $bankInfo['account_name']   =   $reqdata['params']['account_name'];
            $bankInfo['account_number'] =   $reqdata['params']['account_number'];
            $bankInfo['ifsc_code']    =   $reqdata['params']['ifsc_code'];
            $bankInfo['branch_name']    =   $reqdata['params']['branch_name'];
            User::where(['id'=>$user->id])->update($bankInfo);
            $response['result'] = __('success_user.-700');
            $response['result']['meaning'] = "Bank details updated successfully";
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error'] = $e->getMessage();
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }  
    }

    /**
    * @OA\Post(
    * path="/api/show-customer-bank-details",
    * summary="show-customer-bank-details Customer showCustomerBankDetails",
    * description=" show-customer-bank-details Customer showCustomerBankDetails",
    * operationId="showCustomerBankDetails",
    * tags={"Alaaddin: show-customer-bank-details Customer showCustomerBankDetails"},
    * security={{"bearer_token":{}}},
    * @OA\Response(
    *    response=422,
    *    description="Wrong ",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function showCustomerBankDetails(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user();
            $reqdata = $request->json()->all();
            
            $bankInfo['bank_name']      =   @$user->bank_name;
            $bankInfo['account_name']   =   @$user->account_name;
            $bankInfo['account_number'] =   @$user->account_number;
            $bankInfo['ifsc_code']    =   @$user->ifsc_code;
            $bankInfo['branch_name']    =   @$user->branch_name;
            $response['result']['bankInfo'] = $bankInfo;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            // $response['error'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            // $response['error'] = $th->getMessage();
            return response()->json($response);
        }  
    }
}