<?php

namespace App\Http\Controllers\Api\Modules\Cart;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// repository
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductVariantDetailRepository;
use App\Repositories\ProductRepository;
use App\Models\CartDetail;
use App\Models\CartMaster;
use App\Merchant;
use App\Models\Setting;
use App\Models\Product;
use App\Models\ShippingCost;
use App\User;

use JWTFactory;
use JWTAuth;
use Validator;
use Mail;

class CartController extends Controller
{
	protected $cart, $cartDetails, $product, $productVariant, $productVariantDetail;
	protected $userId = NULL;
	protected $deviceId = NULL;

	/**
	* __construct
	* This method is used to initilize the instance
	* Author: Sanjoy
	*/
	public function __construct(CartMasterRepository $cart,
								CartDetailRepository $cartDetails,
								ProductVariantRepository $productVariant,
								ProductRepository $product,
								ProductVariantDetailRepository $productVariantDetail
							)
	{
		$this->cart         			= $cart;
		$this->cartDetails  			= $cartDetails;
		$this->productVariant  			= $productVariant;
		$this->productVariantDetail  	= $productVariantDetail;
		$this->product  				= $product;
	}

	/**
	* Method: index
	* Description: This method is used to show the cart page.
	* Author: Sanjoy
	*/
	/**
    * @OA\Get(
    * path="/api/get-cart/{deviceId}",
    * summary="Get Cart",
    * description="Get Cart",
    * operationId="authLogin",
    * tags={"Alaaddin: Get Cart"},
    * security={{"bearer_token":{}}},
  	*      @OA\Parameter(
    *         name="deviceId",
    *         in="path",
    *         description="device id",
    *         required=false,
    *      ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
	public function index(Request $request, $deviceId = NULL) {
		$response = [
			'jsonrpc' => '2.0'
		];
		try {
			# if no token or device id provided.
			if(!@getUserByToken()->id) {
				$response['error']['message'] = __('customer_api.invalid_request');
				return response()->json($response);
			}
			$setting = Setting::first();
			$response['result']['cart'] = $this->cart;
	        $response['result']['cart'] = $response['result']['cart']->where('user_id', getUserByToken()->id);
			$response['result']['cart'] = $cartMasterData = $response['result']['cart']->first();

			# cart details seller specific
			if(@$cartMasterData) {
				$insurancePrice =  (@$cartMasterData->total * @$setting->insurance) / 100;
				// $response['result']['cart']->insurance_price = (string) number_format(@$insurancePrice,2,'.','');
				$cartDetails = CartDetail::where(['cart_master_id' => $cartMasterData->id])->get();
				@$loadingPrice = 0;
				foreach ($cartDetails as $key => $value) {
					@$loadingPrice +=  @$setting->loading_unloading_price;
				}
				
				$response['result']['cart']->subtotal = $response['result']['cart']->total+$response['result']['cart']->insurance_price+$response['result']['cart']->loading_price+$response['result']['cart']->total_seller_commission;
				$response['result']['cart']->subtotal = (string) number_format($response['result']['cart']->subtotal,2,'.','');
				$cartMasterId = $cartMasterData->id;
				$response['result']['sellers'] = CartDetail::select('id', 'seller_id', 'product_id','product_note')
					->with(['sellerInfo.products.productByLanguage:product_id,language_id,title,description','sellerInfo.products.getProduct.productUnitMasters'])
					->with(['sellerInfo.products.defaultImage:product_id,image'])
					->with(['sellerInfo.products.productVariantDetails'])
					// ->with(['sellerInfo.products.getProduct.productUnitMasters'])
					->with(['sellerInfo.products' => function($query) use($cartMasterId){
						$query->where('cart_master_id', $cartMasterId);
					}])
					->with(['sellerInfo:id,fname,lname,slug'])
					->whereHas('sellerInfo.products', function($q) use($cartMasterId){
						$q->where('cart_master_id', $cartMasterId);
					})
					->where(['cart_master_id' => $cartMasterData->id])
					->groupBy('seller_id')
					->get();	
			}
			else{
				$response['error'] = __('errors.-5099');
			}
			return response()->json($response);
		} catch(\Exception $e) {
			// $response['error']['message'] = $e->getMessage();
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
			return response()->json($response);
		}
	}


	/**
	* Method: addToCart
	* Description: This method is used to add to cart
	* Author: Sanjoy
	*/
	/**
    * @OA\Post(
    * path="/api/add-to-cart",
    * summary="Customer Add To Cart",
    * description="Customer Add To Cart",
    * operationId="addToCart",
    * tags={"Alaaddin: Customer Add To Cart"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer Add To Cart",
    *    @OA\JsonContent(
    *       required={"quantity","product_id","device_id"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="quantity", type="integer", format="quantity", example="1"),
    *           @OA\Property(property="product_id", type="integer", format="product_id", example="1"),
    *           @OA\Property(property="device_id", type="string", format="device_id", example=""),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
	public function addToCart(Request $request) {
		$response = [
			'jsonrpc' => '2.0'
		];

		try {
				$reqData = $request->json('params');
				$reqdata = $request->json()->all();
			
				$userId = @getUserByToken()->id;
				if(!@$reqData['device_id'] && !@$userId) {
					$response['error']['message'] = __('customer_api.invalid_request');
					return response()->json($response);
				}
			
				if(@getUserByToken()->id) {
					$this->userId = getUserByToken()->id;
				}
				// if(@$reqData['device_id']) {
				// 	$this->deviceId = @getUserByToken()->id;
				// }else{
				// 	$reqData['device_id'] = @getUserByToken()->id;
				// 	$this->deviceId = @getUserByToken()->id;
				// }
				
				$validator = Validator::make($reqdata['params'],[
	                'quantity'    => 'required|numeric',
	                'product_id'    => 'required|numeric',
	                'device_id'    => 'required',
	            ],
	            [
	                'quantity.required'=>'payment_method required',
	                'quantity.numeric'=>'provide quantity in number only!You have sent quantity : '.@$reqdata['params']['quantity'],
	                'product_id.required'=>'product_id required',
	                'product_id.numeric'=>'provide product id ',
	            ]);
				if($validator->fails()){
	                 $response['error'] =__('errors.-5001');
	                $response['error']['message'] = 'Error';
	                $response['error']['meaning'] = $validator->errors()->first();
	                return response()->json($response);
	            }
	            $productDtls = Product::with('productUnitMasters')->where(['id'=>$reqdata['params']['product_id'],'status'=>'A','seller_status'=>'A'])->first();
	            
	            if(!@$productDtls){
	            	$response['error'] =__('errors.-5001');
	                $response['error']['meaning'] = 'this product is not available now !';
	                return response()->json($response);
	            }
	            $checkMerchant = Merchant::where(['id'=>$productDtls->user_id,'status'=>'A'])->first();
	            if(!@$checkMerchant){
	            	$response['error'] =__('errors.-5001');
	                $response['error']['meaning'] = 'merchant of the product is not available now !';
	                return response()->json($response);
	            }
	            if(@$productDtls->productUnitMasters->id == 2){
	            	// carat
	            	$validator = Validator::make($reqdata['params'], [
					    'quantity' => [
					        'required',
					        'integer',
					        function ($attribute, $value, $fail) {
					            if ($value % 40 !== 0) {
					                $fail($attribute.' is invalid.Quantity should be multiplication of 40  where unit is carat!You have to buy 40 carat or multiplication of 40 carat!');
					            }
					        },
					    ],
					]);
					if($validator->fails()){
		                 $response['error'] =__('errors.-5001');
		                $response['error']['message'] = 'Error';
		                $response['error']['meaning'] = $validator->errors()->first();
		                return response()->json($response);
		            }

	            }

			# checking stock availability and correct variant ID.
			$productVariant = $this->checkStockAndVariant($reqData);
			// if(!@$productVariant) {
			// 	$avlQty = $this->checkAvailableQty($reqData);
			// 	$response['error'] = __('errors.-5039', ['item' => $avlQty]);
			// 	return response()->json($response);
			// }
			
			# insert to cart master table.
			$cartMaster = $this->checkCartMaster($reqData);

			# if already available with cart master id and product id in cart details table the update it otherwise insert it.
			$cartDetails = CartDetail::where([
				'product_id' 		=> $reqData['product_id'],
				'cart_master_id' 	=> $cartMaster->id,
				// 'product_variant_id'=> @$productVariant->id
			])
			->first();
			if($cartDetails) {
				$this->updateCartDetails($cartMaster, $productVariant, $reqData, $cartDetails);
			} else {
				# insert to cart details page
				$this->insertCartDetails($cartMaster, $productVariant, $reqData);
			}
			
			$response['result']['cart'] = $this->calculateCart($cartMaster);

			if(@$response['result']['cart']){
				$response['result']['cart_item'] = CartMaster::where(['id' => @$response['result']['cart']->id])->select('total_item');
			}else{
				$response['result']['cart_item'] = CartMaster::where(['user_id' => @getUserByToken()->id])->select('total_item');
			}
			// if(@$reqData['device_id']) {
			// 	$response['result']['cart_item'] = $response['result']['cart_item']->where(['session_id' => @$reqData['device_id']]);
			// }
			if(@getUserByToken()->id) {
				$response['result']['cart_item'] = $response['result']['cart_item']->where(['user_id' => @getUserByToken()->id]);
			}
			$response['result']['cart_item'] = $response['result']['cart_item']->first();
			$response['result']['message'] = __('success_site.-15002');

			return response()->json($response);
		} catch(\Exception $e) {
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
			// $response['error']['message'] = $e->getMessage();
			return response()->json($response);
		}
		
	}

	/**
	* Method: checkAvailableQty
	* Description: This method is used to checkavailable quantity.
	* Author: Sanjoy
	*/
	private function checkAvailableQty($reqData) {
		# if trying to add second time
		$cart = CartMaster::where(['user_id' => $this->userId]);
		if(@$this->deviceId) {
			$cart = $cart->orWhere('session_id', $this->deviceId);
		}
		$cart = $cart->first();
		// if(@$reqData['variants']) {
		// 	$variants = $this->productVariant->findWhere(['product_id' => $reqData['product_id']]);
		// 	foreach ($variants as $key => $value) {
		// 		# checking variant with request variant ID's is available or not
		// 		if(!array_diff($reqData['variants'], json_decode($value->variants))) {
		// 			# if previously added to cart then check stock with previous
		// 			if(@$cart) {
		// 				$cartDetails = CartDetail::where(['cart_master_id' => $cart->id, 'product_id' => $reqData['product_id'], 'product_variant_id' => $value->id])->first();
		// 				$availableQty = $value->stock_quantity - @$cartDetails->quantity;
		// 			} else {
		// 				$availableQty = $value->stock_quantity;
		// 			}
		// 		}
		// 	}
		// }
		# if product has no variants then check stock with product table.
		// else {
			$product = $this->product->where(['id' => $reqData['product_id']])->first();
			if(@$cart) {
				$cartDetails = CartDetail::where(['cart_master_id' => $cart->id, 'product_id' => $reqData['product_id']])->first();
				$availableQty = $product->stock - @$cartDetails->quantity;
			} else {
				$availableQty = $product->stock;
			}
		// }
		return $availableQty;
	}

	/**
	* Method: checkStockAndVariant
	* Description: This method is used to check variant stock and correct variand ID.
	* Author: Sanjoy
	*/
	private function checkStockAndVariant($reqData = []) {
		$totalQty = $reqData['quantity'];
		# if trying to add second time
		$cart = CartMaster::where(['user_id' => $this->userId]);
		if(@$this->deviceId) {
			$cart = $cart->orWhere('session_id', $this->deviceId);
		}
		$cart = $cart->first();
		// if(@$reqData['variants']) {
		// 	$variants = $this->productVariant->findWhere(['product_id' => $reqData['product_id']]);
		// 	foreach ($variants as $key => $value) {
		// 		# checking variant with request variant ID's is available or not
		// 		if(!array_diff($reqData['variants'], json_decode($value->variants))) {
		// 			# if previously added to cart then check stock with previous
		// 			if(@$cart) {
		// 				$cartDetails = CartDetail::where(['cart_master_id' => $cart->id, 'product_id' => $reqData['product_id'], 'product_variant_id' => $value->id])->first();
		// 				$totalQty = $totalQty + @$cartDetails->quantity;
		// 			}
		// 			# stock available or not 
		// 			if($value->stock_quantity >= $totalQty) {
		// 				return $value;
		// 			}
		// 		}
		// 	}
		// }
		# if product has no variants then check stock with product table.
		// else {
			$product = $this->product->where(['id' => $reqData['product_id']])->first();
			if(@$cart) {
				$cartDetails = CartDetail::where(['cart_master_id' => $cart->id, 'product_id' => $reqData['product_id']])->first();
				$totalQty = $totalQty + @$cartDetails->quantity;
			}
			if($product->stock >= $totalQty) {
				return 1;
			} 
		// }
		return 0;
	}

	/**
	* Method: calculateShippingPrice
	* Description: This method is used to calculate shipping price
	* Author: Sanjoy
	*/
	private function calculateShippingPrice($cartMaster) {

	}

	/**
	* Method: checkCartMaster
	* Description: This method is used to check is it previously added in cart or not
	* Author: Sanjoy
	*/
	private function checkCartMaster($reqData = []) {
		$cart = CartMaster::where(['user_id' => $this->userId]);
		if(@$this->deviceId) {
			$cart = $cart->orWhere('session_id', $this->deviceId);
		}
		$cart = $cart->first();
		if(!@$cart) {
			return $this->insertToCartMaster($reqData);
		} else {
			return $cart;
		}
	}

	/**
	* Method: insertToCartMaster
	* Description: This method is used to insert cart master table.
	* Author: Sanjoy
	*/
	private function insertToCartMaster($reqData = []) {
		return CartMaster::create([
			'user_id' 		=> @$this->userId?$this->userId:0,
			'session_id'	=> @$this->deviceId,
		]);
	}

	/**
	* Method: insertCartDetails
	* Description: This method is used to insert cart details table.
	* Author: Sanjoy
	*/
	private function insertCartDetails($cartMaster, $productVariant = [], $reqData = []) {
		# get product information
		
		$product = $this->getProduct($reqData['product_id']);
		$productDetails = Product::with(['productUnitMasters'])->where(['id'=>$product->id])->first();
		$originalPrice = 0;
		$discountedPrice = 0;
		$total_discount = 0;
		$subtotal = 0;
		$total = 0;
		
		$totalCom = 0;
			$originalPrice = $product->price;
				
					$qtyvalue = $reqData['quantity'];
				
			if($product->discount_price != 0) {
				$discountedPrice = $product->discount_price;
				$total_discount = ($product->price - $product->discount_price) * $qtyvalue;
			}
			$subtotal = $product->price * $qtyvalue;
			$total = $subtotal - $total_discount;	
			$setting = Setting::first();
			$weight = $reqData['quantity'] * $productDetails->productUnitMasters->weight_in_ton;
			$loading_price = $reqData['quantity'] * ($productDetails->productUnitMasters->weight_in_ton * $setting->loading_unloading_price);
			$insurance_price = ($total* @$setting->insurance) / 100;
		$seller = Merchant::whereId($product->user_id)->where('status','A')->first();
		if(!empty($seller)){
			if(@$seller->commission > 0)
			$totalCom = ($total * @$setting->seller_commission) / 100;
		}
		$payable_amount = $total+$loading_price+$insurance_price+@$totalCom;
		$admin_commission = ($total * @$seller->commission) / 100;
		
	

		return CartDetail::create([
			'cart_master_id' 	=> $cartMaster->id,
			'product_id'		=> $reqData['product_id'],
			'weight'		=> $weight,
			'seller_id' 		=> $product->user_id,
			'quantity'			=> $reqData['quantity'],
			'original_price'	=> $originalPrice,
			'discounted_price' 	=> $discountedPrice,
			'total_discount' 	=> $total_discount,
			'subtotal'			=> $subtotal,
			'total'				=> $total,
			'product_note'		=> @$reqData['product_note'],
			'loading_price'		=> $loading_price,
			'insurance_price'		=> $insurance_price,
			'payable_amount'		=> $payable_amount,
			'admin_commission'		=> $admin_commission,
			'seller_commission' => @$totalCom
		]);
	}

	/**
	* 
	*/
	private function updateCartDetails($cartMaster, $productVariant = [], $reqData = [], $cartDetails) {
		# get product information
		$product = $this->getProduct($reqData['product_id']);
		$originalPrice = 0;
		$discountedPrice = 0;
		$total_discount = 0;
		$subtotal = 0;
		$total = 0;
		
		$totalCom = 0;
		
			$qtyvalue = $reqData['quantity'];
		
		// if(@$productVariant->price != 0) {
		// 	#if discount price is available then calculate this.
		// 	$subtotal = $cartDetails->subtotal + ($productVariant->price * $reqData['quantity']);
		// 	if($productVariant->discount_price != 0) {
		// 		$total_discount = $cartDetails->total_discount + (($productVariant->price - $productVariant->discount_price) * $reqData['quantity']);
		// 	}
		// 	$total = $subtotal - $total_discount;
		
		// } else {
			if($product->discount_price != 0) {
				$total_discount = $cartDetails->total_discount + ($product->price - $product->discount_price) * $qtyvalue;
			}
			$subtotal = $cartDetails->subtotal + ($product->price * $qtyvalue);
			$total = $subtotal - $total_discount;
			
		// }
			$setting = Setting::first();
		$seller = Merchant::whereId($product->user_id)->where('status','A')->first();
		if(!empty($seller)){
			if(@$seller->commission > 0)
			$totalCom = ($total * @$setting->seller_commission) / 100;
		}
		
		$setting = Setting::first();
			
			$productDetails = Product::with(['productUnitMasters'])->where(['id'=>$product->id])->first();
			$weight = $reqData['quantity'] * $productDetails->productUnitMasters->weight_in_ton;
			$loading_price = (($cartDetails->quantity + $reqData['quantity'])*(@$productDetails->productUnitMasters->weight_in_ton) * (@$setting->loading_unloading_price));

		$insurance_price = ($total* @$setting->insurance) / 100;	
		$payable_amount = $total+$loading_price+$insurance_price+@$totalCom;
		$admin_commission = ($total * @$seller->commission) / 100;



		CartDetail::where(['id' => $cartDetails->id])
			->update([
				'quantity' 			=> $cartDetails->quantity + $reqData['quantity'],
				'weight' 			=> $weight,
				'subtotal'			=> $subtotal,
				'total_discount' 	=> $total_discount,
				'total'				=> $total,
				'product_note'		=> @$reqData['product_note'],
				'loading_price'		=> @$loading_price,
				'insurance_price'		=> @$insurance_price,
				'admin_commission'		=> @$admin_commission,
				'payable_amount'		=> @$payable_amount,
				'seller_commission' => @$totalCom
			]);
	}

	/**
	* Method: calculateCart
	* Description: This method is used to calculate cart.
	* Author: Sanjoy
	*/
	private function calculateCart($cartMaster) {
		$cartDetails = CartDetail::where(['cart_master_id' => $cartMaster->id])->get();
		$total_discount = $cartDetails->sum('total_discount');
		$cartMaster->where(['id' => $cartMaster->id])->update([
			'total_discount' => $total_discount,	
			'subtotal' 		 => $cartDetails->sum('subtotal'),
			'total'		     => $cartDetails->sum('total'),
			'total_item'	 => $cartDetails->count(),
			'total_qty'		 =>	$cartDetails->sum('quantity'),
			'loading_price'		 =>	$cartDetails->sum('loading_price'),
			'insurance_price'		 =>	$cartDetails->sum('insurance_price'),
			'payable_amount'		 =>	$cartDetails->sum('payable_amount'),
			'admin_commission'		 =>	$cartDetails->sum('admin_commission'),
			'total_seller_commission' => $cartDetails->sum('seller_commission')
		]);
		return CartMaster::where(['id' => $cartMaster->id])->first(); 
	}

	/**
	* Mehtod: updateCart,
	* Description: This method is used to update cart quantity
	* Author: Sanjoy
	*/
	/**
    * @OA\Post(
    * path="/api/update-cart",
    * summary="Customer Update Cart",
    * description="Customer Update Cart",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Update Cart"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer Update Cart",
    *    @OA\JsonContent(
    *       required={"quantity","product_id","device_id","cart_details_id","cart_master_id"},
    *           @OA\Property(property="params", type="object",
    *           @OA\Property(property="cart_details_id", type="integer", format="cart_details_id", example="0"),
    *           @OA\Property(property="cart_master_id", type="integer", format="cart_master_id", example="0"),
    *           @OA\Property(property="quantity", type="integer", format="quantity", example="1"),
    *           @OA\Property(property="product_id", type="integer", format="product_id", example="1"),
    *           @OA\Property(property="device_id", type="string", format="device_id", example=""),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
	public function updateCart(Request $request) {
		$response = [
			"jsonrpc" => "2.0"
		];
		try {
			$reqData = $request->params;
			$reqdata = $request->json()->all();
			$userId = @getUserByToken()->id;

			$validator = Validator::make($reqdata['params'],[
	                'quantity'    => 'required|numeric',
	                'cart_details_id'    => 'required|numeric',
	                'cart_master_id'    => 'required|numeric',
	                'product_id'    => 'required|numeric',
	            ],
	            [
	                'cart_details_id.required'=>'provide cart details id required',
	                'cart_master_id.numeric'=>'provide cart master id ',
	                'quantity.required'=>'quantity required',
	                'quantity.numeric'=>'provide quantity in number only!',
	                'product_id.required'=>'product_id required',
	                'product_id.numeric'=>'provide product id ',
	            ]);
				if($validator->fails()){
	                 $response['error'] =__('errors.-5001');
	                $response['error']['message'] = 'Error';
	                $response['error']['meaning'] = $validator->errors()->first();
	                return response()->json($response);
	            }
	            $productDtls = Product::with('productUnitMasters')->where(['id'=>$reqdata['params']['product_id'],'status'=>'A','seller_status'=>'A'])->first();
	            
	            if(!@$productDtls){
	            	$response['error'] =__('errors.-5001');
	                $response['error']['meaning'] = 'this product is not available now !';
	                return response()->json($response);
	            }
	            $checkMerchant = Merchant::where(['id'=>$productDtls->user_id,'status'=>'A'])->first();
	            if(!@$checkMerchant){
	            	$response['error'] =__('errors.-5001');
	                $response['error']['meaning'] = 'merchant of the product is not available now !';
	                return response()->json($response);
	            }
	            if(@$productDtls->productUnitMasters->id == 2){
	            	
	            	// carat

	            	$validator = Validator::make($reqdata['params'], [
					    'quantity' => [
					        'required',
					        'integer',
					        function ($attribute, $value, $fail) {
					            if ($value % 40 !== 0) {
					                $fail($attribute.' is invalid.Quantity should be multiplication of 40  where unit is carat!You have to buy 40 carat or multiplication of 40 carat!');
					            }
					        },
					    ],
					]);
					if($validator->fails()){
		                 $response['error'] =__('errors.-5001');
		                $response['error']['message'] = 'Error';
		                $response['error']['meaning'] = $validator->errors()->first();
		                return response()->json($response);
		            }

	            }

			# if device id or user id doesn't provide.
			if(!@$userId) {
				$response['error']['message'] = __('customer_api.invalid_request');
				return response()->json($response);
			}
			
			# if cart master id or cart details id is wrong then.
			$cartDetailsData = CartDetail::where(['id' => $reqData['cart_details_id'], 'cart_master_id' => $reqData['cart_master_id']])->first();
			if(!@$cartDetailsData) {
				$response['error']['message'] = __('customer_api.invalid_request');
				return response()->json($response);
			}

			if(@getUserByToken()->id) {
				$this->userId = getUserByToken()->id;
			}
			if(@$reqData['device_id']) {
				$this->deviceId = $reqData['device_id'];
			}else{
				$this->deviceId = "no_device_id_received";
			}
			# checking stock availability and correct variant ID.
			// $productVariant = $this->checkStockAndVariant1($reqData);
			// if(!@$productVariant) {
			// 	$avlQty = $this->checkAvailableQty1($reqData);
			// 	$response['error'] = __('errors.-5039', ['item' => $avlQty]);
			// 	return response()->json($response);
			// }

			$cartDetails = CartDetail::where([
							'id' => $reqData['cart_details_id'], 
						])
						->first();
			$cartMaster = CartMaster::where(['id' => $cartDetails->cart_master_id])->first();
			// $productVariant = $this->productVariant->where(['id' => $reqData['product_variant_id']])->first();
			$this->updateCartQuantity($cartMaster, $reqData, $cartDetails);

			/*if($this->userId) {
				$this->calculateShippingPrice($cartMaster);
			}*/
			$response['result']['cart'] = $this->calculateCart($cartMaster);
			$response['result']['cart_details'] = CartDetail::where([
							'id' => $reqData['cart_details_id'], 
						])
						->first();
			$response['result']['message'] = __('success_site.-15002');
			return response()->json($response);
		} catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error']['message'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        } catch (\Exception $th) {
        	$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            // $response['error']['message'] = $th->getMessage();
            return response()->json($response);
        }
		
	}

	/**
	* Method: checkAvailableQty
	* Description: This method is used to checkavailable quantity.
	* Author: Sanjoy
	*/
	private function checkAvailableQty1($reqData) {
		# if trying to add second time
		$cart = CartMaster::where(['id' => $reqData['cart_master_id']])->first();
		// if(@$reqData['product_variant_id']) {
		// 	$variants = $this->productVariant->where(['id' => $reqData['product_variant_id']])->first();
		// 	# if previously added to cart then check stock with previous
		// 	if(@$cart) {
		// 		$cartDetails = CartDetail::where(['id' => $reqData['cart_details_id']])->first();
		// 		$availableQty = $variants->stock_quantity - @$cartDetails->quantity;
		// 	} else {
		// 		$availableQty = $variants->stock_quantity;
		// 	}
		// }
		// # if product has no variants then check stock with product table.
		// else {
			$product = $this->product->where(['id' => $reqData['product_id']])->first();
			if(@$cart) {
				$cartDetails = CartDetail::where(['id' => $reqData['cart_details_id']])->first();
				$availableQty = $product->stock - @$cartDetails->quantity;
			} else {
				$availableQty = $product->stock;
			}
		// }
		return $availableQty;
	}

	/**
	* Method: checkStockAndVariant1
	* Description: This method is used to check variant stock and correct variand ID.
	* Author: Sanjoy
	*/
	private function checkStockAndVariant1($reqData = []) {
		$totalQty = $reqData['quantity'];
		# when trying to updating quantity from cart page.
		// if(@$reqData['product_variant_id']) {
		// 	$variants = $this->productVariant->where(['id' => $reqData['product_variant_id']])->first();
		// 	# stock available or not 
		// 	if($variants->stock_quantity >= $totalQty) {
		// 		return $variants;
		// 	}
		// } 
		# if product has no variants then check stock with product table.
		// else {
			$product = $this->product->where(['id' => $reqData['product_id']])->first();
			if($product->stock >= $totalQty) {
				return 1;
			} 
		// }
		return 0;
	}

	/**
	* Method: updateCartQuantity
	* Description: This method is used to update qty 
	*/
	private function updateCartQuantity($cartMaster, $reqData = [], $cartDetails) {
		# get product information
		$product = $this->getProduct($reqData['product_id']);

		$originalPrice = 0;
		$discountedPrice = 0;
		$total_discount = 0;
		$subtotal = 0;
		$total = 0;
		$totalCom = 0;
		// if(@$productVariant->price != 0) {
		// 	#if discount price is available then calculate this.
		// 	$subtotal = $productVariant->price * $reqData['quantity'];
		// 	if(($productVariant->discount_price != 0) && (date('Y-m-d')>=$productVariant->from_date && date('Y-m-d')<=$productVariant->to_date)) {
		// 		$total_discount = ($productVariant->price - $productVariant->discount_price) * $reqData['quantity'];
		// 	}
		// 	$total = $subtotal - $total_discount;
		// } else {
			// if($product->discount_price != 0 && (date('Y-m-d')>=$product->from_date && date('Y-m-d')<=$product->to_date)) {
			if($product->discount_price != 0) {
				$total_discount = ($product->price - $product->discount_price) * $reqData['quantity'];
			}
			$subtotal = $product->price * $reqData['quantity'];
			$total = $subtotal - $total_discount;
			
		// }
		$seller = Merchant::whereId($product->user_id)->where('status','A')->first();
		if(!empty($seller)){
			if(@$seller->commission > 0)
			$totalCom = ($total * @$seller->commission) / 100;
		}
		$setting = Setting::first();
		$productDetails = Product::with(['productUnitMasters'])->where(['id'=>$product->id])->first();
		$weight = $reqData['quantity'] * $productDetails->productUnitMasters->weight_in_ton;
		$loading_price = $reqData['quantity']* @$setting->loading_unloading_price * $productDetails->productUnitMasters->weight_in_ton;

		$insurance_price = ($total* @$setting->insurance) / 100;	
		$payable_amount = $total+$loading_price+$insurance_price+@$totalCom;
		CartDetail::where(['id' => $cartDetails->id])
		->update([
			'quantity' 			=> $reqData['quantity'],
			'subtotal'			=> $subtotal,
			'total_discount' 	=> $total_discount,
			'total'				=> $total,
			'loading_price'			=> $loading_price,
			'insurance_price'			=> $insurance_price,
			'payable_amount'			=> $payable_amount,
			'seller_commission' => @$totalCom
		]);
	}

	/**
	* Method: getProduct
	* Description: This method is used to get product details.
	* Author: Sanjoy
	*/
	private function getProduct($productId = NULL) {
		return $this->product->where(['id' => $productId])->first();
	}

	/**
	* Method: removeCart 
	* Description: This method is used to remove item from cart.
	* Author: Sanjoy
	*/
	/**
    * @OA\Get(
    * path="/api/remove-cart/{cartDetailsId}",
    * summary="Remove Cart",
    * description="Remove Cart",
    * operationId="authLogin",
    * tags={"Alaaddin: Remove Cart"},
    * security={{"bearer_token":{}}},
  	*      @OA\Parameter(
    *         name="cartDetailsId",
    *         in="path",
    *         description="cart details id",
    *         required=true,
    *      ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
	public function removeCart($cartDetailsId) {
		$response = [
			'jsonrpc' => '2.0'
		];
		try{
			$cartDetail = CartDetail::where(['id' => $cartDetailsId])->first();
			if(!@$cartDetail) {
				$response['error']['message'] = __('customer_api.invalid_request');
				return response()->json($response);
			}

			$cartMaster = CartMaster::where(['id' => $cartDetail->cart_master_id])->first();
			CartDetail::where(['id' => $cartDetailsId])->delete();

			$allDetails = CartDetail::where(['cart_master_id' => $cartMaster->id])->count();

			if(@$allDetails) {
				$this->calculateCart($cartMaster);
			} else {
				CartMaster::where(['id' => $cartMaster->id])->delete();
			}
			$response['result']['message'] = trans('success.-4067');
		} catch(\Exception $e) {
			$response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
			// $response['error']['message'] = $e->getMessage();
		}
		return response()->json($response);
	}
}
