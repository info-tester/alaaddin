<?php

namespace App\Http\Controllers\Api\Modules\WalletDetails;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\OrderMaster;
use App\Models\OrderDetail;
use App\Models\Payment;
use App\Models\Wallet;
use App\Models\OrderSeller;
use Illuminate\Validation\Rule;
use JWTFactory;
use JWTAuth;
use Validator;
use Mail;
use Auth;

class WalletDetailsController extends Controller
{
    
    public function customerwalletHistory(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = auth()->user();
            $reqdata = $request->json()->all();
            
            $validator = Validator::make($reqdata['params'],[
                'page'    => 'required|numeric',
            ],
            [
                'page.required'=>'page required',
                'page.numeric'=>'page should be number'
            ]);

            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }

            if(@$user->status == 'I'){
                $response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
            }

            if(@$user->status == 'D'){
                $response['error'] =__('errors.-33085');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = "please login again to continue";
                return response()->json($response,401);
            }

            $response['result']['wallet_amount'] = @$user->wallet_amount;

            $page = @$request['params']['page']?$request['params']['page']:1;
            $limit = 2;
            if($page == 1) {
                $start = 0;
            } else {
                $start = (($page -1) * $limit);
            }

            $wallet = Wallet::where(['user_id'=>$user->id]);
            $response['result']['wallet_count'] =$wallet->count();
            $response['result']['wallet_page_count'] =ceil($wallet->count() /$limit);
            $response['result']['wallet_per_page'] = $limit;
            $wallet = $wallet->orderBy('id','desc')->skip($start)->take($limit)->select('user_id','order_no','amount','type','description','w_date')->get();
            
            $response['result']['wallet'] = $wallet;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            $response['error'] =__('errors.-33085');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
}
