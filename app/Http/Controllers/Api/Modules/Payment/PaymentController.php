<?php

namespace App\Http\Controllers\Api\Modules\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

#repository
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\UserRepository;
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductRepository;

#models
use App\Models\Payment;
use App\Models\Setting;
use App\Models\MerchantRegId;
use App\Models\UserReward;
use App\Models\OrderMaster;
use App\Models\OrderDetail;
use App\Models\OrderSeller;
use App\Driver;


use App\Mail\OrderMail;
use App\Mail\SendOrderDetailsMerchantMail;
use App\User;

use Validator;
use Mail;
use Session;
# use whatsapp controller
use App\Http\Controllers\Modules\WhatsApp\WhatsAppController;

class PaymentController extends Controller
{
    protected $orderMaster, $orderDetails, $orderSeller, $user, $cart, $merchant, $productVariant, $product;
    protected $orderId = NULL;
    protected $userId = NULL;
    protected $deviceId = NULL;

    #whatsapp send message
    protected $whatsApp;

    public function __construct(OrderMasterRepository $orderMaster, 
                                OrderDetailRepository $orderDetails,
                                OrderSellerRepository $orderSeller,
                                UserRepository $user,
                                CartMasterRepository $cart,
                                CartDetailRepository $cartDetails,
                                MerchantRepository $merchant,
                                ProductVariantRepository $productVariant,
                                ProductRepository $product,
                                WhatsAppController $whatsApp
                            ) {
        $this->orderMaster    = $orderMaster;
        $this->orderDetails   = $orderDetails;
        $this->orderSeller    = $orderSeller;
        $this->user           = $user;
        $this->cart           = $cart;
        $this->cartDetails    = $cartDetails;
        $this->merchant       = $merchant;
        $this->whatsApp       = $whatsApp;
        $this->productVariant = $productVariant;
        $this->product        = $product;
    }

    /**
    * Method: makePayment
    * Description: This method is used to update all tables after payment is failed or success.
    * Author: Sanjoy
    */
    /**
    * @OA\Post(
    * path="/api/make-payment",
    * summary="Customer make payment",
    * description="Customer make payment",
    * operationId="makePayment",
    * tags={"Alaaddin: Customer make payment"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer make payment",
    *    @OA\JsonContent(
    *       required={"order_id","device_id","txn_id","invoice_data","response_data","payment_status"},
    *           @OA\Property(property="params", type="object",
    *           @OA\Property(property="order_id", type="integer", format="order_id", example="1"),
    *           @OA\Property(property="device_id", type="string", format="device_id", example="21214"),
    *           @OA\Property(property="txn_id", type="string", format="txn_id", example="123123123"),
    *           @OA\Property(property="invoice_data", type="string", format="invoice_data", example="[]"),
    *           @OA\Property(property="response_data", type="string", format="response_data", example="[]"),
    *           @OA\Property(property="payment_status", type="string", format="payment_status", example="S"),
    * ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function makePayment(Request $request) {
        $response = [
            'jsonrpc' => '2.0'
        ];

        try {

            $reqData = $request->json('params');
            $validator = Validator::make($reqData,[
                'payment_status'    => 'required|in:S,F',
                'txn_id'    => 'required',
            ],
            [
                'payment_status.required'=>'payment_status required',
                'txn_id.required'=>'txn_id required',
                'payment_status.in'=>'payment_status either S or F',
            ]);

            $this->orderId = @$reqData['order_id'];
            
            $order = $this->orderMaster
            ->with([
                'orderMasterDetails.productByLanguage',
                'customerDetails'
            ])
            ->where(['id' => @$reqData['order_id'], 'payment_method' => 'O'])->whereIn('status',['I','F'])->first();
            if(!@$order) {
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = "Error";
                $response['error']['message'] = __('customer_api.invalid_request');
                
                return response()->json($response);
            }
            $paymentinfo = Payment::where(['order_id' => @$reqData['order_id']])->whereIn('status',['I','F'])->first();
            if(!@$paymentinfo) {
                $response['error']['message'] = __('customer_api.invalid_request');
                return response()->json($response);
            }else{
                if(!@$paymentinfo->payment_initated_response){
                    $response['error'] =__('errors.-5001');
                    $response['error']['message'] = "Error";
                    $response['error']['meaning'] = "Please call place order api first to generate razorpay order id as you informed !";
                    return response()->json($response);    
                }
            }
            $this->userId = $order->user_id;
            $user = User::where(['id' => $this->userId])->first();
            if(!@$user) {
                $response['error']['message'] = __('customer_api.invalid_request');
                return response()->json($response);
            }
            
            # need device id to clear cart for guest user.
            
            if(@$reqData['device_id']) {
                $this->deviceId = $reqData['device_id'];
            }else{
                $this->deviceId = $user->id;
            }

            $order = $this->orderMaster
            ->with([
                'orderMasterDetails.productByLanguage',
                'customerDetails'
            ])
            ->where(['id' => $this->orderId, 'user_id' => $this->userId, 'payment_method' => 'O', 'status' => 'I'])
            ->first();
            if(!@$order) {
                $response['error']['message'] = __('customer_api.invalid_request');
                return response()->json($response);
            }

            $params = [
                'invoice_data'  => @$reqData['invoice_data']
            ];
            $this->updatePayment($params, $this->orderId);

            # update stattus when payment is success
            if(@$reqData['payment_status'] == 'S') {
                $params = [
                    'response'  => $reqData['response_data'],
                    'txn_id'    => $reqData['txn_id'],
                    'status'    => 'S'
                ];
                $this->updatePayment($params, $this->orderId);

                # update order status
                

                
                $ordDetl = $this->orderDetails->where(['order_master_id' => $this->orderId])->get()->pluck('seller_id');
                $orderData['status'] = 'INP';
                $orderData['is_paid'] = 'Y';
                $orderData['last_status_date'] = date('Y-m-d H:i:s');
                $this->orderMaster->where(['id' => $this->orderId])->update($orderData);
                $this->clearCart($user);
                # calculating loyalty point for COD
                // if(@$user->user_type == 'C') {
                //     $this->deductLoyaltyPoint($this->orderId);
                // }

                #drcrease stock(quantity) when order placed
                // $this->decreaseStock($this->orderId);
                
                # sending whatsapp message
                // $this->whatsApp->sendMessageMerchants($this->orderId);

                # send push notificatio to all drivers
                // $this->sendNotification($this->orderId);

                # send mail to customer and seller 
                try{
                    $this->sendCustomerAndSellerMail($this->orderId,$user);
                }catch(\Exception $e){
                    
                }
                $response['result'] = __('payment.-723');
                $response['result']['payment_method'] = 'O';
                $response['result']['meaning'] = "You have successfully placed the order";
                $response['result']['message'] = "Success";
                $response['result']['status'] = @$reqData['payment_status'];

            } else {
                $params = [
                    'response'  => @$reqData['response_data'],
                    'status'    => 'F'
                ];
                $this->updatePayment($params, $this->orderId);
                $this->orderMaster->where(['id' => $this->orderId])->update(['status' => 'F']);
                $response['result'] = __('success_user.-900');
                $response['result']['payment_method'] = 'O';
                $response['result']['status'] = @$reqData['payment_status'];
            }
            return response()->json($response);
        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error']['message'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        } catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            // $response['error']['message'] = $th->getMessage();
            return response()->json($response);
        }
    }

        /*
    * Method: sendNotification
    * Description: This method is used to send Notification to driver
    * Author: Jayatri
    */
    private function sendNotification($order_id){
        // $androidDrivers = Driver::where(['status' => 'A', 'device_type' => 'A'])->pluck('firebase_reg_no');

        // $iosDrivers = Driver::where(['status' => 'A', 'device_type' => 'I'])->pluck('firebase_reg_no');
        // $order_dtt = OrderMaster::where('id',$order_id)->first();
        // # send notification to android drivers
        // if(count($androidDrivers)) {
        //     $msg                      = array();
        //     $msg['title']             = 'Order No:'." ".$order_dtt->order_no;
        //     $msg["body"]              = $order_dtt->shipping_fname." placed order recently!";
        //     // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
        //     $msg['link_type']         = "Y";
        //     $msg['is_linked']         = "Y";
        //     $msg['image']             = '';
        //     $msg['soundname']         = 'pick';
        //     $msg['content-available'] = '1';
        //     $msg["info"]              = "";
        //     $msg["priority"]          = "2";
        //     $msg["volume"]           = "10";
        //     $headers = array(
        //         'Authorization: key=' . env('FIREBASE_KEY'),
        //         'Content-Type: application/json',
        //     );
        //     $fields = array(
        //         'registration_ids' => $androidDrivers,
        //         'data'             => $msg
        //     );
        //     // Initializing Curl
        //     $ch = curl_init();
        //     // Posting data to the following URL
        //     curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

        //     // Post Data = True, Defining Headers and SSL Verifier = false
        //     curl_setopt($ch, CURLOPT_POST, true);
        //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //     // Posting fields array in json format
        //     curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        //     // Executing Curl
        //     $result = curl_exec($ch);
            
        //     // Closing Curl
        //     curl_close($ch);
        // }
        
        // # send notification to ios drivers
        // if(count($iosDrivers)) {
        //     $msg                      = array();
        //     $msg['title']             = "New Order Placed";
        //     $msg['soundname']         = 'pick';
        //     $msg['content-available'] = '1';
        //     $msg["info"]              = "";
        //     $msg["priority"]          = "2";
        //     $msg["volume"]            = "10";
        //     $msg["mutable-content"]   = true;
        //     // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
        //     $msg["body"]              = 'Recently customer placed an order! ';

        //     $data['message'] = "New Order Placed";
        //     $data['type'] = "1";
        //     $headers = array(
        //         'Authorization: key=' . env('FIREBASE_KEY'),
        //         'Content-Type: application/json',
        //     );
        //     $fields = array(
        //         'category'                  => "CustomSamplePush",
        //         'content_available'         => true,
        //         'mutable_content'           => true,
        //         'registration_ids'          => $iosDrivers,
        //         'notification'              => $msg,
        //         'data'                      => $data
        //     );
        //     // Initializing Curl
        //     $ch = curl_init();
        //     // Posting data to the following URL
        //     curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

        //     // Post Data = True, Defining Headers and SSL Verifier = false
        //     curl_setopt($ch, CURLOPT_POST, true);
        //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //     // Posting fields array in json format
        //     curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        //     // Executing Curl
        //     $result = curl_exec($ch);
        //     // Closing Curl
        //     curl_close($ch);
        // }
    }

    // /*
    // * Method: sendNotificationDashboardUpdate 
    // * Description : This method is used to update dashboard automatically specialy merchant dashboard client requirement using web notification (ajax call)
    // * Author: Jayatri
    // */
    private function sendNotificationDashboardUpdate($orderId, $merchantId) {
    // For Notification sending
        // dd($merchantId);
        $order_dtt = OrderMaster::where('id',$orderId)->first();
        $new_registrationIds = MerchantRegId::where(['user_id' => $merchantId])->pluck('reg_id')->toArray();
        $idCount = MerchantRegId::where(['user_id' => $merchantId])->pluck('reg_id')->Count();
        // dd(@$idCount);
        if($idCount != 0){
            if(@$order_dtt) {
            $username = "Testing ...";
            $click_action = route('merchant.dashboard');
            $title = $username." Recently customer placed an order!";
            $message = "New order Placed !";
            // dd($new_registrationIds);
            $fields = array (
                'registration_ids' => $new_registrationIds,
                'data' => array (
                    "message"       => $message,
                    "title"         => $title,
                    "image"         => url('firebase-logo.png'),
                    "click_action"  => $click_action,
                ),
                'notification'      => array (
                    "body"          => $message,
                    "title"         => $title,
                    "click_action"  => $click_action,
                    "icon"          => "url('firebase-logo.png')",
                )
            );
            // dd($fields);
            $fields = json_encode ( $fields );
            $API_ACCESS_KEY = 'AAAAcQGyZzs:APA91bEst24UvPwO2UpmnRh6OUH_Y_On82ZRIm1ekOSKcekCuOc1m2yYR2w6CGDt09nBY8dmL5h_8C5Cs_KMgIPd1m0mZ8oMUDWz75KaDJmJeNOkTFklIS-hYgWYA3qHnbO-zb76jNK-';
            $headers = array(
                'Authorization: key=' . $API_ACCESS_KEY,
                'Content-Type: application/json',
            );
            $ch = curl_init ();
            curl_setopt ( $ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt ( $ch, CURLOPT_POST, true );
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
            $result = curl_exec ( $ch );
            curl_close ( $ch );
            // dd($result);
            return $result.' New Order placed! ';
            }
        }

    // End of Notification sending
    }
    /*
    * Method: decreaseStock
    * Description: This method is used to descrease Stock
    * Author: Jayatri
    */
    private function decreaseStock($orderId) {   
        $details = $this->orderDetails->where('order_master_id', @$orderId)->get();
        foreach($details as $dtl) {
            if($dtl->product_variant_id != 0) {
                $variant = $this->productVariant->where('id',$dtl->product_variant_id)->first();
                if($variant->stock_quantity >= $dtl->quantity) {
                    $updatestock['stock_quantity'] = $variant->stock_quantity - $dtl->quantity;
                    $this->productVariant->where('id',$dtl->product_variant_id)->update($updatestock);
                }
            } else {
                if($dtl->product_id != 0) {
                    $product = $this->product->where('id',@$dtl->product_id)->first();    
                    if($product->stock >= $dtl->quantity){
                        $stock_update['stock'] = $product->stock - $dtl->quantity;
                        $this->product->where('id',@$dtl->product_id)->update($stock_update);
                    }
                }
            }   
        }
    }
    /*
    * Method: sendCustomerAndSellerMail
    * Description: This method is used to customer and seller mail
    * Author: Jayatri
    */
    private function sendCustomerAndSellerMail($orderId,$user) {
        try{

        $data = [];
        $data['order'] = $order = OrderMaster::where(['id' => $orderId])
        ->with([
            'shippingAddress.getCountry',
            'billingAddress.getCountry',
            'getCountry'
        ])->first();

        $data['orderDetails'] = OrderDetail::where(['order_master_id' => $orderId])
        ->with([
            'sellerDetails',
            'defaultImage',
            'productByLanguage',
            'productVariantDetails'
        ])
        ->get();

        $contactList = [];
        $i=0;
        $user = $this->user->whereId($order->user_id)->first();
        $data['fname']          = $user->fname;
        $data['lname']          = $user->lname;  
        $data['email']          = $user->email;
        $data['user_type']      = $user->user_type;
        $data['shipping_price'] = @$order->shipping_price;
        $data['orderNo']        = @$order->order_no;
        $data['language_id']    = getLanguage()->id;
        $data['bcc']            = $contactList;
        $data['sub_total']      = @$order->subtotal;
        $data['order_total']    = @$order->order_total;
        $data['total_discount'] = @$order->total_discount;
        if(@$order->status == 'INP') {
            $data['subject'] = "Your order has been placed successfully! Order no: ".@$order->order_no;
        } else {
            $data['subject'] = "Order Details! Order No:".@$order->order_no." ! Order Details!";
        }
        
        #send mail admin to notify for new order only as client requirement (internal order)
        $i=0;
        if(@$order->status =='N'){
            $setting = Setting::first();
            if(@$setting->email_1) {
                $contactList[$i] = $setting->email_1;
                $i++;
            }
            if(@$setting->email_2) {
                $contactList[$i] = $setting->email_2;
                $i++;
            }
            if(@$setting->email_3) {
                $contactList[$i] = $setting->email_3;
                $i++;
            }
        }
        $data['bcc'] = $contactList;
        #send to customer mail(order status can be new/driver assigned)
        Mail::send(new OrderMail($data));
        
        # send mail order details to seller mail
        $dtls_mail = OrderDetail::where(['order_master_id' => $order->id])->groupBy('seller_id')->get();
        
        foreach(@$dtls_mail as $contact) {
            $contactListEmail1 = [];
            $contactListEmail2 = [];
            $contactListEmail3 = [];
            $contactList = [];
            $data['bcc'] = [];
            $data['orderDetails'] = [];
            $i = 0;
            $data['user_type'] = 'C';
            $data['hide_customer_info'] = $contact->sellerDetails->hide_customer_info;

            $data['orderDetails'] = OrderDetail::where(['order_master_id' => $order->id,'seller_id'=>@$contact->seller_id])
            ->with([
                'sellerDetails',
                'defaultImage',
                'productByLanguage',
                'productVariantDetails'
            ])
            ->get();

            $data['sub_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
            $data['order_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
            $data['total_discount'] = OrderSeller::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total_discount');
            $data['shipping_price'] = "NA";
            if(@$order->status == 'N') {
                $data['subject'] = "New Order Placed! Order No : ".@$order->order_no." ! Order Details!";
            } else {
                $data['subject'] = "Order is Accepted! Order No : ".@$order->order_no." ! Order Details!";    
            }
            
            $contactList[$i] = $contact->sellerDetails->email;
            $i++;
            // if(@$order->status =='DA') {
            //     if($contact->sellerDetails->email1) {
            //         $contactListEmail1[$i] = $contact->sellerDetails->email1;
            //         $i++;
            //     }
            //     if($contact->sellerDetails->email2) {
            //         $contactListEmail2[$i] = $contact->sellerDetails->email2;
            //         $i++;
            //     }
            //     if($contact->sellerDetails->email3) {
            //         $contactListEmail3[$i] = $contact->sellerDetails->email3;
            //         $i++;
            //     }    
            // }
            $contactList = array_unique($contactList);
            if(@$contactListEmail1 != '') {
                $contactListEmail1 = array_unique($contactListEmail1);
                $contactList = array_merge($contactList, $contactListEmail1);
            }
            if(@$contactListEmail2 != '') {
                $contactListEmail2 = array_unique($contactListEmail2);
                $contactList = array_merge($contactList, $contactListEmail2);
            }
            if(@$contactListEmail3 != '') {
                $contactListEmail3 = array_unique($contactListEmail3);
                $contactList = array_merge($contactList, $contactListEmail3);
            }
            $data['bcc'] = $contactList;
            Mail::send(new SendOrderDetailsMerchantMail($data));
            // $this->sendNotificationDashboardUpdate($contact->order_master_id,$contact->seller_id);
        }

        }catch(\Exception $e){
            
        }
    }

    /**
    * Method: calculateLoyaltyPoint
    * Description: This method is used to deduct loyalty point from customer account.
    * Author: Sanjoy
    */
    private function deductLoyaltyPoint($orderId) {
        $setting = Setting::first();
        $order = $this->orderMaster->whereId($orderId)->first();
        if(@$order->loyalty_point_used > 0) {  
            $totalOrder = $order->loyalty_point_used*$setting->one_point_to_kwd;
            
            # insert to user reward table
            $addUserReward = new UserReward;
            $addUserReward['user_id']        = @$this->userId;
            $addUserReward['order_id']       = $orderId;
            $addUserReward['earning_points'] = $order->loyalty_point_used;
            $addUserReward['debit']          = '1';
            $addUserReward->save();

            # update customer total reward
            $this->user->whereId(@$this->userId)->decrement('loyalty_balance', $order->loyalty_point_used);
            $this->user->whereId(@$this->userId)->increment('loyalty_used', $order->loyalty_point_used);
        }
    }

    /**
    * Method: calculateLoyaltyPoint
    * Description: This method is used to calculate loyalty point
    * Author: Sanjoy
    */
    private function calculateLoyaltyPoint($request, $order) {
        $setting = Setting::first();
        $loyaltyPoint = $order->order_total*$setting->one_kwd_to_point;
        $totalOrder = $loyaltyPoint*$setting->one_point_to_kwd;
        $this->orderMaster->whereId($order->id)->increment('loyalty_point_received', $loyaltyPoint);
        $this->user->whereId(@$this->userId)->increment('loyalty_balance', $loyaltyPoint);
        $this->user->whereId(@$this->userId)->increment('loyalty_total', $loyaltyPoint);

        $addUserReward = new UserReward;
        $addUserReward['user_id']        = @$this->userId;
        $addUserReward['order_id']       = $order->id;
        $addUserReward['earning_points'] = $loyaltyPoint;
        $addUserReward['credit']         = '1';
        $addUserReward->save();

        if(@$request['reward_point']) {
            if(@$request['reward_point'] == 'Y') {  
                $totalOrder = $request['loyalty_point']*$setting->one_point_to_kwd;
                $addUserReward = new UserReward;
                $addUserReward['user_id']        = @$this->userId;
                $addUserReward['order_id']       = $order->id;
                $addUserReward['earning_points'] = $request['loyalty_point'];
                $addUserReward['debit']          = '1';
                $addUserReward->save();
                $this->user->whereId(@$this->userId)->decrement('loyalty_balance', $request['loyalty_point']);
                $this->user->whereId(@$this->userId)->increment('loyalty_used', $request['loyalty_point']);
                $this->orderMaster->whereId($order->id)->increment('loyalty_point_used', $request['loyalty_point']);
                $this->orderMaster->whereId($order->id)->decrement('order_total', number_format($totalOrder, 3));
                $this->orderMaster->whereId($order->id)->update(['loyalty_amount' => number_format($totalOrder, 3)]);
            }
        }
    }

    /**
    * Method: updatePayment
    * Description: This method is used to update payment table after made payment success or failed.
    * Author: Sanjoy Mandal
    */
    private function updatePayment($params = [], $orderId) {
        Payment::where(['order_id' => $orderId])->update($params);
    }

    /**
    * Method: clearCart
    * Description: This method is used to clear cart
    * Author: Sanjoy
    */
    private function clearCart($user) {
        
        if(@$user->user_type == 'C') {
            $cart = $this->cart->where('user_id', $user->id)->first();
            if($cart) {
                $this->cartDetails->where('cart_master_id', $cart->id)->delete(); 
                $this->cart->where('id', $cart->id)->delete();
            }
        } else {
            $cart = $this->cart->where('session_id', $this->deviceId)->first();
            if($cart) {
                $this->cartDetails->where('cart_master_id', $cart->id)->delete(); 
                $this->cart->where('id', $cart->id)->delete();
            }
        }
    }
}
