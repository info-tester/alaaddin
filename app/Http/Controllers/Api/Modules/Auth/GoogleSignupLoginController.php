<?php

namespace App\Http\Controllers\Api\Modules\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
#models
use App\User;
use App\Merchant;
use App\Models\UserRegId;
use App\Models\Setting;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Hash;
use Auth;

class GoogleSignupLoginController extends Controller
{
    /**
    * @OA\Post(
    * path="/api/customer-login-via-google-step-1",
    * summary="Customer Login Via Google Step 1",
    * description="Customer Google Login Or Sign Up",
    * operationId="customerLoginViaGoogleStep1",
    * tags={" Alaaddin : Customer Login Via Google Step 1"},
    * @OA\RequestBody(
    *    required=true,
    *    description=" Customer Login Via Google Step 1 ",
    *    @OA\JsonContent(
    *       required={"name","phone","email","google_id"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="name", type="string", format="name", example="Fname Lname"),
    *               @OA\Property(property="email", type="string", format="email", example="user1@gmail.com"),
    *               @OA\Property(property="phone", type="string", format="email", example="1234567898"),
    *               @OA\Property(property="image", type="string", format="image", example="https://lh3.googleusercontent.com/a/AATXAJwjjdObnv4weZNG8W1OVKJnJRaFD-xyBQPbG3Vf=s96-c"),
    *               @OA\Property(property="google_id", type="string", format="google_id", example="435352352523532352"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description=" Customer Login Via Google Step 1 ",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, Please try again")
    *        )
    *     )
    * )
    */
    public function customerLoginViaGoogleStep1(Request $request) {
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqData = $request->json()->all();
            #if google id or email is null then 
            if(!@$reqData['params']['google_id'] || !@$reqData['params']['email']) {
                $response['error'] = ERRORS['-33029'];
                return response()->json($response);
            }

            $user = User::where(['google_id' => $reqData['params']['google_id']])->where('status', '!=', 'D')->first();
            
            if(@$reqData['params']['email'] == "buyer@gmail.com"){
                    
                $otp = 100000;
                
            }else{

                $otp = mt_rand(100000, 999999);

            }

            #if previously registered using google then login the user.
            if(@$user) {
                if($user->status == 'A' || $user->status == 'U') {
                    
                    User::where('id', $user->id)->update(['login_via_otp'=>$otp,'is_phone_verified'=>'N']);

                    $user = User::where(['google_id' => $reqData['params']['google_id']])->where('status', '!=', 'D')->first();
                    $phnno = $user->phone;
                    $message = "Mobile verify OTP is ".$otp." - ALLAAD";
                    $smsr = sendSms($phnno,$message);
                    $response['result'] = __('success.-4074');
                    $response['result']['login_via_otp'] = $otp;
                    $response['result']['phone'] = $user->phone;
                    $response['result']['is_phone_verified'] = $user->is_phone_verified;
                    return response()->json($response);
                    // $is_phone = User::where('phone', @$reqData['params']['phone'])->whereNotIn('status',['D'])->count();
                    // if($is_phone>0){
                    //     $response['error'] = ERRORS['-33088'];
                    //     return response()->json($response);
                    // }
                    $response['error'] = ERRORS['-33000'];
                    return response()->json($response);
                } else {
                    $response['error'] = ERRORS['-33086'];
                    return response()->json($response);
                }
            } else {
                # if not matched with google id then check using email.
                $user = User::where(['email' => $reqData['params']['email']])->first();
                if(@$user) {
                    if($user->status == 'A' || $user->status == 'U') {
                        
                        User::where('id', $user->id)->update(['login_via_otp'=>$otp,'is_phone_verified'=>'N']);
                        
                        $user = User::where(['google_id' => $reqData['params']['google_id']])->where('status', '!=', 'D')->first();

                        $phnno = $user->phone;
                        $message = "Mobile verify OTP is ".$otp." - ALLAAD";
                        $smsr = sendSms($phnno,$message);
                        $response['result'] = __('success.-4074');
                        $response['result']['login_via_otp'] = $otp;
                        $response['result']['phone'] = $user->phone;
                        $response['result']['is_phone_verified'] = $user->is_phone_verified;
                        return response()->json($response);
                        // $is_phone = User::where('phone', @$reqData['params']['phone'])->whereNotIn('status',['D'])->count();
                        // if($is_phone>0){
                        //     $response['error'] = ERRORS['-33088'];
                        //     return response()->json($response);
                        // }
                        $response['error'] = ERRORS['-33000'];
                        return response()->json($response);
                    } else if($user->status == 'I') {
                        $response['error'] = ERRORS['-33003'];
                        return response()->json($response);
                    } else if($user->status == 'D') {
                        $response['error'] = ERRORS['-33005'];
                        return response()->json($response);
                    }
                } else {
                    $validator = Validator::make($reqData['params'],[
                        'google_id'    => 'required',
                        'name'    => 'required|string|max:255',
                        'email'    => 'required|string|email|max:255|regex:/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/',
                        'phone'   => 'required|numeric|digits_between:10,12'
                    ],
                    [
                        'google_id.required'=>'google_id is required',
                        'name.required'=>'name is required',
                        'name.string'=>'name should be string',
                        'name.max'=>'name should be 255 charecters',
                        'email.required'=>'Please provide valid email id',
                        'email.email'=>'Please provide valid email-id',
                        'email.regex'=>'Please provide valid email-id',
                        'email.max'=>'email should be 255 charecters',
                        'phone.required'=>'phone number is required',
                        'phone.numeric'=>'phone number should be number',
                        'phone.digits_between'=>'phone number should be 10 digits to maximum 12 digits'
                    ]);
                    if($validator->fails()){
                        $response['error']['message'] = 'Error';
                        $response['error']['meaning'] = $validator->errors()->first();
                        return response()->json($response);
                    }
                    $is_phone = User::where('phone', @$reqData['params']['phone'])->whereNotIn('status',['D'])->count();
                    if($is_phone>0){
                        $response['error'] = ERRORS['-33088'];
                        return response()->json($response);
                    }
                    $is_email = User::where('email', @$reqData['params']['email'])->whereNotIn('status',['D'])->count();
                    if($is_email>0){
                        $response['error'] = ERRORS['-33000'];
                        return response()->json($response);
                    }
                    # new user, need to register
                    $name = explode(' ', $reqData['params']['name']);
                    if(@$reqData['params']['image']) {
                        $image = rand(11111,99999).time().'.jpg';
                        # save file to folder
                        $ch = curl_init();
                        curl_setopt ($ch, CURLOPT_URL, $reqData['params']['image']);
                        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
                        $data = curl_exec($ch);
                        curl_close($ch);
                        $file = fopen('storage/app/public/customer/profile_pics/'.$image, 'w+');
                        fputs($file, $data);
                        fclose($file);
                    }
                    $params = [
                        'fname'         => @$name[0],
                        'lname'         => @$name[1],
                        'email'         => $reqData['params']['email'],
                        'phone'         => $reqData['params']['phone'],
                        'google_id'     => $reqData['params']['google_id'],
                        'email_vcode'     => $otp,
                        'login_via_otp'     => $otp,
                        'is_phone_verified'     => 'N',
                        'status'        => 'U',
                        'signup_from'   => 'G',
                        'user_type'     => 'C', #customer
                        'provider'      => 'google',
                        'image'         =>  @$image
                    ];
                    $user = User::create($params);
                    $phnno = $user->phone;
                    $message = "Mobile verify OTP is ".$otp." - ALLAAD";
                    $smsr = sendSms($phnno,$message);
                    $response['result'] = __('success.-4074');
                    $response['result']['phone'] = $user->phone;
                    $response['result']['login_via_otp'] = $user->login_via_otp;
                    $response['result']['is_phone_verified'] = $user->is_phone_verified;
                    
                }
            }
            
            
            return response()->json($response);
        } catch (\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/check-customer-info-by-google-id",
    * summary="check-customer-info-by-google-id",
    * description="check customer info by google id",
    * operationId="checkcustomerinfobygoogleid",
    * tags={" Alaaddin : check customer info by google id"},
    * @OA\RequestBody(
    *    required=true,
    *    description=" check customer info by google id ",
    *    @OA\JsonContent(
    *       required={"google_id"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="google_id", type="string", format="google_id", example="435352352523532352"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description=" check-customer-info-by-google-id ",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, Please try again")
    *        )
    *     )
    * )
    */
    public function checkcustomerinfobygoogleid(Request $request) {
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqData = $request->json()->all();
            
            if(!@$reqData['params']['google_id']) {
                $response['error'] = ERRORS['-33029'];
                return response()->json($response);
            }
            $user = User::where(['google_id' => $reqData['params']['google_id']])->where('status', '!=', 'D')->first();

            if(@$user) {
                if(@$user->status == 'I'){
                    $response['error'] = ERRORS['-33039'];
                    $response['error']['meaning'] = "Admin inactive you! ";    
                }else{
                    $response['result']['is_phone_verified'] = $user->is_phone_verified;
                    if(@$user->is_phone_verified == 'Y'){
                        $response['result']['user'] = $user;    
                        $response['result']['token'] = JWTAuth::fromUser($user);
                    }
                }
                
            }else{
                $response['result']['is_phone_verified'] = 'N';
                
            }
            return response()->json($response);
        } catch (\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/customer-verify-otp",
    * summary="customer verify otp",
    * description="customer verify otp",
    * operationId="customerVerifyOtp",
    * tags={"Alaaddin: customer verify otp"},
    * @OA\RequestBody(
    *    required=true,
    *    description=" customer verify otp ",
    *    @OA\JsonContent(
    *       required={"phone","login_via_otp"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="phone", type="string", example="9876543211"),
    *               @OA\Property(property="login_via_otp", type="integer", format="login_via_otp", example="111111"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong  response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, Please try again")
    *        )
    *     )
    * )
    */
    public function customerVerifyOtp(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'], [
                'phone' => 'required',
                'login_via_otp' => 'required|max:6'
            ],
            [
                'phone.required'                =>' phone number is required',
                'login_via_otp.required'                =>'login_via_otp is required',
                'login_via_otp.max'                =>'login_via_otp can be maximum 6'
            ]);
            if($validator->fails()){
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $userData = User::whereNotIn('status',['D'])->where(['login_via_otp' => $reqdata['params']['login_via_otp']]);
            $userData = $userData->where(function($q) use ($reqdata){
                $q->where('phone',$reqdata['params']['phone']);
            });
            $userData = $userData->first();
            if(@$userData){
                
                User::where(['id'=>$userData->id])->update(['is_phone_verified' => 'Y', 'login_via_otp' => NULL, 'status' => 'A']);
                
                $userData = User::where(['id'=>$userData->id])->select('is_phone_verified','id','bank_name','account_name','account_number','ifsc_code','branch_name','fname','lname','phone','email','email1','gender','date_of_birth','login_from','image','login_via_otp','tmp_email','address','landmark','city','city_id','state','zipcode','country','facebook_id','google_id','apple_id','provider','signup_from','loyalty_balance','loyalty_total','loyalty_used','email_verified_at','email_vcode','is_email_verified','status','user_type','firebase_reg_no','device_type','remember_token','tag','wallet_amount','created_at','updated_at','password')->first();
                $response['result'] = __('success.-5007');
                $response['result']['userdata'] = $userData;
                $response['result']['token'] = JWTAuth::fromUser($userData);
                $response['result']['user'] = $userData;

                return response()->json($response);
            } else {
                $userData = User::whereNotIn('status',['D']);
                $userData = $userData->where(function($q) use ($reqdata){
                    $q->where('phone',$reqdata['params']['phone']);
                });
                $userData = $userData->first();
                if(@$userData){
                    $response['error'] = ERRORS['-33039'];
                    
                }else{
                    $response['error'] = ERRORS['-33039'];
                    $response['error']['meaning'] = "This ".$reqdata['params']['phone']." is not registered with us!";    
                }
                return response()->json($response);
            }
        } catch (\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return Response::json($response);
        }
    }

}
