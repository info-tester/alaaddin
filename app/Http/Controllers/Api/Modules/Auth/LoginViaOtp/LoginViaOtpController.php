<?php

namespace App\Http\Controllers\Api\Modules\Auth\LoginViaOtp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\EmailVerifyMail;
use App\User;

use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Mail;

use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Models\CartMaster;
use App\Models\CartDetail;
use App\Models\UserRegId;

use App\Mail\SendMailOtp;


#models
use App\Driver;

use Hash;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginViaOtpController extends Controller
{

    /**
    * @OA\Post(
    * path="/api/userverifyLoginViaOtp",
    * summary="Customer userverifyLoginViaOtp",
    * description="Customer userverifyLoginViaOtp",
    * operationId="userverifyLoginViaOtp",
    * tags={"Alaaddin: Customer userverifyLoginViaOtp"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer userverifyLoginViaOtp",
    *    @OA\JsonContent(
    *       required={"email_or_phone","login_via_otp"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="email_or_phone", type="string", example="user1@mail.com"),
    *               @OA\Property(property="login_via_otp", type="integer", format="login_via_otp", example="111111"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong  response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or phone number. Please try again")
    *        )
    *     )
    * )
    */
    public function userverifyLoginViaOtp(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'], [
                'email_or_phone' => 'required',
                'login_via_otp' => 'required|max:6'
            ],
            [
                'email_or_phone.required'                =>'Email-id or phone number is required',
                'login_via_otp.required'                =>'login_via_otp is required',
                'login_via_otp.max'                =>'login_via_otp can be maximum 6'
            ]);
            if($validator->fails()){
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $userData = User::whereNotIn('status',['D'])->where(['login_via_otp' => $reqdata['params']['login_via_otp']]);
            $userData = $userData->where(function($q) use ($reqdata){
                $q->where('email',$reqdata['params']['email_or_phone'])->orWhere('phone',$reqdata['params']['email_or_phone']);
            });
            $userData = $userData->first();
            if(@$userData){
                // if(is_numeric($reqdata['params']['email_or_phone'])){
                    User::where(['id'=>$userData->id])->update(['is_phone_verified' => 'Y', 'login_via_otp' => NULL, 'status' => 'A']);
                // }else{
                    // User::where(['id'=>$userData->id])->update(['is_email_verified' => 'Y', 'login_via_otp' => NULL, 'status' => 'A']);
                // }
                $userData = User::where(['id'=>$userData->id])->select('id','bank_name','account_name','account_number','ifsc_code','branch_name','fname','lname','phone','email','email1','gender','date_of_birth','login_from','image','login_via_otp','tmp_email','address','landmark','city','city_id','state','zipcode','country','facebook_id','google_id','apple_id','provider','signup_from','loyalty_balance','loyalty_total','loyalty_used','email_verified_at','email_vcode','is_email_verified','status','user_type','firebase_reg_no','device_type','remember_token','tag','wallet_amount','created_at','updated_at','password')->first();
                $response['result'] = __('success.-5007');
                $response['result']['userdata'] = $userData;
                $response['result']['token'] = JWTAuth::fromUser($userData);

                return response()->json($response);
            } else {
                $userData = User::whereNotIn('status',['D']);
                $userData = $userData->where(function($q) use ($reqdata){
                    $q->where('email',$reqdata['params']['email_or_phone'])->orWhere('phone',$reqdata['params']['email_or_phone']);
                });
                $userData = $userData->first();
                if(@$userData){
                    $response['error'] = ERRORS['-33039'];
                    
                }else{
                    $response['error'] = ERRORS['-33039'];
                    $response['error']['meaning'] = "This ".$reqdata['params']['email_or_phone']." is not registered with us!";    
                }
                return response()->json($response);
            }
        } catch (\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return Response::json($response);
        }
    }
    //
    /**
    * @OA\Post(
    * path="/api/resend-verify-otp-by-login-via-otp",
    * summary="Customer userResendOtpVerifyByLogin",
    * description="Customer userResendOtpVerifyByLogin",
    * operationId="userResendOtpVerifyByLogin",
    * tags={"Alaaddin: Customer userResendOtpVerifyByLogin"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer userResendOtpVerifyByLogin",
    *    @OA\JsonContent(
    *       required={"email_or_phone"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="email_or_phone", type="string", example="user1@mail.com"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong ",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or phone number . Please try again")
    *        )
    *     )
    * )
    */
    public function userResendOtpVerifyByLogin(Request $request)
    {
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqData = $request->json()->all();
            $this->sessionId = @$reqData['params']['device_id'];

            if(is_numeric(@$reqData['params']['email_or_phone'])){
                $validator = Validator::make(@$reqData['params'], [
                    'email_or_phone'    => 'required|numeric|digits_between:10,12'
                ],[
                    'email_or_phone.digits_between' => 'The phone must be between 10 and 12 digits.',
                    'email.numeric' => 'The phone must be between 10 and 12 digits.',
                    'email_or_phone.required' => 'The phone must be between 10 and 12 digits.'
                ]);
                $field = "phone";
            }
            else{
                $validator = Validator::make(@$reqData['params'], [
                    'email_or_phone'    => 'required|email|regex:/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/'
                ],[
                    'email_or_phone.required' => 'Please provide email-id',
                    'email_or_phone.email' => 'Please provide valid email-id',
                    'email.regex' => 'Please provide valid email-id'
                ]);
                $field = "email";
            }
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            if($field == 'phone'){
                $credentials['phone'] = $reqData['params']['email_or_phone'];
            }
            else{
                $credentials['email'] = $reqData['params']['email_or_phone'];
            }
            // $credentials['password'] = $reqData['params']['password'];


            $userData = User::whereIn('status', ['A','U','I']);
            $userData = $userData->where(function($q) use ($reqData){
                $q->where('email',$reqData['params']['email_or_phone'])->orWhere('phone',$reqData['params']['email_or_phone']);
            });
            $userData = $userData->first();
            if(@$userData){
                if(@$reqData['params']['email_or_phone'] == "buyer@gmail.com"){
                    $login_via_otp = 100000;
                }else{
                    $login_via_otp = mt_rand(100000, 999999);

                }
                // if($userData->status == 'U'){
                //     $response['status'] = 'U';
                //     $response['error'] = ERRORS['-33077'];
                //     return response()->json($response);
                // }
                if($userData->status == 'I'){
                    $response['status'] = 'I';
                    $response['error'] = ERRORS['-33090'];
                    return response()->json($response);
                }
                $credentials['status'] = 'A';
                User::where('id', $userData->id)->update(['login_via_otp'=>$login_via_otp]);
                // if(is_numeric($reqData['params']['email_or_phone'])){
                    $phnno = $userData->phone;
                    $message = "Your login OTP is ".$login_via_otp." - ALLAAD";
                    $smsr = sendSms($phnno,$message);
                // }else{
                //     try{
                //         $mailDetails = new \StdClass();
                //         $mailDetails->to    = @$userData->email;
                //         $mailDetails->fname = @$userData->fname;
                //         $mailDetails->id    = @$userData->id;
                //         $mailDetails->login_via_otp = @$userData->login_via_otp;
                //         $mailDetails->user = @$userData;
                //         Mail::send(new SendMailOtp($mailDetails));
                //     }catch(\Exception $e){

                //     }
                // }
                $response['result'] = __('success.-4074');
                $response['result']['meaning'] = "Otp has been sent to ".$userData->phone;
                $response['result']['login_via_otp'] = $login_via_otp;
                $response['result']['sendsms'] = @$smsr;

                return response()->json($response);
            }else{
                $response['error'] = ERRORS['-33009'];
                return response()->json($response);
            }
        } catch (\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return response()->json($response);
        }
    }
}
