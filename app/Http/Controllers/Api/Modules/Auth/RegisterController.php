<?php
namespace App\Http\Controllers\Api\Modules\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\EmailVerifyMail;
use App\User;
use App\Mail\SendMailOtp;

use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Mail;
class RegisterController extends Controller
{
    /**
    *@ Method: register
    *@ Description: For user registration
    *@ Author: Surajit
    */
    /**
    * @OA\Post(
    * path="/api/register",
    * summary="Customer Sign in",
    * description="Register by customer details",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Register"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer credentials",
    *    @OA\JsonContent(
    *       required={"fname","lname","phone","email","password"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="fname", type="string", format="name", example="user fname"),
    *               @OA\Property(property="lname", type="string", format="name", example="user lname"),
    *               @OA\Property(property="phone", type="integer", format="phone", example="1234567890"),
    *               @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function register(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'],[
                'fname'    => 'required|string|max:255',
                'lname'    => 'required|string|max:255',
                'email'    => 'required|string|email|max:255|regex:/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/',
                'phone'   => 'required|numeric|digits_between:10,12'
            ],
            [
                'fname.required'=>'First name required',
                'fname.string'=>'First name should be string',
                'fname.max'=>'First name should be 255 charecters',
                'lname.required'=>'Last name required',
                'lname.max'=>'Last name maximum 255 charecters',
                'lname.string'=>'Last name should be string',
                'email.required'=>'Please provide valid email id',
                'email.email'=>'Please provide valid email-id',
                'email.regex'=>'Please provide valid email-id',
                'email.max'=>'Email-id should be 255 charecters',
                'phone.required'=>'Phone number is required',
                'phone.numeric'=>'Phone number should be number',
                'phone.digits_between'=>'Phone number should be 10 digits to maximum 12 digits'
            ]);
            if($validator->fails()){
                 // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $is_email = User::where('email', @$reqdata['params']['email'])->whereNotIn('status',['D'])->count();
            if($is_email>0){
                $response['error'] = ERRORS['-33000'];
                return response()->json($response);
            }
            $is_phone = User::where('phone', @$reqdata['params']['phone'])->whereNotIn('status',['D'])->count();
            if($is_phone>0){
                $response['error'] = ERRORS['-33088'];
                return response()->json($response);
            }
            $otp = mt_rand(100000,999999);
            // $name = $this->splitName($reqdata['params']['name']);
            // dd($name);
            $user = User::create([
                'fname' => @$reqdata['params']['fname'],
                'lname' => @$reqdata['params']['lname'],
                'email' => $reqdata['params']['email'],
                'phone' => $reqdata['params']['phone'],
                'email_vcode' => $otp,
                'login_via_otp' => $otp,
                'status' => 'U'
                // 'password' => \Hash::make($reqdata['params']['password']),
            ]);
            $phnno = $user->phone;
                $message = "Mobile verify OTP is ".$otp." - ALLAAD";
                $smsr = sendSms($phnno,$message);
            try{
                $mailDetails = new \StdClass();
                $mailDetails->to = $reqdata['params']['email'];
                $mailDetails->fname = $reqdata['params']['fname'].' '.@$reqdata['params']['lname'];
                $mailDetails->vcode = $otp;
                $mailDetails->login_via_otp = $otp;
                $mailDetails->id = $user->id;
                // Mail::send(new EmailVerifyMail($mailDetails));
            }catch(\Exception $e){
                \Log::error($e->getMessage());
            }

                $response['result']['code'] = "4074";    
                $response['result']['message'] = "Success!";    
                $response['result']['meaning'] = "You have successfully registerd!Otp has been sent to ".$user->phone;    
                $response['result']['sendsms'] = @$smsr;    
            // }
            //mail send end
            $response['result']['email'] = $reqdata['params']['email'];
            $response['result']['phone'] = $reqdata['params']['phone'];
            $response['result']['otp'] = $otp;
            return response()->json($response);
        } catch (\Exception $e) {
            $response['error'] = ERRORS['-33039'];
            $response['error']['message'] = $e->getMessage();
            $response['error']['meaning'] = $e->getMessage();
            \Log::error($e->getMessage());
            return Response::json($response);
        }
    }

    /**
    *@ Method: emailVerify
    *@ Description: For user email verification
    *@ Author: Surajit
    */
    /**
    * @OA\Post(
    * path="/api/emailverify",
    * summary="Customer email verify",
    * description="Customer email verify",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer email verify"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer credentials",
    *    @OA\JsonContent(
    *       required={"email","vcode"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *               @OA\Property(property="vcode", type="integer", format="vcode", example="111111"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function emailVerify(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqdata = $request->json()->all();
            $validator = Validator::make($reqdata['params'], [
                'email' => 'required|email|regex:/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/',
                'vcode' => 'required|max:6'
            ],[
                    'email.required'                =>'Email-id is required',
                    'email.email'                =>'Please provide valid email-id',
                    'email.regex'                =>'Please provide valid email-id',
                    'vcode.required'                =>'Vcode is required',
                    'vcode.max'                =>'Vcode can be maximum 6'
                ]);
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $userData = User::select('id','email','fname','lname','status', 'is_email_verified','phone','login_via_otp')
            ->where(['email'=> $reqdata['params']['email'],'email_vcode' => $reqdata['params']['vcode']])
            ->where('status', '!=', 'D')
            ->first();
            if(@$userData){
                User::where(['email'=>$reqdata['params']['email'],'email_vcode' => $reqdata['params']['vcode']])
                ->where('status', '!=', 'D')
                ->update(['is_email_verified' => 'Y', 'email_vcode' => NULL, 'status' => 'A']);

                $userData = User::select('id','email','fname','lname','status', 'is_email_verified','login_via_otp','phone')
                ->where(['email'=> $reqdata['params']['email']])
                ->where('status', 'A')
                ->first();
                $response['result']['userdata'] = $userData;
                return response()->json($response);
            } else {
                $userData = User::select('id','email','fname','lname','status', 'is_email_verified')
                ->where(['email'=> $reqdata['params']['email']])
                ->where('status', '!=', 'D')
                ->first();
                if(@$userData){
                    $response['error'] = ERRORS['-33039'];
                    $response['error']['meaning'] = "Wrong OTP verification code or this email-id is already verified!";
                }else{
                    $response['error'] = ERRORS['-33039'];
                    $response['error']['meaning'] = "This email-id is not registered with us!";    
                }
                return response()->json($response);
            }
        } catch (\Exception $e) {
            $response['error'] = ERRORS['-33009'];
            $response['error']['message'] = $e->getMessage();
            $response['error']['meaning'] = $e->getMessage();
            \Log::error($e->getMessage());
            return Response::json($response);
        }
    }

    /**
    *@ Method: resendVcode
    *@ Description: For resend email otp
    *@ Author: Surajit
    */
    /**
    * @OA\Post(
    * path="/api/resend-code",
    * summary="Customer resend code",
    * description="Customer resend code",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer resend code"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer credentials",
    *    @OA\JsonContent(
    *       required={"email"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function resendVcode(Request $request)
    {
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqdata = $request->json()->all();
            if(@$reqdata['params']['email']==null) {
                $response['error'] = ERRORS['-33008'];
                return Response::json($response);
            }
            $getVcode = User::where(['email'=>$reqdata['params']['email']])->whereIn('status', ['A','I','U'])->first();

            if(@$getVcode) {
                if(@$getVcode->email_vcode) {
                    $otp = $getVcode->email_vcode;
                } else {
                    $otp = mt_rand(100000, 999999);
                }
                User::where('email', $getVcode->email)->update(['email_vcode' => $otp]);

                $mailDetails = new \StdClass();
                $mailDetails->to    = $getVcode->email;
                $mailDetails->fname = @$getVcode->fname.' '.@$getVcode->lname;
                $mailDetails->id    = $getVcode->id;
                $mailDetails->vcode = $otp;
                $mailDetails->mailBody = 'forget_password';
                Mail::send(new EmailVerifyMail($mailDetails));
                $response['result'] = ERRORS['-33079'];
                $response['result']['vcode']   = $otp;
            } else {
                $response['error'] = ERRORS['-33087'];
            }
            return Response::json($response);
        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $exception) {
            \Log::error($exception->getMessage());
            $response['error'] = ERRORS['-33009'];
            $response['error']['meaning'] = $exception->getMessage();
            return Response::json($response);
        }
    }

    //split fullname
    function splitName($name) {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
        return array($first_name, $last_name);
    }
}
