<?php

namespace App\Http\Controllers\Api\Modules\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Driver;
use App\User;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Hash;
use Auth;

class DriverLoginController extends Controller
{
    public function __construct(
        Request $request
   	)
    {

        # set guard for driver guard
        \Config::set('auth.defaults.guard', 'driver');
    }
    /**
    *@ Class: login
    *@ Description: for login Driver
    *@ Author: Surajit
    */
    public function loginDriver(Request $request){
        auth()->shouldUse('api');
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqData = $request->json()->all();
            $validator = Validator::make(@$reqData['params'], [
                'email'    => 'required|email',
                'password' => 'required'
            ]);
            if($validator->fails()){
                $response['error'] = $validator->errors();
                return response()->json($response);
            }
            $credentials['email'] = $reqData['params']['email'];
            $credentials['password'] = $reqData['params']['password'];


            $userData = Driver::where('email', $reqData['params']['email'])->where('status', 'A')->first();
                            // dd($userData);
            if(@$userData){

                if($userData->status == 'U'){
                    $response['result']['status'] = 'U';
                    // $response['result']['message'] = ERRORS['-33077'];
                    $response['result'] = __('errors.-33077');
                    // $response['error'] = __('errors.-5067');
                    return response()->json($response);
                }
                if($userData->status == 'I'){
                    $response['result']['status'] = 'I';
                    // $response['result']['message'] = ERRORS['-33034'];
                    // $response['error'] = __('errors.-33034');
                    $response['error'] = __('errors.-5068');
                    return response()->json($response);
                }
                if($userData->status == 'D'){
                    $response['result']['status'] = 'D';
                    $response['error'] = __('errors.-5067');
                    return response()->json($response);
                }
                $credentials['status'] = 'A';
                if(!$token = JWTAuth::attempt($credentials)){
                    // $response['error'] = ERRORS['-33009'];
                    $response['error'] = __('errors.-33009');
                    return response()->json($response);
                }
                if(@$userData) {
                    $updateUser['firebase_reg_no'] = @$reqData['params']['firebase_reg_no'];
                    $updateUser['device_type'] = @$reqData['params']['device_type'];
                    $updateUserDetails = Driver::where('id',$userData->id)->update($updateUser);
                }
                $response['result']['status'] = 'A';
                $response['result']['userdata'] = $this->getAuthUser($token);
                $response['result']['token'] = $token;
                // $response['result']['token_data'] = $this->getAuthUser($token);
                return response()->json($response);
            }else{
                // $response['error'] = ERRORS['-33009'];
                $response['error'] = __('errors.-33009');
                return response()->json($response);
            }
        } catch (\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return response()->json($response);
        }
    }
    private function getAuthUser($token)
    {
    	// auth()->shouldUse('api');
        $user = JWTAuth::authenticate($token);
        return  $user;
    }
    /**
    *@ Class: logoutDriver
    *@ Description: for logout Driver
    *@ Author: Surajit
    */
    public function logoutDriver(Request $request) {
        $response = [
            'jsonrpc' => '2.0'
        ];
        // auth()->shouldUse('api');
        try {
            // JWTAuth::invalidate(JWTAuth::getToken());

            $user = JWTAuth::toUser();
            // if($user->status == 'A'){
            // $response['result']['message'] = 'Driver logged out successfully';
                $response['result'] = __('success.-4072');
        	// }else{
        		// $response['error'] = __('errors.-5067');
        	// }
            return response()->json($response);
        } catch (JWTException $exception) {
            // $response['error']['message'] = 'Sorry, the driver cannot be logged out';
            $response['error'] = __('errors.-5070');
            return response()->json($response);
        }
    }
}
