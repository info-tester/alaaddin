<?php
namespace App\Http\Controllers\Api\Modules\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\EmailVerifyMail;
use App\User;
use App\Driver;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Mail;
class ForgotPasswordController extends Controller {
    /**
     *@ Method: forgotPassword
     *@ Description: forgot Password
     *@ Author: Jayatri
     */
    /**
    * @OA\Post(
    * path="/api/forgot-pass-user",
    * summary="Customer Forgot Password",
    * description="Customer Forgot by email",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Forgot Password"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer credentials",
    *    @OA\JsonContent(
    *       required={"email"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function forgotPassword(Request $request) {
        $response = ['jsonrpc' => '2.0'];
        // try {
            $reqData = $request->json()->all();
            $rules = ['email' => 'required|email'];
            $validator = Validator::make(@$reqData['params'], $rules);
            if ($validator->fails()) {
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $userdata = User::where(['email' => $reqData['params']['email']])->whereIn('status', ['A','I','U'])->first();
            if ($userdata) {
                $otp = mt_rand(100000, 999999);
                User::where('id', $userdata->id)->update(['email_vcode' => $otp]);
                $mailDetails = new \StdClass();
                $mailDetails->to    = $reqData['params']['email'];
                $mailDetails->fname = $userdata->fname;
                $mailDetails->id    = $userdata->id;
                $mailDetails->vcode = $otp;
                $mailDetails->mailBody = 'forget_password';
                Mail::send(new EmailVerifyMail($mailDetails));
                $response['result'] = __('success.-4075');
                $response['result']['vcode'] = $otp;
                $response['result']['req'] = $reqData;
            } else {
                $response['error'] = __('errors.-33018');
                $response['error']['message'] = "Error!";
                $response['error']['meaning'] = "Email-id is not registered with us!";
            }
            return Response::json($response);
        // } catch(\Exception $e) {
        //     $response['error']['message'] = $e->getMessage();
        //     return Response::json($response);
        // }
    }
    /**
     *@ Method: passwordUpdate
     *@ Description: Password Update
     *@ Author: Jayatri
     */
    /**
    * @OA\Post(
    * path="/api/update-pass-user",
    * summary="Customer Update Password",
    * description="Customer Update by otp,email,password",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Update Password"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer credentials",
    *    @OA\JsonContent(
    *       required={"otp","email","password"},
    *           @OA\Property(property="params", type="object",
    
    *           @OA\Property(property="otp", type="integer", format="email", example="111111"),
    *           @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
    *           @OA\Property(property="password", type="string", format="password", example="password"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function passwordUpdate(Request $request) {
        $response = ['jsonrpc' => '2.0'];
        try {
            $reqData = $request->json()->all();
            $rules = [
                'otp' => "required|numeric",
                'email' => 'required|email',
                'password' => 'required'
            ];
            $validator = Validator::make(@$reqData['params'], $rules);
            if ($validator->fails()) {
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $user_details = User::where(["email_vcode" => $reqData['params']['otp']])->where(["email" => $reqData['params']['email']])->whereIn('status', ['A','I','U'])->first();
            if (@$user_details) {
                $update_password['email_vcode'] = null;
                $update_password['password'] = \Hash::make($reqData['params']['password']);
                $update_user_password = User::where('id', $user_details->id)->update($update_password);
                // $response['result'] = ERRORS['-33042'];
                $response['result'] = __('success.-4074');
            } else {
                // $response['error'] = ERRORS['-33029'];
                $response['error'] = __('errors.-33018');
            }
            return Response::json($response);
        }
        catch(\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return Response::json($response);
        }
    }
    /**
     *@ Method: passwordUpdate
     *@ Description: Password Update
     *@ Author: Jayatri
     */
    public function forgotPasswordReset(Request $request) {
        $response = ['jsonrpc' => '2.0'];
        try {
            $reqData = $request->json()->all();
            $rules = ['email' => 'required|email'];
            // dd(@$reqData['params']['email']);
            $validator = Validator::make(@$reqData['params'], $rules);
            if ($validator->fails()) {
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $userdata = Driver::where(['email'=>$reqData['params']['email']])->whereIn('status', ['A','I','U'])->first();
            // dd($user_details);
            if ($userdata) {
                $otp = mt_rand(100000, 999999);
                Driver::where('id', $userdata->id)->update(['email_vcode' => $otp]);

                $mailDetails = new \StdClass();
                $mailDetails->to = $reqData['params']['email'];
                $mailDetails->fname = $userdata->fname;
                $mailDetails->vcode = $otp;
                Mail::send(new EmailVerifyMail($mailDetails));

                // $response['result'] = ERRORS['-33079'];
                $response['result'] = __('errors.-33018');
                $response['result']['vcode'] = $otp;
            } else {
                // $response['error'] = ERRORS['-33018'];
                $response['error'] = __('errors.-33018');
            }
            return Response::json($response);
        }
        catch(\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return Response::json($response);
        }
    }
    /**
     *@ Method: passwordUpdate
     *@ Description: Password Update
     *@ Author: Jayatri
     */
    public function passwordUpdateChange(Request $request) {
        $response = ['jsonrpc' => '2.0'];
        try {
            $reqData = $request->json()->all();
            $rules = [
                'email' => 'required|email',
                'password' => 'required'
            ];
            $validator = Validator::make(@$reqData['params'], $rules);
            if ($validator->fails()) {
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            $user_details = Driver::where(["email" => $reqData['params']['email']])->whereIn('status', ['A','I','U'])->first();
            if (@$user_details) {
                $update_password['email_vcode'] = null;
                $update_password['password'] = \Hash::make($reqData['params']['password']);
                $update_user_password = Driver::where('id', $user_details->id)->update($update_password);
                // $response['result'] = ERRORS['-33042'];
                $response['result'] = __('errors.-33042');
            } else {
                $response['error'] = __('errors.-33042');
                // $response['error'] = ERRORS['-33029'];
            }
            return Response::json($response);
        }
        catch(\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return Response::json($response);
        }
    }
}
