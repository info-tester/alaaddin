<?php

namespace App\Http\Controllers\Api\Modules\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

#models
use App\User;
use App\Merchant;
use App\Models\UserRegId;
use App\Models\Setting;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Hash;
use Auth;

class SocialLoginController extends Controller
{
    /**
    * Method : facebookLogin
    * Description: This method is used to login using facebook
	* Author: Sanjoy
    */
    public function facebookLogin(Request $request) {
    	$response = [
            'jsonrpc' => '2.0'
        ];
        try{
    		$reqData = $request->json()->all();
    		#if facebook id or email is null then 
    		if(!@$reqData['params']['facebook_id'] || !@$reqData['params']['email']) {
    			$response['error'] = ERRORS['-33029'];
    			return response()->json($response);
    		}

    		$user = User::where(['facebook_id' => $reqData['params']['facebook_id']])->first();
    		
    		#if previously registered using facebook then login the user.
    		if(@$user) {
    			if($user->status == 'A') {
    				$response['result']['token'] = JWTAuth::fromUser($user);
    			} else {
    				$response['error'] = ERRORS['-33086'];
    				return response()->json($response);
    			}
    		} else {
    			# if not matched with facebook id then check using email.
    			$user = User::where(['email' => $reqData['params']['email']])->first();
    			if(@$user) {
    				if($user->status == 'A') {
    					$response['result']['token'] = JWTAuth::fromUser($user);
    				} else if($user->status == 'U') {
    					$response['error'] = ERRORS['-33002'];
    					return response()->json($response);
    				} else if($user->status == 'I') {
    					$response['error'] = ERRORS['-33003'];
    					return response()->json($response);
    				} else if($user->status == 'D') {
    					$response['error'] = ERRORS['-33005'];
    					return response()->json($response);
    				}
    			} else {
    				# new user, need to register
    				$name = explode(' ', $reqData['params']['name']);
    				if(@$reqData['params']['image']) {
    					$image = rand(11111,99999).time().'.jpg';
    					# save file to folder
	    				$ch = curl_init();
						curl_setopt ($ch, CURLOPT_URL, $reqData['params']['image']);
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						$data = curl_exec($ch);
						curl_close($ch);
						$file = fopen('storage/app/public/profile_pics/'.$image, 'w+');
						fputs($file, $data);
						fclose($file);
    				}
    				$params = [
    					'fname' 		=> @$name[0],
    					'lname'			=> @$name[1],
    					'email' 		=> $reqData['params']['email'],
    					'facebook_id' 	=> $reqData['params']['facebook_id'],
    					'signup_from'	=> 'F',
    					'status'		=> 'A',
    					'user_type'		=> 'C', #customer
    					'provider'		=> 'facebook',
    					'image'			=> 	@$image
    				];
    				$user = User::create($params);
    				$response['result']['token'] = JWTAuth::fromUser($user);
    			}
    		}
            if(@$reqData['params']['firebase_reg_no'] && @$reqData['params']['device_type']){
                $updateUser['firebase_reg_no'] = @$reqData['params']['firebase_reg_no'];
                $updateUser['device_type'] = @$reqData['params']['device_type'];    
				User::where('id',@$user->id)->update($updateUser);
				
				$updateReg['reg_id'] = @$reqData['params']['firebase_reg_no'];
				$updateReg['type'] = @$reqData['params']['device_type'];
				$updateReg['user_id'] = @$user->id;

				$regIds = UserRegId::where(['reg_id' => @$reqData['params']['firebase_reg_no']])->first();

				if(!@$regIds) {
					UserRegId::create($updateReg);
				}else{
					UserRegId::where(['reg_id' => @$reqData['params']['firebase_reg_no']])->update($updateReg);
				}
            }
    		$response['result']['user'] = $user;
    		return response()->json($response);
        } catch (\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return response()->json($response);
        }
    }

    
	/**
    * Method : googleLogin
    * Description: This method is used to login using google
	* Author: Sanjoy
    */
    // this function is not required as there is new api implemented
    public function googleLogin(Request $request) {
    	$response = [
            'jsonrpc' => '2.0'
        ];
    	try {
    		$reqData = $request->json()->all();
    		#if google id or email is null then 
    		if(!@$reqData['params']['google_id'] || !@$reqData['params']['email']) {
    			$response['error'] = ERRORS['-33029'];
    			return response()->json($response);
    		}

    		$user = User::where(['google_id' => $reqData['params']['google_id']])->where('status', '!=', 'D')->first();
    		
    		#if previously registered using google then login the user.
    		if(@$user) {
    			if($user->status == 'A') {
    				$response['result']['token'] = JWTAuth::fromUser($user);
    			} else {
    				$response['error'] = ERRORS['-33086'];
    				return response()->json($response);
    			}
    		} else {
    			# if not matched with google id then check using email.
    			$user = User::where(['email' => $reqData['params']['email']])->first();
    			if(@$user) {
    				if($user->status == 'A') {
    					$response['result']['token'] = JWTAuth::fromUser($user);
    				} else if($user->status == 'U') {
    					$response['error'] = ERRORS['-33002'];
    					return response()->json($response);
    				} else if($user->status == 'I') {
    					$response['error'] = ERRORS['-33003'];
    					return response()->json($response);
    				} else if($user->status == 'D') {
    					$response['error'] = ERRORS['-33005'];
    					return response()->json($response);
    				}
    			} else {
    				# new user, need to register
    				$name = explode(' ', $reqData['params']['name']);
    				if(@$reqData['params']['image']) {
    					$image = rand(11111,99999).time().'.jpg';
    					# save file to folder
	    				$ch = curl_init();
						curl_setopt ($ch, CURLOPT_URL, $reqData['params']['image']);
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						$data = curl_exec($ch);
						curl_close($ch);
						$file = fopen('storage/app/public/customer/profile_pics/'.$image, 'w+');
						fputs($file, $data);
						fclose($file);
    				}
    				$params = [
    					'fname' 		=> @$name[0],
    					'lname'			=> @$name[1],
    					'email' 		=> $reqData['params']['email'],
    					'google_id' 	=> $reqData['params']['google_id'],
    					'status'		=> 'A',
    					'signup_from'	=> 'G',
    					'user_type'		=> 'C', #customer
    					'provider'		=> 'google',
    					'image'			=> 	@$image
    				];
    				$user = User::create($params);
    				$response['result']['token'] = JWTAuth::fromUser($user);
    			}
    		}
            if(@$reqData['params']['firebase_reg_no'] && @$reqData['params']['device_type']){
                $updateUser['firebase_reg_no'] = @$reqData['params']['firebase_reg_no'];
                $updateUser['device_type'] = @$reqData['params']['device_type'];    
				User::where('id',@$user->id)->update($updateUser);
				
				$updateReg['reg_id'] = @$reqData['params']['firebase_reg_no'];
				$updateReg['type'] = @$reqData['params']['device_type'];
				$updateReg['user_id'] = @$user->id;

				$regIds = UserRegId::where(['reg_id' => @$reqData['params']['firebase_reg_no']])->first();

				if(!@$regIds) {
					UserRegId::create($updateReg);
				}else{
					UserRegId::where(['reg_id' => @$reqData['params']['firebase_reg_no']])->update($updateReg);
				}
            }
    		$response['result']['user'] = $user;
    		return response()->json($response);
    	} catch (\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return response()->json($response);
        }
    }

    /**
    * Method : googleLogin
    * Description: This method is used to login using google
    * Author: Sanjoy
    */
    public function appleLogin(Request $request) {
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqData = $request->json()->all();
            #if google id or email is null then 
            if(!@$reqData['params']['apple_id'] && !@$reqData['params']['email']) {
                $response['error'] = ERRORS['-33029'];
                return response()->json($response);
            }

            $user = User::where(['apple_id' => $reqData['params']['apple_id']])->where('status', '!=', 'D')->first();
            
            #if previously registered using google then login the user.
            if(@$user) {
                if($user->status == 'A') {
                    $response['result']['token'] = JWTAuth::fromUser($user);
                } else {
                    $response['error'] = ERRORS['-33086'];
                    return response()->json($response);
                }
            } else {
                # if not matched with google id then check using email.
                $user = User::where(['email' => $reqData['params']['email']])->where('status', '!=', 'D')->first();
                if(@$user) {
                    if($user->status == 'A') {
                        $response['result']['token'] = JWTAuth::fromUser($user);
                    } else if($user->status == 'U') {
                        $response['error'] = ERRORS['-33002'];
                        return response()->json($response);
                    } else if($user->status == 'I') {
                        $response['error'] = ERRORS['-33003'];
                        return response()->json($response);
                    } else if($user->status == 'D') {
                        $response['error'] = ERRORS['-33005'];
                        return response()->json($response);
                    }
                } else {
                    # new user, need to register
                    $name = explode(' ', $reqData['params']['name']);
                    if(@$reqData['params']['image']) {
                        $image = rand(11111,99999).time().'.jpg';
                        # save file to folder
                        $ch = curl_init();
                        curl_setopt ($ch, CURLOPT_URL, $reqData['params']['image']);
                        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
                        $data = curl_exec($ch);
                        curl_close($ch);
                        $file = fopen('storage/app/public/profile_pics/'.$image, 'w+');
                        fputs($file, $data);
                        fclose($file);
                    }
                    $params = [
                        'fname'         => @$name[0],
                        'lname'         => @$name[1],
                        'email'         => $reqData['params']['email'],
                        'apple_id'      => $reqData['params']['apple_id'],
                        'status'        => 'A',
                        'signup_from'   => 'A',
                        'user_type'     => 'C', #customer
                        'provider'      => 'apple',
                        'image'         => @$image
                    ];
                    $user = User::create($params);
                    $response['result']['token'] = JWTAuth::fromUser($user);
                }
            }
            if(@$reqData['params']['firebase_reg_no'] && @$reqData['params']['device_type']){
                $updateUser['firebase_reg_no'] = @$reqData['params']['firebase_reg_no'];
                $updateUser['device_type'] = @$reqData['params']['device_type'];    
				User::where('id',@$user->id)->update($updateUser);
				
				$updateReg['reg_id'] = @$reqData['params']['firebase_reg_no'];
				$updateReg['type'] = @$reqData['params']['device_type'];
				$updateReg['user_id'] = @$user->id;

				$regIds = UserRegId::where(['reg_id' => @$reqData['params']['firebase_reg_no']])->first();

				if(!@$regIds) {
					UserRegId::create($updateReg);
				}else{
					UserRegId::where(['reg_id' => @$reqData['params']['firebase_reg_no']])->update($updateReg);
				}
            }
            $response['result']['user'] = $user;
            return response()->json($response);
        } catch (\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return response()->json($response);
        }
    }
    // this function is not required as there is new api implemented
    public function MerchantgoogleLogin(Request $request) {
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            auth()->shouldUse('merchant');
            $reqData = $request->json()->all();
            #if google id or email is null then 
            if(!@$reqData['params']['google_id'] || !@$reqData['params']['email']) {
                $response['error'] = ERRORS['-33029'];
                return response()->json($response);
            }

            $user = Merchant::where(['google_id' => $reqData['params']['google_id']])->where('status', '!=', 'D')->first();
            
            #if previously registered using google then login the user.
            if(@$user) {
                if($user->status == 'A') {
                    $response['result']['token'] = JWTAuth::fromUser($user);
                } else {
                    $response['error'] = ERRORS['-33086'];
                    return response()->json($response);
                }
            } else {
                # if not matched with google id then check using email.
                $user = Merchant::where(['email' => $reqData['params']['email']])->where('status', '!=', 'D')->first();
                if(@$user) {
                    if($user->status == 'A') {
                        $response['result']['token'] = JWTAuth::fromUser($user);
                    } else if($user->status == 'U') {
                        $response['error'] = ERRORS['-33002'];
                        return response()->json($response);
                    } else if($user->status == 'AA') {
                        $response['error'] = ERRORS['-33089'];
                        return response()->json($response);
                    } else if($user->status == 'I') {
                        $response['error'] = ERRORS['-33003'];
                        return response()->json($response);
                    } else if($user->status == 'D') {
                        $response['error'] = ERRORS['-33005'];
                        return response()->json($response);
                    }
                } else {
                    # new user, need to register
                    $name = explode(' ', $reqData['params']['name']);
                    if(@$reqData['params']['image']) {
                        $image = rand(11111,99999).time().'.jpg';
                        # save file to folder
                        $ch = curl_init();
                        curl_setopt ($ch, CURLOPT_URL, $reqData['params']['image']);
                        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
                        $data = curl_exec($ch);
                        curl_close($ch);
                        $file = fopen('storage/app/public/customer/profile_pics/'.$image, 'w+');
                        fputs($file, $data);
                        fclose($file);
                    }
                    $setting = Setting::first();
                    $params = [
                        'fname'         => @$name[0],
                        'lname'         => @$name[1],
                        'email'         => $reqData['params']['email'],
                        'google_id'     => $reqData['params']['google_id'],
                        'status'        => 'AA',
                        'commission'        => @$setting->commission,
                        'image'         =>  @$image,
                        'country'       => '101'
                    ];
                    $user = Merchant::create($params);
                    $fname = str_slug(@$user->fname);
                    $lname = str_slug(@$user->lname);
                    $slug = $fname.'-'.$lname;
                    $slug = $slug.'-'.$user->id;
                    Merchant::whereId($user->id)->update(['slug' => $slug]);
                    
                    if($user->status == 'A') {
                        $response['result']['token'] = JWTAuth::fromUser($user);
                    } else if($user->status == 'U') {
                        $response['error'] = ERRORS['-33002'];
                        return response()->json($response);
                    } else if($user->status == 'AA') {
                        $response['error'] = ERRORS['-33089'];
                        return response()->json($response);
                    } else if($user->status == 'I') {
                        $response['error'] = ERRORS['-33003'];
                        return response()->json($response);
                    } else if($user->status == 'D') {
                        $response['error'] = ERRORS['-33005'];
                        return response()->json($response);
                    }
                    $response['result']['token'] = JWTAuth::fromUser($user);
                }
            }
            $user = Merchant::where('id',@$user->id)->first();
            $response['result']['user'] = $user;
            return response()->json($response);
        } catch (\Exception $e) {
            $response['error']['message'] = $e->getMessage();
            return response()->json($response);
        }
    }
}
