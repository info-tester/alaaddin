<?php
namespace App\Http\Controllers\Api\Modules\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Models\CartMaster;
use App\Models\CartDetail;
use App\Models\UserRegId;
use App\Mail\SendMailOtp;
use Mail;
#models
use App\Driver;
use App\User;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Hash;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class LoginController extends Controller
{
    protected $cartMaster, $cartMaster1, $cartDetails, $cartData, $sessionId;

    public function __construct(
        Request $request,
        CartMasterRepository $cartMaster,
        CartMasterRepository $cartMaster1,
        CartDetailRepository $cartDetails
    )
    {
        $this->cartMaster = $cartMaster;
        $this->cartMaster1 = $cartMaster1;
        $this->cartDetails = $cartDetails;
    }

    /**
    *@ Class: login
    *@ Description: for login user
    *@ Author: Surajit
    */
    /**
    * @OA\Post(
    * path="/api/login",
    * summary="Customer Login",
    * description="Customer Login",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Login"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer credentials",
    *    @OA\JsonContent(
    *       required={"email"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="email", type="string",example="1234567890"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function login(Request $request){
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
            $reqData = $request->json()->all();
            $this->sessionId = @$reqData['params']['device_id'];

            if(is_numeric(@$reqData['params']['email'])){
                $validator = Validator::make(@$reqData['params'], [
                    'email'    => 'required|numeric|digits_between:10,12'
                ],[
                    'email.digits_between' => 'The phone must be between 10 and 12 digits.',
                    'email.numeric' => 'The phone must be between 10 and 12 digits.',
                    'email.required' => 'The phone must be between 10 and 12 digits.'
                ]);
                $field = "phone";
            }
            else{
                $validator = Validator::make(@$reqData['params'], [
                    'email'    => 'required|email|regex:/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/'
                ],[
                    'email.required' => 'Please provide email-id',
                    'email.email' => 'Please provide valid email-id',
                    'email.regex' => 'Please provide valid email-id'
                ]);
                $field = "email";
            }
            if($validator->fails()){
                // $response['error'] = $validator->errors();
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
            if($field == 'phone'){
                $credentials['phone'] = $reqData['params']['email'];
            }
            else{
                $credentials['email'] = $reqData['params']['email'];
            }
            // $credentials['password'] = $reqData['params']['password'];


            $userData = User::whereIn('status', ['A','U','I']);
            $userData = $userData->where(function($q) use ($reqData){
                $q->where('email',$reqData['params']['email'])->orWhere('phone',$reqData['params']['email']);
            });
            $userData = $userData->first();
            if(@$userData){
                if(@$reqData['params']['email'] == "buyer@gmail.com"){
                    
                    $login_via_otp = 100000;
                    
                }else{

                    $login_via_otp = mt_rand(100000, 999999);

                }
                // if($userData->status == 'U'){
                //     $response['status'] = 'U';
                //     $response['error'] = ERRORS['-33077'];
                //     return response()->json($response);
                // }
                if($userData->status == 'I'){
                    $response['status'] = 'I';
                    $response['error'] = ERRORS['-33090'];
                    return response()->json($response);
                }
                $credentials['status'] = 'A';
                User::where('id', $userData->id)->update(['login_via_otp'=>$login_via_otp]);
                
                $userData = User::where('id', $userData->id)->first();
                // if(is_numeric($reqData['params']['email'])){
                    $phnno = $userData->phone;
                    $message = "Your login OTP is ".$login_via_otp." - ALLAAD";
                    $smsr = sendSms($phnno,$message);
                // }else{

                //     try{
                //         $mailDetails = new \StdClass();
                //         $mailDetails->to    = @$userData->email;
                //         $mailDetails->fname = @$userData->fname;
                //         $mailDetails->id    = @$userData->id;
                //         $mailDetails->login_via_otp = @$userData->login_via_otp;
                //         $mailDetails->user = @$userData;
                //         Mail::send(new SendMailOtp($mailDetails));
                //     }catch(\Exception $e){

                //     }
                // }
                if(!$token = JWTAuth::fromUser($userData)){
                // if(!$token = JWTAuth::attempt($credentials)){
                    $response['error'] = ERRORS['-33009'];
                    return response()->json($response);
                }

                $response['result'] = __('success.-4074');
                $response['result']['meaning'] = "Otp has been sent to ".$userData->phone." Please check!";
                $response['result']['status'] = 'A';
                $response['result']['userdata'] = $this->getAuthUser($token);
                $response['result']['token'] = $token;
                $response['result']['login_via_otp'] = $login_via_otp;
                $response['result']['sendsms'] = @$smsr;
                
                
                
                return response()->json($response);
            }else{
                $response['error'] = ERRORS['-33009'];
                return response()->json($response);
            }
        } catch (\Exception $e) {
            $response['error'] = ERRORS['-33009'];
            $response['error']['message'] = $e->getMessage();
            \Log::error($e->getMessage());
            return response()->json($response);
        }
    }

    /**
    * Mehtod: updateCart
    * Description: This method is used to update cart for this user.
    * Author: Sanjoy
    */
    private function updateCart($sessionId, $userId) {
        // if previously added to the cart then merge the cart with pervious and current 
        $existingCart = CartMaster::where(['user_id' => $userId])->first();
        $currentCart = CartMaster::where(['session_id' => $sessionId])->first();
        if($existingCart && @$currentCart) {
            $existingCartDetails = CartDetail::where(['cart_master_id' => $existingCart->id])->get();

            $currenctCartDetails = CartDetail::where(['cart_master_id' => $currentCart->id])->get();
            
            # if already added
            $item = 0;
            if($existingCartDetails->count()) {
                foreach ($existingCartDetails as $row) {
                    foreach ($currenctCartDetails as $key => $value) {
                        # same product trying to add before login and after login
                        if($row->product_id == $value->product_id) {
                            CartDetail::where(['id' => $row->id])->update([
                                'quantity'          => $row->quantity + $value->quantity,
                                'shipping_price'    => $row->shipping_price + $value->shipping_price,
                                'total_discount'    => $row->total_discount + $value->total_discount,
                                'subtotal'          => $row->subtotal + $value->subtotal,
                                'total'             => $row->total + $value->total,
                                'seller_commission' => $row->seller_commission + $value->seller_commission,
                            ]);
                            CartDetail::where(['id' => $value->id])->delete();
                        } else {
                            CartDetail::where(['id' => $value->id])->update([
                                'cart_master_id' => $row->cart_master_id
                            ]);
                            $item += 1;
                        }
                    }
                }
            }

            # update cart master
            CartMaster::where(['user_id' => $userId])->update([
                'total_item'        => $existingCart->total_item + $item,
                'total_qty'         => $existingCart->total_qty + $currentCart->total_qty,
                'total_discount'    => $existingCart->total_discount + $currentCart->total_discount,
                'subtotal'          => $existingCart->subtotal + $currentCart->subtotal,
                'total'             => $existingCart->total + $currentCart->total,
                'total_seller_commission' => $existingCart->total_seller_commission + $currentCart->total_seller_commission
            ]);
            $currentCart = CartMaster::where(['session_id' => $sessionId])->delete();
        } else {
            CartMaster::where(['session_id' => $sessionId ])->update(['user_id' => $userId]);
        }
    }

    private function getAuthUser($token)
    {
        // $user = JWTAuth::authenticate($token);
        $user = JWTAuth::setToken($token)->toUser();
        return  $user;
    }

    /**
    *@ Class: logout
    *@ Description: for logout user
    *@ Author: Surajit
    */
    public function logout(Request $request)
    {
        // auth()->shouldUse('api');

        $response = [
            'jsonrpc' => '2.0'
        ];
        // $token = $request->header('Authorization');

        // $this->validate($request, [
        //     'token' => 'required'
        // ]);

        try {
            // JWTAuth::invalidate($request->token);
            // JWTAuth::invalidate($token);
             // $user = JWTAuth::toUser();   
            // $updateUser['firebase_reg_no'] = NULL;
            // $updateUser['device_type'] = NULL;    
            // User::where('id',$user->id)->update($updateUser);

            JWTAuth::invalidate(JWTAuth::getToken());
            
            $response['result']['message'] = 'User logged out successfully';
            // if(@$reqData['params']['firebase_reg_no'] && @$reqData['params']['device_type']){
                
            // }
            return response()->json($response);
        } catch (JWTException $exception) {
            \Log::error($exception->getMessage());
            $response['error'] = ERRORS['-33009'];
            $response['error']['message'] = 'Sorry, the user cannot be logged out';
            return response()->json($response);
        }
    }

    // /**
    // *@ Class: logoutDriver
    // *@ Description: for logout Driver
    // *@ Author: Surajit
    // */
    // public function logoutDriver(Request $request)
    // {   
    //     // $user = JWTAuth::toUser();
    //     // auth()->shouldUse('api');
    //     // \Config::set('auth.defaults.guard', 'driver');
    //     $response = [
    //         'jsonrpc' => '2.0'
    //     ];

    //     try {
    //         JWTAuth::invalidate(JWTAuth::getToken());

    //         $response['result']['message'] = 'Driver logged out successfully';
    //         return response()->json($response);
    //     } catch (JWTException $exception) {
    //         $response['error']['message'] = 'Sorry, the driver cannot be logged out';
    //         return response()->json($response);
    //     }
    // }
}
