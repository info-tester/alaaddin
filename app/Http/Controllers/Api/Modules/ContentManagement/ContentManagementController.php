<?php

namespace App\Http\Controllers\Api\Modules\ContentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Variant;
use App\Models\Brand;
use App\Models\UserRegId;
use App\Models\OrderDetail;
use App\Models\OrderSeller;
use App\Merchant;
use App\Models\BannerContent;
use App\Models\ContactUsDetails;
use App\Models\ProductVariant;
use App\Models\ProductVariantDetail;
use App\Repositories\BrandRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\UserFavoriteRepository;
use App\Repositories\ProductOtherOptionRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\OrderDetailRepository;
use App\Models\FaqCategoryDetail;
use App\Models\FaqCategoryMaster;
use App\Models\FaqDetail;
use App\Models\FaqMaster;
use App\Models\Help;

use App\Models\PageContentAboutus;

use App\Models\Content;
use App\Models\ContentDetails;


use Auth;

use App\Models\PageContactContent;
use App\Mail\SendMailContactusDetails;
use Mail;
use JWTFactory;
use JWTAuth;
use Validator;
use Session;
use Config;

class ContentManagementController extends Controller
{
    //
    /**
    * @OA\Get(
    * path="/api/view-about-us",
    * summary="About Us",
    * description="About Us",
    * operationId="showAboutUs",
    * tags={"Alaaddin: About Us"},
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function showAboutUs(){
    	$response = ['jsonrpc'=>'2.0'];
    	try 
        {
        	 
    		$about_content = PageContentAboutus::select('second_sec_first_heading','first_sec_desc_who_we_are','first_sec_right_first_image','second_sec_second_heading','third_sec_description','fourth_section_title','fourth_section_first_content','about_us_title','about_us_description')->first();
            $content['image'] = @$about_content->first_sec_right_first_image;
            $content['image_path'] = url('storage/app/public/brand_logo/'.@$about_content->first_sec_right_first_image);
            $content['title'] = @$about_content->second_sec_first_heading;
            $content['description'] = @$about_content->first_sec_desc_who_we_are;
            $content['title_2'] = @$about_content->second_sec_second_heading;
            $content['description_2'] = @$about_content->third_sec_description;
            $content['title_3'] = @$about_content->fourth_section_title;
            $content['description_3'] = @$about_content->fourth_section_first_content;
            $content['about_us_title'] = @$about_content->about_us_title;
            $content['about_us_description'] = @$about_content->about_us_description;
    		$response['result']['about_content'] = $content;
    		return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();

            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Get(
    * path="/api/show-contact-us",
    * summary="Contact Us",
    * description="Contact Us",
    * operationId="authLogin",
    * tags={"Alaaddin: Contact Us"},
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function showContactUs(){
    	$response = ['jsonrpc'=>'2.0'];
    	try 
        {
    	$contact_content = PageContactContent::select('first_sec_second_heading','first_sec_second_content','first_sec_third_heading','first_sec_third_content','facebook_link','twitter_link','linkedin_link','youtube_link')->first();
    	$response['result']['contact_content'] = $contact_content;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    public function showFaq(){
    	$response = ['jsonrpc'=>'2.0'];
    	try 
        {
    	$faqcat = FaqCategoryMaster::with(['categoryDetailsLanguage','faqMasterT.faqDetailsByLanguage'])->get();
		$response['result']['faqcat'] = $faqcat;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/show-details-faq",
    * summary="Faq",
    * description="Faq",
    * operationId="authLogin",
    * tags={"Alaaddin: Faq"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Faq",
    *    @OA\JsonContent(
    *       required={"type"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="type", type="string", format="type", example="S or B"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */

    public function showDetailsFaq(Request $request){
    	$response = ['jsonrpc'=>'2.0'];
    	try 
        {
		$faqdetails = FaqMaster::with(['faqDetailsByLanguage'])->orderBy('display_order','asc');
        if(!empty(@$request['params']['type'])){
            $faqdetails = $faqdetails->where('type',@$request['params']['type']);
        }
        $faqdetails = $faqdetails->get();
		$response['result']['faqdetails'] = $faqdetails;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            // $response['error'] = $th->getMessage();
            return response()->json($response);
        }
    }
    /**
    * @OA\Get(
    * path="/api/show-terms",
    * summary="Terms and Conditions",
    * description="Terms and Conditions",
    * operationId="authLogin",
    * tags={"Alaaddin: Terms and Conditions"},
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function showTerms(){
    	$response = ['jsonrpc'=>'2.0'];
    	try 
        {
    	$term_content =  ContentDetails::select('title','description')->where(['language_id'=> getLanguage()->id,'content_id'=>1])->first();
    	$response['result']['term_content'] = $term_content;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Get(
    * path="/api/show-help",
    * summary="Help Page Content",
    * description="Help Page Content",
    * operationId="authLogin",
    * tags={"Alaaddin: Help Page Content"},
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function showHelp(){
    	$response = ['jsonrpc'=>'2.0'];
    	try 
        {
    	    // $help = ContentDetails::select('title','description')->where(['language_id'=> getLanguage()->id,'content_id'=>3])->first();

            $help = Help::first();

            if(!@$help){
                $response['error'] =__('errors.-5001');
                return response()->json($response);
            }

            $response['result']['title'] = @$help->title;
            $response['result']['description'] = @$help->description;
            $response['result']['help_image'] = @$help->help_image;
            $response['result']['c_title'] = @$help->c_title;
            $response['result']['c_desc'] = @$help->c_desc;
            $response['result']['f_title'] = @$help->f_title;
            $response['result']['f_desc'] = @$help->f_desc;
            $response['result']['email_title'] = @$help->email_title;
            $response['result']['email_desc'] = @$help->email_desc;
            $response['result']['title_1'] = @$help->title_1;
            $response['result']['title_2'] = @$help->title_2;
            $response['result']['title_3'] = @$help->title_3;

            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            // $response['error'] = $th->getMessage();
            return response()->json($response);
        }
    }
    /**
    * @OA\Get(
    * path="/api/show-privacy",
    * summary="Privacy and Policy",
    * description="Privacy and Policy",
    * operationId="authLogin",
    * tags={"Alaaddin: Privacy and Policy"},
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function showPrivacy(){
    	$response = ['jsonrpc'=>'2.0'];
    	try 
        {
    	$privecy_content = ContentDetails::select('title','description')->where(['language_id'=> getLanguage()->id,'content_id'=>2])->first();
    	$response['result']['privecy_content'] = $privecy_content;
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        }
        catch (\Exception $th) {
            // $response['error'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    
    public function invalidateToken($token = NULL) {
        $response = ['jsonrpc'=>'2.0'];
        try{
            JWTAuth::invalidate($token);    
        }  catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error']['message'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response, 400);
        }
        catch (\Exception $th) {
            // $response['error']['message'] = $th->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response, 400);
        }    
    } 

    public function sendNotify(Request $request){
        $response = ['jsonrpc'=>'2.0'];
        try 
        {
            $user = getUserByToken();
            if(@$user){
                $userId = $user->id;
            }
            $reqData = $request->json()->all();
            $reg_id = $reqData['params']['reg_id'];
            $type = $reqData['params']['type'];
            $device_id = $reqData['params']['device_id'];

            $chkDevice = UserRegId::where(['device_id'=>@$reqData['params']['device_id']])->first();
            if(@$chkDevice){
                if(@$reqData['params']['reg_id']){
                    $update['reg_id'] =  $reqData['params']['reg_id'];
                }   
                if(@$reqData['params']['device_id']){
                    $update['device_id'] =  $reqData['params']['device_id'];
                }
                if(@$reqData['params']['type']){
                    $update['type'] =  $reqData['params']['type'];
                }
                if(@$user){
                    $update['user_id'] =  $userId;
                }
                UserRegId::where(['device_id'=>@$reqData['params']['device_id']])->update($update);
            }else{
                if(@$reqData['params']['reg_id']){
                    $new['reg_id'] =  $reqData['params']['reg_id'];
                }   
                if(@$reqData['params']['type']){
                    $new['type'] =  $reqData['params']['type'];
                }
                if(@$reqData['params']['device_id']){
                    $new['device_id'] =  $reqData['params']['device_id'];
                }
                if(@$user){
                    $new['user_id'] =  $userId;
                }else{
                    $new['user_id'] =  0;
                }
                UserRegId::create($new);
            }
            $response['result'] = __('success.-4082');
            $response['result']['app_settings'] = ContactUsDetails::select('ios_version', 'ios_version_check', 'message_for_ios', 'android_version', 'android_version_check', 'message_for_android')->first();

            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error']['message'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response, 400);
        }
        catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            // $response['error']['message'] = $th->getMessage();
            return response()->json($response, 400);
        }    
    }
    /**
    * @OA\Post(
    * path="/api/post-contact-us-form",
    * summary="Contact Us Form Post",
    * description="Contact Us Form Post",
    * operationId="authLogin",
    * tags={"Alaaddin: Contact Us Form Post"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Contact Us Form Post",
    *    @OA\JsonContent(
    *       required={"firstname","lastname","email","phone","message"},
    *           @OA\Property(property="params", type="object",
    *               @OA\Property(property="firstname", type="string", format="firstname", example="firstname"),
    *               @OA\Property(property="lastname", type="string", format="lastname", example="lastname"),
    *               @OA\Property(property="email", type="string", format="email", example="user123@email.com"),
    *               @OA\Property(property="phone", type="integer", format="phone", example="8965325741"),
    *               @OA\Property(property="message", type="string", format="message", example="message"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function postContactUsForm(Request $request){
    	$response = ['jsonrpc'=>'2.0'];
    	try 
        {
        	$reqData = $request->json()->all();
            $validator = Validator::make($reqData['params'],[
                'firstname'    => 'required|string|max:255',
                'lastname'    => 'required|string|max:255',
                'email'    => 'required|string|email|max:255|regex:/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/',
                'phone'    => 'required|numeric|digits_between:10,12',
                'message'    => 'required',
            ],
            [
                'firstname.required'=>'firstname required',
                'lastname.required'=>'lastname required',
                'email.required'=>'email required',
                'email.string'=>'email required',
                'email.required'=>'email required',
                'email.email'=>'email not valid',
                'email.max'=>'email max 255',
                'email.regex'=>'email not valid',
                'phone.required'=>'phone required',
                'phone.digits_between'=>'phone required',
                'phone.numeric'=>'phone required',
                'message.required'=>'message required'
            ]);
            if($validator->fails()){
                $response['error'] =__('errors.-5001');
                $response['error']['message'] = 'Error';
                $response['error']['meaning'] = $validator->errors()->first();
                return response()->json($response);
            }
			// if(@$reqData){
				$firstname	=	"";
				$lastname	=	"";
				$email="";
				$phone="";
				$message="";
				
				if(@$reqData['params']['firstname']){
		            $firstname = $reqData['params']['firstname'];
		        }
		        if(@$reqData['params']['lastname']){
		            $lastname = $reqData['params']['lastname'];
		        }
		        if(@$reqData['params']['email']){
		            $email = $reqData['params']['email'];
		        }
		        if(@$reqData['params']['phone']){
		            $phone = $reqData['params']['phone'];
		        }
		        if(@$reqData['params']['message']){
		            $message = $reqData['params']['message'];
		        }
	        	$customerDetails 			= 	new \stdClass();
	            $customerDetails->name 		= 	@$firstname." ".@$lastname;
	            $customerDetails->email 	=	@$email;
	            $customerDetails->phone 	=	@$phone;
	            $customerDetails->message 	=	@$message;
                try{


	        	Mail::send(new SendMailContactusDetails($customerDetails));
                }
                catch(\Exception $e){
                    
                }
				$response['result']['success'] = __('success.-4076');
	        // }else{
	        	// $response['error'] = __('errors.-5001');
	        // }
        	
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error']['msg'] = $e->getMessage();
            $response['error'] = __('errors.-5001');
            return response()->json($response);
        }
        catch (\Exception $th) {
            
            $response['error'] = __('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            return response()->json($response);
        }
    }
    /**
    * @OA\Post(
    * path="/api/see-all-merchant-reviews",
    * summary="See All merchant reviews",
    * description="See All merchant reviews",
    * operationId="seeAllMerchantReviews",
    * tags={"Alaaddin: See All merchant reviews"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    description="See All merchant reviews",
    *    @OA\JsonContent(
    *       required={"seller_id"},
    *           @OA\Property(property="params", type="object",
    *           @OA\Property(property="seller_id", type="string", example="3"),
    *           @OA\Property(property="page", type="string", format="page", example="1"),
    *    ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function seeAllMerchantReviews(Request $request) {
        $response = ['jsonrpc'=>'2.0'];
        $reqData = $request->json()->all();
        $validator = Validator::make($reqData['params'],[
            'seller_id'    => 'required|numeric'
        ],
        [
            'seller_id.required'=>'Seller id is required'
        ]);
        if($validator->fails()){
            $response['error'] = __('errors.-5099');
            $response['error']['meaning'] = $validator->errors()->first();
            return response()->json($response);
        }

        $seller = Merchant::where(['id'=>$reqData['params']['seller_id']])->first();
        if(!@$seller){
            $response['error'] = __('errors.-5099');
            $response['error']['meaning'] = " You have provided seller id! ";
            return response()->json($response);
        }

        $reviews=OrderDetail::with(['orderMaster.customerDetails'])->where(['seller_id'=>$reqData['params']['seller_id']])->where('rate','>',0);
        $page = @$reqData['params']['page']?$reqData['params']['page']:0; 
        $limit = 20;
        if($page == 1) {
            $start = 0;
        } else {
            $start = (($page -1) * $limit);
        }
        $response['result']['new_total'] =$reviews->count();
        $response['result']['new_page_count'] =ceil($reviews->count() /$limit);
        $response['result']['new_per_page'] =$limit;
        
        
        $reviews = $reviews->orderBy('id','desc')->skip($start)->take($limit)->get();
        $response['result']['reviews'] = @$reviews;
        $response['result']['seller'] =@$seller;
        $response['result']['avg_review'] =$seller->rate;
        $response['result']['total_no_reviews'] =$seller->total_no_review;

        return response()->json($response);
    }
    
}
