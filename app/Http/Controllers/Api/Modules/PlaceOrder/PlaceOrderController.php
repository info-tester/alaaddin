<?php

namespace App\Http\Controllers\Api\Modules\PlaceOrder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// repository
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductVariantDetailRepository;
use App\Repositories\ProductRepository;
use App\Repositories\UserAddressBookRepository;
use App\Repositories\MerchantRepository;

use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\CountryRepository;
use App\Repositories\UserRepository;

use App\Models\Setting;
use App\Models\ShippingCost;
use App\Models\MerchantRegId;
use App\Models\City;
use App\Models\CitiDetails;
use App\Models\UserReward;
use App\Models\Payment;
use App\Models\OrderMaster;
use App\Models\OrderDetail;
use App\Models\OrderSeller;
use App\Models\Product;
use App\Models\Coupon;
use App\Models\CouponToMerchant;
use App\Models\OrderStatusTiming;
use App\Models\PaymentKeyTable;

use App\Merchant;
use App\Driver;
use App\User;
use App\Mail\SendOrderDetailsMerchantMail;
use App\Mail\OrderMail;
use JWTFactory;
use JWTAuth;
use Validator;
use Mail;

# use whatsapp controller
use App\Http\Controllers\Modules\WhatsApp\WhatsAppController;
# use mail (send customer and seller)
// use App\Http\Controllers\Modules\SendMailOrder\SendMailOrderController;

class PlaceOrderController extends Controller
{
	protected $user, $cart, $cartDetails, $product, $productVariant, $productVariantDetail, $addressBook, $orderMaster, $orderDetails, $orderSeller, $merchant, $country;
	protected $userId = NULL;
	protected $deviceId = NULL;
    #whatsapp send message
    protected $whatsApp;
    #mail to customer an
    // protected $sendMail;
    /**
	* __construct
	* This method is used to initilize the instance
	* Author: Sanjoy
	*/
	public function __construct(CartMasterRepository $cart,
								CartDetailRepository $cartDetails,
								ProductVariantRepository $productVariant,
								ProductRepository $product,
								ProductVariantDetailRepository $productVariantDetail,
								UserAddressBookRepository $addressBook,
								OrderMasterRepository $orderMaster,
								OrderDetailRepository $orderDetails,
								OrderSellerRepository $orderSeller,
								MerchantRepository $merchant,
								CountryRepository $country,
								UserRepository $user,
                                WhatsAppController $whatsApp
                                // SendMailOrderController $sendMail
							)
	{
		$this->cart         			= $cart;
		$this->cartDetails  			= $cartDetails;
		$this->productVariant  			= $productVariant;
		$this->productVariantDetail  	= $productVariantDetail;
		$this->product  				= $product;
		$this->addressBook  			= $addressBook;
		$this->orderMaster  			= $orderMaster;
		$this->orderDetails  			= $orderDetails;
		$this->merchant         		= $merchant;
		$this->orderSeller         		= $orderSeller;
		$this->country         			= $country;
		$this->user         			= $user;
        $this->whatsApp                 = $whatsApp;
        // $this->sendMail                 = $sendMail;
	}

	/**
	* Method: getOrderData
	* Description: This method is used to get order data on place order page
	* Author: Sanjoy
	*/
    /**
    * @OA\Get(
    * path="/api/get-order-data/{orderId}",
    * summary="Get order data",
    * description="Get order data",
    * operationId="authLogin",
    * tags={"Alaaddin: Get order data"},
    * security={{"bearer_token":{}}},
    *      @OA\Parameter(
    *         name="orderId",
    *         in="path",
    *         description="order id",
    *         required=true,
    *      ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function getOrderData($orderId) {
        try {
			$this->userId = auth()->user()->id;
			$order = $this->orderMaster->with(['orderMasterDetails.getOrderDetailsUnitMaster','getCountry','getBillingCountry','shipping_city_details','billing_city_details','shipping_state_details','billing_state_details', 'customerDetails'])->whereId($orderId)->where(['user_id' => $this->userId, 'status' => 'I'])->select('id','order_no','order_from','fname','lname','email','phone','shipping_address_id','billing_address_id','shipping_fname','shipping_lname','shipping_email','shipping_phone','shipping_address','shipping_avenue','shipping_street','shipping_city','shipping_city_id','shipping_zip','shipping_country','location','lat','lng','billing_fname','billing_lname','billing_email','billing_phone','billing_country','billing_state','billing_city','billing_zip','shipping_postal_code','shipping_more_address','billing_postal_code','billing_more_address','product_total_weight','total_discount','subtotal','order_total','insurance_price','loading_price','payable_amount','total_seller_commission','admin_commission','payment_method','status','is_paid','order_type','shipping_state','is_review_done','total_product_price')->first();
			# if order not matching with order and user id then send invalid response. 
			if(!@$order) {
				$response['error']['message'] = __('customer_api.invalid_request');
				return response()->json($response);
			}

	    	$response['result']['sellers'] = $this->orderSeller->with(['orderMerchant:id,slug,fname,lname,phone,email,company_name,company_type,company_details,commission,city,state,zipcode,country',
	    		'orderMerchant.city_details',
                'orderMerchant.state_details',
	    		'orderDetails.getOrderDetailsUnitMaster',
	    		'orderDetails.productDetails.productUnitMasters',
	    		'orderDetails.productDetails.productByLanguage:id,product_id,language_id,title,description',
	    		'orderDetails.productDetails.defaultImage:id,image,product_id,is_default',
	    		// 'orderDetails.productDetails.productVariants',
	    		// 'orderDetails.productVariantDetails'
	    	])
	    	->where(['order_master_id' => $orderId])
	    	->select('id','seller_id','order_master_id')
            ->whereHas('orderDetails', function($q){
                // $q->where('status', 'I');
            })
            ->get();
            
            $response['result']['order'] = $order;
            // $response['result']['settings'] = Setting::select('one_kwd_to_point', 'one_point_to_kwd', 'min_point', 'max_discount')->first();

            $response['result']['total_item'] = $this->orderDetails->where(['order_master_id' => $order->id])->count();
            return response()->json($response);
        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error']['message'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        } catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            // $response['error']['message'] = $th->getMessage();
            return response()->json($response);
        }
    }

    public function updatePaymentMethod(Request $request)
    {
        $response = [
            'jsonrpc' => '2.0'
        ];

        $reqData = $request->json('params');

        $this->orderMaster->where('id',$reqData['order_id'])
                          ->update(['payment_method'=>$reqData['payment_method']]);
        $orderData = $this->orderMaster->select('id','order_no','payment_method')
                                       ->where('id',$reqData['order_id'])
                                       ->first();
        $response['result']['order'] = $orderData;
        $response['result']['message']['status'] = "Success";
        $response['result']['message']['message'] = "Payment method updated successfully.";
        return response()->json($response); 
        
    }

    /**
    * Method: placeOrder
    * Description: This method is used to place order
    * Author: Sanjoy
    */
    /**
    * @OA\Post(
    * path="/api/place-order",
    * summary="Customer Place Order",
    * description="Customer Place Order",
    * operationId="authLogin",
    * tags={"Alaaddin: Customer Place Order"},
    * security={{"bearer_token":{}}},
    * @OA\RequestBody(
    *    required=true,
    *    description="Customer Place Order",
    *    @OA\JsonContent(
    *       required={"payment_method","order_id","device_id"},
    *           @OA\Property(property="params", type="object",
    *           @OA\Property(property="payment_method", type="string", example="O"),
    *           @OA\Property(property="order_id", type="integer", format="order_id", example="1241"),
    *           @OA\Property(property="device_id", type="integer", format="device_id", example="41241"),
    * ),
    * ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function placeOrder(Request $request) {
        $response = [
            'jsonrpc' => '2.0'
        ];
        try {
                
            $this->userId = auth()->user()->id;
            $user = auth()->user();

            $reqData = $request->json('params');

            if(@$reqData['payment_method'] == 'C'){
                $response['error'] =__('errors.-5001');
                $response['error']['meaning'] = "We didnot accept cash on delivery!";
                return response()->json($response);
            }
            $validator = Validator::make($reqData,[
                'payment_method'    => 'required|in:O',
            ],
            [
                'payment_method.required'=>'payment_method required',
                'payment_method.in'=>'payment_method should be O only',
            ]);

            # need device id to clear cart for guest user.
            if(@$reqData['device_id']) {
                $this->deviceId = $reqData['device_id'];
            }else{
                $this->deviceId = $user->id;
            }
            
            $orderChk = OrderMaster::with([
                'orderMasterDetails.productByLanguage',
                'orderMasterDetails.productDetails.productUnitMasters',
                'orderMasterDetails.productVariantDetails',
                'customerDetails',
                'getBillingCountryCode',
                'orderMerchants.orderSeller',
                'shipping_city_details',
                'billing_city_details',
                'shipping_state_details',
                'billing_state_details',
            ])
            ->where(['id' => $reqData['order_id'], 'user_id' => $this->userId])
            ->where('status', 'I')
            ->first();
            
            if(@$orderChk){
                if(@$orderChk->payable_amount >= 500000 && $reqData['payment_method'] == 'O'){
                    $response['error'] =__('errors.-5001');
                    $response['error']['message'] = "Error";
                    $response['error']['meaning'] = "You will have to select only cash on delivery as this order has crossed the maximum amount of 500000!";
                    return response()->json($response);
                }
                OrderMaster::where(['id' => $reqData['order_id']])->update(['payment_method'=>$reqData['payment_method']]);

                $orderChk = OrderMaster::with([
                'orderMasterDetails.productByLanguage',
                'orderMasterDetails.productDetails.productUnitMasters',
                'orderMasterDetails.productVariantDetails',
                'customerDetails',
                'getBillingCountryCode',
                'orderMerchants.orderSeller',
                'shipping_city_details',
                'billing_city_details',
                'shipping_state_details',
                'billing_state_details',
                ])
                ->where(['id' => $reqData['order_id'], 'user_id' => $this->userId])
                ->where('status', 'I')
                ->first();
            }
            // $sendEmail = 0;
            // $sendWhatsApp = 0;
            // foreach ($orderChk->orderMerchants as $key => $value) {
            //     if($value->orderSeller->notify_customer_by_email_internal == 'Y') {
            //         $sendEmail += 1; 
            //     }
            //     if($value->orderSeller->notify_customer_by_whatsapp_internal == 'Y') {
            //         $sendWhatsApp += 1;
            //     }
            // }

            // $totalUnavailableItem = 0;
            // foreach ($orderChk->orderMasterDetails as $key => $value) {
            //     # for variant products
            //     if($value->product_variant_id != 0) {
            //         if($value->productVariantDetails->stock_quantity < $value->quantity) {
            //             $totalUnavailableItem += 1;
            //         }
            //     } else {
            //         if($value->productDetails->stock < $value->quantity) {
            //             $totalUnavailableItem += 1;
            //         }
            //     }
            // }
            // if($totalUnavailableItem>0) {
            //     $response['result']['message'] = 'Some item(s) are out of stock';
            //     $response['result']['status'] = 'OUT_OF_STOCK';
            //     return $response;
            // }
            
            # if order not matching with order and user id then send invalid response. 
            if(!@$orderChk) {
                $response['error']['message'] = __('customer_api.invalid_request');
                $response['error']['meaning'] = __('customer_api.invalid_request');
                return response()->json($response);
            }
            
            $this->increaseTimesOfSold($orderChk->id);

            //set total commistion in order_seller table
            $this->setCommision($orderChk);
            $p = Payment::where(['order_id'=>$orderChk->id])->first();
            if(!@$p){
            # insert to payment table
            $this->insertPayment($orderChk);
            }
            if($orderChk->payment_method == 'C') {
                $orderData['status'] = 'INP';
                $orderData['is_paid'] = 'N';
                $orderData['last_status_date'] = date('Y-m-d H:i:s');
                OrderMaster::where(['id' => $orderChk->id])->update($orderData);
                Payment::where(['order_id'=>$orderChk->id])->update(['status'=>'S']);
            
                    $this->sendCustomerAndSellerMail($orderChk->id,$user);
            
                $this->clearCart();
                $response['result'] = __('success_product.-800');
                $response['result']['payment_method'] = $orderChk->payment_method;
                // $response['result']['meaning'] = "You have successfully placed the order";
                if(@$orderChk->payment_method == 'O'){
                    $response['result']['meaning'] = "You have created the order!Now please pay for the order as this order is online order in the next step make payment!";
                }else{
                    $response['result']['meaning'] = "You have successfully placed the order";
                }
                $response['result']['message'] = "Success";
                $response['result'] = __('success_product.-800');
            }
            
            # insert to order timing table
            $this->insertOrderStatusTiming($orderChk->id);
            if(@$orderChk->payment_method == 'O'){
                $p = Payment::where(['order_id'=>$orderChk->id])->first();
                // if(!@$p->payment_initated_response){
                    $orderinfo = OrderMaster::with(['shippingAddress.getCountry','billingAddress.getCountry','getCountry','customerDetails'])->where(['id'=>$orderChk->id])->first();
                    $orderInfo = OrderMaster::with(['shippingAddress.getCountry','billingAddress.getCountry','getCountry','customerDetails'])->where(['id'=>$orderChk->id])->first();
                    $PaymentKeyTable = PaymentKeyTable::first();
                    $response['result']['RAZORPAY_KEY'] = $PaymentKeyTable->RAZORPAY_KEY;
                    $response['result']['RAZORPAY_SECRET'] = $PaymentKeyTable->RAZORPAY_SECRET;
                    $username=$PaymentKeyTable->RAZORPAY_KEY;
                    $password=$PaymentKeyTable->RAZORPAY_SECRET;

                    $URL='https://api.razorpay.com/v1/orders';
                    $headers = array(
                        "Authorization: Basic ".base64_encode($username.":".$password), 
                        'Content-Type: application/json'
                    );
                    $params['amount']=round(($orderInfo->payable_amount*100));
                    $params['currency']="INR";
                    $params['receipt']=$orderInfo->order_no;
                    $params['partial_payment']=false;
                    

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL,$URL);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
                    $result=curl_exec ($ch);
                    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
                    curl_close ($ch);
                    $jdres = json_decode($result);
                    $jeres = json_encode($result);
                    $updOPayment['payment_initated_response']=@$jeres; 
                    $updOPayment['payment_initated_id']=@$jdres->id; 
                    Payment::where(['order_id'=>$orderChk->id])->update($updOPayment);

                    $p = Payment::where(['order_id'=>$orderChk->id])->first();

                    if(@$status_code == 200){
                        $response['result'] = __('success.-5008');
                        $response['result']['message'] = "Success";
                        $response['result']['payment_initated_response'] = @$jdres;
                        $response['result']['status_code'] = $status_code;
                        $response['result']['payment_initated_id'] = @$p->payment_initated_id;
                        $response['result']['payment_method'] = $orderChk->payment_method;
                        $response['result']['order'] = OrderMaster::where('id', $reqData['order_id'])
                        ->with([
                            'shippingAddress.getCountry',
                            'billingAddress.getCountry',
                            'getCountry', 
                            'customerDetails'
                        ])->first();
                    }else{
                        $response['error'] =__('errors.-5001');
                        $response['error']['meaning'] =@$jdres->error->code." ".@$jdres->error->description;
                        $response['error']['payment_initated_response'] = json_decode(@$p->payment_initated_response);
                        $response['error']['status_code'] = $status_code;
                        $response['error']['payment_initated_id'] = @$p->payment_initated_id;
                        $response['result']['payment_method'] = $orderChk->payment_method;
                        $response['result']['order'] = OrderMaster::where('id', $reqData['order_id'])
                        ->with([
                            'shippingAddress.getCountry',
                            'billingAddress.getCountry',
                            'getCountry', 
                            'customerDetails'
                        ])->first();
                    }
                // }else{
                //     $p = Payment::where(['order_id'=>$orderChk->id])->first();
                //     $response['result'] = __('success.-5008');
                //     $response['result']['payment_initated_response'] = json_decode(@$p->payment_initated_response);
                //     $response['result']['payment_initated_id'] = @$p->payment_initated_id;
                //     $response['result']['payment_method'] = $orderChk->payment_method;
                //     $response['result']['order'] = OrderMaster::where('id', $reqData['order_id'])
                //     ->with([
                //         'shippingAddress.getCountry',
                //         'billingAddress.getCountry',
                //         'getCountry', 
                //         'customerDetails'
                //     ])->first();
                // }
            }else{
                $response['result'] = __('success_user.-800');
                $response['result']['payment_method'] = $orderChk->payment_method;
                $response['result']['order'] = OrderMaster::where('id', $reqData['order_id'])
                ->with([
                    'shippingAddress.getCountry',
                    'billingAddress.getCountry',
                    'getCountry', 
                    'customerDetails'
                ])->first();
            }
            $PaymentKeyTable = PaymentKeyTable::first();
            $response['result']['RAZORPAY_KEY'] = $PaymentKeyTable->RAZORPAY_KEY;
            $response['result']['RAZORPAY_SECRET'] = $PaymentKeyTable->RAZORPAY_SECRET;
            return response()->json($response);
        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            // $response['error']['message'] = $e->getMessage();
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $e->getMessage();
            return response()->json($response);
        } catch (\Exception $th) {
            $response['error'] =__('errors.-5001');
            $response['error']['meaning'] = $th->getMessage();
            \Log::error($th->getMessage());
            // $response['error']['message'] = $th->getMessage();
            return response()->json($response);
        }
    }

    /**
    * Method: insertOrderStatusTiming
    * Description: This method is used to insert time in to order status timing table
    * Author: Sanjoy
    */
    private function insertOrderStatusTiming($orderId) {
        $order = OrderMaster::find($orderId);
        OrderStatusTiming::create([
            'order_id'      => $order->id,
            'status'        => 'N',
            'start_time'    => date('Y-m-d H:i:s'),
            'duration_in_minutes' => 0
        ]);
        if($order->status == 'DA') {
            OrderStatusTiming::create([
                'order_id'      => $order->id,
                'status'        => 'DA',
                'start_time'    => date('Y-m-d H:i:s'),
                'duration_in_minutes' => 0
            ]);
        }
    }

    /**
    * 
    */
    public function calculateCoupon($requset, $order) {
        # check coupon code exist or not
        $checkCoupon = Coupon::with(['getMerchants'])->where(['code' => $requset['coupon_code']])->first();
        if(!@$checkCoupon) {
            return 'Invalid coupon.';
        } else if($checkCoupon->start_date > date('Y-m-d')){
            return 'Invalid coupon.';
        } else if($checkCoupon->end_date < date('Y-m-d')){
            return 'Coupon has been expired.';
        } else if($checkCoupon->no_of_times <= $checkCoupon->used_times){
            return 'This is a used coupon.';
        }

        # check the coupon is associated with merchant or not
        $orderMerchants = $order->orderMerchants->pluck('seller_id')->toArray();

        if($checkCoupon->applied_for == 'M') {
            $couponMerchants = $checkCoupon->getMerchants->pluck('merchant_id')->toArray();
            $associatedMerchants = array_intersect($orderMerchants, $couponMerchants);
            if(!@$associatedMerchants) {
                return 'No seller associated with this coupon.';
            }
        } else {
            $associatedMerchants = $this->orderSeller->where('order_master_id', $order->id)->pluck('seller_id')->toArray();
        }

        $couponDiscount = 0;
        $shippingPrice = @$order->shipping_price;
        if($checkCoupon->applied_on == 'SUB') { // subtotal
            $response['result']['discount_for'] = 'SUB';
            if($checkCoupon->discount_type == 'F') {
                if(@$order->subtotal) {
                    $couponDiscount = $checkCoupon->discount;

                    if($checkCoupon->coupon_bearer == 'M') {
                        $orderSel = $this->orderSeller->where('order_master_id', $order->id)->whereIn('seller_id', $associatedMerchants)->get();
                        $couponNotAssoSel = $this->orderSeller->where('order_master_id', $order->id)->whereNotIn('seller_id', $associatedMerchants)->get();
                        $subtotal = ($order->subtotal - $order->total_discount) - $couponNotAssoSel->sum('order_total');
                        foreach ($orderSel as $sp) {
                            $sellerCommision = $this->merchant->whereId($sp->seller_id)->first();
                            $total = $this->orderDetails->where([
                                'order_master_id' => $order->id,
                                'seller_id'       => $sp->seller_id
                            ])->sum('total');

                            # finding seller percentage from this order
                            $sellerPercentage = (($total * 100) / $subtotal );
                            $sellerCouponPercentAmount = ($couponDiscount * $sellerPercentage) / 100;
                            $totalCommision = ($sellerCommision->commission*($total - $sellerCouponPercentAmount))/100;
                            if($totalCommision < $sellerCommision->minimum_commission){
                                if($sellerCommision->minimum_commission > $order->order_total) {
                                    $update['total_commission'] = $order->order_total - $sellerCouponPercentAmount;
                                } else {
                                    $update['total_commission'] = $sellerCommision->minimum_commission;
                                }
                            } else {
                                $update['total_commission'] = $totalCommision;
                            }
                            $update['coupon_distributed_amount'] = $sellerCouponPercentAmount;
                            $update['order_total']               = $sp->order_total - $sellerCouponPercentAmount;
                            $update['coupon_bearer']             = 'M';
                            $this->orderSeller->whereId($sp->id)->update($update);
                        }
                    }
                }
            } else if($checkCoupon->discount_type == 'P') {
                if($checkCoupon->applied_for == 'M') {
                    $merchantOrderTotal = OrderSeller::where(['order_master_id' => $order->id])->whereIn('seller_id', $associatedMerchants)->sum('order_total');
                    $couponDiscount = (($merchantOrderTotal * $checkCoupon->discount) /100);
                } else {
                    $couponDiscount = ((($order->subtotal - $order->total_discount) * $checkCoupon->discount) /100);
                }

                # calculate seller commission for the coupon
                if($checkCoupon->coupon_bearer == 'M') {
                    $orderSel = $this->orderSeller->where('order_master_id', $order->id)->whereIn('seller_id', $associatedMerchants)->get();
                    $couponNotAssoSel = $this->orderSeller->where('order_master_id', $order->id)->whereNotIn('seller_id', $associatedMerchants)->get();
                    $subtotal = ($order->subtotal - $order->total_discount) - $couponNotAssoSel->sum('order_total');
                    foreach ($orderSel as $sp) {
                        $sellerCommision = $this->merchant->whereId($sp->seller_id)->first();
                        $total = $this->orderDetails->where([
                            'order_master_id' => $order->id,
                            'seller_id'       => $sp->seller_id
                        ])->sum('total');

                        # finding seller percentage from this order
                        $sellerPercentage = (($total * 100) / $subtotal );
                        $sellerCouponPercentAmount = ($couponDiscount * $sellerPercentage) / 100;

                        $totalCommision = ($sellerCommision->commission * ($total - ($total * $checkCoupon->discount / 100)))/100;

                        if($totalCommision < $sellerCommision->minimum_commission){
                            if($sellerCommision->minimum_commission > $order->order_total) {
                                $update['total_commission'] = ($order->order_total * $checkCoupon->discount) / 100; 
                            } else {
                                $update['total_commission'] = ($sellerCommision->minimum_commission * $checkCoupon->discount) / 100;
                            }
                        } else {
                            $update['total_commission'] = $totalCommision;
                        }
                        $update['coupon_distributed_amount'] = $sellerCouponPercentAmount;
                        $update['order_total']               = $sp->order_total - $sellerCouponPercentAmount;
                        $update['coupon_bearer']             = 'M';
                        $this->orderSeller->whereId($sp->id)->update($update);
                    }
                }
            }
        } else if($checkCoupon->applied_on == 'SHI') { // shipping
            $response['result']['discount_for'] = 'SHI';
            if($checkCoupon->discount_type == 'F') {
                if(@$order->shipping_price > 0) {
                    $couponDiscount = $checkCoupon->discount;
                    $shippingPrice = @$order->shipping_price - $checkCoupon->discount;
                }
            } else if($checkCoupon->discount_type == 'P') {
                $couponDiscount = (($order->shipping_price * $checkCoupon->discount) /100);
                $shippingPrice = @$order->shipping_price - $couponDiscount;
            }
        }

        # update order master 
        $params = [
            'coupon_id'         => $checkCoupon->id,
            'coupon_code'       => $requset['coupon_code'],
            'coupon_applied_on' => $checkCoupon->applied_on,
            'coupon_bearer'     => @$checkCoupon->coupon_bearer,
            'coupon_discount'   => $couponDiscount,
            'shipping_price'    => $shippingPrice,
            'order_total'       => $order->order_total - $couponDiscount
        ];
        OrderMaster::where(['id' => $order->id])->update($params);

        # update coupon table 
        Coupon::where(['id' => $checkCoupon->id])->increment('used_times', 1);
    }

    /**
    * Method: increaseTimesOfSold
    * Description: This method is used to increase product times of sold.
    * Author: Sanjoy
    */
    public function increaseTimesOfSold($orderId) {
        $orderDtl = OrderDetail::where(['order_master_id' => $orderId])->get();
        foreach ($orderDtl as $key => $value) {
            Product::where(['id' => $value->product_id])->increment('times_of_sold', $value->quantity);
        }
    }

    /*
    * Method: sendNotification
    * Description: This method is used to send Notification to driver
    * Author: Jayatri
    */
    private function sendNotification($order_id){
        $androidDrivers = Driver::where(['status' => 'A', 'device_type' => 'A'])->pluck('firebase_reg_no');

        $iosDrivers = Driver::where(['status' => 'A', 'device_type' => 'I'])->pluck('firebase_reg_no');
        $order_dtt = OrderMaster::where('id',$order_id)->first();
        # send notification to android drivers
        if(count($androidDrivers)) {
            $msg                      = array();
            $msg['title']             = 'Order No:'." ".$order_dtt->order_no;
            $msg["body"]              = $order_dtt->shipping_fname." placed order recently!";
            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            $msg['link_type']         = "Y";
            $msg['is_linked']         = "Y";
            $msg['image']             = '';
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]           = "10";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'registration_ids' => $androidDrivers,
                'data'             => $msg
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            
            // Closing Curl
            curl_close($ch);
        }
        
        # send notification to ios drivers
        if(count($iosDrivers)) {
            $msg                      = array();
            $msg['title']             = "New Order Placed";
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]            = "10";
            $msg["mutable-content"]   = true;
            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            $msg["body"]              = 'Recently customer placed an order! ';

            $data['message'] = "New Order Placed";
            $data['type'] = "1";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'category'                  => "CustomSamplePush",
                'content_available'         => true,
                'mutable_content'           => true,
                'registration_ids'          => $iosDrivers,
                'notification'              => $msg,
                'data'                      => $data
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            // Closing Curl
            curl_close($ch);
        }
    }

    /*
    // * Method: sendNotificationDashboardUpdate 
    // * Description : This method is used to update dashboard automatically specialy merchant dashboard client requirement using web notification (ajax call)
    // * Author: Jayatri
    // */
    private function sendNotificationDashboardUpdate($orderId, $merchantId) {
        // For Notification sending
        // dd($merchantId);
        $order_dtt = OrderMaster::where('id',$orderId)->first();
        $new_registrationIds = MerchantRegId::where(['user_id' => $merchantId])->pluck('reg_id')->toArray();
        $idCount = MerchantRegId::where(['user_id' => $merchantId])->pluck('reg_id')->Count();
        // dd(@$idCount);
        if($idCount != 0){
            if(@$order_dtt) {
            $username = "Testing ...";
            $click_action = route('merchant.dashboard');
            $title = $username." Recently customer placed an order!";
            $message = "New order Placed !";
            // dd($new_registrationIds);
            $fields = array (
                'registration_ids' => $new_registrationIds,
                'data' => array (
                    "message"       => $message,
                    "title"         => $title,
                    "image"         => url('firebase-logo.png'),
                    "click_action"  => $click_action,
                ),
                'notification'      => array (
                    "body"          => $message,
                    "title"         => $title,
                    "click_action"  => $click_action,
                    "icon"          => "url('firebase-logo.png')",
                )
            );
            // dd($fields);
            $fields = json_encode ( $fields );
            $API_ACCESS_KEY = 'AAAAcQGyZzs:APA91bEst24UvPwO2UpmnRh6OUH_Y_On82ZRIm1ekOSKcekCuOc1m2yYR2w6CGDt09nBY8dmL5h_8C5Cs_KMgIPd1m0mZ8oMUDWz75KaDJmJeNOkTFklIS-hYgWYA3qHnbO-zb76jNK-';
            $headers = array(
                'Authorization: key=' . $API_ACCESS_KEY,
                'Content-Type: application/json',
            );
            $ch = curl_init ();
            curl_setopt ( $ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt ( $ch, CURLOPT_POST, true );
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
            $result = curl_exec ( $ch );
            curl_close ( $ch );
            // dd($result);
            return $result.' New Order placed! ';
            }
        }

    // End of Notification sending
    }

    /*
    * Method: decreaseStock
    * Description: This method is used to descrease Stock
    * Author: Jayatri
    */
    private function decreaseStock($orderId) {   
        $details = $this->orderDetails->where('order_master_id', $orderId)->get();
        foreach($details as $dtl){
            if($dtl->product_variant_id != 0){
                $variant = $this->productVariant->where('id',$dtl->product_variant_id)->first();
                if($variant->stock_quantity >= $dtl->quantity){
                    $updatestock['stock_quantity'] = $variant->stock_quantity - $dtl->quantity;
                    $this->productVariant->where('id',$dtl->product_variant_id)->update($updatestock);
                }else{
                    // return 0;
                }
            }else{
                if($dtl->product_id != 0){
                    $product = $this->product->where('id',@$dtl->product_id)->first();    
                    if($product->stock >= $dtl->quantity){
                        $stock_update['stock'] = $product->stock - $dtl->quantity;
                        $this->product->where('id',@$dtl->product_id)->update($stock_update);
                    }else{
                        // return 0;
                    }
                }else{
                    // return 0;
                }
            }   
        }
    }
    
    /**
    * Method: sendCustomerAndSellerMail (Internal order)
    * Description: This method is used to send order mail to customer and seller if payment method is COD
    * Author: Sanjoy/Jayatri(changes)
    */
    private function sendCustomerAndSellerMail($orderId,$user) {
        $data = [];
        $data['order'] = $order = OrderMaster::where(['id' => $orderId])
        ->with([
            'shippingAddress.getCountry',
            'billingAddress.getCountry',
            'getCountry'
        ])->first();

        $data['orderDetails'] = OrderDetail::where(['order_master_id' => $orderId])
        ->with([
            'sellerDetails',
            'defaultImage',
            'productByLanguage',
            'productVariantDetails'
        ])
        ->get();

        $contactList = [];
        $i=0;
        $user = $this->user->whereId($order->user_id)->first();
        $data['fname']          = $user->fname;
        $data['lname']          = $user->lname;  
        $data['email']          = $user->email;
        $data['user_type']      = $user->user_type;
        $data['shipping_price'] = @$order->shipping_price;
        $data['orderNo']        = @$order->order_no;
        $data['language_id']    = getLanguage()->id;
        $data['bcc']            = $contactList;
        $data['sub_total']      = @$order->subtotal;
        $data['order_total']    = @$order->order_total;
        $data['total_discount'] = @$order->total_discount;
        if(@$order->status == 'INP') {
            $data['subject'] = "Your order has been placed successfully! Order no: ".@$order->order_no;
        } else {
            $data['subject'] = " Order No:".@$order->order_no." ! Order Details!";
        }
        
        #send mail admin to notify for new order only as client requirement (internal order)
        $i=0;
        if(@$order->status =='N'){
            $setting = Setting::first();
            if(@$setting->email_1) {
                $contactList[$i] = $setting->email_1;
                $i++;
            }
            if(@$setting->email_2) {
                $contactList[$i] = $setting->email_2;
                $i++;
            }
            if(@$setting->email_3) {
                $contactList[$i] = $setting->email_3;
                $i++;
            }
        }
        $data['bcc'] = $contactList;
        #send to customer mail(order status can be new/driver assigned)
        Mail::send(new OrderMail($data));
        
        # send mail order details to seller mail
        $dtls_mail = OrderDetail::where(['order_master_id' => $order->id])->groupBy('seller_id')->get();
        
        foreach(@$dtls_mail as $contact) {
            $contactListEmail1 = [];
            $contactListEmail2 = [];
            $contactListEmail3 = [];
            $contactList = [];
            $data['bcc'] = [];
            $data['orderDetails'] = [];
            $i = 0;
            $data['user_type'] = 'C';
            $data['hide_customer_info'] = $contact->sellerDetails->hide_customer_info;

            $data['orderSeller'] = OrderSeller::where(['order_master_id' => $order->id,'seller_id'=>@$contact->seller_id])->first();
            $data['orderDetails'] = OrderDetail::where(['order_master_id' => $order->id,'seller_id'=>@$contact->seller_id])
            ->with([
                'sellerDetails',
                'defaultImage',
                'productByLanguage',
                'productVariantDetails'
            ])
            ->get();

            $data['sub_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
            $data['order_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
            $data['total_discount'] = OrderSeller::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total_discount');
            $data['shipping_price'] = "NA";
            if(@$order->status == 'INP') {
                $data['subject'] = "New Order Placed! Order No : ".@$order->order_no." ! Order Details!";
            } else {
                $data['subject'] = "Order No : ".@$order->order_no." ! Order Details!";    
            }
            
            $contactList[$i] = $contact->sellerDetails->email;
            $i++;
            // if(@$order->status =='DA') {
            //     if($contact->sellerDetails->email1) {
            //         $contactListEmail1[$i] = $contact->sellerDetails->email1;
            //         $i++;
            //     }
            //     if($contact->sellerDetails->email2) {
            //         $contactListEmail2[$i] = $contact->sellerDetails->email2;
            //         $i++;
            //     }
            //     if($contact->sellerDetails->email3) {
            //         $contactListEmail3[$i] = $contact->sellerDetails->email3;
            //         $i++;
            //     }    
            // }
            $contactList = array_unique($contactList);
            if(@$contactListEmail1 != '') {
                $contactListEmail1 = array_unique($contactListEmail1);
                $contactList = array_merge($contactList, $contactListEmail1);
            }
            if(@$contactListEmail2 != '') {
                $contactListEmail2 = array_unique($contactListEmail2);
                $contactList = array_merge($contactList, $contactListEmail2);
            }
            if(@$contactListEmail3 != '') {
                $contactListEmail3 = array_unique($contactListEmail3);
                $contactList = array_merge($contactList, $contactListEmail3);
            }
            $data['bcc'] = $contactList;
            Mail::send(new SendOrderDetailsMerchantMail($data));
            // $this->sendNotificationDashboardUpdate($contact->order_master_id,$contact->seller_id);
        }
    }
    private function sendCustomerAndSellerMailOld($orderId, $user) {
        $data = [];
        $data['order'] = $order = OrderMaster::where(['id' => $orderId])
        ->with([
            'shippingAddress.getCountry',
            'billingAddress.getCountry',
            'getCountry'
        ])->first();

        $data['orderDetails'] = OrderDetail::where(['order_master_id' => $orderId])
        ->with([
            'sellerDetails',
            'defaultImage',
            'productByLanguage',
            'productVariantDetails'
        ])
        ->get();
        $contactList = [];
        $i=0;
        $user = $this->user->whereId($order->user_id)->first();
        $data['fname'] = $user->fname;
        $data['lname'] = $user->lname;  
        $data['email'] = $user->email;
        $data['user_type'] = $user->user_type; 

        $data['shipping_price'] = @$order->shipping_price;
        $data['orderNo'] = $order->order_no;
        $data['language_id'] = getLanguage()->id;
        $data['bcc'] = $contactList;
        
        $data['sub_total'] = @$order->subtotal;
        $data['order_total'] = @$order->order_total;
        $data['total_discount'] = $order->total_discount;
        if(@$order->status == 'N'){
            $data['subject'] = "Your order has been placed successfully! Order no: ".@$order->order_no;
        }else{
            $data['subject'] = "Your order is accepted! Order No:".@$order->order_no." ! Order Details!";
        }
        
        #send mail admin to notify for new order only as client requirement (internal order)
        $i=0;
        if(@$order->status =='N' ||@$order->status =='DA'){
            $setting = Setting::first();
            if(@$setting->email_1) {
                $contactList[$i] = $setting->email_1;
                $i++;
            }
            if(@$setting->email_2) {
                $contactList[$i] = $setting->email_2;
                $i++;
            }
            if(@$setting->email_3) {
                $contactList[$i] = $setting->email_3;
                $i++;
            }
        }
        $data['bcc'] = $contactList;
        #send to customer mail(order status can be new/driver assigned)
        Mail::send(new OrderMail($data));
        
        # send mail order details to seller mail
        $dtls_mail = OrderDetail::where(['order_master_id' => $order->id])->groupBy('seller_id')->get();
        
        foreach(@$dtls_mail as $contact){
            $contactListEmail1 = [];
            $contactListEmail2 = [];
            $contactListEmail3 = [];
            $contactList = [];
            $data['bcc'] = [];
            $i = 0;
            //start
            $data['user_type'] = 'C';
            $data['hide_customer_info'] = $contact->sellerDetails->hide_customer_info;

            $data['orderDetails'] = OrderDetail::where(['order_master_id' => $order->id,'seller_id'=>@$contact->seller_id])
            ->with([
                'sellerDetails',
                'defaultImage',
                'productByLanguage',
                'productVariantDetails'
            ])
            ->get();
            
            $data['sub_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
            $data['order_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
            $data['total_discount'] = OrderSeller::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total_discount');
            $data['shipping_price'] = "NA";
            if(@$order->status == 'N'){
                $data['subject'] = "New Order Placed! Order No : ".@$order->order_no." ! Order Details!";
            }else{
                $data['subject'] = "Order is Accepted! Order No : ".@$order->order_no." ! Order Details!";    
            }
            
            // Fill the Bcc email address
            $contactList[$i] = $contact->sellerDetails->email;
            $i++;
            #send mail to merchant if status driver assigned(client requirement)
            if(@$order->status =='DA' || $order->status =='N'){
                if($contact->sellerDetails->email1) {
                    $contactListEmail1[$i] = $contact->sellerDetails->email1;
                    $i++;
                }
                if($contact->sellerDetails->email2) {
                    $contactListEmail2[$i] = $contact->sellerDetails->email2;
                    $i++;
                }
                if($contact->sellerDetails->email3) {
                    $contactListEmail3[$i] = $contact->sellerDetails->email3;
                    $i++;
                }    
            }
            $contactList = array_unique($contactList);
            if(@$contactListEmail1 != '') {
                $contactListEmail1 = array_unique($contactListEmail1);
                $contactList = array_merge($contactList, $contactListEmail1);
            }
            if(@$contactListEmail2 != '') {
                $contactListEmail2 = array_unique($contactListEmail2);
                $contactList = array_merge($contactList, $contactListEmail2);
            }
            if(@$contactListEmail3 != '') {
                $contactListEmail3 = array_unique($contactListEmail3);
                $contactList = array_merge($contactList, $contactListEmail3);
            }
            $data['bcc'] = $contactList;

            #send web notification for merchant dashboard update
            // $this->sendNotificationDashboardUpdate($contact->order_master_id, $contact->seller_id);
            Mail::send(new SendOrderDetailsMerchantMail($data));
        }
    }

    /**
    * Method: updateOrderLoyaltyPoint
    * Description: This method is used to update loyalty point to order master table.
    * Author: Sanjoy
    */
    private function updateOrderLoyaltyPoint($request, $order) {
        $setting = Setting::first();
        if(@$request['reward_point']) {    
            if(@$request['reward_point'] == 'Y') {  
                $totalOrder = $request['loyalty_point']*$setting->one_point_to_kwd;
                $this->orderMaster->whereId($order->id)->increment('loyalty_point_used', $request['loyalty_point']);
                $this->orderMaster->whereId($order->id)->decrement('order_total', number_format($totalOrder, 3));
                $this->orderMaster->whereId($order->id)->update(['loyalty_amount' => number_format($totalOrder, 3)]);
            }
        }
    }

    /**
    * Method: updateOrderStatus
    * Description: This method is used to change status of order_master and order_details
    * Author: Sanjoy
    */
    private function updateOrderStatus($orderId) {
        # if driver is already assigned with this merchant 
        $ordDetl = $this->orderDetails->where(['order_master_id' => $orderId])->get()->pluck('seller_id');
        $sellerDriver = $this->merchant->select('id','driver_id')->whereIn('id', $ordDetl)->get();
        foreach ($sellerDriver as $sd) {
            if($sd->driver_id != '0') {
                $assignDriver['driver_id']  = $sd->driver_id;    
                $assignDriver['status']     = 'DA';    
                $this->orderDetails->where(['seller_id' => $sd->id,'order_master_id' => $orderId])->update($assignDriver);
                $this->orderSeller->where(['seller_id' => $sd->id,'order_master_id' => $orderId])->update($assignDriver);
            }
        }

        $drvAssndChck = $this->orderDetails->where(['order_master_id' => $orderId,'driver_id' => '0'])->count();
        if($drvAssndChck > 0) {
            $order['status'] = 'N';   
        } else {
            $order['status'] = 'DA';   
        }
        $order['last_status_date'] = date('Y-m-d H:i:s');
        $this->orderMaster->where(['id' => $orderId])->update($order);
    }

    /**
    * Method: calculateLoyaltyPoint
    * Description: This method is used to deduct loyalty point from customer account.
    * Author: Sanjoy
    */
    private function deductLoyaltyPoint($orderId) {
        $setting = Setting::first();
        $order = $this->orderMaster->whereId($orderId)->first();
        if(@$order->loyalty_point_used > 0) {  
            $totalOrder = $order->loyalty_point_used*$setting->one_point_to_kwd;
            
            # insert to user reward table
            $addUserReward = new UserReward;
            $addUserReward['user_id']        = @$this->userId;
            $addUserReward['order_id']       = $orderId;
            $addUserReward['earning_points'] = $order->loyalty_point_used;
            $addUserReward['debit']          = '1';
            $addUserReward->save();

            # update customer total reward
            $this->user->whereId(@$this->userId)->decrement('loyalty_balance', $order->loyalty_point_used);
            $this->user->whereId(@$this->userId)->increment('loyalty_used', $order->loyalty_point_used);
        }
    }

    /**
    * method: insertPayment
    * Description: This method is used to insert to payment table.
    * Author: Sanjoy
    */
    private function insertPayment($reqData = []) {
        Payment::create([
            'user_id'       => $reqData->user_id,
            'order_id'      => $reqData->id,
            'amount'        => $reqData->order_total,
            'type'        => 'order_payment',
            'payment_mode'  => $reqData->payment_method,
            'status'        => 'I' 
        ]);
    }

    /**
    * Method: calculateLoyaltyPoint
    * Description: This method is used to calculate loyalty point
    * Author: Sanjoy
    */
    private function calculateLoyaltyPoint($request, $order) {
        $setting = Setting::first();
        $loyaltyPoint = $order->order_total*$setting->one_kwd_to_point;
        $totalOrder = $loyaltyPoint*$setting->one_point_to_kwd;
        $this->orderMaster->whereId($order->id)->increment('loyalty_point_received', $loyaltyPoint);
        $this->user->whereId(@$this->userId)->increment('loyalty_balance', $loyaltyPoint);
        $this->user->whereId(@$this->userId)->increment('loyalty_total', $loyaltyPoint);

        $addUserReward = new UserReward;
        $addUserReward['user_id']        = @$this->userId;
        $addUserReward['order_id']       = $order->id;
        $addUserReward['earning_points'] = $loyaltyPoint;
        $addUserReward['credit']         = '1';
        $addUserReward->save();

        if(@$request['reward_point']) {
            if(@$request['reward_point'] == 'Y') {  
                $totalOrder = $request['loyalty_point']*$setting->one_point_to_kwd;
                $addUserReward = new UserReward;
                $addUserReward['user_id']        = @$this->userId;
                $addUserReward['order_id']       = $order->id;
                $addUserReward['earning_points'] = $request['loyalty_point'];
                $addUserReward['debit']          = '1';
                $addUserReward->save();
                $this->user->whereId(@$this->userId)->decrement('loyalty_balance', $request['loyalty_point']);
                $this->user->whereId(@$this->userId)->increment('loyalty_used', $request['loyalty_point']);
                $this->orderMaster->whereId($order->id)->increment('loyalty_point_used', $request['loyalty_point']);
                $this->orderMaster->whereId($order->id)->decrement('order_total', number_format($totalOrder, 3));
                $this->orderMaster->whereId($order->id)->update(['loyalty_amount' => number_format($totalOrder, 3)]);
            }
        }
    }

    /**
    * Method: setCommision
    * Description: This method is used to set commission
    * Author: Sanjoy
    */
    private function setCommision($orderChk) {   
        $orderSel = $this->orderSeller->where(['order_master_id' => $orderChk->id])->get(); 
        foreach ($orderSel as $sp) {
            $sellerCommision = $this->merchant->whereId($sp->seller_id)->first();
            $total = $this->orderDetails->where([
                'order_master_id' => $orderChk->id,
                'seller_id'       => $sp->seller_id
            ])->sum('total');
            // $totalCommision = ($sellerCommision->commission*$total)/100;
            // if($totalCommision < $sellerCommision->minimum_commission){
            //     if($sellerCommision->minimum_commission > $total) {
                    // $update['total_commission'] = $total;
                // } else {
                //     $update['total_commission'] = $sellerCommision->minimum_commission;
                // }
            // } else {
                // $update['total_commission'] = $totalCommision;
            // }
            // $this->orderSeller->whereId($sp->id)->update($update);
        }
    }

    /**
    * Method: clearCart
    * Description: This method is used to clear cart
    * Author: Sanjoy
    */
    private function clearCart() {
        $user = auth()->user();
        if(@$user->user_type == 'C') {
            $cart = $this->cart->where('user_id', $this->userId)->first();
            if($cart) {
                $this->cartDetails->where('cart_master_id', $cart->id)->delete(); 
                $this->cart->where('id', $cart->id)->delete();
            }
        } else {
            $cart = $this->cart->where('session_id', $this->deviceId)->first();
            if($cart) {
                $this->cartDetails->where('cart_master_id', $cart->id)->delete(); 
                $this->cart->where('id', $cart->id)->delete();
            }
        }
    }

    /**
    * Method: applyCoupon
    * Description: This method is used to apply coupon
    * Author: Sanjoy
    */
    public function applyCoupon(Request $request) {
        $reqData = $request->params;
        $response = [
            'jsonrpc' => '2.0'
        ];

        # check coupon code exist or not
        $checkCoupon = Coupon::with(['getMerchants'])->where(['code' => $reqData['coupon_code']])->first();
        if(!@$checkCoupon) {
            $response['error']['message'] = 'Invalid coupon.';
            return response()->json($response);
        } else if($checkCoupon->start_date >= date('Y-m-d')){
            $response['error']['message'] = 'Invalid coupon.';
            return response()->json($response);
        } else if($checkCoupon->end_date <= date('Y-m-d')){
            $response['error']['message'] = 'Coupon has been expired.';
            return response()->json($response);
        } else if($checkCoupon->no_of_times <= $checkCoupon->used_times){
            $response['error']['message'] = 'This is a used coupon.';
            return response()->json($response);
        }
        # check the coupon is associated with merchant or not
        $order = OrderMaster::with(['orderMerchants'])->where(['id' => $reqData['order_id']])->first();
        $orderMerchants = $order->orderMerchants->pluck('seller_id')->toArray();

        if($checkCoupon->applied_for == 'M') {
            $couponMerchants = $checkCoupon->getMerchants->pluck('merchant_id')->toArray();
            $associatedMerchants = array_intersect($orderMerchants, $couponMerchants);
            if(!@$associatedMerchants) {
                $response['error']['message'] = 'No seller associated with this coupon.';
                return response()->json($response);
            }
        }

        $couponDiscount = 0;
        if($checkCoupon->applied_on == 'SUB') { // subtotal
            $response['result']['discount_for'] = 'SUB';
            if($checkCoupon->discount_type == 'F') {
                if(@$order->subtotal) {
                    $couponDiscount = $checkCoupon->discount;
                }
            } else if($checkCoupon->discount_type == 'P') {
                if($checkCoupon->applied_for == 'M') {
                    $merchantOrderTotal = OrderSeller::where([
                        'order_master_id' => $reqData['order_id']
                    ])->whereIn('seller_id', $associatedMerchants)
                    ->sum('order_total');
                    
                    $couponDiscount = (($merchantOrderTotal * $checkCoupon->discount) /100);
                } else {
                    $couponDiscount = ((($order->subtotal - $order->total_discount) * $checkCoupon->discount) /100);
                }
            }
        } else if($checkCoupon->applied_on == 'SHI') { // shipping
            $response['result']['discount_for'] = 'SHI';
            if($checkCoupon->discount_type == 'F') {
                if(@$order->shipping_price > 0) {
                    $couponDiscount = $checkCoupon->discount;
                }
            } else if($checkCoupon->discount_type == 'P') {
                $couponDiscount = (($order->shipping_price * $checkCoupon->discount) /100);;
            }
        }
        $response['result']['coupon_discount'] = $couponDiscount;
        return response()->json($response);
    }
}
