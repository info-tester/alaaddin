<?php

namespace App\Http\Controllers\Modules\Driver_review;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OrderMaster;
use App\Models\DriverReview;
class DriverReviewContoller extends Controller
{
    /**
	* Method: index
	* Description: This method is used to show the driver review page.
    * Author: Argha
    * Date  : 2020-09-08
	*/
    public function index($order_no,$user_id,$token)
    {
        $data['order'] = OrderMaster::with(['orderMasterDetails.driverDetails'])
                                    ->where(['order_no'=>$order_no,'verify_code' => $token])
                                    ->first();
        if($data['order'] != null){
            $data['checkReview'] = DriverReview::where(['customer_id' => $user_id , 'order_master_id' =>$data['order']->id])
                                    ->first();
        }
        
        
        //dd($data['order']);
        return view('modules.driver_review.driver_review')->with($data);
    }

    /**
	* Method: submitDriverReview
	* Description: This method is used to show submit driver review .
    * Author: Argha
    * Date  : 2020-09-08
    */
    
    public function submitDriverReview(Request $request)
    {
        $insertRating['order_master_id'] = $request->order_id;
        $insertRating['customer_id'] = $request->customer_id;
        $insertRating['merchant_id'] = 0;
        $insertRating['driver_id'] = $request->driver_id;
        $insertRating['review_point'] = $request->driver_rating;
        $insertRating['review_comments'] = $request->comment;

        $insert = DriverReview::create($insertRating);
        
        if($insert){
            OrderMaster::where(['id'=>$request->order_id])->update(['verify_code' =>null]);
            return '1';
        }else{
            return '0';
        }
    }
}
