<?php

namespace App\Http\Controllers\Modules\Others;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\FaqCategoryDetail;
use App\Models\FaqCategoryMaster;
use App\Models\FaqDetail;
use App\Models\FaqMaster;

use App\Models\PageContentAboutus;

use App\Models\Content;
use App\Models\ContentDetails;

use App\Models\PageContactContent;
use App\Mail\SendMailContactusDetails;
use Mail;
class OthersController extends Controller
{

	/**
	* Method: showAboutUs
	* Description: This method is used to show about us page
	* Author: Surajit
	*/
	public function showAboutUs() {
		$about_content = PageContentAboutus::where('language_id', getLanguage()->id)->first();
		return view('modules.static.about_us')->with([
            'about_content'=>@$about_content,
        ]);
	}

	/**
	* Method: showContactUs
	* Description: This method is used to show contact us page
	* Author: Surajit
	*/
	public function showContactUs() {
		$contact_content = PageContactContent::where('language_id', getLanguage()->id)->first();
		return view('modules.static.contact_us')->with([
            'contact_content'=>@$contact_content,
        ]);
	}

	/**
	* Method: showFaq
	* Description: This method is used to show faq page
	* Author: Surajit
	*/
	public function showFaq() {
		$faqcat = FaqCategoryMaster::with('categoryDetailsByLanguage')->get();
		$faqdetails = FaqMaster::with(['faqDetailsByLanguage', 'categoryByFaq'])->where('faq_category_id',1)->orderBy('id','desc')->get();
		return view('modules.static.faq')->with([
            'faqcat'=>@$faqcat,
            'faqdetails'=>@$faqdetails,
            //'brands'=>@$brands
            ]);
	}

	public function showDetailsFaq($id){
		$faqcat = FaqCategoryMaster::with('categoryDetailsByLanguage')->get();
		$faqdetails = FaqMaster::with(['faqDetailsByLanguage', 'categoryByFaq'])->where('faq_category_id',$id)->orderBy('id','desc')->get();
		return view('modules.static.faq_details')->with([
            'faqdetails'=>@$faqdetails,
            'faqcat'=>@$faqcat
            //'brands'=>@$brands
        ]);
	}

	/**
	* Method: showTerms
	* Description: This method is used to show terms page
	* Author: Surajit
	*/
	public function showTerms() {
		$term_content = ContentDetails::where('language_id', getLanguage()->id)->where('content_id', 1)->first();
		return view('modules.static.terms')->with([
            'term_content'=>@$term_content,
        ]);
	}

	/**
	* Method: showPrivacy
	* Description: This method is used to show privacy page
	* Author: Surajit
	*/
	public function showPrivacy() {
		$privecy_content = ContentDetails::where('language_id', getLanguage()->id)->where('content_id', 2)->first();
		return view('modules.static.privacy')->with([
            'privecy_content'=>@$privecy_content,
        ]);
	}

	/**
	* Method: showHelp
	* Description: This method is used to show help page
	* Author: Surajit
	*/
	public function showHelp() {
		$help = ContentDetails::where(['content_id'=>3,'language_id'=>getLanguage()->id])->first();
		return view('modules.static.help')->with(['help'=>@$help]);
	}

	/**
	* Method: postContactUsForm
	* Description: This method is used to send mail to admin when someone post contact form.
	* Author: Sanjoy
	*/
	public function postContactUsForm(Request $request) {
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$remoteip = $_SERVER['REMOTE_ADDR'];
		$data = [
				'secret' => config('services.recaptcha.secret'),
				'response' => $request->get('recaptcha'),
				'remoteip' => $remoteip
			];
		$options = [
				'http' => [
				'header' => "Content-type: application/x-www-form-urlencoded\r\n",
				'method' => 'POST',
				'content' => http_build_query($data)
				]
			];
		$context = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		$resultJson = json_decode($result);
		//dd($resultJson);
		if ($resultJson->success != true) {
				return back()->withErrors(['error' => 'ReCaptcha Error']);
				}
		if ($resultJson->score >= 0.3) {
			if(@$request->all()){
				$customerDetails 			= 	new \stdClass();
				$customerDetails->name 		= 	@$request->firstname." ".@$request->lastname;
				$customerDetails->email 	=	@$request->email;
				$customerDetails->phone 	=	@$request->phone;
				$customerDetails->message 	=	@$request->message;
				Mail::send(new SendMailContactusDetails($customerDetails));
				session()->flash("success", 'Your query has been successfully submitted.');
				return redirect()->back();
			}else{
				session()->flash("error");
				return redirect()->back();
			}
		} else {
				return back()->withErrors(['error' => 'ReCaptcha Error']);
		}
		//dd($request->all());
		
	}
}
