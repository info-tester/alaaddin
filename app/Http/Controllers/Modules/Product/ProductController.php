<?php

namespace App\Http\Controllers\Modules\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;

#Models
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\ProductVariantDetail;
use App\Models\Language;
use App\Models\ProductCategory;
use App\Models\Variant;
use App\Models\Category;
use App\Models\ProductNotification;

#repo
use App\Repositories\ProductRepository;
use App\Repositories\ProductOtherOptionRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;

use Auth;

class ProductController extends Controller
{
	protected $languageId, $products, $otherOptions;

    public function __construct(ProductRepository $products,
        ProductOtherOptionRepository $otherOptions,
        ProductCategoryRepository $product_cat,
        OrderMasterRepository $order,
        OrderDetailRepository $orderDetails
    )
    {
        $this->products     = $products;
        $this->otherOptions = $otherOptions;
        $this->product_cat  = $product_cat;
        $this->order        = $order;
        $this->orderDetails = $orderDetails;
    }

    private function setLanguage() {
      $lang = Language::where(['prefix' => Config::get('app.locale')])->first();
      $this->languageId = $lang->id;
    }

    private function getLanguageId() {
      return $this->languageId;
    }

    /**
    * Method: index
    * Description: This method is used to show product details
    * Author: Sanjoy
    */
    public function index($slug) {
    	$data = [];
    	$data['slug'] = $slug;
        $data['product'] = $this->products->select('id', 
            'slug', 
            'price', 
            'user_id', 
            'discount_price',
            'from_date',
            'to_date',
            'avg_review',
            'total_no_reviews',
            'product_code'
        )
        ->with([
            'images:product_id,image', 
            'productByLanguage',
            'productParentCategory.Category',
            'productSubCategory.Category',
            'productMarchant'
        ])
        ->where(['slug' => $slug, 'product_hide' => 'N', 'status' => 'A'])
        ->first();

        # IF SLUG NOT MATCHED THEN SEND TO 404 PAGE
        if(!@$data['product']) {
            return abort(404);
        }
        $productId = $data['product']->id;
        $data['product_features'] = $this->otherOptions->with([
            'productVariantDetails' => function($q) use($productId){
                $q->where('product_id', $productId);
            },
            'productVariantDetails.variantValueByLanguage',
            'productVariantByLanguage.variantByLanguage'
        ])
        ->where(['product_id' => $data['product']->id])
        ->whereHas('productVariantByLanguage', function($q) {
            $q->where('status', 'A');
        })
        ->groupBy('variant_id')
        ->get();

        $data['seller_products'] = $this->products->select('id', 
            'slug', 
            'price', 
            'user_id', 
            'discount_price',
            'from_date',
            'to_date',
            'avg_review',
            'total_no_reviews'
        )
        ->with([
            'defaultImage:product_id,image', 
            'productByLanguage',
            /*'productParentCategory.Category',
            'productSubCategory.Category',
            'productVariants.productVariantDetails',*/
            'productMarchant'
        ])
        ->where(['user_id' => $data['product']->user_id])
        ->where(['status' => 'A', 'seller_status' => 'A'])
        ->where('id', '!=', $data['product']->id)
        ->inRandomOrder()
        ->take(10)
        ->get();
        
        $data['reviews_product'] = $this->orderDetails->where(['product_id'=>@$data['product']->id,'status'=>'OD'])->where('rate', '>', 0)->get();
        $product =  $data['seller_products']->pluck('id');
        if(count($product) > 0) {
            $data['product_cat'] = $this->product_cat->whereIn('product_id', $product)->groupBy('category_id')->with('Category')->where('level', 'P')->get();
        } else {
            $data['product_cat'] = [];
        }
        return view('modules.product.product')->with($data);
    }

    /**
    * Method: getProduct
    * Description: This method is used to get product in vue component
    */
    public function getProduct($slug) {
    	$response = [
    		'jsonrpc' => '2.0'
    	];
    	$this->setLanguage();
    	$languageId = $this->getLanguageId();

    	$response['result']['product'] = Product::select(
            'id', 
            'slug', 
            'price',
            'discount_price',
            'stock',
            'from_date',
            'to_date',
            'total_review',
            'total_no_reviews',
            'avg_review',
            'product_code'
        )
        ->with([
            'images:product_id,image', 
            'productByLanguage',
            'productVariants.productVariantDetails',
            'productSubCategory.Category',
        ])
        ->where(['slug' => $slug])
        ->first();

        $productVariants = ProductVariant::where([
            'product_id' => $response['result']['product']->id
        ])
        ->pluck('id');
        $response['result']['all_variants'] = ProductVariantDetail::whereIn('product_variant_id', $productVariants)
        ->groupBy('variant_id')
        ->with([
            'variantValues' => function($q) use($productVariants) {
                $q->whereIn('product_variant_id', $productVariants);
            },
            'variantValues.variantValueByLanguage',
            'getVariant',
            'variantValues.variantValueName'
        ])
        ->with('variantByLanguage')
        ->orderBy('id')
        ->get();

        // get price dependent
        $category = ProductCategory::where(['level' => 'P', 'product_id' => $response['result']['product']->id])->first();
        $subCategory = ProductCategory::where(['level' => 'S', 'product_id' => $response['result']['product']->id])->first();
        $response['result']['price_dependent'] = ProductVariant::where('price', '!=', 0)->where(['product_id' => $response['result']['product']->id])->count();
        return response()->json($response, 200);
    }

    /**
    * Method: getVariantPrice
    * Description: This method is used to get variant price when selecting product options.
    * Author: Sanjoy
    */
    public function getVariantPrice(Request $request) {
        return $request->json()->all();
    }

    /**
    * Method: getAllVariants
    * Description: Find all available sets of the product.
    * Author: Sanjoy
    */
    public function getAllVariants($slug) {
        $response = [
            'jsonrpc'   => '2.0'
        ];
        $product = Product::where(['slug' => $slug])->first();
        $response['result']['all_variant_sets'] = ProductVariant::where(['product_id' => $product->id])->get();

        return response()->json($response, 200);
    }

    /**
    *   Method  : fetchProducts
    *   Use     : Auto complete product,category and subcategory list.  
    *   Author  : surajit
    */
    public function fetchProducts(Request $request){
        $response = array(
            'jsonrpc' => '2.0'
        );
        $response['result']['sub_categories'] = [];
        $keyword = $request->params['keyword'];
        if(@$keyword) {
            $parentCategory = Category::with([
                'categoryDetails',
                'subCategories' => function($q) {
                    $q->take(3);
                },
                'subCategories.categoryDetails'
            ])->where(['parent_id' => 0,'status' => 'A']);
            $parentCategory = $parentCategory->whereHas('categoryDetails', function($q1) use ($keyword){
                $q1->whereRaw("UPPER(title) LIKE '%". strtoupper($keyword)."%'");
            })->take(2)->get();
            if($parentCategory->count() == 0) {
                $subCategory = Category::with([
                    'categoryDetails',
                    'parentCatDetails'
                ])->where('parent_id',"!=",0)->where(['status' => 'A']);
                $subCategory = $subCategory->whereHas('categoryDetails', function($q2) use ($keyword){
                    $q2->whereRaw("UPPER(title) LIKE '%". strtoupper($keyword)."%'");
                })->get();    
                $response['result']['sub_categories'] = @$subCategory;
            }
            
            $response['result']['products'] = Product::with(['productByLanguage', 'productSubCategory.categoryByLanguage'])
            ->where('status', 'A')
            ->where('seller_status', 'A')
            ->where('product_hide', 'N')
            ->where(function($q1) use($keyword) {
                $q1->where('product_code', 'LIKE', '%' . $keyword . '%')
                ->orWhereHas('productByLanguage', function($q) use($keyword) {
                    $q->where('title', 'LIKE', '%'. $keyword .'%');
                });
            })
            ->take(4)->orderBy('id', 'DESC')->get();
            
            $response['result']['parent_categories'] = $parentCategory;
        } else {
            $response['error']['message'] = 'ERROR';    
        }
        return response()->json($response);     
    }

    /**
    *
    */
    public function generateProductCode() {
        $products = Product::get();
        foreach ($products as $key => $value) {
            $productId = sprintf('%06d', $value->id);
            $update['product_code'] = "AS".$productId;
            Product::where(['id' => $value->id])->update($update);
        }
    }

    /**
    * Method: updateFirebaseToken
    * Description: This method is used to update firebase token
    * Author: Sanjoy
    */
    public function updateFirebaseToken(Request $request) {
        $params = $request->params;
        if(@$params['variants']) {
            $variant = $this->getVariantId($params);
        }
        $email = '';
        if(@Auth::user()->email) {
            $email = @Auth::user()->email;
        } else {
            $email = @$params['email'];
        }
        
        $chkToken = ProductNotification::where([
            'firebase_token' => $params['token'], 
            'product_id'     => $params['product_id'], 
            'email'          => $email
        ])
        ->first();
        
        if(@$chkToken) {
            return response()->json([
                'jsonrpc' => '2.0',
                'result'  => [
                    'message' => 'Token already exist'
                ]
            ]);
        }
        
        $productNotification = ProductNotification::create([
            'product_id'        => $params['product_id'],
            'user_id'           => @Auth::user()->id,
            'email'             => $email,
            'variant_id'        => @$variant->id,
            'firebase_token'    => $params['token'],
            'device_type'       => 'W'
        ]);

        return response()->json([
            'jsonrpc' => '2.0',
            'result'  => $productNotification
        ]);
    }

    /**
    * Method: getVariantId
    * Description: This method is used to get variant ID.
    * Author: Sanjoy
    */
    private function getVariantId($reqData = []) {
        if(@$reqData['variants']) {
            $variants = ProductVariant::where(['product_id' => $reqData['product_id']])->get();
            foreach ($variants as $key => $value) {
                # checking variant with request variant ID's is available or not
                if(!array_diff($reqData['variants'], json_decode($value->variants))) {
                    # if previously added to cart then check stock with previous                    
                    return $value;
                } 
            }
        }
    }

}
