<?php

namespace App\Http\Controllers\Modules\GuestOrder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductVariantDetailRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CountryRepository;
use App\Repositories\UserRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\DriverRepository;
use App\Models\Payment;
use App\Mail\OrderMail;
use Mail;
use Auth;
use Session;
class GuestOrderController extends Controller
{
    protected $user, $cart, $cartDetails, $order, $orderDetails, $product, $productVariant, $productVariantDetail,$country;
    /**
	* __construct
	* This method is used to initilize the instance
	* Author: Sanjoy
	*/
	public function __construct(UserRepository $user,
		CartMasterRepository $cart,
		CartDetailRepository $cartDetails,
		OrderMasterRepository $order,
		OrderDetailRepository $orderDetails,
		OrderSellerRepository $orderSeller,
		ProductVariantRepository $productVariant,
		ProductRepository $product,
		ProductVariantDetailRepository $productVariantDetail,
		CountryRepository $country,
		MerchantRepository $merchants,
		ProductVariantRepository $product_variant,
        DriverRepository $drivers
	)
	{
		$this->user         			= $user;
		$this->cart         			= $cart;
		$this->cartDetails  			= $cartDetails;
		$this->order         			= $order;
		$this->orderDetails  			= $orderDetails;
		$this->orderSeller  			= $orderSeller;
		$this->productVariant  			= $productVariant;
		$this->productVariantDetail  	= $productVariantDetail;
		$this->product  				= $product;
		$this->country            		= $country;
		$this->merchants            	= $merchants;
		$this->product_variant          = $product_variant;
        $this->drivers                  = $drivers;
	}


	public function guestOrderDetails(Request $request){
		$order_no = decrypt($request->trackId);
		if(@$order_no){
			$order = $this->order->with([
				'customerDetails',
				'orderMasterDetails',
				'countryDetails',
				'orderMasterDetails',
				'orderMasterDetails.productDetails.productByLanguage',
				'orderMasterDetails.productDetails.defaultImage',
				'orderMasterDetails.productDetails.images',
				'orderMasterDetails.productDetails.productVariants',
				'orderMasterDetails.productVariantDetails',
				'orderMasterDetails.sellerDetails',
				'countryDetails',
				'orderMasterDetails.driverDetails'
			]);
		    $order = $order->where(function($q1) use($order_no) {
		      $q1 = $q1->where('order_no',$order_no);
		    });
		    $order = $order->where(function($q1)  {
		      $q1 = $q1->whereNotIn('status',['','D','I']);
		    });
		    $order = $order->first();
		    if(!@$order) {
		      abort(404);
		    }
			return view('modules.guest_order_details.guest_order_details')->with(['order'=>@$order]);	
		}else{
			  abort(404);
		}
		
	}
}
