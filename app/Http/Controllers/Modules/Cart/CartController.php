<?php

namespace App\Http\Controllers\Modules\Cart;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// repository
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductVariantDetailRepository;
use App\Repositories\ProductRepository;

use App\Models\CartDetail;
use App\Merchant;
use App\Models\Setting;
use App\Models\ShippingCost;

use Auth;

class CartController extends Controller
{
	protected $cart, $cartDetails, $product, $productVariant, $productVariantDetail;

	/**
	* __construct
	* This method is used to initilize the instance
	* Author: Sanjoy
	*/
	public function __construct(CartMasterRepository $cart,
								CartDetailRepository $cartDetails,
								ProductVariantRepository $productVariant,
								ProductRepository $product,
								ProductVariantDetailRepository $productVariantDetail
							)
	{
		$this->cart         			= $cart;
		$this->cartDetails  			= $cartDetails;
		$this->productVariant  			= $productVariant;
		$this->productVariantDetail  	= $productVariantDetail;
		$this->product  				= $product;
	}

	/**
	* Method: index
	* Description: This method is used to show the cart page.
	* Author: Sanjoy
	*/
	public function index() {
		$data = [];
		$data['cart'] = $this->cart->with([
			'cartDetails.productByLanguage', 
			'cartDetails.defaultImage', 
			'cartDetails.productMarchant',
			'cartDetails.getProduct',
			'cartDetails.getProduct',
			'cartDetails.productVariantDetails',
		])
		->orWhere(['session_id' => session()->getId(), 'user_id' => Auth::id()])
		->first();
		$data['language_id'] = getLanguage()->id;
		return view('modules.cart.cart')->with($data);
	}


	/**
	* Method: addToCart
	* Description: This method is used to add to cart
	* Author: Sanjoy
	*/
	public function addToCart(Request $request) {
		$response = [
			'jsonrpc' => '2.0'
		];
		$reqData = $request->params;
		
		# checking stock availability and correct variant ID.
		$productVariant = $this->checkStockAndVariant($reqData);
		if(!@$productVariant) {
			$avlQty = $this->checkAvailableQty($reqData);
			$response['error'] = __('errors.-5039', ['item' => $avlQty]);
			return response()->json($response);
		}
		# insert to cart master table.
		$cartMaster = $this->checkCartMaster($reqData);

		# if already available with cart master id and product id in cart details table the update it otherwise insert it.
		$cartDetails = $this->cartDetails->where([
			'product_id' 		=> $reqData['product_id'], 
			'cart_master_id' 	=> $cartMaster->id,
			'product_variant_id'=> @$productVariant->id
		])
		->first();
		if($cartDetails) {
			$this->updateCartDetails($cartMaster, $productVariant, $reqData, $cartDetails);
		} else {
			# insert to cart details page
			$this->insertCartDetails($cartMaster, $productVariant, $reqData);
		}

		$response['result']['cart'] = $this->calculateCart($cartMaster);

		/*if(Auth::id()) {
			$this->calculateShippingPrice($cartMaster);
		}*/

		$response['result']['message'] = __('success_site.-15002');
		return response()->json($response);
	}

	/**
	* Method: checkAvailableQty
	* Description: This method is used to checkavailable quantity.
	* Author: Sanjoy
	*/
	private function checkAvailableQty($reqData) {
		# if trying to add second time
		$cart = $this->cart->orWhere(['session_id' => session()->getId(), 'user_id' => Auth::id()])->first();
		if(@$reqData['variants']) {
			$variants = $this->productVariant->findWhere(['product_id' => $reqData['product_id']]);
			foreach ($variants as $key => $value) {
				# checking variant with request variant ID's is available or not
				if(!array_diff($reqData['variants'], json_decode($value->variants))) {
					# if previously added to cart then check stock with previous
					if(@$cart) {
						$cartDetails = $this->cartDetails->where(['cart_master_id' => $cart->id, 'product_id' => $reqData['product_id'], 'product_variant_id' => $value->id])->first();
						$availableQty = $value->stock_quantity - @$cartDetails->quantity;
					} else {
						$availableQty = $value->stock_quantity;
					}
				}
			}
		}
		# if product has no variants then check stock with product table.
		else {
			$product = $this->product->where(['id' => $reqData['product_id']])->first();
			if(@$cart) {
				$cartDetails = $this->cartDetails->where(['cart_master_id' => $cart->id, 'product_id' => $reqData['product_id']])->first();
				$availableQty = $product->stock - @$cartDetails->quantity;
			} else {
				$availableQty = $product->stock;
			}
		}
		return $availableQty;
	}

	/**
	* Method: checkStockAndVariant
	* Description: This method is used to check variant stock and correct variand ID.
	* Author: Sanjoy
	*/
	private function checkStockAndVariant($reqData = []) {
		$totalQty = $reqData['quantity'];
		# if trying to add second time
		$cart = $this->cart->orWhere(['session_id' => session()->getId(), 'user_id' => Auth::id()])->first();
		if(@$reqData['variants']) {
			$variants = $this->productVariant->findWhere(['product_id' => $reqData['product_id']]);
			foreach ($variants as $key => $value) {
				# checking variant with request variant ID's is available or not
				if(!array_diff($reqData['variants'], json_decode($value->variants))) {
					# if previously added to cart then check stock with previous
					if(@$cart) {
						$cartDetails = $this->cartDetails->where(['cart_master_id' => $cart->id, 'product_id' => $reqData['product_id'], 'product_variant_id' => $value->id])->first();
						$totalQty = $totalQty + @$cartDetails->quantity;
					}
					# stock available or not 
					if($value->stock_quantity >= $totalQty) {
						return $value;
					}
				} 
			}
		}
		# if product has no variants then check stock with product table.
		else {
			$product = $this->product->where(['id' => $reqData['product_id']])->first();
			if(@$cart) {
				$cartDetails = $this->cartDetails->where(['cart_master_id' => $cart->id, 'product_id' => $reqData['product_id']])->first();
				$totalQty = $totalQty + @$cartDetails->quantity;
			}
			if($product->stock >= $totalQty) {
				return 1;
			} 
		}
		return 0;
	}

	/**
	* Method: calculateShippingPrice
	* Description: This method is used to calculate shipping price
	* Author: Sanjoy
	*/
	private function calculateShippingPrice($cartMaster) {
		$sellers = $this->cartDetails->where(['cart_master_id' => $cartMaster->id])->pluck('seller_id')->toArray();
		$uniqueSellers = array_unique($sellers);
		$insideShippingCost = 0;
		$outsideShippingCost = 0;
		foreach ($uniqueSellers as $key => $value) {
			$merchant = Merchant::where(['id' => $value])->first();
			if($merchant->country == 134 && Auth::user()->country == 134) {
				$insideShippingCost += $merchant->inside_shipping_cost;
			} else {
				$details = CartDetail::where(['cart_master_id' => $cartMaster->id, 'seller_id' => $value])->get();
				$weight = $details->sum('weight');

				$infinityChk = ShippingCost::where('infinity_weight', 'Y')->first();
				if($infinityChk && @$weight >= $infinityChk->from_weight) {
					$sCost = $infinityChk;
				} else {
					$sCost = ShippingCost::where(function($where) use ($weight) {
	                    $where->where(function($where1) use ($weight) {
	                        $where1->where('from_weight', '<=', @$weight)
	                                ->where('to_weight', '>', @$weight);

	                    })
	                    ->orWhere(function($where2) use ($weight) {
	                        $where2->where('from_weight', '<', @$weight)
	                            ->where('to_weight', '>=', @$weight);
	                    });
	                })
	                ->first();
	            }

                $outsideShippingCost += @$sCost->internal_order_rate;
			}
		}

		$cart = $this->cart->where(['id' => $cartMaster->id])->first();
		$this->cart->where(['id' => $cartMaster->id])->update(['shipping_price' => $outsideShippingCost + $insideShippingCost, 'total' => $cart->total + $outsideShippingCost + $insideShippingCost]);
	}

	/**
	* Method: checkCartMaster
	* Description: This method is used to check is it previously added in cart or not
	* Author: Sanjoy
	*/
	private function checkCartMaster($reqData = []) {
		$cart = $this->cart->orWhere(['session_id' => session()->getId(), 'user_id' => Auth::id()])->first();
		if(!@$cart) {
			return $this->insertToCartMaster($reqData);
		} else {
			return $cart;
		}
	}

	/**
	* Method: insertToCartMaster
	* Description: This method is used to insert cart master table.
	* Author: Sanjoy
	*/
	private function insertToCartMaster($reqData = []) {
		return $this->cart->create([
			'user_id' 		=> @Auth::id()?Auth::id():0,
			'session_id'	=> session()->getId(),
		]);
	}

	/**
	* Method: insertCartDetails
	* Description: This method is used to insert cart details table.
	* Author: Sanjoy
	*/
	private function insertCartDetails($cartMaster, $productVariant = [], $reqData = []) {
		# get product information
		$product = $this->getProduct($reqData['product_id']);
		
		$originalPrice = 0;
		$discountedPrice = 0;
		$total_discount = 0;
		$subtotal = 0;
		$total = 0;
		$weight = 0;
		if(@$productVariant->id) {
			# for price and stock dependent
			if(@$productVariant->price != 0) {
				#if discount price is available then calculate this.
				$subtotal = $total = $productVariant->price * $reqData['quantity'];
				$originalPrice = $productVariant->price;
				if($productVariant->discount_price && (date('Y-m-d')>=$productVariant->from_date && date('Y-m-d')<=$productVariant->to_date)) {
					$total_discount = ($productVariant->price - $productVariant->discount_price) * $reqData['quantity'];
					$discountedPrice = $productVariant->discount_price;
				}
				$total = $subtotal - $total_discount;
			} else {
				$originalPrice = $product->price;
				if($product->discount_price != 0 && (date('Y-m-d')>=$product->from_date && date('Y-m-d')<=$product->to_date)) {
					$discountedPrice = $product->discount_price;
					$total_discount = ($product->price - $product->discount_price) * $reqData['quantity'];
				}
				$subtotal = $product->price * $reqData['quantity'];
				$total = $subtotal - $total_discount;
			}
			
			$weight = $productVariant->weight * $reqData['quantity'];
			# to insert variant data to table
			$variants = $this->productVariantDetail->with([
				'getVariantValueDetails', 
				'getVariantDetail'
			])
			->where(['product_variant_id' => $productVariant->id])
			->get();

			$variantData = [];
			foreach ($variants as $key => $value) {
				foreach ($value->getVariantDetail as $key1 => $value1) {
					$variantData[$key][$value1->language_id]['variant'] = $value1->name; 
					$variantData[$key][$value1->language_id]['variant_value'] = $value->getVariantValueDetails[$key1]->name; 
				}
			}
		} else {
			$originalPrice = $product->price;
			if($product->discount_price != 0 && (date('Y-m-d')>=$product->from_date && date('Y-m-d')<=$product->to_date)) {
				$discountedPrice = $product->discount_price;
				$total_discount = ($product->price - $product->discount_price) * $reqData['quantity'];
			}
			$subtotal = $product->price * $reqData['quantity'];
			$total = $subtotal - $total_discount;
			$weight = $product->weight * $reqData['quantity'];
			
		}
		// dd($discountedPrice);
		return $this->cartDetails->create([
			'cart_master_id' 	=> $cartMaster->id,
			'product_id'		=> $reqData['product_id'],
			'product_variant_id'=> @$productVariant->id,
			'seller_id' 		=> $product->user_id,
			'quantity'			=> $reqData['quantity'],
			'original_price'	=> $originalPrice,
			'discounted_price' 	=> $discountedPrice,
			'total_discount' 	=> $total_discount,
			'subtotal'			=> $subtotal,
			'total'				=> $total,
			'variants'			=> @json_encode(@$variantData),
			'weight'			=> $weight
		]);
	}

	/**
	* 
	*/
	private function updateCartDetails($cartMaster, $productVariant = [], $reqData = [], $cartDetails) {
		# get product information
		$product = $this->getProduct($reqData['product_id']);
		$originalPrice = 0;
		$discountedPrice = 0;
		$total_discount = 0;
		$subtotal = 0;
		$total = 0;
		$weight = 0;
		if(@$productVariant->price != 0) {
			#if discount price is available then calculate this.
			$subtotal = $cartDetails->subtotal + ($productVariant->price * $reqData['quantity']);
			if($productVariant->discount_price != 0 && (date('Y-m-d')>=$productVariant->from_date && date('Y-m-d')<=$productVariant->to_date)) {
				$total_discount = $cartDetails->total_discount + (($productVariant->price - $productVariant->discount_price) * $reqData['quantity']);
			}
			$total = $subtotal - $total_discount;
			$weight = $cartDetails->weight + ($productVariant->weight * $reqData['quantity']);
		} else {
			if($product->discount_price != 0 && (date('Y-m-d')>=$product->from_date && date('Y-m-d')<=$product->to_date)) {
				$total_discount = $cartDetails->total_discount + ($product->price - $product->discount_price) * $reqData['quantity'];
			}
			$subtotal = $cartDetails->subtotal + ($product->price * $reqData['quantity']);
			$total = $subtotal - $total_discount;
			$weight = $cartDetails->weight + ($product->weight * $reqData['quantity']);
		}
		
		$this->cartDetails->where(['id' => $cartDetails->id])
			->update([
				'quantity' 			=> $cartDetails->quantity + $reqData['quantity'],
				'subtotal'			=> $subtotal,
				'total_discount' 	=> $total_discount,
				'total'				=> $total,
				'weight'			=> $weight
			]);
	}

	/**
	* Method: calculateCart
	* Description: This method is used to calculate cart.
	* Author: Sanjoy
	*/
	private function calculateCart($cartMaster) {
		$cartDetails = $this->cartDetails->where(['cart_master_id' => $cartMaster->id])->get();
		$total_discount = $cartDetails->sum('total_discount');
		$cartMaster->where(['id' => $cartMaster->id])->update([
			'total_discount' => $total_discount,	
			'subtotal' 		 => $cartDetails->sum('subtotal'),
			'total'		     => $cartDetails->sum('total'),
			'total_item'	 => $cartDetails->count(),
			'total_qty'		 =>	$cartDetails->sum('quantity')
		]);
		return $this->cart->where(['id' => $cartMaster->id])->first(); 
	}

	/**
	* Mehtod: updateCart,
	* Description: This method is used to update cart quantity
	* Author: Sanjoy
	*/
	public function updateCart(Request $request) {
		$reqData = $request->params;

		# checking stock availability and correct variant ID.
		$productVariant = $this->checkStockAndVariant1($reqData);
		if(!@$productVariant) {
			$avlQty = $this->checkAvailableQty1($reqData);
			$response['error'] = __('errors.-5039', ['item' => $avlQty]);
			return response()->json($response);
		}

		$cartDetails = $this->cartDetails->where(['id' => $reqData['cart_details_id']])->first();
		$cartMaster = $this->cart->where(['id' => $cartDetails->cart_master_id])->first();
		$productVariant = $this->productVariant->where(['id' => $reqData['product_variant_id']])->first();
		$this->updateCartQuantity($cartMaster, $productVariant, $reqData, $cartDetails);

		/*if(Auth::id()) {
			$this->calculateShippingPrice($cartMaster);
		}*/
		$response['result']['cart'] = $this->calculateCart($cartMaster);
		$response['result']['cart_details'] = $this->cartDetails->where([
						'id' => $reqData['cart_details_id'], 
					])
					->first();
		$response['result']['message'] = __('success_site.-15002');
		return response()->json($response);
	}

	/**
	* Method: checkAvailableQty
	* Description: This method is used to checkavailable quantity.
	* Author: Sanjoy
	*/
	private function checkAvailableQty1($reqData) {
		# if trying to add second time
		$cart = $this->cart->orWhere(['session_id' => session()->getId(), 'user_id' => Auth::id()])->first();
		if(@$reqData['product_variant_id']) {
			$variants = $this->productVariant->where(['id' => $reqData['product_variant_id']])->first();
			# if previously added to cart then check stock with previous
			if(@$cart) {
				$cartDetails = $this->cartDetails->where(['cart_master_id' => $cart->id, 'product_id' => $reqData['product_id'], 'product_variant_id' => $reqData['product_variant_id']])->first();
				$availableQty = $variants->stock_quantity - @$cartDetails->quantity;
			} else {
				$availableQty = $variants->stock_quantity;
			}
		}
		# if product has no variants then check stock with product table.
		else {
			$product = $this->product->where(['id' => $reqData['product_id']])->first();
			if(@$cart) {
				$cartDetails = $this->cartDetails->where(['cart_master_id' => $cart->id, 'product_id' => $reqData['product_id']])->first();
				$availableQty = $product->stock - @$cartDetails->quantity;
			} else {
				$availableQty = $product->stock;
			}
		}
		return $availableQty;
	}

	/**
	* Method: checkStockAndVariant1
	* Description: This method is used to check variant stock and correct variand ID.
	* Author: Sanjoy
	*/
	private function checkStockAndVariant1($reqData = []) {
		$totalQty = $reqData['quantity'];
		# when trying to updating quantity from cart page.
		if(@$reqData['product_variant_id']) {
			$variants = $this->productVariant->where(['id' => $reqData['product_variant_id']])->first();
			# stock available or not 
			if($variants->stock_quantity >= $totalQty) {
				return $variants;
			}
		} 
		# if product has no variants then check stock with product table.
		else {
			$product = $this->product->where(['id' => $reqData['product_id']])->first();
			if($product->stock >= $totalQty) {
				return 1;
			} 
		}
		return 0;
	}

	/**
	* Method: updateCartQuantity
	* Description: This method is used to update qty 
	*/
	private function updateCartQuantity($cartMaster, $productVariant = [], $reqData = [], $cartDetails) {
		# get product information
		$product = $this->getProduct($reqData['product_id']);

		$originalPrice = 0;
		$discountedPrice = 0;
		$total_discount = 0;
		$subtotal = 0;
		$total = 0;
		$weight = 0;
		if(@$productVariant->price != 0) {
			#if discount price is available then calculate this.
			$subtotal = $productVariant->price * $reqData['quantity'];
			if($productVariant->discount_price && (date('Y-m-d')>=$productVariant->from_date && date('Y-m-d')<=$productVariant->to_date)) {
				$total_discount = ($productVariant->price - $productVariant->discount_price) * $reqData['quantity'];
			}
			$total = $subtotal - $total_discount;
			$weight = $productVariant->weight * $reqData['quantity'];
		} else {
			if($product->discount_price != 0 && (date('Y-m-d')>=$product->from_date && date('Y-m-d')<=$product->to_date)) {
				$total_discount = ($product->price - $product->discount_price) * $reqData['quantity'];
			}
			$subtotal = $product->price * $reqData['quantity'];
			$total = $subtotal - $total_discount;
			$weight = $product->weight * $reqData['quantity'];
		}
		
		$this->cartDetails->where(['id' => $cartDetails->id])
		->update([
			'quantity' 			=> $reqData['quantity'],
			'subtotal'			=> $subtotal,
			'total_discount' 	=> $total_discount,
			'total'				=> $total,
			'weight'			=> $weight
		]);
	}

	/**
	* Method: getProduct
	* Description: This method is used to get product details.
	* Author: Sanjoy
	*/
	private function getProduct($productId = NULL) {
		return $this->product->where(['id' => $productId])->first();
	}

	/**
	* Method: removeCart 
	* Description: This method is used to remove item from cart.
	* Author: Sanjoy
	*/
	public function removeCart($cartDetailsId) {
		$cartDetail = $this->cartDetails->where(['id' => $cartDetailsId])->first();
		$cartMaster = $this->cart->where(['id' => $cartDetail->cart_master_id])->first();

		$this->cartDetails->where(['id' => $cartDetailsId])->delete();

		$allDetails = $this->cartDetails->where(['cart_master_id' => $cartMaster->id])->get();

		if($allDetails->count()) {
			$this->calculateCart($cartMaster);
			/*if(Auth::id()) {
				$this->calculateShippingPrice($cartMaster);
			}*/
		} else {
			$this->cart->where(['id' => $cartMaster->id])->delete();
		}

		
		return redirect()->back();
	}
}
