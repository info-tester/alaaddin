<?php

namespace App\Http\Controllers\Modules\WhatsApp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Twilio\Rest\Client;

use App\Models\OrderMaster;
use App\Models\OrderDetail;
use App\Models\OrderSeller;
use App\Merchant;
use Auth;

class WhatsAppController extends Controller
{
	protected $sid, $authToken;
	/**
	* Method: __construct
	* Description: This method is used to initilize the object
	* Author: Sanjoy
	*/
	public function __construct() {
		$this->sid = env('TWILIO_SID');
		$this->authToken  = env('TWILIO_TOKEN');
	}

    /**
    * Method: sendMessage
    * Description: This method is used to send message to customer
    * Author: Sanjoy
    */
    public function sendMessage($orderId = NULL) {
    	$order = OrderMaster::with([
            'orderMasterDetails', 
            'getBillingCountryCode'
        ])->where(['id' => $orderId])->first();
        //dd($order);
		$twilio = new Client($this->sid, $this->authToken);
        try {
            #send message in english
            $message = $twilio->messages
            ->create("whatsapp:" . $order->getBillingCountryCode->country_code . $order->billing_phone, [
                "from" => "whatsapp:".env('TWILIO_FROM_NO'),
                "body" => $this->getMessageEn($order)
            ]);
            //dd($message);
            #send message in arabic
            $message = $twilio->messages
            ->create("whatsapp:" . $order->getBillingCountryCode->country_code . $order->billing_phone, [
                "from" => "whatsapp:".env('TWILIO_FROM_NO'),
                "body" => $this->getMessageAr($order)
            ]);
        } catch(\Exception $e) {
        }
    }

    /**
    * Method: sendMessageMerchants,
    * Description: This method is used to send whatsapp message to merchants
    * Author: Sanjoy
    */
    public function sendMessageMerchants($orderId = NULL) {
        $order = OrderMaster::with(['orderMasterDetails'])->where(['id' => $orderId])->first();
        $sellers = OrderSeller::with(['orderMerchant'])->where(['order_master_id' => $orderId])->pluck('seller_id');
        $merchantsPhone = Merchant::with(['Country'])->whereIn('id', $sellers)->get();
        $twilio = new Client($this->sid, $this->authToken);
        
        foreach ($merchantsPhone as $key => $value) {
            try {
                #send message in english
                $message = $twilio->messages
                ->create("whatsapp:" . $value->Country->country_code . $value->phone, [
                    "from" => "whatsapp:".env('TWILIO_FROM_NO'),
                    "body" => $this->getMessageEn($order)
                ]);

                #send message in arabic
                $message = $twilio->messages
                ->create("whatsapp:" . $value->Country->country_code . $value->phone, [
                    "from" => "whatsapp:".env('TWILIO_FROM_NO'),
                    "body" => $this->getMessageAr($order)
                ]);
            } catch(\Exception $e) {

            }
        }
    }

    /**
    * Method: getMessageAr
    * Description: This method is used to getMessage
    * Author: Sanjoy
    */
    private function getMessageAr($order = []) {
    	$messages = [
            'merchant_new_order' => "لقد تم تسجيل طلب جديد ".$order->order_no."# لديكم في موقع اسواقنا .. \nالرجاء تجهيز الطلب بأسرع وقت ممكن \nشاكرين لكم حسن تعاونكم\n\n",
            'order_picked_up' => "لقد تم تجميع طلبكم  ".$order->order_no."# من موقع اسواقنا بنجاح .. 👍🏼\n🚗💨 سوف يتم توصيل طلبكم بأسرع وقت ممكن  .. \nسعيدين بخدمتكم",
            'order_delivered' => "لقد تم توصيل طلبكم ".$order->order_no."#  بنجاح .. \nسعيدين لخدمتكم .. \nDriver Group Company  ... 🚙\n\n الان يمكنكم  اقتناء كل ما تحتاجونه في سلة واحده \n📦 🛍️ www.aswagna.co من خلال موقعنا \n\n\n",
            'order_delivered' => "لقد تم توصيل طلبكم ".$order->order_no."#  بنجاح .. \nسعيدين لخدمتكم .. \nDriver Group Company  ... 🚙\n\n الان يمكنكم  اقتناء كل ما تحتاجونه في سلة واحده \n📦 🛍️ \nمن خلال الرابط التالي\nhttps://bit.ly/Aswagna",
        ];
    	if($order->status == 'N' || $order->status == 'DA') { // new order message will sent to merchant
    		$message = $messages['merchant_new_order'];
    	} else if($order->status == 'OP') { // order picked up message will sent to customer
    		$message = $messages['order_picked_up'];
    	}else if($order->status == 'OD') { // order delivered message will sent to customer for internal and external
    		$message = $messages['order_delivered'];
    	}
    	return $message;
    }


    /**
    * Method: getMessageEn
    * Description: This method is used to getMessage
    * Author: Sanjoy
    */
    private function getMessageEn($order = []) {
        $messages = [
            'merchant_new_order' => "You got a new order #".$order->order_no." in Aswagna.co website. Please prepare it as fast as possible.\nThank you for your collaboration.\n\n📞 22213707\n📧 Info@aswagna.co",
            'order_picked_up' => "Your order #".$order->order_no." is fulfilled and it will be delivered as soon as possible. \nIt is our pleasure to serve you.",
            /*'order_delivered' => "Your order #".$order->order_no." is delivered. It is our pleasure to serve you.\n\nYou can shop again using our website\nwww.aswagna.co\n\n",*/
            'order_delivered' => "Your order #".$order->order_no." is delivered. It is our pleasure to serve you.\n\nYou can shop again using our website\nhttps://bit.ly/Aswagna"
        ];
        if($order->status == 'N' || $order->status == 'DA') { // new order message will sent to merchant
            $message = $messages['merchant_new_order'];
        } else if($order->status == 'OP') { // order picked up message will sent to customer
            $message = $messages['order_picked_up'];
        }else if($order->status == 'OD') { // order delivered message will sent to customer for internal and external
            $message = $messages['order_delivered'];
        }
        return $message;
    }
}
