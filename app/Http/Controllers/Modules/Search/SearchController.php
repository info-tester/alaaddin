<?php

namespace App\Http\Controllers\Modules\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// models
use App\Models\Product;
use App\Models\Category;
use App\Models\Variant;
use App\Models\Brand;
use App\Models\ProductCategory;
use App\Models\ProductVariant;
use App\Models\ProductOtherOption;

use App\Repositories\BrandRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\UserFavoriteRepository;

use App;
use Auth;

class SearchController extends Controller
{
    protected $brands, $products, $userFavorite;

    public function __construct(BrandRepository $brands,
                                ProductRepository $products,
                                CategoryRepository $category,
                                UserFavoriteRepository $userFavorite
                            )
    {
        $this->brands           = $brands;
        $this->products         = $products;
        $this->category         = $category;
        $this->userFavorite     = $userFavorite;
    }

	/**
	* Method: search
	* Description: This method is used to search in laravel
    * Author: Sanjoy
	*/
    public function index(Request $request, $category = NULL) {
        $data = [];
        if(@$category) {
            $data['category'] = Category::with(['categoryByLanguage'])->where('slug', $category)->first();
        }
    	return view('modules.search.search')->with($data);
    }

    /**
    * Method: searchProductsVue
    * Description: This method is used to search in laravel on vue request
    * Author: Sanjoy
    */
    public function searchProductsVue(Request $request) {
        $reqData = $request->json()->all();
    	$response = [
    		'jsonrpc'	=> '2.0'
    	];
    	$data = [];
    	$data['products'] = $this->products->with([
    									'productVariants.productVariantDetails.variantValueByLanguage', 
    									'productCategory',
    									'defaultImage',
                                        'productOtherVariants',
                                        'productByLanguage',
                                        'productMarchant:id,fname,lname',
                                        'wishlist'
    								])
                                    ->whereHas('productMarchant', function($q) {
                                        $q->where('hide_merchant', 'N');
                                    });
        
    	// if params is category then all variants of this category
    	if(@$reqData['sub_category']) {
    		$category = $this->category->with(['subCategories'])->where(['slug' => urldecode($reqData['sub_category'])])->first();

    		$data['products'] = $data['products']->whereHas('productCategory', function($query) use($category) {
    			$query->where('category_id', $category->id);
    			$query->where('level', 'S');
    		});
    	} elseif(@$reqData['category']) {
    		$cat = $this->category->where(['slug' => $reqData['category']])->first();
    		if($cat) {
    			$data['products'] = $data['products']->whereHas('productCategory', function($query) use($cat) {
	    			$query->where('category_id', $cat->id);
	    			$query->where('level', 'P');
	    		});
    		}
    	}
    	if(@$reqData['brands']) {
    		$data['products'] = $data['products']->whereIn('brand_id', $reqData['brands']);
    	}

    	if(@$reqData['variant']) {
    		$variants = $reqData['variant'];
			$data['products'] = $data['products']->whereHas('productVariants', function($q) use($variants) {
                foreach ($variants as $key => $value) {
                    if($value) {
	    				$q->whereHas('productVariantDetails', function($q1) use($key, $value) {
		    				$q1->where('variant_id', $key)
		    					->whereIn('variant_value_id', $value);
		    			});
                    }
                }
			});
    	}

        if(@$reqData['other_variant']) {
            foreach ($reqData['other_variant'] as $key => $value) {
                if($value) {
                    $data['products'] = $data['products']->whereHas('productOtherVariants', function($q1) use($key, $value) {
                        $q1 = $q1->where('variant_id', $key)
                                ->whereIn('variant_value_id', $value);
                    });
                }
            }
        }

        $response['result']['min_price'] = $data['products']->min('price');
        $response['result']['max_price'] = $data['products']->max('price');
        
        if(@$reqData['price']) {
            $data['products'] = $data['products']->whereBetween('price', $reqData['price']);
        }

        $data['products'] = $data['products']->where(['status' => 'A', 'seller_status' => 'A', 'product_hide' => 'N']);
        
        # for keyword search
        if(@$reqData['keyword']) {
            $keyword = $reqData['keyword'];
            $data['products'] = $data['products']->where(function($q1) use($keyword) {
                $q1->where('product_code', 'LIKE', '%' . $keyword . '%')
                ->orWhereHas('productByLanguage', function($q) use($keyword) {
                    $q->where('title', 'LIKE', '%'. $keyword .'%');
                });
            });
            $productIds = $data['products']->pluck('id');

            $response['result']['categories'] = ProductCategory::whereIn('product_id', $productIds)
            ->where('level', 'P')
            ->with(['categoryByLanguage', 'Category.subCategories.categoryByLanguage', 'Category.subCategories' => function($q) use($productIds) {
                $q->where('status', 'A');
                $q->whereHas('productBySubCategory', function($q1) use($productIds){
                    $q1->whereIn('product_id', $productIds);
                });
            }])
            ->groupBy('category_id')
            ->get();
        }

        if($reqData['order_by'] == 'H') {
            $data['products'] = $data['products']->orderBy('price', 'DESC');    
        } else if($reqData['order_by'] == 'L') {
            $data['products'] = $data['products']->orderBy('price', 'ASC');    
        } else if($reqData['order_by'] == 'N') {
            $data['products'] = $data['products']->orderBy('id', 'DESC');    
        } else if($reqData['order_by'] == 'A') {
            // $data['products'] = $data['products']->orderBy('price', 'DESC');    
        }

        $response['result']['products'] = $data['products']->paginate($reqData['per_page']);
    	return response()->json($response);
    }

    /**
    * Method: getProductPrice
    * Description: This method is used to get product min and max price
    * Author: Sanjoy
    */
    public function getProductPrice() {

    }

    /**
    * Method: getBrands
    * Description: This method is used to get available brands associated to products
    * Author: Sanjoy
    */
    public function getBrands($catSlug) {
        $response = [
            'jsonrpc' => '2.0'
        ];
        $category = $this->category->where(['slug' => $catSlug])->first();
        if(@$category->parent_id != 0) {
            $category = $this->category->where(['id' => $category->parent_id])->first();
        }

        $productIds = ProductCategory::where('category_id', @$category->id)
        ->whereHas('activeProducts')
        ->pluck('product_id')
        ->toArray();
        $brandIds = Product::whereIn('id', $productIds)->pluck('brand_id')->toArray();

        $response['result']['brands'] = $this->brands->with(['brandDetailsByLanguage'])
            ->withCount('productsByBrand')
            ->whereHas('productsByBrand', function($query) {
                $query->where('status', 'A');
                $query->where('product_hide', 'N');
            }, '>=', 1)
            // ->whereIn('id', $brandIds)
            ->where(['status' => 'A', 'category_id' => @$category->id])->get();
        return response()->json($response, 200);
    }

    /**
    *
    */
    public function getParentCategory($subCatId) {
    	$response = [
    		'jsonrpc'	=> '2.0'
    	];
        $response['result']['category'] = $this->category->where(['status' => 'A'])
                                            ->where('slug', $subCatId)
                                            ->with(['categoryByLanguage'])
                                            ->withCount(['productBySubCategory' => function($query) {
                                                $query->whereHas('activeProducts', function($q) {
                                                    $q->where('status', 'A');
                                                    $q->where('product_hide', 'N');
                                                });
                                            }])
                                            ->first();

    	$response['result']['parent_category'] = $this->category->where(['status' => 'A'])
    										->where('id', $response['result']['category']->parent_id)
    										->with(['categoryByLanguage'])
                                            ->withCount(['productByCategory' => function($query) {
                                                $query->whereHas('activeProducts', function($q) {
                                                    $q->where('status', 'A');
                                                    $q->where('product_hide', 'N');
                                                });
                                            }])
    										->first();
    	return response()->json($response, 200);
    }

    /**
    * 
    */
    public function getSubCategories($slug) {
    	$response = [
    		'jsonrpc'	=> '2.0'
    	];

        // parent category details
    	$response['result']['parent_category'] = Category::with(['categoryByLanguage'])
                            ->withCount(['productBySubCategory' => function($query) {
                                $query->whereHas('activeProducts', function($q) {
                                    $q->where('status', 'A');
                                    $q->where('product_hide', 'N');
                                });
                            }])
                            ->where(['slug' => $slug])->first();
                            // return $response['result']['parent_category']->id;

        // all sub category details 
    	$response['result']['sub_categories'] = $this->category->where(['status' => 'A'])
    										->where('parent_id', @$response['result']['parent_category']->id)
    										->with(['categoryByLanguage'])
                                            ->withCount(['productBySubCategory' => function($query) {
                                                $query->whereHas('activeProducts', function($q) {
                                                    $q->where('status', 'A');
                                                    $q->where('product_hide', 'N');
                                                });
                                            }])
    										->whereHas( 'productBySubCategory', function($query) {
                                                $query->whereHas('activeProducts', function($q) {
                                                    $q->where('status', 'A');
                                                    $q->where('product_hide', 'N');
                                                });
                                            })
    										->get();
    	return response()->json($response, 200);
    }

    /**
    * Method: getVariants
    * Description: This method is used to get variants
    * Author: Sanjoy
    */
    public function getVariants($id) {
        $response = [
            'jsonrpc'   => '2.0'
        ];
        $productIds = ProductCategory::where('category_id', $id)->pluck('product_id')->toArray();
        $productVariants = ProductVariant::whereIn('product_id', $productIds)->pluck('variants')->toArray();
        $variantValues = [];
        foreach ($productVariants as $key => $value) {
            foreach (json_decode($value) as $key => $value1) {
                array_push($variantValues, json_decode($value1));
            }
        }
        $variantValues = array_unique($variantValues);
        $response['result']['variants'] = Variant::with([
                            'variantByLanguage',
                            'variantValues' => function($q) use($variantValues) {
                                $q->whereIn('id', $variantValues);
                            },
                            'variantValues.variantValueByLanguage'
                        ])
                        ->where('sub_category_id', $id)
                        ->where('status', 'A')
                        ->whereIn('type', ['P', 'S'])
                        ->get();
        
        $productOtherVariants = ProductOtherOption::whereIn('product_id', $productIds)->pluck('variant_value_id')->toArray();
        $productOtherVariants = array_unique($productOtherVariants);

        $response['result']['other_variants'] = Variant::with([
                            'variantByLanguage',
                            'variantValues' => function($q) use($productOtherVariants) {
                                $q->whereIn('id', $productOtherVariants);
                            },
                            'variantValues.variantValueByLanguage'
                        ])
                        ->where('sub_category_id', $id)
                        ->where('status', 'A')
                        ->whereIn('type', ['SH'])
                        ->get();
        return response()->json($response, 200);
    }

    /**
    * Method: checkCatOrSubCat
    * Description: This method is used to check the sulg is for category or sub category
    * Author: Sanjoy
    */
    public function checkCatOrSubCat($slug) {
        $response = [
            'jsonrpc'   => '2.0'
        ];
        $response['result']['category'] = $category = $this->category
                            ->with(['categoryByLanguage'])
                            ->where(['slug' => $slug])
                            ->first();
        if(@$category->parent_id) {
            $response['result']['is_category'] = false;
        } else {
            $response['result']['is_category'] = true;
        }
        return response()->json($response);
    }

    /**
    * Method: addToFev
    * Description: This method is used to add to favorite list.
    * Author: Sanjoy
    */
    public function addToFev($id) {
        $response = [
            'jsonrpc' => '2.0'
        ];
        if(!Auth::id()) {
            $response['error']['message'] = __('error.-5034');
            return response()->json($response);
        }
        #checking if already exist or not.
        $userFav = $this->userFavorite->where(['product_id' => $id, 'user_id' => Auth::id()])->first();
        
        #if already not added to wishlist the insert to wishlist table
        if(!@$userFav) {
            $checkProductWishCount = Product::where('id',$id)->first();
            $newCount = $checkProductWishCount->tot_wishlist_done + 1;
            Product::where('id',$id)->update(['tot_wishlist_done' => $newCount]);
            
            $this->userFavorite->create([
                'user_id'       => Auth::id(),
                'product_id'    => $id
            ]);
            $response['result']['message'] = __('success_site.-15000');
        } else {
            $this->userFavorite->where([
                'user_id'       => Auth::id(),
                'product_id'    => $id
            ])->delete();
            $response['result']['message'] = __('success_site.-15001');
        }
        return response()->json($response);
    }
}
