<?php

namespace App\Http\Controllers\Modules\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// repository
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductVariantDetailRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CountryRepository;
use App\Repositories\UserRepository;
use App\Repositories\MerchantRepository;

use App\Models\UserReward;
use App\Models\OrderMaster;
use App\Models\OrderDetail;
use App\Models\MerchantRegId;
use App\Models\Setting;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Coupon;
use App\Models\CouponToMerchant;
use App\Models\OrderStatusTiming;
use App\Models\OrderSeller;

use App\Mail\OrderMail;
// use App\Mail\Setting;
use App\Driver;
use App\User;
use App\Mail\SendOrderDetailsMerchantMail;
use Mail;
use Auth;
use Session;

# use whatsapp controller
use App\Http\Controllers\Modules\WhatsApp\WhatsAppController;

use App\Http\Controllers\Modules\Payment\PaymentController;

class OrderController extends Controller
{
	protected $user, 
              $merchant, 
              $cart, 
              $cartDetails, 
              $order, 
              $orderDetails, 
              $orderSeller, 
              $product, 
              $productVariant, 
              $productVariantDetail, 
              $country, 
              $payment;

    # whatsapp send message
    protected $whatsApp;

	/**
	* __construct
	* This method is used to initilize the instance
	* Author: Sanjoy
	*/
	public function __construct(UserRepository $user,
        MerchantRepository $merchant,
        CartMasterRepository $cart,
        CartDetailRepository $cartDetails,
        OrderMasterRepository $order,
        OrderDetailRepository $orderDetails,
        OrderSellerRepository $orderSeller,
        ProductVariantRepository $productVariant,
        ProductRepository $product,
        ProductVariantDetailRepository $productVariantDetail,
        CountryRepository $country,
        WhatsAppController $whatsApp,
        PaymentController $payment
    )
	{
		$this->user         			= $user;
        $this->merchant                 = $merchant;
        $this->cart         			= $cart;
        $this->cartDetails  			= $cartDetails;
        $this->order         			= $order;
        $this->orderDetails  			= $orderDetails;
        $this->orderSeller  			= $orderSeller;
        $this->productVariant  			= $productVariant;
        $this->productVariantDetail  	= $productVariantDetail;
        $this->product  				= $product;
        $this->country            		= $country;
        $this->whatsApp                 = $whatsApp;
        $this->payment                  = $payment;

        // $this->middleware('auth');
    }

	/**
	* Method: orderPlacement
	* Description: This method is used to show the order placement page.
	* Author: Surajit
	*/
	public function orderPlacement($orderNo) {
		$data = [];
		$data['orderNo'] = $orderNo;
		$data['language_id'] = getLanguage()->id;
        $data['setting'] = Setting::first();
        $data['order'] = $order = $this->order->where('order_no', $orderNo)
        ->with([
            'shippingAddress.getCountry',
            'billingAddress.getCountry',
            'getCountry',
            'getBillingCountry'
        ])
        ->where('status', 'I')
        ->first();
       
        if($data['order']) {
            // to get the products informationa from order_details
            $data['orderDetails'] = $this->orderDetails->where(['order_master_id' => $order->id])
            ->with([
                'sellerDetails',
                'defaultImage',
                'productByLanguage',
                'productVariantDetails',
                'productDetails:id,slug'
            ])
            ->get();
            return view('modules.order.order_placement')->with($data);            
        } else {
            return redirect()->route('home');
        }
    }
    
	/**
	* Method: placeOrder
	* Description: This method is used to show the order placement page.
	* Author: Surajit
	*/
    public function placeOrder(Request $request, $orderNo) {
        $orderChk = $this->order->with([
            'orderMasterDetails.productByLanguage',
            'orderMasterDetails.productDetails',
            'orderMasterDetails.productVariantDetails',
            'customerDetails',
            'getBillingCountryCode',
            'orderMerchants.orderSeller'
        ])
        ->where(['order_no' => $orderNo])
        // ->where('status', 'I')
        ->first();
        $sendEmail = 0;
        $sendWhatsApp = 0;
        foreach ($orderChk->orderMerchants as $key => $value) {
            if($value->orderSeller->notify_customer_by_email_internal == 'Y') {
                $sendEmail += 1; 
            }
            if($value->orderSeller->notify_customer_by_whatsapp_internal == 'Y') {
                $sendWhatsApp += 1;
            }
        }
        $totalUnavailableItem = 0;
        foreach ($orderChk->orderMasterDetails as $key => $value) {
            # for variant products
            if($value->product_variant_id != 0) {
                if($value->productVariantDetails->stock_quantity < $value->quantity) {
                    $totalUnavailableItem += 1;
                }
            } else {
                if($value->productDetails->stock < $value->quantity) {
                    $totalUnavailableItem += 1;
                }
            }
        }
        if($totalUnavailableItem>0) {
            return redirect()->route('cart');
        }

        #set value if no discount then set the value to show common feild to show in driver app
        // $this->setOrderValue(@$orderChk);
        if(!@$orderChk) {
            abort('404');
        }

        $this->increaseTimesOfSold($orderChk->id);

        //set total commistion in order_seller table
        $this->setCommision($orderChk);

        # insert to payment table.
        $this->insertPayment($orderChk);

        # calculate coupon code
        if(@$request->coupon_code) {
            $this->calculateCoupon($request, $orderChk);
        }

        if(@Auth::user()) {
            $this->updateOrderLoyaltyPoint($request, $orderChk);
        }
        # all functionality if payment method is COD
        if($orderChk->payment_method == 'C') {
            try {
                # update order status if payment of is COD
                $this->updateOrderStatus($orderChk->id);

                # deduct loyalty point if customer registered
                if(@Auth::user()) {
                    $this->deductLoyaltyPoint($orderChk->id);
                }
                
                #drcrease stock(quantity) when order placed
                $this->decreaseStock(@$orderChk);
                
                # send whatsapp message to merchant
                $this->whatsApp->sendMessageMerchants($orderChk->id);

                # send mail to customer and seller
                $this->sendCustomerAndSellerMail($orderChk->id);
                
                # send push notificatio to all drivers
                $this->sendNotification($orderChk->id);
            } catch(\Exception $th){
                return redirect()->back()->with(['message' => $th->getMessage()]);
            }
            # insert to order timing table
            $this->insertOrderStatusTiming($orderChk->id);

            # clear cart
            $this->clearCart();
            
            return redirect()->route('order.success', ['trackId' => encrypt($orderChk->id)]);
        } else if($orderChk->payment_method == 'O'){
            // create online payment invoice
            return redirect()->route('payment.init', ['track' => encrypt($orderChk->id)]);
        }
    }

    /**
    * Method: insertOrderStatusTiming
    * Description: This method is used to insert time in to order status timing table
    * Author: Sanjoy
    */
    private function insertOrderStatusTiming($orderId) {
        $order = OrderMaster::find($orderId);
        OrderStatusTiming::create([
            'order_id'      => $order->id,
            'status'        => 'N',
            'start_time'    => date('Y-m-d H:i:s'),
            'duration_in_minutes' => 0
        ]);
        if($order->status == 'DA') {
            OrderStatusTiming::create([
                'order_id'      => $order->id,
                'status'        => 'DA',
                'start_time'    => date('Y-m-d H:i:s'),
                'duration_in_minutes' => 0
            ]);
        }
    }

    /**
    * 
    */
    public function calculateCoupon($requset, $order) {
        # check coupon code exist or not
        $checkCoupon = Coupon::with(['getMerchants'])->where(['code' => $requset->coupon_code])->first();
        if(!@$checkCoupon) {
            return 'Invalid coupon.';
        } else if(date('Y-m-d', strtotime($checkCoupon->start_date)) > date('Y-m-d')){
            return 'Invalid coupon.';
        } else if(date('Y-m-d', strtotime($checkCoupon->end_date)) < date('Y-m-d')){
            return 'Coupon has been expired.';
        } else if($checkCoupon->no_of_times <= $checkCoupon->used_times){
            return 'This is a used coupon.';
        }

        # check the coupon is associated with merchant or not
        $orderMerchants = $order->orderMerchants->pluck('seller_id')->toArray();

        if($checkCoupon->applied_for == 'M') {
            $couponMerchants = $checkCoupon->getMerchants->pluck('merchant_id')->toArray();
            $associatedMerchants = array_intersect($orderMerchants, $couponMerchants);
            if(!@$associatedMerchants) {
                return 'No seller associated with this coupon.';
            }
        } else {
            $associatedMerchants = $this->orderSeller->where('order_master_id', $order->id)->pluck('seller_id')->toArray();
        }

        $couponDiscount = 0;
        $shippingPrice = @$order->shipping_price;
        if($checkCoupon->applied_on == 'SUB') {
            $response['result']['discount_for'] = 'SUB';
            if($checkCoupon->discount_type == 'F') {
                if(@$order->subtotal) {
                    $couponDiscount = $checkCoupon->discount;

                    if($checkCoupon->coupon_bearer == 'M') {
                        $orderSel = $this->orderSeller->where('order_master_id', $order->id)->whereIn('seller_id', $associatedMerchants)->get();
                        $couponNotAssoSel = $this->orderSeller->where('order_master_id', $order->id)->whereNotIn('seller_id', $associatedMerchants)->get();
                        $subtotal = ($order->subtotal - $order->total_discount) - $couponNotAssoSel->sum('order_total');
                        foreach ($orderSel as $sp) {
                            $sellerCommision = $this->merchant->whereId($sp->seller_id)->first();
                            $total = $this->orderDetails->where([
                                'order_master_id' => $order->id,
                                'seller_id'       => $sp->seller_id
                            ])->sum('total');

                            # finding seller percentage from this order
                            $sellerPercentage = (($total * 100) / $subtotal );
                            $sellerCouponPercentAmount = ($couponDiscount * $sellerPercentage) / 100;
                            $totalCommision = ($sellerCommision->commission*($total - $sellerCouponPercentAmount))/100;
                            if($totalCommision < $sellerCommision->minimum_commission){
                                if($sellerCommision->minimum_commission > $order->order_total) {
                                    $update['total_commission'] = $order->order_total - $sellerCouponPercentAmount;
                                } else {
                                    $update['total_commission'] = $sellerCommision->minimum_commission;
                                }
                            } else {
                                $update['total_commission'] = $totalCommision;
                            }
                            $update['coupon_distributed_amount'] = $sellerCouponPercentAmount;
                            $update['order_total']               = $sp->order_total - $sellerCouponPercentAmount;
                            $update['coupon_bearer']             = 'M';
                            $this->orderSeller->whereId($sp->id)->update($update);
                        }
                    }
                }
            } else if($checkCoupon->discount_type == 'P') {
                if($checkCoupon->applied_for == 'M') {
                    $merchantOrderTotal = OrderSeller::where(['order_master_id' => $order->id])->whereIn('seller_id', $associatedMerchants)->sum('order_total');
                    $couponDiscount = (($merchantOrderTotal * $checkCoupon->discount) /100);
                } else {
                    $couponDiscount = ((($order->subtotal - $order->total_discount) * $checkCoupon->discount) /100);
                }
                # calculate seller commission for the coupon
                if($checkCoupon->coupon_bearer == 'M') {
                    $orderSel = $this->orderSeller->where('order_master_id', $order->id)->whereIn('seller_id', $associatedMerchants)->get();
                    $couponNotAssoSel = $this->orderSeller->where('order_master_id', $order->id)->whereNotIn('seller_id', $associatedMerchants)->get();
                    $subtotal = ($order->subtotal - $order->total_discount) - $couponNotAssoSel->sum('order_total');
                    foreach ($orderSel as $sp) {
                        $sellerCommision = $this->merchant->whereId($sp->seller_id)->first();
                        $total = $this->orderDetails->where([
                            'order_master_id' => $order->id,
                            'seller_id'       => $sp->seller_id
                        ])->sum('total');
                        # finding seller percentage from this order
                        $sellerPercentage = (($total * 100) / $subtotal );
                        $sellerCouponPercentAmount = ($couponDiscount * $sellerPercentage) / 100;

                        $totalCommision = ($sellerCommision->commission * ($total - ($total * $checkCoupon->discount / 100)))/100;

                        if($totalCommision < $sellerCommision->minimum_commission){
                            if($sellerCommision->minimum_commission > $order->order_total) {
                                $update['total_commission'] = ($order->order_total * $checkCoupon->discount) / 100; 
                            } else {
                                $update['total_commission'] = ($sellerCommision->minimum_commission * $checkCoupon->discount) / 100;
                            }
                        } else {
                            $update['total_commission'] = $totalCommision;
                        }
                        $update['coupon_distributed_amount'] = $sellerCouponPercentAmount;
                        $update['order_total']               = $sp->order_total - $sellerCouponPercentAmount;
                        $update['coupon_bearer']             = 'M';
                        $this->orderSeller->whereId($sp->id)->update($update);
                    }
                }
            }
        } else if($checkCoupon->applied_on == 'SHI') { // shipping
            $response['result']['discount_for'] = 'SHI';
            if($checkCoupon->discount_type == 'F') {
                if(@$order->shipping_price > 0) {
                    $couponDiscount = $checkCoupon->discount;
                    $shippingPrice = @$order->shipping_price - $checkCoupon->discount;
                }
            } else if($checkCoupon->discount_type == 'P') {
                $couponDiscount = (($order->shipping_price * $checkCoupon->discount) /100);
                $shippingPrice = @$order->shipping_price - $couponDiscount;
            }
        }

        # update order master 
        $params = [
            'coupon_id'         => $checkCoupon->id,
            'coupon_code'       => $requset->coupon_code,
            'coupon_discount'   => $couponDiscount,
            'coupon_applied_on' => $checkCoupon->applied_on,
            'coupon_bearer'     => @$checkCoupon->coupon_bearer,
            'shipping_price'    => $shippingPrice,
            'order_total'       => $order->order_total - $couponDiscount
        ];
        OrderMaster::where(['id' => $order->id])->update($params);

        # update coupon table 
        Coupon::where(['id' => $checkCoupon->id])->increment('used_times', 1);
    }

    /**
    * Method: increaseTimesOfSold
    * Description: This method is used to increase product times of sold.
    * Author: Sanjoy
    */
    public function increaseTimesOfSold($orderId) {
        $orderDtl = OrderDetail::where(['order_master_id' => $orderId])->get();
        foreach ($orderDtl as $key => $value) {
            Product::where(['id' => $value->product_id])->increment('times_of_sold', $value->quantity);
        }
    }

    /*
    * Method: sendNotificationDashboardUpdate 
    * Description : This method is used to update dashboard automatically specialy merchant dashboard client requirement using web notification (ajax call)
    * Author: Jayatri
    */
    private function sendNotificationDashboardUpdate($orderId, $merchantId) {
        // For Notification sending
        $order_dtt = OrderMaster::where('id',$orderId)->first();
        $new_registrationIds = MerchantRegId::where(['user_id' => $merchantId])->pluck('reg_id')->toArray();
        $idCount = MerchantRegId::where(['user_id' => $merchantId])->pluck('reg_id')->Count();
        if($idCount != 0){
            if(@$order_dtt) {
                $username = "Customer placed order!";
                $click_action = route('merchant.dashboard');
                $title = $username." Recently customer placed an order!";
                $message = "New order Placed !";
                $fields = array (
                    'registration_ids' => $new_registrationIds,
                    'data' => array (
                        "message"       => $message,
                        "title"         => $title,
                        "image"         => url('firebase-logo.png'),
                        "click_action"  => $click_action,
                    ),
                    'notification'      => array (
                        "body"          => $message,
                        "title"         => $title,
                        "click_action"  => $click_action,
                        "icon"          => "url('firebase-logo.png')",
                    )
                );
                $fields = json_encode ( $fields );
                $API_ACCESS_KEY = 'AAAAcQGyZzs:APA91bEst24UvPwO2UpmnRh6OUH_Y_On82ZRIm1ekOSKcekCuOc1m2yYR2w6CGDt09nBY8dmL5h_8C5Cs_KMgIPd1m0mZ8oMUDWz75KaDJmJeNOkTFklIS-hYgWYA3qHnbO-zb76jNK-';
                $headers = array(
                    'Authorization: key=' . $API_ACCESS_KEY,
                    'Content-Type: application/json',
                );
                $ch = curl_init ();
                curl_setopt ( $ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt ( $ch, CURLOPT_POST, true );
                curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
                curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
                $result = curl_exec ( $ch );
                curl_close ( $ch );
                return $result.' New Order placed! ';
            }
        }
    }

    /**
    * Method: createInvoice
    * Description: This method is used to create invoice on payment gateway.
    * Author: Sanjoy
    */
    private function createInvoice($order = []) {
        $setting = Setting::first();
        $loyaltyPoint = $order->order_total*$setting->one_kwd_to_point;
        $totalOrder = $loyaltyPoint*$setting->one_point_to_kwd;

        $params = [
            'PaymentMethodId'   => 1,
            'CustomerName'      => $order->customerDetails->fname.' '.$order->customerDetails->lname,
            'DisplayCurrencyIso'=> 'KWD',
            'MobileCountryCode' => $order->getBillingCountryCode->country_code,
            'CustomerMobile'    => $order->customerDetails->billing_phone,
            'CustomerEmail'     => $order->billing_email,
            'InvoiceValue'      => $order->order_total,
            'CallBackUrl'       => env('APP_ENV') == 'local'? 'http://www.example.com' : route('payment.notify', $order->id),
            'ErrorUrl'          => env('APP_ENV') == 'local'? 'http://www.example.com' : route('payment.error', $order->id),
            'Language'          => 'en',
            'CustomerReference' => $order->customerDetails->id,
            'CustomerCivilId'   => 12345678,
            'UserDefinedField'  => 'custom_field',
            'ExpireDate'        => '',
            'CustomerAddress'   => [
                'Block'             => @$order->billing_block,
                'Street'            => @$order->billing_street,
                'HouseBuildingNo'   => @$order->billing_building_no,
                'Address'           => @$order->billing_more_address,
                'AddressInstructions' => ''
            ]
        ];
        foreach ($order->orderMasterDetails as $key => $value) {
            $params['InvoiceItems'][] = [
                'ItemName'  => $value->productByLanguage->title,
                'Quantity'  => $value->quantity,
                'UnitPrice' => $value->total /$value->quantity
            ];
        }
        $params['InvoiceItems'][] = [
            'ItemName'  => 'Shipping price',
            'Quantity'  => 1,
            'UnitPrice' => $order->shipping_price
        ];
        if($order->loyalty_amount > 0) {
            $params['InvoiceItems'][] = [
                'ItemName'  => 'Deduct reward',
                'Quantity'  => 1,
                'UnitPrice' => -$order->loyalty_amount
            ];
        }

        $execPayment = $this->payment->executePayment($params);
        // upddate payment table invoice data.
        # update to payment table
        $params = [
            'invoice_data'  => json_encode($execPayment),
            'invoice_id'    => $execPayment->Data->InvoiceId
        ];
        $this->payment->updatePayment($params, $order->id);
        return $execPayment;
    }

    /*
    * Method: decreaseStock
    * Description: This method is used to descrease Stock
    * Author: Jayatri
    */
    private function decreaseStock($orderChk) {   
        $details = $this->orderDetails->where('order_master_id',@$orderChk->id)->get();
        
        foreach($details as $dtl){
            if($dtl->product_variant_id != 0){
                $variant = $this->productVariant->where('id',$dtl->product_variant_id)->first();
                if($variant->stock_quantity >= $dtl->quantity){
                    $updatestock['stock_quantity'] = $variant->stock_quantity - $dtl->quantity;
                    $this->productVariant->where('id',$dtl->product_variant_id)->update($updatestock);
                }
            }else{
                if($dtl->product_id != 0){
                    $product = $this->product->where('id',@$dtl->product_id)->first();    
                    if($product->stock >= $dtl->quantity){
                        $stock_update['stock'] = $product->stock - $dtl->quantity;
                        $this->product->where('id',@$dtl->product_id)->update($stock_update);
                    }
                }
            }   
        }
    }
    
    /*
    * Method: sendNotification
    * Description: This method is used to send Notification to driver
    * Author: Jayatri
    */
    private function sendNotification($order_id){
        $androidDrivers = Driver::where(['status' => 'A', 'device_type' => 'A'])->pluck('firebase_reg_no');

        $iosDrivers = Driver::where(['status' => 'A', 'device_type' => 'I'])->pluck('firebase_reg_no');
        $order_dtt = OrderMaster::where('id',$order_id)->first();
        # send notification to android drivers
        if(count($androidDrivers)) {
            $msg                      = array();
            $msg['title']             = 'Order No:'." ".$order_dtt->order_no;
            $msg["body"]              = $order_dtt->shipping_fname." placed order recently!";
            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            $msg['link_type']         = "Y";
            $msg['is_linked']         = "Y";
            $msg['image']             = '';
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]            = "10";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'registration_ids' => $androidDrivers,
                'data'             => $msg
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            
            // Closing Curl
            curl_close($ch);
        }
        
        # send notification to ios drivers
        if(count($iosDrivers)) {
            $msg                      = array();
            $msg['title']             = "New Order Placed";
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]            = "10";
            $msg["mutable-content"]   = true;
            $msg["body"]              = 'Recently customer placed an order! ';

            $data['message']          = "New Order Placed";
            $data['type']             = "1";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'category'                  => "CustomSamplePush",
                'content_available'         => true,
                'mutable_content'           => true,
                'registration_ids'          => $iosDrivers,
                'notification'              => $msg,
                'data'                      => $data
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            // Closing Curl
            curl_close($ch);
        }
    }
    
    /**
    * Method: updateOrderStatus
    * Description: This method is used to change status of order_master and order_details
    * Author: Sanjoy
    */
    private function updateOrderStatus($orderId) {
        $ordDetl = $this->orderDetails->where(['order_master_id' => $orderId])->get()->pluck('seller_id');
        $sellerDriver = $this->merchant->select('id','driver_id')->whereIn('id', $ordDetl)->get();
        foreach ($sellerDriver as $sd) {
            if($sd->driver_id != '0') {
                $assignDriver['driver_id'] = $sd->driver_id;    
                $assignDriver['status'] = 'DA';    
                $this->orderDetails->where(['seller_id' => $sd->id,'order_master_id' => $orderId])->update($assignDriver);
                $this->orderSeller->where(['seller_id' => $sd->id,'order_master_id' => $orderId])->update($assignDriver);
            }
        }
        $drvAssndChck = $this->orderDetails->where(['order_master_id' => $orderId, 'driver_id' => '0'])->count();
        if($drvAssndChck > 0) {
            $order['status'] = 'N';   
        } else {
            $order['status'] = 'DA';   
        }
        $order['last_status_date'] = date('Y-m-d H:i:s');
        $this->order->where(['id' => $orderId])->update($order);
    }

    
    private function sendCustomerAndSellerMail($orderId) {
        $data = [];
        $data['order'] = $order = OrderMaster::where('id', $orderId)
        ->with([
            'shippingAddress.getCountry',
            'billingAddress.getCountry',
            'getCountry'
        ])
        ->first();

        $data['orderDetails'] = OrderDetail::where(['order_master_id' => $orderId])
        ->with([
            'sellerDetails',
            'defaultImage',
            'productByLanguage',
            'productVariantDetails'
        ])
        ->get();
        
        $contactList = [];
        $i=0;
        $user = User::whereId($order->user_id)->first();
        $data['fname']          = $user->fname;
        $data['lname']          = $user->lname;  
        $data['email']          = $user->email;
        $data['user_type']      = $user->user_type;
        $data['shipping_price'] = @$order->shipping_price;
        $data['orderNo']        = @$order->order_no;
        $data['language_id']    = getLanguage()->id;
        $data['sub_total']      = @$order->subtotal;
        $data['order_total']    = @$order->order_total;
        $data['total_discount'] = @$order->total_discount;

        $contactList[$i]        = @$order->shipping_email;
        if(@$order->status == 'N'){
            $data['subject'] = "Your order has been placed successfully! Order no: ".@$order->order_no;
        } else if(@$order->status == 'DA') {
            $data['subject'] = "Your order is accepted! Order No:".@$order->order_no." ! Order Details!";
        } else if(@$order->status == 'RP') {
            $data['subject'] = "Your order is ready for pick up! Order No:".@$order->order_no." ! Order Details!";
        } else if(@$order->status == 'OP') {
            $data['subject'] = "Your order is picked up! Order No:".@$order->order_no." ! Order Details!";
        } else if(@$order->status == 'OD') {
            $data['subject'] = "Your order is delivered successfully! Order No:".@$order->order_no." ! Order Details!";
        }

        #send mail admin to notify for new order only as client requirement (internal order)
        $i=0;
        if(@$order->status =='N' ||@$order->status =='DA'){
            $setting = Setting::first();
            if(@$setting->email_1) {
                $contactList[$i] = $setting->email_1;
                $i++;
            }
            if(@$setting->email_2) {
                $contactList[$i] = $setting->email_2;
                $i++;
            }
            if(@$setting->email_3) {
                $contactList[$i] = $setting->email_3;
                $i++;
            }
        }
        $contactList = array_unique($contactList);
        $data['bcc'] = $contactList;
        //dd($data);
        #send to customer mail(order status can be new/driver assigned)
        Mail::queue(new OrderMail($data));
        
        # send mail order details to seller mail
        $contactList = [];
        $data['bcc'] = $contactList;
        $dtls_mail = OrderDetail::with([
            'sellerDetails'
        ])
        ->where(['order_master_id' => $order->id])
        ->groupBy('seller_id')
        ->get();
        
        foreach(@$dtls_mail as $contact) {
            $contactListEmail1 = [];
            $contactListEmail2 = [];
            $contactListEmail3 = [];
            $contactList = [];
            $data['bcc'] = [];
            $contactListEmail3 = [];
            $i = 0;
            //start
            $data['user_type'] = 'C';
            $data['hide_customer_info'] = $contact->sellerDetails->hide_customer_info;

            $data['orderDetails'] = OrderDetail::where([
                'order_master_id' => $order->id,
                'seller_id'       => @$contact->seller_id
            ])
            ->with([
                'sellerDetails',
                'defaultImage',
                'productByLanguage',
                'productVariantDetails'
            ])
            ->get();
            
            $data['sub_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
            $data['order_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
            $data['total_discount'] = OrderSeller::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total_discount');
            $data['shipping_price'] = "NA";
            if(@$order->status == 'N') {
                $data['subject'] = "New Order Placed!Order No : ".@$order->order_no." ! Order Details!";
            } else if(@$order->status =='DA') {
                $data['subject'] = "Order is Accepted! Order No : ".@$order->order_no." ! Order Details!";    
            } else if(@$order->status =='RP') {
                $data['subject'] = "Order is ready for pickup! Order No : ".@$order->order_no." ! Order Details!";    
            } else if(@$order->status =='OP') {
                $data['subject'] = "Order is picked up! Order No : ".@$order->order_no." ! Order Details!";    
            } else if(@$order->status =='OD') {
                $data['subject'] = "Order is Delivered! Order No : ".@$order->order_no." ! Order Details!";    
            }
            
            // Fill the Bcc email address
            $contactList[$i] = $contact->sellerDetails->email;
            $i++;
            #send mail to merchant if status driver assigned(client requirement)
            if(@$order->status =='DA' || $order->status =='N'){
                if($contact->sellerDetails->email1) {
                    $contactListEmail1[$i] = $contact->sellerDetails->email1;
                    $i++;
                }
                if($contact->sellerDetails->email2) {
                    $contactListEmail2[$i] = $contact->sellerDetails->email2;
                    $i++;
                }
                if($contact->sellerDetails->email3) {
                    $contactListEmail3[$i] = $contact->sellerDetails->email3;
                    $i++;
                }    
            }
            $contactList = array_unique($contactList);
            if(@$contactListEmail1 != '') {
                $contactListEmail1 = array_unique($contactListEmail1);
                $contactList = array_merge($contactList, $contactListEmail1);
            }
            if(@$contactListEmail2 != '') {
                $contactListEmail2 = array_unique($contactListEmail2);
                $contactList = array_merge($contactList, $contactListEmail2);
            }
            if(@$contactListEmail3 != '') {
                $contactListEmail3 = array_unique($contactListEmail3);
                $contactList = array_merge($contactList, $contactListEmail3);
            }
            $data['bcc'] = $contactList;
            //dd($data['bcc']);
            #send web notification to merchant only for update merchant dashboard
            Mail::queue(new SendOrderDetailsMerchantMail($data));
            $this->sendNotificationDashboardUpdate($contact->order_master_id, $contact->seller_id);
        }
    }

    /**
    * method: insertPayment
    * Description: This method is used to insert to payment table.
    * Author: Sanjoy
    */
    private function insertPayment($reqData = []) {
        Payment::create([
            'user_id'       => $reqData->user_id,
            'order_id'      => $reqData->id,
            'amount'        => $reqData->order_total,
            'payment_mode'  => $reqData->payment_method,
            'status'        => 'I' 
        ]);
    }

    /**
    * Method: updateOrderLoyaltyPoint
    * Description: This method is used to update loyalty point to order master table.
    * Author: Sanjoy
    */
    private function updateOrderLoyaltyPoint($request, $order) {
        $setting = Setting::first();
        if(@$request->reward_point) {    
            if(@$request->reward_point == 'Y') {  
                $totalOrder = $request->loyalty_point*$setting->one_point_to_kwd;
                $this->order->whereId($order->id)->increment('loyalty_point_used', $request->loyalty_point);
                $this->order->whereId($order->id)->decrement('order_total', number_format($totalOrder, 3));
                $this->order->whereId($order->id)->update(['loyalty_amount' => number_format($totalOrder, 3)]);
            }
        }
    }

    /**
    * Method: calculateLoyaltyPoint
    * Description: This method is used to deduct loyalty point from customer account.
    * Author: Sanjoy
    */
    private function deductLoyaltyPoint($orderId) {
        $setting = Setting::first();
        $order = OrderMaster::where(['id' => $orderId])->first();
        if(@$order->loyalty_point_used > 0) {  
            $totalOrder = $order->loyalty_point_used*$setting->one_point_to_kwd;
            
            # insert to user reward table
            $addUserReward = new UserReward;
            $addUserReward['user_id']        = @Auth::user()->id;
            $addUserReward['order_id']       = $orderId;
            $addUserReward['earning_points'] = $order->loyalty_point_used;
            $addUserReward['debit']          = '1';
            $addUserReward->save();

            # update customer total reward
            $this->user->whereId(@Auth::user()->id)->decrement('loyalty_balance', $order->loyalty_point_used);
            $this->user->whereId(@Auth::user()->id)->increment('loyalty_used', $order->loyalty_point_used);
        }
    }

    /**
    * Method: creaditLoyaltyPoint
    * Description: This method is used to credit loyalty point
    * Author: Sanjoy
    */
    public function creaditLoyaltyPoint($order) {
        $setting = Setting::first();
        $loyaltyPoint = $order->order_total*$setting->one_kwd_to_point;
        $totalOrder = $loyaltyPoint*$setting->one_point_to_kwd;
        $this->order->whereId($order->id)->increment('loyalty_point_received', $loyaltyPoint);
        $this->user->whereId($order->user_id)->increment('loyalty_balance', $loyaltyPoint);
        $this->user->whereId($order->user_id)->increment('loyalty_total', $loyaltyPoint);

        $addUserReward = new UserReward;
        $addUserReward['user_id']        = $order->user_id;
        $addUserReward['order_id']       = $order->id;
        $addUserReward['earning_points'] = $loyaltyPoint;
        $addUserReward['credit']         = '1';
        $addUserReward->save();
    }

    /**
    * Method: setCommision
    * Description: This method is used to set commission
    * Author: Sanjoy
    */
    private function setCommision($orderChk) {   
        $orderSel = $this->orderSeller->where(['order_master_id' => $orderChk->id])->get(); 
        foreach ($orderSel as $sp) {
            $sellerCommision = $this->merchant->whereId($sp->seller_id)->first();
            $total = $this->orderDetails->where([
                'order_master_id' => $orderChk->id,
                'seller_id'       => $sp->seller_id
            ])->sum('total');
            $totalCommision = ($sellerCommision->commission * $total)/100;
            if($totalCommision < $sellerCommision->minimum_commission){
                if($sellerCommision->minimum_commission > $total) {
                    $update['total_commission'] = $total;
                } else {
                    $update['total_commission'] = $sellerCommision->minimum_commission;
                }
            } else {
                $update['total_commission'] = $totalCommision;
            }
            $this->orderSeller->whereId($sp->id)->update($update);
        }
    }

    /**
    * Method: orderSuccess
    * Description: This method is used to show the order success page.
    * Author: Surajit
    */
    public function orderSuccess(Request $request) {
        $data = [];
        try {
            if(@$request->trackId) {
                $orderId = decrypt($request->trackId);
                $data['order'] = $this->order->with(['customerDetails'])->where(['id' => $orderId])->first();
                if(@$data['order']){
                    $orddd = encrypt(@$data['order']->order_no);  
                    return view('modules.extras.order_success')->with($data);  
                }else{
                    return redirect()->back();
                }
            }else{
                return redirect()->back();
            }
        } catch(\Exception $th){
            return redirect()->back()->with(['message'=>$th->getMessage()]);
        }
    }

    /**
    *
    */
    public function applyCoupon(Request $request) {
        $reqData = $request->params;
        $response = [
            'jsonrpc' => '2.0'
        ];

        # check coupon code exist or not
        $checkCoupon = Coupon::with(['getMerchants'])->where(['code' => $reqData['coupon_code']])->first();
        if(!@$checkCoupon) {
            $response['error']['message'] = 'Invalid coupon.';
            return response()->json($response);
        } else if(date('Y-m-d', strtotime($checkCoupon->start_date)) > date('Y-m-d')){
            $response['error']['message'] = 'Invalid coupon.';
            return response()->json($response);
        } else if(date('Y-m-d', strtotime($checkCoupon->end_date)) < date('Y-m-d')){
            $response['error']['message'] = 'Coupon has been expired.';
            return response()->json($response);
        } else if($checkCoupon->no_of_times <= $checkCoupon->used_times){
            $response['error']['message'] = 'This is a used coupon.';
            return response()->json($response);
        }
        # check the coupon is associated with merchant or not
        $order = OrderMaster::with(['orderMerchants'])->where(['id' => $reqData['order_id']])->first();
        $orderMerchants = $order->orderMerchants->pluck('seller_id')->toArray();

        if($checkCoupon->applied_for == 'M') {
            $couponMerchants = $checkCoupon->getMerchants->pluck('merchant_id')->toArray();
            $associatedMerchants = array_intersect($orderMerchants, $couponMerchants);
            if(!@$associatedMerchants) {
                $response['error']['message'] = 'No seller associated with this coupon.';
                return response()->json($response);
            }
        }

        $couponDiscount = 0;
        if($checkCoupon->applied_on == 'SUB') { // subtotal
            $response['result']['discount_for'] = 'SUB';
            if($checkCoupon->discount_type == 'F') {
                if(@$order->subtotal) {
                    $couponDiscount = $checkCoupon->discount;
                }
            } else if($checkCoupon->discount_type == 'P') {
                if($checkCoupon->applied_for == 'M') {
                    $merchantOrderTotal = OrderSeller::where(['order_master_id' => $reqData['order_id']])->whereIn('seller_id', $associatedMerchants)->sum('order_total');
                    $couponDiscount = (($merchantOrderTotal * $checkCoupon->discount) /100);
                } else {
                    $couponDiscount = ((($order->subtotal - $order->total_discount) * $checkCoupon->discount) /100);
                }
            }
        } else if($checkCoupon->applied_on == 'SHI') { // shipping
            $response['result']['discount_for'] = 'SHI';
            if($checkCoupon->discount_type == 'F') {
                if(@$order->shipping_price > 0) {
                    $couponDiscount = $checkCoupon->discount;
                }
            } else if($checkCoupon->discount_type == 'P') {
                $couponDiscount = (($order->shipping_price * $checkCoupon->discount) /100);;
            }
        }
        $response['result']['coupon_discount'] = $couponDiscount;
        return response()->json($response);
    }

    /**
    * Method: clearCart
    * Description: This method is used to clear cart
    * Author: Sanjoy
    */
    private function clearCart() {
        if(@Auth::user()) {
            $cart = $this->cart->where('user_id', Auth::user()->id)->first();
            if($cart) {
                $this->cartDetails->where('cart_master_id', $cart->id)->delete(); 
                $this->cart->where('id', $cart->id)->delete();
            }
        } else {
            $cart = $this->cart->where('session_id', session()->getId())->first();
            if($cart) {
                $this->cartDetails->where('cart_master_id', $cart->id)->delete(); 
                $this->cart->where('id', $cart->id)->delete();
            }
        }
    }
}
