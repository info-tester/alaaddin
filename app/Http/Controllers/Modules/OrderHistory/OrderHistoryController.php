<?php

namespace App\Http\Controllers\Modules\OrderHistory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductVariantDetailRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CountryRepository;
use App\Repositories\UserRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\DriverRepository;
use App\Models\Payment;
use App\Mail\OrderMail;
use Mail;
use Auth;
use Session;

class OrderHistoryController extends Controller
{
    protected $user, $cart, $cartDetails, $order, $orderDetails, $product, $productVariant, $productVariantDetail,$country;

	/**
	* __construct
	* This method is used to initilize the instance
	* Author: Sanjoy
	*/
	public function __construct(UserRepository $user,
		CartMasterRepository $cart,
		CartDetailRepository $cartDetails,
		OrderMasterRepository $order,
		OrderDetailRepository $orderDetails,
		OrderSellerRepository $orderSeller,
		ProductVariantRepository $productVariant,
		ProductRepository $product,
		ProductVariantDetailRepository $productVariantDetail,
		CountryRepository $country,
		MerchantRepository $merchants,
		ProductVariantRepository $product_variant,
        DriverRepository $drivers
	)
	{
		$this->user         			= $user;
		$this->cart         			= $cart;
		$this->cartDetails  			= $cartDetails;
		$this->order         			= $order;
		$this->orderDetails  			= $orderDetails;
		$this->orderSeller  			= $orderSeller;
		$this->productVariant  			= $productVariant;
		$this->productVariantDetail  	= $productVariantDetail;
		$this->product  				= $product;
		$this->country            		= $country;
		$this->merchants            	= $merchants;
		$this->product_variant          = $product_variant;
        $this->drivers                  =   $drivers;
        $this->middleware('auth', ['except' => ['fetchCity','checkCity']]);
	}
	
    private function calculateMerchantRate($id){
    	$orderdetails = $this->orderDetails->where('order_master_id',@$id)->get();
        foreach(@$orderdetails as $key=>$dtls){
        	$obj = $this->orderDetails->where(['seller_id'=>@$dtls->seller_id]);
        	$count = $obj->where('rate','!=',0)->count();
        	$total_rate = $this->orderDetails->where(['seller_id'=>@$dtls->seller_id])->sum('rate');
        	$update['total_no_review'] = $count;
        	$update['total_review_rate'] = $total_rate;
        	$update['rate'] = $total_rate/$count;
        	// dd($update['rate']);
        	$this->merchants->where(['id'=>@$dtls->seller_id])->update($update);
        }
    }
    private function calculateDriverReview($id){
        $orderdetails = $this->orderDetails->where('order_master_id',@$id)->get();
        foreach(@$orderdetails as $key=>$dtls){
            $obj = $this->orderDetails->where(['driver_id'=>@$dtls->driver_id]);
            $count = $obj->where('rate','!=',0)->count();
            $total_rate = $this->orderDetails->where(['driver_id'=>@$dtls->driver_id])->sum('rate');
            $update['total_no_reviews'] = $count;
            $update['total_review'] = $total_rate;
            $update['avg_review'] = $total_rate/$count;
            // dd($update['rate']);
            $this->drivers->where(['id'=>@$dtls->driver_id])->update($update);
        }
    }
    private function calculateProductRate($id){
    	$orderdetails = $this->orderDetails->where('order_master_id',@$id)->get();
        foreach(@$orderdetails as $key=>$dtls){

        	// $this->merchants->where(['id'=>@$dtls->seller_id])->update($update);
        	if($dtls->product_variant_id != 0){

        		$variant = $this->product_variant->where('id',$dtls->product_variant_id)->first();
        		if(@$variant){
        			$obj = $this->orderDetails->where(['product_variant_id'=>$dtls->product_variant_id]);
        			$count = $obj->where('rate','!=',0)->count();
        			$total_rate = $this->orderDetails->where(['product_variant_id'=>$dtls->product_variant_id])->sum('rate');
        			$update['total_review'] = $total_rate;
        			$update['total_no_reviews'] = $count;
        			$update['avg_review'] = $total_rate/$count;
                	$this->product_variant->where('id',$dtls->product_variant_id)->update($update);
                    $attrProductTotalReview = $this->product_variant->where(['product_id'=>$variant->product_id])->sum('total_review');
                    $attrProductTotalNoReview = $this->product_variant->where(['product_id'=>$variant->product_id])->sum('total_no_reviews');
                    $attrTotalReview = $attrProductTotalReview;
                    $attrTotalNoReview = $attrProductTotalNoReview;
                    $attrAvgReview = $attrProductTotalReview/$attrProductTotalNoReview;
                    $this->product->where('id',@$variant->product_id)->update(['avg_review'=>@$attrAvgReview,'total_review'=>@$attrTotalReview,'total_no_reviews'=>@$attrTotalNoReview]);
        		}
        		
        	}else{
        		if($dtls->product_id != 0){
        			// dd("gjdnf");
                    $product = $this->product->where('id',@$dtls->product_id)->first();    
                    $obje = $this->orderDetails->where(['product_id'=>$dtls->product_id]);
                    $count = $obje->where('rate','!=',0)->count();
        			$total_rate = $this->orderDetails->where(['product_id'=>$dtls->product_id])->sum('rate');
        			$updater['total_no_reviews'] = $count;
        			$updater['total_review'] = $total_rate;
        			$updater['avg_review'] = $total_rate/$count;
                    // dd($stock_update['stock']);            
                    $this->product->where('id',@$dtls->product_id)->update($updater);             
                }
        	}
        	
        }	
    }
    public function postReviewCustomer(Request $request,$id){
        // dd($request->all());
        $orderdetails = $this->orderDetails->where('order_master_id',@$id)->get();
        $total_rate = 0;
        foreach(@$orderdetails as $key=>$dtls){
        	$update['rate'] = @$request->rate_product[$key];
        	$update['comment'] = @$request->product_comment[$key];
            $update['customer_name'] = @$request->customer_name[$key];
            $update['review_date'] = now();
        	$this->orderDetails->where(['id'=>@$dtls->id])->update($update);
        }
        $upm['is_review_done'] = 'Y';
        
        $this->order->where('id',@$id)->update($upm);
        // dd("fgndk");
        $this->calculateMerchantRate($id);
        $this->calculateProductRate($id);
        $this->calculateDriverReview($id);
        // $total_rate

        return redirect()->back();
    }
	/**
	* Method: manageOrders
	* Description: This method is used to manage orders
	* Author: Jayatri
	*/
	public function manageOrders(){
		$orderss = $this->order->with(['customerDetails','orderMasterDetails','countryDetails','orderMasterDetails','orderMasterDetails.productDetails.productByLanguage','orderMasterDetails.productVariantDetails','orderMasterDetails.sellerDetails','orderMasterDetails.productDetails.defaultImage','countryDetails.countryDetailsBylanguage','shippingAddress','billingAddress','getBillingCountry','productVariantDetails'])->where('user_id',@Auth::user()->id);
        $orderss = $orderss->where(function($q1) {
                    $q1 = $q1->whereNotIn('status',['','I','D']);
                        
                });
        $orders = $orderss->where('status','!=','I')->orderBy('id', 'DESC')->get();
		// dd($orders);
		return view('modules.dashboard.list_orders')->with(['orders'=>@$orders]);

	}
	/*
	* Method: orderDetails
	* Description: This method is used to order details
	* Author: Jayatri
	*/
	public function orderDetails($order_no){

		$order = $this->order->with(['customerDetails','orderMasterDetails','countryDetails','orderMasterDetails','orderMasterDetails.productDetails.productByLanguage','orderMasterDetails.productDetails.defaultImage','orderMasterDetails.productDetails.images','orderMasterDetails.productDetails.productVariants','orderMasterDetails.productVariantDetails','orderMasterDetails.sellerDetails','countryDetails','orderMasterDetails.driverDetails']);
        $order = $order->where(function($q1) use($order_no) {
          $q1 = $q1->where('order_no',$order_no);
        });
        $order = $order->where(function($q1)  {
          $q1 = $q1->whereNotIn('status',['','D','I']);
        });
        $order = $order->first();
        if(!@$order) {
          abort(404);
        }
		return view('modules.dashboard.order_details')->with(['order'=>@$order]);
	}

	public function orderCancel($order_no){
		$order = $this->order->with(['customerDetails','orderMasterDetails','countryDetails','orderMasterDetails','orderMasterDetails.productDetails.productByLanguage','orderMasterDetails.productDetails.productVariants','orderMasterDetails.productVariantDetails','orderMasterDetails.sellerDetails','countryDetails','getBillingCountry'])->where('order_no',$order_no)->first();

		if(@$order){
			$update['status'] = 'C';
            $this->increaseStock(@$order);
			$this->orderDetails->where('order_master_id',$order->id)->update($update);
			$this->order->where('id',$order->id)->update($update);
		}
		return redirect()->back();
	}
    /*
    * Method: increaseStock
    * Description: 
    * Author: Jayatri
    */
    private function increaseStock($orderChk){
        $details = $this->orderDetails->where('order_master_id',@$orderChk->id)->get();
        
        foreach($details as $dtl){
            if($dtl->product_variant_id != 0){
                $variant = $this->product_variant->where('id',$dtl->product_variant_id)->first();
                $updatestock['stock_quantity'] = $variant->stock_quantity + $dtl->quantity;
                $this->product_variant->where('id',$dtl->product_variant_id)->update($updatestock);
            }else{
                $product = $this->product->where('id',@$dtl->product_id)->first();    
                $stock_update['stock'] = $product->stock + $dtl->quantity;
                $this->product->where('id',@$dtl->product_id)->update($stock_update);
            }
        }
    }
    public function showPaymentHistory(){
        $data['payments'] = Payment::where(['user_id'=>@Auth::user()->id,'payment_mode'=>'O'])->orderBy('id','desc')->get();
        return view('modules.dashboard.payment_history',$data);
    }
	// public function postreview(Request $request){
			
	// }
}
