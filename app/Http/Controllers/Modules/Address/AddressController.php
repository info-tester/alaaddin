<?php

namespace App\Http\Controllers\Modules\Address;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\CountryRepository;
use App\Repositories\UserAddressBookRepository;
use App\Repositories\OrderMasterRepository;
use App\Models\City;
use App\Models\CitiDetails;
use Auth;
use Session;
use Config;

class AddressController extends Controller
{
	protected $user_addr,$user,$country,$order;
	public function __construct(UserAddressBookRepository $user_addr,
		UserRepository $user,
		OrderMasterRepository $order,
		CountryRepository $country,
		Request $request
	)
	{
		$this->user_addr            =   $user_addr;
		$this->user            		=   $user;
		$this->country            	=   $country;
		$this->order            	=   $order;
		$this->middleware('auth', ['except' => ['fetchCity','checkCity']]);
	}

	/**
	* Method: showAddressBook
	* Description: This method is used to show address book page
	* Author: Surajit
	*/
	public function showAddressBook() {
		$data['country'] 	 = $this->country->with('countryDetailsBylanguage')->where('status','!=','D')->get();
		$data['userDefAddr'] = $this->user_addr->where(['user_id' => @Auth::user()->id, 'is_default' => 'Y'])->with('getCountry')->first();
		$data['userAddr'] 	 = $this->user_addr->where(['user_id' => @Auth::user()->id, 'is_default' => 'N'])
		->with('getCountry')->get();
		return view('modules.dashboard.address_book',$data);
	}

	/**
	* Method: addAddressBook
	* Description: This method is used to show add address book page
	* Author: Surajit
	*/
	public function addAddressBook() {
		$data['city'] = City::get();
		$country = $this->country->with('countryDetailsBylanguage');
		$country = $country->where(function($q1) {
            $q1 = $q1->where('status', '!=', 'I');
        });
		$country = $country->where(function($q1) {
            $q1 = $q1->where('status','!=','D');
        });
		$data['country'] = $country->orderBy('id','desc')->get();
		return view('modules.dashboard.add_address_book', $data);
	}

	/**
	* Method: storeAddressBook
	* Description: This method is used to store user details into user_address_books table
	* Author: Surajit
	*/
	public function storeAddressBook(Request $request) {
		$validator = $request->validate([
			'shipping_fname'=> 'required',
			'shipping_lname'=> 'required',
			'email'        	=> 'required',
			'phone'        	=> 'required',
			'country'      	=> 'required',
			'street'     	=> 'required',
		]);
		if($validator) {
			if($request->city_id) {
				$city = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
				$city = $city->whereHas('cityDetailsByLanguage',function($query) use($request) {
					$query->where('name', 'LIKE', "%{$request->city_id}%");
				})->get();

				if(!$city) {
					session()->flash("error",__('success_user.-722'));
					return redirect()->back();		
				}
			}
			$userAddData = $this->user_addr->where('user_id', @Auth::user()->id)->count();
			$userAdd['user_id']     		=   @Auth::user()->id;
			$userAdd['shipping_fname']     	=   $request->shipping_fname;
			$userAdd['shipping_lname']     	=   $request->shipping_lname;
			$userAdd['email']     			=   $request->email;
			$userAdd['phone']     			=   $request->phone;
			$userAdd['country']   			=   $request->country;
			$userAdd['street']   			=   $request->street;
			$userAdd['block']     			=   $request->block;
			$userAdd['building']   			=   $request->building;
			$userAdd['more_address']   		=   $request->more_address;
			$userAdd['location']   			=   $request->location;
			$userAdd['lat']   				=   $request->lat;
			$userAdd['lng']   				=   $request->lng;
			if($request->city) {
				$userAdd['city']      		=   $request->city;
				$userAdd['city_id']      	=   '';
			} elseif($request->city_id) {
				$cityId = CitiDetails::where('name', $request->city_id)->first();
				$userAdd['city_id']      	=   $cityId->city_id;
				$userAdd['city']      		=   $request->city_id;				
			}
			
			if($request->postal_code) {
				$userAdd['postal_code']   	=   $request->postal_code;
			}
			if($request->is_default || $userAddData<1) {
				$userAdd['is_default'] = 'Y';
			} else {
				$userAdd['is_default'] = 'N';				
			}
			$userAddress = $this->user_addr->create($userAdd);
			if($request->is_default) {
				$this->user_addr->where('user_id', @Auth::user()->id)->where('id', '!=', $userAddress->id)->update(['is_default' => 'N']);
			}
			session()->flash("success",__('success_user.-702'));
		}
		return redirect()->back();
	}

	/**
	* Method: showAddressBook
	* Description: This method is used to show address book page
	* Author: Surajit
	*/
	public function showEditAddressBook($id) {
		$data['city'] 		= City::get();
		$data['country'] 	= $this->country->with('countryDetailsBylanguage')->where('status','!=','D')->where('status','!=','D')->get();
		$data['userAddr']	= $this->user_addr->whereId($id)->first();
		if($data['userAddr']) {
			if(@Auth::user()->id == $data['userAddr']->user_id) {
				return view('modules.dashboard.edit_address_book',$data);
			} else {
				session()->flash("error",__('success_user.-703'));
				return redirect()->back();
			}
		} else {
			session()->flash("error",__('success_user.-703'));
			return redirect()->back();			
		}
	}

	/**
	* Method: updateAddressBook
	* Description: This method is used to update user details into user_address_books table
	* Author: Surajit
	*/
	public function updateAddressBook(Request $request, $id) {
		$validator = $request->validate([
			'shipping_fname'=> 'required',
			'shipping_lname'=> 'required',
			'email'        	=> 'required',
			'phone'        	=> 'required',
			'country'      	=> 'required',
			'street'     	=> 'required',
		]);
		if($validator)
		{
			if($request->city_id) {
				$city = City::where('id', '!=', '0')->with('cityDetailsByLanguage');
				$city = $city->whereHas('cityDetailsByLanguage',function($query) use($request) {
					$query->where('name', 'LIKE', "%{$request->city_id}%");
				})->get();

				// $city = City::where('name', $request->city_id)->first();
				if(!$city) {
					session()->flash("error",__('success_user.-722'));
					return redirect()->back();		
				}
			}
			$userAdd['shipping_fname']     	=   $request->shipping_fname;
			$userAdd['shipping_lname']     	=   $request->shipping_lname;
			$userAdd['email']     			=   $request->email;
			$userAdd['phone']     			=   $request->phone;
			$userAdd['country']   			=   $request->country;
			$userAdd['street']   			=   $request->street;
			$userAdd['block']     			=   $request->block;
			$userAdd['building']   			=   $request->building;
			$userAdd['more_address']   		=   $request->more_address;
			$userAdd['location']   			=   $request->location;
			$userAdd['lat']   				=   $request->lat;
			$userAdd['lng']   				=   $request->lng;
			if($request->city) {
				$userAdd['city']      		=   $request->city;
				$userAdd['city_id']      	=   '';
			} elseif($request->city_id) {
				$cityId = CitiDetails::where('name', $request->city_id)->first();
				$userAdd['city_id']      	=   $cityId->city_id;
				$userAdd['city']      		=   $request->city_id;				
			}
			if($request->postal_code) {
				$userAdd['postal_code']   	=   $request->postal_code;
			}
			$this->user_addr->whereId($id)->update($userAdd);
			session()->flash("success",__('success_user.-704'));
		}
		return redirect()->route('user.show.address.book');
	}

    /*
    * Method: removeAddress
    * Description: to remove address from user_address_book
    * Author: Surajit 
    */
    public function removeAddress($id){
    	$userid = Auth::user()->id;
    	$status = '';
    	$response = [
    		'jsonrpc'   => '2.0'
    	];        

    	$data =	$this->user_addr->whereId($id)->where('user_id', @Auth::user()->id)->first();	 
    	if(@$data) {
    		$this->user_addr->whereId($id)->delete();
    		$status = 'Remove';
    	}

    	if($status == '') {
    		$response['error']['message'] = 'Fail';
    	} else {
    		$response['sucess']['result'] = $status;
    		if($status == 'Remove') {
    			$response['sucess']['message'] = __('front_static.remove_success');
    		}
    	}
    	return $response;
    }

	/**
	* Method: setDefault
	* Description: This method is used to store user details into user_address_books table
	* Author: Surajit
	*/
	public function setDefault($id) {
		$addData = $this->user_addr->whereId($id)->first();
		if($addData->user_id == @Auth::user()->id) {
			$this->user_addr->where('user_id', @Auth::user()->id)->update(['is_default' => 'N']);
			$this->user_addr->whereId($id)->update(['is_default' => 'Y']);			
			session()->flash("success",__('success_user.-705'));
		}
		return redirect()->back();
	}

    /**
    * Method  : fetchCity
    * Use     : Auto complete city list.  
    * Author  : surajit
    */
    public function fetchCity(Request $request){
    	$response = array(
    		'jsonrpc' => '2.0'
    	);
    	if($request->get('city')) {
    		$city = $request->get('city');    	
    		$data = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
    		$data = $data->where(function($q1) use($request) {
                    $q1 = $q1->where('status', '!=', 'I');
                });
    		$data = $data->whereHas('cityDetailsByLanguage',function($query) use($request) {
    			$query->whereRaw("UPPER(name) LIKE '%". strtoupper($request->city)."%'");
    		})->get();
    		if($data->count() > 0) {
    			$output = '<ul class="dropdown-menu" style="display:block;width: 100%;padding:10px;max-height: 200px;overflow: auto; border-color: #999;">';
    			foreach($data as $row) {
    				$output .= '
    				<a href="javascript:void(0);" style="color:black;"><li style="font-weight:500; padding: 2px;">'.$row->cityDetailsByLanguage->name.'</li></a>
    				';
    			}
    			$output .= '</ul>';
    		} else {
    			$output = 'No records found..';
    		}
    		$response['result'] = $output;
    		$response['status'] = 'SUCCESS';
    		return response()->json($response);
    	} else {
    		$response['status'] = 'ERROR';    
    		return response()->json($response);    	
    	}
    }

    /**
    * Method  : checkCity
    * Use     : check existing in city list.  
    * Author  : surajit
    */
    public function checkCity(Request $request){
    	$response = array(
    		'jsonrpc' => '2.0'
    	);
    	
    	if($request->get('city')) {
    		$city = $request->get('city');
    		// $data = City::where('name', $city)->first();
    		$data = City::where('status', '!=', 'D')->with('cityDetailsByLanguage');
    		$data = $data->whereHas('cityDetailsByLanguage',function($query) use($request) {
    			$query->where('name', $request->city);
    		})->first();

    		if($data) {
    			$response['status'] = 'SUCCESS';
    			return response()->json($response);    			
    		} else {
    			$response['status'] = 'ERROR';    
    			return response()->json($response);    	
    		}
    	} else {
    		$response['status'] = 'ERROR';    
    		return response()->json($response);    	
    	}
    }

	/**
	* Method: rewardsPoint
	* Description: This method is used to show rewards point page
	* Author: Surajit
	*/
	public function rewardsPoint() {
		$data['rewards'] = $this->order->where('user_id', @Auth::user()->id)
		->where(function($query) {
			$query->orWhere('loyalty_point_used', '>', 0);
			$query->orWhere('loyalty_point_received', '>', 0);
		})
		->whereIn('status', ['N', 'OP', 'DA', 'RP', 'OD', 'OA'])
		->orderBy('id', 'DESC')->get();
		return view('modules.dashboard.my_reward',$data);
	}
}
