<?php

namespace App\Http\Controllers\Modules\CheckOut;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// repository
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductVariantDetailRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CountryRepository;
use App\Repositories\UserRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\UserAddressBookRepository;
use Illuminate\Support\Str;

use App\Merchant;
use App\Models\City;
use App\Models\CitiDetails;
use App\Models\Setting;
use App\Models\ShippingCost;
use App\Models\ZoneDetail;
use App\Models\ZoneRateDetail;
use App\Models\OrderDetail;
use Session;
use Auth;

class UserCheckOutController extends Controller
{
	protected $user, $merchant, $cart, $cartDetails, $order, $orderDetails, $product, $productVariant, $productVariantDetail,$country,$user_addr;

	/**
	* __construct
	* This method is used to initilize the instance
	* Author: Surajit
	*/
	public function __construct(UserRepository $user,
		MerchantRepository $merchant,
		CartMasterRepository $cart,
		CartDetailRepository $cartDetails,
		OrderMasterRepository $order,
		OrderDetailRepository $orderDetails,
		OrderSellerRepository $orderSeller,
		ProductVariantRepository $productVariant,
		ProductRepository $product,
		ProductVariantDetailRepository $productVariantDetail,
		CountryRepository $country,
		UserAddressBookRepository $user_addr
	)
	{
		$this->user         			= $user;
		$this->merchant         		= $merchant;
		$this->cart         			= $cart;
		$this->cartDetails  			= $cartDetails;
		$this->order         			= $order;
		$this->orderDetails  			= $orderDetails;
		$this->orderSeller  			= $orderSeller;
		$this->productVariant  			= $productVariant;
		$this->productVariantDetail  	= $productVariantDetail;
		$this->product  				= $product;
		$this->country            		= $country;
		$this->user_addr            	= $user_addr;
		$this->middleware('auth');
	}

	/**
	* Method: checkOut
	* Description: This method is used to show the check out page.
	* Author: Surajit
	*/
	public function checkOut() {		
		$data = [];
		$data['city'] 	= City::get();
		$data['bcity'] 	= City::get();
		$data['country'] = $this->country->with('countryDetailsBylanguage')->where('status','!=','D')->get();
		$data['language_id'] = getLanguage()->id;

		$data['cart'] = $cart = $this->cart->where(['user_id' => Auth::user()->id])->first();
		if($data['cart']){
			$data['userAddr'] 	= $this->user_addr->where(['user_id' => @Auth::user()->id])->with('getCountry')->get();
			// to get the products informationa from cart_details
			$data['cartDetails'] = $this->cartDetails->where(['cart_master_id' => $cart->id])
			->with('productMarchant','defaultImage','productByLanguage','productVariantDetails','getProduct:id,weight,slug')
			->get();
			
			$checkOutsideMerchant = 'N';
			foreach (@$data['cartDetails'] as $sell) {
				if($sell->productMarchant->country != 134){
					$checkOutsideMerchant = 'Y';
				}
			}
			//dd($checkOutsideMerchant);
		    $data['outsideMerchant'] = $checkOutsideMerchant;
			if(Session::get('orderNo')) {
				$data['orderMaster'] = $this->order->where(['order_no' => Session::get('orderNo')])
				->with('shippingAddress.getCountry','billingAddress.getCountry','getCountry')->first();
			}
		} else {
			return redirect()->route('home');					
		}
		return view('modules.checkout.checkout')->with($data);
	}

	/**
	* Mehtod: storeOreder,
	* Description: This method is used to store Oreder 
	* Author: Surajit
	*/
	public function storeOreder(Request $request) {
		// country 134 = kuwait
		//for outside kuwait checking
		if(@$request->seller_id) {
			foreach ($request->seller_id as $value) {
				$merchantId[] = $value[0];
			}
			$merchantCn = $this->merchant->select('id','country','company_name')
			->whereIn('id', $merchantId)
			->where('country', '!=', 134)
			->get();
			$mrchCount = $merchantCn->count();
		}

		//for outside kuwait checking
		if(@$request->shp_save_addr) {
			$userCountry = $this->user_addr->where('id', $request->shp_save_addr)->first();
			$userCountry = $userCountry->country;				
		} else {
			$userCountry = @$request->shipping_country;
		}
		//for outside kuwait checking
		if(@$mrchCount > 0 && $userCountry != '134') {
			session()->flash("error",__('success_user.-709'));
			Session::put('merchantCn',$merchantCn);
			return redirect()->back();
		} else {
			//for international order checking
			if(@$mrchCount > 0 || $userCountry != '134') {
				$setting = Setting::first();
				if($setting->enable_international_order == 'Y') {
					foreach ($request->seller_id as $value) {
						$merchantIdSlct[] = $value;
					}

					$merchantDetail = $this->merchant->whereIn('id', $merchantIdSlct)->where('international_order', 'OFF')->get();
					$mrchCn = $merchantDetail->count();

					if(@$mrchCn > 0) {
						Session::put('merchantCn',$merchantDetail);
						session()->flash("error",__('success_user.-710'));
						return redirect()->back();
					} else {
						//insert the orders into multiple tables
						$orderNo = $this->storeOrderAll($request);
						return redirect()->route('order.placement', $orderNo);	
					}
				} else {
					session()->flash("error",__('success_user.-711'));
					return redirect()->back();		
				}
			} else {
				//insert the orders into multiple tables
				$orderNo = $this->storeOrderAll($request);
				return redirect()->route('order.placement', $orderNo);	
			}		
		}
	}


	/**
	* Method: storeOrderAll
	* Description: This method is used to process the order when click on the 
	*/
	private function storeOrderAll($request) {
		if(!$request->shp_save_addr) {
			$validator = $request->validate([
				'shipping_fname'	 => 'required',
				'shipping_lname'	 => 'required',
				'shipping_email'     => 'required',
				'shipping_phone'     => 'required',
				'shipping_country'   => 'required',
				'shipping_street'    => 'required',
				'payment_method'     => 'required',
			]);
		} elseif(!$request->bill_save_addr && !$request->same_as_shipping) {
			$validator = $request->validate([
				'billing_fname'		 => 'required',
				'billing_lname'		 => 'required',
				'billing_email'      => 'required',
				'billing_phone'      => 'required',
				'billing_country'    => 'required',
				'billing_street'     => 'required',
				'payment_method'     => 'required',
			]);
		} elseif(!$request->same_as_shipping) {
			$validator = $request->validate([
				'shp_save_addr'		=> 'required',
				'bill_save_addr'	=> 'required',
				'payment_method'    => 'required',
			]);			
		} else {
			$validator = $request->validate([
				'shp_save_addr'		=> 'required',
				'payment_method'    => 'required',
			]);			
		}
		if($validator) {
			# when customer edit address from place order page.
			if(Session::get('orderNo')) {
				$orderNo = Session::get('orderNo');
				$order = $this->updateOrderMaster($request);
			} else {
				# when click on the ckeckout button on cart page.
				$order = $this->insertOrderMaster($request);

				# insert to order details
				$orderDetail['order_master_id'] 	= $order->id;
				$orderDetail['product_id'] 			= $request->product_id;
				$orderDetail['product_variant_id'] 	= $request->product_variant_id;
				$orderDetail['variants'] 			= $request->variants;
				$orderDetail['seller_id'] 			= $request->seller_id;
				$orderDetail['quantity'] 			= $request->quantity;
				$orderDetail['weight'] 				= $request->weight;
				$orderDetail['original_price'] 		= $request->original_price;
				$orderDetail['discounted_price'] 	= $request->discounted_price;
				$orderDetail['sub_total'] 			= $request->sub_total;
				$orderDetail['total'] 				= $request->total;

				$this->insertOrderDetails($orderDetail);

				$this->calculateShippingPrice($order);

				$orderSeller['order_master_id'] 	= $order->id;
				$sellerProducts = $this->insertOrderSeller($orderSeller,$order);

				# update is_more_merchant or not
				$ordId = sprintf('%08d', $order->id);
				$orderNumber = "ORDASW".$ordId;
				$update = [
					'order_no' => $orderNumber
				];
				if($sellerProducts > 1) {
					$update['is_more_seller'] = 'Y';
				}
				$this->order->whereId($order->id)->update($update);
				$orderNo = $orderNumber;
			}
		}
		return $orderNo;	
	}


	/**
	* Method: insertOrderMaster
	* Description: This method is used to insert to order master table.
	* Author: Surajit, Modified by Sanjoy
	*/
	private function insertOrderMaster($request) {
		$order['user_id']     			= @Auth::user()->id;
		$order['shipping_fname']     	= $request->shipping_fname;
		$order['shipping_lname']     	= $request->shipping_lname;
		$order['shipping_email']     	= $request->shipping_email;
		$order['shipping_phone']     	= $request->shipping_phone;
		$order['shipping_country']   	= $request->shipping_country;
		$order['shipping_street']    	= $request->shipping_street;
		$order['shipping_block']    	= $request->shipping_block;
		$order['shipping_building']  	= $request->shipping_building;
		$order['shipping_more_address'] = $request->shipping_more_address;
		if($request->shipping_city) {
			$order['shipping_city']     = $request->shipping_city;
		} elseif($request->shipping_city_id) {
			$cityId = CitiDetails::where('name', $request->shipping_city_id)->first();
			$order['shipping_city_id']  = $cityId->city_id;				
			$order['shipping_city']     = $request->shipping_city_id;				
		}
		
		if($request->shipping_postal_code) {
			$order['shipping_postal_code'] = $request->shipping_postal_code;
		}
		// location, lat, lng
		$order['location']				= @$request->location;
		$order['lat']					= @$request->lat;
		$order['lng']					= @$request->lng;
		# save address book
		if(!@$request->shp_save_addr) {
			if(@$request->save_address == 'Y') {
				$this->saveAddressBook($order);
			}
		}

		if($request->same_as_shipping) {
			if($request->shp_save_addr) {
				$addrShp = $this->user_addr->whereId($request->shp_save_addr)->first();
				$order['billing_address_id']   	=   $request->shp_save_addr;
				$order['billing_fname']     	=   $addrShp->shipping_fname;
				$order['billing_lname']     	=   $addrShp->shipping_lname;
				$order['billing_email']     	=   $addrShp->email;
				$order['billing_phone']     	=   $addrShp->phone;
				$order['billing_country']   	=   $addrShp->country;
				$order['billing_city']      	=   $addrShp->city;
				$order['billing_street']    	=   $addrShp->street;
				$order['billing_block']  	  	=   $addrShp->block;
				$order['billing_building']  	=   $addrShp->building;
				$order['billing_postal_code']  	=   $addrShp->postal_code;
				$order['billing_more_address'] 	=   $addrShp->more_address;
				if($addrShp->country == 134) {
					$order['billing_city_id']  	=   $addrShp->city_id;	
				}
			} else {
				$order['billing_fname']     	=   $request->shipping_fname;
				$order['billing_lname']     	=   $request->shipping_lname;
				$order['billing_email']     	=   $request->shipping_email;
				$order['billing_phone']     	=   $request->shipping_phone;
				$order['billing_country']   	=   $request->shipping_country;
				// $order['billing_city']      	=   $request->shipping_city;
				$order['billing_street']    	=   $request->shipping_street;
				$order['billing_block']    		=   $request->shipping_block;
				$order['billing_building']  	=   $request->shipping_building;
				$order['billing_more_address']  =   $request->shipping_more_address;
				if($request->shipping_postal_code) {
					$order['billing_postal_code']  =   $request->shipping_postal_code;
				}
				if($request->shipping_city) {
					$order['billing_city']      =   $request->shipping_city;
				} elseif($request->shipping_city_id) {
					$cityId = CitiDetails::where('name', $request->shipping_city_id)->first();
					$order['billing_city_id']   =   $cityId->city_id;	
					$order['billing_city']      =   $request->shipping_city_id;			
				}
			}
		} else {
			$order['billing_fname']     	=   $request->billing_fname;
			$order['billing_lname']     	=   $request->billing_lname;
			$order['billing_email']     	=   $request->billing_email;
			$order['billing_phone']     	=   $request->billing_phone;
			$order['billing_country']   	=   $request->billing_country;
			// $order['billing_city']      	=   $request->billing_city;
			$order['billing_street']    	=   $request->billing_street;
			$order['billing_block']   		=   $request->billing_block;
			$order['billing_building']    	=   $request->billing_building;
			$order['billing_more_address']  =   $request->billing_more_address;
			if($request->billing_postal_code) {
				$order['billing_postal_code']   =   $request->billing_postal_code;
			}
			if($request->billing_city) {
				$order['billing_city']      = $request->billing_city;
			} elseif($request->billing_city_id) {
				$cityId = CitiDetails::where('name', $request->billing_city_id)->first();
				$order['billing_city_id']   = $cityId->city_id;	
				$order['billing_city']      = $request->billing_city_id;			
			}
		}

		if($request->shp_save_addr) {
			$addrShp = $this->user_addr->whereId($request->shp_save_addr)->first();
			$order['shipping_address_id']   = $request->shp_save_addr;
			$order['shipping_fname']     	= $addrShp->shipping_fname;
			$order['shipping_lname']     	= $addrShp->shipping_lname;
			$order['shipping_email']     	= $addrShp->email;
			$order['shipping_phone']     	= $addrShp->phone;
			$order['shipping_country']   	= $addrShp->country;
			$order['shipping_city']      	= $addrShp->city;
			$order['shipping_street']    	= $addrShp->street;
			$order['shipping_block']    	= $addrShp->block;
			$order['shipping_building']  	= $addrShp->building;
			$order['shipping_postal_code']  = $addrShp->postal_code;
			$order['shipping_more_address'] = $addrShp->more_address;
			if($addrShp->country == 134) {
				$order['shipping_city_id']  = $addrShp->city_id;	
			}
			// location, lat, lng
			$order['location']				= @$addrShp->location;
			$order['lat']					= @$addrShp->lat;
			$order['lng']					= @$addrShp->lng;
		}

		if($request->bill_save_addr) {
			$addrBill = $this->user_addr->whereId($request->bill_save_addr)->first();
			$order['billing_address_id']    = $request->bill_save_addr;
			$order['billing_fname']     	= $addrBill->shipping_fname;
			$order['billing_lname']     	= $addrBill->shipping_lname;
			$order['billing_email']     	= $addrBill->email;
			$order['billing_phone']     	= $addrBill->phone;
			$order['billing_country']   	= $addrBill->country;
			$order['billing_city']      	= $addrBill->city;
			$order['billing_street']    	= $addrBill->street;
			$order['billing_block']   		= $addrBill->block;
			$order['billing_building']    	= $addrBill->building;
			$order['billing_postal_code']   = $addrBill->postal_code;
			$order['billing_more_address']  = $addrBill->more_address;
			if($addrBill->country == 134) {
				$order['billing_city_id']   = $addrBill->city_id;	
			}
		}
		$order['payment_method']   		= $request->payment_method;
		$order['order_type']   			= 'I';
		$order['status']	   			= 'I';
		$order['verify_code']	   		= time().rand(111, 999);
		$orderMaster = $this->order->create($order);
		return $orderMaster;
	}

	/**
	* Method: saveAddressBook
	* Description: This method is used to save address to address book.
	*/
	public function saveAddressBook($reqData) {
		$totAddress = $this->user_addr->where(['user_id' => @Auth::user()->id])->count();
		$isDefault = 'N';
		if($totAddress == 0) {
			$isDefault = 'Y';
		}
		$this->user_addr->create([
			'user_id'		 => @Auth::user()->id,
			'shipping_fname' => $reqData['shipping_fname'],
			'shipping_lname' => $reqData['shipping_lname'],
			'email'			 => $reqData['shipping_email'],
			'phone'			 => $reqData['shipping_phone'],
			'location'		 => $reqData['location'],
			'lat'			 => $reqData['lat'],
			'lng'			 => $reqData['lng'],
			'country'		 => $reqData['shipping_country'],
			'street'		 => $reqData['shipping_street'],
			'block'			 => $reqData['shipping_block'],
			'building'	     => $reqData['shipping_building'],
			'more_address'	 => $reqData['shipping_more_address'],
			'city_id'		 => @$reqData['shipping_city_id'],
			'city'			 => $reqData['shipping_city'],
			'postal_code' 	 => @$reqData['shipping_postal_code'],
			'is_default'	 => $isDefault
		]);
	}

	/**
	* Method: updateOrderMaster
	* Description: This method is used to update order master data. When user edit from place order page then edit shipping and billing address.
	* Author: Surajit, modified by Sanjoy
	*/
	private function updateOrderMaster($request) {
		$order['shipping_fname']     	=   $request->shipping_fname;
		$order['shipping_lname']     	=   $request->shipping_lname;
		$order['shipping_email']     	=   $request->shipping_email;
		$order['shipping_phone']     	=   $request->shipping_phone;
		$order['shipping_country']   	=   $request->shipping_country;
		$order['shipping_street']    	=   $request->shipping_street;
		$order['shipping_block']    	=   $request->shipping_block;
		$order['shipping_building']  	=   $request->shipping_building;
		$order['shipping_more_address'] =   $request->shipping_more_address;

		// location, lat, lng
		$order['location']				= @$request->location;
		$order['lat']					= @$request->lat;
		$order['lng']					= @$request->lng;

		if($request->shipping_city) {
			$order['shipping_city']      =   $request->shipping_city;
		} elseif($request->shipping_city_id) {
			$cityId = CitiDetails::where('name', $request->shipping_city_id)->first();
			$order['shipping_city_id']      =   $cityId->city_id;	
			$order['shipping_city']      	=   $request->shipping_city_id;				
		}
		
		if($request->shipping_postal_code) {
			$order['shipping_postal_code']  =   $request->shipping_postal_code;
		}
		
		if($request->same_as_shipping) {
			if($request->shp_save_addr) {
				$addrShp = $this->user_addr->whereId($request->shp_save_addr)->first();
				$order['billing_address_id']   	=   $request->shp_save_addr;
				$order['billing_fname']     	=   $addrShp->shipping_fname;
				$order['billing_lname']     	=   $addrShp->shipping_lname;
				$order['billing_email']     	=   $addrShp->email;
				$order['billing_phone']     	=   $addrShp->phone;
				$order['billing_country']   	=   $addrShp->country;
				$order['billing_city']      	=   $addrShp->city;
				$order['billing_street']    	=   $addrShp->street;
				$order['billing_block']  	  	=   $addrShp->block;
				$order['billing_building']  	=   $addrShp->building;
				$order['billing_postal_code']  	=   $addrShp->postal_code;
				$order['billing_more_address'] 	=   $addrShp->more_address;
				if($addrShp->country == 134) {
					$order['billing_city_id']  =   $addrShp->city_id;	
				}
			} else {
				$order['billing_fname']     	=   $request->shipping_fname;
				$order['billing_lname']     	=   $request->shipping_lname;
				$order['billing_email']     	=   $request->shipping_email;
				$order['billing_phone']     	=   $request->shipping_phone;
				$order['billing_country']   	=   $request->shipping_country;
				// $order['billing_city']      	=   $request->shipping_city;
				$order['billing_street']    	=   $request->shipping_street;
				$order['billing_block']    		=   $request->shipping_block;
				$order['billing_building']  	=   $request->shipping_building;
				$order['billing_more_address']  =   $request->shipping_more_address;
				if($request->shipping_postal_code) {
					$order['billing_postal_code']  =   $request->shipping_postal_code;
				}
				if($request->shipping_city) {
					$order['billing_city']      =   $request->shipping_city;
				} elseif($request->shipping_city_id) {
					$cityId = CitiDetails::where('name', $request->shipping_city_id)->first();
					$order['billing_city_id']   =   $cityId->city_id;	
					$order['billing_city']      =   $request->shipping_city_id;			
				}
			}
		} else {
			$order['billing_fname']     	=   $request->billing_fname;
			$order['billing_lname']     	=   $request->billing_lname;
			$order['billing_email']     	=   $request->billing_email;
			$order['billing_phone']     	=   $request->billing_phone;
			$order['billing_country']   	=   $request->billing_country;
			// $order['billing_city']      	=   $request->billing_city;
			$order['billing_street']    	=   $request->billing_street;
			$order['billing_block']   		=   $request->billing_block;
			$order['billing_building']    	=   $request->billing_building;
			$order['billing_more_address']  =   $request->billing_more_address;
			if($request->billing_postal_code) {
				$order['billing_postal_code']   =   $request->billing_postal_code;
			}
			if($request->billing_city) {
				$order['billing_city']      	=   $request->billing_city;
			} elseif($request->billing_city_id) {
				$cityId = CitiDetails::where('name', $request->billing_city_id)->first();
				$order['billing_city_id']   =   $cityId->city_id;
				$order['billing_city']      =   $request->billing_city_id;			
			}
		}

		if($request->shp_save_addr) {
			$addrShp = $this->user_addr->whereId($request->shp_save_addr)->first();
			$order['shipping_address_id']   =   $request->shp_save_addr;
			$order['shipping_fname']     	=   $addrShp->shipping_fname;
			$order['shipping_lname']     	=   $addrShp->shipping_lname;
			$order['shipping_email']     	=   $addrShp->email;
			$order['shipping_phone']     	=   $addrShp->phone;
			$order['shipping_country']   	=   $addrShp->country;
			$order['shipping_city']      	=   $addrShp->city;
			$order['shipping_street']    	=   $addrShp->street;
			$order['shipping_block']    	=   $addrShp->block;
			$order['shipping_building']  	=   $addrShp->building;
			$order['shipping_postal_code']  =   $addrShp->postal_code;
			$order['shipping_more_address'] =   $addrShp->more_address;

			// location, lat, lng
			$order['location']				= @$addrShp->location;
			$order['lat']					= @$addrShp->lat;
			$order['lng']					= @$addrShp->lng;
			if($addrShp->country == 134) {
				$order['shipping_city_id']  =   $addrShp->city_id;	
			}
		}
		if($request->bill_save_addr) {
			$addrBill = $this->user_addr->whereId($request->bill_save_addr)->first();
			$order['billing_address_id']    =   $request->bill_save_addr;
			$order['billing_fname']     	=   $addrBill->shipping_fname;
			$order['billing_lname']     	=   $addrBill->shipping_lname;
			$order['billing_email']     	=   $addrBill->email;
			$order['billing_phone']     	=   $addrBill->phone;
			$order['billing_country']   	=   $addrBill->country;
			$order['billing_city']      	=   $addrBill->city;
			$order['billing_street']    	=   $addrBill->street;
			$order['billing_block']   		=   $addrBill->block;
			$order['billing_building']    	=   $addrBill->building;
			$order['billing_postal_code']   =   $addrBill->postal_code;
			$order['billing_more_address']  =   $addrBill->more_address;
			if($addrShp->country == 134) {
				$order['billing_city_id']  =   $addrBill->city_id;	
			}
		}
		$order['payment_method']   		=   $request->payment_method;
		$orderUpdate = $this->order->where('order_no', Session::get('orderNo'))->update($order);

		#calculate shipping price.
		$orderId = $this->order->where(['order_no' => Session::get('orderNo')])->first();
		$this->calculateShippingPrice($orderId);

		Session::forget('orderNo');
		return $orderUpdate;
	}

	/**
	* 
	*/
	private function insertOrderDetails($orderDetail) {
		$i=0;
		foreach ($orderDetail['product_id'] as $pro) {
			if($pro == @$orderDet['product_id']) {
				$i++;
			} else {
				$i = 0;
			}
			$orderDet['order_master_id']   	=   $orderDetail['order_master_id'];
			$orderDet['product_id']   		=   $pro;
			$orderDet['seller_id']   		=   $orderDetail['seller_id'][$pro][$i];
			$orderDet['quantity']   		=   $orderDetail['quantity'][$pro][$i];
			$orderDet['status']   			=   'N';
			if(@$orderDetail['product_variant_id'][$pro]) {
				$orderDet['product_variant_id'] =   $orderDetail['product_variant_id'][$pro][$i];
				$orderDet['variants']   		=   $orderDetail['variants'][$pro][$i];
			}
			$orderDetailsIns = OrderDetail::create($orderDet);
			
			$this->updateOrderDetails($orderDetailsIns);
		}
		$this->updateOrderMasterPrice($orderDetail['order_master_id']);
	}

	/**
	* 
	*/
	private function updateOrderDetails($orderDetailsIns) {
		$productVariant = $this->checkStockAndVariant($orderDetailsIns->product_variant_id);
		$product = $this->product->whereId($orderDetailsIns->product_id)->first();
		$originalPrice = 0;
		$discountedPrice = 0;
		$total_discount = 0;
		$subtotal = 0;
		$total = 0;
		$weight = 0;
		if(@$productVariant->price != 0) {
			#if discount price is available then calculate this.
			$subtotal = $total = $productVariant->price * $orderDetailsIns->quantity;
			$originalPrice = $productVariant->price;
			if($productVariant->discount_price && date('Y-m-d')>=$productVariant->from_date && date('Y-m-d')<=$productVariant->to_date) {
				$total_discount = ($productVariant->price - $productVariant->discount_price) * $orderDetailsIns->quantity;
				$discountedPrice = $productVariant->discount_price;
			} else {
				$total_discount = 0.000;
			}
			$total = $subtotal - $total_discount;
			$weight = $productVariant->weight * $orderDetailsIns->quantity;
		} else {
			$originalPrice = $product->price;
			$discountedPrice = $product->discount_price;
			if($product->discount_price != 0 && date('Y-m-d')>=$product->from_date && date('Y-m-d')<=$product->to_date) {
				$total_discount = ($product->price - $product->discount_price) * $orderDetailsIns->quantity;
			} else {
				$total_discount = 0.000;
			}
			$subtotal = $product->price * $orderDetailsIns->quantity;
			$total = $subtotal - $total_discount;
			if(!@$productVariant) {
				$weight = $product->weight * $orderDetailsIns->quantity;
			} else {
				$weight = $productVariant->weight * $orderDetailsIns->quantity;
			}
		}
		OrderDetail::where('id', $orderDetailsIns->id)->update([
			'original_price'	=> $originalPrice,
			'discounted_price' 	=> $discountedPrice,
			'sub_total'			=> $subtotal,
			'total'				=> $total,
			'weight'			=> $weight
		]);
	}

	/**
	* 
	*/
	private function updateOrderMasterPrice($orderMasterId) {
		$order = $this->order->whereId($orderMasterId)->first();
		$orderDetails = $this->orderDetails->where('order_master_id', $orderMasterId);
		$discount = $orderDetails->sum('discounted_price');
		$subTotal = $orderDetails->sum('sub_total');
		$orderTotal = $orderDetails->sum('total');
		$totalDiscount = $subTotal - $orderTotal;
		$totalWeight = $orderDetails->sum('weight');

		return $this->order->whereId($orderMasterId)->update([
			'total_discount'		=> $totalDiscount,
			'subtotal' 				=> $subTotal,
			'order_total'			=> $orderTotal,
			// 'total_commission'		=> $totalCommission
		]);
	}

	/**
	* Method: checkStockAndVariant
	* Description: This method is used to check variant stock and correct variand ID.
	* Author: Sanjoy
	*/
	private function checkStockAndVariant($proVarId) {
		$variants = $this->productVariant->whereId($proVarId)->first();
		if($variants && $variants->stock_quantity) {
			return $variants;
		}
		return false;
	}

	/**
	* Method: insertOrderSeller
	* Description: This method is used to insert to order seller table
	* Author: Sanjoy
	*/
	private function insertOrderSeller($orderSeller, $order) {
		$sellerProducts = $this->orderDetails->where(['order_master_id' => $orderSeller['order_master_id']])->groupBy('seller_id')->get();

		foreach ($sellerProducts as $sp) {
			$sellerCommision = $this->merchant->whereId($sp->seller_id)->first();

			$totalWeight = $this->orderDetails->where([
				'order_master_id' => $orderSeller['order_master_id'],
				'seller_id' 	  => $sp->seller_id
			])->sum('weight');
			$subTotal = $this->orderDetails->where([
				'order_master_id' => $orderSeller['order_master_id'],
				'seller_id' 	  => $sp->seller_id
			])->sum('sub_total');
			$total = $this->orderDetails->where([
				'order_master_id' => $orderSeller['order_master_id'],
				'seller_id' 	  => $sp->seller_id
			])->sum('total');
			$totalDiscount = $subTotal - $total;

			$orderSel['seller_id']   		= $sp->seller_id;
			$orderSel['order_master_id']   	= $orderSeller['order_master_id'];
			$orderSel['total_weight']   	= $totalWeight;
			$orderSel['subtotal']   		= $subTotal;
			$orderSel['total_discount']   	= $totalDiscount;
			$orderSel['order_total']   		= $total;
			$orderSel['status']				= 'N';
			$orderSel['verify_code']		= time().rand(111, 999);
			$orderSelD = $this->orderSeller->create($orderSel);
			$orderSellerId = $orderSelD->id;
		}
		return $sellerProducts->count();
	}


	/**
	* Method: calculateSellerShippingPrice
	* Description: This method is used to calculate seller shipping price
	* Author: Sanjoy
	*/
	private function calculateSellerShippingPrice($orderMaster, $orderSellerId, $total) {
		$sellers = $this->orderDetails->where(['order_master_id' => $orderMaster->id])->pluck('seller_id')->toArray();

		$uniqueSellers = array_unique($sellers);
		$setting = Setting::first();
		$insideShippingCost = 0;
		$outsideShippingCost = 0;
		foreach ($uniqueSellers as $key => $value) {
			$merchant = Merchant::where(['id' => $value])->first();
			if($merchant->country == 134 && $orderMaster->shipping_country == 134) {
				$insideShippingCost += @$merchant->inside_shipping_cost;
			} else {
				$details = $this->orderDetails->where(['order_master_id' => $orderMaster->id, 'seller_id' => $value])->get();
				$weight = $details->sum('weight');
				$infinityChk = ShippingCost::where('infinity_weight', 'Y')->first();
				if($infinityChk && @$weight >= $infinityChk->from_weight) {
					$sCost = $infinityChk;
				} else {
					$sCost = ShippingCost::where(function($where) use ($weight) {
						$where->where(function($where1) use ($weight) {
							$where1->where('from_weight', '<=', @$weight)
							->where('to_weight', '>', @$weight);
						})
						->orWhere(function($where2) use ($weight) {
							$where2->where('from_weight', '<', @$weight)
							->where('to_weight', '>=', @$weight);
						});
					})
					->first();
				}
				$outsideShippingCost += @$sCost->internal_order_rate;
			}
		}

		$this->orderSeller->where(['id' => $orderSellerId])->update([
			'shipping_price' => $outsideShippingCost + $insideShippingCost, 
			// 'order_total'    => $total + $outsideShippingCost + $insideShippingCost
		]);
	}


	/**
	* Method: calculateShippingPrice
	* Description: This method is used to calculate shipping price
	* Author: Sanjoy
	*/
	private function calculateShippingPrice($orderMaster) {
		$sellers = $this->orderDetails->where(['order_master_id' => $orderMaster->id])->pluck('seller_id')->toArray();

		$uniqueSellers = array_unique($sellers);
		$setting = Setting::first();
		$insideShippingCost = 0;
		$outsideShippingCost = 0;
		$merHgCst = [];
		foreach ($uniqueSellers as $key => $value) {
			$merchant = Merchant::where(['id' => $value])->first();
			if($merchant->country == 134 && $orderMaster->shipping_country == 134) {
				# if city delivery applicable for merchant
				if($merchant->applicable_city_delivery == 'Y') {
					$cityDelivery = City::where(['id' => $orderMaster->shipping_city_id])->first();
					if(@$cityDelivery->delivery_fees > $merchant->inside_shipping_cost) {
						$merHgCst[] = @$cityDelivery->delivery_fees;
					} else {
						$merHgCst[] = @$merchant->inside_shipping_cost;
					}
				} else {
					$merHgCst[] = @$merchant->inside_shipping_cost;
				}
			} else {
				$details = $this->orderDetails->where(['order_master_id' => $orderMaster->id, 'seller_id' => $value])->get();
				$weight = $details->sum('weight');
				# if seller from outside kuwait and buyer from kuwait
				if($orderMaster->shipping_country == 134) {
					$zone = ZoneDetail::where(['country_id' => $merchant->country])->first();
				} else {
					$zone = ZoneDetail::where(['country_id' => $orderMaster->shipping_country])->first();
				}
				$infinityChk = ZoneRateDetail::where(['zone_id' => $zone->zone_master_id])->where('infinity_weight', 'Y')->first();
				if($infinityChk && @$weight >= $infinityChk->from_weight) {
					$sCost = $infinityChk;
				} else {
					$sCost = ZoneRateDetail::where(['zone_id' => $zone->zone_master_id])
					->where(function($where) use ($weight) {
						$where->where(function($where1) use ($weight) {
							$where1->where('from_weight', '<=', @$weight)
							->where('to_weight', '>', @$weight);
						})
						->orWhere(function($where2) use ($weight) {
							$where2->where('from_weight', '<', @$weight)
							->where('to_weight', '>=', @$weight);
						});
					})
					->first();
				}
				$outsideShippingCost += @$sCost->internal_outside_kuwait;
			}
		}
		if(count($merHgCst)) {
			$insideShippingCost = max($merHgCst);
		}
		$orderDet = $this->order->where(['id' => $orderMaster->id])->first();
		
		$this->order->where(['id' => $orderMaster->id])->update([
			'shipping_price' 	=> 0, 
			'order_total' 		=> $orderDet->order_total - $orderDet->shipping_price
		]);

		$orderDet = $this->order->where(['id' => $orderMaster->id])->first();
		// dd($outsideShippingCost, $insideShippingCost);
		$this->order->where(['id' => $orderMaster->id])
		->update([
			'shipping_price' 	=> $outsideShippingCost + $insideShippingCost, 
			'order_total' 		=> $orderDet->order_total + $outsideShippingCost + $insideShippingCost
		]);
	}


	/**
	* 
	*/
	public function editAddress($orderNo) {
		Session::put('orderNo', $orderNo);
		return redirect()->route('check.out');
	}
}
