<?php

namespace App\Http\Controllers\Modules\Home;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\UserFavorite;
use App\Models\Category;
use App\Models\Setting;
use App\Models\OrderDetail;
use App\Models\OrderSeller;
use App\Models\UserRegId;
use App\Models\NotificationData;
use App\Merchant;
use Session;
use Config;
use Auth;

use App\Models\BannerContent;

class HomeController extends Controller
{
	/**
	* Method: index
	* Description: This method is used to show home page
	* Author: Surajit
	*/
    public function index() {
        $setting = Setting::first();
        return view('modules.home.under_maintanance', compact('setting'));
        // $setting = Setting::first();
        // if($setting->maintenence == 'Y') {
        //     return view('modules.home.under_maintanance', compact('setting'));
        // } else {
        //     $data['featured_products'] = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date')
        //     ->with(['defaultImage:product_id,image', 
        //         'productByLanguage:language_id,product_id,title,description'])
        //     ->with(['productMarchant','wishlist'])
        //     ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N', 'is_featured' => 'Y'])
        //     ->inRandomOrder()
        //     ->take(8)
        //     ->get();

        //     $data['new_products'] = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date')
        //     ->with(['defaultImage:product_id,image', 
        //         'productByLanguage:language_id,product_id,title,description'])
        //     ->with(['productMarchant','wishlist'])
        //     ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N'])
        //     ->orderBy('id', 'DESC')
        //     ->take(8)
        //     ->get();

        //     $data['bestDealProduct'] = Product::select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','from_date','to_date')
        //     ->with(['defaultImage:product_id,image', 
        //         'productByLanguage:language_id,product_id,title,description'])
        //     ->with(['productMarchant','wishlist'])
        //     ->where(['status' => 'A','seller_status' => 'A', 'product_hide' => 'N'])
        //     ->where('times_of_sold', '>', 0)
        //     ->orderBy('times_of_sold', 'DESC')
        //     ->take(20)
        //     ->get();

        //     $data['category'] = Category::where(['parent_id' => 0, 'status' => 'A'])
        //     ->with('categoryByLanguage')
        //     ->with(['productByCategoryCount' => function($q) {
        //         $q->whereHas('activeProducts', function($q1) {
        //             $q1->where('product_hide', 'N');
        //             $q1->where('product_hide', 'N');
        //         });
        //     }])
        //     ->whereHas('productByCategoryCount', function($q) {
        //         $q->whereHas('activeProducts', function($q1) {
        //             $q1->where('product_hide', 'N');
        //             $q1->where('product_hide', 'N');
        //         });
        //     })
        //     ->get();

        //     $data['merchant'] = Merchant::where('status', 'A')
        //                                     ->where('hide_merchant', 'N')
        //                                     ->where('premium_merchant', 'Y')
        //                                     ->take(20)
        //                                     ->get();

        //     $data['all_banners'] = BannerContent::where('language_id', getLanguage()->id)->offset(1)->limit(100)->get();
        //     $data['first_banners'] = BannerContent::where('language_id', getLanguage()->id)->first();
        //     return view('modules.home.home', $data);            
        // }
    }
    
    /**
    * Method: underMaintanance
    * Description: This method is used to show underMaintanance page
    * Author: Surajit
    */
    public function underMaintanance() {
        return view('modules.home.under_maintanance');
    }
    /*
    * Method: addFavorite
    * Description: to add/remove cappers to/from favourites
    * Author: Surajit 
    */
    public function addFavorite($id){

        $userid = Auth::user()->id;
        $status = '';
        $response = [
            'jsonrpc'   => '2.0'
        ];        

        $data = UserFavorite::where('product_id',$id)
        ->where('user_id',$userid)
        ->first(); 
       
        if(@$data)
        {
            UserFavorite::where([
                'product_id'    => $id,
                'user_id'       => $userid
            ])
            ->delete();
            $status = 'Remove';
        } else {
            $checkProductWishCount = Product::where('id',$id)->first();
            $newCount = $checkProductWishCount->tot_wishlist_done + 1;
            Product::where('id',$id)->update(['tot_wishlist_done' => $newCount]);

            $favourites = new UserFavorite;
            $favourites->user_id     = $userid;
            $favourites->product_id  = $id;
            $favourites->save();

            $status = 'Add';
        }

        if($status == '') {
            $response['error']['message'] = 'Fail';
        } else {
            $response['sucess']['result'] = $status;
            if($status == 'Add') {
                $response['sucess']['message'] = 'Add to favourit sucessfully';
            }
            if($status == 'Remove') {
                $response['sucess']['message'] = 'Remove to favourit sucessfully';
            }
        }
        return $response;
    }

    /**
    * Method: changeLanguage
    * Description: This method is used to change language
    */
    public function changeLanguage() {
    	$lang = '';
    	if(Session::get('applocale') == 'ar') {
    		$lang = 'en';
    	} else {
    		$lang = 'ar';
    	}
    	if (array_key_exists($lang, Config::get('languages'))) {    		
            Session::put('applocale', $lang);
        }
        return redirect()->back();
    }

    /**
    * Method: checkLoggedin,
    * Description: This method is used to check user is logged in or not.
    * Author: Sanjoy
    */
    public function checkLoggedin() {
        $response = [
            'jsonrpc' => '2.0'
        ];

        if(Auth::id()) {
            $response['result']['loggedin'] = true;
        } else {
            $response['result']['loggedin'] = false;
        }
        return response()->json($response, 200);
    }

    public function updateOrderTable() {
        $orderDetail = OrderDetail::groupBy('id')->get();
        foreach ($orderDetail as $key => $value) {
            // OrderSeller::where(['order_master_id' => $value->order_master_id, 'seller_id' => $value->seller_id])->update(['driver_id' => $value->driver_id, 'status' => $value->status]);
            OrderSeller::where(['order_master_id' => $value->order_master_id, 'seller_id' => $value->seller_id])->update(['updated_at' => $value->updated_at]);
        }
    }
    /**
    * Method: sendNotificationTest
    * Description: This method is used to send push notification for test
    * Author: Argha
    * Date: 16-FEB-2021
    */
    public function sendNotificationTest(Request $request)
    {
        //dd($request->no);
        $no = $request->no;
        $count_start = ($no-1) * 900;

        $title_eng = "اسواقنا غير";
        $title_ar = "اسواقنا غير";
        $body_eng = "منتجات جديدة على التطبيق 🔥";
        $body_ar = "منتجات جديدة على التطبيق 🔥";
        
        $io = UserRegId::where('type', "I")
                                ->where('reg_id','!=',null)
                                ->skip($count_start)
                                ->take(900)
                                ->pluck('reg_id');
        
        //dd($io);
        $msg                      = array();
        $msg['title']             = $title_eng.'  '.$title_ar;
        
        $msg['soundname']         = 'pick';
        $msg['content-available'] = '1';
        $msg["info"]              = "";
        $msg["priority"]          = "2";
        $msg["volume"]            = "10";
        $msg["mutable-content"]   = true;
        $msg["body"]              = $body_eng.'  '.$body_ar;
        $msg["message"]           = $body_eng.'  '.$body_ar;
        $data['type'] = "1";
        $headers = array(
            'Authorization: key=' . env('FIREBASE_KEY'),
            'Content-Type: application/json',
        );
        $fields = array(
            'category'                  => "CustomSamplePush",
            'content_available'         => true,
            'mutable_content'           => true,
            'registration_ids'          => $io,
            'notification'              => $msg,
            'data'                      => $data,
            'priority'                  =>'high'
        );
        // Initializing Curl
        $ch = curl_init();
        // Posting data to the following URL
        curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

        // Post Data = True, Defining Headers and SSL Verifier = false
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Posting fields array in json format
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Executing Curl
        $result = curl_exec($ch);

        // dd($result);
        // Closing Curl
        curl_close($ch);

        dump($result);
        if($result){
            echo "Message sent";
        }
    }
    /**
    * Method: sendNotificatonCron
    * Description: This method is used to send push notification with cron
    * Author: Argha
    * Date: 16-FEB-2021
    */
    public function sendNotificatonCron()
    {
        $notDeta = NotificationData::where('status','P')
                                    ->orWhere('status','N')
                                    ->first();
        //dd($notDeta);
        if($notDeta){
            $total_ios_sended = $notDeta->total_ios_sended;
            $total_android_sended = $notDeta->total_android_sended;
            $total_ios = $notDeta->total_ios_reg_id;
            $total_android = $notDeta->total_android_reg_id;
            $title_eng = $notDeta->title_eng;
            $title_ar = $notDeta->title_ar;
            $body_eng = $notDeta->body_eng;
            $body_ar = $notDeta->body_ar;
            $count_start_ios = '';
            $count_start_android = '';
            $remain_ios = $total_ios - $total_ios_sended;
            $remain_android = $total_android - $total_android_sended;

            if($remain_ios < 0 || $remain_ios == 0){
                $count_start_ios = 0;
            }elseif($remain_ios < 900 && $remain_ios > 0){
                $count_start_ios = $remain_ios;
            }else{
                $count_start_ios = 900;
            }
            //dump($remain_ios,$remain_android);
            if($remain_android < 0 || $remain_android == 0){
                $count_start_android = 0;
            }elseif($remain_android < 900 && $remain_android > 0){
                $count_start_android = $remain_android;
            }else{
                $count_start_android = 900;
            }
            $ios = UserRegId::where('type', "I")
                                    ->where('reg_id','!=',null)
                                    ->skip($total_ios_sended)
                                    ->take($count_start_ios)
                                    ->pluck('reg_id');
            
            $android = UserRegId::where('type', "A")
                ->where('reg_id','!=',null)
                ->skip($total_android_sended)
                ->take($count_start_android)
                ->pluck('reg_id');
            //dump($ios,$android);

            if(count($ios) > 0){
                //dump($io);
                $msg                      = array();
                $msg['title']             = $title_eng.'  '.$title_ar;
                
                $msg['soundname']         = 'pick';
                $msg['content-available'] = '1';
                $msg["info"]              = "";
                $msg["priority"]          = "2";
                $msg["volume"]            = "10";
                $msg["mutable-content"]   = true;
                $msg["body"]              = $body_eng.'  '.$body_ar;
                $msg["message"]           = $body_eng.'  '.$body_ar;
                $data['type'] = "1";
                $headers = array(
                    'Authorization: key=' . env('FIREBASE_KEY'),
                    'Content-Type: application/json',
                );
                $fields = array(
                    'category'                  => "CustomSamplePush",
                    'content_available'         => true,
                    'mutable_content'           => true,
                    'registration_ids'          => $ios,
                    'notification'              => $msg,
                    'data'                      => $data,
                    'priority'                  =>'high'
                );
                // Initializing Curl
                $ch = curl_init();
                // Posting data to the following URL
                curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

                // Post Data = True, Defining Headers and SSL Verifier = false
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                // Posting fields array in json format
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                // Executing Curl
                $result = curl_exec($ch);

                // dd($result);
                // Closing Curl
                curl_close($ch);
            }

            if(count($android) > 0){
                //dump($an);
                
                $msg                      = array();
                $msg['title']             = $title_eng.'  '.$title_ar;
                $msg["body"]              = $body_eng.'  '.$body_ar;
                $msg["message"]           = $body_eng.'  '.$body_ar;
                $msg['link_type']         = "Y";
                $msg['is_linked']         = "Y";
                $msg['image']             = '';
                $msg['soundname']         = 'pick';
                $msg['content-available'] = '1';
                $msg["info"]              = "";
                $msg["priority"]          = "2";
                $msg["volume"]           = "10";
                $headers = array(
                    'Authorization: key=' . env('FIREBASE_KEY'),
                    'Content-Type: application/json',
                );
                $fields = array(
                    'registration_ids' => $android,
                    'data'             => $msg
                );
                // Initializing Curl
                $ch = curl_init();
                // Posting data to the following URL
                curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

                // Post Data = True, Defining Headers and SSL Verifier = false
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                // Posting fields array in json format
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                // Executing Curl
                $result = curl_exec($ch);
                // Closing Curl
                curl_close($ch);
            }
            $new_ios_sended = $total_ios_sended + $count_start_ios ;
            $new_android_sended = $total_android_sended + $count_start_android ;
            //dump($new_ios_sended,$new_android_sended);
            if(($remain_ios == 0) && ($remain_android == 0)){
                $update = [
                    'status' => 'D',
                    'total_ios_sended' => $new_ios_sended,
                    'total_android_sended' => $new_android_sended,
                ];
            }else{
                $update = [
                    'status' => 'P',
                    'total_ios_sended' => $new_ios_sended,
                    'total_android_sended' => $new_android_sended,
                ];
            }

            NotificationData::where('id',$notDeta->id)->update($update);
        }
        
    }

    public function merchantEarningAllDetails()
    {
       $merchant = Merchant::with(['getOrderSellerDetails.orderMasterTab','getWithdrawRecord'])
                              ->where('status','A')
                              ->orderBy('id','desc')
                              ->get();
        //dd($merchant[0]['getOrderSellerDetails'][0]);
        $data['merchant'] = $merchant;
        return view('modules.home.merchant_all_earning')->with($data);
    }

    // public function deleteDeletedRecord()
    // {
    //     //$order = 
    // }
}
