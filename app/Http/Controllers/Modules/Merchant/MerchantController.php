<?php

namespace App\Http\Controllers\Modules\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MerchantRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\CountryRepository;
use App\Models\City;
use App\Models\OrderMaster;
use Auth;
use Session;

class MerchantController extends Controller
{
	protected $merchant,$user,$product,$product_cat,$country;
	public function __construct(MerchantRepository $merchant,
		ProductRepository $product,ProductCategoryRepository $product_cat,
		CountryRepository $country,
		Request $request
	)
	{
		$this->merchant             =   $merchant;
		$this->product            	=   $product;
		$this->product_cat          =   $product_cat;
		$this->country            		= $country;
	}
	/**
	* Method: index
	* Description: This method is used to show merchant profile page
	* Author: Surajit
	*/
	public function index($slug) {
		$data['merchant'] 	= $this->merchant->with('merchantCityDetails')->where(['slug' => $slug])
		->where('status', 'A')->with('Country','merchantCompanyDetailsByLanguage','merchantImage')->first();

		$mer 	= $this->merchant->with('merchantCityDetails')->where(['slug' => $slug])
		->where('status', 'A')->with('Country','merchantCompanyDetailsByLanguage','merchantImage')->first();
		
		$orders = OrderMaster::with(['orderMasterDetails','customerDetails'])->where(['is_review_done'=>'Y']);
		$ordersss = $orders->whereHas('orderMasterDetails', function($q) use ($mer){
                    $q->where('seller_id',$mer->id);
                });
		$data['order'] =$ordersss->orderBy('id','desc')->get();
		//dd($data['merchant']);
        // $data['order'] = $data['order']->orderBy('id','desc')->get();
		if($data['merchant']) {
			$data['product'] = $this->product->select('id', 'slug', 'user_id', 'is_featured', 'price', 'discount_price','discount_price','from_date','to_date')->where(['user_id' => $data['merchant']->id])
			->with([
				'defaultImage:product_id,image', 
				'productByLanguage:language_id,product_id,title,description'
			])
			->with(['productMarchant','wishlist'])
			->where(['status' => 'A','seller_status' => 'A'])
			->take(50)
			->orderBy('id', 'DESC')
			->get();

			$data['total_products'] = $this->product->select('id', 'slug', 'user_id', 'is_featured', 'price','discount_price','from_date','to_date')
			->where(['user_id' => $data['merchant']->id])
			->where([
				'status' => 'A',
				'seller_status' => 'A'
			])
			->count();

			$product =  $data['product'];
			if(count($product) > 0) {
				foreach ($product as $pro) {
					$product_id[] = $pro->id;
				}
				$data['product_cat'] = $this->product_cat->whereIn('product_id', $product_id)->groupBy('category_id')->with('Category')->where('level', 'P')->get();
				// dd($data['product_cat']);
			} else {
				$data['product_cat'] = '';
			}
			//dd($product);
			return view('modules.merchant_profile.merchant_profile',$data);
		} else {
			return redirect()->back();
		}
	}
	/**
	* Method: sellerSearch
	* Description: This method is used to show seller list page
	* Author: Surajit
	*/
	public function sellerSearch(Request $request) {
		$data['country'] = $this->country->with('countryDetailsBylanguage')->where('status','!=','D')->get();
		$data['city'] 	= City::get();
		$merchant  = $this->merchant->where('status', 'A')->where('hide_merchant', 'N')->with('Country','merchantCompanyDetailsByLanguage','merchantImage','merchantCityDetails');

		if(@$request->all()){
            if(@$request->shipping_country){
                $merchant = $merchant->where('country',$request->shipping_country);
            }
            if(@$request->shipping_city_id){
                $merchant = $merchant->where('city',$request->shipping_city_id);
            }
        }
		$merchant  = $merchant->orderBy('fname', 'ASC')->orderBy('lname', 'ASC')->get();
		$data['merchant'] 	= $merchant;
		$data['key'] 	= @$request->all();

		return view('modules.merchant_profile.search_seller', $data);
	}
}
