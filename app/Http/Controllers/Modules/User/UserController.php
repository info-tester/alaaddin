<?php

namespace App\Http\Controllers\Modules\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Country;
use App\Models\UserFavorite;
use App\Models\UserAddressBook;
use App\Models\OrderMaster;
use App\Models\OrderDetail;
use App\User;
use App\Models\City;
use App\Mail\VerificationMail;
use App\Repositories\CityRepository;
use App\Repositories\CitiDetailsRepository;
use Auth;
use Session;
use Mail;

class UserController extends Controller
{
	public function __construct(CityRepository $city,
     CitiDetailsRepository $city_details)
	{
		$this->middleware('auth');
        $this->city                     =   $city;
        $this->city_details             =   $city_details;
	}

	/**
	* Method: viewDashboard
	* Description: This method is used to show dashboard page
	* Author: Surajit
	*/
	public function viewDashboard() {
        $data['order'] = OrderMaster::with([
            'orderMasterDetails',
            'orderMasterDetails.productDetails.productByLanguage',
            'orderMasterDetails.productDetails.defaultImage',
            'orderMasterDetails.productDetails.productVariants',
            'orderMasterDetails.productVariantDetails'
        ])
        ->where(['user_id' => @Auth::user()->id])
        ->whereNotIn('status', ['I', 'D'])
        ->orderBy('id','desc')
        ->first();
        $data['userDefAddr'] = UserAddressBook::where(['user_id' => @Auth::user()->id, 'is_default' => 'Y'])->with('getCountry')->first();
		return view('modules.dashboard.dashboard', $data);
	}

	/**
	* Method: editProfile
	* Description: This method is used to show edit profile page
	* Author: Surajit
	*/
	public function editProfile() {
		$data['country'] = Country::with('countryDetailsBylanguage')->where('status','!=','D')->get();
        $data['customer'] = User::where('id',@Auth::user()->id)->with('userCityDetails')->first();
		return view('modules.dashboard.edit_profile', $data);
	}

    public function checkCity(Request $request){
        $response = array(
            'jsonrpc' => '2.0'
        );
        
        if($request->get('city')) {
            $city = $request->get('city');
            $data = $this->city_details->where('name', $city)->first();
            if($data) {
                $response['status'] = 'SUCCESS';
                return response()->json($response);
            } else {
                $response['status'] = 'ERROR';    
                return response()->json($response);     
            }
        } else {
            $response['status'] = 'ERROR';    
            return response()->json($response);     
        }
    }

    public function fetchAllCityDetails(Request $request){
        $response = array(
            'jsonrpc' => '2.0'
        );
        if($request->get('city')) {
            $citiesss = $this->city->with('cityDetailsByLanguage');
            $citiess = $citiesss->where(function($q1) use($request) {
                $q1 = $q1->where('status', '!=', 'I')
                ->orWhere('status', '!=', 'D');
            });
            $cities = $citiess->whereHas('cityDetailsByLanguage',function($query) use($request) {
                $query->where('name', 'LIKE', "%{$request->city}%");
            })->get();
        }
        $response['result']['cities'] = $cities;
        return response()->json($response);     
    }

	/**
	* Method: storeUserDetails
	* Description: This method is used to store user details into users table
	* Author: Surajit
	*/
	public function storeUserDetails(Request $request) {
        if(@$request->password || @$request->new_password || @$request->confirm_password) {
            $validator = $request->validate([
                'fname'        => 'required',
                'lname'        => 'required',
                'email'        => 'required',
                'phone'        => 'required',
                // 'password'     => 'required',
                'new_password' => 'required',
                'confirm_password'=> 'required',
            ]);
        }else{
            $validator = $request->validate([
                'fname'        => 'required',
                'lname'        => 'required',
                'email'        => 'required',
                'phone'        => 'required'
            ]); 
        }
		if(@$validator) {
			$user['fname']     = $request->fname;
			$user['lname']     = $request->lname;
			$user['phone']     = $request->phone;
			$user['address']   = $request->address;
			$user['state']     = $request->state;
			$user['zipcode']   = $request->zipcode;
			$user['country']   = $request->country;
            if(@$request->country == 134){
                $user['city']      = $request->kuwait_city_value_name;
                $user['city_id']   = $request->kuwait_city_value_id;
            } else {
                $user['city']      = $request->city;
            }
			if(@$request->image) {
				$image     = $request->image; 
				$imgName   = time().".".$image->getClientOriginalExtension();
				$image->move('storage/app/public/customer/profile_pics', $imgName);
				$user['image']     =   $imgName;
			}
			User::whereId(@Auth::user()->id)->update($user);
			if(@$request->email != @Auth::user()->email){
				$user['tmp_email']   = $request->email;
				$user['email_vcode'] = time();
				User::whereId(@Auth::user()->id)->update($user);
				$user['id']       = @Auth::user()->id;
				$user['email']    = $request->email;
				$user['mailBody'] = 'updated';
				$user['customer'] = 'customer';
				$this->sendVerificationMail($user);
			}

            // Change user password.
			if(@$request->new_password) {
				$this->changePassword($request);
			} else {
				session()->flash("success",__('success_user.-700'));
			}
		}
		return redirect()->back();
	}

    /**
    * Method: reSendVerificationMail
    * Desc: This method is used for resend verification mail to user
    */
    public function reSendVerificationMail()  {
    	$user['fname']     		=   @Auth::user()->fname;
    	$user['lname']     		=   @Auth::user()->lname;
    	$user['id']       		=   @Auth::user()->id;
    	$user['email_vcode'] 	=   @Auth::user()->email_vcode;
    	$user['email']    		=   @Auth::user()->tmp_email;
    	$user['mailBody'] 		= 	'updated';
    	$user['customer'] 		= 	'customer';
    	$this->sendVerificationMail($user);
    	session()->flash("success",__('success_user.-707'));
    	return redirect()->back();
    }

    /**
    * Method: sendVerificationMail
    * Desc: This method is used for send verification mail to user
    */
    public function sendVerificationMail($reqData)  {        
    	Mail::send(new VerificationMail($reqData));
    }

    /**
    * Method: changePassword
    * Description: This method is used to change user password
    * Author: Surajit 
    */
    private function changePassword($request) {
        if($request->new_password){
            if(Auth::user()->password == ''){
                $user_detail = Auth::user();
                $user_detail->password=\Hash::make($request->confirm_password);
                $user_detail->save();
                session()->flash("success",__('success_user.-700'));
            }else{
                if(\Hash::check($request->password, Auth::user()->password)){
                    $user_detail = Auth::user();
                    $user_detail->password=\Hash::make($request->confirm_password);
                    $user_detail->save();
                    session()->flash("success",__('success_user.-700'));
                }
                else{
                    session()->flash("error",__('success_user.-701'));
                }
            }
        }
    }

    /**
    * Checking new email exist or not in edit profile
    */
    public function chkNewEmailExist(Request $request) {
    	$response = [
    		'jsonrpc' => '2.0'
    	];
    	$user = User::where(['email' => trim($request->params['email'])])->where('id', '!=', @Auth::user()->id)->where('user_type', '!=', 'G')->first();
    	if(@$user) {
    		$response['error']['user'] = 'This email id already exists.';
    	} else {
    		$response['result']['status']  = false;
    		$response['result']['message'] = 'This email is available.';
    	}
    	return response()->json($response);
    }

	/**
	* Method: myWishlist
	* Description: This method is used to show my wishlist page
	* Author: Surajit
	*/
	public function myWishlist() {
		$fav = UserFavorite::where('user_id', @Auth::user()->id)->get();
		foreach ($fav as $val) {
			$favVal[] = $val->product_id;
		}
		$data['favProduct'] = UserFavorite::where('user_id', @Auth::user()->id)
		->with(
			'getProduct',
			'getProduct.defaultImage:product_id,image',
			'getProduct.productByLanguage:language_id,product_id,title,description',
			'getProduct.productMarchant'
		)
		->get();
		return view('modules.dashboard.my_wishlist', $data);
	}

    /*
    * Method: removeWishlist
    * Description: to remove address from user_favourites
    * Author: Surajit 
    */
    public function removeWishlist($id){

    	$userid = Auth::user()->id;
    	$status = '';
    	$response = [
    		'jsonrpc'   => '2.0'
    	];        

    	$data =	UserFavorite::whereId($id)->where('user_id', @Auth::user()->id)->first();	 
    	if(@$data) {
    		UserFavorite::whereId($id)->delete();
    		$status = 'Remove';
    	}

    	if($status == '') {
    		$response['error']['message'] = 'Fail';
    	} else {
    		$response['sucess']['result'] = $status;
    		if($status == 'Remove') {
    			$response['sucess']['message'] = __('front_static.wishlist_success');
    		}
    	}
    	return $response;
    }
}
