<?php

namespace App\Http\Controllers\Modules\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

#repository
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\UserRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductRepository;

#models
use App\Models\Payment;
use App\Models\Setting;
use App\Models\UserReward;
use App\Models\OrderMaster;
use App\Models\OrderDetail;
use App\Models\OrderSeller;
use App\Models\MerchantRegId;
use App\Driver;
use App\User;
#mail
use App\Mail\OrderMail;
use App\Mail\SendOrderDetailsMerchantMail;
use Mail;

use Auth;
use Session;

# use whatsapp controller
use App\Http\Controllers\Modules\WhatsApp\WhatsAppController;

class PaymentController extends Controller
{
	protected $orderMaster, $orderDetails, $orderSeller, $user, $merchant, $cart, $cartDetails, $productVariant, $product;

    #whatsapp send message
    protected $whatsApp;

	public function __construct(OrderMasterRepository $orderMaster, 
		OrderDetailRepository $orderDetails,
		OrderSellerRepository $orderSeller,
		UserRepository $user,
		MerchantRepository $merchant,
		CartMasterRepository $cart,
		CartDetailRepository $cartDetails,
        ProductVariantRepository $productVariant,
        ProductRepository $product,
        WhatsAppController $whatsApp
							) {
		$this->orderMaster 	= $orderMaster;
		$this->orderDetails = $orderDetails;
		$this->orderSeller 	= $orderSeller;
		$this->user 		= $user;
		$this->merchant 	= $merchant;
		$this->cart 		= $cart;
		$this->cartDetails 	= $cartDetails;
        $this->productVariant  = $productVariant;
        $this->product      = $product;
        $this->whatsApp     = $whatsApp;
	}

    /**
    * Method: init
    * Description: This method is used to init payment.
    * Author: Sanjoy Mandal
    */
    public function init(Request $request) {
    	try{
    		$data  = [];
	    	$data['orderId'] = $request->track;
	    	$orderId = decrypt($request->track);
	    	$data['order'] = $this->orderMaster->where(['id' => $orderId, 'status' => 'I'])->first();
	    	if(!@$data['order'] || $data['order']->status == 'P' || $data['order']->status == 'C' || $data['order']->status == 'F') {
	    		abort('401');
	    	}
            # initiate payment
            $initPayment = $this->initPayment($data['order']);
            $data['payment_methods'] = @$initPayment->Data->PaymentMethods;
	    	return view('modules.payment.init_payment')->with($data);
    	} catch(\Exception $e) {
    		abort('401');
    	}
    }

    /**
    * Method: initPayment
    * Description: This method is used to init the payment
    * Author: Sanjoy
    */
    private function initPayment($order) {
        $basURL = env('PAYMENT_ENDPOINT');
        $token = env('PAYMENT_TOKEN');

        $curl = curl_init();
        $params = [
            'InvoiceAmount' => $order->order_total,
            'CurrencyIso' => 'KWD'
        ];
        curl_setopt_array($curl, array(
            CURLOPT_URL => "$basURL/v2/InitiatePayment",
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => array("Authorization: Bearer $token","Content-Type: application/json"),
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
        }

        return json_decode($response);
    }


    /**
    * Method: submitPayment
    * Description: This method is used to 
    * Author: Sanjoy Mandal
    */
    public function submitPayment(Request $request) {
    	$response = [
    		'jsonrpc' => '2.0'
    	];
    	// try {
    		$reqData = $request->params;
	    	$orderId = decrypt($reqData['order_id']);
	    	$order = $this->orderMaster
			->with([
				'orderMasterDetails.productByLanguage',
				'customerDetails',
                'getBillingCountryCode',
                'orderMerchants.orderSeller'
			])
			->where(['id' => $orderId])
			->first();

            $paymentDataTbl = Payment::where(['order_id' => $orderId])->first();
            
	    	// if successfully created invoice.
    		$params1 = [
				'paymentType' 	=> 'card',
				'card'			=> [
					'Number'		=> $reqData['number'],
					'expiryMonth'	=> $reqData['expiration_month'],
					'expiryYear'	=> substr($reqData['expiration_year'], 2),
					'securityCode'	=> $reqData['security_code']
				],
				'saveToken'	=> false
			];

			$invoiceData = json_decode($paymentDataTbl->invoice_data);
            
            if(!@$invoiceData->Data->PaymentURL) {
                $response['error']['status'] = 'failed';
                return response()->json($response);
            }
    		$paymentData = $this->directPayment($params1, $invoiceData->Data->PaymentURL);

    		# if payment return success
    		if(@$paymentData->IsSuccess) {
                $sendEmail = 0;
                
                $sendWhatsApp = 0;
                foreach ($order->orderMerchants as $key => $value) {
                    if($value->orderSeller->notify_customer_by_email_internal == 'Y') {
                        $sendEmail += 1; 
                    }
                    if($value->orderSeller->notify_customer_by_whatsapp_internal == 'Y') {
                        $sendWhatsApp += 1;
                    }
                }

    			# update to payment table
    			$params = [
    				'response' 	=> json_encode($paymentData),
    				'txn_id'	=> $paymentData->Data->PaymentId,
    				'status'	=> 'S'
    			];
    			$this->updatePayment($params, $orderId);
    			
    			# updating order status I to N or DA
    			$this->updateOrderStatus($orderId);
    			
    			# deduct loyalty point if customer registered
	            if(@Auth::user()) {
	                $this->deductLoyaltyPoint($orderId);
	            }

                # drcrease stock(quantity) when order placed
                $this->decreaseStock($orderId);

                # send whatsapp message to merchant
                $this->whatsApp->sendMessageMerchants($orderId);
                
                # send mail to customer and seller
                $this->sendCustomerAndSellerMail($orderId);

    			$response['result']['status'] = 'success';
    		} else {
    			$params = [
    				'response' 	=> json_encode($paymentData),
                    'txn_id'    => $paymentData->Data->PaymentId,
    				'status'	=> 'F',
    			];
    			$this->updatePayment($params, $orderId);
    			$this->orderMaster->where(['id' => $orderId])->update(['status' => 'F']);
    			$response['error']['message'] = $paymentData->Data->ErrorMessage;
    			$response['error']['ref_no'] = $paymentData->Data->PaymentId;

                $response['error']['status'] = 'failed';
    		}
    		$this->clearCart();
	    	return response()->json($response);
    	/*} catch(\Exception $e) {
    		return $e->getMessage();
    	}*/
    }

    /*
    * Method: sendNotification
    * Description: This method is used to send Notification to driver
    * Author: Jayatri
    */
    private function sendNotification($order_id){
        $androidDrivers = Driver::where(['status' => 'A', 'device_type' => 'A'])->pluck('firebase_reg_no');
        $iosDrivers = Driver::where(['status' => 'A', 'device_type' => 'I'])->pluck('firebase_reg_no');

        $order_dtt = OrderMaster::where('id',$order_id)->first();
        # send notification to android drivers
        if(count($androidDrivers)) {
            $msg                      = array();
            $msg['title']             = 'Order No:'." ".$order_dtt->order_no;
            $msg["body"]              = $order_dtt->shipping_fname." placed order recently!";
            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            $msg['link_type']         = "Y";
            $msg['is_linked']         = "Y";
            $msg['image']             = '';
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]           = "10";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'registration_ids' => $androidDrivers,
                'data'             => $msg
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            // Closing Curl
            curl_close($ch);
        }
        
        # send notification to ios drivers
        if(count($iosDrivers)) {
            $msg                      = array();
            $msg['title']             = "New Order Placed";
            $msg['soundname']         = 'pick';
            $msg['content-available'] = '1';
            $msg["info"]              = "";
            $msg["priority"]          = "2";
            $msg["volume"]            = "10";
            $msg["mutable-content"]   = true;
            // $msg["body"]              = $order_dtt->shipping_fname." ".$order_dtt->shipping_lname.' placed an order! Order number : '.$order_dtt->order_no;
            $msg["body"]              = 'Recently customer placed an order! ';

            $data['message'] = "New Order Placed";
            $data['type'] = "1";
            $headers = array(
                'Authorization: key=' . env('FIREBASE_KEY'),
                'Content-Type: application/json',
            );
            $fields = array(
                'category'                  => "CustomSamplePush",
                'content_available'         => true,
                'mutable_content'           => true,
                'registration_ids'          => $iosDrivers,
                'notification'              => $msg,
                'data'                      => $data
            );
            // Initializing Curl
            $ch = curl_init();
            // Posting data to the following URL
            curl_setopt($ch, CURLOPT_URL, env('FIREBASE_URL'));

            // Post Data = True, Defining Headers and SSL Verifier = false
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Posting fields array in json format
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Executing Curl
            $result = curl_exec($ch);
            // Closing Curl
            curl_close($ch);
        }
    }

    /*
    * Method: decreaseStock
    * Description: This method is used to descrease Stock
    * Author: Jayatri
    */
    private function decreaseStock($orderId) {   
        $details = $this->orderDetails->where('order_master_id',@$orderId)->get();
        
        foreach($details as $dtl){
            if($dtl->product_variant_id != 0){
                $variant = $this->productVariant->where('id',$dtl->product_variant_id)->first();
                if($variant->stock_quantity >= $dtl->quantity){
                    $updatestock['stock_quantity'] = $variant->stock_quantity - $dtl->quantity;
                    $this->productVariant->where('id',$dtl->product_variant_id)->update($updatestock);
                }else{
                    // return 0;
                }
            }else{
                if($dtl->product_id != 0){
                    $product = $this->product->where('id',@$dtl->product_id)->first();    
                    if($product->stock >= $dtl->quantity){
                        $stock_update['stock'] = $product->stock - $dtl->quantity;
                        $this->product->where('id',@$dtl->product_id)->update($stock_update);
                    } else {
                        // return 0;
                    }
                } else {
                    // return 0;
                }
            }   
        }
    }

    /**
    * Method: updateOrderStatus
    * Description: This method is used to change status of order_master and order_details
    * Author: Sanjoy
    */
    private function updateOrderStatus($orderId) {
        $ordDetl = $this->orderDetails->where(['order_master_id' => $orderId])->get()->pluck('seller_id');
        $sellerDriver = $this->merchant->select('id','driver_id')->whereIn('id', $ordDetl)->get();
        foreach ($sellerDriver as $sd) {
            if($sd->driver_id != '0') {
                $assignDriver['driver_id']    =   $sd->driver_id;    
                $assignDriver['status']       =   'DA';    
                $this->orderDetails->where(['seller_id' => $sd->id,'order_master_id' => $orderId])->update($assignDriver);
                $this->orderSeller->where(['seller_id' => $sd->id,'order_master_id' => $orderId])->update($assignDriver);
            }
        }
        $drvAssndChck = $this->orderDetails->where(['order_master_id' => $orderId, 'driver_id' => '0'])->count();
        if($drvAssndChck > 0) {
            $order['status']    = 'N';   
        } else {
            $order['status']    = 'DA';
        }
        $order['is_paid'] = 'Y';
        $order['last_status_date'] = date('Y-m-d H:i:s');
        $this->orderMaster->where(['id' => $orderId])->update($order);
    }

    /**
    * Method: sendCustomerAndSellerMail (Internal order)
    * Description: This method is used to send order mail to customer and seller if payment method is COD
    * Author: Sanjoy/Jayatri(changes)
    */
    private function sendCustomerAndSellerMail($orderId) {
        $data = [];
        $data['order'] = $order = OrderMaster::where('id', $orderId)->with('shippingAddress.getCountry','billingAddress.getCountry','getCountry')->first();
        $data['orderDetails'] = OrderDetail::where(['order_master_id' => $orderId])->with('sellerDetails','defaultImage','productByLanguage','productVariantDetails')->get();
        $contactList = [];
        $i=0;
        
        $user = User::whereId($order->user_id)->first();
        $data['fname'] = $user->fname;
        $data['lname'] = $user->lname;  
        $data['email'] = $user->email;
        $data['user_type'] = $user->user_type;

        $data['shipping_price'] = @$order->shipping_price;
        $data['orderNo'] = $order->order_no;
        $data['language_id'] = getLanguage()->id;
        $contactList[$i] = @$order->shipping_email;
        
        $data['sub_total'] = @$order->subtotal;
        $data['order_total'] = @$order->order_total;
        $data['total_discount'] = $order->total_discount;
        if(@$order->status == 'N'){
            $data['subject'] = "Your order has been placed successfully! Order no: ".@$order->order_no;
        }else if(@$order->status == 'DA'){
            $data['subject'] = "Your order is accepted! Order No:".@$order->order_no." ! Order Details!";
        }
        else if(@$order->status == 'RP'){
            $data['subject'] = "Your order is ready for pick up! Order No:".@$order->order_no." ! Order Details!";
        }
        else if(@$order->status == 'OP'){
            $data['subject'] = "Your order is picked up! Order No:".@$order->order_no." ! Order Details!";
        }
        else if(@$order->status == 'OD'){
            $data['subject'] = "Your order is delivered successfully! Order No:".@$order->order_no." ! Order Details!";
        }

        #send mail admin to notify for new order only as client requirement (internal order)
        $i=0;
        if(@$order->status =='N' ||@$order->status =='DA'){
            $setting = Setting::first();
            if(@$setting->email_1) {
                $contactList[$i] = $setting->email_1;
                $i++;
            }
            if(@$setting->email_2) {
                $contactList[$i] = $setting->email_2;
                $i++;
            }
            if(@$setting->email_3) {
                $contactList[$i] = $setting->email_3;
                $i++;
            }
        }
        $contactList = array_unique($contactList);
        $data['bcc'] = $contactList;

        #send to customer mail(order status can be new/driver assigned)
        Mail::queue(new OrderMail($data));
        
        # send mail order details to seller mail
        $contactList = [];
        $data['bcc'] = [];
        $dtls_mail = OrderDetail::where(['order_master_id' => $order->id])->groupBy('seller_id')->get();
        
        foreach(@$dtls_mail as $contact){
            $contactListEmail1 = [];
            $contactListEmail2 = [];
            $contactListEmail3 = [];
            $contactList = [];
            $data['bcc'] = [];
            $i = 0;
            //start
            $data['user_type'] = 'C';
            $data['hide_customer_info'] = $contact->sellerDetails->hide_customer_info;
            $data['orderDetails'] = OrderDetail::where(['order_master_id' => $order->id,'seller_id'=>@$contact->seller_id])
            ->with('sellerDetails','defaultImage','productByLanguage','productVariantDetails')
            ->get();
            $data['sub_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
            $data['order_total'] =  OrderDetail::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
            $data['total_discount'] = OrderSeller::where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total_discount');
            $data['shipping_price'] = "NA";
            if(@$order->status == 'N'){
                $data['subject'] = "New Order Placed!Order No : ".@$order->order_no." ! Order Details!";
            }else if(@$order->status =='DA'){
                $data['subject'] = "Order is Accepted! Order No : ".@$order->order_no." ! Order Details!";    
            }else if(@$order->status =='RP'){
                $data['subject'] = "Order is is ready for pickup! Order No : ".@$order->order_no." ! Order Details!";    
            }else if(@$order->status =='OP'){
                $data['subject'] = "Order is picked up! Order No : ".@$order->order_no." ! Order Details!";    
            }else if(@$order->status =='OD'){
                $data['subject'] = "Order is Delivered! Order No : ".@$order->order_no." ! Order Details!";    
            }
            
            // Fill the Bcc email address
            $contactList[$i] = $contact->sellerDetails->email;
            $i++;
            #send mail to merchant if status driver assigned(client requirement)
            if(@$order->status =='DA'){
                if($contact->sellerDetails->email1) {
                    $contactListEmail1[$i] = $contact->sellerDetails->email1;
                    $i++;
                }
                if($contact->sellerDetails->email2) {
                    $contactListEmail2[$i] = $contact->sellerDetails->email2;
                    $i++;
                }
                if($contact->sellerDetails->email3) {
                    $contactListEmail3[$i] = $contact->sellerDetails->email3;
                    $i++;
                }    
            }
            $contactList = array_unique($contactList);
            if(@$contactListEmail1 != '') {
                $contactListEmail1 = array_unique($contactListEmail1);
                $contactList = array_merge($contactList, $contactListEmail1);
            }
            if(@$contactListEmail2 != '') {
                $contactListEmail2 = array_unique($contactListEmail2);
                $contactList = array_merge($contactList, $contactListEmail2);
            }
            if(@$contactListEmail3 != '') {
                $contactListEmail3 = array_unique($contactListEmail3);
                $contactList = array_merge($contactList, $contactListEmail3);
            }
            $data['bcc'] = $contactList;
            Mail::queue(new SendOrderDetailsMerchantMail($data));    
            $this->sendNotificationDashboardUpdate($contact->order_master_id, $contact->seller_id);
        }
    }

    /*
    * Method: sendNotificationDashboardUpdate 
    * Description : This method is used to update dashboard automatically specialy merchant dashboard client requirement using web notification (ajax call)
    * Author: Jayatri
    */
    private function sendNotificationDashboardUpdate($orderId, $merchantId) {
        // For Notification sending
        $order_dtt = OrderMaster::where('id',$orderId)->first();
        $new_registrationIds = MerchantRegId::where(['user_id' => $merchantId])->pluck('reg_id')->toArray();
        $idCount = MerchantRegId::where(['user_id' => $merchantId])->pluck('reg_id')->Count();
        if($idCount != 0){
            if(@$order_dtt) {
            $username = "Testing ...";
            $click_action = route('merchant.dashboard');
            $title = $username." Recently customer placed an order!";
            $message = "New order Placed !";
            // dd($new_registrationIds);
            $fields = array (
                'registration_ids' => $new_registrationIds,
                'data' => array (
                    "message"       => $message,
                    "title"         => $title,
                    "image"         => url('firebase-logo.png'),
                    "click_action"  => $click_action,
                ),
                'notification'      => array (
                    "body"          => $message,
                    "title"         => $title,
                    "click_action"  => $click_action,
                    "icon"          => "url('firebase-logo.png')",
                )
            );
            $fields = json_encode ( $fields );
            $API_ACCESS_KEY = 'AAAAcQGyZzs:APA91bEst24UvPwO2UpmnRh6OUH_Y_On82ZRIm1ekOSKcekCuOc1m2yYR2w6CGDt09nBY8dmL5h_8C5Cs_KMgIPd1m0mZ8oMUDWz75KaDJmJeNOkTFklIS-hYgWYA3qHnbO-zb76jNK-';
            $headers = array(
                'Authorization: key=' . $API_ACCESS_KEY,
                'Content-Type: application/json',
            );
            $ch = curl_init ();
            curl_setopt ( $ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt ( $ch, CURLOPT_POST, true );
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
            $result = curl_exec ( $ch );
            curl_close ( $ch );
            return $result.' New Order placed! ';
            }
        }
        // End of Notification sending
    }
    /**
    * Method: updatePayment
    * Description: This method is used to update payment table after made payment success or failed.
    * Author: Sanjoy Mandal
    */
    public function updatePayment($params = [], $orderId) {
    	Payment::where(['order_id' => $orderId])->update($params);
    }

    /**
    * Method: calculateLoyaltyPoint
    * Description: This method is used to deduct loyalty point from customer account.
    * Author: Sanjoy
    */
    private function deductLoyaltyPoint($orderId) {
        $setting = Setting::first();
        $order = OrderMaster::where(['id' => $orderId])->first();
        if(@$order->loyalty_point_used > 0) {  
            $totalOrder = $order->loyalty_point_used*$setting->one_point_to_kwd;
            
            # insert to user reward table
            $addUserReward = new UserReward;
            $addUserReward['user_id']        = @Auth::user()->id;
            $addUserReward['order_id']       = $orderId;
            $addUserReward['earning_points'] = $order->loyalty_point_used;
            $addUserReward['debit']          = '1';
            $addUserReward->save();

            # update customer total reward
            $this->user->whereId(@Auth::user()->id)->decrement('loyalty_balance', $order->loyalty_point_used);
            $this->user->whereId(@Auth::user()->id)->increment('loyalty_used', $order->loyalty_point_used);
        }
    }

    /**
    * Method: createInvoice
    * Description: This method is used to create invoice on payment gateway.
    * Author: Sanjoy
    */
    public function createInvoice(Request $request) {
        $reqData = $request->params;
        $order = OrderMaster::where(['id' => decrypt($reqData['order_id'])])->first();
        $setting = Setting::first();
        $loyaltyPoint = $order->order_total*$setting->one_kwd_to_point;
        $totalOrder = $loyaltyPoint*$setting->one_point_to_kwd;

        $params = [
            'PaymentMethodId'   => $reqData['payment_method_id'],
            'CustomerName'      => $order->customerDetails->fname.' '.$order->customerDetails->lname,
            'DisplayCurrencyIso'=> 'KWD',
            'MobileCountryCode' => $order->getBillingCountryCode->country_code,
            'CustomerMobile'    => $order->customerDetails->billing_phone,
            'CustomerEmail'     => $order->billing_email,
            'InvoiceValue'      => $order->order_total,
            'CallBackUrl'       => env('APP_ENV') == 'local'? 'http://www.example.com' : route('payment.notify', $order->id),
            'ErrorUrl'          => env('APP_ENV') == 'local'? 'http://www.example.com' : route('payment.error', $order->id),
            'Language'          => 'en',
            'CustomerReference' => $order->customerDetails->id,
            'UserDefinedField'  => $order->order_no,
            'CustomerAddress'   => [
                'Block'             => @$order->billing_block,
                'Street'            => @$order->billing_street,
                'HouseBuildingNo'   => @$order->billing_building_no,
                'Address'           => @$order->billing_more_address,
                'AddressInstructions' => ''
            ]
        ];
        foreach ($order->orderMasterDetails as $key => $value) {
            $params['InvoiceItems'][] = [
                'ItemName'  => $value->productByLanguage->title,
                'Quantity'  => $value->quantity,
                'UnitPrice' => $value->total /$value->quantity
            ];
        }
        $params['InvoiceItems'][] = [
            'ItemName'  => 'Shipping price',
            'Quantity'  => 1,
            'UnitPrice' => $order->shipping_price
        ];
        $params['InvoiceItems'][] = [
            'ItemName'  => 'Coupon Discount',
            'Quantity'  => 1,
            'UnitPrice' => -$order->coupon_discount
        ];
        if($order->loyalty_amount > 0) {
            $params['InvoiceItems'][] = [
                'ItemName'  => 'Deduct reward',
                'Quantity'  => 1,
                'UnitPrice' => -$order->loyalty_amount
            ];
        }
        $execPayment = $this->executePayment($params);
        // upddate payment table invoice data.
        # update to payment table
        $params = [
            'invoice_data'  => json_encode($execPayment),
            'invoice_id'    => @$execPayment->Data->InvoiceId
        ];
        $this->updatePayment($params, $order->id);
        return response()->json($execPayment);
    }

    /**
    * Method: executePayment
    * Description: This method is used to executePayment
    * Author: Sanjoy
    */
    public function executePayment($params = []) {
    	$basURL = env('PAYMENT_ENDPOINT');
    	$token = env('PAYMENT_TOKEN');

    	$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "$basURL/v2/ExecutePayment",
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($params),
			CURLOPT_HTTPHEADER => array("Authorization: Bearer $token","Content-Type: application/json"),
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  	echo "cURL Error #:" . $err;
		}

		return json_decode($response);
    }

    /**
    * Method: directPayment
    * Description: This method is used to direct payment.
    * Author: Sanjoy
    */
    private function directPayment($params = [], $url) {
    	$token = env('PAYMENT_TOKEN');
    	$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($params),
			CURLOPT_HTTPHEADER => array("Authorization: Bearer $token","Content-Type: application/json"),
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		echo "cURL Error #:" . $err;
		} else {
		// echo "$response '<br />'";
		}
		return json_decode($response);
    }

    /**
    * Method: notifyPayment
    * Description: This method is used to update data after getting response from payment gateway.
    * Author: Sanjoy
    */
    public function notifyPayment(Request $request, $orderId) {
        $payment = $this->getPaymentStatus(['Key' => $request->paymentId, 'KeyType' => 'PaymentId']);
        // dd($payment);
        if(@$payment->Data->InvoiceStatus == 'Paid') {
            $order = $this->orderMaster
            ->with([
                'orderMasterDetails.productByLanguage',
                'customerDetails',
                'getBillingCountryCode',
                'orderMerchants.orderSeller'
            ])
            ->where(['id' => $orderId])
            ->first();

            $sendEmail = 0;
            $sendWhatsApp = 0;
            foreach ($order->orderMerchants as $key => $value) {
                if($value->orderSeller->notify_customer_by_email_internal == 'Y') {
                    $sendEmail += 1; 
                }
                if($value->orderSeller->notify_customer_by_whatsapp_internal == 'Y') {
                    $sendWhatsApp += 1;
                }
            }

            # update to payment table
            $params = [
                'response'  => json_encode($payment),
                'txn_id'    => $payment->Data->InvoiceTransactions[0]->PaymentId,
                'status'    => 'S'
            ];
            $this->updatePayment($params, $orderId);
            
            # updating order status I to N or DA
            $this->updateOrderStatus($orderId);
            
            # deduct loyalty point if customer registered
            if(@Auth::user()) {
                $this->deductLoyaltyPoint($orderId);
            }

            #drcrease stock(quantity) when order placed
            $this->decreaseStock($orderId);

            // if(@$sendWhatsApp > 0) {
            //     # send whatsapp message to customer
            //     $this->whatsApp->sendMessageMerchants($orderId);
            // }
            # send mail to customer and seller
            $this->sendCustomerAndSellerMail($orderId);
            
            # sending whatsapp message
            $this->whatsApp->sendMessageMerchants($orderId);
            
            # send push notificatio to all drivers
            $this->sendNotification($orderId);
            $this->clearCart();
            return redirect()->route('order.success', ['trackId' => encrypt($orderId)]);
        } else {
            $params = [
                'response'  => json_encode($payment),
                'txn_id'    => $payment->Data->InvoiceTransactions[0]->PaymentId,
                'status'    => 'F'
            ];
            Payment::where(['order_id' => $orderId])->update($params);
            return redirect()->route('payment.failed', encrypt($orderId));
        }
    }

    /**
    * Method: errorPayment
    * Description: This method is used to update data after getting response from payment gateway.
    * Author: Sanjoy
    */
    public function errorPayment(Request $request, $orderId) {
        $payment = $this->getPaymentStatus(['Key' => $request->paymentId, 'KeyType' => 'PaymentId']);
        
        $params = [
            'response'  => json_encode($payment),
            'txn_id'    => $payment->Data->InvoiceTransactions[0]->PaymentId,
            'status'    => 'F'
        ];
        $this->updatePayment($params, $orderId);
        return redirect()->route('payment.failed', encrypt($orderId));
    }

    /**
    * Method: getPaymentStatus
    * Description: This method is used to update data after getting response from payment gateway.
    * Author: Sanjoy
    */
    private function getPaymentStatus($params = []) {
        // dd(json_encode($params));
        $basURL = env('PAYMENT_ENDPOINT');
        $token = env('PAYMENT_TOKEN');
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "$basURL/v2/GetPaymentStatus",
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => array("Authorization: Bearer $token","Content-Type: application/json"),
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
        // echo "$response '<br />'";
        }
        return json_decode($response);
    }

    

    /**
    * Method: paymentFailed
    * Description: This method is used to show payment failed page.
    * Author: Sanjoy
    */
    public function paymentFailed($trackId) {
        $orderId = decrypt($trackId);
        $paymentData = Payment::where(['order_id' => $orderId])->first();

        $this->orderMaster->where(['id' => $orderId])->update(['status' => 'F']);        
        return view('modules.payment.payment_failed')->with(['ref_id' => $paymentData->txn_id]);
    }

    /**
    * Method: clearCart
    * Description: This method is used to clear cart
    * Author: Sanjoy
    */
    private function clearCart() {
        if(@Auth::user()) {
            $cart = $this->cart->where('user_id', Auth::user()->id)->first();
            if($cart) {
                $this->cartDetails->where('cart_master_id', $cart->id)->delete(); 
                $this->cart->where('id', $cart->id)->delete();
            }
        } else {
            $cart = $this->cart->where('session_id', session()->getId())->first();
            if($cart) {
                $this->cartDetails->where('cart_master_id', $cart->id)->delete(); 
                $this->cart->where('id', $cart->id)->delete();
            }
        }
    }
}
