<?php

namespace App\Http\Controllers\Modules\SendMailOrder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// repository
use App\Repositories\CartMasterRepository;
use App\Repositories\CartDetailRepository;
use App\Repositories\OrderMasterRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderSellerRepository;
use App\Repositories\ProductVariantRepository;
use App\Repositories\ProductVariantDetailRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CountryRepository;
use App\Repositories\UserRepository;
use App\Repositories\MerchantRepository;

use App\Models\UserReward;
use App\Models\OrderMaster;
use App\Models\Setting;
use App\Models\Payment;
use App\Mail\OrderMail;
// use App\Mail\Setting;
use App\Driver;
use App\User;
use App\Mail\SendOrderDetailsMerchantMail;
use Mail;
use Auth;
use Session;

class SendMailOrderController extends Controller
{
    //
    protected $user, $merchant, $cart, $cartDetails, $order, $orderDetails, $product, $productVariant, $productVariantDetail,$country;

    #whatsapp send message
    protected $whatsApp;

	/**
	* __construct
	* This method is used to initilize the instance
	* Author: Sanjoy
	*/
	public function __construct(UserRepository $user,
        MerchantRepository $merchant,
        CartMasterRepository $cart,
        CartDetailRepository $cartDetails,
        OrderMasterRepository $order,
        OrderDetailRepository $orderDetails,
        OrderSellerRepository $orderSeller,
        ProductVariantRepository $productVariant,
        ProductRepository $product,
        ProductVariantDetailRepository $productVariantDetail,
        CountryRepository $country,
        WhatsAppController $whatsApp
    )
	{
		$this->user         			= $user;
        $this->merchant                 = $merchant;
        $this->cart         			= $cart;
        $this->cartDetails  			= $cartDetails;
        $this->order         			= $order;
        $this->orderDetails  			= $orderDetails;
        $this->orderSeller  			= $orderSeller;
        $this->productVariant  			= $productVariant;
        $this->productVariantDetail  	= $productVariantDetail;
        $this->product  				= $product;
        $this->country            		= $country;
        $this->whatsApp                 = $whatsApp;
        // $this->middleware('auth');
    }
    private function sendCustomerAndSellerMail($orderId) {
        $data = [];
        $data['order'] = $order = $this->order->where('id', $orderId)
            ->with('shippingAddress.getCountry','billingAddress.getCountry','getCountry')->first();

        $data['orderDetails'] = $this->orderDetails->where(['order_master_id' => $orderId])
            ->with('sellerDetails','defaultImage','productByLanguage','productVariantDetails')
            ->get();
        $contactList = [];
        $i=0;
        if(@Auth::user()) {
            $data['fname'] = @Auth::user()->fname;
            $data['lname'] = @Auth::user()->lname;
            $data['email'] = @Auth::user()->email;
            $data['user_type'] = @Auth::user()->user_type;
            
        } else {
            $user = $this->user->whereId($order->user_id)->first();
            $data['fname'] = $user->fname;
            $data['lname'] = $user->lname;  
            $data['email'] = $user->email;
            $data['user_type'] = $user->user_type;
            // $orddd = encrypt($order->order_no);
            // $data['url_dtls'] = "http://localhost/drivers_group/guest-order-details/".$orddd;
            // $data['url_dtls'] = route('guest.order.details', ['trackId' => encrypt($order->order_no)]);

            // $contactList[$i] = $user->email;    
        }
        $data['shipping_price'] = @$order->shipping_price;
        $data['orderNo'] = $order->order_no;
        $data['language_id'] = getLanguage()->id;
        // $contactList[$i] = @$order->shipping_email;
        $data['bcc'] = $contactList;
        
        $data['sub_total'] = @$order->subtotal;
        $data['order_total'] = @$order->order_total;
        $data['total_discount'] = $order->total_discount;
        if(@$order->status == 'N'){
            $data['subject'] = "Your order has been placed successfully! Order no: ".@$order->order_no;
        }else{
            $data['subject'] = "Your order is out for delivery! Order No:".@$order->order_no." ! Order Details!";
        }
        

        #send mail admin to notify for new order only as client requirement (internal order)
        $i=0;
        if(@$order->status =='N'){
            $setting = Setting::first();
            if(@$setting->email_1) {
                // $contactList[$i] = $setting->email;
                $contactList[$i] = $setting->email_1;
                $i++;
            }
            if(@$setting->email_2) {
                // $contactList[$i] = $contact->sellerDetails->email;
                $contactList[$i] = $setting->email_2;
                $i++;
            }
            if(@$setting->email_3) {
                // $contactList[$i] = $contact->sellerDetails->email;
                $contactList[$i] = $setting->email_3;
                $i++;
            }    

        }
        $data['bcc'] = $contactList;
        #send to customer mail(order status can be new/driver assigned)
        
        Mail::send(new OrderMail($data));
        
        # send mail order details to seller mail

        $dtls_mail = $this->orderDetails->where(['order_master_id' => $order->id])->groupBy('seller_id')->get();
        
        foreach(@$dtls_mail as $contact){
                // if(@$contact->status == 'DA'){
                    $i = 0;
                    //start
                    $data['user_type'] = 'C';
                    $data['orderDetails'] = $this->orderDetails->where(['order_master_id' => $order->id,'seller_id'=>@$contact->seller_id])
                    ->with('sellerDetails','defaultImage','productByLanguage','productVariantDetails')
                    ->get();
                    $data['sub_total'] =  $this->orderDetails->where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('sub_total');
                    $data['order_total'] =  $this->orderDetails->where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total');
                    $data['total_discount'] = $this->orderSeller->where(['order_master_id' => @$order->id,'seller_id'=>@$contact->seller_id])->sum('total_discount');
                    $data['shipping_price'] = "NA";
                    if(@$order->status == 'N'){
                        $data['subject'] = "New Order Placed!Order No : ".@$order->order_no." ! Order Details!";
                    }else{
                        $data['subject'] = "Order is Accepted! Order No : ".@$order->order_no." ! Order Details!";    
                    }
                    
                    // Fill the Bcc email address
                    
                    $contactList[$i] = $contact->sellerDetails->email;
                    $i++;
                    #send mail to merchant if status driver assigned(client requirement)
                    if(@$order->status =='DA'){
                        if($contact->sellerDetails->email1) {
                            $contactListEmail1[$i] = $contact->sellerDetails->email1;
                            $i++;
                        }
                        if($contact->sellerDetails->email2) {
                            $contactListEmail2[$i] = $contact->sellerDetails->email2;
                            $i++;
                        }
                        if($contact->sellerDetails->email3) {
                            $contactListEmail3[$i] = $contact->sellerDetails->email3;
                            $i++;
                        }    
                    }
                    $contactList = array_unique($contactList);
                    if(@$contactListEmail1 != '') {
                        $contactListEmail1 = array_unique($contactListEmail1);
                        $contactList = array_merge($contactList, $contactListEmail1);
                    }
                    if(@$contactListEmail2 != '') {
                        $contactListEmail2 = array_unique($contactListEmail2);
                        $contactList = array_merge($contactList, $contactListEmail2);
                    }
                    if(@$contactListEmail3 != '') {
                        $contactListEmail3 = array_unique($contactListEmail3);
                        $contactList = array_merge($contactList, $contactListEmail3);
                    }
                    $data['bcc'] = $contactList;
            
                Mail::send(new SendOrderDetailsMerchantMail($data));    
            // }
            

        }
    }
}
