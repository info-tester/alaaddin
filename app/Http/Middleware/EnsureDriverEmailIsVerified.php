<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class EnsureDriverEmailIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return \Illuminate\Http\Response
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user('driver') || ($request->user('driver') instanceof MustVerifyEmail && !$request->user('driver')->hasVerifiedEmail())) {
            return Redirect::route('driver.verification.notice');
        }

        return $next($request);
    }
}
