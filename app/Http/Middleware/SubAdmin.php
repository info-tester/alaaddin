<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class SubAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $menuId = NULL)
    {
        $menuId = explode('|', $menuId);
        if(!@Auth::guard('admin')->user()) {
            return redirect()->route('admin.login');
        }
        $roles = Auth::guard('admin')->user()->roles->pluck('menu_id')->toArray();
        
        if(Auth::guard('admin')->user()->type == 'A') {
            return $next($request);
        } else {
            if(count($menuId) > 1) {
                $counter = 0;
                foreach($menuId as $id) {
                    if(in_array($id, $roles) || Auth::guard('admin')->user()->type == 'A') {
                        $counter++;
                    }    
                }
                # if permission available for anyone internal or external then redirect to next page otherwise send to dashboarc
                if($counter < 1) {
                    return redirect()->route('admin.dashboard');
                } else {
                    return $next($request);
                }
            } else {
                if(in_array($menuId[0], $roles) || Auth::guard('admin')->user()->type == 'A') {
                    return $next($request);
                } else {
                    return redirect()->route('admin.dashboard');
                }
            }
        }
    }
}
