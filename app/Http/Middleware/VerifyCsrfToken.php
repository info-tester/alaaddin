<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'search-products-vue',
        'get-variant-price',
        'get-product-datatable',
        'update-firebase-token',
        'make-payment',
        'admin/delete-mark-orders',

        # admin datatable
        'admin/list-sub-admin',
        'admin/get-product-datatable',
        'admin/load-category-list',
        'admin/list-product-options',
        'admin/list-manufacturer',
        'admin/manage-customer',
        'admin/manage-merchant',
        'admin/manage-driver',
        'admin/lists-of-orders-datatable',
        'admin/zone-list',
        'admin/zone-rate-list',
        'admin/finance',
        'admin/lists-of-earning-orders-datatable',
        'admin/user-report',
        'admin/merchant-report',
        'admin/product-report',
        'admin/lists-of-driver-orders-datatable',

        # merchant datatable
        'merchant/merchant-get-product-datatable',
        'merchant/merchant-search-order'
    ];
}
