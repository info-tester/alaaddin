<?php

namespace App\Http\Middleware;

use Closure;

use App\Models\RequestLog;

class RequestLogger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            # this middleware is used to save the request data on post method
            if(($request->header('sec-fetch-dest') == 'document' || $request->header('sec-fetch-dest') == '' || $request->header('X-Requested-With') == 'XMLHttpRequest') && ($request->route()->getName() != 'login' || $request->route()->getName() != 'register' || $request->route()->getName() != 'api.login' || $request->route()->getName() != 'api.register')) {
                $device = ($request->hasHeader('X-device')) ? $request->header('X-device') : 'W';
                $insert = [
                    'header'            => json_encode($request->header()),
                    'body'              => json_encode($request->all()),
                    'ip'                => $request->ip(),
                    'access_origin'     => $device,
                    'method'            => $request->method(),
                    'request_url'       => $request->url(),
                ];
                RequestLog::create($insert);
            }
        } catch(\Exception $e) {

        }
        return $next($request);
    }
}
