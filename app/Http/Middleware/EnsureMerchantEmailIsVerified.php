<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class EnsureMerchantEmailIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return \Illuminate\Http\Response
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user('merchant') || ($request->user('merchant') instanceof MustVerifyEmail && !$request->user('merchant')->hasVerifiedEmail())) {
            return Redirect::route('merchant.verification.notice');
        }

        return $next($request);
    }
}
