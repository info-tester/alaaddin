<?php
namespace App\Http\View\Composers;

use Illuminate\View\View;
use App\Repositories\SettingRepository;
use App\Repositories\CartMasterRepository;
use Auth;

/**
 * This class is used to show category in megamenu.
 */
class CategoryComposer 
{
	protected $category, $language, $cart;

	public function __construct(SettingRepository $category,
                                CartMasterRepository $cart
                                )
	{
		$this->category = $category;
        $this->cart     = $cart;
	}

	/**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $categories = $this->category->select('category_json')->first();
        $data['categories'] = json_decode($categories->category_json);
        $data['language'] = getLanguage();

        #to show the cart count
        $data['cart'] = $this->cart->orWhere(['session_id' => session()->getId(), 'user_id' => Auth::id()])->first();
        $view->with($data);
    }
}