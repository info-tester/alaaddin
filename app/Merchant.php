<?php

namespace App;

use App\Notifications\Merchant\Auth\ResetPassword;
use App\Notifications\Merchant\Auth\VerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
class Merchant extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'fname','lname', 'email', 'phone', 'password','image','company_name','address','city','state','zipcode','country','email_vcode',
    // ];
    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    /*
    * Method: merchantCompanyDetails
    * Description: This method is used to merchant company details.
    * Author : Jayatri
    */

    public function merchantCompanyDetails() {
        return $this->hasMany('App\Models\MerchantCompanyDescription', 'merchant_id', 'id');
    }
    
    public function merchantCompanyDetailsByLanguage() {
        return $this->hasOne('App\Models\MerchantCompanyDescription', 'merchant_id', 'id')->where('language_id',getLanguage()->id);
    }
    /*
    * Method: merchantOpenning
    * Description: This method is used to get details of merchant openning & closing hours.
    * Author : Jayatri
    */

    public function merchantOpenning() {
        return $this->hasMany('App\Models\MerchantOppening', 'merchant_id', 'id');
    }
    /*
    * Method: merchantImage
    * Description: This method is used to get details of merchant images.
    * Author : Jayatri
    */

    public function merchantImage() {
        return $this->hasMany('App\Models\MerchantImage', 'merchant_id', 'id');
    }
    /*
    * Method: State
    * Description: This method is used to get details of State.
    * Author : Jayatri
    */

    public function State() {
        return $this->hasOne('App\Models\State', 'id', 'state');
    }
    /*
    * Method: Country
    * Description: This method is used to get details of Country.
    * Author : Jayatri
    */

    public function Country() {
        return $this->hasOne('App\Models\CountryDetails', 'country_id', 'country')->where('language_id',getLanguage()->id);
    }
    public function CountryDtls() {
        return $this->hasOne('App\Models\CountryDetails', 'country_id', 'country')->where('language_id',getLanguage()->id);
    }
    public function merchantCityDetails() {
        return $this->hasOne('App\Models\CitiDetails', 'city_id', 'city_id')->where('language_id',getLanguage()->id);
    }
    public function DriverDetails(){
        return $this->hasOne('App\Driver', 'id', 'driver_id');    
    }

    public function orderDetails(){
        return $this->hasMany('App\Models\OrderDetail', 'seller_id', 'id')
        ->select('id','seller_id','order_master_id','rate','comment','review_date')
        ->WhereNotNull('rate')
        ->WhereNotNull('comment');
    }
    public function merchantAllOrder()
    {
        return $this->hasMany('App\Models\OrderDetail', 'seller_id', 'id')
        ->where('status','OD');
    }
    public function merchantOrderDetails(){
        return $this->hasOne('App\Models\OrderDetail', 'seller_id', 'id');    
    }
    public function orderSeller(){
        return $this->hasMany('App\Models\OrderSeller', 'seller_id', 'id');    
    }
    public function getOrderSellerDetails() {
        return $this->hasMany('App\Models\OrderSeller', 'seller_id', 'id');
    }

    /**
    * Method: products
    * Description: This method is used to get products of this seller for a particular cart.
    * Author: Sanjoy
    */
    public function products() {
        return $this->hasMany('App\Models\CartDetail', 'seller_id', 'id')->select('id','cart_master_id','product_id','product_variant_id','weight','variants','seller_id','quantity','shipping_price','original_price',\DB::raw('IF(discounted_price>=0, original_price, discounted_price ) AS discounted_price'),'total_discount','subtotal','total','seller_commission','product_note');
    }

    /**
    * Method: getAddressbook
    * Description: This get merchant address book.
    * Author: Argha
    * Date  : 2020-09-29
    */
    public function getAddressbook() {
        return $this->hasMany('App\Models\MerchantAddressBook', 'merchant_id', 'id');
    }

    /**
    * Method: getWithdrawRecord
    * Description: This get merchant withdraw request after 30-01-12
    * Author: Argha
    * Date  : 2021-03-12
    */
    public function getWithdrawRecord() {
        return $this->hasMany('App\Models\Withdraw', 'seller_id', 'id')->where('payment_date','>','2021-01-30 00:00:00')->where('status','S');
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function city_details() {
        return $this->hasOne('App\Models\City', 'id', 'city');
    }
    public function state_details() {
        return $this->hasOne('App\Models\State', 'id', 'state');
    }
    public function getMerchantProducts() {
        return $this->hasMany('App\Models\CartDetail', 'seller_id', 'id')->select('id','cart_master_id','product_id','product_variant_id','weight','variants','seller_id','quantity','shipping_price','original_price',\DB::raw('IF(discounted_price>=0, original_price, discounted_price ) AS discounted_price'),'total_discount','subtotal','total','seller_commission','product_note');
    }
}
