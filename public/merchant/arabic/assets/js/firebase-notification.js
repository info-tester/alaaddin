var cT;
var firebaseConfig  = {
    apiKey: "AIzaSyDkVeodkC97tvpn8sT2ZvOZpliQ_Hs7lyg",
    authDomain: "aswagnadrivergroup.firebaseapp.com",
    databaseURL: "https://aswagnadrivergroup.firebaseio.com",
    projectId: "aswagnadrivergroup",
    storageBucket: "aswagnadrivergroup.appspot.com",
    messagingSenderId: "485359773499",
    appId: "1:485359773499:web:dc4cc02195ac398f7fb91e",
    measurementId: "G-DGY1XMV15W"
};
firebase.initializeApp(firebaseConfig);
firebase.analytics();
const messaging = firebase.messaging();

messaging.usePublicVapidKey("BC4aUPIkRYgdiDCj7-_bgXF_6Fw2vPLaejWCcLCubboLCUQXozSHF5DVhFACPMDOaNQLr4Q3ljTPxJPvHwKkVS8");
// [END set_public_vapid_key]
// [START refresh_token]
// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(function() {
    messaging.getToken().then(function(refreshedToken) {
        console.log('Token refreshed.');
        // Indicate that the new Instance ID token has not yet been sent to the
        // app server.
        setTokenSentToServer(false);
        // Send Instance ID token to app server.
        sendTokenToServer(refreshedToken);
        // [START_EXCLUDE]
        // Display new Instance ID token and clear UI of all previous messages.
        resetUI();
        // [END_EXCLUDE]
    }).catch(function(err) {
        console.log('Unable to retrieve refreshed token ', err);
    });
});
// [END refresh_token]

// [START receive_message]
// Handle incoming messages. Called when:
// - a message is received while the app has focus
// - the user clicks on an app notification created by a service worker
//   `messaging.setBackgroundMessageHandler` handler.
messaging.onMessage(function(payload) {
    console.log('Message received. ', payload);
    // [START_EXCLUDE]
    // Update the UI to include the received message.
    appendMessage(payload);
    // [END_EXCLUDE]
});
// [END receive_message]


function resetUI() {
  console.log("RestUi");
    // [START get_token]
    // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    messaging.getToken().then(function(currentToken) {
        if (currentToken) {
            cT = currentToken;
            console.log(currentToken);

            sendTokenToServer(currentToken);
            updateUIForPushEnabled(currentToken);
            console.log("restUI");
        } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');
            // Show permission UI
            setTokenSentToServer(false);
        }
    }).catch(function(err) {
        console.log('An error occurred while retrieving token. ', err);
        setTokenSentToServer(false);
    });
    // [END get_token]
}

function showToken(currentToken) {
  console.log("showToken");
    // alert(currentToken);
    // Show token in console and UI.
    //call ajax to insert/update token in server
}

// Send the Instance ID token your application server, so that it can:
// - send messages back to this app
// - subscribe/unsubscribe the token from topics
function sendTokenToServer(currentToken) {
  console.log("sendTokenToServer")
    if (!isTokenSentToServer()) {
        // console.log('Sending token to server...');
        var urlRefresh = "https://aswagna.co/merchant/refresh-dashboard";
        $.ajax({
            url:urlRefresh,
            dataType: 'json',
            data: {
                token: currentToken
            },
            type: 'get',
            success: function(resp){
            }
        });
        // TODO(developer): Send the current token to your server.
        setTokenSentToServer(true);
    } else {
        console.log('Token already sent to server so won\'t send it again ' +
        'unless it changes');
    }
}

function isTokenSentToServer() {
  console.log("isTokenSentToServer");
    return window.localStorage.getItem('sentToServer') === '1';
}

function setTokenSentToServer(sent) {
  console.log("setTokenSentToServer");
    window.localStorage.setItem('sentToServer', sent ? '1' : '0');
}

function requestPermission() {
  console.log("requestPermission");
    console.log('Requesting permission...');
    // [START request_permission]
    messaging.requestPermission().then(function() {
        console.log('Notification permission granted.');
        // TODO(developer): Retrieve an Instance ID token for use with FCM.
        // [START_EXCLUDE]
        // In many cases once an app has been granted notification permission, it
        // should update its UI reflecting this.
        resetUI();
        // [END_EXCLUDE]
    }).catch(function(err) {
        console.log('Unable to get permission to notify.', err);
    });
    // [END request_permission]
}
// Add a message to the messages element.
function appendMessage(payload) {
  console.log("appendMessage");
    //show the custom notification
    console.log(payload);
    var from_id = payload.data.from_id;
    var message_id = payload.data.message_id;
    var to_id = payload.data.to_id;
    var from_user_name = payload.data.from_user_name;
    // receiveMessage(from_id, from_user_name, to_id, message_id);

    getOrderStat()
}

function getOrderStat() {
    // console.log("getOrderStat");
    var urlRefresh = "https://aswagna.co/merchant/get-order-stat";
    $.ajax({
        url:urlRefresh,
        dataType: 'json',
        // data: {
        //     token: currentToken
        // },
        type: 'get',
        success: function(resp){
          // console.log("total orders: "+resp.totalOrders);
          // console.log(resp);
          // console.log(JSON.stringify(resp));
          $(".totalProfitMerchant").text(resp.totalProfit+"KWD");
          $(".todayProfitMerchant").text(resp.todayProfit+"KWD");
          $(".total_products").text(resp.totalProducts);
          $(".totalOrders").text(resp.totalOrders);
          $(".todayExOrders").text(resp.todayExOrders);
          $(".todayInOrderEarnings").text(resp.todayInOrderEarnings+"KWD");
          $(".todayInOrders").text(resp.todayInOrders);
          $(".todayExOrderEarnings").text(resp.todayExOrderEarnings+"KWD");
          $(".monthlyInOrderEarnings").text(resp.monthlyInOrderEarnings+"KWD");
          $(".monthlyInOrders").text(resp.monthlyInOrders);
          $(".monthlyExOrderEarnings").text(resp.monthlyExOrderEarnings+"KWD");
          $(".monthlyExOrders").text(resp.monthlyExOrders+"KWD");
          // console.log($("."))
          // var html = '<tr '+'style="font-weight: 600;'+'>\n\
          //     <td>'+"Image"+'</td>\n\
          //     <td>'+"Title"+'</td>\n\
          //     <td>'+"Price"+'</td>\n\
          //     <td>'+"Status"+'</td>\n\
          // </tr>';
          // var html= "<tr style='font-weight:600px;'>\n\
          //     <td> Image</td>\n\
          //     <td>Title</td>\n\
          //     <td>Price</td>\n\
          //     <td>Status Total</td>\n\
          // </tr>";
          // var image = "",title = "";
          // for(var i=0;i<resp.product.length;i++){ 
          //   var st = resp.product[i].status;
          //   if(st == 'A'){
          //       st = "Active";
          //   }else{
          //       st = "Inactive";
          //   }

          // html = html + '<tr>';  
          //   console.log("response of title : "+resp.product[0].productByLanguage);
          //   if(resp.product[i].productByLanguage != undefined){
          //     title = resp.product[i].productByLanguage.title;  
          //   }else{ 
          //     title = "";
          //   }
            
          //   if(resp.product[i].defaultImage != undefined){
          //     html = html+'<td class="product"><img class="product-img" style="width:60px;height:50px;" data-toggle="tooltip" data-title="'+title+'" src="https://aswagna.co/storage/app/public/products/'+resp.product[i].defaultImage.image+'"></td>';
          //   }else{
          //     // html = html+'<td class="product"><img class="product-img" style="width:100px;height:100px;" data-toggle="tooltip" data-title="'+title+'" src="https://aswagna.co/public/frontend/images/default_product.png"></td>';
          //     html = html+'<td><img class="product-img mCS_img_loaded" style="width:60px;height:50px;" data-toggle="tooltip" data-title="test" src="https://aswagna.co/public/frontend/images/default_product.png" data-original-title="" title=""></td>';
          //   }

          //   // html = html+'<td class="product">'+image+'</td>';
          //   html = html + '<td class="product">'+resp.product[i].slug+'</td>';  
          //   html = html + '<td class="price">'+resp.product[i].price+' KWD </td>';
          //   html = html + '<td class="status"><span class="badge badge-pill bg-success">'+st+'</span></td>';
          //   html = html + "</tr>";   
          // }
          // html = html + '<tr>\n\
          //                   <td colspan="5"><a href="https://aswagna.co/merchant/merchant-manage-product" class="btn btn-outline-light float-right">View All</a></td>\n\
          //               </tr>'
          // // html = $(".changeResponse").html();
          // $(".changeResponse").html(html); 
          var orderHtml= "<tr style='font-weight:600px;'>\n\
              <td>رقم الأمر </td>\n\
              <td>تاريخ</td>\n\
              <td>نوع</td>\n\
              <td>الطلب الكلي</td>\n\
              <td>الحالة </td>\n\
          </tr>";
          for(var i=0;i<resp.orders.length;i++){
            var status = resp.orders[i].status;
            if(status == 'N'){
                status = "New"
            }
            if(status == 'I'){
                status = "Incomeplete"
            }
            if(status == 'DA'){
                status = "Driver Assigned"
            }
            var orderType = resp.orders[i].order_type;
            if(orderType == 'I'){
                orderType = 'Internal';
            }else{
                orderType = 'External';
            }
            orderHtml= orderHtml + '<tr style="font-weight: 600;">';
            orderHtml = orderHtml+'<td style="font-weight: 600;">'+resp.orders[i].order_no+'</td>';
            orderHtml = orderHtml+'<td class="product">'+resp.orders[i].created_at+'</td>';
            orderHtml = orderHtml+'<td class="status"><span class="badge badge-pill bg-info">'+orderType+'</span></td>';
            orderHtml = orderHtml+'<td class="price">'+resp.orders[i].order_total+'</span></td>';
            orderHtml = orderHtml+'<td class="status"><span class="badge badge-pill bg-info">'+status+'</span></td>';
            orderHtml= orderHtml + "</tr>" ;
          } 
          orderHtml = orderHtml + '<tr>\n\
                <td colspan="5"><a href="https://aswagna.co/merchant/merchant-order" class="btn btn-outline-light float-right">View All</a></td>\n\
            </tr>'
          // orderHtml = $(".changeOrderResponse").html();
          // console.log(orderHtml);
          $(".changeOrderResponse").html(orderHtml);
      //     var html = '<tr style="font-weight: 600;">\n\
      //     <td>@lang('admin_lang_static.image')</td>\n\
      //     <td>@lang('admin_lang.title')</td>\n\
      //     <td>@lang('admin_lang_static.price')</td>\n\
      //     <td>@lang('admin_lang.Status')</td>\n\
      // </tr>';
      // for(var i=0;i<resp.product.length;i++){
      //   html = html + '<td class="product">'+resp.product.productByLanguage.title+'</td>';  
      //   if(resp.product.defaultImage.image){
      //     var image = '<img class="product-img" data-toggle="tooltip" data-title="'+resp.product.productByLanguage.title+'" src="{{url('storage/app/public/products/'.resp.product.defaultImage.image)">';
          
      //   }else
      //   html = html+'<td class="product">'+image+'</td>';
      // }
      // $(".changeResponse").text(html);

        }
    });
}

function updateUIForPushEnabled(currentToken) {
  console.log("updateUIForPushEnabled");
    showToken(currentToken);
    cT=currentToken;
}

self.addEventListener('notificationclick', function(event) {
    let url = 'https://google.com';
    event.notification.close(); // Android needs explicit close.
    event.waitUntil(
        clients.matchAll({type: 'window'}).then( windowClients => {
            // Check if there is already a window/tab open with the target URL
            for (var i = 0; i < windowClients.length; i++) {
                var client = windowClients[i];
                // If so, just focus it.
                if (client.url === url && 'focus' in client) {
                    return client.focus();
                }
            }
            // If not, then open the target URL in a new window/tab.
            if (clients.openWindow) {
                return clients.openWindow(url);
            }
        })
    );
});

//resetUI();
requestPermission();