@extends('layouts.app')

@section('title')
{{ config('app.name', '') }} | Under Maintanance
@endsection

@section('links')
@include('includes.links')
<link href="{{ asset('public/frontend/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!--wrapper start-->
<div class="wrapper-new">
			<div class="opacitys"></div>
    <div class="logo_div">
        <a href="https://play.google.com/store/apps/details?id=com.buyer.alaaddin"><img src="{{asset('public/frontend/images/unnamed.webp')}}" alt="" style="width:130px;" /></a>
        <a href="https://play.google.com/store/apps/details?id=com.buyer.alaaddin" target="_blank" class="new_apps">
    			<img src="{{asset('public/frontend/images/goolapp.png')}}" alt="">
    		</a>
    </div>
    <div class="banner_divs" >
    	<div class="grocery-texts">
    		<p>Get Your <br> <span>Products</span> <br> Delivered  <br> <span> Fast</span></p>
    		
    	</div>
    	<img src="{{asset('public/frontend/images/wait.png')}}" alt="">
    </div>

    <div class="copy-rights" >
    	<p>Copyright © 2023 Alaaddin.com | All Rights Reserved.</p>
    </div>
		
</div>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection