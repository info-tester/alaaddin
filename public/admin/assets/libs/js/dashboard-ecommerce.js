
$(function() {
    
    // $.ajax({
    //     url:"{{ route('admin.product.sales.graph') }}",
    //     type:"GET",
    //     success:function(resp){
    //         // alert(resp);
    //     }
    // });
    "use strict";
    // ============================================================== 
    // Product Sales
    // ============================================================== 
    //ajax call to get the json data
    




    // ============================================================== 
    // Customer acquisition
    // ============================================================== 
    



   
    // ============================================================== 
    // Revenue Cards
    // ============================================================== 
    $("#sparkline-revenue").sparkline([5, 5, 7, 7, 9, 5, 3, 5, 2, 4, 6, 7], {
        type: 'line',
        width: '99.5%',
        height: '100',
        lineColor: '#5969ff',
        fillColor: '#dbdeff',
        lineWidth: 2,
        spotColor: undefined,
        minSpotColor: undefined,
        maxSpotColor: undefined,
        highlightSpotColor: undefined,
        highlightLineColor: undefined,
        resize:true
    });



    $("#sparkline-revenue2").sparkline([3, 7, 6, 4, 5, 4, 3, 5, 5, 2, 3, 1], {
        type: 'line',
        width: '99.5%',
        height: '100',
        lineColor: '#ff407b',
        fillColor: '#ffdbe6',
        lineWidth: 2,
        spotColor: undefined,
        minSpotColor: undefined,
        maxSpotColor: undefined,
        highlightSpotColor: undefined,
        highlightLineColor: undefined,
        resize:true
    });



    $("#sparkline-revenue3").sparkline([5, 3, 4, 6, 5, 7, 9, 4, 3, 5, 6, 1], {
        type: 'line',
        width: '99.5%',
        height: '100',
        lineColor: '#25d5f2',
        fillColor: '#dffaff',
        lineWidth: 2,
        spotColor: undefined,
        minSpotColor: undefined,
        maxSpotColor: undefined,
        highlightSpotColor: undefined,
        highlightLineColor: undefined,
        resize:true
    });



    $("#sparkline-revenue4").sparkline([6, 5, 3, 4, 2, 5, 3, 8, 6, 4, 5, 1], {
        type: 'line',
        width: '99.5%',
        height: '100',
        lineColor: '#fec957',
        fillColor: '#fff2d5',
        lineWidth: 2,
        spotColor: undefined,
        minSpotColor: undefined,
        maxSpotColor: undefined,
        highlightSpotColor: undefined,
        highlightLineColor: undefined,
        resize:true,
    });
 
   



    // ============================================================== 
    // Total Revenue
    // ============================================================== 
    Morris.Area({
        element: 'morris_totalrevenue',
        behaveLikeLine: true,
        data: [
            { x: '2016 Q1', y: 0, },
            { x: '2016 Q2', y: 7500, },
            { x: '2017 Q3', y: 15000, },
            { x: '2017 Q4', y: 22500, },
            { x: '2018 Q5', y: 30000, },
            { x: '2018 Q6', y: 40000, }
        ],
        xkey: 'x',
        ykeys: ['y'],
        labels: ['Y'],
        lineColors: ['#5969ff'],
          resize: true

    });




    // ============================================================== 
    // Revenue By Categories
    // ============================================================== 

    // var chart = c3.generate({
    //     bindto: "#c3chart_category",
    //     data: {
    //         columns: [
    //             ['Men', 100],
    //             ['Women', 80],
    //             ['Accessories', 50],
    //             ['Children', 40],
    //             ['Apperal', 20],
                
    //         ],
    //         type: 'donut',
           
    //         onclick: function(d, i) { console.log("onclick", d, i); },
    //         onmouseover: function(d, i) { console.log("onmouseover", d, i); },
    //         onmouseout: function(d, i) { console.log("onmouseout", d, i); },

    //         colors: {
    //             Men: '#5969ff',
    //             Women: '#ff407b',
    //             Accessories: '#25d5f2',
    //             Children: '#ffc750',
    //             Apperal: '#2ec551',
               


    //         }
    //     },
    //     donut: {
    //         label: {
    //             show: false
    //         }
    //     },



    // });



 


    });



