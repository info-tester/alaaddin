var cT;
var notificationParams = {};
var firebaseConfig = {
    apiKey: "AIzaSyDkVeodkC97tvpn8sT2ZvOZpliQ_Hs7lyg",
    authDomain: "aswagnadrivergroup.firebaseapp.com",
    databaseURL: "https://aswagnadrivergroup.firebaseio.com",
    projectId: "aswagnadrivergroup",
    storageBucket: "aswagnadrivergroup.appspot.com",
    messagingSenderId: "485359773499",
    appId: "1:485359773499:web:dc4cc02195ac398f7fb91e",
    measurementId: "G-DGY1XMV15W"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

const messaging = firebase.messaging();

messaging.usePublicVapidKey("BC4aUPIkRYgdiDCj7-_bgXF_6Fw2vPLaejWCcLCubboLCUQXozSHF5DVhFACPMDOaNQLr4Q3ljTPxJPvHwKkVS8");
// [END set_public_vapid_key]
// [START refresh_token]
// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(function() {
  messaging.getToken().then(function(refreshedToken) {
    console.log('Token refreshed.');
    // Indicate that the new Instance ID token has not yet been sent to the
    // app server.
    setTokenSentToServer(false);
    // Send Instance ID token to app server.
    sendTokenToServer(refreshedToken);
    // [START_EXCLUDE]
    // Display new Instance ID token and clear UI of all previous messages.
    resetUI();
    // [END_EXCLUDE]
  }).catch(function(err) {
    console.log('Unable to retrieve refreshed token ', err);
  });
});
// [END refresh_token]

// [START receive_message]
// Handle incoming messages. Called when:
// - a message is received while the app has focus
// - the user clicks on an app notification created by a service worker
//   `messaging.setBackgroundMessageHandler` handler.
messaging.onMessage(function(payload) {
  console.log('Message received. ', payload);
  // [START_EXCLUDE]
  // Update the UI to include the received message.
  appendMessage(payload);
  // [END_EXCLUDE]
});
// [END receive_message]


function resetUI() {
    // [START get_token]
    // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    messaging.getToken().then(function(currentToken) {
        if (currentToken) {
            console.log(currentToken);
            sendTokenToServer(currentToken);
            
        } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');
            // Show permission UI
            setTokenSentToServer(false);
        }
    }).catch(function(err) {
        console.log('An error occurred while retrieving token. ', err);
        setTokenSentToServer(false);
    });
    // [END get_token]
}

function showToken(currentToken) {
  // Show token in console and UI.
  //call ajax to insert/update token in server
  console.log(currentToken);
  // $('#firebaseToken_id').val(currentToken);
  var reqData = {
    jsonrpc : '2.0',
    params  : {
      token : currentToken,
      product_id: notificationParams.product_id,
      variants: notificationParams.variants,
      email: notificationParams.email
    }
  };
  $.ajax({
    // url:"http://localhost/aswagna/update-firebase-token",
    url:"https://www.aswagna.co/addon/update-firebase-token",
    type:"post",
    dataType: 'json',
    data: reqData,
    success: function(response){
      console.log(response)
    }
  });
}

// Send the Instance ID token your application server, so that it can:
// - send messages back to this app
// - subscribe/unsubscribe the token from topics
function sendTokenToServer(currentToken) {
  updateUIForPushEnabled(currentToken);
  if (!isTokenSentToServer()) {
    console.log('Sending token to server...');
    // TODO(developer): Send the current token to your server.
    setTokenSentToServer(true);
  } else {
    console.log('Token already sent to server so won\'t send it again ' +
        'unless it changes');
  }
}

function isTokenSentToServer() {
  return window.localStorage.getItem('sentToServer') === '1';
}

function setTokenSentToServer(sent) {
  window.localStorage.setItem('sentToServer', sent ? '1' : '0');
}

function requestPermission(params) {
  notificationParams = params;
  console.log('Requesting permission...');
  // [START request_permission]
  messaging.requestPermission().then(function() {
    console.log('Notification permission granted.');
    resetUI();
    // [END_EXCLUDE]
  }).catch(function(err) {
    console.log('Unable to get permission to notify.', err);
  });
  // [END request_permission]
}
// Add a message to the messages element.
function appendMessage(payload) {;
  $('.noti-title').html(payload.data.title)
  $('.noti-desc').html(payload.data.message)
  $('.custom-notification').attr('onclick', "location.href='"+ payload.data.click_action+"'")
  $('.custom-notification').fadeIn();
 
}

function updateUIForPushEnabled(currentToken) {
  showToken(currentToken);
  cT=currentToken;
}

self.addEventListener('notificationclick', function(event) {
    let url = 'https://google.com';
    event.notification.close(); // Android needs explicit close.
    event.waitUntil(
        clients.matchAll({type: 'window'}).then( windowClients => {
            // Check if there is already a window/tab open with the target URL
            for (var i = 0; i < windowClients.length; i++) {
                var client = windowClients[i];
                // If so, just focus it.
                if (client.url === url && 'focus' in client) {
                    return client.focus();
                }
            }
            // If not, then open the target URL in a new window/tab.
            if (clients.openWindow) {
                return clients.openWindow(url);
            }
        })
    );
});

//resetUI();

// requestPermission();