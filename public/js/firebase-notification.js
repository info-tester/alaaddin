var cT;
 var firebaseConfig = {
    apiKey: "AIzaSyDkVeodkC97tvpn8sT2ZvOZpliQ_Hs7lyg",
    authDomain: "aswagnadrivergroup.firebaseapp.com",
    databaseURL: "https://aswagnadrivergroup.firebaseio.com",
    projectId: "aswagnadrivergroup",
    storageBucket: "aswagnadrivergroup.appspot.com",
    messagingSenderId: "485359773499",
    appId: "1:485359773499:web:dc4cc02195ac398f7fb91e",
    measurementId: "G-DGY1XMV15W"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

const messaging = firebase.messaging();

messaging.usePublicVapidKey("BC4aUPIkRYgdiDCj7-_bgXF_6Fw2vPLaejWCcLCubboLCUQXozSHF5DVhFACPMDOaNQLr4Q3ljTPxJPvHwKkVS8");
// [END set_public_vapid_key]
// [START refresh_token]
// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(function() {
  messaging.getToken().then(function(refreshedToken) {
    console.log('Token refreshed.');
    // Indicate that the new Instance ID token has not yet been sent to the
    // app server.
    setTokenSentToServer(false);
    // Send Instance ID token to app server.
    sendTokenToServer(refreshedToken);
    // [START_EXCLUDE]
    // Display new Instance ID token and clear UI of all previous messages.
    resetUI();
    // [END_EXCLUDE]
  }).catch(function(err) {
    console.log('Unable to retrieve refreshed token ', err);

  });
});
// [END refresh_token]

// [START receive_message]
// Handle incoming messages. Called when:
// - a message is received while the app has focus
// - the user clicks on an app notification created by a service worker
//   `messaging.setBackgroundMessageHandler` handler.
messaging.onMessage(function(payload) {
  console.log('Message received. ', payload);
  // [START_EXCLUDE]
  // Update the UI to include the received message.
  appendMessage(payload);
  // [END_EXCLUDE]
});
// [END receive_message]


function resetUI() {
    // [START get_token]
    // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    messaging.getToken().then(function(currentToken) {
        if (currentToken) {
            cT = currentToken;
            console.log(currentToken);
            sendTokenToServer(currentToken);
            updateUIForPushEnabled(currentToken);
        } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');
            // Show permission UI
            setTokenSentToServer(false);
        }
    }).catch(function(err) {
        console.log('An error occurred while retrieving token. ', err);
        setTokenSentToServer(false);
    });
    // [END get_token]
}

function showToken(currentToken) {
  // Show token in console and UI.
  //call ajax to insert/update token in server
  console.log(currentToken);
  // $('#firebaseToken_id').val(currentToken);
  var token = {
                'jsonrpc' : '2.0',                
                // '_token'  : '{{csrf_token()}}',
                'data'    : {
                    'token'    : currentToken 
                }
            };
  $.ajax({
    // url:"https://localhost/dzui/update-google-reg",
    url:"https://phpwebdevelopmentservices.com/development/dzui/update-google-reg",
    // type:"get",
    // data:"token="+currentToken,
    // data: token,
    dataType: 'json',
    data: token,
    type: 'get',
    success: function(data){
      console.log(currentToken+' updated to database');
      $(".totalProfitMerchant").text(resp.totalProfit.toFixed(3)+"KWD");
      $(".todayProfitMerchant").text(resp.todayProfit.toFixed(3)+"KWD");
      $(".total_products").text(resp.totalProducts);
      $(".totalOrders").text(resp.totalOrders);
      $(".todayExOrders").text(resp.todayExOrders);
      $(".todayInOrderEarnings").text(resp.todayInOrderEarnings.toFixed(3)+"KWD");
      $(".todayInOrders").text(resp.todayInOrders);
      $(".todayExOrderEarnings").text(resp.todayExOrderEarnings.toFixed(3)+"KWD");
      $(".monthlyInOrderEarnings").text(resp.monthlyInOrderEarnings.toFixed(3)+"KWD");
      $(".monthlyInOrders").text(resp.monthlyInOrders);
      $(".monthlyExOrderEarnings").text(resp.monthlyExOrderEarnings.toFixed(3)+"KWD");
      $(".monthlyExOrders").text(resp.monthlyExOrders.toFixed(3)+"KWD");
      
    }
  });
}

// Send the Instance ID token your application server, so that it can:
// - send messages back to this app
// - subscribe/unsubscribe the token from topics
function sendTokenToServer(currentToken) {
  if (!isTokenSentToServer()) {
    console.log('Sending token to server...');
   
    // TODO(developer): Send the current token to your server.
    setTokenSentToServer(true);
  } else {
    console.log('Token already sent to server so won\'t send it again ' +
        'unless it changes');
  }

}

function isTokenSentToServer() {
  return window.localStorage.getItem('sentToServer') === '1';
}

function setTokenSentToServer(sent) {
  window.localStorage.setItem('sentToServer', sent ? '1' : '0');
}

function requestPermission() {
  console.log('Requesting permission...');
  // [START request_permission]
  messaging.requestPermission().then(function() {
    console.log('Notification permission granted.');
    // TODO(developer): Retrieve an Instance ID token for use with FCM.
    // [START_EXCLUDE]
    // In many cases once an app has been granted notification permission, it
    // should update its UI reflecting this.
    resetUI();
    // [END_EXCLUDE]
  }).catch(function(err) {
    console.log('Unable to get permission to notify.', err);
  });
  // [END request_permission]
}
// Add a message to the messages element.
function appendMessage(payload) {
  //show the custom notification
  console.log(payload);
  var from_id = payload.data.from_id;
  var message_id = payload.data.message_id;
  var to_id = payload.data.to_id;
  var from_user_name = payload.data.from_user_name;
  receiveMessage(from_id, from_user_name, to_id, message_id);
}
function updateUIForPushEnabled(currentToken) {
  showToken(currentToken);
  cT=currentToken;
}

self.addEventListener('notificationclick', function(event) {
    let url = 'https://google.com';
    event.notification.close(); // Android needs explicit close.
    event.waitUntil(
        clients.matchAll({type: 'window'}).then( windowClients => {
            // Check if there is already a window/tab open with the target URL
            for (var i = 0; i < windowClients.length; i++) {
                var client = windowClients[i];
                // If so, just focus it.
                if (client.url === url && 'focus' in client) {
                    return client.focus();
                }
            }
            // If not, then open the target URL in a new window/tab.
            if (clients.openWindow) {
                return clients.openWindow(url);
            }
        })
    );
});

//resetUI();
requestPermission();
