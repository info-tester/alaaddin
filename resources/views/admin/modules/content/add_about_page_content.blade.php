@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | Admin | About Us
@endsection
@section('content')
@section('links')
@include('admin.includes.links')
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">Dasboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        About Us
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if(Session::has('success'))
                <div class="alert alert-info alert-dismissable" id="cross">
                  <a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                    <strong>
                        {!!Session::get('success')!!}
                    </strong>
                </div>              
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible" id="cross">
                  <a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                    <strong>
                        {{Session::get('error')}}
                    </strong>
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible" id="cross">
                    <a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                               
                               About Us
                                
                            </h5>
                            <div class="card-body">
                                <form id="myform" method="post" enctype="multipart/form-data" action="{{ route('admin.manage.pageaboutadd') }}">
                                    @csrf
                                    <div class="row">

                                        <div class="form-group col-12">
                                            <label for="about_us_title" class="col-form-label">Title</label>
                                            <input id="about_us_title" type="text" class="form-control" name="about_us_title" placeholder="" value="{{ @$about_content->about_us_title }}">
                                        </div>

                                        <div class="form-group col-12">
                                            <label for="about_us_description" class="col-form-label">Description</label>
                                            <textarea id="about_us_description" class="form-control desc" name="about_us_description">{{ @$about_content->about_us_description }}</textarea>
                                        </div>
                                            
                                       
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <label for="inputPassword" class="col-form-label"></label>
                                            
                                            <input type="submit" class="btn btn-primary" value="submit">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>

@endsection
@section('scripts')

@include('admin.includes.scripts')
<script src="{{ URL::to('public/frontend/tiny_mce/tinymce.min.js') }}"></script>
<script type="text/javascript">
        tinyMCE.init({
            content_css : "{{ asset('public/frontend/css/style-'.Config::get('app.locale').'.css') }}",
            mode : "specific_textareas",
            editor_selector : "desc",
            height: '320px',
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{csrf_token()}}');
                $.ajax({
                    url: '{{route("admin.artical.img.upload")}}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            }, 
        });
        $("#myform").submit(function (event) {
            if(tinyMCE.get('desc').getContent()==""){
                event.preventDefault();
                $("#cntnt").html("Description field is required").css('color','red');
            }
            else{
                $("#cntnt").html("");
            }
        });
</script>
@endsection