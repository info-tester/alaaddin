@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Content') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | Help Page Content
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            Alaaddin @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        <!-- @lang('admin_lang.edit_city') --> Help Page Content
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                Help Page Content
                            </h5>
                            <div class="card-body">
                                <form id="myform" method="post" enctype="multipart/form-data" action="{{ route('admin.manage.page.help.post',$pageid) }}">
                                    @csrf
                                    @if(@$language)
                                    @foreach(@$language as $key=>$lang)
                                    <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <label for="inputText3" class="col-form-label"><span style="color: red;">*</span> @lang('admin_lang.page_title')</label>
                                        <input type="text" name="title[]" class="form-control required" id="title" placeholder="@lang('admin_lang.title')"  value="{{ @$term_content[$key]->title }}">
                                    </div>
                                    @endforeach
                                    @endif
                                    @if(@$language)
                                    @foreach(@$language as $key=>$lang)
                                    <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label class="personal-label" for="exampleInputEmail1"><span style="color: red;">*</span> @lang('admin_lang.description')</label>
                                            <div class="clearfix"></div>
                                            <textarea name="contract_desc[]" id="desc" rows="10" placeholder="Description" style="width:100%" class="form-control required Description">
                                               {{ strip_tags(@$term_content[$key]->description) }} 
                                            </textarea>
                                        </div>
                                        <div class="clearfix"></div>
                                        <p class="error_1" id="cntnt"></p>
                                        <div class="clearfix"></div>
                                    </div>    
                                    @endforeach
                                    @endif
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top: 10px;">
                                        <div class="submit-login add_btnm">
                                            <input value="Submit" type="submit" class="btn btn-primary">
                                        </div>
                                    </div>
                                    <!--all_time_sho-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>
@endsection
@section('scripts')
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif

<script src="{{ URL::to('public/frontend/tiny_mce/tinymce.min.js') }}"></script>

<style type="text/css">
    .upldd {
        display: block;
        width: auto;
        border-radius: 4px;
        text-align: center;
        background: #9caca9;
        cursor: pointer;
        overflow: hidden;
        padding: 10px 15px;
        font-size: 15px;
        color: #fff;
        cursor: pointer;
        float: left;
        margin-top: 15px;
        font-family: 'Poppins', sans-serif;
        position: relative;
    }
    .upldd input {
        position: absolute;
        font-size: 50px;
        opacity: 0;
        left: 0;
        top: 0;
        width: 200px;
        cursor: pointer;
    }
    .upldd:hover{
        background: #1781d2;
    }

</style>

<script>
    $(document).ready(function(){
        $("#myform").validate();
        tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "desc",
            height: '320px',
            plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{csrf_token()}}');
                $.ajax({
                    url: '{{route("admin.artical.img.upload")}}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            }, 
        });
        $("#myform").submit(function (event) {
            if(tinyMCE.get('desc').getContent()==""){
                event.preventDefault();
                $("#cntnt").html("@lang('admin_lang.desc_req')").css('color','red');
            }
            else{
                $("#cntnt").html("");
            }
        });
    });


</script>


<style>
    .error{
        color: red !important;
    }
</style>

@endsection