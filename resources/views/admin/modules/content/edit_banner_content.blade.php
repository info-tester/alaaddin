@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Content') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | Edit Banner
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            <!-- @lang('admin_lang.e_commerce_dashboard_template') -->
                            Banner
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('admin.manage.bannerlist') }}" class="breadcrumb-link">Banner Management</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        Edit Banner
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                <!-- @lang('admin_lang.edit_city') -->
                                Edit Banner
                                <a class="adbtn btn btn-primary" href="{{ route('admin.manage.bannerlist') }}">Back</a>
                                <!-- <a class="adbtn btn btn-primary" href="">
                                    <i class="fas fa-less-than"></i>
                                    @lang('admin_lang.back')
                                </a> -->
                            </h5>
                            <div class="card-body">
                                

                                <form id="myform" method="post" enctype="multipart/form-data" action="{{ route('admin.manage.banner.details.post',@$pageid) }}">
                                    @csrf
                                    <div class="row">

                                        <!-- <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="inputText3" class="col-form-label">Display Order</label>
                                            <input id="inputText3" type="text" class="form-control" name="display_order" placeholder="Display order">
                                        </div> -->
                                        <!-- ===============first section=========== -->
                                        @if(@$language)
                                            @foreach(@$language as $key=>$lang)
                                                <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                    {{-- <label for="inputText1" class="col-form-label">@lang('admin_lang.banner_heading')</label> --}}
                                                    {{-- <input id="inputText1" type="text" class="form-control" name="banner_heading[]" placeholder="@lang('admin_lang.banner_heading')" value="{{ @$banner_details[$key]->banner_heading }}" required=""> --}}
                                                </div>
                                            @endforeach
                                        @endif 

                                        @if(@$language)
                                            @foreach(@$language as $key=>$lang)
                                                <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                    {{-- <label for="inputText2" class="col-form-label">@lang('admin_lang.banner_sub_heading')</label> --}}
                                                    {{-- <input id="inputText2" type="text" class="form-control" name="banner_sub_heading[]" placeholder="@lang('admin_lang.banner_sub_heading')" value="{{@$banner_details[$key]->banner_sub_heading}}" required=""> --}}
                                                </div>
                                            @endforeach
                                        @endif    

                                        @if(@$language)
                                            @foreach(@$language as $key=>$lang)
                                                <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                                    {{-- <label for="inputText3" class="col-form-label">@lang('admin_lang.banner_desc')</label> --}}
                                                    {{-- <textarea id="inputText3" type="text" style="white-space:unset;overflow-y:scroll;" class="form-control brandName brandNameBylanguage" placeholder='@lang('admin_lang.banner_desc')' name="banner_desc[]" required="">{!!strip_tags(@$banner_details[$key]->banner_desc) !!}</textarea> --}}
                                                    <span class="errorBrandName" style="color: red;"></span>
                                                </div>
                                            @endforeach
                                        @endif

                                        {{-- @if(@$language)
                                            @foreach(@$language as $key=>$lang)
                                                <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <label for="inputText3" class="col-form-label">@lang('admin_lang.banner_btn_caption')  [{{ $lang->name }}]</label>
                                                    <input id="inputText3" type="text" class="form-control " name="banner_buttton_cap[]" placeholder="@lang('admin_lang.banner_btn_caption')" value="{{ @$banner_details[$key]->banner_buttton_cap }}">
                                                </div>
                                            @endforeach
                                        @endif 

                                        @if(@$language)
                                            @foreach(@$language as $key=>$lang)
                                                <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <label for="inputText3" class="col-form-label">@lang('admin_lang.banner_btn_url')  [{{ $lang->name }}]</label>
                                                    <input id="inputText3" type="text" class="form-control required" name="banner_buttton_url[]" placeholder="@lang('admin_lang.banner_btn_url')" value="{{ @$banner_details[$key]->banner_buttton_url }}">
                                                </div>
                                            @endforeach
                                        @endif
 --}}
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 fstbtncls">
                                            <!-- <p for="" class="col-form-label">Category Picture</p> -->
                                            <input type="file" class="custom-file-input" id="customFile" name="banner_image" accept="image/jpg,image/jpeg,image/png">
                                            <label class="custom-file-label extrlft" for="customFile">
                                            @lang('admin_lang.banner_image')
                                            </label>
                                            <div class="errorpic" style="color: red;">
                                                
                                            </div>
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 fstbtncls">
                                            @if(@$banner_details[$key]->banner_image)
                                                @php
                                                    $image_path = 'storage/app/public/bannerimg/'.@$banner_details[$key]->banner_image; 
                                                @endphp
                                                @if(file_exists(@$image_path))
                                                    <img id="profilePicture" src="{{ URL::to('storage/app/public/bannerimg/'.$banner_details[$key]->banner_image) }}" alt="" style="width: 150px;">
                                                @endif
                                            @endif
                                        </div>

                                        

                                       

                                       
                                       
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <label for="inputPassword" class="col-form-label"></label>
                                            <input type="submit" class="btn btn-primary" value="submit">
                                        </div>
               
                                    </div>
                                </form>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script>
    $(document).ready(function(){
        $("#myform").validate();
    });
</script>
<script type="text/javascript">
    $("#customFile").change(function() {
        var filename = $.trim($("#customFile").val());
        if(filename != ""){
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".profile").show();
                $(".errorpic").text("");
                readURL(this);
            }else{
                $(".errorpic").html('@lang('validation.img_upload')');
                $('#profilePicture').attr('src', "");
            }
        } 
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#profilePicture').attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
      }
  }
</script>
<style>
    .error{
        color: red !important;
    }
</style>
@endsection