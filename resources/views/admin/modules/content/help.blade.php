@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | Admin | Help 
@endsection
@section('content')
@section('links')
@include('admin.includes.links')
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            Help
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        Help
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong></strong> {{ session('success') }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong></strong> {{ session('error') }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                Help
                            </h5>
                            <div class="card-body">
                                <form id="myform" method="post" enctype="multipart/form-data" action="{{ route('admin.get.help.page.post') }}">
                                    @csrf
                                    <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <label for="title" class="col-form-label"><span style="color: red;">*</span> Title</label>
                                        <input type="text" name="title" class="form-control required" id="title" placeholder="Title"  value="{{ @$help->title }}">
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <label for="description" class="col-form-label"><span style="color: red;">*</span> Description</label>
                                        <input type="text" name="description" class="form-control required" id="description" placeholder="description"  value="{{ @$help->description }}">
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <label for="c_title" class="col-form-label"><span style="color: red;">*</span> Chat Title</label>
                                        <input type="text" name="c_title" class="form-control required" id="description" placeholder="Chat Title"  value="{{ @$help->c_title }}">
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <label for="c_desc" class="col-form-label"><span style="color: red;">*</span> Chat Description</label>
                                        <input type="text" name="c_desc" class="form-control required" id="c_desc" placeholder="Chat Description"  value="{{ @$help->c_desc }}">
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <label for="f_title" class="col-form-label"><span style="color: red;">*</span>FAQ Title</label>
                                        <input type="text" name="f_title" class="form-control required" id="f_title" placeholder="FAQ Title"  value="{{ @$help->f_title }}">
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <label for="f_desc" class="col-form-label"><span style="color: red;">*</span>FAQ Description</label>
                                        <input type="text" name="f_desc" class="form-control required" id="f_desc" placeholder="FAQ description"  value="{{ @$help->f_desc }}">
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <label for="email_title" class="col-form-label"><span style="color: red;">*</span> Email Title</label>
                                        <input type="text" name="email_title" class="form-control required" id="email_title" placeholder="Email title"  value="{{ @$help->email_title }}">
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <label for="email_desc" class="col-form-label"><span style="color: red;">*</span> Email Description</label>
                                        <input type="text" name="email_desc" class="form-control required" id="email_desc" placeholder="Email Description"  value="{{ @$help->email_desc }}">
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <label for="title_1" class="col-form-label"><span style="color: red;">*</span>Title 1</label>
                                        <input type="text" name="title_1" class="form-control required" id="title_1" placeholder="title 1"  value="{{ @$help->title_1 }}">
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <label for="title_2" class="col-form-label"><span style="color: red;">*</span>Title 2</label>
                                        <input type="text" name="title_2" class="form-control required" id="title_2" placeholder=" title 2"  value="{{ @$help->title_2 }}">
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <label for="title_3" class="col-form-label"><span style="color: red;">*</span>Title 3</label>
                                        <input type="text" name="title_3" class="form-control required" id="title_3" placeholder=" title 3"  value="{{ @$help->title_3 }}">
                                    </div>
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 fstbtncls">
                                        <input type="file" class="custom-file-input" id="customFile" name="help_image" accept="image/jpg,image/jpeg,image/png">
                                        <label class="custom-file-label extrlft" for="customFile">
                                        Image
                                        </label>
                                        <div class="errorpic" style="color: red;">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 fstbtncls">
                                        @if(@$help->help_image)
                                            @php
                                                $image_path = 'storage/app/public/help/'.@$help->help_image; 
                                            @endphp
                                            @if(file_exists(@$image_path))
                                                <img id="profilePicture" style="max-height: 100%;max-width: 100%;" src="{{ URL::to('storage/app/public/help/'.@$help->help_image) }}" alt="">
                                            @endif
                                        @else
                                        <img id="profilePicture" style="max-height: 100%;max-width: 100%;" src="{{ URL::to('storage/app/public/help/'.@$help->help_image) }}" alt="">
                                        @endif
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top: 10px;">
                                        <div class="submit-login add_btnm">
                                            <input value="Submit" type="submit" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>
@endsection
@section('scripts')
@include('admin.includes.scripts')
<script src="{{ URL::to('public/frontend/tiny_mce/tinymce.min.js') }}"></script>
<style type="text/css">
    .upldd {
        display: block;
        width: auto;
        border-radius: 4px;
        text-align: center;
        background: #9caca9;
        cursor: pointer;
        overflow: hidden;
        padding: 10px 15px;
        font-size: 15px;
        color: #fff;
        cursor: pointer;
        float: left;
        margin-top: 15px;
        font-family: 'Poppins', sans-serif;
        position: relative;
    }
    .upldd input {
        position: absolute;
        font-size: 50px;
        opacity: 0;
        left: 0;
        top: 0;
        width: 200px;
        cursor: pointer;
    }
    .upldd:hover{
        background: #1781d2;
    }

</style>

<script>
    $(document).ready(function(){
        $("#myform").validate();
        tinyMCE.init({
            mode : "specific_textareas",
            editor_selector : "desc",
            height: '320px',
            plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: 'insertfile undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview | forecolor backcolor emoticons',
            images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
            images_upload_handler: function(blobInfo, success, failure) {
                var formD = new FormData();
                formD.append('file', blobInfo.blob(), blobInfo.filename());
                formD.append( "_token", '{{csrf_token()}}');
                $.ajax({
                    url: '{{route("admin.artical.img.upload")}}',
                    data: formD,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'JSON',
                    success: function(jsn) {
                        if(jsn.status == 'ERROR') {
                            failure(jsn.error);
                        } else if(jsn.status == 'SUCCESS') {
                            success(jsn.location);
                        }
                    }
                });
            }, 
        });
        $("#myform").submit(function (event) {
            if(tinyMCE.get('desc').getContent()==""){
                event.preventDefault();
                $("#cntnt").html("@lang('admin_lang.desc_req')").css('color','red');
            }
            else{
                $("#cntnt").html("");
            }
        });
    });
</script>
<style>
    .error{
        color: red !important;
    }
</style>
<script type="text/javascript">
    $("#customFile").change(function() {
        var filename = $.trim($("#customFile").val());
        if(filename != ""){
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".profile").show();
                $(".errorpic").text("");
                readURL(this);
            }else{
                $(".errorpic").html('@lang('validation.img_upload')');
                $('#profilePicture').attr('src', "");
            }
        } 
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#profilePicture').attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
      }
  }
</script>
@endsection