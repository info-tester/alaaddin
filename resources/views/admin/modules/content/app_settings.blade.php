@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Content') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | App Settings
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            Alaaddin @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">App Settings</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <form id="app_settings"  method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                            <label for="app_desc_arabic_label" class="col-form-label">IOS Version</label>
                                            <input type="text" name="ios_version" class="form-control required" placeholder="IOS version"  value="{{@$app_settings->ios_version}}">
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                            <label for="app_desc_arabic_label" class="col-form-label">IOS Version Check</label>
                                            <select name="ios_version_check" class="form-control required">
                                                <option value="Y" @if(@$app_settings->ios_version_check == 'Y') selected="" @endif>YES</option>
                                                <option value="N" @if(@$app_settings->ios_version_check == 'N') selected="" @endif>NO</option>
                                            </select>
                                            
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                            <label for="app_desc_arabic_label" class="col-form-label">Message for IOS</label>
                                            <input type="text" name="message_for_ios" class="form-control required" placeholder="Message for IOS"  value="{{@$app_settings->message_for_ios}}">
                                        </div>

                                        <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                            <label for="app_desc_arabic_label" class="col-form-label">Android Version</label>
                                            <input type="text" name="android_version" class="form-control required" placeholder="Android version"  value="{{@$app_settings->android_version}}">
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                            <label for="app_desc_arabic_label" class="col-form-label">Android Version Check</label>
                                            <select name="android_version_check" class="form-control required">
                                                <option value="Y" @if(@$app_settings->android_version_check == 'Y') selected="" @endif>YES</option>
                                                <option value="N" @if(@$app_settings->android_version_check == 'N') selected="" @endif>NO</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                            <label for="app_desc_arabic_label" class="col-form-label">Message for Android</label>
                                            <input type="text" name="message_for_android" class="form-control required" placeholder="Message for Android"  value="{{@$app_settings->message_for_android}}">
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top: 10px;">
                                            <div class="submit-login add_btnm">
                                                <input value="submit" type="submit" class="btn btn-primary">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!--all_time_sho-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>

@endsection
@section('scripts')
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif

<script>
    $(document).ready(function(){
        $("#app_settings").validate({
            errorPlacement: function() {

            }
        });        
    });
</script>


<style>
    .error{
        color: red !important;
    }
</style>

@endsection