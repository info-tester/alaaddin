@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Content') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | Contact Us
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.contact_us')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                <!-- @lang('admin_lang.edit_city') -->
                                @lang('admin_lang.contact_us')
                                <!-- <a class="adbtn btn btn-primary" href="">
                                    <i class="fas fa-less-than"></i>
                                    @lang('admin_lang.back')
                                </a> -->
                            </h5>
                            <div class="card-body">
                                <form id="myform" method="post" enctype="multipart/form-data" action="{{ route('admin.manage.pagecontactadd') }}">
                                    @csrf
                                    <div class="row">  
                                    {{-- @if(@$language)
                                        @foreach(@$language as $key=>$lang)
                                            <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                                <label for="inputText3" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.contact_us_headlin') [{{ $lang->name }}]</label>
                                                <input type="text" name="sec_heading_contact[]" class="form-control required" id="title" placeholder="@lang('admin_lang.contact_us_headlin')"  value="{{ @$contact_content[$key]->sec_heading_contact }}">
                                            </div>
                                        @endforeach
                                    @endif   

                                    @if(@$language)
                                        @foreach(@$language as $key=>$lang)
                                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                                
                                                <label for="inputText3" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.box1_heading') [{{ $lang->name }}]</label>
                                                <input type="text" name="first_sec_first_title[]" class="form-control required" id="title" placeholder="@lang('admin_lang.box1_heading')"  value="{{ @$contact_content[$key]->first_sec_first_title }}">
                                                
                                            </div>
                                        @endforeach
                                    @endif 

                                    @if(@$language)
                                        @foreach(@$language as $key=>$lang)
                                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div class="form-group">
                                                    <label class="personal-label" for="exampleInputEmail1">@lang('admin_lang.box1_description') [{{ $lang->name }}]</label>
                                                    <div class="clearfix"></div>
                                                    <textarea name="first_sec_first_content[]" id="desc" style="white-space:unset;overflow-y:scroll;" rows="10" placeholder=">@lang('admin_lang.box1_description')" style="width:100%" class="form-control required desc">{{ strip_tags(@$contact_content[$key]->first_sec_first_content) }}</textarea>
                                                </div>
                                                <div class="clearfix"></div>
                                                <p class="error_1" id="cntnt"></p>
                                                <div class="clearfix"></div>
                                            </div>    
                                        @endforeach
                                    @endif 

                                    @if(@$language)
                                        @foreach(@$language as $key=>$lang)
                                            <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                                
                                                <label for="inputText3" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.box1_buttan_caption') [{{ $lang->name }}]</label>
                                                <input type="text" name="boxone_hed_caption[]" class="form-control required" id="title" placeholder="@lang('admin_lang.box1_buttan_caption')"  value="{{ @$contact_content[$key]->boxone_hed_caption }}">
                                                
                                            </div>
                                        @endforeach
                                    @endif

                                     @if(@$language)
                                        @foreach(@$language as $key=>$lang)
                                            <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                                
                                                <label for="inputText3" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.box1_buttan_link') [{{ $lang->name }}]</label>
                                                <input type="text" name="boxone_butn_link[]" class="form-control required" id="title" placeholder="@lang('admin_lang.box1_buttan_link')"  value="{{ @$contact_content[$key]->boxone_butn_link }}">
                                                
                                            </div>
                                        @endforeach
                                    @endif
                                    --}}
                                    <!-- ====2==== -->

                                    @if(@$language)
                                    @foreach(@$language as $key=>$lang)
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">

                                        <label for="inputText3" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.box1_heading')</label>
                                        <input type="text" name="first_sec_second_heading[]" class="form-control required" id="title" placeholder="@lang('admin_lang.box2_heading')"  value="{{ @$contact_content[$key]->first_sec_second_heading }}" required="">

                                    </div>
                                    @endforeach
                                    @endif 

                                    @if(@$language)
                                    @foreach(@$language as $key=>$lang)
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label class="personal-label" for="exampleInputEmail1">@lang('admin_lang.box1_description')</label>
                                            <div class="clearfix"></div>
                                            <textarea name="first_sec_second_content[]" id="desc" style="white-space:unset;overflow-y:scroll;" rows="10" placeholder=">@lang('admin_lang.box2_description')" style="width:100%" class="form-control required desc">{{ strip_tags(@$contact_content[$key]->first_sec_second_content) }}</textarea>
                                        </div>
                                        <div class="clearfix"></div>
                                        <p class="error_1" id="cntnt"></p>
                                        <div class="clearfix"></div>
                                    </div>    
                                    @endforeach
                                    @endif 


                                    {{-- @if(@$language)
                                        @foreach(@$language as $key=>$lang)
                                            <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                                <label for="inputText3" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.box2_buttan_caption') [{{ $lang->name }}]</label>
                                                <input type="text" name="boxtwo_hed_caption[]" class="form-control required" id="title" placeholder="@lang('admin_lang.box2_buttan_caption')"  value="{{ @$contact_content[$key]->boxtwo_hed_caption }}">
                                            </div>
                                        @endforeach
                                        @endif --}}

                                    {{--  @if(@$language)
                                        @foreach(@$language as $key=>$lang)
                                            <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                                <label for="inputText3" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.box2_buttan_link') [{{ $lang->name }}]</label>
                                                <input type="text" name="boxtwo_butn_link[]" class="form-control required" id="title" placeholder="@lang('admin_lang.box2_buttan_link')"  value="{{ @$contact_content[$key]->boxtwo_butn_link }}">
                                            </div>
                                        @endforeach
                                        @endif  --}}


                                        <!-- =====3===== -->

                                        @if(@$language)
                                        @foreach(@$language as $key=>$lang)
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                            <label for="inputText3" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.box3_heading')</label>
                                            <input type="text" name="first_sec_third_heading[]" class="form-control required" id="title" placeholder="@lang('admin_lang.box3_heading')"  value="{{ @$contact_content[$key]->first_sec_third_heading }}">
                                        </div>
                                        @endforeach
                                        @endif 


                                        @if(@$language)
                                        @foreach(@$language as $key=>$lang)
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="form-group">
                                                <label class="personal-label" for="exampleInputEmail1">@lang('admin_lang.box3_description')</label>
                                                <div class="clearfix"></div>
                                                <textarea name="first_sec_third_content[]" id="desc" style="white-space:unset;overflow-y:scroll;" rows="10" placeholder="@lang('admin_lang.box3_description')" style="width:100%" class="form-control required desc">{{ strip_tags(@$contact_content[$key]->first_sec_third_content) }}</textarea>
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="error_1" id="cntnt"></p>
                                            <div class="clearfix"></div>
                                        </div>    
                                        @endforeach
                                        @endif
                                        @if(@$language)
                                        @foreach(@$language as $key=>$lang)
                                        <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">

                                            <label for="inputText3" class="col-form-label">Facebook Link </label>
                                            <input type="text" name="facebook_link[]" class="form-control" id="facebook_link" placeholder="Facebook Link"  value="{{ @$contact_content[$key]->facebook_link }}">

                                        </div>
                                        @endforeach
                                        @endif 
                                        @if(@$language)
                                        @foreach(@$language as $key=>$lang)
                                        <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">

                                            <label for="inputText3" class="col-form-label">Twitter Link </label>
                                            <input type="text" name="twitter_link[]" class="form-control" id="twitter_link" placeholder="Twitter Link"  value="{{ @$contact_content[$key]->twitter_link }}">

                                        </div>
                                        @endforeach
                                        @endif 
                                        @if(@$language)
                                        @foreach(@$language as $key=>$lang)
                                        <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">

                                            <label for="inputText3" class="col-form-label">Linkedin Link </label>
                                            <input type="text" name="linkedin_link[]" class="form-control" id="linkedin_link" placeholder="Linkedin Link"  value="{{ @$contact_content[$key]->linkedin_link }}">

                                        </div>
                                        @endforeach
                                        @endif
                                        <!--  =====3==== -->
                                        @if(@$language)
                                        @foreach(@$language as $key=>$lang)
                                        <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">

                                            <label for="inputText3" class="col-form-label">Youtube Link </label>
                                            <input type="text" name="youtube_link[]" class="form-control" id="youtube_link" placeholder="Youtube Link"  value="{{ @$contact_content[$key]->youtube_link }}">

                                        </div>
                                        @endforeach
                                        @endif

                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-top: 10px;">
                                            <div class="submit-login add_btnm">
                                                <input value="submit" type="submit" class="btn btn-primary">
                                            </div>
                                        </div>
                                        <!--all_time_sho-->

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif

<script src="{{ URL::to('public/frontend/tiny_mce/tinymce.min.js') }}"></script>

<style type="text/css">
    .upldd {
        display: block;
        width: auto;
        border-radius: 4px;
        text-align: center;
        background: #9caca9;
        cursor: pointer;
        overflow: hidden;
        padding: 10px 15px;
        font-size: 15px;
        color: #fff;
        cursor: pointer;
        float: left;
        margin-top: 15px;
        font-family: 'Poppins', sans-serif;
        position: relative;
    }
    .upldd input {
        position: absolute;
        font-size: 50px;
        opacity: 0;
        left: 0;
        top: 0;
        width: 200px;
        cursor: pointer;
    }
    .upldd:hover{
        background: #1781d2;
    }

</style>

<script>
    $(document).ready(function(){
        jQuery.validator.addMethod("validateFacebookUrl", function(value, element){
            var p = /^(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?$/;
            return this.optional(element) || (value.match(p));
        }, "Please provide a valid facebook link."); 
        jQuery.validator.addMethod("validatetwitterUrl", function(value, element){
            var p = /^(?:(?:http|https):\/\/)?(?:www.)?twitter.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?$/;
            return this.optional(element) || (value.match(p));
        }, "Please provide a valid twitter link."); 
        jQuery.validator.addMethod("validatelinkedinUrl", function(value, element){
            var p = /(ftp|http|https):\/\/?(?:www\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
            return this.optional(element) || (value.match(p));
        }, "Please provide a valid linkedin link.");
        jQuery.validator.addMethod("validateyoutubeUrl", function(value, element){
            var p = /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
            return this.optional(element) || (value.match(p));
        }, "Please provide a valid youtube link.");
        $("#myform").validate({
            rules:{
                'facebook_link[]' : {
                    validateFacebookUrl:true,
                },
                'twitter_link[]' : {
                    validatetwitterUrl:true,
                },
                'linkedin_link[]' : {
                    validatelinkedinUrl:true,
                },
                'youtube_link[]' : {
                    validateyoutubeUrl:true,
                }
            }
        });
    });
</script>


<style>
    .error{
        color: red !important;
    }
</style>

@endsection