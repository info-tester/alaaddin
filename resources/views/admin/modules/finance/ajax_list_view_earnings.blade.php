<table class="table table-striped table-bordered first" id="myTable">
    <thead>
        <tr>
            <th>@lang('admin_lang.seller')</th>
            <th>@lang('admin_lang.tot_earn_usd')</th>
            <th>@lang('admin_lang.tot_com_usd')</th>
            <th>@lang('admin_lang.tot_due_usd')</th>
            <th>@lang('admin_lang.tot_payout_usd')</th>
            <th>@lang('admin_lang.Action')</th>
        </tr>
    </thead>
    <tbody>
        @if(@$finance)
            @foreach(@$finance as $earning)
                <tr>
                    <td>{{ $earning->fname." ".$earning->lname }}</td>
                    <td>{{ $earning->total_earning }}</td>
                    <td>{{ $earning->total_commission }}</td>
                    <td>{{ $earning->total_due }}</td>
                    <td>{{ $earning->total_paid }}</td>
                    <td>
                        <a href="javascript:void(0)"><i class=" fas fa-eye" title='@lang('admin_lang.view')'></i></a>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
    <tfoot>
        <tr>
            <th>@lang('admin_lang.seller')</th>
            <th>@lang('admin_lang.tot_earn_usd')</th>
            <th>@lang('admin_lang.tot_com_usd')</th>
            <th>@lang('admin_lang.tot_due_usd')</th>
            <th>@lang('admin_lang.tot_payout_usd')</th>
            <th>@lang('admin_lang.Action')</th>
        </tr>
    </tfoot>
</table>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>