@extends('admin.layouts.app')
@section('title', 'Alaaddin | Admin | View Customer Profile')
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" href="{{ asset('public/admin/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
<link href="{{ asset('public/admin/assets/vendor/fonts/circular-std/style.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('public/admin/assets/libs/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('public/admin/assets/vendor/fonts/fontawesome/css/fontawesome-all.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/buttons.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/select.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/fixedHeader.bootstrap4.css') }}">
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== --> 
            <!-- pageheader --> 
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">View Customer Profile</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link"> Manage Customer</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">View Customer Profile</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== --> 
            <!-- end pageheader --> 
            <!-- ============================================================== -->
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="row">
                <!-- ============================================================== --> 
                <!-- basic table  --> 
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">View Customer Profile </h5>
                        <div class="card-body">
                            <div class="manager-dtls tsk-div">
                                <div class="row fld">
                                    <div class="col-md-12">
                                        <h4 class="fultxt" style="margin-bottom:10px;">Customer Details</h4>
                                    </div>
                                    <div class="col-md-2" style="padding-right:0px;">
                                        @if(@$customer->image)
                                        @php
                                        $image_path = 'storage/app/public/customer/profile_pics/'.@$customer->image; 
                                        @endphp
                                        @if(file_exists(@$image_path))
                                        <div class="uplodpic">
                                            <li>
                                                <img src="{{ URL::to('storage/app/public/customer/profile_pics/'.@$customer->image) }}" style="width: 100px;height: 100px;">
                                            </li>
                                        </div>
                                        
                                        @endif
                                        @endif
                                        

                                    </div>
                                    <div class="col-md-6" style="padding-left:0px;">
                                        <p><span class="titel-span">Name </span> <span class="deta-span"><strong>:</strong> {{ @$customer->fname." ".@$customer->lname }}</span> </p>
                                        <p><span class="titel-span">Email Address</span> <span class="deta-span"><strong>:</strong> {{ @$customer->email }}</span> </p>
                                        <p><span class="titel-span">Phone Number</span> <span class="deta-span"><strong>:</strong> {{ @$customer->phone }}</span></p>
                                        <p><span class="titel-span">Total Order</span> <span class="deta-span"><strong>:</strong> </span></p>
                                    </div>
                                </div>
                            </div>
                            <h4 class="fultxt" style="margin-bottom:10px;">Order of this Customer :</h4>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered first">
                                    <thead>
                                        <tr>
                                            <th>#Order</th>
                                            <th>Date</th>
                                            <th>Buyer</th>
                                            <th>Order Total</th>
                                            <th>#Item</th>
                                            <th>Shipping</th>
                                            <th>Subtotal</th>
                                            <th>Total</th>
                                            <th>Payment</th>
                                            <th>Status</th>
                                            <th>Driver</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- <tr>
                                            <td>#G24534452</td>
                                            <td>2020-02-10</td>
                                            <td>{{ @$customer->fname." ".@$customer->lname }}</td>
                                            <td>$600</td>
                                            <td>5</td>
                                            <td>$20</td>
                                            <td>$880</td>
                                            <td>$900</td>
                                            <td>COD</td>
                                            <td>Active</td>
                                            <td>Rohit Roy</td>
                                            <td><a href=""><i class=" fas fa-eye" title="Edit"></i></a> <a href=""><i class="far fa-times-circle" title="Cancel"></i></a></td>
                                        </tr> --}}
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#Order</th>
                                            <th>Date</th>
                                            <th>Buyer</th>
                                            <th>Order Total</th>
                                            <th>#Item</th>
                                            <th>Shipping</th>
                                            <th>Subtotal</th>
                                            <th>Total</th>
                                            <th>Payment</th>
                                            <th>Status</th>
                                            <th>Driver</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== --> 
                <!-- end basic table  --> 
                <!-- ============================================================== --> 
            </div>
        </div>
        
        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif

<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/slimscroll/jquery.slimscroll.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/multi-select/js/jquery.multi-select.js') }}"></script> 
<script src="{{ asset('public/admin/assets/libs/js/main-js.js') }}"></script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>

@endsection