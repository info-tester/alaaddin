@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.view_earnings')
@endsection
@section('content')
@section('links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@include('admin.includes.links')
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">@lang('admin_lang.view_earnings')</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.view_earnings')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="mail_success" style="display: none;">
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
            </div>
        </div>
        <div class="mail_error" style="display: none;">
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>Error!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
            </div>
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
        </div>
        @elseif ((session()->has('error')))
        <div class="alert alert-danger vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong>{{ session('error')['code']['message'] }}</strong> {{ session('error')['meaning'] }} 
        </div>
        @endif
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">@lang('admin_lang.view_earnings')</h5>
                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="inputText3" class="col-form-label">@lang('admin_lang.Keywords') </label>
                                    <input id="col1_filter" type="text" class="form-control keyword" placeholder="Keywords">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <a href="javascript:void(0)" id="search_earning" class="btn btn-primary fstbtncls">@lang('admin_lang.Search')</a>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive listBody">
                            <table class="table table-striped table-bordered" id="finance">
                                <thead>
                                    <tr>
                                        <th>@lang('admin_lang.merchant_name')</th>
                                        <th>@lang('admin_lang.merchant_email_id')</th>
                                        <th>@lang('admin_lang.merchant_phone_number')</th>
                                        <th>@lang('admin_lang.total_earning_kwd')</th>
                                        <th>@lang('admin_lang.total_commission')</th>
                                        <th>@lang('admin_lang.total_due_kwd')</th>
                                        <th>@lang('admin_lang.total_payout_kwd')</th>
                                        {{-- <th>@lang('admin_lang.invoice_in')</th> --}}
                                        {{-- <th>@lang('admin_lang.invoice_out')</th> --}}
                                        {{-- <th>@lang('admin_lang.Action')</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @if(@$finance)
                                    @foreach(@$finance as $earning)
                                    <tr>
                                        <td>{{ $earning->fname." ".$earning->lname }}</td>
                                        <td>{{ $earning->email }}</td>
                                        <td>{{ $earning->phone }}</td>
                                        <td>{{ $earning->total_earning }}</td>
                                        <td>{{ $earning->total_commission }}</td>
                                        <td>{{ $earning->total_due }}</td>
                                        <td>{{ $earning->total_paid }}</td>
                                        <td>
                                            <form id="view_dtls_form{{ @$earning->id }}" method="post" action="{{ route('admin.list.order') }}">
                                                @csrf
                                                <input type="hidden" id="id" name="id" value="{{ @$earning->id }}"/>
                                                <a href="javascript:void(0)" data-id="{{ @$earning->id }}" class="view_dtls"><i class=" fas fa-eye" title='@lang('admin_lang.view_ords')'></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif --}}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>@lang('admin_lang.merchant_name')</th>
                                        <th>@lang('admin_lang.merchant_email_id')</th>
                                        <th>@lang('admin_lang.merchant_phone_number')</th>
                                        <th>@lang('admin_lang.total_earning_kwd')</th>
                                        <th>@lang('admin_lang.total_commission')</th>
                                        <th>@lang('admin_lang.total_due_kwd')</th>
                                        <th>@lang('admin_lang.total_payout_kwd')</th>
                                        {{-- <th>@lang('admin_lang.invoice_in')</th> --}}
                                        {{-- <th>@lang('admin_lang.invoice_out')</th> --}}
                                        {{-- <th>@lang('admin_lang.Action')</th> --}}
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('admin.includes.footer')
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
    <div class="loader" style="display: none;">
        <img src="{{url('public/loader.gif')}}">
    </div>
</div>
</div>
<div class="modal fade" id="exampleModalCenter">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.notes')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <!-- <label>Notes</label> -->
                                <textarea class="form-control h-80px" rows="2" id="comment"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" class="btn btn-primary popbtntp">@lang('admin_lang.save')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
    
        $('body').on('click', '.reset_search', function() {
            $('#finance').DataTable().search('').columns().search('').draw();
            $(".slct").val("").trigger("chosen:updated");
        })
    
        function filterColumn ( i ) {
            $('#finance').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }
        $("#finance_filter").hide();
            var styles = `
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
`
var styleSheet = document.createElement("style");
styleSheet.innerText = styles;
document.head.appendChild(styleSheet);
        $('#finance').DataTable( {
            stateSave: true,
            "order": [[0, "asc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
                $('#col2_filter').val(data.search.status)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
                data.search.status = $('#col2_filter').val()
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('admin.finance') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
                { 
                    data: 0,
                    render: function(data, type, full) {
                        return full.fname+' '+ full.lname
                    }
                },
                {   data: 'email' },
                {   data: 'phone' },
                {   data: 'total_earning' },
                {   data: 'total_commission' },
                {   data: 'total_due' },
                {   data: 'total_paid' },
                // {   data: 'invoice_in' },
                // {   data: 'invoice_out' },
                /*{   
                    data: 'status',
                    render: function(data, type, full) {
                        var a = '';
                        
                        return a;
                    }
                }*/
            ]
        });
    
        // change event
        $('input.keyword').on( 'keyup click', function () {
            filterColumn( 1 );
        });
        $('.status').on( 'change', function () {
            filterColumn( 2 );
        });
    
        $(".view_dtls").click(function(e){
            var id = $(this).attr("data-id");
            $("#view_dtls_form"+id).submit();
        });
        /*$("#keyword").on("keyup", function(e) {
            var value = $(this).val().toLowerCase();
            $("#myTable tbody tr").filter(function(){
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
          });
        });
        $("#search_earning").click(function(){
            var keyword = $.trim($("#keyword").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.search.earnings') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    keyword:keyword
                },
                success:function(resp){
                    if(resp != 0){
                        $(".listBody").html(resp);
                        $(".loader").hide();
                    }
                    
                }
            });
        });
        $("#keyword").blur(function(){
            var keyword = $.trim($("#keyword").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.search.earnings') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    keyword:keyword
                },
                 beforeSend:function(){
                        $(".loader").show();
                    },
                success:function(resp){
                    if(resp != 0){
                        $(".listBody").html(resp);
                        $(".loader").hide();
                    }
                }
            });
        });*/
    
    });
</script>
@endsection