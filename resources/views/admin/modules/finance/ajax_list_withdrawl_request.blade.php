<table class="table table-striped table-bordered first" id="myTable">
                                <thead>
                                    <tr>
                                        <th>@lang('admin_lang.sl_no')</th>
                                        <th>@lang('admin_lang.request_id')</th>
                                        <th>@lang('admin_lang.request_date')</th>
                                        <th>@lang('admin_lang.payment_date')</th>
                                        <th>@lang('admin_lang.seller')</th>
                                        <th>@lang('admin_lang.seller') @lang('admin_lang.email_id_addr')</th>
                                        <th>@lang('admin_lang.seller') @lang('admin_lang.phone_number')</th>
                                        <!-- <th>Seller Comission in %</th> -->
                                        <!-- <th>Seller specific order subtotal (KWD)</th> -->
                                        <!-- <th>Seller commission received(in merchant) (KWD)</th> -->
                                        <th>@lang('admin_lang.total_payout_kwd')</th>
                                        <th>@lang('admin_lang.requested_withdrawl') (Rs)</th>
                                        <th>@lang('admin_lang.balence_amt')(Rs)</th>
                                        <th>@lang('admin_lang.Status')</th>
                                        <th>@lang('admin_lang.notes')</th>
                                        <th>@lang('admin_lang.Action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(@$withdraw)
                                    @foreach(@$withdraw as $key=>$w)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $w->id }}</td>
                                        <td>{{ $w->request_date }}</td>
                                        <td>{{ $w->payment_date }}</td>
                                        <td>{{ $w->merchantDetails->fname." ".$w->merchantDetails->lname }}</td>
                                        <td>{{ $w->merchantDetails->email }}</td>
                                        <td>{{ $w->merchantDetails->phone }}</td>
                                        <!-- <td>{{ $w->merchantDetails->commission }} %</td> -->
                                        <!-- <td>{{ $w->merchantDetails->total_earning }}</td> -->
                                        <!-- <td>{{ $w->merchantDetails->total_commission }}</td> -->
                                        <td>{{ $w->merchantDetails->total_paid }}</td>
                                        <td>{{ $w->amount }}</td>
                                        <td>{{ $w->balance }}</td>
                                        
                                        <td>
                                            @if($w->status == 'N')
                                            @lang('admin_lang.new')
                                            @elseif($w->status == 'S')
                                            @lang('admin_lang.success')
                                            @elseif($w->status == 'C')
                                            @lang('admin_lang.Cancelled_1')
                                            @endif
                                            {{-- {{ $withdraw->status }} --}}
                                        </td>
                                        <td>
                                            {!! strip_tags(@$w->description) !!}
                                        </td>
                                        <td>
                                            {{-- <a href="javascript:void(0)"><i class=" fas fa-eye" title="View"></i></a> --}}
                                            @if($w->status == 'N')
                                            <a href="javascript:void(0)" data-toggle="modal" class="approve_request" data-action="{{ route('admin.approve.request',$w->id) }}" id="approve_request">

                                            <i class="fas fa-thumbs-up" title="@lang('admin_lang.approve_request')"></i></a>

                                            <a href="javascript:void(0)" data-toggle="modal" data-id="" data-action="{{ route('admin.reject.request',@$w->id) }}" id="reject_request_notes" class="reject_request_notes">

                                            <i class="fas fa-ban" title="@lang('admin_lang.reject_request')"></i>
                                            </a>
                                            @endif 
                                            
                                            <a href="{{ route('admin.view.merchant.profile',@$w->seller_id) }}"><i class="fa fa-info" title="@lang('admin_lang.seller_dtlss')"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>@lang('admin_lang.sl_no')</th>
                                        <th>@lang('admin_lang.request_id')</th>
                                        <th>@lang('admin_lang.request_date')</th>
                                        <th>@lang('admin_lang.payment_date')</th>
                                        <th>@lang('admin_lang.seller')</th>
                                        <th>@lang('admin_lang.seller') @lang('admin_lang.email_id_addr')</th>
                                        <th>@lang('admin_lang.seller') @lang('admin_lang.phone_number')</th>
                                        <!-- <th>Seller Comission in %</th> -->
                                        <!-- <th>Seller specific order subtotal (KWD)</th> -->
                                        <!-- <th>Seller commission received(in merchant) (KWD)</th> -->
                                        <th>@lang('admin_lang.total_payout_kwd')</th>
                                        <th>@lang('admin_lang.requested_withdrawl') (Rs)</th>
                                        <th>@lang('admin_lang.balence_amt')(Rs)</th>
                                        <th>@lang('admin_lang.Status')</th>
                                        <th>@lang('admin_lang.notes')</th>
                                        <th>@lang('admin_lang.Action')</th>
                                    </tr>
                                </tfoot>
                            </table>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>