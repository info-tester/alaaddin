@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }}| Admin | Manage Unit
@endsection
@section('content')
@section('links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@include('admin.includes.links')

@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">Manage Unit </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Manage Unit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="mail_success" style="display: none;">
                <div class="alert alert-success vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
                </div>
            </div>
            <div class="mail_error" style="display: none;">
                <div class="alert alert-danger vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>@lang('admin_lang.error')!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
                </div>
            </div>
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden session_success_div" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden session_error_div" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['code']['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong class="success_msg"></strong> 
            </div>
            <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong class="error_msg"></strong> 
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <!-- ============================================================== -->
                <!-- basic table  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        {{-- <h5 class="card-header">
                                Add Unit
                                <a class="adbtn btn btn-primary" href="{{ route('admin.add.unit') }}">
                                    <i class="fas fa-less-than"></i>
                                    Add
                                </a>
                            </h5> --}}
                        <div class="card-body">
                            
                            
                                <div class="row">
                                    
                                    {{-- <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="keyword_lb" class="col-form-label">Keyword</label>
                                        <input id="col1_filter" name="keyword" type="text" class="form-control keyword" placeholder="Search unit name" value="">
                                    </div> --}}
                                    {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <a id="search" href="javascript:void(0)" class="btn btn-primary fstbtncls">Search</a>
                                        <input type="reset" value="Reset Search" class="btn btn-default fstbtncls reset_search">
                                    </div> --}}
                                </div>
                            

                            <div class="table-responsive listBody">
                                <table class="table table-striped table-bordered first" id="myTable2">
                                    <thead>
                                        <tr>
                                            <th>Unit Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Unit Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end basic table  -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
        <div class="loader" style="display: none;">
            <img src="{{url('public/loader.gif')}}">
        </div>
        @endsection
        @section('scripts')
        @include('admin.includes.scripts')
        <script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                // datatable
                function filterColumn ( i ) {
                    $('#myTable2').DataTable().column( i ).search(
                        $('#col'+i+'_filter').val(),
                    ).draw();
                }
                $("#merchants_filter").hide();
                var styles = `
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
`
var styleSheet = document.createElement("style");
styleSheet.innerText = styles;
document.head.appendChild(styleSheet);
                $('#myTable2').DataTable( {
                    stateSave: false,
                    sDom: 'lrtip',
                    "order": [[1, "asc"]],
                    "stateLoadParams": function (settings, data) {
                        
                    },
                    stateSaveParams: function (settings, data) {
                        
                    },
                    "processing": true,
                    "serverSide": true,
                    'serverMethod': 'post',
                    "ajax": {
                        "url": "{{ route('admin.list.unit') }}",
                        "data": function ( d ) {
                            d._token = "{{ @csrf_token() }}";
                        }
                    },
                    'columns': [
                        { 
                            orderable: false,
                            render: function(data, type, full) {
                                return full.unit_name;
                            }
                        },
                        {  
                            orderable: false,
                            data: "id" ,
                            render: function(data, type, full) {
                                var a = '';
                                 a += '<a class="unit_master" href="{{ url('admin/edit-unit') }}/'+full.id+'" data-id="'+full.id+'" data-status="'+full.status+'" title="Unit"><i class="icon_change_'+full.id+' fa fa-pencil"></i></a>'; 
                                return a;
                            }
                        },
                    ],
                });
                // change event
                $('input.keyword').on( 'keyup click blur', function () {
                    filterColumn( 1 );
                });
                $('select.state').on( 'change', function () {
                    filterColumn( 2 );
                });
                $('body').on('click', '.reset_search', function() {
                    $('#myTable2').DataTable().search('').columns().search('').draw();
                });
                $(document).on('click', '#search', function() {
                    $('#myTable2').DataTable().draw();
                });
            });
        </script>
@endsection