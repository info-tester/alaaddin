@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Add Customer') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.edit_time_slot')
@endsection
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.edit_time_slot')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                @lang('admin_lang.edit_time_slot')
                            </h5>
                            <div class="card-body">
                                <form id="addTimeForm" method="post"  action="{{ route('admin.update.time.slot') }}">
                                    @csrf
                                    <input type="hidden" name="id" value="{{ @$time_data->id }}">
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="from_time" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.from_time')</label>
                                            <input id="from_time" name="from_time" type="text" class="form-control required timepicker" placeholder="@lang('admin_lang.from_time')" value="{{ @$time_data->from_time }}">
                                            <span class="error_from_time" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="to_time" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.to_time')</label>
                                            <input id="to_time" name="to_time" type="text" class="form-control required timepicker" placeholder="@lang('admin_lang.to_time')" value="{{ @$time_data->to_time }}">
                                            <span class="error_to_time" style="color: red;"></span>
                                        </div>

                                        <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">

                                            <a href="javascript:void(0)" id="saveTime" class="btn btn-primary ">@lang('admin_lang.save')</a>
                                        </div>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.timepicker').timepicker();

        $("#saveTime").click(function(){
            var from_time = $('#from_time').val();
            var to_time = $('#to_time').val();
            var id = "{{ @$time_data->id }}"
            var reqData = {
                '_token' : "{{ csrf_token() }}",
                'jsonrpc' : "2.0",
                'data'   :{
                    from_time: from_time,
                    to_time: to_time,
                    id:id
                }
            }

            $.ajax({
                    type:"POST",
                    url:"{{ route('admin.check.time.range') }}",
                    data:reqData,
                    success:function(resp){
                        if(resp.status == 1){
                            $("#addTimeForm").submit();
                            
                        }else{
                            $("#from_time").val("");
                            $("#to_time").val("");
                            $(".error_from_time").text('@lang('admin_lang.range_already_exist')');
                            $(".error_to_time").text('@lang('admin_lang.range_already_exist')');
                        }
                    }
                });
        });
        $("#addTimeForm").validate({
            rules:{
                'from_time':{
                    required:true
                },
                "to_time":{
                    required:true
                },
                
            },
            messages: { 
                "from_time": { 
                    required: "@lang('validation.required')"
                },
                "to_time": { 
                    required: "@lang('validation.required')"
                },
                
            },
            errorPlacement: function (error, element) 
            {
                    if (element.attr("name") == "from_time") {
                        var error = '<label for="from_time" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                        $('.error_from_time').html(error);
                    }
                    if (element.attr("name") == "to_time") {
                        var error = '<label for="to_time" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                        $('.error_to_time').html(error);
                    }
                    
            }
        });
    
    });
</script>

@endsection