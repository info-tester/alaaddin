@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage Merchant') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.show_address_book')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style type="text/css">
    
    #merchants_filter{
        display: none;
    }

    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
</style>
<style type="text/css">
    .action_col{
        display: inline-block;
        width: 310px;
    }
    .action_col a{
        margin: 2px;
        float: left;
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
{{-- content --}}
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">@lang('admin_lang.manage_sub_admin')</h2>
                <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                @lang('admin_lang.show_address_book')
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="mail_success" style="display: none;">
        <div class="alert alert-success vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
        </div>
    </div>
    <div class="mail_error" style="display: none;">
        <div class="alert alert-danger vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong>Error!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
        </div>
    </div>
    @if (session()->has('success'))
    <div class="alert alert-success vd_hidden" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
    </div>
    @elseif ((session()->has('error')))
    <div class="alert alert-danger vd_hidden" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
    </div>
    @endif
    <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong class="success_msg">Success!</strong> 
    </div>
    <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong class="error_msg"></strong> 
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">
                    @lang('admin_lang.show_address_book')
                    <a class="adbtn btn btn-primary" href="{{ route('admin.add.merchant.address',['id' => request()->segment(3)]) }}"><i class="fas fa-plus"></i> @lang('admin_lang.Add')</a>
                </h5>
                <div class="card-body">
                    
                    <div class="table-responsive listBody">
                        <table class="table table-striped table-bordered first" id="address">
                            <thead>
                                <tr>
                                    <th>@lang('admin_lang.country')</th>
                                    <th>@lang('admin_lang.city')</th>
                                    <th>@lang('admin_lang.block')</th>
                                    <th>@lang('admin_lang.street')</th>
                                    <th>@lang('admin_lang.postal_code')</th>
                                    <th>@lang('admin_lang.building')</th>
                                    <th>@lang('admin_lang.more_address_details')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>@lang('admin_lang.country')</th>
                                    <th>@lang('admin_lang.city')</th>
                                    <th>@lang('admin_lang.block')</th>
                                    <th>@lang('admin_lang.street')</th>
                                    <th>@lang('admin_lang.postal_code')</th>
                                    <th>@lang('admin_lang.building')</th>
                                    <th>@lang('admin_lang.more_address_details')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table  -->
        <!-- ============================================================== -->
    </div>
</div>
<!-- footer -->
<!-- ============================================================== -->
@include('admin.includes.footer')
<!-- ============================================================== -->
<!-- end footer -->
<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        // datatable
        $('body').on('click', '.reset_search', function() {
            $('#address').DataTable().search('').columns().search('').draw();
        })

        function filterColumn ( i ) {
            console.log(i, $('#col'+i+'_filter').val())
            $('#address').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }
        $('#address').DataTable( {
            stateSave: true,
            "order": [[0, "desc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
                $('#col2_filter').val(data.search.applied_for)
                $('#col3_filter').val(data.search.applied_on)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
                data.search.applied_for = $('#col2_filter').val()
                data.search.applied_on = $('#col3_filter').val()
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('show.address.book') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                    d.id = "{{ request()->segment(3) }}";
                }
            },
            'columns': [
                { 
                    data: "country",
                    render: function(data, type, full) {
                        return full.country_details_bylanguage.name;
                    }
                },
                {  
                    data: 'city',
                    render: function(data, type, full) {
                        if(full.city_id == null){
                            return full.city;
                        }else{
                            return full.get_city_name_by_language.name;
                        }
                    }
                },
                {   data: 'block' },
                {   data: 'street' },
                {   data: 'postal_code' },
                {   
                    data: 'building_no'
                },
                {  
                    data: 'more_address',
                },
                {  
                    render: function(data, type, full) {
                        var a = '';
                        a += ' <a href="{{ url('admin/edit-merchant-address') }}/'+full.id+'/'+'{{ request()->segment(3) }}'+'"><i class="fa fa-edit" title="@lang('admin_lang.Edit')"></i></a>';
                        a += ' <a class="delete_address" href="javascript:void(0)" data-id="'+full.id+'"><i class=" fa fa-trash" title="@lang('admin_lang.Delete')"></i></a>';

                        return a;
                    }
                },
            ],
            // "createdRow": function( row, data, dataIndex ) {
            //     console.log( row, data, dataIndex)
            //     // add your condition here...
            //     $(row).addClass( 'important' );
            // }
        });

        // change event
        $('input.keyword').on( 'keyup click blur', function () {
            filterColumn( 1 );
        });
        $('.applied_for').on( 'change', function () {
            filterColumn( 2 );
        });
        $('.applied_on').on( 'change', function () {
            filterColumn( 3 );
        });

        $(document).on("click",".delete_address",function(e){
            var id = $(e.currentTarget).attr("data-id");
            var $this = $(this);
            if(confirm("@lang('merchant_lang.delete_address_warning')")){

                var reqData = {
                    "jsonrpc":"2.0",
                    "_token":"{{ csrf_token() }}",
                    "data":{
                        id:id
                    }
                };
                $.ajax({
                    url:"{{ route('admin.delete.merchant.address') }}",
                    type:"post",
                    data:reqData,
                    success:function(resp){
                        console.log(resp)
                        if(resp.status == 1){
                            $(".success_msg").html('@lang('merchant_lang.delete_address_success')');
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".tr_"+id).hide();
                            $this.parent().parent().remove();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('@lang('merchant_lang.delete_address_error')');
                        }
                        $(".session_success_div").hide();
                        $(".session_error_div").hide();
                    }
                });
            }
            // }
        });
    });


</script>
@endsection