@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Edit Merchant') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.edit_merchant')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<style type="text/css">
    .error {
        background: none;
    }
</style>
<style type="text/css">
    label.error{
    border: none !important;
    background-color: #fff !important;
    color: #f00;
    }
    #cityList{
    position: absolute;
    width: 96%;
    /*height: 200px;*/
    z-index: 99;
    }
    #cityList ul{
    background: #fff;
    width: 96%;
    border: solid 1px #eee;
    padding: 0;
    max-height: 200px;
    overflow-y: scroll;
    }
    #cityList ul li{
    list-style: none;
    padding: 5px 15px;
    cursor: pointer;
    border-bottom: solid 1px #eee;
    }
    #cityList ul li:hover{
    background: #3a82c4;
    color: #fff;
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        <a href="{{ route('admin.list.merchant') }}" class="breadcrumb-link">
                                            @lang('admin_lang.manage_merchant')
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.edit_merchant')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                @lang('admin_lang.edit_merchant')
                                <a class="adbtn btn btn-primary" href="{{ route('admin.list.merchant') }}">
                                    <i class="fas fa-less-than"></i>
                                    @lang('admin_lang.back')
                                </a>
                            </h5>
                            <div class="card-body">
                                <form id="editMerchant" method="post" enctype="multipart/form-data" action="{{ route('admin.edit.merchant',$merchant->id) }}">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="fname" class="col-form-label">
                                                <span style="color: red;">*</span>@lang('admin_lang.first_name')
                                            </label>
                                            <input type="hidden" id="old_phone" value="{{ @$merchant->old_phone }}">
                                            <input id="fname" name="fname" type="text" class="form-control fname required" placeholder='@lang('admin_lang.first_name')' value="{{ @$merchant->fname }}">
                                            <span class="errorFname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="lname" class="col-form-label">
                                                <span style="color: red;">*</span>@lang('admin_lang.last_name')</label>
                                                <input id="lname" name="lname" type="text" class="form-control  lname required" placeholder='@lang('admin_lang.last_name')' value="{{ @$merchant->lname }}">
                                                <span class="errorLname" style="color: red;"></span>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="email" class="col-form-label">
                                                    <span style="color: red;">*</span>@lang('admin_lang.email')
                                                </label>
                                                <input id="email" name="email" type="email" placeholder='@lang('admin_lang.example_email')' class="form-control required email" value="{{ @$merchant->email }}">
                                                <span class="errorEmail" style="color: red;"></span>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="email1" class="col-form-label">
                                                    <span style="color: red;">*</span>Alternate email-id
                                                </label>
                                                <input id="email1" name="email1" type="email" placeholder='Email-id' class="form-control" value="{{ @$merchant->email1 }}">
                                                
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="phone" class="col-form-label">
                                                    <span style="color: red;">*</span>@lang('admin_lang.phone')
                                                </label>
                                                <input id="phone" name="phone" type="text" class="form-control required" placeholder='@lang('admin_lang.phone')' value="{{ @$merchant->phone }}" onkeypress='validate(event)' maxlength="10">
                                                <span class="errorPhone" style="color: red;"></span>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="company_name" class="col-form-label">
                                                    <span style="color: red;">*</span>@lang('admin_lang.company_name')
                                                </label>
                                                <input id="company_name" name="company_name" type="text" class="form-control required" placeholder='@lang('admin_lang.company_name')' value="{{ @$merchant->company_name }}">
                                                <span class="errorCompanyName" style="color: red;"></span>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="company_type" class="col-form-label">
                                                    <span style="color: red;">*</span>Company Type
                                                </label>
                                                <input id="company_type" name="company_type" type="text" class="form-control required" placeholder='Company Type' value="{{ @$merchant->company_type }}">
                                                <span class="errorCompanyType" style="color: red;"></span>
                                            </div>
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label for="company_type" class="col-form-label">
                                                    <span style="color: red;">*</span>Company Details
                                                </label>
                                                <textarea id="company_details" name="company_details" type="text" class="form-control required" placeholder='Company Details'>{{ @$merchant->company_details }}</textarea>
                                                <span class="errorCompanyType" style="color: red;"></span>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="commssion" class="col-form-label">
                                                    Commission(%)
                                                </label>
                                                <input id="commssion" name="commssion" type="text" class="form-control" placeholder='@lang('admin_lang.comission')' value="{{ @$merchant->commission }}" onkeypress='validate(event)'>
                                                <span class="errorCommission" style="color: red;"></span>
                                            </div>
                                            {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="commssion" class="col-form-label">
                                                    @lang('admin_lang.external_1') @lang('admin_lang.shipping_cost')
                                                </label>
                                                <input id="shipping_cost" name="shipping_cost" type="text" class="form-control" placeholder='Shipping Cost' value="{{ @$merchant->shipping_cost }}" onkeypress='validate(event)'>
                                                <span class="error_shipping_cost" style="color: red;"></span>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="commssion" class="col-form-label">
                                                    @lang('admin_lang.internal_1') @lang('admin_lang.shipping_cost')
                                                </label>
                                                <input id="internal_shipping_cost" name="internal_shipping_cost" type="text" class="form-control" placeholder='Shipping Cost' value="{{ @$merchant->inside_shipping_cost }}" onkeypress='validate(event)'>
                                                <span class="error_internal_shipping_cost" style="color: red;"></span>
                                            </div> --}}
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 fstbtncls">
                                                <input type="file" class="custom-file-input" id="customFile" name="profile_pic" accept="image/jpg,image/jpeg,image/png">
                                                <label class="custom-file-label extrlft" for="customFile">
                                                    Upload profile picture
                                                </label>
                                                @lang('admin_lang.recommended_size_250')
                                                <span class="errorPic" style="color: red;"></span>
                                                {{-- <div class="uploaded_pics">
                                                    @if(@$merchant->image)
                                                    @php
                                                    $image_path = 'storage/app/public/profile_pics/'.@$merchant->image; 
                                                    @endphp
                                                    @if(file_exists(@$image_path))
                                                    <img id="profilePictures" src="{{ URL::to('storage/app/public/profile_pics/'.@$merchant->image) }}" alt="" style="width: 100px;height: 80px;">
                                                    @endif
                                                    @endif
                                                </div> --}}
                                                <div class="profile" @if(@$merchant->image) style="display: block;" @else style="display: none;"  @endif>
                                                    {{-- <img src="" id="profilePicture" style="height: 100px;width: 100px;"> --}}
                                                    @if(@$merchant->image)
                                                    @php
                                                    $image_path = 'storage/app/public/customer/profile_pics/'.@$merchant->image; 
                                                    @endphp
                                                    @if(file_exists(@$image_path))
                                                    <img id="profilePicture" src="{{ URL::to('storage/app/public/customer/profile_pics/'.@$merchant->image) }}" alt="" style="width: 100px;height: 80px;">
                                                    @else
                                                    <img id="profilePicture" src="#" alt="" style="width: 100px;height: 80px;">
                                                    @endif
                                                    @else
                                                    <img id="profilePicture" src="#" alt="" style="width: 100px;height: 80px;">
                                                    @endif
                                                    
                                                </div>

                                            </div>
                                           {{--  <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 fstbtncls">
                                                <input type="file" class="custom-file-input" id="cover_pic" name="cover_pic" accept="image/jpg,image/jpeg,image/png">
                                                <label class="custom-file-label extrlft" for="cover_pic">
                                                    Upload cover picture
                                                </label>
                                                @lang('admin_lang.recommended_size_cover')
                                                <span class="errorcoverpic" style="color: red;"></span>
                                                <div class="cover" @if(@$merchant->cover_pic) style="display: block;" @else style="display: none;"  @endif>
                                                    @if(@$merchant->image)
                                                    @php
                                                    $image_path = 'storage/app/public/profile_pics/'.@$merchant->cover_pic; 
                                                    @endphp
                                                    @if(file_exists(@$image_path))
                                                    <img id="coverPicture" src="{{ URL::to('storage/app/public/cover_pic/'.@$merchant->cover_pic) }}" alt="" style="width: 100px;height: 80px;">
                                                    @endif
                                                    @endif
                                                    
                                                </div>

                                            </div> --}}
                                            {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="password_n" class="col-form-label">
                                                    Change password
                                                </label>
                                                <input id="nw_password" name="nw_password" type="password" class="form-control" placeholder='New password' value="" >
                                                <span class="error_nw_password" style="color: red;"></span>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="password_c" class="col-form-label">
                                                    Confirm password
                                                </label>
                                                <input id="confirm_password" name="confirm_password" type="password" class="form-control" placeholder='Confirm password' value="" >
                                                <span class="error_confirm_password" style="color: red;"></span>
                                            </div> --}}
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <h4 class="fultxt">
                                                    @lang('admin_lang.store_address')
                                                </h4>
                                            </div>
                                            <hr>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="country" class="col-form-label">
                                                    <span style="color: red;">*</span>@lang('admin_lang.country')</label>
                                                    <select class="form-control required" id="country" name="country">
                                                        <option value="">@lang('admin_lang.select_country')</option>
                                                        @if(@$countries)
                                                        @foreach(@$countries as $country)
                                                        <option value="{{ @$country->id }}" @if(@$merchant->country == @$country->id) selected @elseif($country->status == 'I') style="display:none;" @endif>{{ @$country->countryDetailsBylanguage->name }}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    <span class="errorCountry" style="color: red;"></span>
                                                </div>
                                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="state" class="col-form-label"> <span style="color: red;">*</span> @lang('admin_lang.state')</label>
                                               {{--  <input name="state" type="text" class="form-control required" placeholder='@lang('admin_lang.state')' value="{{ @$merchant->state }}"> --}}
                                                <select class="form-control required" id="state" name="state">
                                                        <option value="">Select State</option>
                                                        @if($state)
                                                        @foreach(@$state as $sta)
                                                        <option value="{{ @$sta->id }}" @if(@$merchant->state == @$sta->id) selected @endif>{{ @$sta->name }}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                <span class="errorState" style="color: red;"></span>
                                            </div>
                                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 outside_kuwait_city">

                                                    <label for="city" class="col-form-label"><span style="color: red;">*</span> @lang('admin_lang.city')</label>

                                                    {{-- <input id="city" name="city" type="text" class="form-control required" placeholder='@lang('admin_lang.city')' value="{{ @$merchant->city }}"> --}}
                                                    <select class="form-control required" id="city" name="city">
                                                        <option value="">Select City</option>
                                                    </select>
                                                    <span class="errorCity" style="color: red;"></span>
                                                </div>
                                                {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12  inside_kuwait_city" @if(@$merchant->country == 134) style="display:block;" @else style="display:none;" @endif>
                                                    <label for="kuwait_city_lb" class="col-form-label">@lang('admin_lang.city')(@lang('admin_lang.required'))</label>
                                                    <input type="text" class="form-control required insideKuwCity" name="kuwait_city" id="kuwait_city" placeholder="@lang('admin_lang.city')" value="{{@$merchant->merchantCityDetails->name }}" autocomplete="off">
                                                    <div id="cityList"></div>
                                                    <input type="hidden" id="kuwait_city_value_id" name="kuwait_city_value_id" value="{{@$merchant->merchantCityDetails->city_id }}">
                                                    <input type="hidden" id="kuwait_city_value_name" name="kuwait_city_value_name" value="{{@$merchant->merchantCityDetails->name }}">
                                                    <span class="error_kuwait_city removeText city_id_err text-danger" ></span>
                                                </div> --}}
                                            {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="street" class="col-form-label">
                                                    @lang('admin_lang.street')
                                                </label>
                                                <input id="street" name="street" type="text" class="form-control required" placeholder="Street" value="{{ @$merchant->address }}">
                                                <span class="errorStreet" style="color: red;"></span>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="street" class="col-form-label">
                                                    @lang('admin_lang.avenue')
                                                </label>
                                                <input id="avenue" name="avenue" type="text" class="form-control" placeholder="Avenue" value="{{ @$merchant->avenue }}">
                                                <span class="errorAvenue" style="color: red;"></span>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="street" class="col-form-label">
                                                    @lang('admin_lang.building_number')
                                                </label>
                                                <input id="building_number" name="building_number" type="text" class="form-control" placeholder="BuildingNumber" value="{{ @$merchant->building_number }}">
                                                <span class="errorBuildingNumber" style="color: red;"></span>
                                            </div> --}}
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="zip" class="col-form-label"><span style="color: red;">*</span> @lang('admin_lang.zip')</label>
                                                <input id="zip" name="zip" type="text" class="form-control required" placeholder="Zip" value="{{ @$merchant->zipcode }}" min="6">
                                                <span class="errorZip" style="color: red;"></span>
                                            </div>

                                            {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label class="col-form-label">@lang('front_static.location')</label>
                                                <input id="pac-input" type="text" placeholder="@lang('front_static.location')" name="location" class="form-control" value="{{ $merchant->location }}">
                                                
                                                <input type="hidden" name="lat" id="lat" value="{{ $merchant->location }}">
                                                <input type="hidden" name="lng" id="lng" value="{{ $merchant->location }}">
                                            </div> --}}
                                        </div>
                                        <div class="row">
                                            {{-- <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label for="street" class="col-form-label">
                                                    @lang('admin_lang.address_note')
                                                </label>
                                                <textarea id="address_note" name="address_note" type="text" class="form-control" placeholder="Address Note" rows="4">{!! strip_tags(@$merchant->address_note) !!}</textarea>
                                                <span class="errorAddressNote" style="color: red;"></span>
                                            </div> --}}
                                            {{-- <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                               
                                                    <input type="file" class="custom-file-input inpt" id="gallery-photo-add" name="images[]" multiple="" accept="image/jpg,image/jpeg,image/png"/>
                                                    <label class="custom-file-label extrlft" for="gallery-photo-add">
                                                    @lang('merchant_lang.UploadPortfolioImages')</label>
                                                    @lang('admin_lang.recommended_size_300')
                                                <span class="errorPortfolio" style="color: red;"></span>
                                            </div> --}}
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="uplodpic">
                                                    <div class="uplodpic">
                                                    </div>
                                                    <div class="uplodpic gallery"></div>
                                                    <div class="uplodpic">
                                                        @if(@$merchant->merchantImage)
                                                        @foreach(@$merchant->merchantImage as $img)
                                                        @php
                                                        $image_path = 'storage/app/public/merchant_portfolio/'.@$img->image; 
                                                        @endphp
                                                        @if(file_exists(@$image_path))
                                                        <li class=" remove-rw-{{ @$img->id }}">
                                                            <img id="merchantPic" src="{{ URL::to('storage/app/public/merchant_portfolio/'.@$img->image) }}" alt="" style="width: 100px;height: 100px;">
                                                            <div class="action-opt">
                                                                <span class="remove-img">
                                                                    <a href="javascript:;" title="Remove" class="remove-image" data-id="{{ @$img->id }}">
                                                                        <i class="fa fa-times"></i>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                            <div class="action-opt">
                                                                <span class="remove-img">
                                                                    <a href="javascript:;" title="Remove" class="remove-image" data-id="{{ @$img->id }}">
                                                                        <i class="fa fa-times"></i>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        </li>     
                                                        @endif
                                                        @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <h4 class="fultxt">
                                                    @lang('admin_lang.order_notification_email')
                                                </h4>
                                            </div>
                                            <hr>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="email1" class="col-form-label">
                                                    <span style="color: red;">*</span>@lang('admin_lang.email_1')
                                                </label>
                                                <input id="email1" name="email_1" type="text" class="form-control emailNotification" placeholder="Email 1" value="{{ @$merchant->email1 }}">
                                                <span class="error_email_notification error_email_order_notification error_email_1" style="color: red;"></span>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="email2" class="col-form-label">
                                                    @lang('admin_lang.email_2')
                                                </label>
                                                <input id="email2" name="email_2" type="text" class="form-control emailNotification" placeholder="Email 2" value="{{ @$merchant->email2 }}">
                                                <span class="error_email_notification" style="color: red;"></span>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="email3" class="col-form-label">
                                                    @lang('admin_lang.email_3')
                                                </label>
                                                <input id="email3" name="email_3" type="text" class="form-control emailNotification" placeholder="Email 3" value="{{ @$merchant->email3 }}">
                                                <span class="error_email_notification" style="color: red;"></span>
                                            </div> --}}

                                            {{-- @if(@$language)
                                            @foreach(@$language as $key=>$lang)
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label class="col-form-label" for="company_desc{{ @$lang->id }}">
                                                    <span style="color: red;">*</span>@lang('admin_lang.company_description')  [{{ @$lang->name }}] </label>
                                                    <textarea class="form-control desc_comp required" id="company_desc{{ @$lang->id }}" name="comp_desc[]" rows="3">{!! strip_tags(@$merchant->merchantCompanyDetails[$key]->description) !!}</textarea>
                                                    <span class="desc" style="color: red;"></span>
                                                </div>
                                                @endforeach
                                                @endif --}}

                                                {{-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <h4 class="fultxt">
                                                        @lang('admin_lang.bank_information')
                                                    </h4>
                                                </div> --}}
                                                {{-- <hr> --}}
                                                {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                    <label for="bank_name" class="col-form-label">
                                                        <span style="color: red;">*</span>@lang('admin_lang.bank_name')</label>
                                                        <input id="bank_name" name="bank_name" type="text" class="form-control required" placeholder="Bank Name" value="{{ @$merchant->bank_name }}">
                                                        <span class="errorBank_name" style="color: red;"></span>
                                                    </div>
                                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                        <label for="account_name" class="col-form-label">
                                                            <span style="color: red;">*</span>@lang('admin_lang.bank_account_name')
                                                        </label>
                                                        <input id="account_name" name="account_name" type="text" class="form-control required" placeholder='@lang('admin_lang.account_name')' value="{{ @$merchant->account_name }}">
                                                        <span class="errorAccount_name" style="color: red;"></span>
                                                    </div>
                                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                        <label for="account_number" class="col-form-label">
                                                            <span style="color: red;">*</span>@lang('admin_lang.bank_account_number')
                                                        </label>
                                                        <input id="account_number" name="account_number" type="text" class="form-control required" placeholder='@lang('admin_lang.bank_account_number')' value="{{ @$merchant->account_number }}">
                                                        <span class="errorAccount_number" style="color: red;"></span>
                                                    </div> --}}
                                                    {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                        <label for="iban_number" class="col-form-label">
                                                            <span style="color: red;">*</span>@lang('admin_lang.iban_number')</label>
                                                            <input id="iban_number" name="iban_number" type="text" class="form-control required" placeholder="IBAN Number" value="{{ @$merchant->iban_number }}">
                                                            <span class="errorIban_number" style="color: red;"></span>
                                                        </div> --}}
                                                        {{-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <h4 class="fultxt">
                                                            @lang('admin_lang.Payment_acceptance_mode')</h4>
                                                        </div> --}}
                                                        {{-- <hr> --}}
                                                        {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                            <label for="payment_mode" class="col-form-label">
                                                                <span style="color: red;">*</span>@lang('admin_lang.Payment_mode')</label>
                                                                <select class="form-control required" id="payment_mode" name="payment_mode">
                                                                    <option value="">@lang('admin_lang.select_payment_mode')</option>
                                                                    <option value="O" @if(@$merchant->payment_mode == "O") selected @endif>
                                                                        @lang('admin_lang.only_online')
                                                                    </option>
                                                                    <option value="C" @if(@$merchant->payment_mode == "C") selected @endif>
                                                                        @lang('admin_lang.cash_on_delivery')
                                                                    </option>
                                                                    <option value="A" @if(@$merchant->payment_mode == "A") selected @endif>@lang('admin_lang.all')</option>
                                                                </select>
                                                                <span class="errorPaymentMode" style="color: red;"></span>
                                                            </div> --}}
                                                            {{-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"style="margin-bottom: 12px;">
                                                                <h4 class="fultxt">@lang('admin_lang.openning_hours')</h4>
                                                            </div>
                                                            <hr>
                                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                @php 
                                                                $days = getDays();
                                                                @endphp
                                                                @foreach($days as $key => $value)
                                                                <div class="day_div">
                                                                    <span class="day_name">
                                                                        <div class="form-group check_group logchk">
                                                                            @lang('merchant_lang.'.$value)
                                                                        </div>
                                                                    </span>
                                                                    <div class="tymm width-set">
                                                                        <input type="hidden" id="old_email" name="old_email" value="{{ @$merchant->email }}">
                                                                        <input type="hidden" name="day[]" value="{{ $key+1 }}">
                                                                        <span class="file_div">
                                                                            <input type="text" class="form-control timepicker from_time {{ $value }} required_remove" placeholder='@lang('admin_lang.from_time')' name="from_time[]" value="@if(@$merchant->merchantOpenning[$key]->from_time) {{ @$merchant->merchantOpenning[$key]->from_time }} @endif" data-value="{{ $value }}" >
                                                                            <span class="error_time_from from_time_error_{{ $key }} text-danger error_from_{{ $value }}"></span>
                                                                        </span>
                                                                        <span class="file_div"><input type="text" class="form-control timepicker to_time" placeholder='@lang('admin_lang.to_time')' name="to_time[]"  value="@if(@$merchant->merchantOpenning[$key]->to_time) {{ @$merchant->merchantOpenning[$key]->to_time }} @endif" data-value="{{ $value }}"  ><span class="error_time_to to_time_error_{{ $key }} text-danger error_to_{{ $value }}"></span></span>
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            </div> --}}
                                                            <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                                                <a href="javascript:void(0)" id="saveNewMerchant" class="btn btn-primary ">
                                                                    @lang('admin_lang.save')
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- footer -->
                        <!-- ============================================================== -->
                        @include('admin.includes.footer')
                        <!-- ============================================================== -->
                        <!-- end footer -->
                        <!-- ============================================================== -->
                    </div>
                </div>
            </div>
        </div>

        @endsection
        @section('scripts')
        {{-- @include('admin.includes.scripts') --}}

        @if(Config::get('app.locale') == 'en')
        @include('admin.includes.scripts')
        @elseif(Config::get('app.locale') == 'ar')
        @include('admin.includes.arabic_scripts')
        @endif
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
        <script>


        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                @if(@$merchant->city)
                $('#state').change(function (){
                    var stateid = $(this).val();
                    $.ajax({
                        url:"{{ url('admin/merchant-get-city') }}-"+stateid,
                        type:"GET",
                        success:function(responce){
                            $('select#city').html(responce);
                            $('select#city').val({{ @$merchant->city }});
                        },
                        error: function(xhr){
                            console.log(xhr);
                        }
                    });
                });
                @else
                $('#state').change(function (){
                    var stateid = $(this).val();
                    $.ajax({
                        url:"{{ url('admin/merchant-get-city') }}-"+stateid,
                        type:"GET",
                        success:function(responce){
                            $('select#city').html(responce);
                        },
                        error: function(xhr){
                            console.log(xhr);
                        }
                    });
                });
                @endif
                @if(@$merchant->city)
                var stateid = $('#state').val();
                $.ajax({
                    url:"{{ url('admin/merchant-get-city') }}-"+stateid,
                    type:"GET",
                    success:function(responce){
                        $('select#city').html(responce);
                        $('select#city').val({{ @$merchant->city }});
                    },
                    error: function(xhr){
                        console.log(xhr);
                    }
                });
                @endif
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('#blah').hide();
                    $('#blah').fadeIn(500);
                }
                function readURLGalary(input) {
                    if (input.files && input.files[0]) {
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                $("#imgInp").change(function(){
                    readURLGalary(this);
                });
        // $(document).on('keyup',"#phone",function(e){
        //     var name = $(e.currentTarget).val();
        //     if(isNaN(e.key)){
        //         $(e.currentTarget).val("");
        //         $(".errorPhone").text('@lang('validation.error_phn_valid')');
        //     }
        //     $(".errorPhone").text("");
        // });
        $(document).on('blur',".timepicker",function(e){

            var day = $.trim($(this).attr("data-value")),
            from_time = $.trim($("."+day).val()),
            to_time = $.trim($(this).val());
            if(from_time !="" && to_time != ""){
                var stt = new Date("November 13, 2013 " + from_time);
                stt = stt.getTime();

                var endt = new Date("November 13, 2013 " + to_time);
                endt = endt.getTime();

                //by this you can see time stamp value in console via firebug
                // console.log("Time1: "+ stt + " Time2: " + endt);

                if(stt > endt) {
                    $(this).val("");
                    $("."+day).val("");
                    $(".error_to_"+day).text('@lang('admin_lang.from_time_invalid')');
                    $(".error_from_"+day).text('@lang('admin_lang.to_time_invalid')');
                }
            }
            
        });
        $('#country').change(function(){
            var country = $(this).val();
            $("#kuwait_city").val("");
            $("#city").val("");
            if(country == 134){
                $(".outside_kuwait_city").hide();
                $(".inside_kuwait_city").show();
                $("#city").removeClass("required");
                $("#kuwait_city").addClass("required");
            }else{
                $(".outside_kuwait_city").show();
                $(".inside_kuwait_city").hide();
                $("#kuwait_city").removeClass("required");
                $("#city").addClass("required");
            }
        });
        $(document).on('blur',"#commssion",function(e){
            var commission = $.trim($("#commssion").val());
            if(commission != ""){

                var com = parseFloat(commission);
                com = com.toFixed(2);
                // alert(com);
                if(isNaN(com)){
                    $(".errorCommission").text('@lang('validation.error_commission_valid')');
                    $("#commssion").val("");
                }else{
                    if(com>100){
                        $(e.currentTarget).val("");
                        $(".errorCommission").text('@lang('validation.com_percentage')');
                    }else{

                        var len = commission.length;
                        if(len>6){
                            $(".errorCommission").text('@lang('validation.error_commission_valid')');
                        }else{
                            $(".errorCommission").text("");
                        }
                        
                        $(e.currentTarget).val(com);
                    }
                }
            }
        });
    //     $(document).on('keyup',"#commssion",function(e){
    //       var name = $.trim($(e.currentTarget).val());
    //       if(name != ""){
    //         if(e.keyCode != 110 || e.keyCode != 8){
    //             if(e.key != '.'){
    //                 if(isNaN(e.key)){
    //                     $(e.currentTarget).val("");
    //                     $(".errorCommission").text('@lang('valudation.error_commission_valid')');
    //                 }else{
    //                     $(".error_shipping_cost").text("");
    //                 }
    //             }
    //         }else{
    //             $(".errorCommission").text("");
    //         } 
    //     }

    // });
    //     $(document).on('blur',"#shipping_cost",function(e){
    //         var cost = $.trim($("#shipping_cost").val());
    //         if(cost != ""){
    //             cost = parseFloat(cost);
    //             cost = cost.toFixed(3);
    //             if(isNaN(cost)){
    //                 $(".error_shipping_cost").text('@lang('validation.shippingcost_error')');
    //                 $("#shipping_cost").val("");
    //             }else{
    //                 $("#shipping_cost").val(cost);
    //             }

    //         }
    //     });
    //     $(document).on('keyup',"#shipping_cost",function(e){
    //       var name = $(e.currentTarget).val();
    //       if(e.key != '.'){
    //         if(isNaN(e.key)){
    //             $(e.currentTarget).val("");
    //             $(".error_shipping_cost").text('@lang('validation.shippingcost_error')');
    //         }else{
    //             $(".error_shipping_cost").text("");
    //         }
    //     }else{
    //         $(".error_shipping_cost").text("");
    //     }
    // });
    //     $(document).on('blur',"#internal_shipping_cost",function(e){
    //         var cost = $.trim($("#internal_shipping_cost").val());
    //         if(cost != ""){
    //             cost = parseFloat(cost);
    //             cost = cost.toFixed(3);
    //             if(isNaN(cost)){
    //                 $(".error_internal_shipping_cost").text('@lang('validation.shippingcost_error')');
    //                 $("#internal_shipping_cost").val("");
    //             }else{
    //                 $("#internal_shipping_cost").val(cost);
    //             }

    //         }
    //     });
    //     $(document).on('keyup',"#internal_shipping_cost",function(e){
    //       var name = $(e.currentTarget).val();
    //       if(e.key != '.'){
    //         if(isNaN(e.key)){
    //             $(e.currentTarget).val("");
    //             $(".error_internal_shipping_cost").text('@lang('validation.shippingcost_error')');
    //         }else{
    //             $(".error_internal_shipping_cost").text("");
    //         }
    //     }else{
    //         $(".error_internal_shipping_cost").text("");
    //     }
    // });
    $("#customFile").change(function() {
        var filename = $.trim($("#customFile").val());
        if(filename != ""){
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".profile").show();
                $(".errorpic").text("");
                readURL(this);
            }else{
                $(".errorpic").html('@lang('validation.img_upload')');
                $('#profilePicture').attr('src', "");
            }
        } 
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#profilePicture').attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
      }
  }
    $("#cover_pic").change(function() {
        var filename = $.trim($("#cover_pic").val());
        if(filename != ""){
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".cover").show();
                $(".errorcoverpic").text("");
                readCoverURL(this);
            }else{
                $(".errorcoverpic").html('@lang('validation.img_upload')');
                $('#coverPicture').attr('src', "");
            }
        } 
    });
    function readCoverURL(input) {
        // alert("dkjvbdjhbg");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#coverPicture').attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
        }
    }
  $('input.timepicker').timepicker({
    change: function(time) {
                // if ($(this).val() != '') {
                //     $($(this).next().html(''));
                // } else {
                //     $($(this).next().html('@lang('validation.required')'));
                // }
                var day = $.trim($(this).attr("data-value")),
                from_time = $.trim($("."+day).val()),
                to_time = $.trim($(this).val());
                if(from_time !="" && to_time != ""){
                    var stt = new Date("November 13, 2013 " + from_time);
                    stt = stt.getTime();

                    var endt = new Date("November 13, 2013 " + to_time);
                    endt = endt.getTime();

                    if(stt > endt){
                        $(this).val("");
                        $("."+day).val("");
                        $(".error_to_"+day).text("@lang('admin_lang.to_time_invalid')");
                        $(".error_from_"+day).text("@lang('admin_lang.to_time_invalid')");
                    }else{
                        $(".error_to_"+day).text("");
                        $(".error_from_"+day).text("");
                    }
                }
            }
        });
  $(document).on("blur","#phone",function(e){
    var phone = $.trim($("#phone").val());
            // $.trim($("#old_phone").val())
            if(phone != "")
            {
                var rurl = '{{ route('change.merchant.phone.no') }}';
                $.ajax({
                    type:"GET",
                    url:rurl,
                    data: {
                        phone:phone,
                        old_phone:"{{  @$merchant->phone }}"
                    },
                    success:function(resp){
                        if(resp == 0){
                            $(".errorPhone").text("@lang('validation.phone_number_already_exist')");
                            $('#phone').val('');    
                        }else{
                            $(".errorPhone").text("");
                        }
                        
                    }
                });
            }
            else{
                $(".errorEmail").text("@lang('validation.please_provide_valid_email_id')");
                
            }
        }); 
  var imagesPreview = function(input, placeToInsertImagePreview) {

    if (input.files) {
        var filesAmount = input.files.length;

        for (i = 0; i < filesAmount; i++) {
            var reader = new FileReader();

            reader.onload = function(event) {
                var new_html = '<li><img src="'+event.target.result+'"></li>';
                $('.gallery').append(new_html);
                    // $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }

    };
    
    $(document).on('change','#gallery-photo-add', function() {
        imagesPreview(this, 'div.gallery');
        $('.gallery').html('');
    });
        // $(document).on("blur",".emailNotification",function(e){

        // });
        $('#country').change(function(){
            if($(this).val() != '')
            {
                var value  = $(this).val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('state.fetch') }}",
                    method:"POST",
                    data:{value:value, _token:_token},
                    success:function(result)
                    {
                        $('#state').html(result);
                    }
                });
            }
        });
        $('#country').change(function(){
            $('#state').val('');
        });
        // $('body').on('click','.remove-image',function(){
        //     var var_id = $(this).data('id');
        //     var reqDataa = {
        //         jsonrpc: '2.0'
        //     };

        //     $.ajax({
        //         type:'GET',
        //         url : '/'+var_id,
        //         data:reqDataa,
        //         success:function(response){
        //             if(response.sucess) {
        //                 // if(response.sucess.result == 'Add') {
        //                     $(".remove-rw-"+var_id).hide();
        //                 // }

        //             } else {
        //                 $(".remove-rw-"+var_id).hide();
        //                 // console.log(".remove-rw-"+var_id);
        //             }
        //         }
        //     });     

        // });   
        $(document).on("blur","#email",function(e){
            var email = $.trim($("#email").val());
            // alert(email);
            if(email != "")
            {
                var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(regex.test(email)){
                    var rurl = '{{ route('change.merchant.email.id') }}';
                         // alert($.trim($("#old_email").val()));
                         $.ajax({
                            type:"GET",
                            url:rurl,
                            data: {
                                email:email,
                                old_email:$.trim($("#old_email").val())
                            },
                            success:function(resp){
                                if(resp == 0){
                                    $(".errorEmail").text("@lang('validation.email_id_already_exist')");
                                    $('#email').val('');    
                                }else{
                                    $(".errorEmail").text("");
                                }
                                
                            }
                        });
                         
                         
                     }else{
                        $(".errorEmail").text("@lang('validation.please_provide_valid_email_id')");
                        
                    }
                }
                else{
                    $(".errorEmail").text("@lang('validation.please_provide_valid_email_id')");
                    
                }
            });
        jQuery.validator.addMethod("check_desc",function(value, element, params){
            var len = $(".desc_comp").length,errorCount=0;
            for(var i =0;i<len;i++){
                var nm = $.trim($(".desc_comp").eq(i).val());

                if(nm == ""){
                    errorCount++
                    $(".desc") .eq(i).text('@lang('validation.required')');
                }
            }
            return this.optional(element) || (errorCount == 0 ) 
        },
        '@lang('validation.required')'
        );
        $("#editMerchant").validate({
            rules:{
                'fname':{
                    required:true
                },
                'lname':{
                    required:true
                },
                'nw_password': {
                    minlength: 8
                },            
                'confirm_password':{
                    equalTo: '#nw_password'
                },
                'company_name':{
                    required:true
                },
                'comp_desc':{
                    check_desc:true
                },
                'bank_name':{
                    required:true
                },
                'country':{
                    required:true
                },
                'iban_number':{
                    required:true
                },
                'account_name':{
                    required:true
                },
                'account_number':{
                    required:true
                },
                'payment_mode':{
                    required:true
                },
                'city':{
                    required:true
                },
                'zip':{
                    digits: true
                },
                'street':{
                    required:true
                },
                'email1':{
                    required:true,
                    email:true
                },
                'email_2':{
                    email:true
                },
                'email_3':{
                    email:true
                },
                'profile_pic':{
                    accept:"image/jpg,image/jpeg,image/png"
                },
                'cover_pic':{
                    accept:"image/jpg,image/jpeg,image/png"
                },
                'phone':{
                    required: true,
                    digits: true
                },
                'email':{
                    required:true,
                    email:true
                },
            },
            messages: { 
                fname: { 
                    required: '@lang('validation.required')'
                },
                lname: { 
                    required: '@lang('validation.required')'
                },
                iban_number: { 
                    required: '@lang('validation.required')'
                },
                account_name: { 
                    required: '@lang('validation.required')'
                },
                account_number: { 
                    required: '@lang('validation.required')'
                },
                payment_mode: { 
                    required: '@lang('validation.required')'
                },
                comp_desc: { 
                    required: '@lang('validation.required')'
                },
                city: { 
                    required: '@lang('validation.required')'
                },
                bank_name: { 
                    required: '@lang('validation.required')'
                },
                country: { 
                    required: '@lang('validation.required')'
                },
                zip: { 
                    digits:'@lang('admin_lang.zip_error')'
                },
                company_name: { 
                    required: '@lang('validation.required')'
                },
                street: { 
                    required: '@lang('validation.required')'
                },
                profile_pic: { 
                    accept: '@lang('validation.img_upload')'
                },
                cover_pic: { 
                    accept: '@lang('validation.img_upload')'
                },
                phone: { 
                    required: '@lang('validation.required')',
                    digits:'@lang('validation.error_phone')'
                },
                email_1:{
                    required:'@lang('validation.required')'
                },
                email: { 
                    required: '@lang('validation.required')'
                },
            },
            errorPlacement: function (error, element) 
            {
                console.log(element.attr("name"));
                // console.log(error);
                if (element.attr("name") == "fname") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorFname').html(error);
                }
                if (element.attr("name") == "lname") {
                    var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorLname').html(error);
                }
                if (element.attr("name") == "comp_desc[]") {
                    var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.desc').html(error);
                }
                
                if (element.attr("name") == "bank_name") {
                    var error = '<label for="bn" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorBank_name').html(error);
                }
                if (element.attr("name") == "iban_number") {
                    var error = '<label for="ibn" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorIban_number').html(error);
                }
                if (element.attr("name") == "account_name") {
                    var error = '<label for="accnm" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorAccount_name').html(error);
                }
                if (element.attr("name") == "account_number") {
                    var error = '<label for="accnm" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorAccount_number').html(error);
                }
                if (element.attr("name") == "payment_mode") {
                    var error = '<label for="accnm" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorPaymentMode').html(error);
                }
                
                if (element.attr("name") == "email_1") {
                    var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.error_email_1').html(error);
                }
                if (element.attr("name") == "street") {
                    var error = '<label for="str" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorStreet').html(error);
                }
                if (element.attr("name") == "country") {
                    var error = '<label for="cty" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorCountry').html(error);
                }
                if (element.attr("name") == "city") {
                    var error = '<label for="cty" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorCity').html(error);
                }
                // if (element.attr("name") == "state") {
                //     var error = '<label for="ste" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                //     $('.errorState').html(error);
                // }
                if (element.attr("name") == "zip") {
                    var error = '<label for="zp" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'Please provide valid zipcode & it should be in digits only!'+'</label>';
                    $('.errorZip').html(error);
                }
                if (element.attr("name") == "phone") {
                    var error = '<label for="phn" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.error_phn')'+'</label>';
                    $('.errorZip').html(error);
                }
                if (element.attr("name") == "phone") {
                    var error = '<label for="phn" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorPhone').html(error);
                }
                if (element.attr("name") == "email") {
                    var error = '<label for="e" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorEmail').html(error);
                }
                if (element.attr("name") == "company_name") {
                    var error = '<label for="cn" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorCompanyName').html(error);
                }
                if (element.attr("name") == "profile_pic") {
                    var error = '<label for="pro" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.upload_img_extension_jpg_jpeg_png')'+'</label>';
                    $('.errorPic').html(error);
                }
                if (element.attr("name") == "cover_pic") {
                    var error = '<label for="pro" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.upload_img_extension_jpg_jpeg_png')'+'</label>';
                    $('.errorcoverPic').html(error);
                }
            }
        });
$("#saveNewMerchant").click(function(){
    $("#editMerchant").submit();
});
        $('#kuwait_city').keyup(function () {
        var city = $(this).val();
        // alert(city);
        if (city != '') {
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('admin.fetch.city.all') }}",
                method: "POST",
                data: {
                    city: city,
                    _token: _token
                },
                success: function (response) {
                    if (response.error) {
                        // alert("error");
                    } 
                    else
                    {
                        // alert("success");
                        var cityHtml = '<ul><li data-id="" class="kuwait_cities" data-nm="">Select City</li>';
                        response.result.cities.forEach(function (item, index) {
                            cityHtml = cityHtml + '<li class="kuwait_cities" data-id="' + item.city_details_by_language.city_id + '" data-nm="' + item.city_details_by_language.name + '">' + item.city_details_by_language.name + '</li>';
                        })
                        cityHtml = cityHtml + '</ul>';

                        $('#cityList').show();
                        $('#cityList').html(cityHtml);
                    }

                }
            });
        }

    });
    $(document).on("keyup", "#kuwait_city", function () {
        $("#k_cityname").val($.trim($("#kuwait_city").val()));
        var value = $.trim($(this).val());
        if (value == "") {
            $("#kuwait_city_value_id").val("");
        }
    });
    $(document).on("blur", "#kuwait_city", function () {
        var id = $.trim($("#kuwait_city_value_id").val());
        var name = $.trim($("#kuwait_city_value_name").val());
        var city = $.trim($("#kuwait_city").val());
        if (city != name) {
            $("#kuwait_city").val("");
            $("#kuwait_city_value_id").val("");
            $("#kuwait_city_value_name").val("");
            $(".city_id_err").text("@lang('admin_lang.please_select_name_city')");
        }else 
        {
            $(".city_id_err").text("");
        }
    });
    $('body').on('click', '.cityChange', function () {
        $('#kuwait_city').val($(this).text());
        $('#cityList').fadeOut();
    });
    $("body").click(function () {
        $("#cityList").fadeOut();
    });
    $(document).on("click", ".kuwait_cities", function (e) {
        var id = $(e.currentTarget).attr("data-id");
        $("#kuwait_city_value_id").val(id);
    });
    $(document).on("click", ".kuwait_cities", function (e) {
        var id = $(e.currentTarget).attr("data-id");
        var name = $(e.currentTarget).attr("data-nm");
        $("#kuwait_city_value_id").val(id);
        $("#kuwait_city_value_name").val(name);

        $("#kuwait_city").val(name);
        $(".city_id_err").text("");
        $("error_kuwait_city").text("");
    });
}); 


</script>
<script type="text/javascript">
    function validate(evt) {
        var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]/;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>
<script>

    $(document).ready(function(){
        $('body').on('click','.remove-image',function(){
            var var_id = $(this).data('id');
            var reqDataa = {
                jsonrpc: '2.0'
            };

            $.ajax({
                type:'GET',
                url : '{{ url('admin/remove-merchant-image') }}/'+var_id,
                data:reqDataa,
                success:function(response){
                    if(response.sucess) {
                        $(".remove-rw-"+var_id).hide();
                    } else {
                        $(".remove-rw-"+var_id).hide();
                    }
                }
            });     

        });   

    });

</script>

<script>
function initMap() {
    var input = document.getElementById('pac-input');
    var autocomplete = new google.maps.places.Autocomplete(input);
    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    autocomplete.setTypes(['address']);

    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
      $('#lat').val(place.geometry.location.lat())
      $('#lng').val(place.geometry.location.lng())
    });
}
$(document).ready(function() {
    $('#pac-input').blur(function() {
        if($(this).val() == '') {
            $('#lat').val('')
            $('#lng').val('')
        }
    })
})
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API') }}&libraries=places&callback=initMap" async defer></script>
@endsection