@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.manage_merchant')
@endsection
@section('content')
@section('links')
@include('admin.includes.links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
{{-- content --}}
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">@lang('admin_lang.manage_sub_admin')</h2>
                <p class="pageheader-text">Manage Merchant</p>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                @lang('admin_lang.manage_merchant')
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="mail_success" style="display: none;">
        <div class="alert alert-success vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
        </div>
    </div>
    <div class="mail_error" style="display: none;">
        <div class="alert alert-danger vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong>Error!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
        </div>
    </div>
    @if (session()->has('success'))
    <div class="alert alert-success vd_hidden" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
    </div>
    @elseif ((session()->has('error')))
    <div class="alert alert-danger vd_hidden" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
    </div>
    @endif
    <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong class="success_msg">Success!</strong> 
    </div>
    <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong class="error_msg"></strong> 
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                {{-- <h5 class="card-header">
                    @lang('admin_lang.merchant_management')
                    <a class="adbtn btn btn-primary" href="{{ route('admin.add.merchant') }}"><i class="fas fa-plus"></i> @lang('admin_lang.add')</a>
                </h5> --}}
                <div class="card-body">
                    <form action="javascript:;" id="searchMerchant" method="post">
                        @csrf
                        <div class="row">
                            <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <label for="keyword_lb" class="col-form-label">@lang('admin_lang.keyword')</label>
                                <input id="col1_filter" name="keyword" type="text" class="form-control keyword" placeholder="@lang('admin_lang.keyword')" value="{{ @$key['keyword'] }}">
                            </div>
                            <div data-column="2" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <label for="status" class="col-form-label">@lang('admin_lang.status_1')</label>
                                <select class="form-control status" id="col2_filter" name="status">
                                    <option value="">@lang('admin_lang.select_status')</option>
                                    <option value="A" @if(@$key['status'] == "A") selected @endif>@lang('admin_lang.active')</option>
                                    <option value="I" @if(@$key['status'] == "I") selected @endif>@lang('admin_lang.in_active')</option>
                                    <option value="U" @if(@$key['status'] == "U") selected @endif>@lang('admin_lang.un_verified')</option>
                                    <option value="AA" @if(@$key['status'] == "AA") selected @endif>@lang('admin_lang.waiting_approval')</option>
                                </select>
                            </div>
                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                {{-- <label for="inputPassword" class="col-form-label"></label> --}}
                                <a id="search" href="javascript:void(0)" class="btn btn-primary fstbtncls">@lang('admin_lang.search')</a>

                                <input type="reset" value="Reset Search" class="btn btn-default fstbtncls reset_search">
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive listBody">
                        <table class="table table-striped table-bordered first" id="merchants">
                            <thead>
                                <tr>
                                    <th>@lang('admin_lang.name')</th>
                                    <th>@lang('admin_lang.email')</th>
                                    <th>@lang('admin_lang.phone')</th>
                                    {{-- <th>@lang('admin_lang.drivers')</th> --}}
                                    {{-- <th>@lang('admin_lang.internal_shipping_cost')</th> --}}
                                    {{-- <th>@lang('admin_lang.external_shipping_cost')</th> --}}
                                    <th>@lang('admin_lang.review_ratings')</th>
                                    <th>@lang('admin_lang.total_order') ({{ getCurrency() }})</th>
                                    <th>@lang('admin_lang.total_paid') ({{ getCurrency() }})</th>
                                    <th>@lang('admin_lang.total_due') ({{ getCurrency() }})</th>
                                    {{-- <th>@lang('admin_lang.premium_merchant')</th> --}}
                                    {{-- <th>@lang('admin_lang.international_order')</th> --}}
                                    <th>@lang('admin_lang.hide_merchant')</th>
                                    <th>@lang('admin_lang.register_date')</th>
                                    <th>@lang('admin_lang.status')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>@lang('admin_lang.name')</th>
                                    <th>@lang('admin_lang.email')</th>
                                    <th>@lang('admin_lang.phone')</th>
                                    {{-- <th>@lang('admin_lang.drivers')</th> --}}
                                    {{-- <th>@lang('admin_lang.internal_shipping_cost')</th> --}}
                                    {{-- <th>@lang('admin_lang.external_shipping_cost')</th> --}}
                                    <th>@lang('admin_lang.review_ratings')</th>
                                    <th>@lang('admin_lang.total_order') ({{ getCurrency() }})</th>
                                    <th>@lang('admin_lang.total_paid') ({{ getCurrency() }})</th>
                                    <th>@lang('admin_lang.total_due') ({{ getCurrency() }})</th>
                                    {{-- <th>@lang('admin_lang.premium_merchant')</th> --}}
                                    {{-- <th>@lang('admin_lang.international_order')</th> --}}
                                    <th>@lang('admin_lang.hide_merchant')</th>
                                    <th>@lang('admin_lang.register_date')</th>
                                    <th>@lang('admin_lang.status')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table  -->
        <!-- ============================================================== -->
    </div>
</div>
<!-- footer -->
<!-- ============================================================== -->
@include('admin.includes.footer')
<!-- ============================================================== -->
<!-- end footer -->
<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>
<div class="modal fade" id="commissionModalCenter" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    @lang('admin_lang.set_commission_label')
                </h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="saveCommissionForm" method="post" action="">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input id="merchant_c_email" name="merchant_c_email" type="hidden" class="form-control" placeholder="" readonly>
                                <input id="merchant_c_name" name="merchant_c_name" type="hidden" class="form-control" placeholder="" readonly>
                                <input id="merchant_c_id" name="merchant_c_id" type="hidden" class="form-control" placeholder="" readonly>
                                <label for="commission">
                                Commission(in % with 2 decimal places)
                                </label>
                                <input id="commission" name="commission" type="number" class="form-control" placeholder="Commission" onkeypress='validate(event)'>
                                <span class="error_com" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="saveCommission" class="btn btn-primary popbtntp">@lang('admin_lang.Save')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="sendMailNotificationModalCenter" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    @lang('admin_lang.send_email_notification')
                </h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="sendEmailForm" method="post" action="">
                        @csrf
                        <div class="form-row">
                            {{-- 
                            <div class="form-group col-md-12"> --}}
                                {{-- <label id="title">Email-id</label> --}}
                                <input id="merchant_email" name="merchant_email" type="hidden" class="form-control" placeholder="" readonly>
                                <input id="merchant_name" name="merchant_name" type="hidden" class="form-control" placeholder="" readonly>
                                <input id="merchant_id" name="merchant_id" type="hidden" class="form-control" placeholder="" readonly>
                                {{-- <span class="error_title" style="color: red;"></span> --}}
                                {{-- 
                            </div>
                            --}}
                            <div class="form-group col-md-12">
                                <label id="titleLabel">@lang('admin_lang.title')</label>
                                <input id="title" name="title" type="text" class="form-control required" placeholder="Title">
                                <span class="error_title" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label id="messageLabel">@lang('admin_lang.msg')</label>
                                <textarea id="message" name="message" class="form-control required" rows="5"></textarea>
                                <span class="error_message" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="sendEmailButton" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="shippingCostModalCenter" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.shp_cost')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="saveShippingCostForm" method="post" action="">
                        @csrf
                        <div class="form-row">
                            <input id="email_m" name="email_m" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="name_m" name="name_m" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="id_m" name="id" type="hidden" class="form-control" placeholder="" readonly>
                            <div class="form-group col-md-12">
                                <label>
                                @lang('admin_lang.external_shipping_cost')
                                </label>
                                <input id="shipping_cost" name="shipping_cost" type="number" class="form-control" placeholder='@lang('admin_lang.shipping_cst')' onkeypress='validate(event)'>
                                <span class="error_cost" style="color: red;"></span>
                                <span class="error_shipping_cost" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label>
                                @lang('admin_lang.internal_shipping_cost')
                                </label>
                                <input id="internal_shipping_cost" name="internal_shipping_cost" type="number" class="form-control" placeholder='@lang('admin_lang.internal_shipping_cost')' onkeypress='validate(event)'>
                                <span class="error_internal_shipping_cost" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="saveShippingCost" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="reassign_driver_popup_show" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.assign_driver')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="driver_reassign_form" method="post" action="javascript:void(0)">
                        @csrf
                        <div class="form-row">
                            <input id="order_id_reassign" name="order_id_reassign" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="order_type_driver_reassign" name="order_type_driver_reassign" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="status_reassign" name="status_reassign" type="hidden" class="form-control" placeholder="" readonly>
                            <div class="form-group col-md-12">
                                <label>
                                @lang('admin_lang.assign_driver')
                                </label>
                                <select id="reassign" name="reassign" class="form-control">
                                    <option value="">@lang('admin_lang.select_driver')</option>
                                    @foreach(@$drivers as $driver)
                                    <option value="{{ @$driver->id }}">{{ @$driver->fname }} {{ @$driver->lname }}</option>
                                    @endforeach
                                </select>
                                <span class="error_reassign_driver" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12 reassign_type_div" style="display: none;">
                                <label for="specific" class="col-form-label">Assign type</label>    
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;Assign entire order to this driver
                                <input type="radio" name="assign_type" class="type" value="EO">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;Assign All products of this Seller to this driver 
                                <input type="radio" name="assign_type" class="type" value="SS">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;None
                                <input type="radio" name="assign_type" class="type" value="N">
                                <span class="checkmark"></span>
                                </label>
                                {{-- Seller specific --}}
                                {{-- <label for="ord_sp">Order specific</label>
                                <input type="radio" id="ord_sp" name="order_specific" value="Order Specific" class="form-control" />Order specific
                                <label for="seller_sp">Seller specific</label>
                                <input type="radio" id="seller_sp" name="order_specific" value="Order Specific" class="form-control" />Seller specific --}}
                                {{-- <span class="error_assign_driver" style="color: red;"></span> --}}
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="reassignDriver" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="merchant_setting_popup" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.merchant_settings')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <div class="col-md-12">
                        <span class="show_merchat_msg"></span>
                    </div>
                    <form id="merchant_setting_form" method="post" action="javascript:void(0)">
                        @csrf
                        <input type="hidden" name="merchant_setting_id" value="" id="merchant_setting_id">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>@lang('admin_lang.minimum_commission')</label>
                                <input type="text" name="minimum_commission" id="minimum_commission" class="form-control">
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-6">

                                <input type="checkbox" name="show_order_processed" id="show_order_processed" value="Y">
                                <label>@lang('admin_lang.show_order_processed')</label>
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-6">
                                <input type="checkbox" name="hide_customer_info" id="hide_customer_info" value="Y">
                                <label>@lang('admin_lang.hide_customer_info')</label>
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-6">

                                <input type="checkbox" name="notify_merchant_on_delivery_internal" id="notify_merchant_on_delivery_internal" value="Y">
                                <label>@lang('admin_lang.notify_merchant_on_delivery_internal')</label>
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-6">
                                <input type="checkbox" name="notify_merchant_on_delivery_external" id="notify_merchant_on_delivery_external" value="Y">
                                <label>@lang('admin_lang.notify_merchant_on_delivery_external')</label>
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-6">
                                <input type="checkbox" name="notify_customer_by_email_internal" id="notify_customer_by_email_internal" value="Y">
                                <label>@lang('admin_lang.notify_customer_by_email_internal')</label>
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-6">
                                <input type="checkbox" name="notify_customer_by_email_external" id="notify_customer_by_email_external" value="Y">
                                <label>@lang('admin_lang.notify_customer_by_email_external')</label>
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-6">

                                <input type="checkbox" name="notify_customer_by_whatsapp_internal" id="notify_customer_by_whatsapp_internal" value="Y">
                                <label>@lang('admin_lang.notify_customer_by_whatsapp_internal')</label>
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-6">
                                <input type="checkbox" name="notify_customer_by_whatsapp_external" id="notify_customer_by_whatsapp_external" value="Y">
                                <label>@lang('admin_lang.notify_customer_by_whatsapp_external')</label>
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-6">
                                <input type="checkbox" name="applicable_city_delivery" id="applicable_city_delivery" value="Y">
                                <label>@lang('admin_lang.applicable_city_delivery_internal')</label>
                            </div>
                            <div class="form-group col-md-6 col-lg-6 col-sm-6">
                                <input type="checkbox" name="applicable_city_delivery_external" id="applicable_city_delivery_external" value="Y">
                                <label>@lang('admin_lang.applicable_city_delivery_external')</label>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="save_merchat_setting" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        // datatable
        $('body').on('click', '.reset_search', function() {
            $('#merchants').DataTable().search('').columns().search('').draw();
            $(".slct").val("").trigger("chosen:updated");
        })

        function filterColumn ( i ) {
            $('#merchants').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }
        
        $("#merchants_filter").hide();
        var styles = `
            table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
                right: 0em;
                content: "";
            }
            table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
                right: 0em;
                content: "";
            }
        `
        var styleSheet = document.createElement("style");
        styleSheet.innerText = styles;
        document.head.appendChild(styleSheet);
        $('#merchants').DataTable( {
            stateSave: false,
            "order": [[8, "desc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
                $('#col2_filter').val(data.search.status)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
                data.search.status = $('#col2_filter').val()
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "createdRow": function( row, data, dataIndex ) { 
                var id = (JSON.stringify(data.id)),
                r = JSON.stringify(dataIndex);
                // $(row).
                $(row).addClass("tr_class_"+id);
                $(row).addClass("tr_row_class");
            },
            "ajax": {
                "url": "{{ route('admin.list.merchant') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
                { 
                    data: 0,
                    render: function(data, type, full) {
                        return full.fname+' '+ full.lname
                    }
                },
                {   data: 'email' },
                {   data: 'phone' },
                // {   
                //     data: 'driver_id',
                //     render:function(data, type, full){
                //         if(data != 0){
                //             var fname = full.driver_details.fname,
                //             lname = full.driver_details.lname,
                //             name = fname+" "+lname;
                //             return '<span class="driver_name_'+full.id+'">'+name+'</span>';
                //         }else{
                //             return '<span class="driver_name_'+full.id+'">'+" "+'</span>';
                //         }
                        
                //     }
                // },
                // {   
                //     data: 'inside_shipping_cost',
                //     render: function(data, type, full) {
                //         return '<span class="inside_shipping_cst_'+full.id+'">'+data+'</span>'
                //     }
                // },
                // {   
                //     data: 'shipping_cost',
                //     render: function(data, type, full) {
                //         return '<span class="external_shipping_cost_'+full.id+'">'+data+'</span>'
                //     }
                // },
                {
                    data:'rate'
                },
                // {
                //     data:'commission',
                //     render:function(data, type, full){
                //         return ''+data+' % ';
                //     }
                // },
                {
                    data:'total_earning'
                },
                {
                    data:'total_paid'
                },
                {
                    data:'total_due'
                },
                // { 
                //     data: 'premium_merchant',
                //     render: function(data, type, full) {
                //         if(data == 'Y') {
                //             return '<span class="premium_merchant_'+full.id+'">@lang('admin_lang.yes')</span>'
                //         } else {
                //             return '<span class="premium_merchant_'+full.id+'">@lang('admin_lang.no')</span>'
                //         }
                //     }
                // },
                // { 
                //     data: 'international_order',
                //     render: function(data, type, full) {
                //         if(data == 'ON') {
                //             return '<span class="int_status_'+full.id+'">@lang('admin_lang.on')</span>'
                //         } else {
                //             return '<span class="int_status_'+full.id+'">@lang('admin_lang.off')</span>'
                //         }
                //     }
                // },
                {   
                    data: 'hide_merchant',
                    render: function(data, type, full) {
                        if(data == 'Y'){
                            return '<span class="hide_merchant_'+full.id+'">'+"Yes"+'</span>'
                        }else{
                            return '<span class="hide_merchant_'+full.id+'">'+"No"+'</span>'
                        }
                    }
                },
                {   
                    data: 'created_at',
                },
                { 
                    data: 'status',
                    render: function(data, type, full) {
                        if(data == 'A') {
                            return '<span class="status_'+full.id+'">@lang('admin_lang.Active')</span>'
                        } else if(data == 'I') {
                            return '<span class="status_'+full.id+'">@lang('admin_lang.Inactive')</span>'
                        } else if(data == 'U') {
                            return '<span class="status_'+full.id+'">@lang('admin_lang.un_verified')</span>'
                        } else if(data == 'AA') {
                            return '<span class="status_'+full.id+'">@lang('admin_lang.awaiting_approval')</span>'
                        }
                    }
                },
                { 
                    data: 0,
                    render: function(data, id, full, meta) {
                        var a = '';

                        a+= ' <a href="{{ url('admin/view-merchant-profile') }}/'+full.id+'"><i class="fas fa-eye" title="@lang('admin_lang.view')"></i></a>';
                        // show orders
                        // a+= ' <a class="get_orders" href="javascript:void(0)" data-id="'+full.id+'"><i class="fas fa-info" title="@lang('admin_lang.view')"></i></a>';
                        
                        // status
                        if(full.hide_merchant == 'Y') {
                            a +=' <a class="merchant_hide merchant_hide_'+full.id+'" href="javascript:void(0)" data-id="'+full.id+'" data-status="'+full.hide_merchant+'"><i class="fas fa-times icon_hide_change_'+full.id+'" title="Show Merchant"></i></a>'
                        } else if(full.hide_merchant == 'N') {
                            a += ' <a class="merchant_hide merchant_hide_'+full.id+'" href="javascript:void(0)" data-id="'+full.id+'" data-status="'+full.hide_merchant+'"><i class="fas fa-check icon_hide_change_'+full.id+'" title="Hide Merchant"></i></a>'
                        }

                        if(full.status == 'A') {
                            a +=' <a class="block_unblock_merchant" href="javascript:void(0)" data-id="'+full.id+'" data-status="A"><i class="fas fa-ban icon_change_'+full.id+'" title="@lang('admin_lang.block')"></i></a>'
                        } else if(full.status == 'I') {
                            a += ' <a class="block_unblock_merchant" href="javascript:void(0)" data-id="'+full.id+'" data-status="I"><i class="far fa-check-circle icon_change_'+full.id+'" title="@lang('admin_lang.unblock')"></i></a>'
                        } else if(full.status == 'U') {
                            a += ' <a href="javascript:void(0)" data-id="'+full.id+'" class="m_verify_account" data-status="'+full.id+'"><i class="fa fa-user-circle v_account_'+full.id+'" title="@lang('admin_lang.verify_merchant_account')"></i></a>'
                        } else if(full.status == 'AA') {
                            a += ' <a class="app_merchant_details" href="javascript:void(0)" data-id="'+full.id+'"><i class="fas fa-thumbs-up appr_merchant_'+full.id+'" title="@lang('admin_lang.approve')"></i></a>'
                        } 
                        //assign driver 
                        // a += ' <a href="javascript:void(0)" data-toggle="modal" data-target="#reassign_driver_popup_show" class="reassign_driver_popup reassign_dr_'+full.id+'" data-id="'+full.id+'" data-otype="'+full.id+'"><i class=" fas fa-male" title="Assign Driver"></i></a>'
                        //remove driver
                        // if(full.driver_id != 0){
                        // a += ' <a href="javascript:void(0)"  class="remove_driver remove_driver_'+full.id+'" data-id="'+full.id+'"><i class=" fas fa-times" title="Remove Driver"></i></a>'
                        // }else{
                        //     a += ' <a href="javascript:void(0)"  class="remove_driver remove_driver_'+full.id+'" data-id="'+full.id+'" style="display:none;"><i class=" fas fa-times" title="Remove Driver"></i></a>'
                        // }
                        // commission
                        a += ' <a href="javascript:void(0)" data-toggle="modal" data-target="" data-id="'+full.id+'" data-emailid="'+full.email+'" data-name="'+full.fname+' '+full.lname+'" data-commission="'+full.commission+'" class="commision_set set_com_value_'+full.id+'"><i class="fas fa-percent" title="@lang('admin_lang.commission')"></i></a>'
                        
                        // shipping cost

                        // a += ' <a href="javascript:void(0)" data-toggle="modal" data-target="" class="shipping_cost s_cost_'+full.id+'" data-id="'+full.id+'" data-emailid="'+full.email+'" data-name="'+full.fname+' '+full.lname+'" data-shippingcost="'+full.shipping_cost+'" data-internalshippingcost ="'+full.inside_shipping_cost+'"><i class="fas fa-truck" title="@lang('admin_lang.shipping_cst')"></i></a>'
                        // edit
                        if(full.city != ""){
                            a += ' <a href="{{ url('admin/edit-merchant') }}/'+full.id+'"><i class="fas fa-edit" title="@lang('admin_lang.Edit')"></i></a>'
                        }
                        // premium merchant status change
                        // a += ' <a class="premium_merchant_status" href="javascript:void(0)" data-id="'+full.id+'"><i class="far fa-money-bill-alt premium_'+full.id+'" title="Premium"></i></a>'
                        // international status
                        // a += '<a class="international_order_status" href="javascript:void(0)" data-id="'+full.id+'"><i class="fa fa-globe int_st_icon_'+full.id+'" title="@lang('admin_lang.international_order')"></i></a>'
                        
                        //merchant setting
                        // a += '<a class="show_merchant_settings" href="javascript:void(0)"  data-id="'+full.id+'"><i class="fa fa-cog" title="@lang('admin_lang.merchant_settings')"></i></a>'
                        // merchant address book
                        // a += '<a href="{{ url('admin/show-address-book') }}/'+full.id+'"  data-id="'+full.id+'"><i class="fa fa-address-book-o" title="@lang('admin_lang.address_book')"></i></a>'
                        //send email
                        // a += '<a href="javascript:void(0)" data-toggle="modal" data-id="'+full.id+'" data-emailid="'+full.email+'" data-name="'+full.fname+" "+full.lname+'" class="send_email_notify" data-target="" data-action="{{ url('admin/send-notification-merchant') }}/'+full.id+'"><i class="fas fa-paper-plane" title="@lang('admin_lang.send_email')"></i></a>'
                        // delete
                        a += ' <a class="delete_merchant" href="javascript:void(0)" data-id="'+full.id+'"><i class="fas fa-trash d_m_'+full.id+'" title="@lang('admin_lang.Delete')"></i></a>'

                        return '<span class="action_col">'+a+'</span>';
                    }
                }
            ]
        });

        // change event
        $('input.keyword').on( 'keyup click blur', function () {
            filterColumn( 1 );
        });
        $('.status').on( 'change', function () {
            filterColumn( 2 );
        });

        $(document).on("click",".reassign_driver_popup",function(e){
            var id= $.trim($(e.currentTarget).attr("data-id"));
            $('#reassign_driver').attr("data-id",id);
            $('#reassign_driver_popup_show').modal('hide');
            $('#assign_driver_popup_show').modal('hide');
            $("#order_id_reassign").val(id);
        });
        $(document).on("click","#reassignDriver",function(e){
            var assign = $.trim($("#reassign option:selected").val());
            var name = $.trim($("#reassign option:selected").text());
            if(assign == ""){
                $(".error_assign_driver").text("@lang('validation.required')");
            }else{
                $(".error_assign_driver").text("");
            }
            
            if(assign != ""){
                var id = $("#order_id_reassign").val();
                $('#reassign_driver_popup_show').modal('hide');
                $.ajax({
                    url:"{{ route('admin.add.driver.assign.auto') }}",
                    type:"GET",
                    data:{
                        id:id,
                        assign:assign
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".success_msg").html("@lang('admin_lang.suc_driver_assined')");
                            $(".remove_driver_"+id).show();
                            $(".driver_name_"+id).text(name);
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.err_driver_assined')");
                        }
                    }
                });
            }
        });
        $(document).on("click",".get_orders",function(e){
            var merchant = $(this).attr("data-id");
            if(merchant != ""){
                $.ajax({
                    url:"{{ route('admin.list.order') }}",
                    data:{
                        merchant:merchant
                    },
                    success:function(res){
                        
                    }
                });
            }
        });
        $(document).on("click","#sendEmailButton",function(e){
            
            var title = $.trim($("#title").val()),
            id = $.trim($("#merchant_id").val()),
            merchant_email = $.trim($("#merchant_email").val()),
            merchant_name = $.trim($("#merchant_name").val()),
            message = $.trim($("#message").val());
            // alert(merchant_email);
            if(title == ""){
                $(".error_title").text('@lang('validation.title_error')');
            }else{
                $(".error_title").text("");
            }
            if(message == ""){
                $(".error_message").text('@lang('validation.message_error')');
            }else{
                $(".error_message").text("");
            }
            if(title != "" && message != ""){
                // $("#sendEmailForm").submit();
                var rurl = '{{ route('admin.send.email.notification') }}';
                $('#sendMailNotificationModalCenter').modal('hide');
                $(".mail_success").css({
                    "display":"block"
                });
                $(".successMsg").text('@lang('admin_lang.mail_sending')');
                $(".mail_error").css({
                    "display":"none"
                });
                $.ajax({
                    type:"GET",
                    url:rurl,
                    data: {
                        id : id,
                        name : merchant_name,
                        email : merchant_email,
                        title : title,
                        message : message
                    },
                    success:function(resp){
                        if(resp==1){
                            $(".mail_success").css({
                                "display":"block"
                            });
                            $(".successMsg").text('@lang('admin_lang.msg_snt')');
                            
                            $(".mail_error").css({
                                "display":"none"
                            });
                            $("#title").val("");
                            $("#message").val("");
                        }else{
                            $(".mail_error").css({
                                "display":"block"
                            });
                            
                            $(".errorMsg").text('@lang('admin_lang.unauthorized_msg_not_sent')');
                            $(".mail_success").css({
                                "display":"none"
                            });
                        }
                    }
                });
            }
        });
        
        $(document).on("click",".send_email_notify",function(e){
            var id= $.trim($(e.currentTarget).attr("data-id")),
            email_id= $.trim($(e.currentTarget).attr("data-emailid")),
            name= $.trim($(e.currentTarget).attr("data-name")),
            action= $.trim($(e.currentTarget).attr("data-action"));
            
            $('#sendMailNotificationModalCenter').attr("data-id",id);
            $('#sendMailNotificationModalCenter').attr("data-emailid",email_id);
            $('#sendMailNotificationModalCenter').attr("data-name",name);
            $('#sendMailNotificationModalCenter').modal('show');
            $("#merchant_email").val(email_id);
            $("#merchant_name").val(name);
            $("#merchant_id").val(id);
            $("#sendEmailForm").attr("action",action);
            
        });

        
        $(document).on("click","#saveCommission",function(e){
            var com = $.trim($("#commission").val());
            if(com != ""){
                var commission = parseFloat(com),
                merchant_email = $.trim($("#merchant_c_email").val()),
                id = $.trim($("#merchant_c_id").val()),
                merchant_name = $.trim($("#merchant_c_name").val());
                commission = commission.toFixed(2);
    
                if(com == ""){
                    $(".error_com").text('@lang('validation.required')');
                }else{
                    var rurl = '{{ route('admin.set.commision') }}';
                    $.ajax({
                        type:"GET",
                        url:rurl,
                        data: {
                            id : id,
                            name : merchant_name,
                            email : merchant_email,
                            commission : commission,
                        },
                        success:function(resp){
                            // alert(resp);
                            if(resp != null){
                                $(".mail_success").css({
                                    "display":"block"
                                });
                                $(".successMsg").text("@lang('admin_lang.com_updated_suc')");
                                $(".mail_error").css({
                                    "display":"none"
                                });
                                $("#commission").val("");
                                $(".error_com").text("");
                                $('#commissionModalCenter').modal('hide');
                                $(".set_com_value_"+id).attr("data-commission",commission);
                            }else{
                                $(".mail_error").css({
                                    "display":"block"
                                });
                                $(".errorMsg").text("@lang('admin_lang.unauth_acces_com_nt_updated')");
                                $(".mail_success").css({
                                    "display":"none"
                                });
                            }
                        }
                    });
                    
                }
            }else{
                $(".error_com").text('@lang('admin_lang.comission_cannot_left_blank')');
            }
        });
        $(document).on("click",".commision_set",function(e){
            var id= $.trim($(e.currentTarget).attr("data-id")),
            email_id= $.trim($(e.currentTarget).attr("data-emailid")),
            commission= $.trim($(e.currentTarget).attr("data-commission")),
            name= $.trim($(e.currentTarget).attr("data-name"));
            $('#commissionModalCenter').attr("data-id",id);
            $('#commissionModalCenter').attr("data-emailid",email_id);
            $('#commissionModalCenter').attr("data-name",name);
            $('#commissionModalCenter').modal('show');
            $("#merchant_c_email").val(email_id);
            $("#merchant_c_name").val(name);
            $("#merchant_c_id").val(id);
        
            if(commission == "NaN"){
                $("#commission").val("");
            }else{
                $("#commission").val(commission);
            }
        });

        $(document).on("click","#saveShippingCost",function(e){
            var cost = $.trim($("#shipping_cost").val());
            var internal_shipping_cost = $.trim($("#internal_shipping_cost").val());
            if(cost != "" && internal_shipping_cost != ""){
                var shipping_cost = parseFloat(cost),
                internal_shipping_cost = parseFloat(internal_shipping_cost),
                merchant_email = $.trim($("#merchant_email").val()),
                id = $.trim($("#id_m").val()),
                merchant_name = $.trim($("#merchant_name").val());
                shipping_cost = shipping_cost.toFixed(3);
                internal_shipping_cost = internal_shipping_cost.toFixed(3);
                if(shipping_cost == "") {
                    $(".error_cost").text('@lang('admin_lang.please_provide_shipping_cost')');
                } else if(internal_shipping_cost == "") {
                    $(".error_internal_shipping_cost").text('@lang('admin_lang.please_provide_internal_shipping_cost')');
                } else {
                    var rurl = '{{ route('admin.set.shipping.cost') }}';
                    $.ajax({
                        type:"GET",
                        url:rurl,
                        data: {
                            id : id,
                            name : merchant_name,
                            email : merchant_email,
                            shipping_cost : shipping_cost,
                            internal_shipping_cost : internal_shipping_cost
                        },
                        beforeSend:function(){
                            $(".loader").show();
                        },
                        success:function(resp){
                            if(resp != null){
                                $(".mail_success").css({
                                    "display":"block"
                                });
                                $(".successMsg").text("@lang('admin_lang.shipping_cost_upd_suc')");
                                $(".mail_error").css({
                                    "display":"none"
                                });
                                $("#commission").val("");
                                $(".error_cost").text("");
                                $(".s_cost_"+id).attr("data-shippingcost",resp.shipping_cost);
                                $(".s_cost_"+id).attr("data-internalshippingcost",resp.inside_shipping_cost);
                                $(".inside_shipping_cst_"+id).text(resp.inside_shipping_cost);
                                $(".external_shipping_cost_"+id).text(resp.shipping_cost);
                                $('#shippingCostModalCenter').modal('hide');
                            }else{
                                $(".mail_error").css({
                                    "display":"block"
                                });
                                $(".errorMsg").text("@lang('admin_lang.unauth_acc_ship_cost_updated')");
                                $(".mail_success").css({
                                    "display":"none"
                                });
                            }
                            $(".loader").hide();
                        }
                    });
                }
            }else{
                $(".error_cost").text('@lang('admin_lang.please_provide_internal_shipping_cost')');
                if(shipping_cost == ""){
                }
                if(internal_shipping_cost == ""){
                    $(".error_internal_shipping_cost").text('@lang('admin_lang.please_provide_internal_shipping_cost')');
                }
            }
        });
        $(document).on("click",".shipping_cost",function(e){
            var id= $.trim($(e.currentTarget).attr("data-id")),
            email_id= $.trim($(e.currentTarget).attr("data-emailid")),
            shippingcost= $.trim($(e.currentTarget).attr("data-shippingcost")),
            internalshippingcost= $.trim($(e.currentTarget).attr("data-internalshippingcost")),
            name= $.trim($(e.currentTarget).attr("data-name"));
            $('#shippingCostModalCenter').attr("data-id",id);
            $('#shippingCostModalCenter').attr("data-emailid",email_id);
            $('#shippingCostModalCenter').attr("data-name",name);
            $('#shippingCostModalCenter').modal('show');
            $("#email_m").val(email_id);
            $("#name_m").val(name);
            $("#id_m").val(id);
            $("#id_m").attr({
                "value":id
            });
            if(shippingcost == "NaN"){
                $("#shipping_cost").val("");
            }else{
                $("#shipping_cost").val(shippingcost);
            }
            if(internalshippingcost == "NaN"){
                $("#internal_shipping_cost").val("");
            }else{
                $("#internal_shipping_cost").val(internalshippingcost);
            }
        });

        $(document).on("click",".m_verify_account",function(e){
            var id = $(e.currentTarget).attr("data-id"),
            status = $(e.currentTarget).attr("data-status");
    
            if(confirm('Do you want to verify this merchant account ?')){
                $.ajax({
                    url:"{{ route('admin.verify.merchant.email.admin') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".status_"+id).html("Active");
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".success_msg").html("@lang('admin_lang.suc_mer_verified_suc_mer_login')");
                            $(".v_account_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.unauth_acc_mer_nt_v')");
                        }
                    }
                });
            }
        });

        $(document).on("click",".app_merchant_details",function(e){
            var id = $(e.currentTarget).attr("data-id");
            if(confirm("@lang('admin_lang.do_u_want_appro_merchant_details')")){
                $.ajax({
                    url:"{{ route('admin.approve.merchant.details') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".status_"+id).html("Active");
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".success_msg").html("@lang('admin_lang.suc_merchant_approved_suc')");
                            $(".appr_merchant_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.unauth_mer_is_nt_approved')");
                        }
                    }
                });
            }
        });

        // international order status change
        $(document).on("click",".international_order_status",function(e){
            var id = $(e.currentTarget).attr("data-id");
            if(confirm('Do you want to change the status of the international order for this merchant ?')){
                $.ajax({
                    url:"{{ route('admin.international.order.status.change') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp != null){
                            $(".int_status_"+id).html(resp.international_order);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".success_msg").html("@lang('admin_lang.succ_international_ord_changed')");
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.unau_iner_ord_change')");
                        }
                    }
                });
            }
            // }
        });
        // premium merchant status change
        $(document).on("click",".premium_merchant_status",function(e){
            var id = $(e.currentTarget).attr("data-id");
            if(confirm("@lang('admin_lang.do_u_prem_not_merch')")){
                $.ajax({
                    url:"{{ route('admin.premium.merchant.status.change') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp != null){
                            var st="";
                            if(resp.premium_merchant == 'Y'){
                                st="Yes";
                                $(".success_msg").html("@lang('admin_lang.suc_merchant_is_prem_merchant')");
                            }else{
                                st="No";
                                $(".success_msg").html("@lang('admin_lang.suc_merchant_is_not_prem_merchant')");
                            }
                            $(".premium_merchant_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.unauth_acc_pl_try_again')");
    
                        }
                    }
                });
            }
            // }
        });

        $(document).on("click",".merchant_hide",function(e){
            var id = $(e.currentTarget).attr("data-id"),
            status = $(e.currentTarget).attr("data-status"),
            $status_que = "";
            if(status == 'Y'){
                $status_que = "@lang('admin_lang.do_u_hide_show_merchant')";
            }else{
                $status_que = "@lang('admin_lang.do_u_hide_show_merchant')";   
            }
            if(confirm($status_que)){
                $.ajax({
                    url:"{{ route('admin.hide.show.merchant') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp != 0){
                            var st="";
                            if(resp.hide_merchant == 'Y' || resp.hide_merchant == "Y"){
                                st="@lang('admin_lang.yes')";
                                $(".success_msg").html("@lang('admin_lang.merchant_hide_product_suc')");
                                $(".icon_hide_change_"+id).removeClass("fas fa-check");
                                $(".icon_hide_change_"+id).addClass("fas fa-times");
                                $(".icon_hide_change_"+id).attr("title","@lang('admin_lang.show_merchant')");
                            }else if(resp.hide_merchant == 'N' || resp.hide_merchant == "N"){
                                st="No";
                                $(".success_msg").html("@lang('admin_lang.merchant_show_product_suc')");
                                $(".icon_hide_change_"+id).removeClass("fas fa-times");
                                $(".icon_hide_change_"+id).addClass("fas fa-check");
                                $(".icon_hide_change_"+id).attr("title","@lang('admin_lang.hide_merchant')");
                            }
                            $(".hide_merchant_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.unauthh_accc_mer_changed')");
                        }
                    }
                });
            }
        });
        // block unblock merchant
        $(document).on("click",".block_unblock_merchant",function(e){
            var id = $(e.currentTarget).attr("data-id"),
            status = $(e.currentTarget).attr("data-status"),
            status_que = "",
            obj = $(e.currentTarget);
            if(status == 'A'){
                status_que = "Do you want to block this merchant ?";
            }else{
                status_que = "Do you want to unblock this merchant ?";   
            }
            if(confirm(status_que)){
                $.ajax({
                    url:"{{ route('admin.block.unblock.merchant') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp != null){
                            var st="";
                            if(resp.status == 'A'){
                                st="Active";
                                $(".success_msg").html("@lang('admin_lang.suc_merc_unblocked')");
                                $(".icon_change_"+id).removeClass("far fa-check-circle");
                                // $(".icon_change_"+id).removeClass("");
                                $(".icon_change_"+id).addClass("fas fa-ban");
                                $(".icon_change_"+id).attr("title","@lang('admin_lang.block')");
                                obj.attr("data-status","A");
                            }else if(resp.status == 'I'){
                                st="Inactive";
                                $(".success_msg").html("@lang('admin_lang.suc_mer_blocked')");
                                $(".icon_change_"+id).removeClass("fas fa-ban");
                                $(".icon_change_"+id).addClass("far fa-check-circle");
                                $(".icon_change_"+id).attr("title","@lang('admin_lang.unblock')");
                                obj.attr("data-status","I");
                            }
                            $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.unauth_acc_status_merchant_not_changed')");
    
                        }
                    }
                });
            }
            // }
        });

        $(document).on("click",".remove_driver",function(e){
            var id = $(e.currentTarget).attr("data-id");
            var $this = $(this);
            if(confirm("@lang('admin_lang.do_u_remove_driver')")){
                $.ajax({
                    url:"{{ route('admin.remove.driver.assign.auto') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".success_msg").html("@lang('admin_lang.suc_driv_rem_suc')");
                            // $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".driver_name_"+id).text("");
                            $(".remove_driver_"+id).hide();
                            // $(this).hide();
                            // $(".tr_"+id).hide();
                            // $this.parent().parent().remove();
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.unauth_acc_driv_not_removed')");
                        }
                    }
                });
            }

        });
        $(document).on("click",".delete_merchant",function(e){
            var id = $(e.currentTarget).attr("data-id");
            var obj = $(this);
            if(confirm("@lang('admin_lang.do_u_delete_merchant')")){
                $.ajax({
                    url:"{{ route('admin.delete.merchant.permanently') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".success_msg").html("@lang('admin_lang.suc_mer_deleted_suc')");
                            // $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".tr_class_"+id).hide();
                            // obj.parent().parent().parent().remove();
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.unauth_acc_mer_nt_del_mer_product')");
                        }
                    }
                });
            }
            // }
        });

        // show merchant setting modal with data

        $(document).on('click','.show_merchant_settings',function(e){
            var id = $(this).attr('data-id');
            var reqdata = {
                'jsonrpc':'2.0',
                '_token':"{{ @csrf_token() }}",
                'data':{
                    'id':id
                }
            }
            $.ajax({
                url:"{{ route('admin.get.merchant.setting') }}",
                type:"POST",
                data:reqdata,
                success:function(resp){
                    if(resp.result != null){
                        if(resp.result.minimum_commission != null){
                            $('#minimum_commission').val(resp.result.minimum_commission);
                        }else{
                            $('#minimum_commission').val('');
                        }

                        if(resp.result.show_order_processed == 'Y'){
                            $('#show_order_processed').prop('checked',true)
                        }else{
                            $('#show_order_processed').prop('checked',false)
                        }

                        if(resp.result.hide_customer_info == 'Y'){
                            $('#hide_customer_info').prop('checked',true)
                        }else{
                            $('#hide_customer_info').prop('checked',false)
                        }

                        if(resp.result.notify_merchant_on_delivery_internal == 'Y'){
                            $('#notify_merchant_on_delivery_internal').prop('checked',true)
                        }else{
                            $('#notify_merchant_on_delivery_internal').prop('checked',false)
                        }

                        if(resp.result.notify_merchant_on_delivery_external == 'Y'){
                            $('#notify_merchant_on_delivery_external').prop('checked',true)
                        }else{
                            $('#notify_merchant_on_delivery_external').prop('checked',false)
                        }

                        if(resp.result.notify_customer_by_email_internal == 'Y'){
                            $('#notify_customer_by_email_internal').prop('checked',true)
                        }else{
                            $('#notify_customer_by_email_internal').prop('checked',false)
                        }

                        if(resp.result.notify_customer_by_whatsapp_internal == 'Y'){
                            $('#notify_customer_by_whatsapp_internal').prop('checked',true)
                        }else{
                            $('#notify_customer_by_whatsapp_internal').prop('checked',false)
                        }

                        if(resp.result.notify_customer_by_email_external == 'Y'){
                            $('#notify_customer_by_email_external').prop('checked',true)
                        }else{
                            $('#notify_customer_by_email_external').prop('checked',false)
                        }

                        if(resp.result.notify_customer_by_whatsapp_external == 'Y'){
                            $('#notify_customer_by_whatsapp_external').prop('checked',true)
                        }else{
                            $('#notify_customer_by_whatsapp_external').prop('checked',false)
                        }

                        if(resp.result.applicable_city_delivery == 'Y'){
                            $('#applicable_city_delivery').prop('checked',true)
                        }else{
                            $('#applicable_city_delivery').prop('checked',false)
                        }

                        if(resp.result.applicable_city_delivery_external == 'Y'){
                            $('#applicable_city_delivery_external').prop('checked',true)
                        }else{
                            $('#applicable_city_delivery_external').prop('checked',false)
                        }

                    }
                }
            });

            $('#merchant_setting_id').val(id);
            $('#merchant_setting_popup').modal('show');
        });

        // edit merchant settings
        $(document).on('click','#save_merchat_setting',function(){
            var form = $('#merchant_setting_form');
            $.ajax({
                url:"{{ route('admin.edit.merchant.setting') }}",
                type:"POST",
                dataType:"json",
                data:form.serialize(),
                success:function(resp){
                    if(resp.res == 1){
                        $('.show_merchat_msg').html("<div class='alert alert-success'>"+
                                                        "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+
                                                        "@lang('admin_lang.edit_merchant_success')"+
                                                    "</div>"
                                                    )
                    }else{
                        $('.show_merchat_msg').html("<div class='alert alert-danger'>"+
                                                        "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+
                                                        "@lang('admin_lang.error')"+
                                                    "</div>"
                                                    )
                    }
                }
            });
        })
    });
    // });
        function validate(evt) {
            var theEvent = evt || window.event;
                  // Handle paste
                  if (theEvent.type === 'paste') {
                      key = event.clipboardData.getData('text/plain');
                  } else {
                  // Handle key press
                  var key = theEvent.keyCode || theEvent.which;
                  key = String.fromCharCode(key);
              }
              var regex = /[0-9]|\./;
              if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }
</script>
@endsection