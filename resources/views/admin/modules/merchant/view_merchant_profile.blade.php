@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.view_merchant_profile')
@endsection
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
<div class="dashboard-ecommerce">
<div class="container-fluid dashboard-content ">
    <!-- ============================================================== -->
    <!-- pageheader  -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h3 class="mb-2">
                    @lang('admin_lang.merchant_view_profile')
                    <a class="adbtn btn btn-primary" href="{{ route('admin.list.merchant') }}" style="float: right;"><i class="fas fa-back"></i> @lang('admin_lang.back')</a>
                </h3>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                            <li class="breadcrumb-item" aria-current="page">
                                <a href="{{ route('admin.list.merchant') }}" class="breadcrumb-link">
                                    @lang('admin_lang.manage_merchant')
                                </a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.merchant_view_profile')</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (session()->has('success'))
    <div class="alert alert-success vd_hidden" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
    </div>
    @elseif ((session()->has('error')))
    <div class="alert alert-danger vd_hidden" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
    </div>
    @endif
    <!-- ============================================================== -->
    <!-- end pageheader  -->
    <!-- ============================================================== -->
    <!-- ============================================================== --> 
    <!-- ============================================================== --> 
    <!-- content --> 
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== --> 
        <!-- profile --> 
        <!-- ============================================================== -->
        <div class="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12">
            <!-- ============================================================== --> 
            <!-- card profile --> 
            <!-- ============================================================== -->
            <div class="card">
                <div class="card-body">
                    <div class="user-avatar text-center d-block"> 
                        @if(@$merchant->image)
                        @php
                        $image_path = 'storage/app/public/customer/profile_pics/'.@$merchant->image; 
                        @endphp
                        @if(file_exists(@$image_path))
                        <img id="profilePicture" src="{{ URL::to('storage/app/public/customer/profile_pics/'.@$merchant->image) }}" alt="User Avatar" class="rounded-circle user-avatar-xxl">
                        @endif
                        @endif
                        {{-- <img src="assets/images/avatar-1.jpg" alt="User Avatar" class="rounded-circle user-avatar-xxl">  --}}
                    </div>
                    <div class="text-center">
                        <h2 class="font-24 mb-0">
                            {{ @$merchant->fname." ".@$merchant->lname }}
                        </h2>
                        <p>
                            {{ @$merchant->email }}
                        </p>
                        <p>
                            {{ @$merchant->email1 }}
                        </p>
                        <a class="viw-ords" href="javascript::void(0);" onclick="event.preventDefault(); document.getElementById('merchantOrd').submit();">@lang('admin_lang.view_orders')</a>
                        <form id="merchantOrd" action="{{ route('admin.list.order') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                            <input type="text" name="merchant_id" value="{{ @$merchant->id }}">
                        </form>
                        {{-- <a href="{{ route('merchant.products', @$merchant->id) }}">View Products</a> --}}
                        <a class="viw-ords" href="javascript::void(0);" onclick="event.preventDefault(); document.getElementById('merchantPro').submit();">@lang('admin_lang.view_products')</a>
                        <form id="merchantPro" action="{{ route('manage.product') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                            <input type="text" name="merchant_id" value="{{ @$merchant->id }}">
                        </form>
                    </div>
                </div>
                <div class="card-body border-top">
                    <h3 class="font-16">@lang('admin_lang.cnt_info')</h3>
                    <div class="">
                        <ul class="list-unstyled mb-0">
                            <li class="mb-2"><i class="fas fa-fw fa-envelope mr-2"></i>
                                {{ @$merchant->email }}
                            </li>
                            <li class="mb-2"><i class="fas fa-fw fa-envelope mr-2"></i>
                                {{ @$merchant->email1 }}
                            </li>
                            <li class="mb-0"><i class="fas fa-fw fa-phone mr-2"></i>
                                {{ @$merchant->phone }}
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card-body border-top">
                    <h3 class="font-16">@lang('admin_lang.str_address')</h3>
                    <div class="">
                        <ul class="list-unstyled mb-0">
                            {{-- <li class="mb-2">
                                {{ @$merchant->address }}<br/>
                            </li> --}}
                            <li class="mb-0">
                                {{ @$merchant->Country->name }}
                                @if(!empty(@$merchant->state_details))
                                , {{ @$merchant->state_details->name }}
                                @endif
                                @if(!empty(@$merchant->city_details))
                                , {{ @$merchant->city_details->name }}
                                @endif
                                @if(!empty(@$merchant->zipcode))
                                , {{ @$merchant->zipcode }}
                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
                @if(@$merchant->rate != 0.000)
                <div class="card-body border-top">
                    <h3 class="font-16">@lang('admin_lang.ratings')</h3>
                    <h1 class="mb-0">@if(@$merchant->rate != 0.000) {{ @$merchant->rate }} @endif</h1>
                    <div class="rating-star">
                        @if(@$merchant->rate > 4.000)
                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                        <!-- <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> -->
                        @elseif(@$merchant->rate == 4.000)
                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                        <!-- <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i>  -->
                        @elseif(@$merchant->rate >2.000)
                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                        <!-- <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i>  -->
                        @elseif(@$merchant->rate >1.000)
                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                        <!-- <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> -->
                        @elseif(@$merchant->rate <= 1.000)
                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                        <!-- <i class="fa fa-fw fa-star"></i>  -->
                        @endif
                        <p class="d-inline-block text-dark">
                            @if(@$merchant->rate != 0.000) {{ @$merchant->total_no_review }} @endif @lang('admin_lang.reviews') 
                        </p>
                    </div>
                </div>
                @endif
            </div>
            <!-- ============================================================== --> 
            <!-- end card profile --> 
            <!-- ============================================================== --> 
        </div>
        <!-- ============================================================== --> 
        <!-- end profile --> 
        <!-- ============================================================== --> 
        <!-- ============================================================== --> 
        <!-- campaign data --> 
        <!-- ============================================================== -->
        <div class="col-xl-9 col-lg-9 col-md-7 col-sm-12 col-12">
            <!-- ============================================================== --> 
            <!-- campaign tab one --> 
            <!-- ============================================================== -->
            <div class="influence-profile-content pills-regular">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" id="pills-campaign-tab" data-toggle="pill" href="#pills-campaign" role="tab" aria-controls="pills-campaign" aria-selected="true">@lang('admin_lang.about_us')</a> </li>
                    <li class="nav-item"> <a class="nav-link" id="pills-review-tab" data-toggle="pill" href="#pills-review" role="tab" aria-controls="pills-review" aria-selected="false">Reviews</a> </li>
                    {{-- <li class="nav-item"> <a class="nav-link" id="pills-msg-tab" data-toggle="pill" href="#pills-msg" role="tab" aria-controls="pills-msg" aria-selected="false">Send Messages</a> </li> --}}
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-campaign" role="tabpanel" aria-labelledby="pills-campaign-tab">
                        <div class="section-block">
                            <h3 class="section-title">About</h3>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="media Drivers Group-profile-data d-flex align-items-center p-2">
                                            <div class="media-body ">
                                                <div class="manager-dtls tsk-div" style="margin-top:12px;">
                                                    <div class="row fld">
                                                        <div class="col-md-12">
                                                            <p><span class="titel-span">Company Name </span> <span class="deta-span"><strong>:</strong>
                                                                {{ @$merchant->company_name }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <p><span class="titel-span">Company Type </span> <span class="deta-span"><strong>:</strong>
                                                                {{ @$merchant->company_type }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <p><span class="titel-span">Company Details </span> <span class="deta-span"><strong>:</strong>
                                                                {{ @$merchant->company_details }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        {{-- <div class="col-md-12">
                                                            <p><span class="titel-span">@lang('merchant_lang.add_external_order_link') </span> <span class="deta-span"><strong>:</strong>
                                                                <a id="track_link" href="{{route('merchant.create.anonymous.external.order', ['slug' =>@$merchant->slug,'token' => md5(@$merchant->id)])}}" target="_blank">{{route('merchant.create.anonymous.external.order', ['slug' =>@$merchant->slug,'token' => md5(@$merchant->id)])}}</a>
                                                                    &nbsp;
                                                                    <i class="fa fa-clone" id="copy_link" aria-hidden="true" title="Copy link" data-toggle="tooltip"></i>
                                                                </span> 
                                                            </p>
                                                        </div> --}}
                                                        {{-- @if(@$merchant->merchantImage)
                                                        <div class="col-md-12">
                                                            <h4 class="fultxt" style="margin-bottom:10px;">Store Image</h4>
                                                        </div>
                                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div class="uplodpic">
                                                                @if(@$merchant->merchantImage)
                                                                @foreach(@$merchant->merchantImage as $img)
                                                                @php
                                                                $image_path = 'storage/app/public/merchant_portfolio/'.@$img->image; 
                                                                @endphp
                                                                @if(file_exists(@$image_path))
                                                                <li>
                                                                    <img id="merchantPic" src="{{ URL::to('storage/app/public/merchant_portfolio/'.@$img->image) }}" alt="" style="width: 100px;height: 100px;">
                                                                </li>
                                                                @endif
                                                                @endforeach
                                                                @endif 
                                                            </div>
                                                        </div>
                                                        @endif --}}
                                                        {{-- <div class="col-md-12">
                                                            <h4 class="fultxt" style="margin-bottom:10px;">Orders Notification Email</h4>
                                                        </div>
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Email 1</span> <span class="deta-span"><strong>:</strong>
                                                                {{ @$merchant->email1 }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Email 2</span> <span class="deta-span"><strong>:</strong>
                                                                {{ @$merchant->email2 }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Email 3</span> <span class="deta-span"><strong>:</strong>
                                                                {{ @$merchant->email3 }}
                                                                </span> 
                                                            </p>
                                                        </div> --}}
                                                        <div class="col-md-12">
                                                            <h4 class="fultxt" style="margin-bottom:10px;">Bank account information</h4>
                                                        </div>
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Bank Name</span> <span class="deta-span"><strong>:</strong>
                                                                {{ @$merchant->bank_name }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Account Name</span> <span class="deta-span"><strong>:</strong>
                                                                {{ @$merchant->account_name }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Account No</span> <span class="deta-span"><strong>:</strong>
                                                                {{ @$merchant->account_number }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">IFSC Number</span> <span class="deta-span"><strong>:</strong>
                                                                {{ @$merchant->ifsc_number }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Branch Name</span> <span class="deta-span"><strong>:</strong>
                                                                {{ @$merchant->branch_name }}
                                                                </span> 
                                                            </p>
                                                        </div>



                                                        <div class="col-md-12">
                                                            <h4 class="fultxt" style="margin-bottom:10px;">Merchant Earnings</h4>
                                                        </div>
                                                        
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Gross Earning</span> <span class="deta-span"><strong>:</strong>
                                                                ({{ getCurrency() }}) {{ @$merchant->total_earning }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Total Admin Commission</span> <span class="deta-span"><strong>:</strong>
                                                                ({{ getCurrency() }}) {{ @$merchant->total_commission }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Net Earning</span> <span class="deta-span"><strong>:</strong>
                                                                ({{ getCurrency() }}) {{ @$merchant->total_earning - @$merchant->total_commission  }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        {{-- <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Total Insurance Price</span> <span class="deta-span"><strong>:</strong>
                                                                Rs. {{ @$merchant->total_insurance_price }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Total Loading Unloading Price</span> <span class="deta-span"><strong>:</strong>
                                                                Rs. {{ @$merchant->total_loading_inloading_price }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Total Product Price</span> <span class="deta-span"><strong>:</strong>
                                                                Rs. {{ @$merchant->total_product_price }}
                                                                </span> 
                                                            </p>
                                                        </div>
                                                        <div class="col-md-12 shrt-des">
                                                            <p><span class="titel-span">Total Admin Commission</span> <span class="deta-span"><strong>:</strong>
                                                                Rs. {{ @$merchant->total_admin_commission }}
                                                                </span> 
                                                            </p>
                                                        </div> --}}
                                                        
                                                        
                                                        {{-- <div class="col-md-12">
                                                            <h4 class="fultxt" style="margin-bottom:10px;">Payment Acceptance Mode</h4>
                                                        </div>
                                                        <div class="col-md-6 shrt-des">
                                                            <p><span class="titel-span">Payment Mode</span> <span class="deta-span"><strong>:</strong>
                                                                @if(@$merchant->payment_mode == 'O')
                                                                Only Online
                                                                @elseif(@$merchant->payment_mode == 'C')
                                                                Only Cash On Delivery
                                                                @elseif(@$merchant->payment_mode == 'A')
                                                                All( Cash On Delivery & Online Both)
                                                                @endif
                                                                </span> 
                                                            </p>
                                                        </div> --}}
                                                       {{--  <div class="col-md-12">
                                                            <h4 class="fultxt" style="margin-bottom:10px;">Store Opening Hours</h4>
                                                        </div>
                                                        @php 
                                                        $days = getDays();
                                                        @endphp
                                                        @if(@$merchant->merchantOpenning)
                                                        @foreach($days as $key => $value)
                                                        <div class="col-md-6 shrt-des">
                                                            <p><span class="titel-span">
                                                                @if($key == 0)
                                                                Monday
                                                                @elseif($key == 1)
                                                                Tuesday
                                                                @elseif($key == 2)
                                                                Wednesday
                                                                @elseif($key == 3)
                                                                Thursday
                                                                @elseif($key == 4)
                                                                Friday
                                                                @elseif($key == 5)
                                                                Saturday
                                                                @elseif($key == 6)
                                                                Sunday
                                                                @endif
                                                                </span> <span class="deta-span"><strong>:</strong>
                                                                @if(@$merchant->merchantOpenning[$key]->from_time) {{ date('h:ia',strtotime(@$merchant->merchantOpenning[$key]->from_time)) }} @endif-@if(@$merchant->merchantOpenning[$key]->to_time) {{ date('h:ia',strtotime(@$merchant->merchantOpenning[$key]->to_time)) }} @endif
                                                                .</span> 
                                                            </p>
                                                        </div>
                                                        @endforeach
                                                        @endif --}}
                                                        {{-- 
                                                        <div class="col-md-6 shrt-des">
                                                            <p><span class="titel-span">Tuesday</span> <span class="deta-span"><strong>:</strong>9am-3pm.</span> </p>
                                                        </div>
                                                        --}}
                                                        {{-- 
                                                        <div class="col-md-6 shrt-des">
                                                            <p><span class="titel-span">Wednesday</span> <span class="deta-span"><strong>:</strong>9am-3pm.</span> </p>
                                                        </div>
                                                        <div class="col-md-6 shrt-des">
                                                            <p><span class="titel-span">Thursday</span> <span class="deta-span"><strong>:</strong>9am-3pm.</span> </p>
                                                        </div>
                                                        --}}
                                                        {{-- 
                                                        <div class="col-md-6 shrt-des">
                                                            <p><span class="titel-span">Friday</span> <span class="deta-span"><strong>:</strong>9am-3pm.</span> </p>
                                                        </div>
                                                        <div class="col-md-6 shrt-des">
                                                            <p><span class="titel-span">Saturday</span> <span class="deta-span"><strong>:</strong>9am-3pm.</span> </p>
                                                        </div>
                                                        --}}
                                                        {{-- 
                                                        <div class="col-md-6 shrt-des">
                                                            <p><span class="titel-span">Sunday</span> <span class="deta-span"><strong>:</strong>9am-3pm.</span> </p>
                                                        </div>
                                                        --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="tab-pane fade" id="pills-packages" role="tabpanel" aria-labelledby="pills-packages-tab">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="section-block">
                                    <h2 class="section-title">My Packages</h2>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-header bg-primary text-center p-3 ">
                                        <h4 class="mb-0 text-white"> Basic</h4>
                                    </div>
                                    <div class="card-body text-center">
                                        <h1 class="mb-1">$150</h1>
                                        <p>Per Month Plateform</p>
                                    </div>
                                    <div class="card-body border-top">
                                        <ul class="list-unstyled bullet-check font-14">
                                            <li>Facebook, Instagram, Pinterest,Snapchat.</li>
                                            <li>Guaranteed follower growth for increas brand awareness.</li>
                                            <li>Daily updates on choose platforms</li>
                                            <li>Stronger customer service through daily interaction</li>
                                            <li>Monthly progress report</li>
                                            <li>1 Million Followers</li>
                                        </ul>
                                        <a href="#" class="btn btn-outline-secondary btn-block btn-lg">Get Started</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-header bg-info text-center p-3">
                                        <h4 class="mb-0 text-white"> Standard</h4>
                                    </div>
                                    <div class="card-body text-center">
                                        <h1 class="mb-1">$350</h1>
                                        <p>Per Month Plateform</p>
                                    </div>
                                    <div class="card-body border-top">
                                        <ul class="list-unstyled bullet-check font-14">
                                            <li>Facebook, Instagram, Pinterest,Snapchat.</li>
                                            <li>Guaranteed follower growth for increas brand awareness.</li>
                                            <li>Daily updates on choose platforms</li>
                                            <li>Stronger customer service through daily interaction</li>
                                            <li>Monthly progress report</li>
                                            <li>2 Blog Post & 3 Social Post</li>
                                            <li>5 Millions Followers</li>
                                            <li>Growth Result</li>
                                        </ul>
                                        <a href="#" class="btn btn-secondary btn-block btn-lg">Get Started</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-header bg-primary text-center p-3">
                                        <h4 class="mb-0 text-white">Premium</h4>
                                    </div>
                                    <div class="card-body text-center">
                                        <h1 class="mb-1">$550</h1>
                                        <p>Per Month Plateform</p>
                                    </div>
                                    <div class="card-body border-top">
                                        <ul class="list-unstyled bullet-check font-14">
                                            <li>Facebook, Instagram, Pinterest,Snapchat.</li>
                                            <li>Guaranteed follower growth for increas brand awareness.</li>
                                            <li>Daily updates on choose platforms</li>
                                            <li>Stronger customer service through daily interaction</li>
                                            <li>Monthly progress report & Growth Result</li>
                                            <li>4 Blog Post & 6 Social Post</li>
                                        </ul>
                                        <a href="#" class="btn btn-secondary btn-block btn-lg">Contact us</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>-->
                    <div class="tab-pane fade" id="pills-review" role="tabpanel" aria-labelledby="pills-review-tab">
                        <div class="card" style="overflow-y: scroll;overflow-x: scroll; height: 500px;">
                            <h5 class="card-header">Reviews </h5>
                            @if(@$order)
                                @foreach(@$order as $ord)
                                    @if($ord->orderMasterDetails)
                                        @foreach(@$ord->orderMasterDetails as $dtls)
                                            @if($dtls->rate > 0)
                                            <div class="card-body">
                                                <div class="review-block">
                                                    <h5 style="margin-bottom:5px;"># Order : {{ @$ord->order_no }}</h5>

                                                    <div class="rating-star mb-4">
                                                        @if(@$dtls->rate == 5)
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        @elseif(@$dtls->rate == 4)
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        @elseif(@$dtls->rate == 3)
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>  
                                                        @elseif(@$dtls->rate == 2)
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a> 
                                                        @elseif(@$dtls->rate == 1)
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        @endif
                                                    </div>
                                                    <p class="review-text font-italic m-0">“ {{ @$dtls->comment }}  ”</p>
                                                    <span class="text-dark font-weight-bold">{{ @$ord->shipping_fname }} {{ @$ord->shipping_lname }}</span><small class="text-mute"> </small> 
                                                </div>
                                            </div>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                            <!-- <div class="card-body border-top">
                                <div class="review-block">
                                    <h5 style="margin-bottom:5px;"># Order : 123654</h5>
                                    <p class="review-text font-italic m-0">“Maecenas rutrum viverra augue. Nulla in eros vitae ante ullamcorper congue. Praesent tristique massa ac arcu dapibus tincidunt. Mauris arcu mi, lacinia et ipsum vel, sollicitudin laoreet risus.”</p>
                                    <div class="rating-star mb-4"> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> </div>
                                    <span class="text-dark font-weight-bold">Luise M. Michet</span><small class="text-mute"> (Company name)</small> 
                                </div>
                                </div> -->
                            <!-- <div class="card-body border-top">
                                <div class="review-block">
                                    <h5 style="margin-bottom:5px;"># Order : 123654</h5>
                                    <p class="review-text font-italic m-0">“ Cras non rutrum neque. Sed lacinia ex elit, vel viverra nisl faucibus eu. Aenean faucibus neque vestibulum condimentum maximus. In id porttitor nisi. Quisque sit amet commodo arcu, cursus pharetra elit. Nam tincidunt lobortis augueat euismod ante sodales non. ”</p>
                                    <div class="rating-star mb-4"> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> <i class="fa fa-fw fa-star"></i> </div>
                                    <span class="text-dark font-weight-bold">Gloria S. Castillo</span><small class="text-mute"> (Company name)</small> 
                                </div>
                                </div> -->
                        </div>
                        <!-- <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active"><a class="page-link " href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav> -->
                    </div>
                    {{-- <div class="tab-pane fade" id="pills-msg" role="tabpanel" aria-labelledby="pills-msg-tab">
                        <div class="card">
                            <h5 class="card-header">Send Messages</h5>
                            <div class="card-body">
                                <form id="sendMsgForm" method="post" action="{{ route('admin.send.message',@$merchant->id) }}">
                                    @csrf
                                    <div class="row">
                                        <div class="offset-xl-3 col-xl-6 offset-lg-3 col-lg-3 col-md-12 col-sm-12 col-12 p-4">
                                            <div class="form-group">
                                                <label for="title">Title</label>
                                                <input type="text" class="form-control form-control-lg required" id="title" name="title" placeholder="Title">
                                                <div class="error_title" style="color: red;"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="messages">Messages</label>
                                                <textarea class="form-control required" id="messages" name="messages" rows="3"></textarea>
                                                <div class="error_message" style="color: red;"></div>
                                            </div>
                                            <button type="submit" id="sendMsg" class="btn btn-primary float-right">Send Message</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
            <!-- ============================================================== --> 
            <!-- end campaign tab one --> 
            <!-- ============================================================== --> 
        </div>
        <!-- ============================================================== --> 
        <!-- end campaign data --> 
        <!-- ============================================================== --> 
    </div>
</div>
<!-- footer -->   
@include('admin.includes.footer')
<!-- end footer --> 
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha256-3blsJd4Hli/7wCQ+bmgXfOdK7p/ZUMtPXY08jmxSSgk=" crossorigin="anonymous"></script>
{{--<script src="{{ URL::to('public/admin/assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script> --}}
<!----- bootstap bundle js---> 
<script src="{{ URL::to('public/admin/assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script> 
<!----- slimscroll js---> 
<script src="{{ URL::to('assets/vendor/slimscroll/jquery.slimscroll.js') }}"></script> 
<!----- main js---> 
<script src="{{ URL::to('assets/libs/js/main-js.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
    // $("#saveNewMerchant").click(function(){
        $("#sendMsgForm").validate({
            rules:{
                'title':{
                    required:true
                },
                'messages':{
                    required:true
                },
            },
            messages: { 
                title: { 
                    required: '@lang('validation.required')'
                },
                messages: { 
                    required: '@lang('validation.required')'
                }
            },
            errorPlacement: function (error, element) 
            {
                // console.log(element.attr("name"));
                // console.log(error);
                if (element.attr("name") == "title") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.error_title').html(error);
                }
                if (element.attr("name") == "messages") {
                    var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.error_message').html(error);
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#copy_link').click(function() {
            CopyToClipboard('track_link')
        })
    })
    function CopyToClipboard(containerid) {
        if (window.getSelection) {
            if (window.getSelection().empty) { // Chrome
                window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) { // Firefox
                window.getSelection().removeAllRanges();
            }
        } else if (document.selection) { // IE?
            document.selection.empty();
        }

        if (document.selection) {
            var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById(containerid));
            range.select().createTextRange();
            document.execCommand("copy");
        } else if (window.getSelection) {
            var range = document.createRange();
            range.selectNode(document.getElementById(containerid));
            window.getSelection().addRange(range);
            document.execCommand("copy");
        }
        toastr.success('Link copied!');
    }
</script>
@endsection