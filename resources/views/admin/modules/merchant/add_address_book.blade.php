@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Add City') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('merchant_lang.add_address_book')
@endsection
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('merchant_lang.add_address_book')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                @lang('merchant_lang.add_address_book')
                                <a class="adbtn btn btn-primary" href="{{ url('admin/show-address-book') }}/{{ request()->segment(3) }}"> @lang('admin_lang.back')</a>
                            </h5>
                            <div class="card-body">
                                <form id="addAddressForm" method="POST" action="{{ route('admin.insert.address.book') }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="merchant_id" value="{{ request()->segment(3) }}">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="country" class="col-form-label">@lang('admin_lang.country') (@lang('admin_lang.required'))</label>
                                            <select class="custom-select form-control required" id="country" name="country">
                                                <option value="">@lang('admin_lang.select_country')</option>
                                                @if(@$countries)
                                                @foreach(@$countries as $country)
                                                <option value="{{ @$country->id }}" @if($country->id == 134) selected  @elseif($country->status == 'I') style="display:none;" @endif>{{ @$country->countryDetailsBylanguage->name }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label class="col-form-label">@lang('admin_lang.phone') (@lang('admin_lang.required'))</label>
                                            <input id="phone" name="phone" class="form-control required number" placeholder='@lang('admin_lang.phone')' type="tel"  onkeypress="return isNumber(event)"> 
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 outside_kuwait_city">
                                            <label for="city" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                            <input id="city" name="city" type="text" class="form-control" placeholder='@lang('admin_lang.city')'>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 inside_kuwait_city">
                                            <label for="kuwait_city_label" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                            
                                            <input type="text" class="form-control required custom-select" name="kuwait_city" id="kuwait_city" placeholder="@lang('admin_lang.city')" value="" >
                                            <div id="cityList"></div>
                                            <span class="text-danger city_id_err"></span>
                                        </div>
                                        
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 blockDiv">
                                            <label for="" class="col-form-label block_label">@lang('admin_lang.block_required') </label>
                                            <input type="text" id="block" name="block" type="text" class="form-control required" placeholder='@lang('admin_lang.block')' />
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="" class="col-form-label">@lang('admin_lang.street') (@lang('admin_lang.required'))</label>
                                            <input id="street" name="street" type="text" class="form-control required" placeholder='@lang('admin_lang.street')'>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 zipDiv" >
                                        <label for="zip" class="col-form-label zip_label">@lang('admin_lang.postal_code_optional')</label>
                                        <input id="zip" name="zip" type="number" class="form-control" placeholder='@lang('admin_lang.postal_code')' onkeypress="return isNumber(event)">
                                        </div>
                                        
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 buidingDiv buidingDiv">
                                            <label for="" class="col-form-label building_label">@lang('admin_lang.build_required')</label>
                                            <input type="text" id="building" name="building" type="text" class="form-control required" placeholder='@lang('admin_lang.building')' />
                                        </div>
                                        
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label class="col-form-label">@lang('front_static.type_url') (@lang('front_static.optional'))</label>
                                            <input id="" type="text" placeholder="@lang('front_static.type_url')" name="location" class="form-control">
                                            
                                            <input type="hidden" name="lat" id="lat1">
                                            <input type="hidden" name="lng" id="lng1">
                                        </div>
        
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="" class="col-form-label">@lang('admin_lang.more_add_details_opt')</label>
                                            <input type="text" id="more_address" name="address" type="text" class="form-control" placeholder='@lang('admin_lang.more_address_details')' />
                                        </div>
                                    </div>
                                    
                                    <div class="form-row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <label>
                                                <input type="checkbox" name="is_default" id="is_default" value="Y" placeholder="@lang('front_static.is_default')"> @lang('front_static.is_default')
                                            </label>
                                        </div>
                                        <hr>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <input type="hidden" id="k_cityname" name="k_cityname" value="">
                                            <button class="btn btn-primary" id="addAddressBtn" type="submit">@lang('merchant_lang.save_address')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        $(".outside_kuwait_state").hide();
        $(".outside_kuwait_city").hide();
        $(".zipDiv").hide();

        $(document).on("change","#country",function(e){
            $(".removeText").text("");
            var country = $.trim($("#country").val());
            if(country == 134){ //if inside kuwait
                $(".outside_kuwait_state").hide();
                $(".outside_kuwait_city").hide();
                $(".inside_kuwait_state").show();
                $(".inside_kuwait_city").show();
                $(".zipDiv").hide();
            }else{
                $(".inside_kuwait_state").hide();
                $(".outside_kuwait_state").show();
                $(".inside_kuwait_city").hide();
                $(".outside_kuwait_city").show();
                $(".zipDiv").show();
            }
        });
        $('#kuwait_city').change(function(event) {
            $('.city_id_err').html('');    
        });
        $("#addAddressForm").validate({
            messages: { 
                street: { 
                    required: '@lang('validation.required')'
                },
                city: { 
                    required: '@lang('validation.required')'
                },
                kuwait_city: { 
                    required: '@lang('validation.required')'
                },
                zip: { 
                    required: '@lang('validation.required')'
                },
                country: { 
                    required: '@lang('validation.required')'
                },
            }
        });

        $('#kuwait_city').keyup(function(){ 
            var city = $(this).val();
            if(city != '')
            {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('merchant.external.fetch.city') }}",
                    method:"POST",
                    data:{city:city, _token:_token},
                    success:function(response){
                        // alert(JSON.stringify(response));
                        if(response.status == 'ERROR') {
                            $('#cityList').html('@lang('admin_lang.nothing_found')');
                        } else {
                        $('#cityList').fadeIn();  
                        $('#cityList').html(response.result);
                        }
                    }
                });
            }
        });
        $(document).on("blur","#kuwait_city",function(){
            $("#k_cityname").val($.trim($("#kuwait_city").val()));
            var keyword = $.trim($("#kuwait_city_value_id").val());
            if(keyword == ""){
                $("#kuwait_city").val("");
                $(".kuwait_city-error").text('@lang('admin_lang.please_select_name_city')');
            }
        });

        $('body').on('click', '.cityChange', function(){  
            $('#kuwait_city').val($(this).text());  
            $('#cityList').fadeOut();  
        });  
        $("body").click(function(){
            $("#cityList").fadeOut();
        });
        $("#kuwait_city").click(function(){
            var city = $(this).val();
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url:"{{ route('merchant.external.fetch.city') }}",
                method:"POST",
                data:{city:city, _token:_token},
                success:function(response){
                    if(response.status == 'ERROR') {
                        $('#cityList').html('@lang('admin_lang.nothing_found')');
                    } else {
                    $('#cityList').fadeIn();  
                    $('#cityList').html(response.result);
                    }
                }
            });
        });
        $(document).on("click",".kuwait_cities",function(e){
            var id = $(e.currentTarget).attr("data-id");
            $("#kuwait_city_value_id").val(id);
        });
        // when change country
        $('#country').change(function(event) {
            country = $(this).val();
            $('#street').addClass('required');

            // seller and buyer both are from kuwait
            if(country == 134) {
                //add text required
                
                $(".block_label").html('@lang('admin_lang.block_required')');
                $(".building_label").html('@lang('admin_lang.buildings_required')');
                //add text optional

                // add required
                $('#street').addClass('required');
                $('#kuwait_city').addClass('required');
                $('#street').addClass('required');
                $('#block').addClass('required');
                $('#building').addClass('required');
                
                
                // remove required
                $('#city').removeClass('required');
                $('#city').removeClass('error');
                $("#street-error").text("");
                $(".zipDiv").hide();
                
            } 
            // out side kuwait
            else {
                $(".block_label").html('@lang('admin_lang.block_optional')');
                $(".building_label").html('@lang('admin_lang.building_optional')');
                //add text required
                
                //add text optional

                //add required
                // block-error
                $("#block-error").text("");
                $("#building-error").text("");
                $('#city').addClass('required');
                

                //remove required
                $('#block').removeClass('required');
                $('#block').removeClass('error');
                $('#building').removeClass('required');
                $('#building').removeClass('error');
                $('#kuwait_city').removeClass('required');
                $('#kuwait_city').removeClass('error');
                $(".zipDiv").show();
                //remove text

            }
        });
        
    });
    function validate(evt) {
        var theEvent = evt || window.event;
        // Handle paste
        if (theEvent.type === 'paste') {
            key = event.clipboardData.getData('text/plain');
        } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        }
        var regex = /[0-9]|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>
<script>
    function initMap1() {
        var input = document.getElementById('pac-input1');
        var autocomplete = new google.maps.places.Autocomplete(input);
        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
        autocomplete.setTypes(['address']);
    
        autocomplete.addListener('place_changed', function() {
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
          $('#lat1').val(place.geometry.location.lat())
          $('#lng1').val(place.geometry.location.lng())
        });
    }
    $(document).ready(function() {
        $('#pac-input1').blur(function() {
            if($(this).val() == '') {
                $('#lat1').val('')
                $('#lng1').val('')
            }
        })
    })
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API') }}&libraries=places&callback=initMap1" async defer></script>
@endsection