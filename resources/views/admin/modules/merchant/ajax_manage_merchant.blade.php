<table class="table table-striped table-bordered first" id="myTable">
                            <thead>
                                <tr>
                                    <th>@lang('admin_lang.name')</th>
                                    <th>@lang('admin_lang.email')</th>
                                    <th>@lang('admin_lang.phone')</th>
                                    <th>@lang('admin_lang.internal_shipping_cost')</th>
                                    <th>@lang('admin_lang.external_shipping_cost')</th>
                                    {{-- 
                                    <th>@lang('admin_lang.total_order')</th>
                                    --}}
                                    {{-- 
                                    <th>@lang('admin_lang.total_due')</th>
                                    --}}
                                    <th>@lang('admin_lang.premium_merchant')</th>
                                    <th>@lang('admin_lang.international_order')</th>
                                    <th>@lang('admin_lang.status')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(@$merchants)
                                @foreach(@$merchants as $merchant)
                                <tr class="tr_{{ @$merchant->id }}">
                                    <td>{{ @$merchant->fname." ".@$merchant->lname }}</td>
                                    <td>{{ @$merchant->email }}</td>
                                    <td>{{ @$merchant->phone }}</td>
                                    <td class="inside_shipping_cst_{{ @$merchant->id }}">
                                        {{ @$merchant->inside_shipping_cost }}
                                    </td>
                                    <td class="external_shipping_cost_{{ @$merchant->id }}">
                                        {{ @$merchant->shipping_cost }}
                                    </td>
                                    {{-- 
                                    <td>{{ @$merchant->total_earning }}</td>
                                    --}}
                                    {{-- 
                                    <td>{{ @$merchant->total_due }}</td>
                                    --}}
                                    <td class="premium_merchant_{{ @$merchant->id }}">
                                        @if(@$merchant->premium_merchant == 'Y')
                                        @lang('admin_lang.yes')
                                        @elseif(@$merchant->premium_merchant == 'N')
                                        @lang('admin_lang.no')
                                        @endif
                                    </td>
                                    <td class="int_status_{{ @$merchant->id }}">
                                        @if(@$merchant->international_order == 'ON')
                                        @lang('admin_lang.on')
                                        @elseif(@$merchant->international_order == 'OFF')
                                        @lang('admin_lang.off')
                                        @endif
                                    </td>
                                    <td class="status_{{ @$merchant->id }}">
                                        @if(@$merchant->status == 'A')
                                        @lang('admin_lang.Active')
                                        @elseif(@$merchant->status == 'I')
                                        @lang('admin_lang.Inactive')
                                        @elseif(@$merchant->status == 'U')
                                        @lang('admin_lang.un_verified')
                                        @elseif(@$merchant->status == 'AA')
                                        @lang('admin_lang.awaiting_approval')
                                        @endif
                                    </td>
                                    <td>
                                        {{-- View merchant --}}
                                        <a href="{{ route('admin.view.merchant.profile',@$merchant->id) }}"><i class="fas fa-eye" title='@lang('admin_lang.view')'></i></a>
                                        {{-- verify merchant --}}
                                        @if(@$merchant->status =='U')
                                        <a href="javascript:void(0)" data-id="{{ @$merchant->id }}" class="m_verify_account" data-status="{{ @$merchant->status }}"><i class="fa fa-user-circle v_account_{{ @$merchant->id }}" title='@lang('admin_lang.verify_merchant_account')'></i></a>
                                        @endif
                                        {{-- block unblock --}}
                                        @if(@$merchant->status == 'A')
                                        <a class="block_unblock_merchant" href="javascript:void(0)" data-id="{{ @$merchant->id }}" data-status="A"><i class="fas fa-ban icon_change_{{ @$merchant->id }}" title='@lang('admin_lang.block')'></i></a>
                                        @elseif(@$merchant->status == 'I')
                                        <a class="block_unblock_merchant" href="javascript:void(0)" data-id="{{ @$merchant->id }}" data-status="I"><i class="far fa-check-circle icon_change_{{ @$merchant->id }}" title='@lang('admin_lang.unblock')'></i></a>
                                        @endif
                                        {{-- Edit --}}
                                        <a href="{{ route('admin.edit.merchant',$merchant->id) }}"><i class="fas fa-edit" title='@lang('admin_lang.Edit')'></i></a>
                                        {{-- Premium merchant --}}
                                        <a class="premium_merchant_status" href="javascript:void(0)" data-id="{{ @$merchant->id }}"><i class="far fa-money-bill-alt premium_{{ @$merchant->id }}" title='@lang('admin_lang.without_fees')'></i></a>
                                        {{-- admin.international.order --}}
                                        <a class="international_order_status" href="javascript:void(0)" data-id="{{ @$merchant->id }}"><i class="fa fa-globe int_st_icon_{{ @$merchant->id }}" title='@lang('admin_lang.international_order')'></i></a>
                                        {{-- email notification --}}
                                        <a href="javascript:void(0)" data-toggle="modal" data-id="{{ @$merchant->id }}" data-emailid="{{ @$merchant->email }}" data-name="{{ @$merchant->fname." ".@$merchant->lname }}" class="send_email_notify" data-target="" data-action="{{ route('admin.send.notification.merchant',@$merchant->id) }}"><i class="fas fa-paper-plane" title='@lang('admin_lang.send_email')'></i></a>
                                        {{-- approve --}}
                                        @if(@$merchant->status == 'AA')
                                        <a class="app_merchant_details" href="javascript:void(0)" data-id="{{ @$merchant->id }}"><i class="fas fa-thumbs-up appr_merchant_{{ @$merchant->id }}" title='@lang('admin_lang.approve')'></i></a>
                                        @endif
                                        {{-- commission --}}
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="" data-id="{{ @$merchant->id }}" data-emailid="{{ @$merchant->email }}" data-name="{{ @$merchant->fname." ".@$merchant->lname }}" @if(@$merchant->commission) data-commission="{{ @$merchant->commission }}" @else data-commission="" @endif class="commision_set set_com_value_{{ @$merchant->id }}"><i class="fas fa-percent " title='@lang('admin_lang.commission')'></i></a>
                                         {{--  delete --}}
                                        <a class="delete_merchant" href="javascript:void(0)" data-id="{{ @$merchant->id }}"><i class="fas fa-trash d_m_{{ @$merchant->id }}" title='@lang('admin_lang.Delete')'></i></a>
                                        {{-- shipping cost --}}
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="" class="shipping_cost s_cost_{{ @$merchant->id }}" data-id="{{ @$merchant->id }}" data-emailid="{{ @$merchant->email }}" data-name="{{ @$merchant->fname." ".@$merchant->lname }}" @if(@$merchant->shipping_cost)  data-shippingcost="{{ @$merchant->shipping_cost }}" @else data-shippingcost="" @endif @if(@$merchant->inside_shipping_cost) data-internalshippingcost ="{{ @$merchant->inside_shipping_cost }}" @else data-internalshippingcost="" @endif><i class="fas fa-truck" title='@lang('admin_lang.shipping_cst')'></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>@lang('admin_lang.name')</th>
                                    <th>@lang('admin_lang.email')</th>
                                    <th>@lang('admin_lang.phone')</th>
                                    <th>@lang('admin_lang.internal_shipping_cost')</th>
                                    <th>@lang('admin_lang.external_shipping_cost')</th>
                                    {{-- 
                                    <th>@lang('admin_lang.total_order')</th>
                                    --}}
                                    {{-- 
                                    <th>@lang('admin_lang.total_due')</th>
                                    --}}
                                    <th>@lang('admin_lang.premium_merchant')</th>
                                    <th>@lang('admin_lang.international_order')</th>
                                    <th>@lang('admin_lang.status')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </tfoot>
                        </table>
                        <script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>