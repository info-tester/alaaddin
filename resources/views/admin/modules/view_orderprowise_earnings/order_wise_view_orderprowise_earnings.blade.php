@extends('admin.layouts.app')

@section('title')
{{ config('app.name', 'Alaaddin') }} | Insurance Collection
@endsection
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')

<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/arabic/assets/libs/css/chosen.css') }}">
<!-- <style type="text/css"></style> -->
@endif
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

{{-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> --}}
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
@php
	//ini_set('memory_limit', '-1');
@endphp
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<!-- ============================================================== -->
			<!-- pageheader  -->
			<!-- ============================================================== -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="page-header">
						<h2 class="pageheader-title">@lang('admin_lang.manage_orders')</h2>
						<div class="page-breadcrumb">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
									<li class="breadcrumb-item"><a href="{{ route('admin.list.order') }}" class="breadcrumb-link">Orders</a></li>
									<li class="breadcrumb-item active" aria-current="page">Insurance Collection</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</div>
			@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			<div class="mail_success" style="display: none;">
				<div class="alert alert-success vd_hidden" style="display: block;">
					<a class="close" data-dismiss="alert" aria-hidden="true">
						<i class="icon-cross"></i>
					</a>
					<span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
					<strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
				</div>
			</div>
			<div class="mail_error" style="display: none;">
				<div class="alert alert-danger vd_hidden" style="display: block;">
					<a class="close" data-dismiss="alert" aria-hidden="true">
						<i class="icon-cross"></i>
					</a>
					<span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
					<strong>Error!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
				</div>
			</div>
			@if (session()->has('success'))
			<div class="alert alert-success vd_hidden" style="display: block;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
				<strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
			</div>
			@elseif ((session()->has('error')))
			<div class="alert alert-danger vd_hidden" style="display: block;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
				<strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
			</div>
			@endif
			{{-- success msg show --}}
			<div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
				<strong class="success_msg">@lang('admin_lang.suc_st_ord_ch')</strong>
			</div>
			{{-- error msg show --}}
			<div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
				<strong class="error_msg">@lang('admin_lang.err_st_ord_ch')</strong> 
			</div>
			<!-- ============================================================== -->
			<!-- end pageheader -->
			<!-- ============================================================== -->
			<div class="row">
				<!-- ============================================================== -->
				<!-- basic table  -->
				<!-- ============================================================== -->
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="card">
						<h5 class="card-header">Insurance Collection</h5>
						<div class="card-body">
							
								<div class="row">
									<div data-column="0" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="keyword" class="col-form-label">Search for @lang('admin_lang.Keywords')</label>
										<input id="col0_filter" name="keyword" type="text" class="form-control keyword" placeholder="#Order/shipping name/email/phone">
									</div>
									{{-- <div data-column="2" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="city_label_ku" class="col-form-label">City </label>
										<select data-placeholder="Select City" class="form-control select slt slct kuwait_search_city_nm"  tabindex="2" id="col2_filter" name="kuwait_search_city_nm">
											<option value="">Select City</option>
											@if(@$kuwaitCities)
											@foreach(@$kuwaitCities as $kuwaitCity)
											<option value="{{ @$kuwaitCity->id }}" @if(@$key['kuwait_search_city_nm'] == $kuwaitCity->id) selected @endif>{{ @$kuwaitCity->name }} </option>
											@endforeach
											@endif
										</select>
									</div> --}}
									<div data-column="3" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="cust_label" class="col-form-label">Customer</label>
										<select data-placeholder="Select customer" class="form-control select slt slct  customer_search"  tabindex="3" id="col3_filter" name="customer_search">
											<option value="">Select Customer</option>
											@if(@$userCustomer)
											@foreach(@$userCustomer as $cus)
											<option value="{{ @$cus->id }}" @if(@$key['customer_search'] == $cus->id) selected @endif>{{ @$cus->fname." ".@$cus->lname }} </option>
											@endforeach
											@endif
										</select>
									</div>
									<div data-column="4" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="merchant_lb" class="col-form-label">@lang('admin_lang.merchant')</label>
										<select data-placeholder="@lang('admin_lang.choose_a_merchant')" class="form-control select slt slct merchant"  tabindex="4" id="col4_filter" name="merchant">
											<option value="">@lang('admin_lang.select_merchant')</option>
											@if(@$merchants)
											@foreach(@$merchants as $merchant)
											<option value="{{ @$merchant->id }}" @if(@$key['merchant'] == $merchant->id) selected @endif>{{ @$merchant->fname." ".@$merchant->lname }}</option>
											@endforeach
											@endif
										</select>
									</div>
									<div data-column="5" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="input-select" class="col-form-label">@lang('admin_lang.Status')</label>
										<select name="type[]" class="form-control order_status status select slt selectpicker"  data-live-search="true" id="col5_filter" multiple="">
											<option value='INP'>In Progress</option>
											<option value='CM'>Completed</option>
											<option value='OA'>@lang('admin_lang.order_accepted')</option>
											
											<option value='OP'>@lang('admin_lang.ord_picked_up')</option>
											<option value='OD'>@lang('admin_lang.ord_dlv')</option>
											
											
										</select>
									</div>

									<div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="to_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
										<input type="text" class="form-control datepicker from_date" name="from_date" placeholder="@lang('admin_lang_static.from_date')" value="" readonly="" id="col1_filter_from">
									</div>
									<div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="to_date" class="col-form-label">@lang('admin_lang_static.to_date')</label>
										<input type="text" class="form-control datepicker to_date" name="to_date" placeholder="@lang('admin_lang_static.to_date')" value="" readonly="" id="col1_filter_to">
									</div>
									<div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
										<label for="search_orders_ser" class="col-form-label" style="padding: 12px;"> </label>
										<a href="javascript:void(0)" id="search_orders_ser" class="btn btn-primary ">@lang('admin_lang.Search')</a>
										<input type="reset" value="@lang('admin_lang.reset_search')" class="btn btn-default reset_search">
										{{-- <button type="submit" id="search_orders" class="btn btn-primary ">@lang('admin_lang.export')</button> --}}
									</div>
								</div>
							
							<div class="table-responsive listBody">
								<div class="row">
									<div class="col-md-12">
										{{-- <p class="amount-total-cls">COD Subtotal: <span id="showCod"></span></p> --}}
										{{-- <p class="amount-total-cls">COD TOTAL : <span id="showCodSub"></span></p> --}}
										{{-- <p class="amount-total-cls">Online Order Sub-Total : <span id="showOnlineSub"></span>  </p> --}}
										{{-- <p class="amount-total-cls">Online Order Total  : <span id="showOnline"></span></p> --}}
									</div>
								</div>
								
								<table class="table table-striped table-bordered first" id="myTable1">
									<thead>
										<tr>
											<th>@lang('admin_lang.order_no')</th>
											<th>@lang('admin_lang.date')</th>
											<th>Customer</th>
											
											<th>Order Total ({{ getCurrency() }})</th>
											<th>Insurance Price ({{ getCurrency() }})</th>
											
											<th>@lang('admin_lang.Status')</th>
											<th>@lang('admin_lang.Action')</th>
									</thead>
									<tfoot>
										<tr>
											
											<th>@lang('admin_lang.order_no')</th>
											<th>@lang('admin_lang.date')</th>
											<th>Customer</th>
											
											<th>Order Total ({{ getCurrency() }})</th>
											<th>Insurance Price ({{ getCurrency() }})</th>
											
											
											<th>@lang('admin_lang.Status')</th>
											
											<th>@lang('admin_lang.Action')</th>
											
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end basic table  -->
				<!-- ============================================================== -->
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- footer -->
		<!-- ============================================================== -->
		@include('admin.includes.footer')
		<!-- ============================================================== -->
		<!-- end footer -->
		<!-- ============================================================== -->
		<div class="loader" style="display: none;">
			<img src="{{url('public/loader.gif')}}">
		</div>
	</div>
</div>

<div class="modal fade" id="assign_driver_popup_show" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.assign_driver')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="driver_assign_form" method="post" action="javascript:void(0)">
                        @csrf
                        <div class="form-row">
                            <input id="order_id" name="order_id" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="order_type_driver" name="order_type_driver_driver" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="status_assign" name="status_assign" type="hidden" class="form-control" placeholder="" readonly>
                            <div class="form-group col-md-12">
                                <label>
                                @lang('admin_lang.assign_driver')
                                </label>
                                <select id="assign" name="assign" class="form-control">
                                    <option value="">@lang('admin_lang.select_driver')</option>
                                    <option value="N">No Driver</option>
                                    @foreach(@$drivers as $driver)
                                    <option value="{{ @$driver->id }}">{{ @$driver->fname }} {{ @$driver->lname }}</option>
                                    @endforeach
                                </select>
                                <span class="error_assign_driver" style="color: red;"></span>
							</div>
							
                            <div class="form-group col-md-12 assign_type_div" style="display: none;">
                                <label for="specific" class="col-form-label">@lang('admin_lang.assign_type')</label>    
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;@lang('admin_lang.assign_order_entire')
                                <input type="radio" name="assign_type" class="type" value="EO">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;@lang('admin_lang.assign_all_products_seller')
                                <input type="radio" name="assign_type" class="type" value="SS">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;Product specific
                                <input type="radio" name="assign_type" class="type" value="N">
                                <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="assignDriver" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    	</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="reassign_driver_popup_show" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.assign_driver')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="driver_reassign_form" method="post" action="javascript:void(0)">
                        @csrf
                        <div class="form-row">
                            <input id="order_id_reassign" name="order_id_reassign" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="order_type_driver_reassign" name="order_type_driver_reassign" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="status_reassign" name="status_reassign" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="is_final_assign" name="is_final_assign" type="hidden" class="form-control" value="N">
                            <div class="form-group col-md-12">
                                <label>
                                @lang('admin_lang.assign_driver')
                                </label>
                                <select id="reassign" name="reassign" class="form-control">
                                    <option value="">@lang('admin_lang.select_driver')</option>
                                    <option value="N">No Driver</option>
                                    @foreach(@$drivers as $driver)
                                    <option value="{{ @$driver->id }}">{{ @$driver->fname }} {{ @$driver->lname }}</option>
                                    @endforeach
                                </select>
                                <span class="error_reassign_driver" style="color: red;"></span>
							</div>
                            <div class="form-group col-md-12 reassign_type_div" style="display: none;">
                                <label for="specific" class="col-form-label">@lang('admin_lang.assign_type')</label>    
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;@lang('admin_lang.assign_order_entire')
                                <input type="radio" name="assign_type" class="type" value="EO">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;@lang('admin_lang.assign_all_products_seller') 
                                <input type="radio" name="assign_type" class="type" value="SS">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;Only for this Product
                                <input type="radio" name="assign_type" class="type" value="N">
                                <span class="checkmark"></span>
                                </label>
                                {{-- Seller specific --}}
                                {{-- <label for="ord_sp">Order specific</label>
                                <input type="radio" id="ord_sp" name="order_specific" value="Order Specific" class="form-control" />Order specific
                                <label for="seller_sp">Seller specific</label>
                                <input type="radio" id="seller_sp" name="order_specific" value="Order Specific" class="form-control" />Seller specific --}}
                                {{-- <span class="error_assign_driver" style="color: red;"></span> --}}
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="reassignDriver" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    	</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="add_notes_popup" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.notes')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="notes_form" method="post" action="javascript:void(0)">
                        @csrf
                        <div class="form-row">
                            <input id="order_id_notes" name="order_id_notes" type="hidden" class="form-control" placeholder="" readonly>
                            
                            <div class="form-group col-md-12">
                                <label>
                                @lang('admin_lang.notes')
                                </label>
                                <textarea class="form-control" id="add_notes_text" class="add_notes_text"></textarea>
                                <span class="error_add_notes" style="color: red;"></span>
                            </div>
                            
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="addNotes" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    	</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="push_notify_popup" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Send Push notification</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="send_push_notify_form" method="post" action="javascript:void(0)">
                        @csrf
                        <div class="form-row">
                            <input id="order_id_notification" name="order_id_notification" type="hidden" class="form-control" placeholder="" readonly>
                            
                            <div class="form-group col-md-12">
                                <label>
                                Push notification Text in English
                                </label>
                                <textarea class="form-control" id="push_notify_eng" class="push_notify_eng"></textarea>
                                <span class="error_text_english" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label>
                                Push notification Text in Arabic
                                </label>
                                <textarea class="form-control" id="push_notify_ar" class="push_notify_ar"></textarea>
                                <span class="error_text_arabic" style="color: red;"></span>
                            </div>
                            
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="pushNotification" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    	</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" />
<script type="text/javascript">
$( function() {
    $(".datepicker").datepicker({dateFormat: "yy-mm-dd",
		defaultDate: new Date(),
		maxDate: new Date(),
		changeMonth: true,
		changeYear: true,
		yearRange: '-100:+0'
 	});      	
})
$(document).ready(function () {
	var markOrders = [];
	
	$('body').on('click', '.select_all', function() {
		markOrders = [];
		if($(this).is(':checked')) {
			$('.mark_order').prop('checked', true);
			$('.mark_order').each(function(item, index) {
				markOrders.push($(this).val())
			});	
		} else {
	        $('.mark_order').prop('checked', false);
	        $('.mark_order').each(function(item, index1) {
				const index = markOrders.indexOf($(this).val());
		        if (index > -1) {
		           markOrders.splice(index, 1);
		        }
			});
		}
		if(markOrders.length) {
			$('.mark_delete').removeAttr('disabled')
			$('.delete_mark_text').html(markOrders.length + ' Orders Seleted')
			$('.print_mark_text').html(markOrders.length + ' Orders Seleted')
		}
		if(markOrders.length == 0) {
			$('.mark_delete').attr('disabled', 'disabled')
			$('.delete_mark_text').html(' Delete Selected')
			$('.print_mark_text').html(' Print Selected')
		}
	});

	$('body').on('click', '.mark_order', function() {
		if($(this).is(':checked')) {
			markOrders.push($(this).val())
		} else {
			const index = markOrders.indexOf($(this).val());
	        if (index > -1) {
	           markOrders.splice(index, 1);
	        }
	        $('.select_all').prop('indeterminate', true);
		}
		if(markOrders.length) {
			$('.mark_delete').removeAttr('disabled')
			$('.delete_mark_text').html(markOrders.length + ' Orders Seleted')
			$('.print_mark_text').html(markOrders.length + ' Orders Seleted')
		}
		if(markOrders.length == 0) {
			$('.mark_delete').attr('disabled', 'disabled')
			$('.delete_mark_text').html(' Delete Selected')
			$('.print_mark_text').html(' Print Selected')
		}
	})

	$('.mark_delete').click(function() {
		if(confirm('Do you really want to delete selected orders?')) {
			if(markOrders.length > 100) {
				toastr.error('Maximum 100 orders can be delete at a time');
			} else if(markOrders.length == 0) {
				toastr.error('Please select at least 1 order');
				$('.mark_delete').attr('disabled', 'disabled')
			} else {
				$('.delete_icon').hide();
				$('.delete_loader').show();
				$('.delete_mark_text').html('Deleting')
				$('.mark_delete').attr('disabled', 'disabled')
				$.ajax({
					url: '{{ route('admin.delete.mark.orders') }}',
					type: 'post',
					data: {
						jsonrpc: '2.0',
						params: {
							order_id: markOrders
						}
					},
					success: function(response) {
						$('.delete_icon').show();
						$('.delete_loader').hide();
						$('.delete_mark_text').html('Delete Seleted')
						$('#myTable1').DataTable().draw();
					}
				})
			}	
		}
	});

	// Print bulk order
	$('.print_order_details').on('click',function(){
		if(markOrders.length <= 100){
			if(markOrders.length <1) {
				toastr.error('Please select at least 1 order');
			} else {
				orderIds = btoa(JSON.stringify(markOrders));
				window.open("admin/bulk-print-orders/"+orderIds ,orderIds,"location=0,toolbar=no,scrollbars=yes,height=850,width=800,left=100,top=10");
			}
		} else {
			toastr.error('Maximum 100 orders can be selected at a time');
		}
	});

	$('body').on('click', '.reset_search', function() {
		$('#myTable1').DataTable().search('').columns().search('').draw();
		$(".slct").val("").trigger("chosen:updated");
		// reset status select picker
		$('.selectpicker').selectpicker('deselectAll');
	});
	function filterColumn ( i ) {
        $('#myTable1').DataTable().column( i ).search(
        	$('#col'+i+'_filter').val(),
        ).draw();
    }
	function getStatus(data, type, full, meta) {
		if(data == 'I'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.incomplete')"+"</span>"
       	} else if(data == 'N'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.new')"+"</span>"
		} else if(data == 'INP'){
			return "<span class='status_"+full.id+"'>"+"In Progress"+"</span>"
		} else if(data == 'CM'){
			return "<span class='status_"+full.id+"'>"+"Completed"+"</span>"
		} else if(data == 'OA'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.order_accepted')"+"</span>"
		} else if(data == 'DA'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.driver_assigned')"+"</span>"
		} else if(data == 'RP'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.ready_for_pickup')"+"</span>"
		} else if(data == 'OP'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.ord_picked_up')"+"</span>"
		} else if(data == 'OD'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.ord_dlv')"+"</span>"
		} else if(data == 'OC'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.order_canceled')"+"</span>"
		} else if(data == 'PP'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.partial_pickup')"+"</span>"
		} else if(data == 'PC'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.partial_pickup_completed')"+"</span>"
		} else if(data == 'F'){
			return "<span class='status_"+full.id+"'>Payment Failed</span>"
		}
	}
	function getPaymentMethod(data, type, full, meta){
		if(data == 'C'){
			return "<span class='change_payment_"+full.id+"'>"+"@lang('admin_lang.cod')"+"</span>"
		}
		else if(data == 'O'){
			return "<span class='change_payment_"+full.id+"'>"+"@lang('admin_lang.online')"+"</span>"
		}else{
			return "<span class='change_payment_"+full.id+"'>--</span>"
		}
	}
	function getOrderType(data, type, full, meta){
		if(data == 'I'){
			return "@lang('admin_lang.internal_1')"
		}
		else if(data == 'E'){
			return '@lang('admin_lang.external_1')'
		}
	}
	function getCustomerName(data, type, full, meta){
		return data.shipping_fname+' '+data.shipping_lname ;
	}
	var styles = `
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
`
var styleSheet = document.createElement("style");
styleSheet.innerText = styles;
document.head.appendChild(styleSheet);
	$('#myTable1').DataTable({
		stateSave: false,
		order: [
			[0, "desc"]
		],
		stateLoadParams: function (settings, data) {
            $('#col0_filter').val(data.search.order_no);
            $('#col7_filter').val(data.search.order_type);
            $('#col8_filter').val(data.search.payment_method);
            $('#col5_filter').val(data.search.status);
            $('#col4_filter').val(data.search.merchant);
            $('#col11_filter').val(data.search.invoice_details);
            $('#col14_filter').val(data.search.driver_search);
            $('#col17_filter').val(data.search.order_from);
            $('#col3_filter').val(data.search.customer_search);
            $('#col19_filter').val(data.search.shipping_city_search);
            $('#col2_filter').val(data.search.kuwait_search_city_nm);
            $('#col6_filter').val(data.search.txn_id);
            $('#col22_filter').val(data.search.coupon_code);
            $('#col23_filter').val(data.search.external_order_from);
            $('#col1_filter_from').val(data.search.from_date);
            $('#col1_filter_to').val(data.search.to_date);
            // for status multi-select
            $('.selectpicker').selectpicker('val', data.search.status);
            if(data.search.order_type == 'E') {
				$('.show_external_type').show();
			}
        },
        stateSaveParams: function (settings, data) {
            data.search.order_no = $('#col0_filter').val();
            data.search.invoice_details = $('#col11_filter').val();
            data.search.shipping_city_search = $('#col19_filter').val();
            data.search.order_type = $('#col7_filter').val();
            data.search.payment_method = $('#col8_filter').val();
            data.search.status = $('#col5_filter').val();
            data.search.merchant = $('#col4_filter').val();
            data.search.driver_search = $('#col14_filter').val();
            data.search.customer_search = $('#col3_filter').val();
            data.search.kuwait_search_city_nm = $('#col2_filter').val();
            data.search.from_date = $('#col1_filter_from').val();
            data.search.to_date = $('#col1_filter_to').val();
            data.search.order_from = $('#col17_filter').val();
            data.search.txn_id = $('#col6_filter').val();
            data.search.coupon_code = $('#col22_filter').val();
            data.search.external_order_from = $('#col23_filter').val();
        },
        processing: true,
        serverSide: true,
        serverMethod: 'post',
        ajax: {
        	"url": "{{ route('admin.orderwiselists') }}",
        	"data": function (d) {
				d._token = "{{ @csrf_token() }}";
			},
			"dataSrc": function(response) {
				$('#showOnline').html(response.orderTotal+' Rs');
				$('#showCod').html(response.totalCodShpPrc+' Rs');
				$('#showCodSub').html(response.totalCodSubtotal+' Rs');
				$('#showOnlineSub').html(response.totalOnlineSubtotal+' Rs');
				$('#showTotalDel').html(response.totalOrderDelivered);
				return response.aaData
			}
		},
		'columns': [
			{
				data: 'order_no',
			},
			{
				data: 'created_at'
			},
			{
				data: 'shipping_fname',
				render:function(data, type, full, meta){
					if(full.order_type == 'I'){
						var fname="--",lname="--";
						if(full.shipping_fname == null || full.shipping_fname == ""){
							fname = "--";
							return fname+" "+lname;
						}
						else if(full.shipping_lname == null || full.shipping_lname == "")
						{
							lname = "--";
							return fname+" "+lname;
						}else{
							return full.shipping_fname+" "+full.shipping_lname;
						}
					}else{
						var fname="--",lname="--";
						if(full.fname == null || full.fname == ""){
							fname = "--";
							return fname+" "+lname;
						}
						else if((full.fname != null || full.fname != "") && (full.lname == null || full.lname == "")){
							return full.fname;
						}
						else if(full.lname == null || full.lname == "")
						{
							lname = "--";
							return fname+" "+lname;
						}
						else{
							return full.fname+" "+full.lname;
						}
					}
				}
			},
			{
				data: "payable_amount"
			},
			{
				data: "insurance_price"
			},
			{
				data: 'status',
				render:getStatus
			},
			{
				data: 'status',
				render:function(data, id, full, meta) {
			       	var a = '';
			       	a += '<a href="{{ url('admin/view-order-details').'/' }}'+full.id+'"><i class="fas fa-eye" title="@lang('admin_lang.view')"></i></a>'
				    
				    if(full.order_type == 'E'){
				    	// a += '<a href="{{ url('admin/edit-external-order-details').'/' }}'+full.id+'"><i class="fas fa-edit" title="@lang('admin_lang.Edit')"></i></a>'
				    }

				    if(full.order_type == 'I'){
				    	// a += '<a href="{{ url('admin/edit-internal-order').'/' }}'+full.id+'"><i class="fas fa-edit" title="@lang('admin_lang.Edit')"></i></a>'
				    }
				    
				    if(data == 'N') {
						// a += '<a href="javascript:void(0)" data-toggle="modal" data-target="#assign_driver_popup_show" class="assign_driver_popup assign_dr_'+full.id+'" data-id="'+full.id+'" data-otype="'+full.order_tyoe+'"><i class=" fas fa-male" title="'+"@lang('admin_lang.assign_driver')"+'""></i></a>';
						// a += '&nbsp; <a href="javascript:void(0)" data-toggle="modal" data-target="#reassign_driver_popup_show" class="reassign_driver_popup reassign_dr_'+full.id+'" data-id="'+full.id+'" data-otype="'+full.order_type+'" style="display:none;"><i class=" fas fa-male" title="'+"@lang('admin_lang.reassign_driver')"+'"></i></a>';
					} else if(data != 'F' && data != 'I') {
						// a += '<a href="javascript:void(0)" data-toggle="modal" data-target="#reassign_driver_popup_show" class="reassign_driver_popup reassign_dr_'+full.id+'" data-id="'+full.id+'" data-otype="'+full.order_type+'"><i class=" fas fa-male" title="'+"@lang('admin_lang.reassign_driver')"+'"></i></a>';
						// a += '<a href="javascript:void(0)" data-toggle="modal" data-target="#assign_driver_popup_show" class="assign_driver_popup assign_dr_'+full.id+'" data-id="'+full.id+'" data-otype="'+full.order_tyoe+'" style="display:none;"><i class=" fas fa-male" title="'+"@lang('admin_lang.assign_driver')"+'"></i></a>';
					}

					if(data == 'PC'){
						// a += '<a href="javascript:void(0)" data-final="Y" class="final_assign f_assign_dr_'+full.id+'" data-id="'+full.id+'" ><i class=" fas fa-check" title="'+"@lang('admin_lang.final_assign_driver')"+'""></i></a>';
					}
					if(data != 'F' && data != 'I'){
						// a += '<a href="javascript:void(0)" data-toggle="modal" data-target="#add_notes_popup" class="add_notes add_notes_'+full.id+'" data-id="'+full.id+'" data-otype="'+full.order_type+'" style="display:block;"><i class="fas fa-plus" title="'+"@lang('admin_lang.add_notes')"+'"></i></a>';	
					}

				// 	if(data == 'I' || data == 'F') {
				// 		a += '<a onclick="return confirm(\'Do you really want to complete this order?\')" href="{{ url('admin/complete-order') }}/'+full.id+'"><i class=" fas fa-check" title="Complete Order"></i></a>';
				// 	}
				    
				    
				    if(full.status != 'I'){

				    	if(full.status != 'OC' && full.status != 'OD'){
				    		// a += '&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" class="change_order_status_admin change_order_status_admin'+full.id+'" data-id="'+full.id+'" data-otype="'+full.order_type+'" style="display:block;margin-left:10px;"><i class="fas fa-check" title="'+"Change Order Status"+'"></i></a>';
				    	}
				    	// if(full.status != 'OC' && full.status != 'OD'){
				    		
				    	// 	a += '&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" class="delete_order order_delete_'+full.id+'" data-id="'+full.id+'" data-otype="'+full.order_type+'" style="display:block;margin-left:10px;"><i class="fa fa-trash" title="'+"@lang('admin_lang.order_delete')"+'"></i></a>';

				    	// 	a += '&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" class="cancel_order_change_order_status_admin cancel_order_change_order_status_admin'+full.id+'" data-id="'+full.id+'" data-otype="'+full.order_type+'" style="display:block;margin-left:10px;"><i class="fas fa-ban" title="'+"Cancel Order"+'"></i></a>';
				    	// }
				    }else{
				    	// a += '&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" class="delete_order order_delete_'+full.id+'" data-id="'+full.id+'" data-otype="'+full.order_type+'" style="display:block;margin-left:10px;"><i class="fa fa-trash" title="'+"@lang('admin_lang.order_delete')"+'"></i></a>';
				    }

					return '<div class="action_col">'+a+'</div>';
				},
			},
			// {
			// 	data:'status',
			// 	render:function(data, id, full, meta){
			// 		var a = '';
			// 		if(full.order_type == 'I'){
			// 			if(data == 'N'){
			// 				a += '<select name="status_change_order_mstr'+full.id+'" class="form-control type status select slt slct any_status_change" id="status_change_order_mstr'+full.id+'" data-id="'+full.id+'" style="width:auto;">\n\
	  //                           	<option value="N" selected>'+"@lang('admin_lang.new')"+'</option>\n\
	  //                           	<option value="DA" >'+"@lang('admin_lang.driver_assigned')"+'</option>\n\
	  //                           	<option value="RP">'+"@lang('admin_lang.ready_for_pickup')"+'</option>\n\
	  //                           	<option value="OP">'+"@lang('admin_lang.ord_picked_up')"+'</option>\n\
	  //                           	<option value="OD">'+"@lang('admin_lang.ord_dlv')"+'</option>\n\
	  //                           	<option value="OC">'+"@lang('admin_lang.order_cancel')"+'</option>\n\
	  //                       </select>';	
			// 			}
			// 			else if(data == 'DA'){
			// 				a += '<select name="status_change_order_mstr'+full.id+'" class="form-control type status select slt slct any_status_change" id="status_change_order_mstr'+full.id+'" data-id="'+full.id+'" style="width:auto;">\n\
	  //                           <option value="N">'+"@lang('admin_lang.new')"+'</option>\n\
	  //                           <option value="DA" selected>'+"@lang('admin_lang.driver_assigned')"+'</option>\n\
	  //                           <option value="RP">'+"@lang('admin_lang.ready_for_pickup')"+'</option>\n\
	  //                           <option value="OP">'+"@lang('admin_lang.ord_picked_up')"+'</option>\n\
	  //                           <option value="OD">'+"@lang('admin_lang.ord_dlv')"+'</option>\n\
	  //                           <option value="OC">'+"@lang('admin_lang.order_cancel')"+'</option>\n\
	  //                       </select>';	
			// 			}
			// 			else if(data == 'RP'){
			// 				a += '<select name="status_change_order_mstr'+full.id+'" class="form-control type status select slt slct any_status_change" id="status_change_order_mstr'+full.id+'" data-id="'+full.id+'" style="width:auto;">\n\
	  //                           <option value="N">'+"@lang('admin_lang.new')"+'</option>\n\
	  //                           <option value="DA" >'+"@lang('admin_lang.driver_assigned')"+'</option>\n\
	  //                           <option value="RP" selected>'+"@lang('admin_lang.ready_for_pickup')"+'</option>\n\
	  //                           <option value="OP" >'+"@lang('admin_lang.ord_picked_up')"+'</option>\n\
	  //                           <option value="OD">'+"@lang('admin_lang.ord_dlv')"+'</option>\n\
	  //                           <option value="OC">'+"@lang('admin_lang.order_cancel')"+'</option>\n\
	  //                       </select>';	
			// 			}
			// 			else if(data == 'OP'){
			// 				a += '<select name="status_change_order_mstr'+full.id+'" class="form-control type status select slt slct any_status_change" id="status_change_order_mstr'+full.id+'" data-id="'+full.id+'" style="width:auto;">\n\
	  //                           <option value="N">'+"@lang('admin_lang.new')"+'</option>\n\
	  //                           <option value="DA" >'+"@lang('admin_lang.driver_assigned')"+'</option>\n\
	  //                           <option value="RP">'+"@lang('admin_lang.ready_for_pickup')"+'</option>\n\
	  //                           <option value="OP" selected>'+"@lang('admin_lang.ord_picked_up')"+'</option>\n\
	  //                           <option value="OD">'+"@lang('admin_lang.ord_dlv')"+'</option>\n\
	  //                           <option value="OC">'+"@lang('admin_lang.order_cancel')"+'</option>\n\
	  //                       </select>';	
			// 			}
			// 			else if(data == 'OD'){
			// 				a += '<select name="status_change_order_mstr'+full.id+'" class="form-control type status select slt slct any_status_change" id="status_change_order_mstr'+full.id+'" data-id="'+full.id+'" style="width:auto;">\n\
	  //                           <option value="N">'+"@lang('admin_lang.new')"+'</option>\n\
	  //                           <option value="DA" >'+"@lang('admin_lang.driver_assigned')"+'</option>\n\
	  //                           <option value="RP">'+"@lang('admin_lang.ready_for_pickup')"+'</option>\n\
	  //                           <option value="OP">'+"@lang('admin_lang.ord_picked_up')"+'</option>\n\
	  //                           <option value="OD" selected>'+"@lang('admin_lang.ord_dlv')"+'</option>\n\
	  //                           <option value="OC">'+"@lang('admin_lang.order_cancel')"+'</option>\n\
	  //                       </select>';	
			// 			}
			// 			else if(data == 'OC'){
			// 				a += '<select name="status_change_order_mstr'+full.id+'" class="form-control type status select slt slct any_status_change" id="status_change_order_mstr'+full.id+'" data-id="'+full.id+'" style="width:auto;">\n\
	  //                           <option value="N">'+"@lang('admin_lang.new')"+'</option>\n\
	  //                           <option value="DA" >'+"@lang('admin_lang.driver_assigned')"+'</option>\n\
	  //                           <option value="RP">'+"@lang('admin_lang.ready_for_pickup')"+'</option>\n\
	  //                           <option value="OP">'+"@lang('admin_lang.ord_picked_up')"+'</option>\n\
	  //                           <option value="OD">'+"@lang('admin_lang.ord_dlv')"+'</option>\n\
	  //                           <option value="OC" selected>'+"@lang('admin_lang.order_cancel')"+'</option>\n\
	  //                       </select>';	
			// 			}
			// 		}else{
			// 			if(data == 'N'){
			// 				a += '<select name="status_change_order_mstr'+full.id+'" class="form-control type status select slt slct any_status_change" id="status_change_order_mstr'+full.id+'" data-id="'+full.id+'" style="width:auto;">\n\
	  //                           <option value="N" selected>'+"@lang('admin_lang.new')"+'</option>\n\
	  //                           <option value="DA" >'+"@lang('admin_lang.driver_assigned')"+'</option>\n\
	  //                           <option value="OP">'+"@lang('admin_lang.ord_picked_up')"+'</option>\n\
	  //                           <option value="OD">'+"@lang('admin_lang.ord_dlv')"+'</option>\n\
	  //                           <option value="OC">'+"@lang('admin_lang.order_cancel')"+'</option>\n\
	  //                       </select>';	
			// 			}	
			// 			else if(data == 'OA'){
			// 				a += '<select name="status_change_order_mstr'+full.id+'" class="form-control type status select slt slct any_status_change" id="status_change_order_mstr'+full.id+'" data-id="'+full.id+'" style="width:auto;">\n\
	  //                           <option value="N">'+"@lang('admin_lang.new')"+'</option>\n\
	  //                           <option value="DA" >'+"@lang('admin_lang.driver_assigned')"+'</option>\n\
	  //                           <option value="OP">'+"@lang('admin_lang.ord_picked_up')"+'</option>\n\
	  //                           <option value="OD">'+"@lang('admin_lang.ord_dlv')"+'</option>\n\
	  //                           <option value="OC">'+"@lang('admin_lang.order_cancel')"+'</option>\n\
	  //                       </select>';	
			// 			}
			// 			else if(data == 'DA'){
			// 				a += '<select name="status_change_order_mstr'+full.id+'" class="form-control type status select slt slct any_status_change" id="status_change_order_mstr'+full.id+'" data-id="'+full.id+'" style="width:auto;">\n\
	  //                           <option value="N">'+"@lang('admin_lang.new')"+'</option>\n\
	  //                           <option value="DA" selected>'+"@lang('admin_lang.driver_assigned')"+'</option>\n\
	  //                           <option value="OP">'+"@lang('admin_lang.ord_picked_up')"+'</option>\n\
	  //                           <option value="OD">'+"@lang('admin_lang.ord_dlv')"+'</option>\n\
	  //                           <option value="OC">'+"@lang('admin_lang.order_cancel')"+'</option>\n\
	  //                       </select>';	
			// 			}
			// 			else if(data == 'OD'){
			// 				a += '<select name="status_change_order_mstr'+full.id+'" class="form-control type status select slt slct any_status_change" id="status_change_order_mstr'+full.id+'" data-id="'+full.id+'" style="width:auto;">\n\
	  //                           <option value="N">'+"@lang('admin_lang.new')"+'</option>\n\
	  //                           <option value="DA" >'+"@lang('admin_lang.driver_assigned')"+'</option>\n\
	  //                           <option value="OP">'+"@lang('admin_lang.ord_picked_up')"+'</option>\n\
	  //                           <option value="OD" selected>'+"@lang('admin_lang.ord_dlv')"+'</option>\n\
	  //                           <option value="OC" >'+"@lang('admin_lang.order_cancel')"+'</option>\n\
	  //                       </select>';	
			// 			}
			// 			else if(data == 'OP'){
			// 				a += '<select name="status_change_order_mstr'+full.id+'" class="form-control type status select slt slct any_status_change" id="status_change_order_mstr'+full.id+'" data-id="'+full.id+'" style="width:auto;">\n\
	  //                           <option value="N">'+"@lang('admin_lang.new')"+'</option>\n\
	  //                           <option value="DA" >'+"@lang('admin_lang.driver_assigned')"+'</option>\n\
	  //                           <option value="OP" selected>'+"@lang('admin_lang.ord_picked_up')"+'</option>\n\
	  //                           <option value="OD">'+"@lang('admin_lang.ord_dlv')"+'</option>\n\
	  //                           <option value="OC">'+"@lang('admin_lang.order_cancel')"+'</option>\n\
	  //                       </select>';	
			// 			}else if(data == 'OC'){
			// 				a += '<select name="status_change_order_mstr'+full.id+'" class="form-control type status select slt slct any_status_change" id="status_change_order_mstr'+full.id+'" data-id="'+full.id+'" style="width:auto;">\n\
	  //                           <option value="N">'+"@lang('admin_lang.new')"+'</option>\n\
	  //                           <option value="DA" >'+"@lang('admin_lang.driver_assigned')"+'</option>\n\
	  //                           <option value="OP">'+"@lang('admin_lang.ord_picked_up')"+'</option>\n\
	  //                           <option value="OD">'+"@lang('admin_lang.ord_dlv')"+'</option>\n\
	  //                           <option value="OC" selected>'+"@lang('admin_lang.order_cancel')"+'</option>\n\
	  //                       </select>';	
			// 			}
			// 		}
   //                  return a;
			// 	}
			// },
			// {
			// 	data:'payment_method',
			// 	render:function(data, id, full, meta){
			// 		var a = '';
			// 		if(full.order_type == 'E'){
			// 			if(full.payment_method == 'C'){
			// 				a += '<select name="change_payment_method'+full.id+'" class="form-control type status select slt slct change_payment_method" id="change_payment_method'+full.id+'" data-id="'+full.id+'" style="width:186px;">\n\
		 //                            <option value="C" selected>'+"@lang('admin_lang.cash_on_delivery')"+'</option>\n\
		 //                            <option value="O">'+"@lang('admin_lang.online')"+'</option>\n\
		 //                        </select>';	
			// 				return a;
			// 			}else{
			// 				a += '<select name="change_payment_method'+full.id+'" class="form-control type status select slt slct change_payment_method" id="change_payment_method'+full.id+'" data-id="'+full.id+'" style="width:186px;">\n\
		 //                            <option value="C">'+"@lang('admin_lang.cash_on_delivery')"+'</option>\n\
		 //                            <option value="O" selected>'+"@lang('admin_lang.online')"+'</option>\n\
		 //                        </select>';	
			// 				return a;
			// 			}					
			// 		}
			// 		if(full.status != 'I' && full.status != 'F') {
			// 			if(full.order_type == 'I' && full.payment_method == 'C' && full.is_paid != 'Y'){
			// 				a += '<select name="change_payment_method'+full.id+'" class="form-control type status select slt slct change_payment_method" id="change_payment_method'+full.id+'" data-id="'+full.id+'" style="width:186px;">\n\
		 //                            <option value="C" selected>'+"@lang('admin_lang.cash_on_delivery')"+'</option>\n\
		 //                            <option value="O">'+"@lang('admin_lang.online')"+'</option>\n\
		 //                        </select>';	
			// 				return a;
			// 			}else if(full.order_type == 'I' && full.payment_method == 'F'){
			// 				a += '<select name="change_payment_method'+full.id+'" class="form-control type status select slt slct change_payment_method" id="change_payment_method'+full.id+'" data-id="'+full.id+'" style="width:186px;">\n\
		 //                            <option value="">Select Payment Method</option>\n\
		 //                            <option value="O">'+"@lang('admin_lang.online')"+'</option>\n\
		 //                            <option value="C">'+"@lang('admin_lang.cash_on_delivery')"+'</option>\n\
		 //                        </select>';	
			// 				return a;
			// 			}else if(full.order_type == 'I' && full.is_paid != 'Y'){
			// 				if(full.payment_method == 'O'){
			// 					a += '<select name="change_payment_method'+full.id+'" class="form-control type status select slt slct change_payment_method" id="change_payment_method'+full.id+'" data-id="'+full.id+'" style="width:186px;">\n\
		 //                            <option value="O" selected>'+"@lang('admin_lang.online')"+'</option>\n\
		 //                            <option value="C">'+"@lang('admin_lang.cash_on_delivery')"+'</option>\n\
		 //                        </select>';	
			// 				}else{
			// 					a += '<select name="change_payment_method'+full.id+'" class="form-control type status select slt slct change_payment_method" id="change_payment_method'+full.id+'" data-id="'+full.id+'" style="width:186px;">\n\
		 //                            <option value="O">'+"@lang('admin_lang.online')"+'</option>\n\
		 //                            <option value="C" selected>'+"@lang('admin_lang.cash_on_delivery')"+'</option>\n\
		 //                        </select>';	
			// 				}
			// 				return a;
			// 			}
			// 			else{
			// 				return " ";
			// 			}    
			// 		} else {
			// 			return " ";
			// 		}
			// 	}
			// },
			// {
			// 	data:'admin_id',
			// 	render: function(data, type, full) {
			// 		if(full.get_admin_for_external_order != null){
			// 			if(full.admin_id == 1){
			// 				return full.get_admin_for_external_order.fname+" "+full.get_admin_for_external_order.lname+" (@lang('admin_lang.admin'))";
			// 			}else{
			// 				return full.get_admin_for_external_order.fname+" "+full.get_admin_for_external_order.lname+" (@lang('admin_lang.sub_admin'))";
			// 			}
						
			// 		}else{
			// 			if(full.order_type == 'E'){
			// 				if(full.order_merchants.length != 0){
			// 					return full.order_merchants[0].order_seller.fname+" "+full.order_merchants[0].order_seller.lname+" (@lang('admin_lang.mer'))";
			// 				}else{
			// 					return "--";
			// 				}
			// 			}else{
			// 				return "--";
			// 			}
			// 		}
			// 	}
			// },
			// {
			// 	data:'creation_type',
			// 	render: function(data, type, full) {
			// 		if(full.creation_type == 'C'){
			// 			return "@lang('admin_lang.created')";
			// 		}else if(full.creation_type == 'E'){
			// 			return "@lang('admin_lang.edited')";
			// 		}else{
			// 			return "--";
			// 		}
			// 	}
			// },
			// {
			// 	render: function(data, id, full) {
			// 		if(full.edit_date != '') {
			// 			return full.edit_date
			// 		} else {
			// 			return ''
			// 		}
			// 	}
			// },
			// {
			// 	render: function(data, id, full) {
			// 		if(full.edited_by != null) {
			// 			return full.edited_by.fname +' '+full.edited_by.lname
			// 		} else {
			// 			return ''
			// 		}
			// 	}
			// },
			// {
			// 	data: 'order_from',
			// 	render: function(data, id, full, meta) {
			// 		if(full.order_from == 'A') {
			// 			return '<span class="badge badge-warning">Android</span>'
			// 		} else if(full.order_from == 'I') {
			// 			return '<span class="badge badge-danger">IOS</span>'
			// 		} else if(full.order_from == 'W') {
			// 			return '<span class="badge badge-primary">Web</span>'
			// 		} 
			// 	}
			// },
		]
	});
	$('input.keyword').on( 'keyup click', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('input.shipping_city_search').on( 'keyup click', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('input.shipping_city_nm').on( 'keyup click', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('.o_t').on( 'change', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('.payment_method').on( 'change', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('.type').on('change', function () {
		filterColumn( $(this).parents('div').data('column'));
	});
	$('.order_status').on('change', function () {
		filterColumn(5);
	});
	$('.merchant').on('change', function () {
		filterColumn( $(this).parents('div').data('column'));
	});
	$('.driver_search').on('change', function () {
		filterColumn( $(this).parents('div').data('column'));
	});
	$('.kuwait_search_city_nm').on('change', function () {
		filterColumn( $(this).parents('div').data('column'));
	});
	$('.customer_search').on('change', function () {
		filterColumn( $(this).parents('div').data('column'));
	});

	$('.coupon_code').on( 'keyup click', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	
	$('.external_order_type').on( 'keyup click', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('.from_date').on('change', function () {		
        if($('#col1_filter_to').val() == '' && $('#col1_filter_to').val() < $('#col1_filter_from').val()) {
            return false;
        } else {
			$('#myTable1').DataTable().column( 1 ).search(
				[$('#col1_filter_from').val(),$('#col1_filter_to').val()],
				).draw();
		}
	});
	$('.to_date').on('change', function () {		
        if($('#col1_filter_from').val() == '' && $('#col1_filter_from').val() > $('#col1_filter_to').val()) {
            return false;
        } else {
			$('#myTable1').DataTable().column( 1 ).search(
				[$('#col1_filter_from').val(),$('#col1_filter_to').val()],
				).draw();
		}
	});

	@if(@$key['merchant_id'])
	$('#myTable1').DataTable().column( 10 ).search(
		"{{ $key['merchant_id'] }}",
		).draw();
	$('#col4_filter').val("{{ $key['merchant_id'] }}");
	@endif

	$('#col7_filter').on('change',function(){
		var val = $(this).val();
		if(val == 'E'){
			$('.show_external_type').show()
		}else{
			$('.show_external_type').hide()
			$('#col23_filter').val('')
		}
	})


	$(document).on("click",".delete_order",function(e){
    	var id = $(this).attr("data-id"),
    	obj  = $(this);
    	if(confirm('Do you want to delete this order ?')){
        	$.ajax({
				url:"{{ route('admin.order.cancel') }}",
				type:"GET",
				data:{
					id:id
				},
				success:function(resp){
					if(resp == 1){
						$.ajax({
			        		url:"{{ route('admin.delete.temp.order') }}",
			        		type:"GET",
			        		data:{
			        			id:id
			        		},
			        		success:function(resp){
			        			if(resp == 1){
			        				$(".success_msg_div").show();
									$(".error_msg_div").hide();
									$(".success_msg").html("@lang('admin_lang.succ_ord_delete')");
									obj.parent().parent().parent().hide();
			        			}else{
			        				$(".error_msg_div").show();
									$(".success_msg_div").hide();
									$(".error_msg").html("@lang('admin_lang.err_ord_delete')");
			        			}
			        		}
			        	});
					}else{
						$(".error_msg_div").show();
						$(".success_msg_div").hide();
						$(".error_msg").html("@lang('admin_lang.err_ord_delete')");
					}
				}
			});
    	}
    });
    $(document).on("click",".change_order_status_admin",function(e){
    	var id = $(this).attr("data-id"),
    	obj = $(this),
    	reqData = {
                'jsonrpc' : '2.0',                
                '_token'  : '{{csrf_token()}}',
                'data'    : {
                'id'      : id
                }
            },
    	obj  = $(this);
    	if(confirm('Do you want to change order status this order ?')){
        	$.ajax({
				url:"{{ route('admin.ajax.order.status.change') }}",
				type:"POST",
				data:reqData,
				success:function(resp){
					// if(resp.output == 1){
						var status = resp.order.status;
						if(resp.order.status == 'OA'){
							status = "Order Accepted";
						}
						if(resp.order.status == 'OP'){
							status = "Order Pickedup";
						}
						if(resp.order.status == 'OD'){
							status = "Order Delivered";
							$(".cancel_order_change_order_status_admin").hide();
							obj.hide();
						}
			        	$(".success_msg_div").show();
						$(".error_msg_div").hide();
						$(".success_msg").html("Order Status Changed successfully!");
						$(".status_"+id).html(status);
					// }else{
					// 	$(".error_msg_div").show();
					// 	$(".success_msg_div").hide();
					// 	$(".error_msg").html("Order status is not changed!");
					// }
				}
			});
    	}
    });
    $(document).on("click",".cancel_order_change_order_status_admin",function(e){
    	var id = $(this).attr("data-id"),
    	reqData = {
                'jsonrpc' : '2.0',                
                '_token'  : '{{csrf_token()}}',
                'data'    : {
                'id'      : id
                }
            },
    	obj  = $(this);
    	if(confirm('Do you want to change order status this order ?')){
        	$.ajax({
				url:"{{ route('admin.cancel.order.ajax.order.status.change') }}",
				type:"POST",
				data:reqData,
				success:function(resp){
					if(resp.output == 1){
						var status = resp.order.status;
						if(resp.order.status == 'OA'){
							status = "Order Accepted";
						}
						if(resp.order.status == 'OP'){
							status = "Order Pickedup";
						}
						if(resp.order.status == 'OD'){
							status = "Order Delivered";
						}
			        	$(".success_msg_div").show();
						$(".error_msg_div").hide();
						$(".success_msg").html("Order Status Changed successfully!");
						$(".status_"+id).html(status);
					}else{
						$(".error_msg_div").show();
						$(".success_msg_div").hide();
						$(".error_msg").html("Order status is not changed!");
					}
				}
			});
    	}
    });

	$(document).on("click",".final_assign",function(e){
		$('#is_final_assign').val($(this).data('final'))
		$('#order_id_reassign').val($(this).data('id'))
		$('#reassign_driver_popup_show').modal('show');
	});
	
	$(document).on("click","#assignDriver",function(e){
        var assign = $.trim($("#assign").val());
        var drivername = $.trim($("#assign option:selected").text());
        var isFinalSubmit = 'N'
        if(assign == ""){
            $(".error_assign_driver").text('@lang('validation.required')');
        }else{
            $(".error_assign_driver").text("");
        }
        
        if(assign != ""){
        	if($('#is_final_assign').val() == 'Y') {
        		isFinalSubmit = 'Y'
        	}
        	var order_id = $("#order_id").val();
        	var st = $("#status_assign").val();
            $('#reassign_driver_popup_show').modal('hide');
        	$('#assign_driver_popup_show').modal('hide');
            $.ajax({
            	url:"{{ route('admin.driver.assign.entire.order') }}",
            	type:"GET",
            	data:{
            		order_id:order_id,
            		assign:assign,
            		is_final_assign: isFinalSubmit
            	},
            	success:function(resp){
            		if(resp == 1){
            			$(".driver_name_"+order_id).html(drivername);
            			$(".success_msg_div").show();
						$(".error_msg_div").hide();
						$(".success_msg").html("@lang('admin_lang.suc_driver_change')");
	              		}else{
	              			$(".error_msg_div").show();
						$(".success_msg_div").hide();
						$(".error_msg").html("@lang('admin_lang.driver_not_change')");
            		}
            	}
            });
        }
    });

    $(document).on("click","#addNotes",function(e){
        var add_notes_text = $.trim($("#add_notes_text").val());
        if(add_notes_text == ""){
            $(".error_add_notes").text('@lang('validation.required')');
        }else{
            $(".error_add_notes").text("");
        }
        
        if(add_notes_text != ""){
        	var order_id_notes = $("#order_id_notes").val();
            $('#reassign_driver_popup_show').modal('hide');
        	$('#assign_driver_popup_show').modal('hide');
        	$('#add_notes_popup').modal('hide');
            $.ajax({
            	url:"{{ route('admin.update.notes') }}",
            	type:"GET",
            	data:{
            		id:order_id_notes,
            		notes:add_notes_text
            	},
            	success:function(resp){
            		if(resp == 1){
            			$(".success_msg_div").show();
						$(".error_msg_div").hide();
						$(".success_msg").html("@lang('admin_lang.suc_note_updated')");
						$(".notes_"+order_id_notes).html(add_notes_text);
	              		}else{
	              			$(".error_msg_div").show();
						$(".success_msg_div").hide();
						$(".error_msg").html("@lang('admin_lang.err_notes_not_updated')");
            		}
            	}
            });
        }
    });
	
    $(document).on("click","#addNotes",function(e){
        var add_notes_text = $.trim($("#add_notes_text").val());
        if(add_notes_text == ""){
            $(".error_add_notes").text('@lang('validation.required')');
        }else{
            $(".error_add_notes").text("");
        }
        
        if(add_notes_text != ""){
        	var order_id_notes = $("#order_id_notes").val();
            $('#reassign_driver_popup_show').modal('hide');
        	$('#assign_driver_popup_show').modal('hide');
        	$('#add_notes_popup').modal('hide');
            $.ajax({
            	url:"{{ route('admin.update.notes') }}",
            	type:"GET",
            	data:{
            		id:order_id_notes,
            		notes:add_notes_text
            	},
            	success:function(resp){
            		if(resp == 1){
            			$(".success_msg_div").show();
						$(".error_msg_div").hide();
						$(".success_msg").html("@lang('admin_lang.suc_note_updated')");
						$(".notes_"+order_id_notes).html(add_notes_text);
	              		}else{
	              			$(".error_msg_div").show();
						$(".success_msg_div").hide();
						$(".error_msg").html("@lang('admin_lang.err_notes_not_updated')");
            		}
            	}
            });
        }
    });

	
    $(document).on("click",".assign_driver_popup",function(e){
        var id= $.trim($(e.currentTarget).attr("data-id"));
        var otype= $.trim($(e.currentTarget).attr("data-otype"));
        var status= $.trim($(e.currentTarget).attr("data-status"));
        $('#assign_driver').attr("data-id",id);
        $('#assign_driver').attr("data-otype",id);
		// new
        $('#reassign_driver_popup_show').modal('hide');
        $('#assign_driver_popup_show').modal('hide');
        $("#order_id").val(id);
        $("#order_type_driver").val(otype);
        $("#assign_status").val(status);
    });

    $(document).on("click",".add_notes",function(e){
        var id= $.trim($(e.currentTarget).attr("data-id"));
        $('#order_id_notes').attr("data-id",id);
        $('#reassign_driver_popup_show').modal('hide');
        $('#assign_driver_popup_show').modal('hide');
        $("#order_id_notes").val(id);
    });

    $(document).on("click","#reassignDriver",function(e){
        var assign = $.trim($("#reassign").val());
        var drivername = $.trim($("#reassign option:selected").text());
        var isFinalSubmit = 'N'
        var order_id = $.trim($("#order_id_reassign").val());
        var st = $.trim($("#status_reassign").val());
        if(assign == ""){
            $(".error_reassign_driver").text('@lang('validation.required')');
        }else{
            $(".error_reassign_driver").text("");
        }
        
        if(assign != ""){
        	if($('#is_final_assign').val() == 'Y') {
        		isFinalSubmit = 'Y'
        	}
            $('#reassign_driver_popup_show').modal('hide');
        	$('#assign_driver_popup_show').modal('hide');
            $.ajax({
            	url:"{{ route('admin.driver.reassign.entire.order') }}",
            	type:"GET",
            	data:{
            		order_id:order_id,
            		assign:assign,
            		is_final_assign: isFinalSubmit
            	},
            	success:function(resp){
            		if(resp == 1){
            			$(".driver_name_"+order_id).html(drivername);
            			$(".success_msg_div").show();
						$(".error_msg_div").hide();
						$(".success_msg").html("@lang('admin_lang.suc_driver_change')");
	              		}else{
	              			$(".error_msg_div").show();
						$(".success_msg_div").hide();
						$(".error_msg").html("@lang('admin_lang.driver_not_change')");
            		}
            	}
            });
        }
    });
    
    $(document).on("click",".reassign_driver_popup",function(e){
        var id= $.trim($(e.currentTarget).attr("data-id"));
        var otype= $.trim($(e.currentTarget).attr("data-otype"));
        var status= $.trim($(e.currentTarget).attr("data-status"));
        
        $('#reassign_driver').attr("data-id",id);
        $('#reassign_driver').attr("data-otype",id);
        $('#assign_driver_popup_show').modal('hide');
        $('#reassign_driver_popup_show').modal('show');

        $("#order_id_reassign").val(id);
        $("#order_type_driver_reassign").val(otype);
        $("#status_reassign").val(status);
    });

	$("select[name=myTable1_length]").change(function() {
		markOrders = [];
		$('.mark_delete').attr('disabled', 'disabled')
		$('.delete_mark_text').html(' Delete Selected')
		$('.print_mark_text').html(' Print Selected')
		$('.select_all').prop('checked', false);
	});
	$('body').on('click','.paginate_button',function(){
		markOrders = [];
		$('.mark_delete').attr('disabled', 'disabled')
		$('.delete_mark_text').html(' Delete Selected')
		$('.print_mark_text').html(' Print Selected')
		$('.select_all').prop('checked', false);
	})
});
</script>
{{-- jayatri ajax action buttons work --}}
<script type="text/javascript">
	$(document).ready(function(){
		$('.slct').chosen();
		$(document).on("change",".any_status_change",function(e){
			var id = $(e.currentTarget).attr("data-id");
			status = $(this).val();
			if(status == 'OC'){
				var id = $(e.currentTarget).attr("data-id");
				$.ajax({
					url:"{{ route('admin.order.cancel') }}",
					type:"GET",
					data:{
						id:id
					},
					success:function(resp){
						if(resp == 1){
							$(".status_"+id).html("Cancelled");
							$(".success_msg_div").show();
							$(".error_msg_div").hide();
							$(".success_msg").html("@lang('admin_lang.ord_cancel_suc_msg')");
							$(".cn_or_"+id).hide();
						}else{
							$(".error_msg_div").show();
							$(".success_msg_div").hide();
							$(".error_msg").html('@lang('admin_lang.err_nw_q')');

						}
					}
				});
			}else{
				$.ajax({
					url:"{{ route('admin.update.status.order.master') }}",
					type:"GET",
					data:{
						id:id,
						status:status
					},
					success:function(resp){
						if(resp != 0){
							var status = resp.status,
							st = "";
							if(resp.status == 'OA'){
								st = "@lang('admin_lang.order_accepted')";
							}else if(resp.status == 'N'){
								st = "@lang('admin_lang.new')";
							}else if(resp.status == 'DA'){
								st = "Driver Assigned";
							}else if(resp.status == 'OP'){
								st = "@lang('admin_lang.ord_picked_up')";
							}else if(resp.status == 'RP'){
								st = "@lang('admin_lang.ready_pick_up')";
							}else if(resp.status == 'OD'){
								st = "@lang('admin_lang.delivered_1')";
							}
							$(".status_"+id).html(st);
							$(".success_msg_div").show();
							$(".error_msg_div").hide();
							$(".success_msg").html('@lang('admin_lang.success_st_ch_ord')');
							$(".ord_"+id).show();
						}else{
							$(".error_msg_div").show();
							$(".success_msg_div").hide();
							$(".error_msg").html('@lang('admin_lang.err_st_or_ch')');
						}
					}
				});
			}
		});


		$(document).on("change",".change_payment_method",function(e){
			var id = $(e.currentTarget).attr("data-id");
			status = $(this).val();
			if(status !=""){
				$.ajax({
					url:"{{ route('admin.update.payment.method') }}",
					type:"GET",
					data:{
						id:id
					},
					success:function(resp){
						if(resp != 0){
							var payment = resp.payment_method,
							st = "";
							if(payment == 'O'){
								st = "@lang('admin_lang.online')";
							}else if(payment == 'C'){
								st = "@lang('admin_lang.cod')";
							}
							$(".change_payment_"+id).html(st);
							$(".success_msg_div").show();
							$(".error_msg_div").hide();
							$(".success_msg").html("@lang('admin_lang.payment_method_changed')");
							$(this).hide();
						}else{
							$(".error_msg_div").show();
							$(".success_msg_div").hide();
							$(".error_msg").html("@lang('admin_lang.unautho_payment_method_err')");
						}
					}
				});
			}
		});

		$(document).on("click",".order_mstr_cancel",function(e){
			var id = $(e.currentTarget).attr("data-id"),
			ord_no = $(e.currentTarget).attr("data-ordno");
			$.ajax({
				url:"{{ route('admin.order.cancel') }}",
				type:"GET",
				data:{
					id:id
				},
				success:function(resp){
					
						$(".status_"+id).html("@lang('admin_lang.Cancelled_1')");
						$(".success_msg_div").show();
						$(".error_msg_div").hide();
						$(".success_msg").html("@lang('admin_lang.suc_ord_cancel_msg')");
						$(".cn_or_"+id).hide();
						$(".ord_"+id).hide();
					
				}
			});
		});
	});

	$('#col6_filter').selectpicker();
</script>
@endsection