@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | Payment Management
@endsection
@section('links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@include('admin.includes.links')


@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<!-- ============================================================== -->
			<!-- pageheader  -->
			<!-- ============================================================== -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="page-header">
						<h2 class="pageheader-title">Payment Management</h2>
						<div class="page-breadcrumb">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
									<li class="breadcrumb-item active" aria-current="page">Payment Management</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- end pageheader -->
			<!-- ============================================================== -->
			<div class="row">
				<!-- ============================================================== -->
				<!-- basic table  -->
				<!-- ============================================================== -->
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="card">
						<h5 class="card-header">Payment Management</h5>
						<div class="card-body">
								<div class="row">
									<div data-column="0" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="keyword" class="col-form-label">Search for Keywords</label>
										<input id="col0_filter" name="keyword" type="text" class="form-control keyword rs" placeholder="#Order/Seller name">
									</div>
									{{-- <div data-column="2" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="city_label_ku" class="col-form-label">City </label>
										<select data-placeholder="Select City" class="form-control select slt slct kuwait_search_city_nm rs"  tabindex="2" id="col2_filter" name="kuwait_search_city_nm">
											<option value="">Select City</option>
											@if(@$cities)
											@foreach(@$cities as $city)
											<option value="{{ @$city->id }}" >{{ @$city->name }} </option>
											@endforeach
											@endif
										</select>
									</div> --}}
									{{-- <div data-column="3" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="cust_label" class="col-form-label">Customer</label>
										<select data-placeholder="Select customer" class="form-control select slt slct  customer_search"  tabindex="3" id="col3_filter" name="customer_search">
											<option value="">Select Customer</option>
											@if(@$customers)
											@foreach(@$customers as $cus)
											<option value="{{ @$cus->id }}" >{{ @$cus->fname." ".@$cus->lname }} </option>
											@endforeach
											@endif
										</select>
									</div> --}}
									<div data-column="4" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="merchant_lb" class="col-form-label">@lang('admin_lang.merchant')</label>
										<select data-placeholder="@lang('admin_lang.choose_a_merchant')" class="form-control select slt slct merchant rs"  tabindex="4" id="col4_filter" name="merchant">
											<option value="">@lang('admin_lang.select_merchant')</option>
											@if(@$merchants)
											@foreach(@$merchants as $merchant)
											<option value="{{ @$merchant->id }}" @if(@$key['merchant'] == $merchant->id) selected @endif>{{ @$merchant->fname." ".@$merchant->lname }}</option>
											@endforeach
											@endif
										</select>
									</div>
									<div data-column="5" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="input-select" class="col-form-label">@lang('admin_lang.Status')</label>
										<select name="type[]" class="form-control order_status status select slt selectpicker"  data-live-search="true" id="col5_filter" multiple="">
											<option value='N'>New</option>
											<option value='OA'>Order Accepted</option>
											<option value='OP'>Order Picked Up</option>
											<option value='OD'>Order Delivered</option>
											
										</select>
									</div>
									<div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="to_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
										<input type="text" class="form-control datepicker from_date rs" name="from_date" placeholder="From Date" value="" readonly="" id="col1_filter_from">
									</div>
									<div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="to_date" class="col-form-label">Order Date</label>
										<input type="text" class="form-control datepicker to_date rs" name="to_date" placeholder="To Date" value="" readonly="" id="col1_filter_to">
									</div>
									{{-- <div data-column="6" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="input-select" class="col-form-label">Payment Txn ID</label>
										<input id="col6_filter" name="txn_no" type="text" class="form-control keyword" placeholder="Payment Txn ID">
									</div> --}}

									<div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
										<label for="search_orders_ser" class="col-form-label" style="padding: 12px;"> </label>
										<a href="javascript:void(0)" id="search_orders_ser" class="btn btn-primary ">Search</a>
										<input type="reset" value="Reset Search" class="btn btn-default reset_search">
									</div>
								</div>
							</form>
							<div class="table-responsive listBody">
								<table class="table table-striped table-bordered first" id="myTable1">
									<thead>
										<tr>
											<th>Order Number</th>
											<th>Seller Name</th>
											<th>Payable Amount(Rs)</th>
											<th>Insurance Price(Rs)</th>
											<th>Admin Commission(Rs)</th>
											<th>Seller Net Earnings(Rs)</th>
											<th>Status</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th>Order Number</th>
											<th>Seller Name</th>
											<th>Payable Amount(Rs)</th>
											<th>Insurance Price(Rs)</th>
											<th>Admin Commission(Rs)</th>
											<th>Seller Net Earnings(Rs)</th>
											<th>Status</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end basic table  -->
				<!-- ============================================================== -->
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- footer -->
		<!-- ============================================================== -->
		@include('admin.includes.footer')
		<!-- ============================================================== -->
		<!-- end footer -->
		<!-- ============================================================== -->
		<div class="loader" style="display: none;">
			<img src="{{url('public/loader.gif')}}">
		</div>
	</div>
</div>
@endsection
@section('scripts')
@include('admin.includes.scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" />
<script type="text/javascript">
$( function() {
    $(".datepicker").datepicker({dateFormat: "yy-mm-dd",
		defaultDate: new Date(),
		maxDate: new Date(),
		changeMonth: true,
		changeYear: true,
		yearRange: '-100:+0'
 	});      	
})
$(document).ready(function () {
	var markOrders = [];
	
	$('body').on('click', '.select_all', function() {
		markOrders = [];
		if($(this).is(':checked')) {
			$('.mark_order').prop('checked', true);
			$('.mark_order').each(function(item, index) {
				markOrders.push($(this).val())
			});	
		} else {
	        $('.mark_order').prop('checked', false);
	        $('.mark_order').each(function(item, index1) {
				const index = markOrders.indexOf($(this).val());
		        if (index > -1) {
		           markOrders.splice(index, 1);
		        }
			});
		}
		if(markOrders.length) {
			$('.mark_delete').removeAttr('disabled')
			$('.delete_mark_text').html(markOrders.length + ' Orders Seleted')
			$('.print_mark_text').html(markOrders.length + ' Orders Seleted')
		}
		if(markOrders.length == 0) {
			$('.mark_delete').attr('disabled', 'disabled')
			$('.delete_mark_text').html(' Delete Selected')
			$('.print_mark_text').html(' Print Selected')
		}
	});

	$('body').on('click', '.mark_order', function() {
		if($(this).is(':checked')) {
			markOrders.push($(this).val())
		} else {
			const index = markOrders.indexOf($(this).val());
	        if (index > -1) {
	           markOrders.splice(index, 1);
	        }
	        $('.select_all').prop('indeterminate', true);
		}
		if(markOrders.length) {
			$('.mark_delete').removeAttr('disabled')
			$('.delete_mark_text').html(markOrders.length + ' Orders Seleted')
			$('.print_mark_text').html(markOrders.length + ' Orders Seleted')
		}
		if(markOrders.length == 0) {
			$('.mark_delete').attr('disabled', 'disabled')
			$('.delete_mark_text').html(' Delete Selected')
			$('.print_mark_text').html(' Print Selected')
		}
	})

	$('.mark_delete').click(function() {
		if(confirm('Do you really want to delete selected orders?')) {
			if(markOrders.length > 100) {
				toastr.error('Maximum 100 orders can be delete at a time');
			} else if(markOrders.length == 0) {
				toastr.error('Please select at least 1 order');
				$('.mark_delete').attr('disabled', 'disabled')
			} else {
				$('.delete_icon').hide();
				$('.delete_loader').show();
				$('.delete_mark_text').html('Deleting')
				$('.mark_delete').attr('disabled', 'disabled')
				$.ajax({
					url: '{{ route('admin.delete.mark.orders') }}',
					type: 'post',
					data: {
						jsonrpc: '2.0',
						params: {
							order_id: markOrders
						}
					},
					success: function(response) {
						$('.delete_icon').show();
						$('.delete_loader').hide();
						$('.delete_mark_text').html('Delete Seleted')
						$('#myTable1').DataTable().draw();
					}
				})
			}	
		}
	});

	// Print bulk order
	$('.print_order_details').on('click',function(){
		if(markOrders.length <= 100){
			if(markOrders.length <1) {
				toastr.error('Please select at least 1 order');
			} else {
				orderIds = btoa(JSON.stringify(markOrders));
				window.open("admin/bulk-print-orders/"+orderIds ,orderIds,"location=0,toolbar=no,scrollbars=yes,height=850,width=800,left=100,top=10");
			}
		} else {
			toastr.error('Maximum 100 orders can be selected at a time');
		}
	});

	$('body').on('click', '.reset_search', function() {
		$('#myTable1').DataTable().search('').columns().search('').draw();
		// reset status select picker
		$(".rs").val("");
		$(".slct").val("").trigger("chosen:updated");
		$('.selectpicker').selectpicker('deselectAll');
	});
	function filterColumn ( i ) {
        $('#myTable1').DataTable().column( i ).search(
        	$('#col'+i+'_filter').val(),
        ).draw();
    }
	function getStatus(data, type, full, meta) {
		if(data == 'I'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.incomplete')"+"</span>"
       	} else if(data == 'N'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.new')"+"</span>"
		} else if(data == 'INP'){
			return "<span class='status_"+full.id+"'>"+"In Progress"+"</span>"
		}else if(data == 'CM'){
			return "<span class='status_"+full.id+"'>"+"Completed"+"</span>"
		} else if(data == 'OA'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.order_accepted')"+"</span>"
		} else if(data == 'DA'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.driver_assigned')"+"</span>"
		} else if(data == 'RP'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.ready_for_pickup')"+"</span>"
		} else if(data == 'OP'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.ord_picked_up')"+"</span>"
		} else if(data == 'OD'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.ord_dlv')"+"</span>"
		} else if(data == 'OC'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.order_canceled')"+"</span>"
		} else if(data == 'PP'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.partial_pickup')"+"</span>"
		} else if(data == 'PC'){
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.partial_pickup_completed')"+"</span>"
		} else if(data == 'F'){
			return "<span class='status_"+full.id+"'>Payment Failed</span>"
		}else{
			return "<span class='status_"+full.id+"'>"+data+"</span>"
		}
	}
	function getPaymentMethod(data, type, full, meta){
		if(data == 'C'){
			return "<span class='change_payment_"+full.id+"'>"+"@lang('admin_lang.cod')"+"</span>"
		}
		else if(data == 'O'){
			return "<span class='change_payment_"+full.id+"'>"+"@lang('admin_lang.online')"+"</span>"
		}else{
			return "<span class='change_payment_"+full.id+"'>--</span>"
		}
	}
	function getOrderType(data, type, full, meta){
		if(data == 'I'){
			return "@lang('admin_lang.internal_1')"
		}
		else if(data == 'E'){
			return '@lang('admin_lang.external_1')'
		}
	}
	function getCustomerName(data, type, full, meta){
		return data.shipping_fname+' '+data.shipping_lname ;
	}
	var styles = `
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
`
var styleSheet = document.createElement("style");
styleSheet.innerText = styles;
document.head.appendChild(styleSheet);
	$('#myTable1').DataTable({
		stateSave: false,
		order: [
			[0, "desc"]
		],
		stateLoadParams: function (settings, data) {
            $('#col0_filter').val(data.search.order_no);
            $('#col7_filter').val(data.search.order_type);
            $('#col8_filter').val(data.search.payment_method);
            $('#col5_filter').val(data.search.status);
            $('#col4_filter').val(data.search.merchant);
            $('#col11_filter').val(data.search.invoice_details);
            $('#col14_filter').val(data.search.driver_search);
            $('#col17_filter').val(data.search.order_from);
            $('#col3_filter').val(data.search.customer_search);
            $('#col19_filter').val(data.search.shipping_city_search);
            $('#col2_filter').val(data.search.kuwait_search_city_nm);
            $('#col6_filter').val(data.search.txn_id);
            $('#col22_filter').val(data.search.coupon_code);
            $('#col23_filter').val(data.search.external_order_from);
            $('#col1_filter_from').val(data.search.from_date);
            $('#col1_filter_to').val(data.search.to_date);
            // for status multi-select
            $('.selectpicker').selectpicker('val', data.search.status);
            if(data.search.order_type == 'E') {
				$('.show_external_type').show();
			}
        },
        stateSaveParams: function (settings, data) {
            data.search.order_no = $('#col0_filter').val();
            data.search.invoice_details = $('#col11_filter').val();
            data.search.shipping_city_search = $('#col19_filter').val();
            data.search.order_type = $('#col7_filter').val();
            data.search.payment_method = $('#col8_filter').val();
            data.search.status = $('#col5_filter').val();
            data.search.merchant = $('#col4_filter').val();
            data.search.driver_search = $('#col14_filter').val();
            data.search.customer_search = $('#col3_filter').val();
            data.search.kuwait_search_city_nm = $('#col2_filter').val();
            data.search.from_date = $('#col1_filter_from').val();
            data.search.to_date = $('#col1_filter_to').val();
            data.search.order_from = $('#col17_filter').val();
            data.search.txn_id = $('#col6_filter').val();
            data.search.coupon_code = $('#col22_filter').val();
            data.search.external_order_from = $('#col23_filter').val();
        },
        processing: true,
        serverSide: true,
        serverMethod: 'post',
        ajax: {
        	"url": "{{ route('admin.view.order.wise.payment.details') }}",
        	"data": function (d) {
				d._token = "{{ @csrf_token() }}";
			},
			"dataSrc": function(response) {
				// $('#showOnline').html(response.orderTotal+' Rs');
				// $('#showCod').html(response.totalCodShpPrc+' Rs');
				// $('#showCodSub').html(response.totalCodSubtotal+' Rs');
				// $('#showOnlineSub').html(response.totalOnlineSubtotal+' Rs');
				// $('#showTotalDel').html(response.totalOrderDelivered);
				return response.aaData
			}
		},
		'columns': [
			{
				data: 'order_master_tab.order_no',
				render:function(data, type, full, meta){
					return data;
				}
			},
			{
				data: 'order_merchant.fname',
				render:function(data, type, full, meta){
					var nm = "";
					if(full.order_merchant == null){
						return "";
					}
					if(full.order_merchant.fname != null){
						nm += full.order_merchant.fname;
					}
					nm += " ";
					if(full.order_merchant.lname != null){
						nm += full.order_merchant.lname;
					}
					return nm;
				}
			},
			{
				data: 'payable_amount'
			},
			{
				data: 'insurance_price'
			},
			{
				data: 'admin_commission'
			},
			{
				data: 'earning',
				render:function(data, type, full, meta){
					
					return data;
				}
			},
			{
				data: 'status',
				render:getStatus
			},
			
		]
	});
	$('input.keyword').on( 'keyup click', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('input.shipping_city_search').on( 'keyup click', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('input.shipping_city_nm').on( 'keyup click', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('.o_t').on( 'change', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('.payment_method').on( 'change', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('.type').on('change', function () {
		filterColumn( $(this).parents('div').data('column'));
	});
	$('.order_status').on('change', function () {
		filterColumn(5);
	});
	$('.merchant').on('change', function () {
		filterColumn( $(this).parents('div').data('column'));
	});
	$('.driver_search').on('change', function () {
		filterColumn( $(this).parents('div').data('column'));
	});
	$('.kuwait_search_city_nm').on('change', function () {
		filterColumn( $(this).parents('div').data('column'));
	});
	$('.customer_search').on('change', function () {
		filterColumn( $(this).parents('div').data('column'));
	});

	$('.coupon_code').on( 'keyup click', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	
	$('.external_order_type').on( 'keyup click', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('.from_date').on('change', function () {		
        if($('#col1_filter_to').val() == '' && $('#col1_filter_to').val() < $('#col1_filter_from').val()) {
            return false;
        } else {
			$('#myTable1').DataTable().column( 1 ).search(
				[$('#col1_filter_from').val(),$('#col1_filter_to').val()],
				).draw();
		}
	});
	$('.to_date').on('change', function () {		
        if($('#col1_filter_from').val() == '' && $('#col1_filter_from').val() > $('#col1_filter_to').val()) {
            return false;
        } else {
			$('#myTable1').DataTable().column( 1 ).search(
				[$('#col1_filter_from').val(),$('#col1_filter_to').val()],
				).draw();
		}
	});

	@if(@$key['merchant_id'])
	$('#myTable1').DataTable().column( 10 ).search(
		"{{ $key['merchant_id'] }}",
		).draw();
	$('#col4_filter').val("{{ $key['merchant_id'] }}");
	@endif

	$('#col7_filter').on('change',function(){
		var val = $(this).val();
		if(val == 'E'){
			$('.show_external_type').show()
		}else{
			$('.show_external_type').hide()
			$('#col23_filter').val('')
		}
	});
	$('.slct').chosen();
	$('#col6_filter').selectpicker();
});
</script>
@endsection