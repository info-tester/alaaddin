@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Add Customer') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.add_customer')
@endsection
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
<link href="{{ asset('public/frontend/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.cust')
                                    </li><li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.send_push_notification')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div id="showMsgIos"></div>
            <div id="showMsgAndroid"></div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                @lang('admin_lang.send_push_notification') 
                            </h5>
                            
                            <div class="card-body">
                                <form id="addCustomerForm" method="post" enctype="multipart/form-data" action="{{ route('admin.send.push.notification') }}">
                                    @csrf
                                    <input type="hidden" name="an_all" id="an_all" value="{{@$an_block}}">
                                    <input type="hidden" name="io_all" id="io_all" value="{{@$ios_block}}">
                                    <input type="hidden" name="ios_count"  value="{{@$ios_count}}">
                                    <input type="hidden" name="android_count"  value="{{@$android_count}}">
                                    <div class="row">
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="title_eng" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.title_in_english')</label>
                                            <textarea id="title_eng" name="title_eng" class="form-control required" placeholder="@lang('admin_lang.title_in_english')"></textarea>
                                            <!-- <input  type="text" class="form-control required" > -->
                                            <span class="error_fname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="title_ar" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.title_in_arabic')
                                                <span style="color: red;">*</span>
                                            </label>
                                            <textarea id="title_ar" name="title_ar" class="form-control" placeholder="@lang('admin_lang.title_in_arabic')"></textarea>
                                            <!-- <input  type="text" > -->
                                            <span class="error_lname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="body_eng" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.body_message_in_english')</label>
                                            <textarea id="body_eng" name="body_eng" class="form-control required" placeholder="@lang('admin_lang.body_message_in_english')"></textarea>
                                            <!-- <input  type="email"  class="form-control required"> -->
                                            <span class="error_email" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="body_ar" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.body_message_in_arabic')</label>
                                            <textarea id="body_ar" name="body_ar" class="form-control" placeholder="Body message in arabic"></textarea>
                                            <!-- <input id="body_ar" name="body_ar" type="text" class="form-control required" placeholder="Body message in arabic" onkeypress='validate(event)'> -->
                                            <span class="error_phone" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">

                                            {{-- <a href="javascript:void(0)" id="addCustomer" class="btn btn-primary ">@lang('admin_lang.send_push_notification')</a> --}}
                                            <button type="submit" class="btn btn-primary ">@lang('admin_lang.send_push_notification')</button>
                                        </div>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loader" style="display:none;">
        <img src="{{ url('public/frontend/images/Bounce-Bar-Preloader-1.gif')}}" >
    </div>
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ asset('public/frontend/toastr/toastr.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    
    var all_an = $('#an_all').val();
    all_an = jQuery.parseJSON(all_an);

    var all_io = $('#io_all').val();
    all_io = jQuery.parseJSON(all_io);

    var android_count = "{{@$android_count}}";
    var ios_count = "{{@$ios_count}}";

    $("#addCustomerForm").validate({
        rules:{
            'title_eng':{
                required:true
            },
            /*'title_ar':{
                required:true
            },*/
            'body_eng':{
                required:true
            },
            /*'body_ar':{
                required:true
            },*/
            
        },
        messages: { 
            title_eng: { 
                required: "@lang('validation.required')"
            },
            title_ar: { 
                required: "@lang('validation.required')"
            },
            body_eng: { 
                required: "@lang('validation.required')"
            },
            body_ar: { 
                required: "@lang('validation.required')"
            }
        },
        errorPlacement: function (error, element) 
        {
                // console.log(element.attr("name"));
                // console.log(error);
                if (element.attr("name") == "title_eng") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.error_fname').html(error);
                }
                if (element.attr("name") == "title_ar") {
                    var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.error_lname').html(error);
                }
                if (element.attr("name") == "body_eng") {
                    var error = '<label for="gn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.error_email').html(error);
                }
                if (element.attr("name") == "body_ar") {
                    var error = '<label for="phn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.error_phone').html(error);
                }
                
                
            }
        });
    }); 
</script>
<script>
    function hideLoader(i) {
        $('.loader').hide();
        console.log(i)
    }
</script>
@endsection