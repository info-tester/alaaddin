@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage Orders') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.order_report')
@endsection
@section('content')
@section('links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@include('admin.includes.links')
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<!-- ============================================================== -->
			<!-- pageheader  -->
			<!-- ============================================================== -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="page-header">
						<h2 class="pageheader-title">@lang('admin_lang.order_report')</h2>
						<div class="page-breadcrumb">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
									<li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.order_report')</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</div>
			@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			<div class="mail_success" style="display: none;">
				<div class="alert alert-success vd_hidden" style="display: block;">
					<a class="close" data-dismiss="alert" aria-hidden="true">
						<i class="icon-cross"></i>
					</a>
					<span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
					<strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
				</div>
			</div>
			<div class="mail_error" style="display: none;">
				<div class="alert alert-danger vd_hidden" style="display: block;">
					<a class="close" data-dismiss="alert" aria-hidden="true">
						<i class="icon-cross"></i>
					</a>
					<span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
					<strong>Error!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
				</div>
			</div>
			@if (session()->has('success'))
			<div class="alert alert-success vd_hidden" style="display: block;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
				<strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
			</div>
			@elseif ((session()->has('error')))
			<div class="alert alert-danger vd_hidden" style="display: block;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
				<strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
			</div>
			@endif
			{{-- success msg show --}}
			<div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
				<strong class="success_msg">@lang('admin_lang.suc_st_ord_ch')</strong>
			</div>
			{{-- error msg show --}}
			<div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
				<strong class="error_msg">@lang('admin_lang.err_st_ord_ch')</strong> 
			</div>
			<!-- ============================================================== -->
			<!-- end pageheader -->
			<!-- ============================================================== -->
			<div class="row">
				<!-- ============================================================== -->
				<!-- basic table  -->
				<!-- ============================================================== -->
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="card">
						<h5 class="card-header">@lang('admin_lang.order_report')</h5>
						<div class="card-body">
							<form id="search_orders_form" method="get" action="{{route('order.report.export')}}">
								@csrf
								<div class="row">
									<div data-column="0" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="keyword" class="col-form-label">@lang('admin_lang.Keywords')</label>
										<input id="col0_filter" name="keyword" type="text" class="form-control keyword" placeholder='Search by #order'>
									</div>
									<div data-column="7" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="merchant_lb" class="col-form-label">@lang('admin_lang.merchant')</label>
										<select data-placeholder="Choose a Merchant..." class="select slt slct merchant"  tabindex="3" id="col7_filter" name="merchant">
											<option value="">@lang('admin_lang.select_merchant')</option>
											@if(@$merchants)
											@foreach(@$merchants as $merchant)
											<option value="{{ @$merchant->id }}" @if(@$key['merchant_id'] == $merchant->id) selected @endif>{{ @$merchant->fname." ".@$merchant->lname }}</option>
											@endforeach
											@endif
										</select>
									</div>
									{{-- <div data-column="2" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="input-select" class="col-form-label">@lang('admin_lang.Customer')</label>
										<select id="col2_filter" name="customer" data-placeholder='@lang('admin_lang.ch_cust')' class="select slt slct customer"  tabindex="3" id="slct">
										<option value="">@lang('admin_lang.select_customer')</option>
										
										@if(@$external_customer)
										@foreach(@$external_customer as $cust)
										<option value="{{ @$cust->id }}" data-external='y'>{{ @$cust->shipping_fname." ".@$cust->shipping_lname }}</option>
										@endforeach
										@endif
										</select>
									</div> --}}
									<div data-column="4" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="input-select" class="col-form-label">@lang('admin_lang.order_type')</label>
										<select id="col4_filter" name="o_t" class="form-control o_t">
											<option value="">@lang('admin_lang.slt_order_type')</option>
											<option value='I'>@lang('admin_lang.internal_1')</option>
											<option value='E'>@lang('admin_lang.external_1')</option>
										</select>
										{{-- 
										<select data-placeholder="Choose a Driver..." class="select slt slct"  tabindex="3" >
											<option value="">@lang('admin_lang.select_driver')</option>
											@if(@$drivers)
											@foreach(@$drivers as $driver)
											<option value="{{ $driver->id }}">{{ $driver->fname." ".$driver->lname }}</option>
											@endforeach
											@endif
										</select>
										--}}
									</div>

									<div data-column="5" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="input-select" class="col-form-label">@lang('admin_lang.payment_method')</label>
										<select class="form-control select slt slct payment_method" id="col5_filter" name="payment_method">
											<option value="">@lang('admin_lang.select_payment_method')</option>
											<option value='C'>@lang('admin_lang.cod')</option>
											<option value='O'>@lang('admin_lang.online_payment')</option>
										</select>
									</div>
									<div data-column="6" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="input-select" class="col-form-label">@lang('admin_lang.Status')</label>
										<select name="type" class="form-control type status select slt slct" id="col6_filter">
											<option value="">@lang('admin_lang.select_status')</option>
											<option value='N'>@lang('admin_lang.new')</option>
											<option value='OA'>@lang('admin_lang.order_accepted')</option>
											<option value='DA'>@lang('admin_lang.driver_assigned')</option>
											<option value='RP'>@lang('admin_lang.ready_for_pickup')</option>
											<option value='OP'>@lang('admin_lang.ord_picked_up')</option>
											<option value='OD'>@lang('admin_lang.ord_dlv')</option>
											<option value='OC'>@lang('admin_lang.Cancelled_1')</option>
										</select>
									</div>
									<div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="to_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
										<input type="text" class="form-control datepicker from_date" name="from_date" placeholder="@lang('admin_lang_static.from_date')" value="" readonly="" id="col1_filter_from">
									</div>
									<div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="to_date" class="col-form-label">@lang('admin_lang_static.to_date')</label>
										<input type="text" class="form-control datepicker to_date" name="to_date" placeholder="@lang('admin_lang_static.to_date')" value="" readonly="" id="col1_filter_to">
									</div>
									<div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
										<label for="search_orders" class="col-form-label"></label>
										<a href="javascript:void(0)" id="search_orders" class="btn btn-primary ">@lang('admin_lang.Search')</a>
										
										<input type="reset" value="Reset Search" class="btn btn-default reset_search" style="margin-left: 25px;">
										<button type="submit" id="search_orders" class="btn btn-primary ">@lang('admin_lang.export')</button>
									</div>
								</div>
							</form>
							<div class="table-responsive listBody">
								<table class="table table-striped table-bordered first" id="myTable1">
									<thead>
										<tr>
											<th>@lang('admin_lang.order_no')</th>
											<th>@lang('admin_lang.date')</th>
											<th>@lang('admin_lang.customers')</th>
											<th>@lang('admin_lang.order_total')</th>
											{{-- <th>@lang('admin_lang.item')</th> --}}
											<th>@lang('admin_lang.Type')</th>
											<th>@lang('admin_lang.payment_1')</th>
											{{-- 
											<th>@lang('admin_lang.driver')</th>
											--}}
											<th>@lang('admin_lang.Status')</th>
											<!-- <th>@lang('admin_lang.Action')</th> -->
										</tr>
									</thead>
									{{-- 
										<tbody> --}}
										{{-- 
									</tbody>
									--}}
									<tfoot>
										<tr>
											<th>@lang('admin_lang.order_no')</th>
											<th>@lang('admin_lang.date')</th>
											<th>@lang('admin_lang.customers')</th>
											<th>@lang('admin_lang.order_total')</th>
											{{-- <th>@lang('admin_lang.item')</th> --}}
											<th>@lang('admin_lang.Type')</th>
											<th>@lang('admin_lang.payment_1')</th>
											{{-- 
											<th>@lang('admin_lang.driver')</th>
											--}}
											<th>@lang('admin_lang.Status')</th>
											<!-- <th>@lang('admin_lang.Action')</th> -->
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end basic table  -->
				<!-- ============================================================== -->
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- footer -->
		<!-- ============================================================== -->
		@include('admin.includes.footer')
		<!-- ============================================================== -->
		<!-- end footer -->
		<!-- ============================================================== -->
		<div class="loader" style="display: none;">
			<img src="{{url('public/loader.gif')}}">
		</div>
	</div>
</div>
{{-- 
<div class="modal fade" id="assign_driver_popup_show" data-id="" data-emailid="">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">@lang('admin_lang.assign_driver')</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="basic-form">
					<form id="driver_assign_form" method="post" action="{{ route('admin.driver.assign') }}">
						@csrf
						<div class="form-row">
							<input id="order_id" name="order_id" type="hidden" class="form-control" placeholder="" readonly>
							<div class="form-group col-md-12">
								<label>
								@lang('admin_lang.assign_driver')
								</label>
								<select id="assign" name="assign" class="form-control">
									<option value="">@lang('admin_lang.select_driver')</option>
									@foreach(@$drivers as $driver)
									<option value="{{ @$driver->id }}">{{ @$driver->fname }} {{ @$driver->lname }}</option>
									@endforeach
								</select>
								<span class="error_assign_driver" style="color: red;"></span>
							</div>
							<div class="form-group col-md-12">
								<label></label>
								<button type="button" id="assignDriver" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
							</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
--}}
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
{{-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script> --}}
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script> --}}
<script type="text/javascript">

      $( function() {
        $(".datepicker").datepicker({dateFormat: "yy-mm-dd",
         defaultDate: new Date(),
         maxDate: new Date(),
         changeMonth: true,
         changeYear: true,
         yearRange: '-100:+0'
     });         	
    })
	$(document).ready(function () {

	$('body').on('click', '.reset_search', function() {
		$('#myTable1').DataTable().search('').columns().search('').draw();
		$('#col7_filter').val('');
		$('#col5_filter').val('');
		$('#col6_filter').val('');
	});
	// $("#myTable1_filter").style("display","none");
	function filterColumn ( i ) {
        // console.log(i, $('#col'+i+'_filter').val())
        $('#myTable1').DataTable().column( i ).search(
        	$('#col'+i+'_filter').val(),
        	).draw();
    }

	// function getAction(data, id, full, meta) {
 //       	var a = '';
 //       	a += '<a href="{{ url('admin/view-order-details').'/' }}'+full.id+'"><i class="fas fa-eye" title="View"></i></a>'
	//     return a;
	// }

	function getStatus(data, type, full, meta) {
		if(data == 'I'){
			// return '@lang('admin_lang.initiated_1')'
			return "<span class='status_"+full.id+"'>"+"Incomplete"+"</span>"
       	}
		else if(data == 'N'){
			// return '@lang('admin_lang.new')'
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.new')"+"</span>"
		}
		else if(data == 'OA'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"Order accepted"+"</span>"
		}
		else if(data == 'DA'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"Driver assigned"+"</span>"
		}
		else if(data == 'RP'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"Ready for picked up"+"</span>"
		}
		else if(data == 'OP'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"Order Picked up"+"</span>"
		}
		else if(data == 'OD'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"Order Delivered"+"</span>"
		}else if(data == 'OC'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"Order Cancelled"+"</span>"
		}
		// else{
		// 	// return '@lang('admin_lang.Cancelled_1')'
		// 	return "<span class='status_"+full.id+"'>"+""+"</span>"
		// }
	}
	function getPaymentMethod(data, type, full, meta){
		// alert(data);
		if(data == 'C'){
			return '@lang('admin_lang.cod')'
		}
		else if(data == 'O'){
			return '@lang('admin_lang.online')'
		}
	}
	function getOrderType(data, type, full, meta){
		if(data == 'I'){
			return "@lang('admin_lang.internal_1')"
		}
		else if(data == 'E'){
			return '@lang('admin_lang.external_1')'
		}
	}
	function getCustomerName(data, type, full, meta){
		return data.shipping_fname+' '+data.shipping_lname ;
	}
	var styles = `
            table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
                right: 0em;
                content: "";
            }
            table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
                right: 0em;
                content: "";
            }
        `
        var styleSheet = document.createElement("style");
        styleSheet.innerText = styles;
        document.head.appendChild(styleSheet);
	$('#myTable1').DataTable({
		stateSave: true,
		"order": [
		[1, "desc"]
		],
		"stateLoadParams": function (settings, data) {
            // console.log('stateLoadParams', data)
            $('#col0_filter').val(data.search.order_no)
            $('#col4_filter').val(data.search.order_type)
            $('#col5_filter').val(data.search.payment_method)
            // $('#col2_filter').val(data.search.subcategory)
            $('#col6_filter').val(data.search.status)
            $('#col7_filter').val(data.search.merchant)
            $('#col1_filter_from').val(data.search.from_date)
            $('#col1_filter_to').val(data.search.to_date)
            // $('#amount').val(data.search.price)
        },
        stateSaveParams: function (settings, data) {
            // console.log('stateSaveParams', data)
            data.search.order_no = $('#col0_filter').val();
            data.search.order_type = $('#col4_filter').val();
            data.search.payment_method = $('#col5_filter').val();
            data.search.status = $('#col6_filter').val();
            data.search.merchant = $('#col7_filter').val();
            data.search.from_date = $('#col1_filter_from').val();
            data.search.to_date = $('#col1_filter_to').val();
        },
        "processing": true,
        "serverSide": true,
        'serverMethod': 'post',
        "ajax": {
        	"url": "{{ route('admin.list.orders.datatable') }}",
        	"data": function (d) {
				// console.log(d);
				d.myKey = "myValue";
				d._token = "{{ @csrf_token() }}";
			}
		},
		
		'columns': [
		{
			data: 'order_no',
		},
		{
			data: 'created_at'
		},
		{
			data: 'shipping_fname',
			render:function(data, type, full, meta){
				console.log(full.shipping_fname);
					// return full.shipping_fname+" "+full.shipping_lname;
					// $(row).addClass('tr_');
					if(full.order_type == 'I'){
						var fname="--",lname="--";
						if(full.shipping_fname == null || full.shipping_fname == ""){
							fname = "--";
							return fname+" "+lname;
						}
						else if(full.shipping_lname == null || full.shipping_lname == "")
						{
							lname = "--";
							return fname+" "+lname;
						}else{
							return full.shipping_fname+" "+full.shipping_lname;
						}
					}else{
						var fname="--",lname="--";
						if(full.fname == null || full.fname == ""){
							fname = "--";
							return fname+" "+lname;
						}
						else if(full.lname == null || full.lname == "")
						{
							lname = "--";
							return fname+" "+lname;
						}else{
							return full.fname+" "+full.lname;
						}
					}
					


				}

			},
			{
				data: "order_total"
			},
			{
				data: 'order_type',
				render: getOrderType
			},
			{
				data: 'payment_method',
				render:getPaymentMethod
			},
			{
				data: 'status',
				render:getStatus
			},
			// {
			// 	data: 'status',
			// 	render:function(data, id, full, meta) {
			//        	var a = '';
			//        	a += '<a href="{{ url('admin/view-order-details').'/' }}'+full.id+'"><i class="fas fa-eye" title="@lang('admin_lang.')"></i></a>'
				    
			// 	    if(full.order_type == 'E'){
			// 	    	a += ' <a href="{{ url('admin/edit-external-order-details').'/' }}'+full.id+'"><i class="fas fa-edit" title="@lang('admin_lang.Edit')"></i></a>'
			// 	    }
			// 	    if(full.status != 'OC' || full.status != 'OD'){
			// 	    	a += ' <a href="javascript:void(0)" class="order_mstr_cancel" data-id='+full.id+' data-ordno='+full.order_no+'><i class="far fa-times-circle cn_or_'+full.id+'" title="@lang('admin_lang.cancel')"></i></a>'
			// 	    }
			// 	    if(full.status != 'OC'){
			// 		    if(full.status == 'N'){
			// 		    	a += '&nbsp;  <a href="javascript:void(0)" class="order_mstr_status_change" data-id='+full.id+' data-ordno='+full.order_no+' data-changeStatus="Order Accepted"><i class="fas fa-check ord_'+full.id+'" title="Order Accepted"></i></a>  '
			// 		    }
			// 		    // else if(full.status == 'I'){
			// 		    // 	a += '  <a href="javascript:void(0)" class="order_mstr_status_change" data-id='+full.id+' data-ordno='+full.order_no+' data-changeStatus="Paid"><i class="fas fa-check ord_'+full.id+'" title="@lang('admin_lang.ord_paid')"></i></a>  '
			// 		    // }
			// 		}
			// 		// if(full.status != 'C'){
			// 		// 	if(full.status == 'N'){
			// 		// 		a += '  <a href="javascript:void(0)" class="order_mstr_status_change" data-id='+full.id+' data-ordno='+full.order_no+' data-changeStatus="Initiated"><i class="fas fa-check ord_'+full.id+'" title="@lang('admin_lang.ord_initiated')"></i></a>  '
			// 		// 	}else if(full.status == 'I'){
			// 		// 		a += '  <a href="javascript:void(0)" class="order_mstr_status_change" data-id='+full.id+' data-ordno='+full.order_no+' data-changeStatus="Paid"><i class="fas fa-check ord_'+full.id+'" title="@lang('admin_lang.ord_paid')"></i></a>  '
			// 		// 	}
			// 		// }
			// 		return a;
			// 	}
			// },
			]
		});
	$('input.keyword').on( 'keyup click', function () {
		filterColumn( $(this).parents('div').data('column') );
	});

	$('.o_t').on( 'change', function () {
		filterColumn( $(this).parents('div').data('column') );
	});

	$('.payment_method').on( 'change', function () {
		filterColumn( $(this).parents('div').data('column') );
	});

	$('.type').on('change', function () {
		filterColumn( $(this).parents('div').data('column'));
	});
	$('.merchant').on('change', function () {
		filterColumn( $(this).parents('div').data('column'));
	});
	$('.from_date').on('change', function () {		
        if($('#col1_filter_to').val() == '' && $('#col1_filter_to').val() < $('#col1_filter_from').val()) {
            return false;
        } else {
			$('#myTable1').DataTable().column( 1 ).search(
				[$('#col1_filter_from').val(),$('#col1_filter_to').val()],
				).draw();
		}
	});
	$('.to_date').on('change', function () {		
        if($('#col1_filter_from').val() == '' && $('#col1_filter_from').val() > $('#col1_filter_to').val()) {
            return false;
        } else {
			$('#myTable1').DataTable().column( 1 ).search(
				[$('#col1_filter_from').val(),$('#col1_filter_to').val()],
				).draw();
		}
	});

	@if(@$key['merchant_id'])
	$('#myTable1').DataTable().column( 7 ).search(
		{{ $key['merchant_id'] }},
		).draw();
	$('#col7_filter').val({{ $key['merchant_id'] }});
	@endif 

});
</script>
{{-- jayatri ajax action buttons work --}}
<script type="text/javascript">
	$(document).ready(function(){
		$('.slct').chosen();
		// $(document).on("change","#type",function(e){
		// 	var keyword = $.trim($("#keyword").val()),
		// 	merchant = $.trim($("#merchant").val()),
		// 	customer = $.trim($("#customer").val()),
		// 	type = $.trim($("#type").val()),
		// 	driver = $.trim($("#driver").val()),
		// 	external = $.trim($("#customer").children("option:selected").attr("data-external")),
		// 	o_t = $.trim($("#o_t").val()),
		// 	payment_method = $.trim($("#payment_method").val());
		// 	// alert(external);
		// 	// $.fn.dataTable.ext.errMode = 'none';
		// 	// $('#myTable').dataTable({
		// 	//     "bServerSide": true,
		// 	//     ....
		// 	//     "bDestroy": true
		// 	// });
		// 	$.ajax({
		// 		type:"POST",
		// 		url:"{{ route('admin.search.order') }}",
		// 		data:{
		// 			_token: '{{ csrf_token() }}',
		// 			keyword:keyword,
		// 			merchant:merchant,
		// 			customer:customer,
		// 			type:type,
		// 			driver:driver,
		// 			o_t:o_t,
		// 			payment_method:payment_method
		// 		},
		// 		success:function(resp){
		// 			// if(resp != 0){
		// 				$(".listBody").html(resp);
		// 			// }
		// 		}
		// 	});
		// });
		// $(document).on("change","#customer",function(e){
		// 	var keyword = $.trim($("#keyword").val()),
		// 	merchant = $.trim($("#merchant").val()),
		// 	customer = $.trim($("#customer").val()),
		// 	type = $.trim($("#type").val()),
		// 	driver = $.trim($("#driver").val()),
		// 	external = $.trim($("#customer").children("option:selected").attr("data-external")),
		// 	o_t = $.trim($("#o_t").val()),
		// 	payment_method = $.trim($("#payment_method").children("option:selected").val());
		// 	// alert(payment_method);
		// 	// $.fn.dataTable.ext.errMode = 'none';
		// 	// $('#myTable').dataTable({
		// 	//     "bServerSide": true,
		// 	//     ....
		// 	//     "bDestroy": true
		// 	// });
		// 	$.ajax({
		// 		type:"POST",
		// 		url:"{{ route('admin.search.order') }}",
		// 		data:{
		// 			_token: '{{ csrf_token() }}',
		// 			keyword:keyword,
		// 			merchant:merchant,
		// 			customer:customer,
		// 			external:external,
		// 			type:type,
		// 			driver:driver,
		// 			o_t:o_t,
		// 			payment_method:payment_method
		// 		},
		// 		success:function(resp){
		// 			// if(resp != 0){
		// 				$(".listBody").html(resp);
		// 			// }
		// 		}
		// 	});
		// });
		// $(document).on("change","#payment_method",function(e){
		// 	var keyword = $.trim($("#keyword").val()),
		// 	merchant = $.trim($("#merchant").val()),
		// 	customer = $.trim($("#customer").val()),
		// 	type = $.trim($("#type").val()),
		// 	driver = $.trim($("#driver").val()),
		// 	external = $.trim($("#customer").children("option:selected").attr("data-external")),
		// 	payment_method = $.trim($("#payment_method").val()),
		// 	o_t = $.trim($("#o_t").val());
		// 	// alert(payment_method);
		// 	// $.fn.dataTable.ext.errMode = 'none';
		// 	// $('#myTable').dataTable({
		// 	//     "bServerSide": true,
		// 	//     ....
		// 	//     "bDestroy": true
		// 	// });
		// 	$.ajax({
		// 		type:"POST",
		// 		url:"{{ route('admin.search.order') }}",
		// 		data:{
		// 			_token: '{{ csrf_token() }}',
		// 			keyword:keyword,
		// 			merchant:merchant,
		// 			customer:customer,
		// 			external:external,
		// 			type:type,
		// 			driver:driver,
		// 			o_t:o_t,
		// 			payment_method:payment_method
		// 		},
		// 		success:function(resp){
		// 			// if(resp != 0){
		// 				$(".listBody").html(resp);
		// 			// }
		// 		}
		// 	});
		// });
		// $(document).on("blur","#keyword",function(e){
		// 	var keyword = $.trim($("#keyword").val()),
		// 	merchant = $.trim($("#merchant").val()),
		// 	customer = $.trim($("#customer").val()),
		// 	type = $.trim($("#type").val()),
		// 	driver = $.trim($("#driver").val()),
		// 	external = $.trim($("#customer").children("option:selected").attr("data-external")),
		// 	payment_method = $.trim($("#payment_method").val()),
		// 	o_t = $.trim($("#o_t").val());
		// 	// alert(external);
		// 	// $.fn.dataTable.ext.errMode = 'none';
		// 	// $('#myTable').dataTable({
		// 	//     "bServerSide": true,
		// 	//     ....
		// 	//     "bDestroy": true
		// 	// });
		// 	$.ajax({
		// 		type:"POST",
		// 		url:"{{ route('admin.search.order') }}",
		// 		data:{
		// 			_token: '{{ csrf_token() }}',
		// 			keyword:keyword,
		// 			merchant:merchant,
		// 			customer:customer,
		// 			type:type,
		// 			driver:driver,
		// 			o_t:o_t,
		// 			payment_method:payment_method
		// 		},
		// 		success:function(resp){
		// 			// if(resp != 0){
		// 				$(".listBody").html(resp);
		// 			// }
		// 		}
		// 	});
		// });
		// $("#search_orders").click(function(){
		// 	var keyword = $.trim($("#keyword").val()),
		// 	merchant = $.trim($("#merchant").val()),
		// 	customer = $.trim($("#customer").val()),
		// 	type = $.trim($("#type").val()),
		// 	driver = $.trim($("#driver").val()),
		// 	external = $.trim($("#customer").children("option:selected").attr("data-external")),
		// 	payment_method = $.trim($("#payment_method").val()),
		// 	o_t = $.trim($("#o_t").val());
		// 	// $.fn.dataTable.ext.errMode = 'none';
		// 	// $('#myTable').dataTable({
		// 	//     "bServerSide": true,
		// 	//     ....
		// 	//     "bDestroy": true
		// 	// });
		// 	$.ajax({
		// 		type:"POST",
		// 		url:"{{ route('admin.search.order') }}",
		// 		data:{
		// 			_token: '{{ csrf_token() }}',
		// 			keyword:keyword,
		// 			merchant:merchant,
		// 			customer:customer,
		// 			type:type,
		// 			driver:driver,
		// 			o_t:o_t,
		// 			payment_method:payment_method
		// 		},
		// 		success:function(resp){
		// 			// if(resp != 0){
		// 				$(".listBody").html(resp);
		// 			// }
		// 		}
		// 	});
		// });
		// $("#o_t").change(function(){
		// 	var keyword = $.trim($("#keyword").val()),
		// 	merchant = $.trim($("#merchant").val()),
		// 	customer = $.trim($("#customer").val()),
		// 	type = $.trim($("#type").val()),
		// 	driver = $.trim($("#driver").val()),
		// 	external = $.trim($("#customer").children("option:selected").attr("data-external")),
		// 	payment_method = $.trim($("#payment_method").val()),
		// 	o_t = $.trim($("#o_t").val());
		// 	// $.fn.dataTable.ext.errMode = 'none';
		// 	// $('#myTable').dataTable({
		// 	//     "bServerSide": true,
		// 	//     ....
		// 	//     "bDestroy": true
		// 	// });
		// 	$.ajax({
		// 		type:"POST",
		// 		url:"{{ route('admin.search.order') }}",
		// 		data:{
		// 			_token: '{{ csrf_token() }}',
		// 			keyword:keyword,
		// 			merchant:merchant,
		// 			customer:customer,
		// 			type:type,
		// 			driver:driver,
		// 			o_t:o_t,
		// 			payment_method:payment_method
		// 		},
		// 		success:function(resp){
		// 			// if(resp != 0){
		// 				$(".listBody").html(resp);
		// 			// }
		// 		}
		// 	});
		// });
		$(document).on("click",".order_mstr_status_change",function(e){
			var id = $(e.currentTarget).attr("data-id"),
			ch_status = $(e.currentTarget).attr("data-changeStatus");
			if(confirm('@lang('admin_lang.do_u_wnt_chng')'+" "+ch_status+' ? ')){
				$.ajax({
					url:"{{ route('admin.update.status.order.master') }}",
					type:"GET",
					data:{
						id:id
					},
					success:function(resp){
						if(resp != 0){
							var status = JSON.stringify(resp.status),
							st = "";
							if(resp.status == 'OA'){
								st = "Order Accepted";
								$(".ord_"+id).hide();
							}
							// if(resp.status == 'P'){
							// 	st = "Paid";
								
							// }
							$(".status_"+id).html(st);
							$(".success_msg_div").show();
							$(".error_msg_div").hide();
							$(".success_msg").html('@lang('admin_lang.success_st_ch_ord')');

							$(".status_"+id).html(st);
						}else{
							$(".error_msg_div").show();
							$(".success_msg_div").hide();
							$(".error_msg").html('@lang('admin_lang.err_st_or_ch')');
						}
					}
				});
			}
			// }
		});
		$(document).on("click",".order_mstr_cancel",function(e){
			var id = $(e.currentTarget).attr("data-id"),
			ord_no = $(e.currentTarget).attr("data-ordno");
			if(confirm('Do you want to cancel this order having order number '+ ord_no+'?')){
				$.ajax({
					url:"{{ route('admin.order.cancel') }}",
					type:"GET",
					data:{
						id:id
					},
					success:function(resp){
						if(resp == 1){
							$(".status_"+id).html("Cancelled");
							$(".success_msg_div").show();
							$(".error_msg_div").hide();
							$(".success_msg").html('Success!Order has been cancelled successfully!');
							// $(".status_"+id).text("Cancelled");
							$(".cn_or_"+id).hide();
							$(".ord_"+id).hide();
						}else{
							$(".error_msg_div").show();
							$(".success_msg_div").hide();
							$(".error_msg").html('@lang('admin_lang.err_nw_q')');

						}
					}
				});
			}
			// }
		});
		// $('#myTable1').dataTable({
		//   order: [[1, 'desc']]
	 //  	});
	});
</script>
@endsection