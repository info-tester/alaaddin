@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.product_report')
@endsection
@section('content')
@section('links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@include('admin.includes.links')
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">@lang('admin_lang_static.product_report')</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">@lang('admin_lang_static.dashboard')</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('add.product.step.one') }}" class="breadcrumb-link">@lang('admin_lang_static.add_product') </a></li>
                                <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang_static.product_report')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        @if (session()->has('success'))
        <div class="alert alert-success vd_hidden session_success_msg" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
        </div>
        @elseif ((session()->has('error')))
        <div class="alert alert-danger vd_hidden session_error_msg" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
        </div>
        @endif
        <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong class="success_msg"></strong> 
        </div>
        <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong id="error_msg"></strong> 
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">@lang('admin_lang_static.product_report')</h5>
                    <div class="card-body">
                        <form action="{{ route('product.analysis.report.export') }}" method="get">
                            <div class="row">
                                <div data-column="0" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="inputText3" class="col-form-label">@lang('admin_lang_static.keyword')</label>
                                    <input type="text" class="form-control keyword" placeholder="@lang('admin_lang.search_by_title_order')" id="col0_filter" name="keyword" value="{{ @$key['keyword'] }}">
                                </div>
                                
                                <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="to_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
                                    <input type="text" class="form-control datepicker from_date" name="from_date" placeholder="@lang('admin_lang_static.from_date')" value="" readonly="" id="col1_filter_from">
                                </div>
                                <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="to_date" class="col-form-label">@lang('admin_lang_static.to_date')</label>
                                    <input type="text" class="form-control datepicker to_date" name="to_date" placeholder="@lang('admin_lang_static.to_date')" value="" readonly="" id="col1_filter_to">
                                </div>

                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-column="2">
                                    <label for="input-select" class="col-form-label">@lang('admin_lang_static.merchant')</label>
                                    <select data-placeholder="Choose a Merchant..." class="select slt slct user_id" name="user_id" tabindex="3" id="col2_filter">
                                        <option value="">@lang('admin_lang_static.select_merchant')</option>
                                        @foreach($merchants as $us)
                                        <option value="{{ $us->id }}">{{ $us->fname }} {{ $us->lname }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="form-group col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                    <a href="javascript:void(0)" class="btn btn-primary fstbtncls">@lang('admin_lang_static.search')</a>
                                    <input type="reset" value="@lang('admin_lang.reset_search')" class="btn btn-default fstbtncls reset_search">
                                    <button type="submit" id="search_orders" class="btn btn-primary fstbtncls">@lang('admin_lang.export')</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive tpmrgn listBody">
                            <table class="table table-striped table-bordered" id="example">
                                <thead>
                                    <tr>                                        
                                        <th>@lang('admin_lang.order_no')</th>
                                        <th>@lang('admin_lang.date')</th> 
                                        <th>@lang('admin_lang_static.merchant')</th>
                                        <th>@lang('admin_lang_static.product_code')</th>
                                        <th>@lang('admin_lang.product_title')</th>
                                        <th>@lang('admin_lang_static.price')</th>
                                        <th>@lang('admin_lang.qty')</th>
                                    </tr>
                                </thead>                                
                                <tfoot>
                                    <tr>                                        
                                        <th>@lang('admin_lang.order_no')</th>
                                        <th>@lang('admin_lang.date')</th> 
                                        <th>@lang('admin_lang_static.merchant')</th>
                                        <th>@lang('admin_lang_static.product_code')</th>
                                        <th>@lang('admin_lang.product_title')</th>
                                        <th>@lang('admin_lang_static.price')</th>
                                        <th>@lang('admin_lang.qty')</th>                                        
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end basic table  -->
            <!-- ============================================================== -->
        </div>
    </div>
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>
<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>
@endsection
@section('scripts')

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script>

  $( function() {
    $(".datepicker").datepicker({dateFormat: "yy-mm-dd",
     defaultDate: new Date(),
     maxDate: new Date(),
     changeMonth: true,
     changeYear: true,
     yearRange: '-100:+0'
 });            
})
  function filterColumn ( i ) {
    $('#example').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
        ).draw();
}
function filterColumnPrice ( i ) {
    $('#example').DataTable().search(
        $('#amount').val(),
        ).draw();
}


$(document).ready(function() {
    var styles = `
            table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
                right: 0em;
                content: "";
            }
            table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
                right: 0em;
                content: "";
            }
        `
        var styleSheet = document.createElement("style");
        styleSheet.innerText = styles;
        document.head.appendChild(styleSheet);
    $('#example').DataTable( {
        stateSave: true,
        "order": [[1, "desc"]],
        "stateLoadParams": function (settings, data) {
            // console.log(data);
            $('#col0_filter').val(data.search.keyword)
            $('#col1_filter_from').val(data.search.from_date)
            $('#col1_filter_to').val(data.search.to_date)
            
        },
        stateSaveParams: function (settings, data) {
            data.search.keyword = $('#col0_filter').val();
            data.search.from_date = $('#col1_filter_from').val()
            data.search.to_date = $('#col1_filter_to').val();
        },
        "processing": true,
        "serverSide": true,
        'serverMethod': 'post',
        "ajax": {
            "url": "{{ route('admin.product.analysis.report') }}",
            "data": function ( d ) {
                d._token = "{{ @csrf_token() }}";
            }
        },
        'columns': [
            { 
                render: function(data, type, full, meta){
                    return full.order_master.order_no;
                } 
            },
            { 
                render: function(data, type, full, meta){
                    return full.created_at;
                } 
            },
            { 
                render: function(data, type, full, meta){
                    return full.seller_details.fname+' '+full.seller_details.lname;
                } 
            },
            { data: 'product_details.product_code' },
            { data: 'product_details.product_by_language.title' },
            {
                data:'original_price'
            },
            { data: 'quantity' }
        ]
    });

    $('input.keyword').on( 'keyup click', function () {
        filterColumn( $(this).parents('div').data('column') );
    } );

    $('#col2_filter').on( 'change', function () {
        filterColumn( $(this).parents('div').data('column') );
    } );

    $('.from_date').on('change', function () {      
        if($('#col1_filter_to').val() == '' && $('#col1_filter_to').val() < $('#col1_filter_from').val()) {
            return false;
        } else {
            $('#example').DataTable().column( 1 ).search(
                [$('#col1_filter_from').val(),$('#col1_filter_to').val()],
                ).draw();
        }
    });
    $('.to_date').on('change', function () {        
        if($('#col1_filter_from').val() == '' && $('#col1_filter_from').val() > $('#col1_filter_to').val()) {
            return false;
        } else {
            $('#example').DataTable().column( 1 ).search(
                [$('#col1_filter_from').val(),$('#col1_filter_to').val()],
                ).draw();
        }
    });

    $('body').on('click', '.reset_search', function() {
        $('#example').DataTable().search('').columns().search('').draw();
    })
});
</script>
<script>
    $(document).ready(function() {
        $('.slct').chosen();
    })
</script>
<script>
    $(document).ready(function(){
        $('#col1_filter').change(function(){
            if($(this).val() != '') {
                var value  = $(this).val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('sub.cat.fetch') }}",
                    method:"POST",
                    data:{value:value, _token:_token},
                    success:function(response) {
                        $('#col2_filter').html(response.result.output);
                    }
                })
            }
        });

        $('#input-select').change(function(){
            $('#col2_filter').val('');
        });

    });
</script>
@endsection