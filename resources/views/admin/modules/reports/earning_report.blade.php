@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage Orders') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.earning_report')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
{{-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> --}}
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<style type="text/css">
	#myTable1_filter{
		display: none;
	}
	table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
		right: 0em;
		content: "";
	}
	table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
		right: 0em;
		content: "";
	}
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<!-- ============================================================== -->
			<!-- pageheader  -->
			<!-- ============================================================== -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="page-header">
						<h2 class="pageheader-title">@lang('admin_lang.earning_report')</h2>
						<div class="page-breadcrumb">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
									<li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.earning_report')</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</div>
			@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			<div class="mail_success" style="display: none;">
				<div class="alert alert-success vd_hidden" style="display: block;">
					<a class="close" data-dismiss="alert" aria-hidden="true">
						<i class="icon-cross"></i>
					</a>
					<span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
					<strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
				</div>
			</div>
			<div class="mail_error" style="display: none;">
				<div class="alert alert-danger vd_hidden" style="display: block;">
					<a class="close" data-dismiss="alert" aria-hidden="true">
						<i class="icon-cross"></i>
					</a>
					<span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
					<strong>Error!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
				</div>
			</div>
			@if (session()->has('success'))
			<div class="alert alert-success vd_hidden" style="display: block;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
				<strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
			</div>
			@elseif ((session()->has('error')))
			<div class="alert alert-danger vd_hidden" style="display: block;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
				<strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
			</div>
			@endif
			{{-- success msg show --}}
			<div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
				<strong class="success_msg">@lang('admin_lang.suc_st_ord_ch')</strong>
			</div>
			{{-- error msg show --}}
			<div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
				<strong class="error_msg">@lang('admin_lang.err_st_ord_ch')</strong> 
			</div>
			<!-- ============================================================== -->
			<!-- end pageheader -->
			<!-- ============================================================== -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="card">
						<h5 class="card-header">@lang('admin_lang.earning_report')</h5>
						<div class="card-body">
							<form id="search_orders_form" method="get" action="{{route('earning.report.export')}}">
								@csrf
								<div class="row">
									<div data-column="0" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="keyword" class="col-form-label">@lang('admin_lang.Keywords')</label>
										<input id="col0_filter" name="keyword" type="text" class="form-control keyword" placeholder='Search by #order'>
									</div>
									<div data-column="7" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="merchant_lb" class="col-form-label">@lang('admin_lang.merchant')</label>
										<select data-placeholder="Choose a Merchant..." class="select slt slct merchant"  tabindex="3" id="col7_filter" name="merchant">
											<option value="">@lang('admin_lang.select_merchant')</option>
											@if(@$merchants)
											@foreach(@$merchants as $merchant)
											<option value="{{ @$merchant->id }}" @if(@$key['merchant_id'] == $merchant->id) selected @endif>{{ @$merchant->fname." ".@$merchant->lname }}</option>
											@endforeach
											@endif
										</select>
									</div>
									<div data-column="4" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="input-select" class="col-form-label">@lang('admin_lang.order_type')</label>
										<select id="col4_filter" name="o_t" class="form-control o_t">
											<option value="">@lang('admin_lang.slt_order_type')</option>
											<option value='I'>@lang('admin_lang.internal_1')</option>
											<option value='E'>@lang('admin_lang.external_1')</option>
										</select>
									</div>

									<div data-column="5" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="input-select" class="col-form-label">@lang('admin_lang.payment_method')</label>
										<select class="form-control select slt slct payment_method" id="col5_filter" name="payment_method">
											<option value="">@lang('admin_lang.select_payment_method')</option>
											<option value='C'>@lang('admin_lang.cod')</option>
											<option value='O'>@lang('admin_lang.online_payment')</option>
										</select>
									</div>
									<div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="to_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
										<input type="text" class="form-control datepicker from_date" name="from_date" placeholder="@lang('admin_lang_static.from_date')" value="" readonly="" id="col1_filter_from">
									</div>
									<div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="to_date" class="col-form-label">@lang('admin_lang_static.to_date')</label>
										<input type="text" class="form-control datepicker to_date" name="to_date" placeholder="@lang('admin_lang_static.to_date')" value="" readonly="" id="col1_filter_to">
									</div>
									<div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
										<label for="search_orders" class="col-form-label">&nbsp;</label>
										<a href="javascript:void(0)" id="search_orders" class="btn btn-primary ">@lang('admin_lang.Search')</a>
										<input type="reset" value="Reset Search" class="btn btn-default reset_search">
										<button type="submit" id="search_orders" class="btn btn-primary ">@lang('admin_lang.export')</button>
									</div>
								</div>
							</form>

							<div class="table-responsive listBody">
								<table class="table table-striped table-bordered first" id="myTable1">
									<thead>
										<tr>
											<th>@lang('admin_lang.order_no')</th>
											<th>@lang('admin_lang.date')</th>
											<th>@lang('admin_lang.customers')</th>
											<th>@lang('admin_lang.Type')</th>
											<th>@lang('admin_lang.payment_1')</th>
											<th>@lang('admin_lang.Status')</th>
											<th>@lang('admin_lang.commision_amount') (@lang('admin_lang.in_kwd'))</th>
											<th>@lang('admin_lang.shipping_cost') (@lang('admin_lang.in_kwd'))</th>
											<th>@lang('admin_lang.order_total') (@lang('admin_lang.in_kwd'))</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th>@lang('admin_lang.order_no')</th>
											<th>@lang('admin_lang.date')</th>
											<th>@lang('admin_lang.customers')</th>
											<th>@lang('admin_lang.Type')</th>
											<th>@lang('admin_lang.payment_1')</th>
											<th>@lang('admin_lang.Status')</th>
											<th>@lang('admin_lang.commision_amount') (@lang('admin_lang.in_kwd'))</th>
											<th>@lang('admin_lang.shipping_cost') (@lang('admin_lang.in_kwd'))</th>
											<th>@lang('admin_lang.order_total') (@lang('admin_lang.in_kwd'))</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('admin.includes.footer')
		<div class="loader" style="display: none;">
			<img src="{{url('public/loader.gif')}}">
		</div>
	</div>
</div>
	
@endsection
@section('scripts')
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">

	$( function() {
		$(".datepicker").datepicker({dateFormat: "yy-mm-dd",
			defaultDate: new Date(),
			maxDate: new Date(),
			changeMonth: true,
			changeYear: true,
			yearRange: '-100:+0'
		});         	
	})

	$(document).ready(function () {
		$('body').on('click', '.reset_search', function() {
			$('#myTable1').DataTable().search('').columns().search('').draw();
		});

		function filterColumn ( i ) {
	        $('#myTable1').DataTable().column( i ).search(
	        	$('#col'+i+'_filter').val(),
	        ).draw();
	    }

		function getStatus(data, type, full, meta) {
			if(data == 'I'){
				return "<span class='status_"+full.id+"'>"+"Incomplete"+"</span>"
			}
			else if(data == 'N'){
				return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.new')"+"</span>"
			}
			else if(data == 'OA'){
				return "<span class='status_"+full.id+"'>"+"Order accepted"+"</span>"
			}
			else if(data == 'DA'){
				return "<span class='status_"+full.id+"'>"+"Driver assigned"+"</span>"
			}
			else if(data == 'RP'){
				return "<span class='status_"+full.id+"'>"+"Ready for picked up"+"</span>"
			}
			else if(data == 'OP'){
				return "<span class='status_"+full.id+"'>"+"Order Picked up"+"</span>"
			}
			else if(data == 'OD'){
				return "<span class='status_"+full.id+"'>"+"Order Delivered"+"</span>"
			}else if(data == 'OC'){
				return "<span class='status_"+full.id+"'>"+"Order Cancelled"+"</span>"
			}
		}

		function getPaymentMethod(data, type, full, meta){
			if(data == 'C'){
				return '@lang('admin_lang.cod')'
			}
			else if(data == 'O'){
				return '@lang('admin_lang.online')'
			} else {
				return ''
			}
		}

		function getOrderType(data, type, full, meta){
			if(data == 'I'){
				return "@lang('admin_lang.internal_1')"
			}
			else if(data == 'E'){
				return '@lang('admin_lang.external_1')'
			}
		}

		function getCustomerName(data, type, full, meta){
			return data.shipping_fname+' '+data.shipping_lname ;
		}

		function getCommission(data, type, full, meta) {
			// if(full.order_seller_info) {
				return full.order_seller_info.total_commission
			// } else {
			// 	return full.total_commission
			// }
		}
		function getOrderTotal(data, type, full, meta) {
			// if(full.order_seller_info) {
			// 	return (full.order_seller_info.earnings).toFixed(2)
			// } else {
				return full.order_total
			// }
		}

		$('#myTable1').DataTable({
			stateSave: true,
			"order": [
				[1, "desc"]
			],
			"stateLoadParams": function (settings, data) {
	            $('#col0_filter').val(data.search.order_no)
	            $('#col4_filter').val(data.search.order_type)
	            $('#col5_filter').val(data.search.payment_method)
	            $('#col6_filter').val(data.search.status)
	            $('#col7_filter').val(data.search.merchant)
	            $('#col1_filter_from').val(data.search.from_date)
	            $('#col1_filter_to').val(data.search.to_date)
	        },
	        stateSaveParams: function (settings, data) {
	            data.search.order_no = $('#col0_filter').val();
	            data.search.order_type = $('#col4_filter').val();
	            data.search.payment_method = $('#col5_filter').val();
	            data.search.status = $('#col6_filter').val();
	            data.search.merchant = $('#col7_filter').val();
	            data.search.from_date = $('#col1_filter_from').val();
	            data.search.to_date = $('#col1_filter_to').val();
	        },
	        "processing": true,
	        "serverSide": true,
	        'serverMethod': 'post',
	        "ajax": {
	        	"url": "{{ route('admin.earning.orders.datatable') }}",
	        	"data": function (d) {
					d.myKey = "myValue";
					d._token = "{{ @csrf_token() }}";
				}
			},
			'columns': [
				{
					data: 'order_no',
				},
				{
					data: 'created_at'
				},
				{
					data: 'shipping_fname',
					render:function(data, type, full, meta) {
						if(full.order_type == 'I'){
							var fname="--",lname="--";
							if(full.shipping_fname == null || full.shipping_fname == ""){
								fname = "--";
								return fname+" "+lname;
							}
							else if(full.shipping_lname == null || full.shipping_lname == "")
							{
								lname = "--";
								return fname+" "+lname;
							}else{
								return full.shipping_fname+" "+full.shipping_lname;
							}
						}else{
							var fname="--",lname="--";
							if(full.fname == null || full.fname == ""){
								fname = "--";
								return fname+" "+lname;
							}
							else if(full.lname == null || full.lname == "")
							{
								lname = "--";
								return fname+" "+lname;
							}else{
								return full.fname+" "+full.lname;
							}
						}
					}
				},
				{
					data: 'order_type',
					render: getOrderType
				},
				{
					data: 'payment_method',
					render:getPaymentMethod
				},
				{
					data: 'status',
					render:getStatus
				},
				{
					data: 'total_commission',
					render: getCommission
				},
				{
					data: 'shipping_price'
				},
				{
					data: "order_total",
					render: getOrderTotal
				}
			]
		});

		$('input.keyword').on( 'keyup click', function () {
			filterColumn( $(this).parents('div').data('column') );
		});

		$('.o_t').on( 'change', function () {
			filterColumn( $(this).parents('div').data('column') );
		});

		$('.payment_method').on( 'change', function () {
			filterColumn( $(this).parents('div').data('column') );
		});

		$('.type').on('change', function () {
			filterColumn( $(this).parents('div').data('column'));
		});
		$('.merchant').on('change', function () {
			filterColumn( $(this).parents('div').data('column'));
		});
		$('.from_date').on('change', function () {
			if($('#col1_filter_to').val() == '' && $('#col1_filter_to').val() < $('#col1_filter_from').val()) {
				return false;
			} else {
				$('#myTable1').DataTable().column( 1 ).search(
					[$('#col1_filter_from').val(),$('#col1_filter_to').val()],
					).draw();
			}
		});
		$('.to_date').on('change', function () {		
			if($('#col1_filter_from').val() == '' && $('#col1_filter_from').val() > $('#col1_filter_to').val()) {
				return false;
			} else {
				$('#myTable1').DataTable().column( 1 ).search(
					[$('#col1_filter_from').val(),$('#col1_filter_to').val()],
					).draw();
			}
		});

		@if(@$key['merchant_id'])
		$('#myTable1').DataTable().column( 7 ).search(
			"{{ $key['merchant_id'] }}",
			).draw();
		$('#col7_filter').val("{{ $key['merchant_id'] }}");
		@endif
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.slct').chosen();
	});
</script>
@endsection