@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage Merchant') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.merchant_report')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style type="text/css">

    #merchants_filter{
        display: none;
    }

    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
{{-- content --}}
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">@lang('admin_lang.manage_sub_admin')</h2>
                    <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    @lang('admin_lang.merchant_report')
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="mail_success" style="display: none;">
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
            </div>
        </div>
        <div class="mail_error" style="display: none;">
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>Error!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
            </div>
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
        </div>
        @elseif ((session()->has('error')))
        <div class="alert alert-danger vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
        </div>
        @endif
        <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong class="success_msg">Success!</strong> 
        </div>
        <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong class="error_msg"></strong> 
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- ============================================================== -->
            <!-- basic table  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">
                        @lang('admin_lang.merchant_management')
                        <a class="adbtn btn btn-primary" href="{{ route('admin.add.merchant') }}"><i class="fas fa-plus"></i> @lang('admin_lang.add')</a>
                    </h5>
                    <div class="card-body">
                        <form action="{{route('merchant.report.export')}}" id="searchMerchant" method="get">
                            @csrf
                            <div class="row">
                                <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="keyword_lb" class="col-form-label">@lang('admin_lang.keyword')</label>
                                    <input id="col1_filter" name="keyword" type="text" class="form-control keyword" placeholder="" value="{{ @$key['keyword'] }}">
                                </div>
                                <div data-column="2" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="status" class="col-form-label">@lang('admin_lang.status_1')</label>
                                    <select class="form-control status" id="col2_filter" name="status">
                                        <option value="">@lang('admin_lang.select_status')</option>
                                        <option value="A" @if(@$key['status'] == "A") selected @endif>@lang('admin_lang.active')</option>
                                        <option value="I" @if(@$key['status'] == "I") selected @endif>@lang('admin_lang.in_active')</option>
                                        <option value="U" @if(@$key['status'] == "U") selected @endif>@lang('admin_lang.un_verified')</option>
                                        <option value="AA" @if(@$key['status'] == "AA") selected @endif>@lang('admin_lang.waiting_approval')</option>
                                    </select>
                                </div>
                                <div data-column="8" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="to_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
                                    <input type="text" class="form-control datepicker from_date" name="from_date" placeholder="@lang('admin_lang_static.from_date')" value="" readonly="" id="col1_filter_from">
                                </div>
                                <div data-column="8" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="to_date" class="col-form-label">@lang('admin_lang_static.to_date')</label>
                                    <input type="text" class="form-control datepicker to_date" name="to_date" placeholder="@lang('admin_lang_static.to_date')" value="" readonly="" id="col1_filter_to">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    {{-- <label for="inputPassword" class="col-form-label"></label> --}}
                                    <a id="search" href="javascript:void(0)" class="btn btn-primary fstbtncls">@lang('admin_lang.search')</a>

                                    <input type="reset" value="Reset Search" class="btn btn-default fstbtncls reset_search">
                                    <button type="submit" id="search_orders" class="btn btn-primary fstbtncls">@lang('admin_lang.export')</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive listBody">
                            <table class="table table-striped table-bordered first" id="merchants">
                                <thead>
                                    <tr>
                                        <th>@lang('admin_lang.name')</th>
                                        <th>@lang('admin_lang.email')</th>
                                        <th>@lang('admin_lang.phone')</th>
                                        <th>@lang('admin_lang.internal_shipping_cost')</th>
                                        <th>@lang('admin_lang.external_shipping_cost')</th>
                                        <th>@lang('admin_lang.premium_merchant')</th>
                                        <th>@lang('admin_lang.international_order')</th>
                                        <th>@lang('admin_lang.status')</th>
                                        <th>@lang('admin_lang.date')</th>
                                        <!-- <th>@lang('admin_lang.Action')</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>@lang('admin_lang.name')</th>
                                        <th>@lang('admin_lang.email')</th>
                                        <th>@lang('admin_lang.phone')</th>
                                        <th>@lang('admin_lang.internal_shipping_cost')</th>
                                        <th>@lang('admin_lang.external_shipping_cost')</th>
                                        <th>@lang('admin_lang.premium_merchant')</th>
                                        <th>@lang('admin_lang.international_order')</th>
                                        <th>@lang('admin_lang.status')</th>
                                        <th>@lang('admin_lang.date')</th>
                                        <!-- <th>@lang('admin_lang.Action')</th> -->
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end basic table  -->
            <!-- ============================================================== -->
        </div>
    </div>
    <!-- footer -->
    <!-- ============================================================== -->
    @include('admin.includes.footer')
    <!-- ============================================================== -->
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">

  $( function() {
    $(".datepicker").datepicker({dateFormat: "yy-mm-dd",
     defaultDate: new Date(),
     maxDate: new Date(),
     changeMonth: true,
     changeYear: true,
     yearRange: '-100:+0'
 });            
})
  $(document).ready(function(){
        // datatable
        $('body').on('click', '.reset_search', function() {
            $('#merchants').DataTable().search('').columns().search('').draw();
        })

        function filterColumn ( i ) {
            $('#merchants').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
                ).draw();
        }
        $('#merchants').DataTable( {
            stateSave: true,
            "order": [[0, "asc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
                $('#col2_filter').val(data.search.status)
                $('#col1_filter_from').val(data.search.from_date)
                $('#col1_filter_to').val(data.search.to_date)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
                data.search.status = $('#col2_filter').val()
                data.search.from_date = $('#col1_filter_from').val()
                data.search.to_date = $('#col1_filter_to').val();
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('admin.merchant.report') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
            { 
                data: 0,
                render: function(data, type, full) {
                    return full.fname+' '+ full.lname
                }
            },
            {   data: 'email' },
            {   data: 'phone' },
            {   
                data: 'inside_shipping_cost',
                render: function(data, type, full) {
                    return '<span class="inside_shipping_cst_'+full.id+'">'+data+'</span>'
                }
            },
            {   
                data: 'shipping_cost',
                render: function(data, type, full) {
                    return '<span class="external_shipping_cost_'+full.id+'">'+data+'</span>'
                }
            },
            { 
                data: 'premium_merchant',
                render: function(data, type, full) {
                    if(data == 'Y') {
                        return '<span class="premium_merchant_'+full.id+'">@lang('admin_lang.yes')</span>'
                    } else {
                        return '<span class="premium_merchant_'+full.id+'">@lang('admin_lang.no')</span>'
                    }
                }
            },
            { 
                data: 'international_order',
                render: function(data, type, full) {
                    if(data == 'ON') {
                        return '<span class="int_status_'+full.id+'">@lang('admin_lang.on')</span>'
                    } else {
                        return '<span class="int_status_'+full.id+'">@lang('admin_lang.off')</span>'
                    }
                }
            },
            { 
                data: 'status',
                render: function(data, type, full) {
                    if(data == 'A') {
                        return '<span class="status_'+full.id+'">@lang('admin_lang.Active')</span>'
                    } else if(data == 'I') {
                        return '<span class="status_'+full.id+'">@lang('admin_lang.Inactive')</span>'
                    } else if(data == 'U') {
                        return '<span class="status_'+full.id+'">@lang('admin_lang.un_verified')</span>'
                    } else if(data == 'AA') {
                        return '<span class="status_'+full.id+'">@lang('admin_lang.awaiting_approval')</span>'
                    }
                }
            },
            {   data: 'created_at' }
            ]
        });

        // change event
        $('input.keyword').on( 'keyup click blur', function () {
            filterColumn( 1 );
        });
        $('.status').on( 'change', function () {
            filterColumn( 2 );
        });
        $('.from_date').on('change', function () {      
            if($('#col1_filter_to').val() == '' && $('#col1_filter_to').val() < $('#col1_filter_from').val()) {
                return false;
            } else {
                $('#merchants').DataTable().column( 8 ).search(
                    [$('#col1_filter_from').val(),$('#col1_filter_to').val()],
                    ).draw();
            }
        });
        $('.to_date').on('change', function () {        
            if($('#col1_filter_from').val() == '' && $('#col1_filter_from').val() > $('#col1_filter_to').val()) {
                return false;
            } else {
                $('#merchants').DataTable().column( 8 ).search(
                    [$('#col1_filter_from').val(),$('#col1_filter_to').val()],
                    ).draw();
            }
        });


        $(document).on("click",".get_orders",function(e){
            var merchant = $(this).attr("data-id");
            if(merchant != ""){
                $.ajax({
                    url:"{{ route('admin.list.order') }}",
                    data:{
                        merchant:merchant
                    },
                    success:function(res){

                    }
                });
            }
        });
        $(document).on("click","#sendEmailButton",function(e){

            var title = $.trim($("#title").val()),
            id = $.trim($("#merchant_id").val()),
            merchant_email = $.trim($("#merchant_email").val()),
            merchant_name = $.trim($("#merchant_name").val()),
            message = $.trim($("#message").val());
            // alert(merchant_email);
            if(title == ""){
                $(".error_title").text('@lang('validation.title_error')');
            }else{
                $(".error_title").text("");
            }
            if(message == ""){
                $(".error_message").text('@lang('validation.message_error')');
            }else{
                $(".error_message").text("");
            }
            if(title != "" && message != ""){
                // $("#sendEmailForm").submit();
                var rurl = '{{ route('admin.send.email.notification') }}';
                $('#sendMailNotificationModalCenter').modal('hide');
                $(".mail_success").css({
                    "display":"block"
                });
                $(".successMsg").text('@lang('admin_lang.mail_sending')');
                $(".mail_error").css({
                    "display":"none"
                });
                $.ajax({
                    type:"GET",
                    url:rurl,
                    data: {
                        id : id,
                        name : merchant_name,
                        email : merchant_email,
                        title : title,
                        message : message
                    },
                    success:function(resp){
                        // alert(resp);
                        if(resp==1){
                            $(".mail_success").css({
                                "display":"block"
                            });
                            $(".successMsg").text('@lang('admin_lang.msg_snt')');
                            
                            $(".mail_error").css({
                                "display":"none"
                            });
                            $("#title").val("");
                            $("#message").val("");
                            // $('#sendMailNotificationModalCenter').modal('hide');
                        }else{
                            $(".mail_error").css({
                                "display":"block"
                            });
                            
                            $(".errorMsg").text('@lang('admin_lang.unauthorized_msg_not_sent')');
                            $(".mail_success").css({
                                "display":"none"
                            });
                        }
                    }
                });
            }
        });
        
        $(document).on("click",".send_email_notify",function(e){
            var id= $.trim($(e.currentTarget).attr("data-id")),
            email_id= $.trim($(e.currentTarget).attr("data-emailid")),
            name= $.trim($(e.currentTarget).attr("data-name")),
            action= $.trim($(e.currentTarget).attr("data-action"));
            
            $('#sendMailNotificationModalCenter').attr("data-id",id);
            $('#sendMailNotificationModalCenter').attr("data-emailid",email_id);
            $('#sendMailNotificationModalCenter').attr("data-name",name);
            $('#sendMailNotificationModalCenter').modal('show');
            $("#merchant_email").val(email_id);
            $("#merchant_name").val(name);
            $("#merchant_id").val(id);
            $("#sendEmailForm").attr("action",action);
            
        });

        
        $(document).on("click","#saveCommission",function(e){
            var com = $.trim($("#commission").val());
            if(com != ""){
                var commission = parseFloat(com),
                merchant_email = $.trim($("#merchant_c_email").val()),
                id = $.trim($("#merchant_c_id").val()),
                merchant_name = $.trim($("#merchant_c_name").val());
                commission = commission.toFixed(3);

                if(com == ""){
                    $(".error_com").text('@lang('validation.required')');
                }else{
                    var rurl = '{{ route('admin.set.commision') }}';
                    $.ajax({
                        type:"GET",
                        url:rurl,
                        data: {
                            id : id,
                            name : merchant_name,
                            email : merchant_email,
                            commission : commission,
                        },
                        success:function(resp){
                            // alert(resp);
                            if(resp != null){
                                $(".mail_success").css({
                                    "display":"block"
                                });
                                $(".successMsg").text("Commssion updated successfully!");
                                $(".mail_error").css({
                                    "display":"none"
                                });
                                $("#commission").val("");
                                $(".error_com").text("");
                                $('#commissionModalCenter').modal('hide');
                                $(".set_com_value_"+id).attr("data-commission",commission);
                                // $("#search").trigger("click");
                            }else{
                                $(".mail_error").css({
                                    "display":"block"
                                });
                                $(".errorMsg").text("Unauthorized access!Commission not updated!");
                                $(".mail_success").css({
                                    "display":"none"
                                });
                            }
                        }
                    });
                    
                }
            }else{
                $(".error_com").text('@lang('admin_lang.comission_cannot_left_blank')');
            }
        });
        $(document).on("click",".commision_set",function(e){
            var id= $.trim($(e.currentTarget).attr("data-id")),
            email_id= $.trim($(e.currentTarget).attr("data-emailid")),
            commission= $.trim($(e.currentTarget).attr("data-commission")),
            name= $.trim($(e.currentTarget).attr("data-name"));
            $('#commissionModalCenter').attr("data-id",id);
            $('#commissionModalCenter').attr("data-emailid",email_id);
            $('#commissionModalCenter').attr("data-name",name);
            $('#commissionModalCenter').modal('show');
            $("#merchant_c_email").val(email_id);
            $("#merchant_c_name").val(name);
            $("#merchant_c_id").val(id);

            if(commission == "NaN"){
                $("#commission").val("");
            }else{
                $("#commission").val(commission);
            }
        });

        $(document).on("click","#saveShippingCost",function(e){
            // alert("dfgdkjhgdfjg");
            var cost = $.trim($("#shipping_cost").val());
            // var external_shipping_cost = $.trim($("#external_shipping_cost").val());
            var internal_shipping_cost = $.trim($("#internal_shipping_cost").val());
            // alert("internal_shipping_cost : "+internal_shipping_cost);
            if(cost != "" && internal_shipping_cost != ""){

                var shipping_cost = parseFloat(cost),
                // external_shipping_cost = parseFloat(external_shipping_cost),
                internal_shipping_cost = parseFloat(internal_shipping_cost),
                merchant_email = $.trim($("#merchant_email").val()),
                id = $.trim($("#id_m").val()),
                merchant_name = $.trim($("#merchant_name").val());
                shipping_cost = shipping_cost.toFixed(3);
                internal_shipping_cost = internal_shipping_cost.toFixed(3);
                // external_shipping_cost = external_shipping_cost.toFixed(3);
                // alert("internal");

                if(shipping_cost == ""){
                    $(".error_cost").text('@lang('admin_lang.please_provide_shipping_cost')');
                }else if(internal_shipping_cost == ""){
                    $(".error_internal_shipping_cost").text('@lang('admin_lang.please_provide_internal_shipping_cost')');
                }else{

                    // $("#saveShippingCostForm").submit();
                    var rurl = '{{ route('admin.set.shipping.cost') }}';
                    $.ajax({
                        type:"GET",
                        url:rurl,
                        data: {
                            id : id,
                            name : merchant_name,
                            email : merchant_email,
                            shipping_cost : shipping_cost,
                            internal_shipping_cost : internal_shipping_cost
                        },
                        beforeSend:function(){
                            $(".loader").show();
                        },
                        success:function(resp){
                            // alert(resp);
                            if(resp != null){

                                $(".mail_success").css({
                                    "display":"block"
                                });
                                $(".successMsg").text("Shipping cost updated successfully!");
                                $(".mail_error").css({
                                    "display":"none"
                                });
                                $("#commission").val("");
                                $(".error_cost").text("");
                                $(".s_cost_"+id).attr("data-shippingcost",resp.shipping_cost);
                                $(".s_cost_"+id).attr("data-internalshippingcost",resp.inside_shipping_cost);
                                $(".inside_shipping_cst_"+id).text(resp.inside_shipping_cost);
                                $(".external_shipping_cost_"+id).text(resp.shipping_cost);
                                $('#shippingCostModalCenter').modal('hide');
                                
                                // $("#search").trigger("click");

                            }else{

                                $(".mail_error").css({
                                    "display":"block"
                                });
                                $(".errorMsg").text("Unauthorized access!Shipping cost not updated!");
                                $(".mail_success").css({
                                    "display":"none"
                                });
                            }
                            $(".loader").hide();
                        }
                    });
                }
            }else{
                $(".error_cost").text('@lang('admin_lang.please_provide_internal_shipping_cost')');
                if(shipping_cost == ""){
                    // $(".error_cost").text("Please provide shipping cost!");
                    // $(".error_shipping_cost").text("Please provide shipping cost!");
                }
                if(internal_shipping_cost == ""){
                    $(".error_internal_shipping_cost").text('@lang('admin_lang.please_provide_internal_shipping_cost')');
                }
            }
            // $("#search").trigger("click");
        });
$(document).on("click",".shipping_cost",function(e){
    var id= $.trim($(e.currentTarget).attr("data-id")),
    email_id= $.trim($(e.currentTarget).attr("data-emailid")),
    shippingcost= $.trim($(e.currentTarget).attr("data-shippingcost")),
    internalshippingcost= $.trim($(e.currentTarget).attr("data-internalshippingcost")),
    name= $.trim($(e.currentTarget).attr("data-name"));
    $('#shippingCostModalCenter').attr("data-id",id);
    $('#shippingCostModalCenter').attr("data-emailid",email_id);
    $('#shippingCostModalCenter').attr("data-name",name);
    $('#shippingCostModalCenter').modal('show');
    $("#email_m").val(email_id);
    $("#name_m").val(name);
    $("#id_m").val(id);
    $("#id_m").attr({
        "value":id
    });
            // alert(id);
            if(shippingcost == "NaN"){
                $("#shipping_cost").val("");
            }else{
                $("#shipping_cost").val(shippingcost);
            }
            if(internalshippingcost == "NaN"){
                $("#internal_shipping_cost").val("");
            }else{
                $("#internal_shipping_cost").val(internalshippingcost);
            }
        });


        $(document).on("click",".m_verify_account",function(e){
            var id = $(e.currentTarget).attr("data-id"),
            status = $(e.currentTarget).attr("data-status");

            if(confirm('Do you want to verify this merchant account ?')){
                $.ajax({
                    url:"{{ route('admin.verify.merchant.email.admin') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".status_"+id).html("Active");
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".success_msg").html('Success!Merchant has been verified successfully!Now merchant can login!');
                            $(".v_account_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('Unauthorized access!Merchant is not verified!');

                        }
                    }
                });
            }
            
        });
        $(document).on("click",".app_merchant_details",function(e){
            var id = $(e.currentTarget).attr("data-id");

            if(confirm('Do you want to approve this merchant details ?')){
                $.ajax({
                    url:"{{ route('admin.approve.merchant.details') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".status_"+id).html("Active");
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".success_msg").html('Success!Merchant has been approved successfully!Now merchant can login!');
                            $(".appr_merchant_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('Unauthorized access!Merchant is not approved!');

                        }
                    }
                });
            }
            
        });
        // international order status change
        $(document).on("click",".international_order_status",function(e){
            var id = $(e.currentTarget).attr("data-id");
            if(confirm('Do you want to change the status of the international order for this merchant ?')){
                $.ajax({
                    url:"{{ route('admin.international.order.status.change') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp != null){
                            $(".int_status_"+id).html(resp.international_order);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".success_msg").html('Success!Status of international order has been changed!');
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('Unauthorized access!Status of international order is not changed!');

                        }
                    }
                });
            }
            // }
        });
        // premium merchant status change
        $(document).on("click",".premium_merchant_status",function(e){
            var id = $(e.currentTarget).attr("data-id");
            if(confirm('Do you want to change status of premium/not premium merchant ?')){
                $.ajax({
                    url:"{{ route('admin.premium.merchant.status.change') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp != null){
                            var st="";
                            if(resp.premium_merchant == 'Y'){
                                st="Yes";
                                $(".success_msg").html('Success!Now merchant is a premium merchant!');
                            }else{
                                st="No";
                                $(".success_msg").html('Success!Now merchant is not a premium merchant!');
                            }
                            $(".premium_merchant_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('Unauthorized access!Please try again!');

                        }
                    }
                });
            }
            // }
        });
        // block unblock merchant
        $(document).on("click",".block_unblock_merchant",function(e){
            var id = $(e.currentTarget).attr("data-id"),
            status = $(e.currentTarget).attr("data-status"),
            $status_que = "";
            if(status == 'A'){
                $status_que = "Do you want to block this merchant ?";
            }else{
                $status_que = "Do you want to unblock this merchant ?";   
            }
            if(confirm($status_que)){
                $.ajax({
                    url:"{{ route('admin.block.unblock.merchant') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp != null){
                            var st="";
                            if(resp.status == 'A'){
                                st="Active";
                                $(".success_msg").html('Success!Now merchant is unblocked!');
                                $(".icon_change_"+id).removeClass("far fa-check-circle");
                                // $(".icon_change_"+id).removeClass("");
                                $(".icon_change_"+id).addClass("fas fa-ban");
                                $(".icon_change_"+id).attr("title","Block");
                            }else if(resp.status == 'I'){
                                st="Inactive";
                                $(".success_msg").html('Success!Now merchant is blocked!');
                                $(".icon_change_"+id).removeClass("fas fa-ban");
                                $(".icon_change_"+id).addClass("far fa-check-circle");
                                $(".icon_change_"+id).attr("title","Unblock");
                            }
                            $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('Unauthorized access!Status of merchant is not changed!');

                        }
                    }
                });
            }
            // }
        });
        $(document).on("click",".delete_merchant",function(e){
            var id = $(e.currentTarget).attr("data-id");
            var $this = $(this);
            if(confirm("Do you want to delete this merchant ?")){
                $.ajax({
                    url:"{{ route('admin.delete.merchant.permanently') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".success_msg").html('Success!Now merchant is deleted successfully!');
                            // $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".tr_"+id).hide();
                            $this.parent().parent().remove();
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('Unauthorized access!Merchant is not deleted!Merchant has products!');
                        }
                    }
                });
            }
            // }
        });
    });
    // });
    function validate(evt) {
        var theEvent = evt || window.event;
                  // Handle paste
                  if (theEvent.type === 'paste') {
                      key = event.clipboardData.getData('text/plain');
                  } else {
                  // Handle key press
                  var key = theEvent.keyCode || theEvent.which;
                  key = String.fromCharCode(key);
              }
              var regex = /[0-9]|\./;
              if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>

    @endsection