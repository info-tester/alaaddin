@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.product_report')
@endsection
@section('content')
@section('links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

@include('admin.includes.links')

@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">@lang('admin_lang_static.product_report')</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">@lang('admin_lang_static.dashboard')</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('add.product.step.one') }}" class="breadcrumb-link">@lang('admin_lang_static.add_product') </a></li>
                                <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang_static.product_report')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        @if (session()->has('success'))
        <div class="alert alert-success vd_hidden session_success_msg" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
        </div>
        @elseif ((session()->has('error')))
        <div class="alert alert-danger vd_hidden session_error_msg" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
        </div>
        @endif
        <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong class="success_msg"></strong> 
        </div>
        <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong id="error_msg"></strong> 
        </div>
        <div class="row">
            <!-- ============================================================== -->
            <!-- basic table  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">@lang('admin_lang_static.product_report')</h5>
                    <div class="card-body">
                        <form action="{{ route('product.report.export') }}" method="get">
                            <div class="row">
                                <div data-column="4" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="inputText3" class="col-form-label">@lang('admin_lang_static.keyword')</label>
                                    <input type="text" class="form-control keyword" placeholder="@lang('admin_lang_static.search_by_title')" id="col4_filter" name="keyword" value="{{ @$key['keyword'] }}">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-column="3">
                                    <label for="input-select" class="col-form-label">@lang('admin_lang_static.merchant')</label>
                                    <select data-placeholder="Choose a Merchant..." class="select slt slct user_id" name="user_id" tabindex="3" id="col3_filter">
                                        <option value="">@lang('admin_lang_static.select_merchant')</option>
                                        @foreach(@$user as $us)
                                        <option value="{{ $us->id }}" @if(@$key['merchant_id'] == $us->id) selected @endif>{{ $us->fname }} {{ $us->lname }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-column="1">
                                    <label for="input-select" class="col-form-label">@lang('admin_lang_static.category')</label>
                                    <select class="form-control category" name="category" id="col1_filter">
                                        <option value="">@lang('admin_lang_static.select_category')</option>
                                        @foreach(@$category as $cat)
                                        <option value="{{ @$cat->id }}" >{{ @$cat->categoryByLanguage->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-column="2">
                                    <label for="input-select" class="col-form-label">@lang('admin_lang_static.sub_category')</label>
                                    <select class="form-control subcategory" name="sub_category" id="col2_filter">
                                        <option value="">@lang('admin_lang_static.select_sub_category')</option>
                                        @foreach(@$subCategory as $subCat)
                                        <option value="{{ @$subCat->id }}">{{ @$subCat->categoryByLanguage->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-column="7">
                                    <label for="input-select" class="col-form-label">@lang('admin_lang_static.status')</label>
                                    <select class="form-control status" name="status" id="col7_filter">
                                        <option value="">@lang('admin_lang_static.select_status')</option>
                                        <option value="A">@lang('admin_lang_static.active')</option>
                                        <option value="I">@lang('admin_lang_static.inactive')</option>
                                        <option value="W">@lang('admin_lang_static.awaiting_approval')</option>
                                        {{-- <option value="U" @if(@$key['status'] == 'U') selected @endif>@lang('admin_lang_static.unvarified')</option> --}}
                                    </select>
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-column="5" id="priceVal">
                                    <label for="input-select" class="col-form-label">@lang('admin_lang_static.price_range')</label>
                                    <div>
                                        <input type="hidden" id="amount" name="price" class="price" readonly style="border:0; color:#f6931f; font-weight:bold;" value="{{ @$key['price'] }}" class="price">
                                        <span class="price_from"></span>
                                        <span class="price_to" style="position: absolute; right: 25px"></span>
                                    </div>
                                    <div id="slider-range"></div>
                                </div>
                                <div data-column="8" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="to_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
                                    <input type="text" class="form-control datepicker from_date" name="from_date" placeholder="@lang('admin_lang_static.from_date')" value="" readonly="" id="col1_filter_from">
                                </div>
                                <div data-column="8" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="to_date" class="col-form-label">@lang('admin_lang_static.to_date')</label>
                                    <input type="text" class="form-control datepicker to_date" name="to_date" placeholder="@lang('admin_lang_static.to_date')" value="" readonly="" id="col1_filter_to">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                    {{-- <label for="inputPassword" class="col-form-label"></label> --}}
                                    {{-- <a href="add-product-step2.html" class="btn btn-primary fstbtncls">Search</a> --}}
                                    {{-- <a id="search" href="javascript:void(0)" class="btn btn-primary fstbtncls">@lang('admin_lang_static.search')</a> --}}
                                    <a href="javascript:void(0)" class="btn btn-primary fstbtncls">@lang('admin_lang_static.search')</a>
                                    <input type="reset" value="@lang('admin_lang.reset_search')" class="btn btn-default fstbtncls reset_search">
                                    <button type="submit" id="search_orders" class="btn btn-primary fstbtncls">@lang('admin_lang.export')</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive tpmrgn listBody">
                            <table class="table table-striped table-bordered" id="example">
                                <thead>
                                    <tr>                                        
                                        <th>@lang('admin_lang_static.category')</th>
                                        <th>@lang('admin_lang_static.sub_category')</th>
                                        <th>@lang('admin_lang_static.merchant')</th>
                                        <th>@lang('admin_lang_static.title')</th>
                                        <th>@lang('admin_lang.total_stock')</th>
                                        <th>@lang('admin_lang_static.price')</th>
                                        <th>@lang('admin_lang_static.is_featured')</th>
                                        <th>@lang('admin_lang_static.status')</th>
                                        <th>@lang('admin_lang_static.merchant_status')</th>
                                        <th>@lang('admin_lang.date')</th>                                        
                                    </tr>
                                </thead>                                
                                <tfoot>
                                    <tr>                                        
                                        <th>@lang('admin_lang_static.category')</th>
                                        <th>@lang('admin_lang_static.sub_category')</th>
                                        <th>@lang('admin_lang_static.merchant')</th>
                                        <th>@lang('admin_lang_static.title')</th>
                                        <th>@lang('admin_lang.total_stock')</th>
                                        <th>@lang('admin_lang_static.price')</th>
                                        <th>@lang('admin_lang_static.is_featured')</th>
                                        <th>@lang('admin_lang_static.status')</th>
                                        <th>@lang('admin_lang_static.merchant_status')</th>
                                        <th>@lang('admin_lang.date')</th>                                        
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end basic table  -->
            <!-- ============================================================== -->
        </div>
    </div>
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>
<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script>

  $( function() {
    $(".datepicker").datepicker({dateFormat: "yy-mm-dd",
     defaultDate: new Date(),
     maxDate: new Date(),
     changeMonth: true,
     changeYear: true,
     yearRange: '-100:+0'
 });            
})
  function filterColumn ( i ) {
    $('#example').DataTable().column( i ).search(
        $('#col'+i+'_filter').val(),
        ).draw();
}
function filterColumnPrice ( i ) {
    $('#example').DataTable().search(
        $('#amount').val(),
        ).draw();
}

function getImg(data, type, full, meta) {
       //
       // console.log(data)
       if(data) {
         return '<img width="100" height="70" src="storage/app/public/products/'+data+'" />';
     } else {
        return '<img width="100" height="70" src="public/frontend/images/new-pro3.png" />';
    }
}

function getAction(data, id, full, meta) {
   var a = '';
   var ln = full.product_sub_category.category.price_variant;
   if(full.status == 'A') {
        a += '<a class="status_change block" href="javascript:void(0)" data-id="'+full.id+'" data-status=""><i class="fa fa-ban icon_change_'+full.id+'" title="@lang('admin_lang.block')"></i></a> '
    } else if(full.status == 'I') {
        a += '<a class="status_change unblock" href="javascript:void(0)" data-id="'+full.id+'" data-status="'+full.id+'"><i class="fa fa-check-circle icon_change_'+full.id+'" title="@lang('admin_lang.unblock')"></i></a> '
    } else if(full.status == 'W') {
        a += '<a class="status_change approve" href="javascript:void(0)" data-id="'+full.id+'" data-status="'+full.id+'"><i class="fa fa-thumbs-up icon_change_'+full.id+'" title="@lang('admin_lang.approve')"></i></a> '
    }
    a += '<a href="{{ url('admin/edit-product').'/' }}'+full.slug+'"><i class="fa fa-edit" title="Edit"></i></a> '
    a += '<a class="delete_product" href="javascript:void(0)" data-id="'+full.id+'"><i class="fa fa-trash" title="Delete"></i></a> '
    if(ln.length > 0) {
        a += '<a href="{{ url('admin/product-discount').'/' }}'+full.slug+'"><i class="fa fa-percent" title="Set Discount Price"></i></a> '
    }
    return a;
}
function getMerchant(data, type, full, meta) {
    return data.fname+' '+data.lname ;
}
function getStatus(data, type, full, meta) {
    if(data == 'Y') 
        return "<span class='status_"+full.id+"'>Yes</span>";
    else if(data == 'N') 
            // return 'No';
        return "<span class='status_"+full.id+"'>No</span>";
        else if(data == 'A') 
            // return 'Active';
        return "<span class='status_"+full.id+"'>Active</span>";
        else if(data == 'I') 
            // return 'Inactive';
        
        return "<span class='status_"+full.id+"'>Inactive</span>";
        else if(data == 'W') 
            // return 'Awaiting Approval';
        return "<span class='status_"+full.id+"'>Awaiting Approval</span>";
    }
    $(document).ready(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            @if(@$highestPrice->price)
            max: {{ @$highestPrice->price }},
            @else
            max: 1000.000,
            @endif
            @php
            if(@$key['price']){
                $price_val = $key['price'];
            }else{
                if(@$highestPrice->price) {
                $price_val = '0, '. (Int)@$highestPrice->price;
                } else {
                $price_val = '0, 1000';
                }
            }
            @endphp
            values: [ {{ $price_val }} ],
            slide: function( event, ui ) {
                $( "#amount" ).val( ui.values[ 0 ]  + "," + ui.values[ 1 ] );
                $('.price_from').html(ui.values[ 0 ] + ' {{ getCurrency() }}')
                $('.price_to').html(ui.values[ 1 ] + ' {{ getCurrency() }}')
            }
        });
        var styles = `
            table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
                right: 0em;
                content: "";
            }
            table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
                right: 0em;
                content: "";
            }
        `
        var styleSheet = document.createElement("style");
        styleSheet.innerText = styles;
        document.head.appendChild(styleSheet);
        $('#example').DataTable( {
            stateSave: true,
            "order": [[1, "desc"]],
            "stateLoadParams": function (settings, data) {
            // console.log(data);
            $('#col3_filter').val(data.search.user_id)
            $('#col4_filter').val(data.search.keyword)
            $('#col1_filter').val(data.search.category)
            $('#col2_filter').val(data.search.subcategory)
            $('#col7_filter').val(data.search.status)
            $('#amount').val(data.search.price)
            $('#col1_filter_from').val(data.search.from_date)
            $('#col1_filter_to').val(data.search.to_date)
            const a = data.search.price.split(',');
            if (a[0] && a[1]) {
                $('#slider-range').slider("option", "values", a);
            }
        },
        stateSaveParams: function (settings, data) {
            // console.log('stateSaveParams', data)
            data.search.user_id = $('#col3_filter').val();
            data.search.keyword = $('#col4_filter').val();
            data.search.category = $('#col1_filter').val();
            data.search.subcategory = $('#col2_filter').val();
            data.search.status = $('#col7_filter').val();
            data.search.price = $('#amount').val();
            data.search.from_date = $('#col1_filter_from').val()
            data.search.to_date = $('#col1_filter_to').val();
        },
        "processing": true,
        "serverSide": true,
        'serverMethod': 'post',
        "ajax": {
            "url": "{{ route('admin.product.report') }}",
            "data": function ( d ) {
                d.myKey = "myValue";
                d._token = "{{ @csrf_token() }}";
            }
        },
        'columns': [
        { data: 'product_parent_category.category.category_by_language.title' },
        { data: 'product_sub_category.category.category_by_language.title' },
        { 
            data: 'product_marchant',
            render: getMerchant
        },
        { data: 'product_by_language.title' },
        {
            render: function(data, type, full, meta){
                if(full.product_variants.length != 0){
                    var totStock = 0;
                    for(var i=0;i<full.product_variants.length;i++){
                        totStock = totStock + full.product_variants[i].stock_quantity;    
                    }
                    
                    return totStock;
                }else{
                    return full.stock;
                }
            }
        },
        { data: 'price' },
        { 
            data: 'is_featured',
            render: getStatus
        },
        { 
            data: 'status',
            render: getStatus
        },
        { 
            data: 'seller_status',
            render: getStatus
        },
        { data: 'created_at' }
        // { 
        //     data: 'seller_status,id',
        //     render: getAction
        // },
        ]
    });

        $('input.keyword').on( 'keyup click', function () {
            filterColumn( $(this).parents('div').data('column') );
        } );

        $('.category').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        } );

        $('.subcategory').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        } );

        $('.status').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        } );

        $('.user_id').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        } );
        $('.from_date').on('change', function () {      
            if($('#col1_filter_to').val() == '' && $('#col1_filter_to').val() < $('#col1_filter_from').val()) {
                return false;
            } else {
                $('#example').DataTable().column( 8 ).search(
                    [$('#col1_filter_from').val(),$('#col1_filter_to').val()],
                    ).draw();
            }
        });
        $('.to_date').on('change', function () {        
            if($('#col1_filter_from').val() == '' && $('#col1_filter_from').val() > $('#col1_filter_to').val()) {
                return false;
            } else {
                $('#example').DataTable().column( 8 ).search(
                    [$('#col1_filter_from').val(),$('#col1_filter_to').val()],
                    ).draw();
            }
        });

        @if(@$key['merchant_id'])
        $('#example').DataTable().column( 3 ).search(
            {{ $key['merchant_id'] }},
            ).draw();
        $('#col3_filter').val({{ $key['merchant_id'] }});
        @endif

        $('body').on('mouseleave', '.ui-slider-handle', function () {
            filterColumnPrice( $('#priceVal').data('column') );
        });
        
        
        $('body').on('click', '.reset_search', function() {
            $('#example').DataTable().search('').columns().search('').draw();
        })
    });
$( function() {
    // $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 )  +
    //   "," + $( "#slider-range" ).slider( "values", 1 ) );
    $('.price_from').html($( "#slider-range" ).slider( "values", 0 ) + ' {{ getCurrency() }}');
    $('.price_to').html($( "#slider-range" ).slider( "values", 1 ) + ' {{ getCurrency() }}');
    $( "#slider-range" ).slider({
    //   change: function( event, ui ) {
    //     var status = $.trim($("#status").val()),
    //     keyword = $.trim($("#keyword").val()),
    //     category = $.trim($("#category").val()),
    //     sub_category = $.trim($("#subCategory").val()),
    //     price = $.trim($("#amount").val()),
    //     user_id = $.trim($("#slct").val());
    //     $.ajax({
    //         type:"POST",
    //         url:"{{ route('manage.product') }}",
    //         data:{
    //             status:status,
    //             _token: '{{ csrf_token() }}',
    //             keyword:keyword,
    //             category:category,
    //             sub_category:sub_category,
    //             price:price,
    //             user_id:user_id,
    //         },
    //         beforeSend:function(){
    //             $(".loader").show();
    //         },
    //         success:function(resp){
    //             $(".listBody").html(resp);
    //             $(".loader").hide();
    //         }
    //     });
    // }
});
});
</script>
<script>
    $(document).ready(function() {
        $('.slct').chosen();
    })
</script>
{{-- for fetching models of brand  --}}
<script>
    $(document).ready(function(){

        /*$(document).on("change","#status",function(e){
            var status = $.trim($("#status").val()),
            keyword = $.trim($("#keyword").val()),
            category = $.trim($("#category").val()),
            sub_category = $.trim($("#subCategory").val()),
            price = $.trim($("#amount").val()),
            user_id = $.trim($("#slct").val());
            $.ajax({
                type:"POST",
                url:"{{ route('manage.product') }}",
                data:{
                    status:status,
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    category:category,
                    sub_category:sub_category,
                    price:price,
                    user_id:user_id,
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });

        $(document).on("change","#category",function(e){
            var status = $.trim($("#status").val()),
            keyword = $.trim($("#keyword").val()),
            category = $.trim($("#category").val()),
            sub_category = $.trim($("#subCategory").val()),
            price = $.trim($("#amount").val()),
            user_id = $.trim($("#slct").val());
            $.ajax({
                type:"POST",
                url:"{{ route('manage.product') }}",
                data:{
                    status:status,
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    category:category,
                    sub_category:sub_category,
                    price:price,
                    user_id:user_id,
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });

        $(document).on("change","#subCategory",function(e){
            var status = $.trim($("#status").val()),
            keyword = $.trim($("#keyword").val()),
            category = $.trim($("#category").val()),
            sub_category = $.trim($("#subCategory").val()),
            price = $.trim($("#amount").val()),
            user_id = $.trim($("#slct").val());
            $.ajax({
                type:"POST",
                url:"{{ route('manage.product') }}",
                data:{
                    status:status,
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    category:category,
                    sub_category:sub_category,
                    price:price,
                    user_id:user_id,
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });

        $(document).on("change","#slct",function(e){
            var status = $.trim($("#status").val()),
            keyword = $.trim($("#keyword").val()),
            category = $.trim($("#category").val()),
            sub_category = $.trim($("#subCategory").val()),
            price = $.trim($("#amount").val()),
            user_id = $.trim($("#slct").val());
            $.ajax({
                type:"POST",
                url:"{{ route('manage.product') }}",
                data:{
                    status:status,
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    category:category,
                    sub_category:sub_category,
                    price:price,
                    user_id:user_id,
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });

        $("#search").click(function(){
            console.log($.trim($("#amount").val()));
            var status = $.trim($("#status").val()),
            keyword = $.trim($("#keyword").val()),
            category = $.trim($("#category").val()),
            sub_category = $.trim($("#subCategory").val()),
            price = $.trim($("#amount").val()),
            user_id = $.trim($("#slct").val());
            $.ajax({
                type:"POST",
                url:"{{ route('manage.product') }}",
                data:{
                    status:status,
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    category:category,
                    sub_category:sub_category,
                    price:price,
                    user_id:user_id,
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });
        */
       /* $(document).on("blur","#keyword",function(e){
            var status = $.trim($("#status").val()),
            keyword = $.trim($("#keyword").val()),
            category = $.trim($("#category").val()),
            sub_category = $.trim($("#subCategory").val()),
            price = $.trim($("#amount").val()),
            user_id = $.trim($("#slct").val());
            $.ajax({
                type:"POST",
                url:"{{ route('manage.product') }}",
                data:{
                    status:status,
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    category:category,
                    sub_category:sub_category,
                    price:price,
                    user_id:user_id,
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });*/

        $('#col1_filter').change(function(){
            if($(this).val() != '')
            {
                var value  = $(this).val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('sub.cat.fetch') }}",
                    method:"POST",
                    data:{value:value, _token:_token},
                    success:function(response)
                    {
                        $('#col2_filter').html(response.result.output);
                    }
                })
            }
        });

        $('#input-select').change(function(){
          $('#col2_filter').val('');
      });
        $(document).on("click",".status_change",function(e){

            var id = $(e.currentTarget).attr("data-id");
            // ch_status = $(e.currentTarget).attr("data-status");
            // alert("id : "+id);
            if(confirm('Do you want to change status?')){
                $.ajax({
                    url:"{{ route('status.product.update') }}",
                    type:"get",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        // console.log(resp);
                        if(resp != 0){
                            var st="";
                            if(resp.status == 'A'){
                                st="Active";
                                $(".success_msg").html("Success!Status is changed to Active!");
                                $(".icon_change_"+id).removeClass("far fa-check-circle");
                                // $(".icon_change_"+id).removeClass("");
                                $(".icon_change_"+id).addClass("fas fa-ban");
                                $(".icon_change_"+id).attr("title","Block");
                            }else if(resp.status == 'I'){
                                st="Inactive";
                                $(".success_msg").html("Success!Status is changed to inactive!");
                                $(".icon_change_"+id).removeClass("fas fa-ban");
                                $(".icon_change_"+id).addClass("far fa-check-circle");
                                $(".icon_change_"+id).attr("title","Unblock");
                            }else if(resp.status == 'W'){
                                st="Awaiting Approval";
                                $(".success_msg").html("Success!Status is changed to Awaiting Approval!");
                                // $(".icon_change_"+id).removeClass("fas fa-ban");
                                $(".icon_change_"+id).addClass("fa fa-thumbs-up");
                                $(".icon_change_"+id).attr("title","Awaiting Approval");
                            }
                            $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('Unauthorized access!Status of merchant is not changed!');

                        }
                        $(".session_success_msg").hide();
                        $(".session_error_msg").hide();
                    },
                    error:function(err){
                        console.log("error");
                    }
                });
            }
            // }
        }); 
        $(document).on("click",".delete_product",function(e){
            var id = $(e.currentTarget).attr("data-id");
            var $this = $(this);
            if(confirm("Do you want to delete this product ?")){
                $.ajax({
                    url:"{{ route('delete.product') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".success_msg").html('Success!Product is deleted successfully!');
                            // $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            // $(".tr_"+id).hide();
                            $this.parent().parent().remove();
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('Unauthorized access!Product is not deleted!');
                        }
                        $(".session_success_msg").hide();
                        $(".session_error_msg").hide();
                    }
                });
            }
            // }
        });
    });
</script>
@endsection