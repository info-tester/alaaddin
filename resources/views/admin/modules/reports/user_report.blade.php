@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage Customer') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.user_report')
@endsection
@section('content')
@section('links')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

@include('admin.includes.links')

@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.user_report') </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link"> @lang('admin_lang.customers') </a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.user_report')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="mail_success" style="display: none;">
                <div class="alert alert-success vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
                </div>
            </div>
            <div class="mail_error" style="display: none;">
                <div class="alert alert-danger vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>Error!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
                </div>
            </div>
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['code']['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong class="success_msg">Success!</strong>
            </div>
            <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong class="error_msg">Error!</strong>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <!-- ============================================================== -->
                <!-- basic table  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">@lang('admin_lang.user_report') <a class="adbtn btn btn-primary" href="{{ route('admin.add.customer') }}"><i class="fas fa-plus"></i> @lang('admin_lang.Add')</a></h5>
                        <div class="card-body">
                            <form id="search_customers_form" method="get" action="{{route('user.report.export')}}">
                                @csrf
                                <div class="row">
                                    <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="inputText3" class="col-form-label">@lang('admin_lang.Keywords') </label>
                                        <input id="col1_filter" name="keyword" type="text" class="form-control keyword" placeholder="Keywords" value="{{ @$key['keyword'] }}">
                                    </div>
                                    <div data-column="2" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="input-select" class="col-form-label">@lang('admin_lang.status')</label>
                                        <select class="form-control status" id="col2_filter" name="status">
                                            <option value="">@lang('admin_lang.select_status')</option>
                                            <option value="A" @if(@$key['status'] == 'A') selected @endif>@lang('admin_lang.Active')</option>
                                            <option value="I" @if(@$key['status'] == 'I') selected @endif>@lang('admin_lang.Inactive')</option>
                                            <option value="U" @if(@$key['status'] == 'U') selected @endif>@lang('admin_lang.un_verified')</option>
                                        </select>
                                    </div>
                                    <div data-column="3" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="signup_from_lb" class="col-form-label">@lang('admin_lang.signup_from')</label>
                                        <select class="form-control signup_from" id="col3_filter" name="signup_from">
                                            <option value="">@lang('admin_lang.select_signup_from')</option>
                                            <option value="S">@lang('admin_lang.web_site')</option>
                                            <option value="F">@lang('admin_lang.facebook')</option>
                                            <option value="G">@lang('admin_lang.google')</option>
                                        </select>
                                    </div>
                                    <div data-column="5" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="to_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
                                        <input type="text" class="form-control datepicker from_date" name="from_date" placeholder="@lang('admin_lang_static.from_date')" value="" readonly="" id="col1_filter_from">
                                    </div>
                                    <div data-column="5" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="to_date" class="col-form-label">@lang('admin_lang_static.to_date')</label>
                                        <input type="text" class="form-control datepicker to_date" name="to_date" placeholder="@lang('admin_lang_static.to_date')" value="" readonly="" id="col1_filter_to">
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <a href="javascript:void(0)" id="search_customers" class="btn btn-primary fstbtncls">@lang('admin_lang.Search')</a>

                                        <input type="reset" value="Reset Search" class="btn btn-default fstbtncls reset_search">
                                        <button type="submit" id="search_orders" class="btn btn-primary fstbtncls">@lang('admin_lang.export')</button>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive listBody tpmrgn">
                                <table class="table table-striped table-bordered" id="customers">
                                    <thead>
                                        <tr>
                                            <th>@lang('admin_lang.Name')</th>
                                            <th>@lang('admin_lang.email')</th>
                                            <th>@lang('admin_lang.phone')</th>
                                            <th>@lang('admin_lang.signup_from')</th>
                                            <th>@lang('admin_lang.status')</th>
                                            <th>@lang('admin_lang.date')</th>
                                            <!-- <th>@lang('admin_lang.Action')</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- @if(@$customers)
                                            @foreach(@$customers as $customer)
                                            <tr class="tr_{{ @$customer->id }}">
                                                <td>{{ @$customer->fname." ".@$customer->lname }}</td>
                                                <td>{{ @$customer->email }}</td>
                                                <td>{{ @$customer->phone }}</td>
                                                <td>
                                                    @if(@$customer->signup_from == 'S')
                                                    @lang('admin_lang.web_site')
                                                    @elseif(@$customer->signup_from == 'F')
                                                    @lang('admin_lang.facebook')
                                                    @elseif(@$customer->signup_from == 'G')
                                                    @lang('admin_lang.google')
                                                    @endif
                                                </td>
                                                <td class="status_{{ @$customer->id }}">
                                                    @if(@$customer->status == 'A')
                                                    @lang('admin_lang.Active')
                                                    @elseif(@$customer->status == 'I')
                                                    @lang('admin_lang.Inactive')
                                                    @elseif(@$customer->status == 'U')
                                                    @lang('admin_lang.un_verified')
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(@$customer->status == 'I')
                                                    <a class="block_unblock_customer b_u_{{ @$customer->id }}" href="javascript:void(0)" data-status="{{ @$customer->status }}" data-id="{{ @$customer->id }}"><i class="far fa-check-circle icon_change_{{ @$customer->id }}" title="Unblock"></i></a>
                                                    @elseif(@$customer->status == 'A')
                                                    <a href="javascript:void(0)" class="block_unblock_customer" data-status="{{ @$customer->status }}" data-id="{{ @$customer->id }}"><i class="fas fa-ban icon_change_{{ @$customer->id }}" title="Block"></i></a>
                                                    @endif
                                                    <a href="{{ route('admin.view.customer.profile',@$customer->id) }}"><i class="fas fa-eye" title="View"></i></a>
                                                    <a href="{{ route('admin.edit.customer',@$customer->id) }}"><i class="fas fa-edit" title="Edit"></i></a>
                                                    <a href="javascript:void(0)" data-toggle="modal" class="send_email_notify" data-id="{{ @$customer->id }}" data-email="{{ @$customer->email }}" data-fname="{{ @$customer->fname }}" data-lname="{{ @$customer->lname }}" data-action="{{ route('admin.send.notification.customer',@$customer->id) }}"><i class="fas fa-paper-plane" title="Send Email"></i></a>
                                                    <a class="delete_customer" href="javascript:void(0)" data-id="{{ @$customer->id }}"><i class=" fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @endif --}}
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>@lang('admin_lang.Name')</th>
                                                <th>@lang('admin_lang.email')</th>
                                                <th>@lang('admin_lang.phone')</th>
                                                <th>@lang('admin_lang.signup_from')</th>
                                                <th>@lang('admin_lang.status')</th>
                                                <th>@lang('admin_lang.date')</th>
                                                <!-- <th>@lang('admin_lang.Action')</th> -->
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
            </div>
            <!-- footer -->   
            @include('admin.includes.footer')
            <!-- end footer -->
            <div class="loader" style="display: none;">
                <img src="{{url('public/loader.gif')}}">
            </div>
            <div class="modal fade" id="sendMailNotificationModalCenter" data-id="" data-emailid="">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">
                                @lang('admin_lang.send_email_notification')
                            </h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="basic-form">
                                <form id="sendEmailForm" method="post" action="">
                                    @csrf
                                    <div class="form-row">
                                        {{-- 
                                            <div class="form-group col-md-12"> --}}
                                                {{-- <label id="title">Email-id</label> --}}
                                                <input id="customer_email" name="customer_email" type="hidden" class="form-control" placeholder="" readonly>
                                                <input id="customer_name" name="customer_name" type="hidden" class="form-control" placeholder="" readonly>
                                                <input id="customer_id" name="customer_id" type="hidden" class="form-control" placeholder="" readonly>
                                                {{-- <span class="error_title" style="color: red;"></span> --}}
                                                {{-- 
                                                </div>
                                                --}}
                                                <div class="form-group col-md-12">
                                                    <label id="titleLabel">@lang('admin_lang.title')</label>
                                                    <input id="title" name="title" type="text" class="form-control required" placeholder='@lang('admin_lang.title')'>
                                                    <span class="error_title" style="color: red;"></span>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label id="messageLabel">@lang('admin_lang.msg')</label>
                                                    <textarea id="message" name="message" class="form-control required" rows="5"></textarea>
                                                    <span class="error_message" style="color: red;"></span>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label></label>
                                                    <button type="button" id="sendEmailButton" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endsection
                    @section('scripts')
                    @include('admin.includes.scripts')
                    <script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
                    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
                    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
                    <script type="text/javascript">
                      $( function() {
                        $(".datepicker").datepicker({dateFormat: "yy-mm-dd",
                           defaultDate: new Date(),
                           maxDate: new Date(),
                           changeMonth: true,
                           changeYear: true,
                           yearRange: '-100:+0'
                       });            
                    })
                      function filterColumn ( i ) {
                        $('#customers').DataTable().column( i ).search(
                            $('#col'+i+'_filter').val(),
                            ).draw();
                    }

                    $(document).ready(function(){

                        $(document).on("click","#sendEmailButton",function(e){

                            var title = $.trim($("#title").val()),
                            id = $.trim($("#customer_id").val()),
                            customer_email = $.trim($("#customer_email").val()),
                            customer_name = $.trim($("#customer_name").val()),
                            message = $.trim($("#message").val());
            // alert(merchant_email);
            if(title == ""){
                $(".error_title").text('@lang('validation.title_error')');
            }else{
                $(".error_title").text("");
            }
            if(message == ""){
                $(".error_message").text('@lang('validation.message_error')');
            }else{
                $(".error_message").text("");
            }
            if(title != "" && message != ""){
                // $("#sendEmailForm").submit();
                var rurl = '{{ route('admin.send.message.customer') }}';
                $('#sendMailNotificationModalCenter').modal('hide');
                $(".mail_success").css({
                    "display":"block"
                });
                $(".successMsg").text('@lang('admin_lang.mail_sending')');
                
                $(".mail_error").css({
                    "display":"none"
                });
                $.ajax({
                    type:"GET",
                    url:rurl,
                    data: {
                        id : id,
                        name : customer_name,
                        email : customer_email,
                        title : title,
                        message : message
                    },
                    success:function(resp){
                        // alert(resp);
                        if(resp==1){
                            $(".mail_success").css({
                                "display":"block"
                            });
                            $(".successMsg").text('@lang('admin_lang.msg_snt')');
                            
                            $(".mail_error").css({
                                "display":"none"
                            });
                            $("#title").val("");
                            $("#message").val("");
                            $('#sendMailNotificationModalCenter').modal('hide');
                        }else{
                            $(".mail_error").css({
                                "display":"block"
                            });
                            
                            $(".errorMsg").text("@lang('admin_lang.msg_not_sent')");
                            $(".mail_success").css({
                                "display":"none"
                            });
                            $("#title").val("");
                            $("#message").val("");
                            $('#sendMailNotificationModalCenter').modal('hide');
                        }
                    }
                });
            }
        });

        $(document).on("click",".send_email_notify",function(e){
            var id= $.trim($(e.currentTarget).attr("data-id")),
            email_id= $.trim($(e.currentTarget).attr("data-email")),
            name= $.trim($(e.currentTarget).attr("data-fname")),
            action= $.trim($(e.currentTarget).attr("data-action"));

            $('#sendMailNotificationModalCenter').attr("data-id",id);
            $('#sendMailNotificationModalCenter').attr("data-emailid",email_id);
            $('#sendMailNotificationModalCenter').attr("data-name",name);
            $('#sendMailNotificationModalCenter').modal('show');
            $("#customer_email").val(email_id);
            $("#customer_name").val(name);
            $("#customer_id").val(id);
            $("#sendEmailForm").attr("action",action);

        });

        $(document).on("click",".block_unblock_customer",function(e){
            var id = $(e.currentTarget).attr("data-id"),
            status = $(e.currentTarget).attr("data-status");
            // $status_que = "";
            // alert(status);
            // if(status == 'A'){
            //     $status_que = "Do you want to block this customer ?";
            // }else{
            //     $status_que = "Do you want to unblock this customer ?";   
            // }
            if(confirm("Do you want to change status of this customer ?")){
                $.ajax({
                    url:"{{ route('admin.block.unblock.customer') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp != null){
                            var st="";
                            if(resp.status == 'A'){
                                st="Active";
                                $(".success_msg").html('Success!Now customer is unblocked!');
                                $(".icon_change_"+id).removeClass("far fa-check-circle");
                                // $(".icon_change_"+id).removeClass("");
                                $(".icon_change_"+id).addClass("fas fa-ban");
                                $(".icon_change_"+id).attr("title","Block Customer");
                                // $(".b_u_"+id).attr('data-status','I');
                                // $(e.currentTarget).attr('data-status','A');
                            }else if(resp.status == 'I'){
                                st="Inactive";
                                $(".success_msg").html('Success!Now customer is blocked!');
                                $(".icon_change_"+id).removeClass("fas fa-ban");
                                $(".icon_change_"+id).addClass("far fa-check-circle");
                                $(".icon_change_"+id).attr("title","Unblock Customer");
                                // $(".b_u_"+id).attr('data-status','I');
                                // $(e.currentTarget).attr('data-status','I');
                            }
                            $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('Unauthorized access!Status of merchant is not changed!');

                        }
                    }
                });
            }
            // }
        });

                        $(document).on("click",".delete_customer",function(e){
                            var id = $(e.currentTarget).attr("data-id");
                            var $this = $(this);
                            if(confirm("Do you want to delete this customer ?")){
                                $.ajax({
                                    url:"{{ route('admin.delete.customer.details') }}",
                                    type:"GET",
                                    data:{
                                        id:id
                                    },
                                    success:function(resp){
                                        if(resp == 1){
                                            $(".success_msg").html('@lang('admin_lang.success_cust_delete')');
                            // $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            // $(".tr_"+id).hide();
                            console.log($this.parent().parent());
                            $this.parent().parent().remove();
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('@lang('admin_lang.unauthorized_access_cust')');
                        }
                    }
                });
                            }
            // }
        });


        // datatable started
        var styles = `
            table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
                right: 0em;
                content: "";
            }
            table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
                right: 0em;
                content: "";
            }
        `
        var styleSheet = document.createElement("style");
        styleSheet.innerText = styles;
        document.head.appendChild(styleSheet);
        $('#customers').DataTable( {
            stateSave: true,
            "order": [[0, "asc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
                $('#col2_filter').val(data.search.status)
                $('#col3_filter').val(data.search.signup_from)
                $('#col1_filter_from').val(data.search.from_date)
                $('#col1_filter_to').val(data.search.to_date)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
                data.search.status = $('#col2_filter').val()
                data.search.signup_from = $('#col3_filter').val()
                data.search.from_date = $('#col1_filter_from').val()
                data.search.to_date = $('#col1_filter_to').val();
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('admin.user.report') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
            { 
                data: 0,
                render: function(data, type, full) {
                    var fname = full.fname;
                    var lname = full.lname;
                    if(fname != null){
                        fname = fname;
                    }else{
                        fname ='';
                    }

                    if(lname != null){
                        lname = lname;
                    }else{
                        lname ='';
                    }
                    return fname+' '+ lname
                }
            },
            {   data: 'email' },
            { 
                data: 0,
                render: function(data, type, full) {
                    if(full.phone == 0) {
                        return '--';
                    } else {
                        return full.phone;
                    }
                }
            },
            { 
                data: 'signup_from',
                render: function(data, type, full) {
                    if(full.signup_from == 'S') {
                        return 'Website';
                    } else if(full.signup_from == 'F') {
                        return 'Facebook';
                    } else if(full.signup_from == 'G') {
                        return 'Google+';
                    } else {
                        return '--';
                    }
                }
            },
            { 
                data: 'status',
                render: function(data, type, full) {
                    if(data == 'A') {
                        return '<span class="status_'+full.id+'">Active</span>'
                    } else if(data == 'I') {
                        return '<span class="status_'+full.id+'">Inactive</span>'
                    } else if(data == 'U') {
                        return '<span class="status_'+full.id+'">Unverified</span>'
                    }
                }
            },
            {   data: 'created_at' }
                // { 
                //     data: 0,
                //     render: function(data, id, full, meta) {
                //         var a = '';
                //         if(full.status == 'A') {
                //             a +='<a href="javascript:void(0)" class="block_unblock_customer" data-status="'+full.status+'" data-id="'+full.id+'"><i class="fas fa-ban icon_change_'+full.id+'" title="Block"></i></a>'
                //         } else if(full.status == 'I') {
                //             a += '<a class="block_unblock_customer b_u_'+full.id+'" href="javascript:void(0)" data-status="'+full.status+'" data-id="'+full.id+'"><i class="far fa-check-circle icon_change_'+full.id+'" title="Unblock"></i></a>'
                //         } 

                //         a += ' <a href="{{ url('admin/view-customer-profile') }}/'+full.id+'"><i class="fas fa-eye" title="View"></i></a>'

                //         a += ' <a href="{{ url('edit-customer') }}/'+full.id+'"><i class="fas fa-edit" title="Edit"></i></a>'

                //         a += ' <a href="javascript:void(0)" data-toggle="modal" class="send_email_notify" data-id="'+full.id+'" data-email="'+full.email+'" data-fname="'+full.fname+'" data-lname="'+full.lname+'" data-action="{{ url('send-notification-customer') }}/'+full.id+'"><i class="fas fa-paper-plane" title="Send Email"></i></a>'

                //         a += ' <a class="delete_customer" href="javascript:void(0)" data-id="'+full.id+'"><i class=" fas fa-trash"></i></a>'

                //         return a;
                //     }
                // }
                ]
            });

        // change event
        $('input.keyword').on( 'keyup click blur', function () {
            filterColumn( 1 );
        });
        $('.status').on( 'change', function () {
            filterColumn( 2 );
        }); 
        $('.signup_from').on( 'change', function () {
            filterColumn( 3 );
        });
        $('.from_date').on('change', function () {      
            if($('#col1_filter_to').val() == '' && $('#col1_filter_to').val() < $('#col1_filter_from').val()) {
                return false;
            } else {
                $('#customers').DataTable().column( 5 ).search(
                    [$('#col1_filter_from').val(),$('#col1_filter_to').val()],
                    ).draw();
            }
        });
        $('.to_date').on('change', function () {        
            if($('#col1_filter_from').val() == '' && $('#col1_filter_from').val() > $('#col1_filter_to').val()) {
                return false;
            } else {
                $('#customers').DataTable().column( 5 ).search(
                    [$('#col1_filter_from').val(),$('#col1_filter_to').val()],
                    ).draw();
            }
        });

        
        $('body').on('click', '.reset_search', function() {
            // localStorage.removeItem('DataTables_customers_/drivers_group/admin/manage-customer');
            $('#customers').DataTable().search('').columns().search('').draw();
        })



        $("#keyword").on("keyup", function(e){
            var value = $(this).val().toLowerCase();
            $("#myTable tbody tr").filter(function(){
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
        /*$(document).on("change","#status",function(e){
            var keyword = $.trim($("#keyword").val());
            var status = $.trim($("#status").val());
            var signup_from = $.trim($("#signup_from").val());
            
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.customer') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    signup_from:signup_from,
                    status:status
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });*/
        /*$(document).on("change","#signup_from",function(e){
            var keyword = $.trim($("#keyword").val());
            var status = $.trim($("#status").val());
            var signup_from = $.trim($("#signup_from").val());
            
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.customer') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    signup_from:signup_from,
                    status:status
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });*/
        /*$(document).on("blur","#status",function(e){
            var keyword = $.trim($("#keyword").val());
            var status = $.trim($("#status").val());
            var signup_from = $.trim($("#signup_from").val());
            
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.customer') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    signup_from:signup_from,
                    status:status
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });*/
        $(document).on("blur","#keyword",function(e){
            var keyword = $.trim($("#keyword").val());
            var status = $.trim($("#status").val());
            var signup_from = $.trim($("#signup_from").val());
            // $.fn.dataTable.ext.errMode = 'none';
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.customer') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    signup_from:signup_from,
                    status:status
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });
        // $("#search_customers").click(function(){
        //     // $("#search_customers_form").submit();
        //     var keyword = $.trim($("#keyword").val());
        //     var status = $.trim($("#status").val());
        //     var signup_from = $.trim($("#signup_from").val());
        //     // $.fn.dataTable.ext.errMode = 'none';
        //     // $('#myTable').dataTable({
        //     //     "bServerSide": true,
        //     //     ....
        //     //     "bDestroy": true
        //     // });
        //     $.ajax({
        //         type:"POST",
        //         url:"{{ route('admin.list.customer') }}",
        //         data:{
        //             _token: '{{ csrf_token() }}',
        //             keyword:keyword,
        //             signup_from:signup_from,
        //             status:status
        //         },
        //         beforeSend:function(){
        //             $(".loader").show();
        //         },
        //         success:function(resp){
        //             $(".listBody").html(resp);
        //             $(".loader").hide();
        //         }
        //     });
        // });


        
    });
</script>
@endsection