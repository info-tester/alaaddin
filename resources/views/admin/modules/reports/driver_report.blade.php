@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage Orders') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.driver_report')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
{{-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> --}}
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<style type="text/css">
	#myTable1_filter{
		display: none;
	}
	table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
		right: 0em;
		content: "";
	}
	table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
		right: 0em;
		content: "";
	}
</style>

@if(Config::get('app.locale') == 'en')
<style type="text/css">
	.amount-total-cls{
		width: 20%;
		float: left;
		font-weight: 600;
		margin-right: 7px;
	}
</style>
@elseif(Config::get('app.locale') == 'ar')
<style type="text/css">
	.amount-total-cls{
		width: 20%;
		float: right;
		font-weight: 600;
		margin-left: 7px;
	}
</style>
@endif

@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<!-- ============================================================== -->
			<!-- pageheader  -->
			<!-- ============================================================== -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="page-header">
						<h2 class="pageheader-title">@lang('admin_lang.driver_report')</h2>
						<div class="page-breadcrumb">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
									<li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.driver_report')</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</div>
			@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			<div class="mail_success" style="display: none;">
				<div class="alert alert-success vd_hidden" style="display: block;">
					<a class="close" data-dismiss="alert" aria-hidden="true">
						<i class="icon-cross"></i>
					</a>
					<span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
					<strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
				</div>
			</div>
			<div class="mail_error" style="display: none;">
				<div class="alert alert-danger vd_hidden" style="display: block;">
					<a class="close" data-dismiss="alert" aria-hidden="true">
						<i class="icon-cross"></i>
					</a>
					<span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
					<strong>Error!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
				</div>
			</div>
			@if (session()->has('success'))
			<div class="alert alert-success vd_hidden" style="display: block;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
				<strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
			</div>
			@elseif ((session()->has('error')))
			<div class="alert alert-danger vd_hidden" style="display: block;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
				<strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
			</div>
			@endif
			{{-- success msg show --}}
			<div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
				<strong class="success_msg">@lang('admin_lang.suc_st_ord_ch')</strong>
			</div>
			{{-- error msg show --}}
			<div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
				<a class="close" data-dismiss="alert" aria-hidden="true">
					<i class="icon-cross"></i>
				</a>
				<span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
				<strong class="error_msg">@lang('admin_lang.err_st_ord_ch')</strong> 
			</div>
			<!-- ============================================================== -->
			<!-- end pageheader -->
			<!-- ============================================================== -->
			<div class="row">
				<!-- ============================================================== -->
				<!-- basic table  -->
				<!-- ============================================================== -->
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="card">
						<h5 class="card-header">@lang('admin_lang.driver_report')</h5>
						<div class="card-body">
							<form id="search_orders_form" method="get" action="{{route('driver.report.export')}}">
								@csrf
								<div class="row">
									<div data-column="2" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="merchant_lb" class="col-form-label">@lang('admin_lang.driver')</label>
										<select data-placeholder="Choose a Driver..." class="select slt slct driver"  tabindex="3" name="driver" id="co2_filter">
											<option value="">@lang('admin_lang.select_driver')</option>
											@if(@$drivers)
												@foreach(@$drivers as $drivers)
													<option value="{{ @$drivers->id }}">{{ @$drivers->fname." ".@$drivers->lname }}</option>
												@endforeach
											@endif
										</select>
									</div>
									<div data-column="3" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="merchant_lb_txt2" class="col-form-label">Merchant</label>
										<select data-placeholder="Choose a Merchant..." class="select slt slct seller_nm"  tabindex="3" name="seller_nm" id="col3_filter">
											<option value="">Select merchant</option>
											@if(@$merchants)
												@foreach(@$merchants as $merchant)
													<option value="{{ @$merchant->id }}">{{ @$merchant->fname." ".@$merchant->lname }}</option>
												@endforeach
											@endif
										</select>
									</div>
									<!-- <div data-column="8" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="merchant_lb_txt1" class="col-form-label">Payment method</label>
										<select data-placeholder="Choose a payment method..." class="select slt slct driver payment_method_s"  tabindex="8" name="payment_method_s" id="col8_filter">
											<option value="">Select payment method</option>
											<option value="C">Cash on delivery(COD)</option>
											<option value="O">Online</option>
										</select>
									</div> -->
									<div data-column="8" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="input-select8" class="col-form-label">Payment method</label>
										<select name="pm" class="form-control type pm select slt slct" id="col8_filter">
											<option value="">Select payment method</option>
											<option value='C'>Cash on delivery(Cash on delivery)</option>
											<option value='O'>Online</option>
										</select>
									</div>
									<div data-column="9" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="input-select" class="col-form-label">@lang('admin_lang.Status')</label>
										<select name="status" class="form-control type status select slt slct" id="col9_filter">
											<option value="">@lang('admin_lang.select_status')</option>
											<option value='N'>@lang('admin_lang.new')</option>
											<option value='OA'>@lang('admin_lang.order_accepted')</option>
											<option value='DA'>@lang('admin_lang.driver_assigned')</option>
											<option value='RP'>@lang('admin_lang.ready_for_pickup')</option>
											<option value='OP'>@lang('admin_lang.ord_picked_up')</option>
											<option value='OD'>@lang('admin_lang.ord_dlv')</option>
											<option value='OC'>@lang('admin_lang.Cancelled_1')</option>
										</select>
									</div>
									@if(@auth()->guard('admin')->user()->type == 'A')
									<div data-column="7" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="input-select" class="col-form-label">@lang('admin_lang.order_type')</label>
										<select id="col7_filter" name="o_t" class="form-control o_t">
											<option value="">@lang('admin_lang.slt_order_type')</option>
											<option value='I'>@lang('admin_lang.internal_1')</option>
											<option value='E'>@lang('admin_lang.external_1')</option>
										</select>
									</div>
									<div data-column="4" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 show_external_type"  style="display: none">
										<label for="input-select" class="col-form-label">@lang('merchant_lang.external_order_type')</label>
										<select id="col4_filter" name="external_order_type" class="form-control external_order_type">
											<option value='A'>@lang('merchant_lang.all_external')</option>
											<option value='S'>@lang('merchant_lang.website')</option>
											<option value='L'>@lang('merchant_lang.link')</option>
										</select>
									</div>
									@elseif(@auth()->guard('admin')->user()->type == 'S')
										@if(in_array(6, @auth()->guard('admin')->user()->roles->pluck('menu_id')->toArray()) && in_array(13, @auth()->guard('admin')->user()->roles->pluck('menu_id')->toArray()))
										<div data-column="7" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
											<label for="input-select" class="col-form-label">@lang('admin_lang.order_type')</label>
											<select id="col7_filter" name="o_t" class="form-control o_t">
												<option value="">@lang('admin_lang.slt_order_type')</option>
												<option value='I'>@lang('admin_lang.internal_1')</option>
												<option value='E'>@lang('admin_lang.external_1')</option>
											</select>
										</div>
										<div data-column="4" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 show_external_type"  style="display: none">
											<label for="input-select" class="col-form-label">@lang('merchant_lang.external_order_type')</label>
											<select id="col4_filter" name="external_order_type" class="form-control external_order_type">
												<option value='A'>@lang('merchant_lang.all_external')</option>
												<option value='S'>@lang('merchant_lang.website')</option>
												<option value='L'>@lang('merchant_lang.link')</option>
											</select>
										</div>
										@elseif(in_array(6, @auth()->guard('admin')->user()->roles->pluck('menu_id')->toArray()))
										<div data-column="7" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
											<label for="input-select" class="col-form-label">@lang('admin_lang.order_type')</label>
											<select id="col7_filter" name="o_t" class="form-control o_t">
												<option selected="" value='I'>@lang('admin_lang.internal_1')</option>
											</select>
										</div>
										@elseif(in_array(13, @auth()->guard('admin')->user()->roles->pluck('menu_id')->toArray()))
										<div data-column="7" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
											<label for="input-select" class="col-form-label">@lang('admin_lang.order_type')</label>
											<select id="col7_filter" name="o_t" class="form-control o_t">
												<option selected value='E'>@lang('admin_lang.external_1')</option>
											</select>
										</div>
										<div data-column="4" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 show_external_type" id="" style="display: none">
											<label for="input-select" class="col-form-label">@lang('merchant_lang.external_order_type')</label>
											<select id="col4_filter" name="external_order_type" class="form-control external_order_type">
												<option value='A'>@lang('merchant_lang.all_external')</option>
												<option value='S'>@lang('merchant_lang.website')</option>
												<option value='L'>@lang('merchant_lang.link')</option>
											</select>
										</div>
										@endif
									@endif
									<div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="to_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
										<input type="text" class="form-control datepicker from_date" name="from_date" placeholder="@lang('admin_lang_static.from_date')" value="" readonly="" id="col1_filter_from">
									</div>
									<div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
										<label for="to_date" class="col-form-label">@lang('admin_lang_static.to_date')</label>
										<input type="text" class="form-control datepicker to_date" name="to_date" placeholder="@lang('admin_lang_static.to_date')" value="" readonly="" id="col1_filter_to">
									</div>
									<div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
										<label for="search_orders" class="col-form-label">&nbsp;</label>
										<a href="javascript:void(0)" id="search_orders" class="btn btn-primary ">@lang('admin_lang.Search')</a>

										<input type="reset" value="Reset Search" class="btn btn-default reset_search">
										<button type="submit" id="search_orders" class="btn btn-primary ">@lang('admin_lang.export')</button>
									</div>
								</div>
							</form>
							<div class="table-responsive listBody">
								<p class="amount-total-cls">@lang('admin_lang.cod_shipping_cost'): <span id="showCod"></span></p>
								<p class="amount-total-cls" style="width: 15%">@lang('admin_lang.cod_sub_total') : <span id="showCodSub"></span></p>
								<p class="amount-total-cls">@lang('admin_lang.online_shipping_cost') : <span id="showOnline"></span></p>
								<p class="amount-total-cls">@lang('admin_lang.online_subtotal') : <span id="showOnlineSub"></span></p>
								<p class="amount-total-cls">@lang('admin_lang.total_order_delivered') : <span id="showTotalDel"></span></p>
								<div class="row" style="margin-bottom: 20px;width:100%">
									<div class="col-md-12">
										<button class="btn btn-primary" type="button" id="checking_merchant" style="margin-bottom: 5px"> Quick Checking Merchant</button>
										<button class="btn btn-primary" type="button" id="checking_driver" style="margin-bottom: 5px"> Quick Checking Driver</span></button>
									</div>
								</div>
								<table class="table table-striped table-bordered first" id="myTable1">
									<thead>
										<tr>
											<th>@lang('admin_lang.order_no')</th>
											<th>@lang('admin_lang.date')</th>
											<th>@lang('admin_lang.driver')</th>
											<th>@lang('admin_lang.seller')</th>
											<th>@lang('admin_lang.shipping_cost') (@lang('admin_lang.in_kwd'))</th>
											<th>@lang('admin_lang.sub_total') (@lang('admin_lang.in_kwd'))</th>
											<th>@lang('admin_lang.order_total') (@lang('admin_lang.in_kwd'))</th>
											<th>@lang('admin_lang.Type')</th>
											<th>@lang('admin_lang.payment_method')</th>
											<th>@lang('admin_lang.status')</th>
											<th>Last status date</th>
										</tr>
									</thead>
									{{-- <tbody> --}}
									{{-- </tbody>--}}
									<tfoot>
										<tr>
											<th>@lang('admin_lang.order_no')</th>
											<th>@lang('admin_lang.date')</th>
											<th>@lang('admin_lang.driver')</th>
											<th>@lang('admin_lang.seller')</th>
											<th>@lang('admin_lang.shipping_cost') (@lang('admin_lang.in_kwd'))</th>
											<th>@lang('admin_lang.sub_total') (@lang('admin_lang.in_kwd'))</th>
											<th>@lang('admin_lang.order_total') (@lang('admin_lang.in_kwd'))</th>
											<th>@lang('admin_lang.Type')</th>
											<th>@lang('admin_lang.payment_method')</th>
											<th>@lang('admin_lang.status')</th>
											<th>Last status date</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end basic table  -->
				<!-- ============================================================== -->
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- footer -->
		<!-- ============================================================== -->
		@include('admin.includes.footer')
		<!-- ============================================================== -->
		<!-- end footer -->
		<!-- ============================================================== -->
		<div class="loader" style="display: none;">
			<img src="{{url('public/loader.gif')}}">
		</div>
	</div>
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('body').on('click','#checking_merchant',function(){
			var date_from = $('.from_date').val();
			var date_to = $('.to_date').val();
			
			var obj = {
						date_from: date_from,
						date_to: date_to,
					};
			var token = btoa(JSON.stringify(obj));
			window.location.href="{{url('admin/export-quick-merchant-report')}}"+"/"+token;

		})
		$('body').on('click','#checking_driver',function(){
			var date_from = $('.from_date').val();
			var date_to = $('.to_date').val();
			
			var obj = {
						date_from: date_from,
						date_to: date_to,
					};
			var token = btoa(JSON.stringify(obj));
			window.location.href="{{url('admin/export-quick-driver-report')}}"+"/"+token;

		})
	})
</script>
<script type="text/javascript">

	$( function() {
		$(".datepicker").datepicker({dateFormat: "yy-mm-dd",
			defaultDate: new Date(),
			maxDate: new Date(),
			changeMonth: true,
			changeYear: true,
			yearRange: '-100:+0'
		});         	
	})
	$(document).ready(function () {
		$('body').on('click', '.reset_search', function() {
			$('#myTable1').DataTable().search('').columns().search('').draw();
		});
	// $("#myTable1_filter").style("display","none");
	function filterColumn ( i ) {
        // console.log(i, $('#col'+i+'_filter').val())
        $('#myTable1').DataTable().column( i ).search(
        	$('#col'+i+'_filter').val(),
        	).draw();
    }

    function getCustomerName(data, type, full, meta){
		if(data != null){
			var fname = '';
			var lname = '';
			if(data.fname != null){
				fname = data.fname;
			}else{
				fname = '';
			}
			if(data.lname != null){
				lname = data.lname;
			}else{
				lname = '';
			}
			return fname+' '+lname ;
		}else{
			return '--';
		}
		
    }


    function getStatus(data, type, full, meta) {
    	if(data == 'I'){
			// return '@lang('admin_lang.initiated_1')'
			return "<span class='status_"+full.id+"'>"+"Incomplete"+"</span>"
		}
		else if(data == 'N'){
			// return '@lang('admin_lang.new')'
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.new')"+"</span>"
		}
		else if(data == 'OA'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"Order accepted"+"</span>"
		}
		else if(data == 'DA'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.driver_assigned')"+"</span>"
		}
		else if(data == 'RP'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.ready_pick_up')"+"</span>"
		}
		else if(data == 'OP'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.ord_picked_up')"+"</span>"
		}
		else if(data == 'OD'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.order_delivered')"+"</span>"
		}else if(data == 'OC'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.order_cancel')"+"</span>"
		}else if(data == 'PP'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.partial_pickup')"+"</span>"
		}else if(data == 'PC'){
			// return ''
			return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.partial_pickup_completed')"+"</span>"
		}
		// else{
		// 	// return '@lang('admin_lang.Cancelled_1')'
		// 	return "<span class='status_"+full.id+"'>"+""+"</span>"
		// }
	}

	function getPaymentMethod(data, type, full, meta){
		if(data == 'C'){
			return '@lang('admin_lang.cod')'
		} else if(data == 'O'){
			return '@lang('admin_lang.online')'
		} else {
			return ''
		}
	}
	function getOrderType(data, type, full, meta){
		if(data == 'I'){
			return "@lang('admin_lang.internal_1')"
		}
		else if(data == 'E'){
			return '@lang('admin_lang.external_1')'
		}
	}

	function getSubtotal(data, type, full, meta) {
		//return (full.order_master.subtotal - full.order_master.total_discount).toFixed(3)
		if(full.order_master.total_discount != null){
			var total_discount = parseFloat(full.order_master.total_discount);
		}else{
			var total_discount = parseFloat(0.000);
		}
		var subtotal = parseFloat(full.order_master.subtotal);
		var loyalty_amount = parseFloat(full.order_master.loyalty_amount);
		if(full.order_master.coupon_id != null){
			if(full.order_master.coupon_applied_on != null){
				var applied_on = full.order_master.coupon_applied_on;
			}else{
				if(full.order_master.get_coupon != null){
					var applied_on = full.order_master.get_coupon.applied_on;
				}else{
					var applied_on = '';
				}
				
			}
			if(applied_on == 'SUB'){
				subtotal = subtotal - parseFloat(full.order_master.coupon_discount);
				subtotal = parseFloat(subtotal);
			}
		}
		//console.log(subtotal)
		return (subtotal - total_discount - loyalty_amount).toFixed(3)
	}

	$('#myTable1').DataTable({
		stateSave: true,
		"order": [
		[0, "desc"]
		],
		"stateLoadParams": function (settings, data) {
            $('#co2_filter').val(data.search.driver);
            $('#col3_filter').val(data.search.seller_nm);
            $('#col7_filter').val(data.search.o_t);
            $('#col8_filter').val(data.search.pm);
            $('#col9_filter').val(data.search.status);
            $('#col1_filter_from').val(data.search.from_date);
            $('#col1_filter_to').val(data.search.to_date);
        },
        stateSaveParams: function (settings, data) {
            data.search.driver = $('#co2_filter').val();
            data.search.seller_nm = $('#col3_filter').val();
            data.search.pm = $('#col8_filter').val();
			data.search.o_t = $('#col7_filter').val();
            data.search.status = $('#col9_filter').val();
            data.search.from_date = $('#col1_filter_from').val();
            data.search.to_date = $('#col1_filter_to').val();
        },
        "processing": true,
        "serverSide": true,
        'serverMethod': 'post',
        "ajax": {
        	"url": "{{ route('admin.driver.report.datatable') }}",
        	"data": function (d) {
				d.myKey = "myValue";
				d._token = "{{ @csrf_token() }}";
			},
			"dataSrc": function(response) {
				var totalOnlineShpPrc = parseFloat(response.totalOnlineShpPrc);
				var totalCodShpPrc = parseFloat(response.totalCodShpPrc);
				var totalCodSubtotal = parseFloat(response.totalCodSubtotal);
				var totalOnlineSubtotal = parseFloat(response.totalOnlineSubtotal);
				$('#showOnline').html(totalOnlineShpPrc.toFixed(3)+' KWD');
				$('#showCod').html(totalCodShpPrc.toFixed(3)+' KWD');
				$('#showCodSub').html(totalCodSubtotal.toFixed(3)+' KWD');
				$('#showOnlineSub').html(totalOnlineSubtotal.toFixed(3)+' KWD');
				$('#showTotalDel').html(response.totalOrderDelivered);
				return response.aaData
			}
		},
		'columns': [
		{
			data: 'order_master.order_no'
		},
		{
			data: 'order_master.created_at'
		},
		{
			data: 'driver_details',
			render: getCustomerName
		},
		{
			data: 'seller_details',
			render: getCustomerName
		},
		{
			data: 'order_master.shipping_price'
		},
		{
			data: "order_master.subtotal",
			render: getSubtotal
		},
		{
			data: "order_master.order_total"
		},
		{
			data: 'order_master.order_type',
			render: getOrderType
		},
		{
			data: 'order_master.payment_method',
			render: getPaymentMethod
		},
		{
			data: 'status',
			render: getStatus
		},
		{
			data: 'last_status_date',
			render: function(data, id, full ) {
				if(full.order_master.last_status_date != null) {
					return full.order_master.last_status_date
				} else {
					return 'Not updated'
				}
			}
		}
		]
	});
	$('.driver').on('change', function () {
		$('#myTable1').DataTable().column( 2 ).search(
			$('#co2_filter').val(),
			).draw();
	});
	$('.status').on('change', function () {
		$('#myTable1').DataTable().column( 9 ).search(
			$('#col9_filter').val(),
			).draw();
	});
	$('.pm').on('change', function () {
		$('#myTable1').DataTable().column( 8 ).search(
			$('#col8_filter').val(),
			).draw();
	});
	$('.seller_nm').on('change', function () {
		$('#myTable1').DataTable().column( 3 ).search(
			$('#col3_filter').val(),
			).draw();
	});
	$('.from_date').on('change', function () {		
		if($('#col1_filter_to').val() == '' && $('#col1_filter_to').val() < $('#col1_filter_from').val()) {
			return false;
		} else {
			$('#myTable1').DataTable().column( 1 ).search(
				[$('#col1_filter_from').val(),$('#col1_filter_to').val()],
				).draw();
		}
	});
	$('.to_date').on('change', function () {		
		if($('#col1_filter_from').val() == '' && $('#col1_filter_from').val() > $('#col1_filter_to').val()) {
			return false;
		} else {
			$('#myTable1').DataTable().column( 1 ).search(
				[$('#col1_filter_from').val(),$('#col1_filter_to').val()],
				).draw();
		}
	});
	$('.o_t').on( 'change', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('.external_order_type').on( 'keyup click', function () {
		filterColumn( $(this).parents('div').data('column') );
	});
	$('#col7_filter').on('change',function(){
		var val = $(this).val();
		if(val == 'E'){
			$('.show_external_type').show()
		}else{
			$('.show_external_type').hide()
		}
	})
	
});
</script>
{{-- jayatri ajax action buttons work --}}
<script type="text/javascript">
	$(document).ready(function(){
		$('.slct').chosen();
		$(document).on("click",".order_mstr_status_change",function(e){
			var id = $(e.currentTarget).attr("data-id"),
			ch_status = $(e.currentTarget).attr("data-changeStatus");
			if(confirm('@lang('admin_lang.do_u_wnt_chng')'+" "+ch_status+' ? ')){
				$.ajax({
					url:"{{ route('admin.update.status.order.master') }}",
					type:"GET",
					data:{
						id:id
					},
					success:function(resp){
						if(resp != 0){
							var status = JSON.stringify(resp.status),
							st = "";
							if(resp.status == 'OA'){
								st = "Order Accepted";
								$(".ord_"+id).hide();
							}
							// if(resp.status == 'P'){
							// 	st = "Paid";

							// }
							$(".status_"+id).html(st);
							$(".success_msg_div").show();
							$(".error_msg_div").hide();
							$(".success_msg").html('@lang('admin_lang.success_st_ch_ord')');

							$(".status_"+id).html(st);
						}else{
							$(".error_msg_div").show();
							$(".success_msg_div").hide();
							$(".error_msg").html('@lang('admin_lang.err_st_or_ch')');
						}
					}
				});
			}
			// }
		});
		$(document).on("click",".order_mstr_cancel",function(e){
			var id = $(e.currentTarget).attr("data-id"),
			ord_no = $(e.currentTarget).attr("data-ordno");
			if(confirm('Do you want to cancel this order having order number '+ ord_no+'?')){
				$.ajax({
					url:"{{ route('admin.order.cancel') }}",
					type:"GET",
					data:{
						id:id
					},
					success:function(resp){
						if(resp == 1){
							$(".status_"+id).html("Cancelled");
							$(".success_msg_div").show();
							$(".error_msg_div").hide();
							$(".success_msg").html('Success!Order has been cancelled successfully!');
							// $(".status_"+id).text("Cancelled");
							$(".cn_or_"+id).hide();
							$(".ord_"+id).hide();
						}else{
							$(".error_msg_div").show();
							$(".success_msg_div").hide();
							$(".error_msg").html('@lang('admin_lang.err_nw_q')');

						}
					}
				});
			}
			// }
		});
		// $('#myTable1').dataTable({
		//   order: [[1, 'desc']]
	 //  	});
	});
</script>
@endsection