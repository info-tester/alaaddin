@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage Cities') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }}| @lang('admin_lang.admin') | @lang('admin_lang.list_of_countries')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.country_management') </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link"> @lang('admin_lang.countries') </a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.manage_countries')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="mail_success" style="display: none;">
                <div class="alert alert-success vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
                </div>
            </div>
            <div class="mail_error" style="display: none;">
                <div class="alert alert-danger vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>@lang('admin_lang.error')!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
                </div>
            </div>
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden session_success_div" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden session_error_div" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong class="success_msg"></strong> 
            </div>
            <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong class="error_msg"></strong> 
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <!-- ============================================================== -->
                <!-- basic table  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">@lang('admin_lang.manage_countries') <a class="adbtn btn btn-primary" href="{{ route('admin.add.country') }}"><i class="fas fa-plus"></i> @lang('admin_lang.Add')</a></h5>
                        <div class="card-body">

                            {{-- <form id="search_city_form">
                                @csrf
                                <div class="row">

                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="inputText3" class="col-form-label">@lang('admin_lang.Keywords') </label>
                                        <input id="keyword" name="keyword" type="text" class="form-control" placeholder="@lang('admin_lang.Keywords')" value="{{ @$key['keyword'] }}">
                                    </div>
                                    
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="input-select" class="col-form-label">@lang('admin_lang.status')</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="">@lang('admin_lang.select_status')</option>
                                            <option value="A">@lang('admin_lang.Active')</option>
                                            <option value="I">@lang('admin_lang.Inactive')</option>
                                            <option value="U">@lang('admin_lang.un_verified')</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <a href="javascript:void(0)" id="search_customers" class="btn btn-primary fstbtncls">@lang('admin_lang.search')</a>
                                    </div>
                                    
                                </div>
                            </form>
 --}}
                            <div class="table-responsive listBody">
                                <table class="table table-striped table-bordered first" id="myTable">
                                    <thead>
                                        <tr>
                                            <th>@lang('admin_lang.slno')</th>
                                            <th>@lang('admin_lang.country_code_list')</th>
                                            <th>@lang('admin_lang.nm_of_the_country')</th>
                                            <th>@lang('admin_lang.Status')</th>
                                            <th>@lang('admin_lang.Action')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(@$country)
                                            @foreach(@$country as $key=>$c)
                                                <!-- @php
                                                if(Config::get('app.locale') == 'en') {
                                                    $city = $c->name;
                                                } elseif(Config::get('app.locale') == 'ar') {
                                                    $city = $c->name_ar;
                                                }
                                                @endphp -->
                                                <tr class="tr_{{ @$c->id }}">
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ @$c->countryDetailsBylanguage->country_code}}</td>
                                                    <td>{{ @$c->countryDetailsBylanguage->name}}</td>
                                                    <td class="staus_{{@$c->id}}">
                                                        @if(@$c->status == 'A')
                                                        @lang('admin_lang.Active')
                                                        @elseif(@$c->status == 'I')
                                                        @lang('admin_lang.Inactive')
                                                        @endif
                                                    </td>
                                                    <td>
                                                        
                                                        <a href="{{ route('admin.edit.country',@$c->id) }}"><i class="fas fa-edit" title="@lang('admin_lang.Edit')"></i></a>

                                                        @if(@$c->id != 134)
                                                        <!-- fas fa-ban -->
                                                        @if(@$c->status == 'A')
                                                        <a class="show_hide_country" href="javascript:void(0)" data-id="{{ @$c->id }}" data-status="{{ @$c->status }}" title="@lang('admin_lang.hide_country')"><i class="icon_change_{{ @$c->id }} fas fa-ban"></i></a>
                                                        @elseif(@$c->status == 'I')
                                                        <a class="show_hide_country" href="javascript:void(0)" data-id="{{ @$c->id }}" data-status="{{ @$c->status }}" title="@lang('admin_lang.show_country')"><i class="icon_change_{{ @$c->id }} fas fa-check"></i></a>
                                                        @endif
                                                        <a class="delete_city" href="{{ route('admin.delete.country',@$c->id) }}" data-id="{{ @$c->id }}" title="@lang('admin_lang.delete_country')"><i class=" fas fa-trash"></i></a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>@lang('admin_lang.slno')</th>
                                            <th>@lang('admin_lang.country_code_list')</th>
                                            <th>@lang('admin_lang.nm_of_the_country')</th>
                                            <th>@lang('admin_lang.Status')</th>
                                            <th>@lang('admin_lang.Action')</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end basic table  -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
        <div class="loader" style="display: none;">
            <img src="{{url('public/loader.gif')}}">
        </div>
       <!--  <div class="modal fade" id="sendMailNotificationModalCenter" data-id="" data-emailid="">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            @lang('admin_lang.send_email_notification')
                        </h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="basic-form">
                            
                            <form id="sendEmailForm" method="post" action="">
                                @csrf
                                <div class="form-row">
                                    {{-- <div class="form-group col-md-12"> --}}
                                        {{-- <label id="title">Email-id</label> --}}
                                        <input id="customer_email" name="customer_email" type="hidden" class="form-control" placeholder="" readonly>

                                        <input id="customer_name" name="customer_name" type="hidden" class="form-control" placeholder="" readonly>

                                        <input id="customer_id" name="customer_id" type="hidden" class="form-control" placeholder="" readonly>
                                        {{-- <span class="error_title" style="color: red;"></span> --}}
                                    {{-- </div> --}}
                                    <div class="form-group col-md-12">
                                        <label id="titleLabel">@lang('admin_lang.title')</label>
                                        <input id="title" name="title" type="text" class="form-control required" placeholder='@lang('admin_lang.title')'>
                                        <span class="error_title" style="color: red;"></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label id="messageLabel">@lang('admin_lang.msg')</label>
                                        <textarea id="message" name="message" class="form-control required" rows="5"></textarea>
                                        <span class="error_message" style="color: red;"></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label></label>
                                        <button type="button" id="sendEmailButton" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                                    </div>
                                </form>
                                
                                
                            </div>
                        </div>
                    </div> 
                </div>
            </div>

        </div> -->
        @endsection
        @section('scripts')
        {{-- @include('admin.includes.scripts') --}}

        @if(Config::get('app.locale') == 'en')
        @include('admin.includes.scripts')
        @elseif(Config::get('app.locale') == 'ar')
        @include('admin.includes.arabic_scripts')
        @endif
        <script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                // $(document).on("click",".delete_city",function(e){
                //     var id = $(e.currentTarget).attr("data-id");
            
                //     if(confirm("Do you want to delete this city ?")){
                //         $.ajax({
                //             url:"{{ route('admin.delete.city.details') }}",
                //             type:"GET",
                //             data:{
                //                 id:id
                //             },
                //             success:function(resp){
                //                 if(resp == 1){
                //                     $(".success_msg").html('Success!City is deleted successfully!');
                //                     $(".success_msg_div").show();
                //                     $(".error_msg_div").hide();
                //                     $(".tr_"+id).hide();
                //                 }else{
                //                     $(".error_msg_div").show();
                //                     $(".success_msg_div").hide();
                //                     $(".error_msg").html('Error!Unauthorized access!');
                //                 }
                //                 $(".session_success_div").hide();
                //                 $(".session_error_div").hide();
                //             }
                //         });
                //     }
                // });    
                $(document).on("click",".show_hide_country",function(e){
                    var id = $(e.currentTarget).attr("data-id"),
                    status = $(e.currentTarget).attr("data-status"),
                    icon_status = $(e.currentTarget).attr("data-status"),
                    obj = $(e.currentTarget);
                    sh = "";
                    if(status == 'A'){
                        status = "@lang('admin_lang.Inactive')";
                        sh = "@lang('admin_lang.hide')";
                    }else if(status == 'I'){
                        status = "@lang('admin_lang.Active')";
                         sh = "@lang('admin_lang.show')";
                    }
                    if(confirm("@lang('admin_lang.do_u_want_to') "+sh+" @lang('admin_lang.this_country_question')")){
                        $.ajax({
                            url:"{{ route('admin.show.hide.country') }}",
                            type:"GET",
                            data:{
                                id:id
                            },
                            success:function(resp){
                                if(resp == 1){
                                    $(".success_msg").html("@lang('admin_lang.suc_st_country_change')"+sh);
                                    $(".success_msg_div").show();
                                    $(".error_msg_div").hide();
                                    $(".staus_"+id).text(status);
                                    if(icon_status == 'A'){
                                        // alert("Active icon");
                                        obj.attr("data-status",'I');
                                        $(".icon_change_"+id).removeClass("fa-ban");
                                        $(".icon_change_"+id).addClass("fa-check");

                                    }else if(icon_status == 'I'){
                                        obj.attr("data-status",'A');
                                        $(".icon_change_"+id).removeClass("fa-check");
                                        $(".icon_change_"+id).addClass("fa-ban");
                                    }
                                }else{
                                    $(".error_msg_div").show();
                                    $(".success_msg_div").hide();
                                    $(".error_msg").html("@lang('admin_lang.err_unau_acc_country_nt_change')");
                                }
                                $(".session_success_div").hide();
                                $(".session_error_div").hide();
                            }
                        });
                    }
                });
            });
        </script>
        @endsection