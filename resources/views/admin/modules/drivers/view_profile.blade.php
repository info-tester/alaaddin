@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | View Drivers Profile') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.view_drivers_profile')
@endsection

@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="influence-profile">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h3 class="mb-2">@lang('admin_lang.driver_v_pro')</h3>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.driver_v_pro')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- end pageheader -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- content -->
            <!-- ============================================================== -->
            <div class="row">
                <!-- ============================================================== -->
                <!-- profile -->
                <!-- ============================================================== -->
                <div class="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12">
                    <!-- ============================================================== -->
                    <!-- card profile -->
                    <!-- ============================================================== -->
                    <div class="card">
                        <div class="card-body">
                            <div class="user-avatar text-center d-block">
                                @if(@$driver->image)
                                @php
                                $image_path = 'storage/app/public/driver/profile_pics/'.@$driver->image; 
                                @endphp
                                @if(file_exists(@$image_path))
                                <div class="uplodpic">
                                    <li>
                                        <img src="{{ URL::to('storage/app/public/driver/profile_pics/'.@$driver->image) }}" style="width: 100px;height: 100px;" alt="User Avatar" class="rounded-circle user-avatar-xxl">
                                    </li>
                                </div>
                                @endif
                                @endif
                                {{-- <img src="assets/images/avatar-1.jpg" > --}}
                            </div>
                            <div class="text-center">
                                <h2 class="font-24 mb-0">
                                    {{ @$driver->fname." ".@$driver->lname }}
                                </h2>
                                <p>{{ @$driver->email }}</p>
                            </div>
                        </div>
                        <div class="card-body border-top">
                            <h3 class="font-16">@lang('admin_lang.cnt_info')</h3>
                            <div class="">
                                <ul class="list-unstyled mb-0">
                                    <li class="mb-2"><i class="fas fa-fw fa-envelope mr-2"></i>{{ @$driver->email }}</li>
                                    <li class="mb-0"><i class="fas fa-fw fa-phone mr-2"></i>{{ @$driver->phone }}</li>
                                </ul>
                            </div>
                        </div>
                        @if(@$driver->avg_review != 0.000)
                        <div class="card-body border-top">
                            <h3 class="font-16">@lang('admin_lang.ratings')</h3>
                            <h1 class="mb-0">{{ @$driver->avg_review }}</h1>
                            <div class="rating-star">
                                <i class="fa fa-fw fa-star"></i>
                                <i class="fa fa-fw fa-star"></i>
                                <i class="fa fa-fw fa-star"></i>
                                <i class="fa fa-fw fa-star"></i>
                                <i class="fa fa-fw fa-star"></i>
                                <p class="d-inline-block text-dark">{{ @$driver->total_no_reviews }} @lang('admin_lang.reviews') </p>
                            </div>
                        </div>
                        @endif
                        <!--<div class="card-body border-top">
                            <h3 class="font-16">Social Channels</h3>
                            <div class="">
                                <ul class="mb-0 list-unstyled">
                                <li class="mb-1"><a href="#"><i class="fab fa-fw fa-facebook-square mr-1 facebook-color"></i>fb.me/michael99</a></li>
                                <li class="mb-1"><a href="#"><i class="fab fa-fw fa-twitter-square mr-1 twitter-color"></i>twitter.com/michael99</a></li>
                                
                            </ul>
                            </div>
                            </div>-->
                    </div>
                    <!-- ============================================================== -->
                    <!-- end card profile -->
                    <!-- ============================================================== -->
                </div>
                <!-- ============================================================== -->
                <!-- end profile -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- campaign data -->
                <!-- ============================================================== -->
                <div class="col-xl-9 col-lg-9 col-md-7 col-sm-12 col-12">
                    <!-- ============================================================== -->
                    <!-- campaign tab one -->
                    <!-- ============================================================== -->
                    <div class="influence-profile-content pills-regular">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-campaign" role="tabpanel" aria-labelledby="pills-campaign-tab">
                                <div class="section-block">
                                    <h3 class="section-title">@lang('admin_lang.abt')
                                        <a class="adbtn btn btn-primary" href="{{ route('admin.list.driver') }}"><i class="fas fa-less-than"></i> @lang('admin_lang.back')</a>
                                    </h3>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="media Drivers Group-profile-data d-flex align-items-center p-2">
                                                    <div class="media-body ">
                                                        <div class="Drivers Group-profile-data">
                                                        </div>
                                                        <h4 class="fultxt" style="margin-bottom:10px;">
                                                            <br/>
                                                            @lang('admin_lang.total_no_of_orders')      : {{ @$total_no }}<br/>
                                                            @lang('admin_lang.sub_total_orders')          : {{ @$total_sum }}<br/>
                                                            @lang('admin_lang.order_total')                 : {{ @$total_cost }}<br/>
                                                            <br/>
                                                            @lang('admin_lang.delivered_order_list') : 
                                                        </h4>
                                                        <div class="table-responsive">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered first">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>@lang('admin_lang.slno_order')</th>
                                                                            <th>@lang('admin_lang.date_1')</th>
                                                                            <th>@lang('admin_lang.order_total')</th>
                                                                            <th>@lang('admin_lang.payment_method')</th>
                                                                            <th>@lang('admin_lang.status')</th>
                                                                            <th>@lang('admin_lang.Action')</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @if(@$orders)
                                                                        @foreach(@$orders as $order)
                                                                        @if($order->status != 'D')
                                                                        <tr>
                                                                        <td>
                                                                            {{ @$order->order_no }}
                                                                        </td>
                                                                        <td>{{ @$order->created_at }}</td>
                                                                        
                                                                        <td>{{ @$order->order_total }}</td>
                                                                        <td>
                                                                            @if(@$order->payment_method == 'O')
                                                                            Online
                                                                            @elseif(@$order->payment_method == 'C')
                                                                            Cash on delivery
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            @if(@$order->status == 'I')
                                                                            Incomplete
                                                                            @elseif(@$order->status == 'N')
                                                                            New
                                                                            @elseif(@$order->status == 'OA')
                                                                            @lang('admin_lang.order_accepted')
                                                                            @elseif(@$order->status == 'DA')
                                                                            @lang('admin_lang.driver_assigned')
                                                                            @elseif(@$order->status == 'RP')
                                                                            @lang('admin_lang.ready_for_pickup')
                                                                            @elseif(@$order->status == 'OP')
                                                                            Picked up
                                                                            @elseif(@$order->status == 'OD')
                                                                            @lang('admin_lang.delivered_1')
                                                                            @elseif(@$order->status == 'OC')
                                                                            @lang('admin_lang.Cancelled_1')
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            <a href="{{ route('admin.view.order',@$order->id) }}"><i class=" fas fa-eye" title="View"></i></a>
                                                @if(@$order->order_type == 'E')

                                                <a href="{{ route('admin.edit.order.details',@$order->id) }}"><i class=" fas fa-edit" title="Edit"></i></a>

                                                @endif
                                                {{-- cancel order --}}
                                                @if(@$order->status == 'I' || @$order->status == 'P' || @$order->status == 'N')
                                                <a href="{{ route('admin.cancel.order',@$order->id) }}"><i class="far fa-times-circle" title="Cancel"></i></a> 
                                                @endif
                                                {{-- status change of order master--}}
                                                @if(@$order->order_type == 'E')
                                                    @if(@$order->status == 'N')
                                                        {{-- <a href="{{ route('admin.status.order.master',@$order->id) }}" onclick="return confirm('Do you want to change status to order initiated ? ');"><i class="fas fa-check" title="Order Initiated"></i></a> --}}
                                                        <a href="javascript:void(0)"  class="order_mstr_status_change" data-id="{{ @$order->id }}" data-changeStatus="Initiated"><i class="fas fa-check" title="Order Initiated"></i></a>

                                                    <!-- @elseif(@$order->status == 'I') 
                                                    <a href="javascript:void(0)"  class="order_mstr_status_change" data-id="{{ @$order->id }}" data-changeStatus="Paid"><i class="fas fa-check" title="Order Paid" ></i></a> -->
                                                    {{-- <a href="{{ route('admin.status.order.master',@$order->id) }}" onclick="return confirm('Do you want to change status to order paid ? ');"><i class="fas fa-check" title="Order Paid" ></i></a> --}}
                                                    @endif
                                                @else
                                                    <!-- @if(@$order->status == 'I')
                                                    <a href="{{ route('admin.status.order.master',@$order->id) }}"  data-changeStatus="Paid"><i class="fas fa-money" title="Order Paid"></i></a>
                                                    @endif -->
                                                @endif
                                                                        </td>
                                                                        </tr>
                                                                        @endif
                                                                        @endforeach
                                                                       @endif
                                                                    </tbody>
                                                                    <tfoot>
                                                                        <tr>
                                                                            <th>@lang('admin_lang.slno_order')</th>
                                                                            <th>@lang('admin_lang.date_1')</th>
                                                                            
                                                                            <th>@lang('admin_lang.order_total')</th>
                                                                            <th>@lang('admin_lang.payment_method')</th>
                                                                            <th>@lang('admin_lang.status')</th>
                                                                            <th>@lang('admin_lang.Action')</th>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end campaign tab one -->
                    <!-- ============================================================== -->
                </div>
                <!-- ============================================================== -->
                <!-- end campaign data -->
                <!-- ============================================================== -->
            </div>
        </div>
    </div>
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
@endsection