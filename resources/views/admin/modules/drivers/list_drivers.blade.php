@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage Driver') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.mng_drivers')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<style type="text/css">
    #drivers_filter{
        display: none;
    }

    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.driver_management') </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link"> 
                                        @lang('admin_lang.drivers') 
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.driver_management')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="mail_success" style="display: none;">
                <div class="alert alert-success vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
                </div>
            </div>
            <div class="mail_error" style="display: none;">
                <div class="alert alert-danger vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>@lang('admin_lang.error')!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
                </div>
            </div>
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['code']['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong class="success_msg"></strong>  
            </div>
            <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong class="error_msg"></strong>  
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <!-- ============================================================== -->
                <!-- basic table  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">@lang('admin_lang.driver_management') <a class="adbtn btn btn-primary" href="{{ route('admin.add.driver') }}"><i class="fas fa-plus"></i> @lang('admin_lang.add')</a></h5>
                        <div class="card-body">
                            <form id="search_customers_form" method="post">
                                @csrf
                                <div class="row">
                                    <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="inputText3" class="col-form-label">@lang('admin_lang.Keywords') </label>
                                        <input id="col1_filter" name="keyword" type="text" class="form-control keyword" placeholder="Keywords" value="{{ @$key['keyword'] }}">
                                    </div>
                                    <div data-column="2" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="input-select" class="col-form-label">@lang('admin_lang.Status')</label>
                                        <select class="form-control status" id="col2_filter" name="status">
                                            <option value="">@lang('admin_lang.select_status')</option>
                                            <option value="A" @if(@$key['status'] == 'A') selected @endif>@lang('admin_lang.Active')</option>
                                            <option value="I" @if(@$key['status'] == 'I') selected @endif>@lang('admin_lang.Inactive')</option>
                                            <option value="U" @if(@$key['status'] == 'U') selected @endif>@lang('admin_lang.unverified')</option>
                                            {{-- <option value="AA" @if(@$key['status'] == 'AA') selected @endif>Awaiting Approval</option> --}}
                                        </select>
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <a href="javascript:void(0)" id="search_customers" class="btn btn-primary fstbtncls">@lang('admin_lang.Search')</a>

                                        <input type="reset" value="Reset Search" class="btn btn-default fstbtncls reset_search">
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive listBody">
                                <table class="table table-striped table-bordered first" id="drivers">
                                    <thead>
                                        <tr>
                                            <th>@lang('admin_lang.Name')</th>
                                            <th>@lang('admin_lang.email')</th>
                                            <th>@lang('admin_lang.phone_number')</th>
                                            {{-- <th>@lang('admin_lang.login_from')</th> --}}
                                            <th>@lang('admin_lang.avg_rating')</th>
                                            <th>@lang('admin_lang.Status')</th>
                                            <th>@lang('admin_lang.Action')</th>
                                        </tr>
                                    </thead>
                                    
                                    <tfoot>
                                        <tr>
                                            <th>@lang('admin_lang.Name')</th>
                                            <th>@lang('admin_lang.email')</th>
                                            <th>@lang('admin_lang.phone_number')</th>
                                            {{-- <th>@lang('admin_lang.login_from')</th> --}}
                                            <th>@lang('admin_lang.avg_rating')</th>
                                            <th>@lang('admin_lang.Status')</th>
                                            <th>@lang('admin_lang.Action')</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end basic table  -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
        <div class="loader" style="display: none;">
            <img src="{{url('public/loader.gif')}}">
        </div>
        <div class="loader" style="display: none;">
            <img src="{{url('public/loader.gif')}}">
        </div>
        <div class="modal fade" id="sendMailNotificationModalCenter" data-id="" data-emailid="">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            @lang('admin_lang.send_email_notification')
                        </h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="basic-form">
                            <form id="sendEmailForm" method="post" action="">
                                @csrf
                                <div class="form-row">
                                    <input id="customer_email" name="customer_email" type="hidden" class="form-control" placeholder="" readonly>
                                    <input id="customer_name" name="customer_name" type="hidden" class="form-control" placeholder="" readonly>
                                    <input id="customer_id" name="customer_id" type="hidden" class="form-control" placeholder="" readonly>
                                    <div class="form-group col-md-12">
                                        <label id="titleLabel">@lang('admin_lang.title')</label>
                                        <input id="title" name="title" type="text" class="form-control required" placeholder="@lang('admin_lang.title')">
                                        <span class="error_title" style="color: red;"></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label id="messageLabel">@lang('admin_lang.msg')</label>
                                        <textarea id="message" name="message" class="form-control required" rows="5"></textarea>
                                        <span class="error_message" style="color: red;"></span>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label></label>
                                        <button type="button" id="sendEmailButton" class="btn btn-primary popbtntp">@lang('admin_lang.send')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click', '.reset_search', function() {
            $('#drivers').DataTable().search('').columns().search('').draw();
        })

        function filterColumn ( i ) {
            $('#drivers').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }

        $('#drivers').DataTable( {
            stateSave: true,
            "order": [[0, "asc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
                $('#col2_filter').val(data.search.status)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
                data.search.status = $('#col2_filter').val()
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('admin.list.driver') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
                { 
                    data: 0,
                    render: function(data, type, full) {
                        return full.fname+' '+ full.lname
                    }
                },
                {   data: 'email' },
                {   data: 'phone' },
                {
                    render: function(data, type, full) {
                        if(full.get_rating.length != 0){
                            var total_orders = full.get_rating.length;
                            var total_rating = 0.00;
                            $.each(full.get_rating,function(i,v){
                                total_rating = total_rating + parseFloat(v.review_point) ;
                            })
                            
                            var avg_rating = total_rating / total_orders ;
                            return avg_rating.toFixed(2);
                        }else{
                            return "0.00";
                        }
                        
                    }
                },
                {   
                    data: 'status',
                    render: function(data, type, full) {
                        if(data == 'A') {
                            return '<span class="status_'+full.id+'">'+"@lang('admin_lang.Active')"+'</span>'
                        } if(data == 'I') {
                            return '<span class="status_'+full.id+'">'+"@lang('admin_lang.Inactive')"+'</span>'
                        }if(data == 'U') {
                            return '<span class="status_'+full.id+'">'+"@lang('admin_lang.unverified')"+'</span>'
                        }
                        if(data == 'D') {
                            return '<span class="status_'+full.id+'">'+"Deleted"+'</span>'
                        }
                    }
                },
                {   
                    data: 'status',
                    render: function(data, type, full) {
                        var a = '';
                        if(data == 'A') {
                            a += '<a class="block_unblock_driver" href="javascript:void(0)" data-id="'+full.id+'"><i class="fas fa-ban icon_change_'+full.id+'" title="@lang('admin_lang.block')"></i></a>'
                        }
                        if(data == 'I') {
                            a += '<a class="block_unblock_driver" href="javascript:void(0)" data-id="'+full.id+'"><i class="far fa-check-circle icon_change_'+full.id+'" title="@lang('admin_lang.unblock')"></i></a>'
                        }

                        a += ' <a href="{{ url('admin/view-driver-profile') }}/'+full.id+'"><i class="fas fa-eye" title="@lang('admin_lang.view')"></i></a>'
                        
                        a += ' <a href="{{ url('admin/driver-ratings') }}/'+full.id+'"><i class="fa fa-star" title="@lang('admin_lang.all_rating')"></i></a>'

                        a += ' <a href="{{ url('admin/edit-driver') }}/'+full.id+'"><i class="fas fa-edit" title="@lang('admin_lang.Edit')"></i></a>'
                        a += '<a href="javascript:void(0)" data-toggle="modal" class="send_email_notify" data-id="'+full.id+'" data-email="'+full.email+'" data-fname="'+full.fname+'" data-lname="'+full.lname+'" data-action="{{  url('admin/send-message-driver') }}"><i class="fas fa-paper-plane" title="@lang('admin_lang.send_email')"></i></a>'
                        a += ' <a href="javascript:void(0)" class="delete_driver" data-id="'+full.id+'"><i class=" fas fa-trash" title="@lang('admin_lang.Delete')"></i></a>'
                        return a;
                    }
                }
            ]
        });

        // change event
        $('input.keyword').on( 'keyup click blur', function () {
            filterColumn( 1 );
        });
        $('.status').on( 'change', function () {
            filterColumn( 2 );
        });

        
    
        $(document).on("click","#sendEmailButton",function(e){
    
            var title = $.trim($("#title").val()),
            id = $.trim($("#customer_id").val()),
            customer_email = $.trim($("#customer_email").val()),
            customer_name = $.trim($("#customer_name").val()),
            message = $.trim($("#message").val());
            
            if(title != "" && message != ""){
                
                $('#sendMailNotificationModalCenter').modal('hide');
                var rurl = "{{ route('admin.send.message.driver') }}";
                // $("#sendEmailForm").submit();
                $(".mail_success").css({
                            "display":"block"
                        });
            
                        $(".successMsg").text("@lang('admin_lang.mail_sent_to_dr')");
            $.ajax({
                type:"GET",
                url:rurl,
                data: {
                    id : id,
                    name : customer_name,
                    email : customer_email,
                    title : title,
                    message : message
                },
                success:function(resp){
                
                    if(resp==1){
                        $(".mail_success").css({
                            "display":"block"
                        });
            
                        $(".successMsg").text("@lang('admin_lang.msg_snt')");
            
                        $(".mail_error").css({
                            "display":"none"
                        });
                        $("#title").val("");
                        $("#message").val("");
            
                    }else{
                        $(".mail_error").css({
                            "display":"block"
                        });
            
                        $(".errorMsg").text("@lang('admin_lang.unauthorized_msg_not_sent')");
                        $(".mail_success").css({
                            "display":"none"
                        });
                        $("#title").val("");
                        $("#message").val("");
                    }
                }
            });
            
        }else{
            if(title == ""){
                $(".error_title").text("@lang('validation.title_error')");
            }
            else{
                $(".error_title").text("");
            }
            if(message == ""){
                $(".error_message").text("@lang('validation.message_error')");
            }
            else{
                $(".error_message").text("");
            }
        }
    });
    
    $(document).on("click",".send_email_notify",function(e){
        var id= $.trim($(e.currentTarget).attr("data-id")),
        email_id= $.trim($(e.currentTarget).attr("data-email")),
        name= $.trim($(e.currentTarget).attr("data-fname"));
        // action= $.trim($(e.currentTarget).attr("data-action"));
    
        $('#sendMailNotificationModalCenter').attr("data-id",id);
        $('#sendMailNotificationModalCenter').attr("data-emailid",email_id);
        $('#sendMailNotificationModalCenter').attr("data-name",name);
        // $('#sendMailNotificationModalCenter').attr("data-action",action);
        $('#sendMailNotificationModalCenter').modal('show');
        $("#customer_email").val(email_id);
        $("#customer_name").val(name);
        $("#customer_id").val(id);
        // $("#sendEmailForm").attr("action",action);
    });

    

    $(document).on("click",".block_unblock_driver",function(e){
        var id = $(e.currentTarget).attr("data-id");
        if(confirm("@lang('admin_lang.do_u_ch_driver')")){
            $.ajax({
                url:"{{ route('admin.block.unblock.driver') }}",
                type:"GET",
                data:{
                    id:id
                },
                success:function(resp){
                    if(resp != null){
                        var st="";
                        if(resp.status == 'A'){
                            st="@lang('admin_lang.Active')";
                            $(".success_msg").html("@lang('admin_lang.suc_dr_block')");
                            $(".icon_change_"+id).removeClass("far fa-check-circle");
                            // $(".icon_change_"+id).removeClass("");
                            $(".icon_change_"+id).addClass("fas fa-ban");
                            $(".icon_change_"+id).attr("title","@lang('admin_lang.block_driver')");
                            // $(".b_u_"+id).attr('data-status','I');
                            // $(e.currentTarget).attr('data-status','A');
                        }else if(resp.status == 'I'){
                            st="@lang('admin_lang.Inactive')";
                            $(".success_msg").html("@lang('admin_lang.suc_dr_block_nw')");
                            $(".icon_change_"+id).removeClass("fas fa-ban");
                            $(".icon_change_"+id).addClass("far fa-check-circle");
                            $(".icon_change_"+id).attr("title","@lang('admin_lang.unb_dr_text')");
                            // $(".b_u_"+id).attr('data-status','I');
                            // $(e.currentTarget).attr('data-status','I');
                        }
                        $(".status_"+id).html(st);
                        $(".success_msg_div").show();
                        $(".error_msg_div").hide();
                        
                        // $(".int_st_icon_"+id).hide();
                    }else{
                        $(".error_msg_div").show();
                        $(".success_msg_div").hide();
                        $(".error_msg").html("@lang('admin_lang.una_ac_st_dr_nt_ch')");
    
                    }
                }
            });
        }
        // }
    });

    $(document).on("click",".delete_driver",function(e){
        var id = $(e.currentTarget).attr("data-id");
        var obj = $(this);
        if(confirm("@lang('admin_lang.do_u_want_to_delete_dr_question')")){
            $.ajax({
                url:"{{ route('admin.delete.driver.details') }}",
                type:"GET",
                data:{
                    id:id
                },
                success:function(resp){
                    if(resp == 1){
                        $(".success_msg").html("@lang('admin_lang.suc_dr_deleted_suc')");
                        // $(".status_"+id).html(st);
                        $(".success_msg_div").show();
                        $(".error_msg_div").hide();
                        // $(".tr_"+id).hide();
                        // $(".int_st_icon_"+id).hide();
                        obj.parent().parent().hide();
                    }else{
                        $(".error_msg_div").show();
                        $(".success_msg_div").hide();
                        $(".error_msg").html("@lang('admin_lang.err_dr_ntdeleted_suc')");
                    }
                }
            });
        }
        // }
    });

   /* $("#search_customers").click(function(){
        var keyword = $.trim($("#keyword").val());
        var status = $.trim($("#status").val());
        $.ajax({
            type:"POST",
            url:"{{ route('admin.list.driver') }}",
            data:{
                _token: '{{ csrf_token() }}',
                keyword:keyword,
                status:status
            },
            beforeSend:function(){
                $(".loader").show();
            },
            success:function(resp){
                $(".listBody").html(resp);
                $(".loader").hide();
            }
        });
    });*/
       /* $("#keyword").on("keyup", function(e){
            var value = $(this).val().toLowerCase();
            $("#myTable tbody tr").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
          });
        });

        $(document).on("change","#status",function(e){
            var keyword = $.trim($("#keyword").val());
            var status = $.trim($("#status").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.driver') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    status:status
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });

        $(document).on("blur","#status",function(e){
            var keyword = $.trim($("#keyword").val());
            var status = $.trim($("#status").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.driver') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    status:status
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });

        $(document).on("blur","#keyword",function(e){
            var keyword = $.trim($("#keyword").val());
            var status = $.trim($("#status").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.driver') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    status:status
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });

        $("#search_customers").click(function(){
            // $("#search_customers_form").submit();
            var keyword = $.trim($("#keyword").val());
            var status = $.trim($("#status").val());
            $.fn.dataTable.ext.errMode = 'none';
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.driver') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    status:status
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });*/
    });
</script>
@endsection