@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage Driver') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.all_rating')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<style type="text/css">
    #drivers_filter{
        display: none;
    }

    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.all_rating') </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link"> 
                                        @lang('admin_lang.drivers') 
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('admin.list.driver') }}" class="breadcrumb-link"> 
                                        @lang('admin_lang.driver_management') 
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.all_rating')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="mail_success" style="display: none;">
                <div class="alert alert-success vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
                </div>
            </div>
            <div class="mail_error" style="display: none;">
                <div class="alert alert-danger vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>@lang('admin_lang.error')!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
                </div>
            </div>
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['code']['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong class="success_msg"></strong>  
            </div>
            <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong class="error_msg"></strong>  
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <!-- ============================================================== -->
                <!-- basic table  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">@lang('admin_lang.all_rating') <a class="adbtn btn btn-primary" href="{{ route('admin.list.driver') }}"><i class="fas fa-less-than"></i> @lang('admin_lang.back')</a></h5>
                        <div class="card-body">
                            {{-- <form id="search_customers_form" method="post">
                                @csrf
                                <div class="row">
                                    <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="inputText3" class="col-form-label">@lang('admin_lang.Keywords') </label>
                                        <input id="col1_filter" name="keyword" type="text" class="form-control keyword" placeholder="Keywords" value="{{ @$key['keyword'] }}">
                                    </div>
                                    <div data-column="2" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="input-select" class="col-form-label">@lang('admin_lang.Status')</label>
                                        <select class="form-control status" id="col2_filter" name="status">
                                            <option value="">@lang('admin_lang.select_status')</option>
                                            <option value="A" @if(@$key['status'] == 'A') selected @endif>@lang('admin_lang.Active')</option>
                                            <option value="I" @if(@$key['status'] == 'I') selected @endif>@lang('admin_lang.Inactive')</option>
                                            <option value="U" @if(@$key['status'] == 'U') selected @endif>@lang('admin_lang.unverified')</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <a href="javascript:void(0)" id="search_customers" class="btn btn-primary fstbtncls">@lang('admin_lang.Search')</a>

                                        <input type="reset" value="Reset Search" class="btn btn-default fstbtncls reset_search">
                                    </div>
                                </div>
                            </form> --}}
                            <div class="table-responsive listBody">
                                <table class="table table-striped table-bordered first" id="drivers">
                                    <thead>
                                        <tr>
                                            <th>@lang('admin_lang.order_no')</th>
                                            <th>@lang('admin_lang.Name')</th>
                                            <th>@lang('admin_lang.review_ratings')</th>
                                            <th>@lang('admin_lang.review_comments')</th>
                                        </tr>
                                    </thead>
                                    
                                    <tfoot>
                                        <tr>
                                            <th>@lang('admin_lang.order_no')</th>
                                            <th>@lang('admin_lang.Name')</th>
                                            <th>@lang('admin_lang.review_ratings')</th>
                                            <th>@lang('admin_lang.review_comments')</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end basic table  -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
        <div class="loader" style="display: none;">
            <img src="{{url('public/loader.gif')}}">
        </div>
        <div class="loader" style="display: none;">
            <img src="{{url('public/loader.gif')}}">
        </div>
        
        
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click', '.reset_search', function() {
            $('#drivers').DataTable().search('').columns().search('').draw();
        })

        function filterColumn ( i ) {
            $('#drivers').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }

        $('#drivers').DataTable( {
            stateSave: true,
            "order": [[0, "asc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
                $('#col2_filter').val(data.search.status)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
                data.search.status = $('#col2_filter').val()
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('admin.driver.rating.list') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                    d.id = "{{ request()->segment(3) }}";
                }
            },
            'columns': [
                { 
                    render: function(data, type, full) {
                        return full.get_order.order_no;
                        
                    }
                },
                {   
                    render: function(data, type, full) {
                        if(full.customer_id == 0){
                            return full.get_merchant.fname+' '+ full.get_merchant.lname+' (@lang("admin_lang.mer"))';
                        }else{
                            return full.get_customer.fname+' '+ full.get_customer.lname+' (@lang("admin_lang.customer_1"))';
                        }
                        
                    }
                },
                {   data: 'review_point' },
                {
                    data: 'review_comments'
                },
                
            ]
        });

        // change event
        $('input.keyword').on( 'keyup click blur', function () {
            filterColumn( 1 );
        });
        $('.status').on( 'change', function () {
            filterColumn( 2 );
        });
        
       
    });
</script>
@endsection