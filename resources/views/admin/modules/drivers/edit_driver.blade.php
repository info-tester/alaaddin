@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Edit Driver') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.edit_driver')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.edit_driver')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang.edit_driver')
                                <a class="adbtn btn btn-primary" href="{{ route('admin.list.driver') }}"><i class="fas fa-less-than"></i> @lang('admin_lang.back')</a>
                            </h5>
                            <div class="card-body">
                                <form id="editDriverForm" method="post" enctype="multipart/form-data" action="{{ route('admin.edit.driver',@$driver->id) }}">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <input type="hidden" id="old_email" value="{{ @$driver->email }}">

                                            <label for="fname" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.first_name')</label>
                                            <input id="fname" name="fname" type="text" class="form-control required" placeholder="@lang('admin_lang.first_name')" value="{{ @$driver->fname }}">
                                            <span class="error_fname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="lname" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.last_name')</label>
                                            <input id="lname" name="lname" type="text" class="form-control required" placeholder="@lang('admin_lang.last_name')" value="{{ @$driver->lname }}">
                                            <span class="error_lname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="email" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.email')</label>
                                            <input id="email" name="email" type="email" placeholder="@lang('admin_lang.email')" class="form-control " value="{{ @$driver->email }}">
                                            <span class="error_email" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="phone" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.phone_number')</label>
                                            <input id="phone" name="phone" type="text" class="form-control " placeholder="@lang('admin_lang.phone_number')" value="{{ @$driver->phone }}" onkeypress='validate(event)'>
                                            <span class="error_phone" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 fstbtncls">
                                            <input type="file" class="custom-file-input" id="profile_pic" name="profile_pic">
                                            <label class="custom-file-label extrlft" for="profile_pic">@lang('admin_lang.upload_picture')</label>
                                            <span class="errorPic" style="color: red;"></span>
                                            @if(@$driver->image)
                                            @php
                                            $image_path = 'storage/app/public/driver/profile_pics/'.@$driver->image; 
                                            @endphp
                                            @if(file_exists(@$image_path))
                                            <div class="profile" style="display: block;">
                                                <img id="profilePicture" src="{{ URL::to('storage/app/public/driver/profile_pics/'.@$driver->image) }}" alt="" style="width: 100px;height: 100px;">
                                            </div>
                                            @endif
                                            @endif
                                            {{-- <img src="" id="profilePicture" style="height: 100px;width: 100px;"> --}}
                                        </div>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                            <a href="javascript:void(0)" id="addCustomer" class="btn btn-primary ">@lang('admin_lang.save')</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
    @endsection
    @section('scripts')
    {{-- @include('admin.includes.scripts') --}}

    @if(Config::get('app.locale') == 'en')
    @include('admin.includes.scripts')
    @elseif(Config::get('app.locale') == 'ar')
    @include('admin.includes.arabic_scripts')
    @endif
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#addCustomer").click(function(){
                $("#editDriverForm").submit();
            });
        // $(document).on("blur",'#password',function(e){
        //     $("#password").val("");
        // });
        $("#profile_pic").change(function() {
            var filename = $.trim($("#profile_pic").val()),
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".errorpic").text("");
                readURL(this);
            }else{
                
                $(".errorpic").text('@lang('validation.image_extension_type')');
                $('#profilePicture').attr('src', "");
                
            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                  $('#profilePicture').attr('src', e.target.result);
                  $(".profile").show();
              }
              reader.readAsDataURL(input.files[0]);
          }
      }
      $(document).on("blur","#email",function(){
        
        var email = $.trim($("#email").val()),
        old_email = $.trim($("#old_email").val());

        if(email != "")
        {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(regex.test(email)){
                
                var rurl = '{{ route('driver.duplicate.email') }}';
                $.ajax({
                    type:"GET",
                    url:rurl,
                    data:{
                        email:email,
                        old_email:old_email
                    },
                    success:function(resp){
                            // alert(resp);
                            if(resp == 0){
                                $(".error_email").text("@lang('validation.email_id_already_exist')");
                                $("#email").val("");
                            }else{
                                $(".error_email").text("");
                            }
                        }
                    }); 
            }else{
                    // alert("if email format false");
                    $(".errorEmail").text("@lang('validation.please_provide_valid_email_id')");
                }
                
            }else{
                // alert("if blank");
                $(".errorEmail").text("@lang('validation.please_provide_valid_email_id')");
                
            }
        });
      
      $(document).on("blur","#phone",function(){
        var phone = $.trim($("#phone").val());
        
            // if(phone != "")
            // {
            //     var rurl = '';
            //     $.ajax({
            //         type:"GET",
            //         url:rurl,
            //         data:{
            //             phone:phone,
            //             old_phone:
            //         },
            //         success:function(resp){
            //             if(resp == 0){
                
            //                 $(".error_phone").text('@lang('admin_lang.phone_number_already_exist')');
            //                 $("#phone").val("");
            //             }
            //         }
            //     });  
            // }
        });
      $("#editDriverForm").validate({
        rules:{
            'fname':{
                required:true
            },
            'lname':{
                required:true
            },
            'email':{
                required:true,
                email:true
            },
            'profile_pic':{
                accept:"image/jpg,image/jpeg,image/png"
            }
        },
        messages: { 
            fname: { 
                required: "@lang('validation.required')"
            },
            lname: { 
                required: "@lang('validation.required')"
            },
            email: { 
                required: "@lang('validation.required')",
                email: "@lang('admin_lang.please_provide_valid_email')"
            },
            profile_pic: { 
                accept: "@lang('validation.required')"
            }
        },
        errorPlacement: function (error, element) 
        {
                // console.log(element.attr("name"));
                // console.log(error);
                if (element.attr("name") == "fname") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.error_fname').html(error);
                }
                if (element.attr("name") == "lname") {
                    var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.error_lname').html(error);
                }
                if (element.attr("name") == "email") {
                    var error = '<label for="gn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.error_email').html(error);
                }
                if (element.attr("name") == "phone") {
                    var error = '<label for="phn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.error_phone').html(error);
                }
                if (element.attr("name") == "profile_pic") {
                    var error = '<label for="pro" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.errorPic').html(error);
                }
                
            }
        });
      
  });
</script>
<script type="text/javascript">
    function validate(evt) {
    var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>
@endsection