<table class="table table-striped table-bordered first" id="myTable">
    <thead>
        <tr>
            <th>@lang('admin_lang.Name')</th>
            <th>@lang('admin_lang.email')</th>
            <th>@lang('admin_lang.phone_number')</th>
            <th>@lang('admin_lang.login_from')</th>
            <th>@lang('admin_lang.Status')</th>
            <th>@lang('admin_lang.Action')</th>
        </tr>
    </thead>
    <tbody class="">
        @if(@$drivers)
        @foreach(@$drivers as $driver)
        <tr>
            <td>{{ @$driver->fname." ".@$driver->lname }}</td>
            <td>{{ @$driver->email }}</td>
            <td>{{ @$driver->phone }}</td>
            <td>{{ @$driver->login_from }}</td>
            <td>
                @if(@$driver->status == 'A')
                @lang('admin_lang.Active')
                @elseif(@$driver->status == 'I')
                @lang('admin_lang.Inactive')
                @elseif(@$driver->status == 'U')
                @lang('admin_lang.unverified')
                @endif
            </td>
            <td>
                @if(@$driver->status == 'I')
                <a href="{{ route('admin.change.status.driver',@$driver->id) }}"><i class="far fa-check-circle" title='@lang('admin_lang.unblock')'></i></a>
                @elseif(@$driver->status == 'A')
                <a href="{{ route('admin.change.status.driver',@$driver->id) }}"><i class="fas fa-ban" title='@lang('admin_lang.block')'></i></a>
                @endif
                <a href="{{ route('admin.view.driver.profile',@$driver->id) }}"><i class="fas fa-eye" title='@lang('admin_lang.view')'></i></a>
                <a href="{{ route('admin.edit.driver',@$driver->id) }}"><i class="fas fa-edit" title='@lang('admin_lang.Edit')'></i></a>
                <a href="javascript:void(0)" data-toggle="modal" class="send_email_notify" data-id="{{ @$driver->id }}" data-email="{{ @$driver->email }}" data-fname="{{ @$driver->fname }}" data-lname="{{ @$driver->lname }}" data-action="{{ route('admin.send.notification.driver',@$driver->id) }}"><i class="fas fa-paper-plane" title='@lang('admin_lang.send_email')'></i></a> 
                <a href="{{ route('admin.delete.driver',@$driver->id) }}" onclick="return confirm('@lang('admin_lang.do_u_delete_driver')');"><i class=" fas fa-trash"></i></a>
            </td>
        </tr>
        @endforeach
        @endif
    </tbody>
    <tfoot>
        <tr>
            <th>@lang('admin_lang.Name')</th>
            <th>@lang('admin_lang.email')</th>
            <th>@lang('admin_lang.phone_number')</th>
            <th>@lang('admin_lang.login_from')</th>
            <th>@lang('admin_lang.Status')</th>
            <th>@lang('admin_lang.Action')</th>
        </tr>
    </tfoot>
</table>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>