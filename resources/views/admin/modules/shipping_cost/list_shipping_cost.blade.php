@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | List o Shipping Cost') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.list_shipping_cost')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->

    <div class="dashboard-wrapper">
        <div class="dashboard-ecommerce">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">@lang('admin_lang.e_commerce_dashboard_template')</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.edit_cost')</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (session()->has('success'))
                <div class="alert alert-success vd_hidden session_success_div" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                </div>
                @elseif ((session()->has('error')))
                <div class="alert alert-danger vd_hidden session_error_div" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>{{ session('error')['code']['message'] }}</strong> {{ session('error')['meaning'] }} 
                </div>
                @endif
                <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong class="success_msg"></strong> 
                </div>
                <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong class="error_msg"></strong> 
                </div>
                 <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang.list_shipping_cost') <a class="adbtn btn btn-primary" href="{{ route('admin.add.shipping.cost') }}"><i class="fas fa-plus"></i>@lang('admin_lang.Add')</a></h5>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>@lang('admin_lang.from_weight')</th>
                                                <th>@lang('admin_lang.to_weight')</th>
                                                <th>@lang('admin_lang.internal_order')</th>
                                                <th>@lang('admin_lang.external_order')</th>
                                                <th>@lang('admin_lang.actions')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(@$shipping_costs)
                                                @foreach(@$shipping_costs as $cost)
                                                    <tr class="tr_{{ @$cost->id }}">
                                                        <td>{{ @$cost->from_weight }}</td>
                                                        <td>
                                                            @if(@$cost->infinity_weight == 'Y')
                                                            ∞ @lang('admin_lang.infinity')
                                                            @else
                                                            {{ @$cost->to_weight }}
                                                            @endif
                                                        </td>
                                                        <td>{{ @$cost->internal_order_rate }}</td>
                                                        <td>{{ @$cost->external_order_rate }}</td>
                                                        <td>

                                                        <a href="{{ route('admin.edit.shipping.cost',@$cost->id) }}"><i class="fas fa-edit" title="Edit"></i></a>

                                                        <a class="delete_cost" href="javascript:void(0)" data-id="{{ @$cost->id }}"><i class=" fas fa-trash"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>@lang('admin_lang.from_weight')</th>
                                                <th>@lang('admin_lang.to_weight')</th>
                                                <th>@lang('admin_lang.internal_order')</th>
                                                <th>@lang('admin_lang.external_order')</th>
                                                <th>@lang('admin_lang.actions')</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <!-- footer -->   
            @include('admin.includes.footer')
            <!-- end footer -->
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
            <div class="loader" style="display: none;">
                <img src="{{url('public/loader.gif')}}">
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper -->
    <!-- ============================================================== -->
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on("click",".delete_cost",function(e){
            var id = $(e.currentTarget).attr("data-id");
            
            if(confirm("Do you want to delete this shipping cost ?")){
                $.ajax({
                    url:"{{ route('admin.delete.shipping.cost.details') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".success_msg").html('Success!Shipping cost deleted successfully!');
                            // $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".tr_"+id).hide();
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('@lang('admin_lang.unauthorized_access_cust')');
                        }
                        $(".session_success_div").hide();
                        $(".session_error_div").hide();
                    }
                });
            }
            // }
        });
    });
</script>
@endsection