@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Edit Shipping Cost') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.edit_shipping_cost')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.e_commerce_dashboard_template')  </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.edit_cost')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang.edit_shipping_cost')
                                <a class="adbtn btn btn-primary" href="{{ route('admin.list.shipping.cost') }}">
                                    <i class="fas fa-less-than"></i>
                                    @lang('admin_lang.back')
                                </a>
                            </h5>
                            <div class="card-body">
                                <form id="editShippingCostForm" method="POST" action="{{ route('admin.edit.shipping.cost',@$shipping_cost->id) }}">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="from_weight" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.from_weight_grm')</label>
                                            <input id="from_weight" name="from_weight" type="number" class="form-control" placeholder="From weight" value="{{ @$shipping_cost->from_weight }}" onkeypress='validate(event)'>
                                            <span class="error_from_weight" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" id="to_weight_div"@if(@$shipping_cost->infinity_weight == 'Y') style="display: none;" @endif>
                                            <label for="to_weight" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.to_weight_grm')</label>
                                            <input id="to_weight" name="to_weight" type="number" class="form-control" placeholder="To weight" value="{{ @$shipping_cost->to_weight }}" onkeypress='validate(event)'>
                                            <span class="error_to_weight" style="color: red;"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="internal_order_rate" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.internal_order_grm')</label>
                                            <input id="internal_order_rate" name="internal_order_rate" type="number" class="form-control" placeholder="Internal order rate" value="{{ @$shipping_cost->internal_order_rate }}" onkeypress='validate(event)'>
                                            <span class="error_internal_order_rate" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="external_order_rate" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.external_order_grm')</label>
                                            <input id="external_order_rate" name="external_order_rate" type="number" class="form-control" placeholder="External order rate" value="{{ @$shipping_cost->external_order_rate }}" onkeypress='validate(event)'>
                                            <span class="error_external_order_rate" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <label for="infinity_weight" class="col-form-label">
                                                <input id="infinity_weight" name="infinity_weight" type="checkbox" value="Y" @if(@$shipping_cost->infinity_weight == 'Y') checked="" @endif> @lang('admin_lang.infinity_weight')</label>
                                            </div>
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <a href="javascript:void(0)" id="editShippingCost" class="btn btn-primary">@lang('admin_lang.save')</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
    </div>
    <!-- ============================================================== -->
    <!-- end wrapper  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper  -->

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $(document).on('blur',"#from_weight",function(e){
            var cost = $.trim($("#from_weight").val());
            if(cost != ""){
                cost = parseFloat(cost);
                cost = cost.toFixed(3);
                if(isNaN(cost)){
                    $(".error_internal_order_rate").text('Please provide weight with 3 decimal places![Ex: 5.894]');
                    $("#from_weight").val("");
                }else{
                    $("#from_weight").val(cost);
                }
            }
        });

    //     $(document).on('keyup',"#from_weight",function(e){
    //       var name = $(e.currentTarget).val();
    //       if(e.key != '.'){
    //           if(isNaN(e.key)){
    //             $(e.currentTarget).val("");
    //             $(".error_from_weight").text('Please provide weight with 3 decimal places![Ex: 5.894]');
    //         }else{
    //             $(".error_from_weight").text("");
    //         }
    //     }else{
    //         $(".error_from_weight").text("");
    //     }
    // });
    // $(document).on('blur',"#to_weight",function(e){
    //     var cost = $.trim($("#to_weight").val());
    //     if(cost != ""){
    //         cost = parseFloat(cost);
    //         cost = cost.toFixed(3);
    //         if(isNaN(cost)){
    //             $(".error_to_weight").text('Please provide weight with 3 decimal places![Ex: 5.894]');
    //             $("#to_weight").val("");
    //         }else{
    //             $("#to_weight").val(cost);
    //         }
    //     }
    // });

    //     $(document).on('keyup',"#to_weight",function(e){
    //       var name = $(e.currentTarget).val();
    //       if(e.key != '.'){
    //           if(isNaN(e.key)){
    //             $(e.currentTarget).val("");
    //             $(".error_to_weight").text('Please provide weight with 3 decimal places![Ex: 5.894]');
    //         }else{
    //             $(".error_to_weight").text("");
    //         }
    //     }else{
    //         $(".error_to_weight").text("");
    //     }
    // });
    // $(document).on('blur',"#internal_order_rate",function(e){
    //     var cost = $.trim($("#internal_order_rate").val());
    //     if(cost != ""){
    //         cost = parseFloat(cost);
    //         cost = cost.toFixed(3);
    //         if(isNaN(cost)){
    //             $(".error_internal_order_rate").text('Please provide internal order rate with 3 decimal places![Ex: 5.894]');
    //             $("#internal_order_rate").val("");
    //         }else{
    //             $("#internal_order_rate").val(cost);
    //         }
    //     }
    // });

    //     $(document).on('keyup',"#internal_order_rate",function(e){
    //       var name = $(e.currentTarget).val();
    //       if(e.key != '.'){
    //           if(isNaN(e.key)){
    //             $(e.currentTarget).val("");
    //             $(".error_internal_order_rate").text('Please provide internal order rate with 3 decimal places![Ex: 5.894]');
    //         }else{
    //             $(".error_internal_order_rate").text("");
    //         }
    //     }else{
    //         $(".error_internal_order_rate").text("");
    //     }
    // });
    // $(document).on('blur',"#external_order_rate",function(e){
    //     var cost = $.trim($("#external_order_rate").val());
    //     if(cost != ""){
    //         cost = parseFloat(cost);
    //         cost = cost.toFixed(3);
    //         if(isNaN(cost)){
    //             $(".error_external_order_rate").text('Please provide external order rate with 3 decimal places![Ex: 5.894]');
    //             $("#external_order_rate").val("");
    //         }else{
    //             $("#external_order_rate").val(cost);
    //         }
    //     }
    // });

    //     $(document).on('keyup',"#external_order_rate",function(e){
    //       var name = $(e.currentTarget).val();
    //       if(e.key != '.'){
    //           if(isNaN(e.key)){
    //             $(e.currentTarget).val("");
    //             $(".error_external_order_rate").text('Please provide external order rate with 3 decimal places![Ex: 5.894]');
    //         }else{
    //             $(".error_external_order_rate").text("");
    //         }
    //     }else{
    //         $(".error_external_order_rate").text("");
    //     }
    // });
    $("#editShippingCost").click(function(){
        var rurl = "{{ route('check.duplicate.range.shipping.cost') }}",
        from_weight = parseFloat($.trim($("#from_weight").val())),
        to_weight = parseFloat($.trim($("#to_weight").val()));
        if(to_weight == '') {
            to_weight = 0.000;
        }
        if(from_weight !=""){
                // alert("from_weight : "+from_weight+" to_weight : "+to_weight);
                $.ajax({
                    type:"GET",
                    url:rurl,
                    data:{
                        from_weight: from_weight,
                        old_id: {{ @$shipping_cost->id }},
                        to_weight: to_weight
                    },
                    success:function(resp){
                        if(resp == 1){
                            $("#editShippingCostForm").submit();
                        }else{
                            $(".error_from_weight").text("Range is already exist!");
                            $(".error_to_weight").text("Range is already exist!");
                        }
                    }
                });

            }else{
                if(from_weight == ""){
                    $(".error_from_weight").text("Please provide weight with 3 decimal places");
                }
                if(to_weight == ""){
                    $(".error_to_weight").text("Please provide weight with 3 decimal places!");
                }
            }
            
        });

    $("#editShippingCostForm").validate({
        rules:{
            'from_weight':{
                required:true,
                number:true
            },
            'to_weight':{
                required:true,
                number:true
            },
            'internal_order_rate':{
                required:true,
                number:true
            },
            'external_order_rate':{
                required:true,
                number:true
            }
        },
        messages: { 
            from_weight: { 
                required: '@lang('validation.required')'
            },
            to_weight: { 
                required: '@lang('validation.required')'
            },
            internal_order_rate: { 
                required: '@lang('validation.required')'
            },
            external_order_rate: { 
                required: '@lang('validation.required')'
            }
        },
        errorPlacement: function (error, element) 
        {
                // console.log(element.attr("name"));
                // console.log(error);
                if (element.attr("name") == "from_weight") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'Please provide weight in grams with 2 decimal places! [Ex: 200.69]'+'</label>';
                    $('.error_from_weight').html(error);
                }
                if (element.attr("name") == "to_weight") {
                    var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'Please provide weight in grams with 2 decimal places! [Ex: 500.00]'+'</label>';
                    $('.error_to_weight').html(error);
                }
                if (element.attr("name") == "internal_order_rate") {
                    var error = '<label for="gn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'Please provide internal order rate (inside kuwait) with 2 decimal places![Ex: 9960.693]'+'</label>';
                    $('.error_internal_order_rate').html(error);
                }
                if (element.attr("name") == "external_order_rate") {
                    var error = '<label for="phn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'Please provide external order rate (outside kuwait) with 2 decimal places![Ex:5.698]'+'</label>';
                    $('.error_external_order_rate').html(error);
                }
            }
        });


    $('#infinity_weight').click(function() {
        if($(this).is(':checked')) {
            $("#to_weight_div").hide();
            $("#to_weight").val('0.000');
        } else {
            $("#to_weight_div").show();
            $("#to_weight").val('');
        }
    });
});

function validate(evt) {
    var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>
@endsection