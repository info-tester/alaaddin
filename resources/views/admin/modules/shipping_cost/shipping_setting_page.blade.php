@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Shipping Settings') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.shipping_settings')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.shipping_setting')</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.shipping_setting')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang.shipping_setting')
                                <a class="adbtn btn btn-primary" href="{{ route('admin.list.shipping.cost') }}">
                                    <i class="fas fa-less-than"></i>
                                    @lang('admin_lang.back')
                                </a>
                            </h5>
                            <div class="card-body">
                                <form id="addShippingSettingForm" method="POST" action="">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" >
                                            <h5><span style="color: red;">*</span>@lang('admin_lang.enable_international_order')</h5>
                                            <label id="international_order" class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" name="international_order" @if(@$setting->enable_international_order == 'Y') checked="" @endif class="custom-control-input" value="Y"><span class="custom-control-label">@lang('admin_lang.yes')</span>
                                            </label>
                                            <label id="international_order" class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" name="international_order" @if(@$setting->enable_international_order == 'N') checked="" @endif class="custom-control-input" value="N"><span class="custom-control-label">@lang('admin_lang.no')</span>
                                            </label>
                                            <span class="error_international_order" style="color: red;"></span>
                                        </div>

                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" style="display: none;">
                                            <label for="internal_order_cost" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.internal_order_shipping_cost')</label>
                                            <input id="internal_order_cost" name="internal_order_cost" type="text" class="form-control" placeholder='@lang('admin_lang.internal_order_shipping_cost')' value="{{ @$setting->order_shipping_cost }}">
                                            <span class="error_internal_order_cost" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <a href="javascript:void(0)" id="addShippingSetting" class="btn btn-primary">@lang('admin_lang.save')</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('admin.includes.footer')
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper  -->
<!-- ============================================================== -->
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('blur',"#internal_order_cost",function(e){
            var cost = $.trim($("#internal_order_cost").val());
            if(cost != ""){
                cost = parseFloat(cost);
                cost = cost.toFixed(3);
                if(isNaN(cost)){
                    $(".error_internal_order_cost").text('Please provide external order rate with 3 decimal places![Ex: 5.894]');
                    $("#internal_order_cost").val("");
                }else{
                    $("#internal_order_cost").val(cost);
                }
            }
        });

        $(document).on('keyup',"#internal_order_cost",function(e){
          var name = $(e.currentTarget).val();
            if(e.key != '.'){
              if(isNaN(e.key)){
                $(e.currentTarget).val("");
                $(".error_internal_order_cost").text('Please provide external order rate with 3 decimal places![Ex: 5.894]');
              }else{
                $(".error_internal_order_cost").text("");
              }
            }else{
                $(".error_internal_order_cost").text("");
            }
        });
        $("#addShippingSetting").click(function(){
            $("#addShippingSettingForm").submit();
        });

        $("#addShippingSettingForm").validate({
        rules:{
            'internal_order_cost':{
                required:true
            },
            'international_order_cost':{
                required:true
            }
        },
        messages: { 
            internal_order_cost: { 
                required: '@lang('validation.required')'
            },
            international_order_cost: { 
                required: '@lang('validation.required')'
            },
        },
        errorPlacement: function (error, element) 
        {
                // console.log(element.attr("name"));
                // console.log(error);
                if (element.attr("name") == "internal_order_cost") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
                    $('.error_internal_order_cost').html(error);
                }
                if (element.attr("name") == "international_order_cost") {
                    var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
                    $('.error_international_order_cost').html(error);
                }
            }
        });
    });
</script>
@endsection