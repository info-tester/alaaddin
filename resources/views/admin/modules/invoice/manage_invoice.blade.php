@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage Customer') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.manage_invoice')
@endsection
@section('content')
@section('links')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/arabic/assets/libs/css/chosen.css') }}">


@include('admin.includes.links')

@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
<div class="dashboard-ecommerce">
<div class="container-fluid dashboard-content ">
    <!-- ============================================================== -->
    <!-- pageheader  -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">@lang('admin_lang.manage_invoice') </h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                            
                            <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.manage_invoice')</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="mail_success" style="display: none;">
        <div class="alert alert-success vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
        </div>
    </div>
    <div class="mail_error" style="display: none;">
        <div class="alert alert-danger vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong> @lang('admin_lang.com_err') !</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
        </div>
    </div>
    @if (session()->has('success'))
    <div class="alert alert-success vd_hidden session_suc_msg_show" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
    </div>
    @elseif ((session()->has('error')))
    <div class="alert alert-danger vd_hidden session_err_msg_show" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
    </div>
    @endif
    <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong class="success_msg">@lang('admin_lang.success')!</strong>
    </div>
    <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong class="error_msg">@lang('admin_lang.com_err')!</strong>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">@lang('admin_lang.manage_invoice') <a class="adbtn btn btn-primary" href="{{ route('admin.add.invoice') }}"><i class="fas fa-plus"></i> @lang('admin_lang.Add')</a></h5>
                <div class="card-body">
                    <form id="search_orders_form" method="post" action="{{route('admin.invoice.export')}}">
                        @csrf
                        <div class="row">
                            <div data-column="0" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <label for="keyword" class="col-form-label">Search for @lang('admin_lang.Keywords')</label>
                                <input id="col0_filter" name="keyword" type="text" class="form-control keyword" placeholder="#Invoice no./applicable to /total">
                            </div>
                            <div data-column="4" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <label for="merchant_lb_txt2" class="col-form-label">Merchant</label>
                                <select  class=" form-control select slt slct seller_nm"  tabindex="4" name="merchant_id" id="col4_filter">
                                    <option value="">Select merchant</option>
                                    @if(@$merchants)
                                        @foreach(@$merchants as $merchant)
                                            <option value="{{ @$merchant->id }}">{{ @$merchant->fname." ".@$merchant->lname }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div data-column="3" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <label for="input-select" class="col-form-label">@lang('admin_lang.invoice_type')</label>
                                <select class="custom-select form-control required" name="invoice_type" id="col3_filter">
                                    <option value="">@lang('admin_lang.select_invoice_type')</option>
                                    <option value="I">@lang('admin_lang.in')</option>
                                    <option value="O">@lang('admin_lang.out')</option>
                                </select>
                            </div>
                            <div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <label for="to_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
                                <input type="text" class="form-control datepicker from_date" name="from_date" placeholder="@lang('admin_lang_static.from_date')" value="" readonly="" id="col1_filter_from">
                            </div>
                            <div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <label for="to_date" class="col-form-label">@lang('admin_lang_static.to_date')</label>
                                <input type="text" class="form-control datepicker to_date" name="to_date" placeholder="@lang('admin_lang_static.to_date')" value="" readonly="" id="col1_filter_to">
                            </div>
                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <label for="search_orders" class="col-form-label">&nbsp;</label>
                                <a href="javascript:void(0)" id="search_orders" class="btn btn-primary ">@lang('admin_lang.Search')</a>

                                <input type="reset" value="Reset Search" class="btn btn-default reset_search">
                                <button type="submit" id="search_orders" class="btn btn-primary ">@lang('admin_lang.export')</button>
                            </div>
                        </div>
                    </form>
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-md-12">
                            <button class="btn btn-warning print_bulk_invoice"><i class="fa fa-print"></i> <span class="print_mark_text"> Print Selected</span></button>
                        </div>
                    </div>
                    <div class="table-responsive listBody tpmrgn">
                        <table class="table table-striped table-bordered" id="invoices">
                            <thead>
                                <tr>
                                    <th class="no-sort" style="padding: 10px"><input type="checkbox" class="select_all" >  Select all</th>
                                    <th>@lang('admin_lang.invoice_date')</th>
                                    <th>@lang('admin_lang.invoice_no')</th>
                                    <th>@lang('admin_lang.invoice_type')</th>
                                    <th>@lang('admin_lang.applicable_to')</th>
                                    <th>@lang('admin_lang.total')</th>
                                    <th>@lang('admin_lang.actions')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="no-sort"></th>
                                    <th>@lang('admin_lang.invoice_date')</th>
                                    <th>@lang('admin_lang.invoice_no')</th>
                                    <th>@lang('admin_lang.invoice_type')</th>
                                    <th>@lang('admin_lang.applicable_to')</th>
                                    <th>@lang('admin_lang.total')</th>
                                    <th>@lang('admin_lang.actions')</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- footer -->   
@include('admin.includes.footer')
<!-- end footer -->
<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>

</div>


</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script type="text/javascript">
    $( function() {
		$(".datepicker").datepicker({dateFormat: "yy-mm-dd",
			defaultDate: new Date(),
			changeMonth: true,
			changeYear: true,
			yearRange: '-100:+0'
		});         	
	})

    $(document).ready(function(){
        $('.slct').chosen();
        $('body').on('click', '.reset_search', function() {
			$('#invoices').DataTable().search('').columns().search('').draw();
		});
        function filterColumn ( i ) {
            $('#invoices').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
                ).draw();
        }
        var markOrders = [];
        $('body').on('click', '.select_all', function() {
            markOrders = [];
            if($(this).is(':checked')) {
                $('.mark_order').prop('checked', true);
                $('.mark_order').each(function(item, index) {
                    markOrders.push($(this).val())
                });	
            } else {
                $('.mark_order').prop('checked', false);
                $('.mark_order').each(function(item, index1) {
                    const index = markOrders.indexOf($(this).val());
                    if (index > -1) {
                    markOrders.splice(index, 1);
                    }
                });
            }
            if(markOrders.length) {
                $('.print_mark_text').html(markOrders.length + ' Invoice Seleted')
            }
            if(markOrders.length == 0) {
                $('.print_mark_text').html(' Print Selected')
            }
        });

        $('body').on('click', '.mark_order', function() {
            if($(this).is(':checked')) {
                markOrders.push($(this).val())
            } else {
                const index = markOrders.indexOf($(this).val());
                if (index > -1) {
                markOrders.splice(index, 1);
                }
                $('.select_all').prop('indeterminate', true);
            }
            if(markOrders.length) {
                $('.print_mark_text').html(markOrders.length + ' Invoice Seleted')
            }
            if(markOrders.length == 0) {
                $('.print_mark_text').html(' Print Selected')
            }
        })

        // Print bulk invoice
        $('.print_bulk_invoice').on('click',function(){
            if(markOrders.length <= 100){
                if(markOrders.length <1) {
                    toastr.error('Please select at least 1 invoice');
                } else {
                    invoiceIds = btoa(JSON.stringify(markOrders));
                    window.open("admin/bulk-print-invoice/"+invoiceIds ,invoiceIds,"location=0,toolbar=no,scrollbars=yes,height=850,width=800,left=100,top=10");
                }
            } else {
                toastr.error('Maximum 100 invoice can be selected at a time');
            }
        });
        // datatable started
        var styles = `
            table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
                right: 0em;
                content: "";
            }
            table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
                right: 0em;
                content: "";
            }
        `
        var styleSheet = document.createElement("style");
        styleSheet.innerText = styles;
        document.head.appendChild(styleSheet);
        $('#invoices').DataTable( {
            stateSave: true,
            "order": [[1, "desc"]],
            "stateLoadParams": function (settings, data) {
                $('#col0_filter').val(data.search.keyword)
                $('#col3_filter').val(data.search.invoice_type)
                $('#col4_filter').val(data.search.merchant_id)
                $('#col1_filter_from').val(data.search.from_date);
                $('#col1_filter_to').val(data.search.to_date);
            
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col0_filter').val()
                data.search.invoice_type = $('#col3_filter').val()
                data.search.merchant_id = $('#col4_filter').val()
                data.search.from_date = $('#col1_filter_from').val();
                data.search.to_date = $('#col1_filter_to').val();
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('admin.manage.invoice') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
                {
                    "orderable": false,
                    render: function(data, type, full) {
                        return '<input type="checkbox" class="mark_order" name="mark_order" value="'+ full.id +'">'
                    }
                },
                { 
                    data: 'invoice_date',
                },
                { 
                    data: 'invoice_no',
                },
                {  
                     data: 'invoice_type',
                     render: function(data, type, full) {
                         if(data == 'I'){
                             return "@lang('admin_lang.in')";
                         }else{
                            return "@lang('admin_lang.out')"
                         }
                     }
                },
                { 
                    render: function(data, type, full) {
                        if(full.merchant_id != null){
                            return full.get_merchant.fname+' '+full.get_merchant.lname
                        }else{
                            return full.name;
                        }
                        
                    }
                },
                { 
                    data: 'total',
                },
                { 
                    data: 0,
                    render: function(data, id, full, meta) {
                        var a = '';

                        a += ' <a href="{{ url('admin/view-invoice') }}/'+full.id+'"><i class="fas fa-eye" title="'+"@lang('admin_lang.view')"+'"></i></a>'

                        a += ' <a href="{{ url('admin/edit-invoice') }}/'+full.id+'"><i class="fas fa-edit" title='+"@lang('admin_lang.Edit')"+'></i></a>'

                        a += ' <a class="delete_invoice" href="javascript:void(0)" data-id="'+full.id+'"><i class=" fas fa-trash"  title='+"@lang('admin_lang.Delete')"+'></i></a>'

                        return a;
                    }
                }
            ]
        });
        
        // change event
        $('#col0_filter').on( 'keyup click', function () {
            filterColumn( $(this).parents('div').data('column') );
        });
        $('#col3_filter').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        }); 
        $('#col4_filter').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        });
        $('.from_date').on('change', function () {		
            if($('#col1_filter_to').val() == '' && $('#col1_filter_to').val() < $('#col1_filter_from').val()) {
                return false;
            } else {
                $('#invoices').DataTable().column( 1 ).search(
                    [$('#col1_filter_from').val(),$('#col1_filter_to').val()],
                    ).draw();
            }
        });
        $('.to_date').on('change', function () {		
            if($('#col1_filter_from').val() == '' && $('#col1_filter_from').val() > $('#col1_filter_to').val()) {
                return false;
            } else {
                $('#invoices').DataTable().column( 1 ).search(
                    [$('#col1_filter_from').val(),$('#col1_filter_to').val()],
                    ).draw();
            }
        });

        $(document).on("click",".delete_invoice",function(e){
            var id = $(e.currentTarget).attr("data-id");
            var $this = $(this);
            if(confirm("@lang('admin_lang.delete_invoice_msg')")){
                $.ajax({
                    url:"{{ route('admin.delete.invoice') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".success_msg").html('@lang('admin_lang.success_invoice_msg')');
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $this.parent().parent().remove();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('@lang('admin_lang.failed_invoice_msg')');
                        }
                    }
                });
            }
        });
        $("select[name=invoices_length]").change(function() {
            markOrders = [];
            $('.print_mark_text').html(' Print Selected')
            $('.select_all').prop('checked', false);
        });
        $('body').on('click','.paginate_button',function(){
            markOrders = [];
            $('.print_mark_text').html(' Print Selected')
            $('.select_all').prop('checked', false);
        })
    });
</script>
@endsection