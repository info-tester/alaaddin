@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Add City') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.edit_invoice')
@endsection
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/jquery.datetimepicker.css') }}">
    <style>
        .ad1Span{
            margin: 0 0 0 15px;
        }
        .ad1Span2{
            margin: 0 0 12px 35px;
        }
        #merchantList{
            position: absolute;
            width: 96%;
            z-index: 99;
        }
        #merchantList ul{
            background: #fff;
            width: 96%;
            border: solid 1px #eee;
            padding: 0;
            max-height: 200px;
            overflow-y: scroll;
        }
        #merchantList ul li{
            list-style: none;
            padding: 5px 15px;
            cursor: pointer;
            border-bottom: solid 1px #eee;
        }
        #merchantList ul li:hover{
            background: #3a82c4;
            color: #fff;
        }
        .btn_cls{
            margin: 32px 0px 6px 5px;
        }
    </style>
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.edit_invoice')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                @lang('admin_lang.edit_invoice')
                                <a class="adbtn btn btn-primary" href="{{ route('admin.manage.invoice') }}"> @lang('admin_lang.back')</a>
                            </h5>
                            <div class="card-body">
                                <form id="addInvoiceForm" method="POST" action="{{ route('admin.update.invoice') }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="id" id="id" value="{{ @$invoice->id }}">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="invoice_no" class="col-form-label">@lang('admin_lang.invoice_no') : <strong>{{ @$invoice->invoice_no }}</strong></label>
                                            <input type="hidden" id="invoice_no" name="invoice_no" value="{{ @$invoice->invoice_no }}">
                                            <input type="hidden" id="merchant_id" name="merchant_id" value="{{ @$invoice->merchant_id }}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="" id="showMerchant" @if(@$invoice->merchant_id != null) style="display:inline-bloack;width:50%" @else style="display:none;width:50%" @endif>
                                                <label for="mer" class="col-form-label">@lang('admin_lang.mer') : {{ @$invoice->getMerchant->fname }} {{ @$invoice->getMerchant->lname }}</label>
                                            <label class="" @if(@$invoice->merchant_id != null) style="display:none;width:50%" @else style="display:inline-bloack;width:50%" @endif id="showName">
                                                <label for="name" class="col-form-label">@lang('admin_lang.name') : <strong>{{ @$invoice->name }}</strong></label>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6" style="width:50%">
                                            <label for="invoice_type" class="col-form-label">@lang('admin_lang.invoice_type') (@lang('admin_lang.required'))</label>
                                            <select class="custom-select form-control required" id="invoice_type" name="invoice_type">
                                                <option value="">@lang('admin_lang.select_invoice_type')</option>
                                                <option value="I" @if(@$invoice->invoice_type == 'I') {{ 'selected' }} @endif>@lang('admin_lang.in')</option>
                                                <option value="O" @if(@$invoice->invoice_type == 'O') {{ 'selected' }} @endif>@lang('admin_lang.out')</option>
                                            </select>
                                            <span class="invoice_type_error"></span>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="invoice_date" class="col-form-label">@lang('admin_lang.invoice_date') (@lang('admin_lang.required'))</label>
                                        <input type="text" name="invoice_date" id="invoice_date" class="form-control" value="{{ @$invoice->invoice_date }}">
                                            <span class="invoice_date_error"></span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <h3>
                                                @lang('admin_lang.products')
                                                <span class="pull-right">@lang('admin_lang.total'): <span id="showTotal">{{ @$invoice->total }}</span></span>
                                                <input type="hidden" name="total" id="total" value="{{ @$invoice->total }}">
                                            </h3>
                                            
                                        </div>
                                        
                                        <input type="hidden" name="product_count" value="{{ count(@$invoice->getInvoiceDetails) }}" id="product_count">
                                        @if($invoice->getInvoiceDetails != null)
                                            @foreach($invoice->getInvoiceDetails as $i => $pro)
                                                @php
                                                    $i = $i + 1;
                                                @endphp
                                                <div class="row col-md-12">
                                                    <div class="col-md-3 form-group">
                                                        <label for="product_name" class="col-form-label">@lang('admin_lang.product_name') </label>
                                                        <input id="product_name" name="product_name[]" type="text" class="form-control productDiv{{ $i }}" placeholder='@lang('admin_lang.product_name')' value="{{ @$pro->product_name }}">
                                                    </div>
                                                    <div class="col-md-3 form-group">
                                                        <label for="price" class="col-form-label">@lang('admin_lang.price') </label>
                                                        <input id="price" name="price[]" type="text" class="form-control changeTotal getPrice{{ $i }} productDiv{{ $i }}" placeholder='@lang('admin_lang.price')' onkeypress="return isNumber(event)" value="{{ @$pro->price }}">
                                                    </div>
                                                    <div class="col-md-3 form-group">
                                                        <label for="qty" class="col-form-label">@lang('admin_lang.qty') </label>
                                                        <input id="qty" name="qty[]" type="text" onkeypress="return isNumber(event)" class="form-control changeTotal productDiv{{ $i }} getQty{{ $i }}" placeholder='@lang('admin_lang.qty')' value="{{ @$pro->qty }}" >
                                                    </div>
                                                    @if($i == 1)
                                                        <div class="col-md-3 form-group">
                                                            <button type="button" class="btn btn-success btn_cls addRow"><i class="fa fa-plus"></i> @lang('admin_lang.add_more')</button>
                                                        </div>
                                                    @else
                                                        <div class="col-md-3 form-group">
                                                            <button class="btn btn-danger btn_cls deleteRow" data-count="{{ @$i }}"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    @endif
                                                    <div class="showError{{ $i }} text-danger col-md-12"></div>
                                                </div>
                                            @endforeach
                                        @endif
                                        
                                        <div id="appendRow" class="col-md-12"></div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <button class="btn btn-primary" id="addinvoiceBtn" type="button">@lang('admin_lang.Edit')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ asset('public/admin/assets/libs/js/jquery.datetimepicker.full.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#addInvoiceForm").validate({
        messages: { 
            invoice_type: { 
                required: '@lang('validation.required')'
            },
            merchant_id: { 
                required: '@lang('validation.required')'
            },
            name: { 
                required: '@lang('validation.required')'
            },
        },
        errorPlacement: function (error, element) 
            {
                if (element.attr("name") == "invoice_type") {
                    var error = '<label for="invoice_type" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.invoice_type_error').html(error);
                }
                if (element.attr("name") == "merchant_id") {
                    var error = '<label for="merchant_id" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.merchant_id_error').html(error);
                }
                if (element.attr("name") == "name") {
                    var error = '<label for="name" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.name_error').html(error);
                }
                
            }
    });

    $('body').on('keyup','.changeTotal',function(){
        var count = $('#product_count').val();
        var total = 0.000;
         
        for(var i =1;i<=count ; i++){
            var price = $('.getPrice'+i).val();
            var qty   = $('.getQty'+i).val();

            if(price == ''){
                price = 0;
            }else{
                price = parseFloat(price);
            }

            if(qty == ''){
                qty = 0;
            }else{
                qty = parseInt(qty);
            }
            total = total + (price * qty);
            //console.log(total);
        }
        $('#total').val(total.toFixed(3));
        $('#showTotal').text(total.toFixed(3));

    })
    $("#mer").on('keyup',function(){
        var merchant = $(this).val();
        var reqdata = {
            'jsonrpc':"2.0",
            '_token':"{{ csrf_token() }}",
            merchant: merchant
        }
        $.ajax({
            url:"{{ route('admin.get.all.merchant') }}",
            type:"POST",
            data:reqdata,
            success:function(response){
                var result = '';
                if(response.merchant.length != 0){
                    result += '<ul class="dropdown-menu" style="display:block;width: 100%;">';
                    $.each(response.merchant,function(i,v){
                        result += '<a href="javascript:void(0);" style="color:black;" class="merchant_name" data-name="'+v.fname+' '+v.lname+'"><li>'+v.fname+' '+v.lname+'</li></a>';
                    })
                    result += '</ul>';
                }else{
                    result += '<ul class="dropdown-menu" style="display:block;width: 100%;"><li>Nothing found</li></ul>';
                }
                $('#merchantList').show();
                $('#merchantList').html(result);
                
            }
        });
    });

    $("body").click(function () {
        $("#merchantList").fadeOut('slow');
    });
    $('body').on('click','.merchant_name',function(){
        var merchant_name = $(this).data('name');
        $('#mer').val(merchant_name);
    })
    
    $('body').on('click','.apply_radio',function(){
        var value = $(this).val();
        if(value == 'M'){
            $('#showMerchant').show();
            $('#showName').hide();
        }else{
            $('#showMerchant').hide();
            $('#showName').show();
        }
    });

    $('.addRow').on('click',function(){
        var res = '';
        var count = $('#product_count').val();
        count = parseInt(count) + 1 ;
        res += '<div class="row addDiv">'+
                    '<div class="col-md-3 form-group">'+
                        '<label for="product_name" class="col-form-label">@lang('admin_lang.product_name') </label>'+
                        '<input id="product_name" name="product_name[]" type="text" class="form-control productDiv'+count+'" placeholder="@lang('admin_lang.product_name')">'+
                    '</div>'+
                    '<div class="col-md-3 form-group">'+
                        '<label for="price" class="col-form-label">@lang('admin_lang.price') </label>'+
                        '<input id="price" name="price[]" type="text" class="form-control changeTotal getPrice'+count+' productDiv'+count+'" placeholder="@lang('admin_lang.price')" onkeypress="return isNumber(event)">'+
                    '</div>'+
                    '<div class="col-md-3 form-group">'+
                        '<label for="qty" class="col-form-label">@lang('admin_lang.qty') </label>'+
                        '<input id="qty" name="qty[]" type="text" class="form-control changeTotal getQty'+count+' productDiv'+count+'" placeholder="@lang('admin_lang.qty')" onkeypress="return isNumber(event)">'+
                    '</div>'+
                    '<div class="col-md-3 form-group">'+
                        '<button class="btn btn-danger btn_cls deleteRow" type="button" data-count="'+count+'"><i class="fa fa-minus"></i></button>'+
                    '</div>'+
                    '<div class="showError'+count+' text-danger col-md-12"></div>'+
                '</div>';
        $('#appendRow').append(res);
        $('#product_count').val(count);
    });
    $('body').on('click','.deleteRow',function(){
        
        var count = $('#product_count').val();
        count = parseInt(count) - 1 ;
        var rowCount = $(this).data('count');
        var alltotal = $('#total').val();
        $('#product_count').val(count);

        var price = $('.getPrice'+rowCount).val();
        var qty   = $('.getQty'+rowCount).val();
        if(price == ''){
            price = 0;
        }else{
            price = parseFloat(price);
        }

        if(qty == ''){
            qty = 0;
        }else{
            qty = parseInt(qty);
        }
        total = (price * qty);
        alltotal = parseFloat(alltotal) - parseFloat(total);
        $('#total').val(alltotal.toFixed(3))
        $('#showTotal').text(alltotal.toFixed(3))
        $(this).closest('.addDiv').remove();
    });

    $("#addinvoiceBtn").click(function(){
            var invoice_no = $('#invoice_no').val();
            var reqData = {
                'jsonrpc' : '2.0',
                '_token'  : "{{ csrf_token() }}",
                'data'    :{
                    'invoice_no':invoice_no,
                    'id':$('#id').val()
                }
            }
            $.ajax({
                'url':"{{ route('admin.check.invoice') }}",
                'type':"POST",
                'data':reqData,
                success:function(res){
                    if(res.status == 'false'){
                        var error = '<label for="invoice_no" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">Invoice no already added</label>';
                        $('.invoice_no_error').html(error);
                    }else{
                        var val = validateProduct();
                        if(val == 'true'){
                            $("#addInvoiceForm").submit();
                        }
                    }
                }
            })
            
        });
});

</script>
<script>
    function validateProduct(){
        var count = $('#product_count').val();
        //var total = $('#total').val();
        for(var i = 1;i<=count;i++){
            if($('.productDiv1').val() == '' && i == 1){
                $('.showError'+i).html("Please enter data at least one row");
                return 'false';
            }else if($('.productDiv'+i).val() == ''){
                $('.showError'+i).html("Please enter all data");
                return 'false';
            }else{
                $('.showError'+i).html("");
            }
        }
        // if(total == 0.000){
        //     $('.showError1').html("Invalid product total");
        //     return 'false';
        // }
        return 'true';
    }
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
            return false;
        }
        return true;
    }
</script>
<script>
    /*jslint browser:true*/
    /*global jQuery, document*/

    jQuery(document).ready(function () {
        'use strict';

        jQuery('#invoice_date').datetimepicker();
    });
</script>
@endsection