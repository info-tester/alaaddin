@extends('admin.layouts.app')
@section('title', 'Alaaddin | Admin | Manage Product')
@section('content')
@section('links')
@include('admin.includes.links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style type="text/css">
    #example_filter{
        display: none;
    }
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Products Management</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active" aria-current="page">Products Management</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        @if (session()->has('success'))
        <div class="alert alert-success vd_hidden session_success_msg" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
        </div>
        @elseif ((session()->has('error')))
        <div class="alert alert-danger vd_hidden session_error_msg" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
        </div>
        @endif
        <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong class="success_msg"></strong> 
        </div>
        <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong id="error_msg" class="error_msg"></strong> 
        </div>
        <div class="row">
            <!-- ============================================================== -->
            <!-- basic table  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    
                    <div class="card-body">
                        
                            <div class="row">
                                <div data-column="4" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="inputText3" class="col-form-label">Keyword</label>
                                    <input type="text" class="form-control keyword" placeholder="Search by title" id="col4_filter" name="keyword" value="{{ @$key['keyword'] }}">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-column="3">
                                    <label for="input-select" class="col-form-label">Merchant</label>
                                    <select data-placeholder="Choose a Merchant..." class="form-control select slt slct user_id" name="user_id" tabindex="3" id="col3_filter">
                                        <option value="">Select Merchant</option>
                                        @foreach(@$user as $us)
                                        <option value="{{ $us->id }}" @if(@$key['merchant_id'] == $us->id) selected @endif>{{ $us->fname }} {{ $us->lname }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-column="1">
                                    <label for="input-select" class="col-form-label">Category</label>
                                    <select class="form-control category" name="category" id="col1_filter">
                                        <option value="">Select Category</option>
                                        @foreach(@$category as $cat)
                                        <option value="{{ @$cat->id }}" >{{ @$cat->categoryByLanguage->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-column="7">
                                    <label for="input-select" class="col-form-label">Status</label>
                                    <select class="form-control status" name="status" id="col7_filter">
                                        <option value="">Select Status</option>
                                        <option value="A">Active</option>
                                        <option value="I">Inactive</option>
                                        <option value="W">Awaiting Approval</option>
                                    </select>
                                </div>
                                
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-column="5" id="priceVal">
                                    <label for="input-select" class="col-form-label">@lang('admin_lang_static.price_range')</label>
                                    <div>
                                        <input type="hidden" id="amount" name="price" class="price" readonly style="border:0; color:#f6931f; font-weight:bold;" value="{{ @$key['price'] }}" class="price">
                                        <span class="price_from"></span>
                                        <span class="price_to" style="position: absolute; right: 25px"></span>
                                    </div>
                                    <div id="slider-range"></div>
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                    <a href="javascript:void(0)" class="btn btn-primary fstbtncls"> search</a>
                                    <input type="reset" value="Reset search" class="btn btn-default fstbtncls reset_search">
                                </div>
                            </div>
                        
                        <div class="table-responsive tpmrgn listBody">
                            <table class="table table-striped table-bordered" id="example">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Product Code</th>
                                        <th>Category</th>
                                        <th>Merchant</th>
                                        <th>Title</th>
                                        <th>Price ({{ getCurrency() }})</th>
                                        <th>Total no of reviews</th>
                                        <th>Average Review Ratings(point out of 5)</th>
                                        <th>Average Review Ratings</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                
                                <tfoot>
                                    <tr>
                                        <th>Image</th>
                                        <th>Product Code</th>
                                        <th>Category</th>
                                        <th>Merchant</th>
                                        <th>Title</th>
                                        <th>Price ({{ getCurrency() }})</th>
                                        <th>Total no of reviews</th>
                                        <th>Average Review Ratings(point out of 5)</th>
                                        <th>Average Review Ratings</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end basic table  -->
            <!-- ============================================================== -->
        </div>
    </div>
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>
<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>
@endsection
@section('scripts')
@include('admin.includes.scripts')
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script>
    var table;
    function filterColumn ( i ) {
        $('#example').DataTable().column( i ).search(
            $('#col'+i+'_filter').val(),
            ).draw();
    }
    function filterColumnPrice ( i ) {
        $('#example').DataTable().search(
            $('#amount').val(),
            ).draw();
    }

    function getImg(data, type, full, meta) {
        if(full.default_image){
            if(full.default_image.image) {
                return '<img src="storage/app/public/products/80/'+full.default_image.image+'" />';
            } else {
                return '<img src="public/frontend/images/default_product.png" />';
            }    
        }else{
            return '';   
        }
    }

    function getAction(data, id, full, meta) {
       var a = '';
       if(full.status == 'A') {
            a += '<a class="status_change block" href="javascript:void(0)" data-id="'+full.id+'" data-status=""><i class="fa fa-ban icon_change_'+full.id+'" title="@lang('admin_lang.block')"></i></a> '
        } else if(full.status == 'I') {
            a += '<a class="status_change unblock" href="javascript:void(0)" data-id="'+full.id+'" data-status="'+full.id+'"><i class="fa fa-check-circle icon_change_'+full.id+'" title="@lang('admin_lang.unblock')"></i></a> '
        } else if(full.status == 'W') {
            a += '<a class="status_change approve" href="javascript:void(0)" data-id="'+full.id+'" data-status="'+full.id+'"><i class="fa fa-thumbs-up icon_change_'+full.id+'" title="@lang('admin_lang.approve')"></i></a> ';
        }
        if(full.status == 'A') {
            if(full.is_featured == 'Y'){
                a += '<a class="set_featured_product" href="javascript:void(0)" data-id="'+full.id+'" data-status="'+full.id+'"><i style="color:#ee1c25!important;" class="fa fa-star icon_change_'+full.id+'" title="Remove from featured products"></i></a> ';   
            }
            else if(full.is_featured == 'N'){
                a += '<a class="set_featured_product" href="javascript:void(0)" data-id="'+full.id+'"><i  class="fa fa-star '+full.id+'" title="Set as featured products"></i></a> ';
            }
        }
        a += '<a href="{{ url('admin/product-comments-review-ratings').'/' }}'+full.slug+'"><i class="fas fa-eye" title="View Reviews & ratings"></i></a> ';
        a += '<a class="delete_product" href="javascript:void(0)" data-id="'+full.id+'"><i class="fa fa-trash" title="Delete"></i></a> ';
        
        return a;
    }
    function getMerchant(data, type, full, meta) {
        if(full.product_marchant){
            return full.product_marchant.fname+' '+full.product_marchant.lname ;
        }else{
            return " ";
        }
    }
    function getStatus(data, type, full, meta) {
        if(data == 'Y') 
            return "<span class='status_"+full.id+"'>Yes</span>";
        else if(data == 'N') 
            return "<span class='status_"+full.id+"'>No</span>";
        else if(data == 'A')
            return "<span class='status_"+full.id+"'>Active</span>";
        else if(data == 'I')
            return "<span class='status_"+full.id+"'>Inactive</span>";
        else if(data == 'W') 
            return "<span class='status_"+full.id+"'>Awaiting Approval</span>";
    }
    
    $(document).ready(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            @if(@$highestPrice->product_price)
            max: {{ @$highestPrice->product_price }},
            @else
            max: 1000.000,
            @endif
            @php
            if(@$key['price']){
                $price_val = $key['price'];
            }else{
                if(@$highestPrice->price) {
                $price_val = '0, '. (Int)@$highestPrice->product_price;
                } else {
                $price_val = '0, 1000';
                }
            }
            @endphp
            values: [ {{ $price_val }} ],
            slide: function( event, ui ) {
                $( "#amount" ).val( ui.values[ 0 ]  + "," + ui.values[ 1 ] );
                $('.price_from').html(ui.values[ 0 ] + ' {{ getCurrency() }}')
                $('.price_to').html(ui.values[ 1 ] + ' {{ getCurrency() }}')
            }
        });
        $("#example_filter").hide();
        var styles = `
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
`
var styleSheet = document.createElement("style");
styleSheet.innerText = styles;
document.head.appendChild(styleSheet);
        table = $('#example').DataTable( {
            stateSave: false,
            "order": [[1, "desc"]],
            "stateLoadParams": function (settings, data) {
            // console.log(data);
            $('#col3_filter').val(data.search.user_id)
            $('#col4_filter').val(data.search.keyword)
            $('#col1_filter').val(data.search.category)
            $('#col2_filter').val(data.search.subcategory)
            $('#col7_filter').val(data.search.status)
            $('#amount').val(data.search.price)
            const a = data.search.price.split(',');
            if (a[0] && a[1]) {
                $('#slider-range').slider("option", "values", a);
            }
        },
        stateSaveParams: function (settings, data) {
            data.search.user_id = $('#col3_filter').val();
            data.search.keyword = $('#col4_filter').val();
            data.search.category = $('#col1_filter').val();
            data.search.subcategory = $('#col2_filter').val();
            data.search.status = $('#col7_filter').val();
            data.search.price = $('#amount').val();
        },
        "createdRow": function( row, data, dataIndex ) { 
            var id = (JSON.stringify(data.id));
            var r = JSON.stringify(dataIndex);
            $(row).addClass("tr_class_"+id);
            $(row).addClass("tr_row_class");
        },
        "processing": true,
        "serverSide": true,
        'serverMethod': 'post',
        "ajax": {
            "url": "{{ route('get.product.datatable') }}",
            "data": function ( d ) {
                d.myKey = "myValue";
                d._token = "{{ @csrf_token() }}";
            }
        },
        'columns': [
        { 
            data: 'id',
            render: getImg
        },
        { data: 'product_code' },
        { data: 'product_category[0].category.category_by_language.title' },
        { 
            data: 'product_marchant',
            render: getMerchant
        },
        { data: 'product_by_language.title' },
        { 
            data: 'price',
            render: function(data, type, full, meta){
                return " Rs. "+data+" /"+full.product_unit_masters.unit_name;
                
            }
        },
        { data: 'total_no_reviews' },
        { data: 'avg_review' },
        { data: 'avg_review',
            render:function(data, type, full, meta){
                if(data == 0.000){
                    return "No reviews";
                }else{
                    var a = '';
                    if(data >4.000){
                        a += '<a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            ';
                    }
                    else if(data == 4.000){
                        a += '<a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>\n\
                            ';
                    }
                    else if(data >3.000){
                        a += '<a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>\n\
                            ';
                    }
                    else if(data >2.000){
                        a += '<a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>\n\
                            ';
                    }
                    else if(data >1.000){
                        a += '<a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>\n\
                            ';
                    }
                    else if(data <=1.000){
                        a += '<a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>\n\
                            <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>\n\
                            ';
                    }
                    return a;
                    
                }
            }
        },
        { 
            data: 'status',
            render: getStatus
        },
        { 
            data: 'seller_status,id',
            render: getAction
        },
        ]
    });

        $('input.keyword').on( 'keyup click', function () {
            filterColumn( $(this).parents('div').data('column') );
        } );

        $('.category').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        } );

        $('.subcategory').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        } );

        $('.status').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        } );

        $('.user_id').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        } );

        @if(@$key['merchant_id'])
        $('#example').DataTable().column( 3 ).search(
            {{ $key['merchant_id'] }},
            ).draw();
        $('#col3_filter').val({{ $key['merchant_id'] }});
        @endif

        $('body').on('mouseleave', '.ui-slider-handle', function () {
        // alert(filterColumnPrice(5));
        filterColumnPrice( $('#priceVal').data('column') );
    } );
    $('body').on('click', '.reset_search', function() {
        $('#example').DataTable().search('').columns().search('').draw();
        $(".slct").val("").trigger("chosen:updated");
    })
});
$( function() {
    $('.price_from').html($( "#slider-range" ).slider( "values", 0 ) + ' {{ getCurrency() }}');
    $('.price_to').html($( "#slider-range" ).slider( "values", 1 ) + ' {{ getCurrency() }}');
    $( "#slider-range" ).slider({
    //   change: function( event, ui ) {
    //     var status = $.trim($("#status").val()),
    //     keyword = $.trim($("#keyword").val()),
    //     category = $.trim($("#category").val()),
    //     sub_category = $.trim($("#subCategory").val()),
    //     price = $.trim($("#amount").val()),
    //     user_id = $.trim($("#slct").val());
    //     $.ajax({
    //         type:"POST",
    //         url:"{{ route('manage.product') }}",
    //         data:{
    //             status:status,
    //             _token: '{{ csrf_token() }}',
    //             keyword:keyword,
    //             category:category,
    //             sub_category:sub_category,
    //             price:price,
    //             user_id:user_id,
    //         },
    //         beforeSend:function(){
    //             $(".loader").show();
    //         },
    //         success:function(resp){
    //             $(".listBody").html(resp);
    //             $(".loader").hide();
    //         }
    //     });
    // }
});
    $(document).ready(function() {
        $('.slct').chosen();
    })
    $(document).ready(function(){
        $('#col1_filter').change(function(){
            if($(this).val() != '')
            {
                var value  = $(this).val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('sub.cat.fetch') }}",
                    method:"POST",
                    data:{value:value, _token:_token},
                    success:function(response)
                    {
                        $('#col2_filter').html(response.result.output);
                    }
                })
            }
        });

        $('#input-select').change(function(){
          $('#col2_filter').val('');
        });
        $('.review_search').change(function(){
          $('#col9_filter').val('');
        });
        $(document).on("click",".status_change",function(e){
            var id = $(e.currentTarget).attr("data-id");
            if(confirm("@lang('admin_lang.do_u_want_to_ch_st')")){
                $.ajax({
                    url:"{{ route('status.product.update') }}",
                    type:"get",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        // console.log(resp);
                        if(resp != 0){
                            var st="";
                            if(resp.status == 'A'){
                                st="Active";
                                $(".success_msg").html("@lang('admin_lang.suc_status_change_to_active')");
                                $(".icon_change_"+id).removeClass("far fa-check-circle");
                                // $(".icon_change_"+id).removeClass("");
                                $(".icon_change_"+id).addClass("fas fa-ban");
                                $(".icon_change_"+id).attr("title","Block");
                            }else if(resp.status == 'I'){
                                st="Inactive";
                                $(".success_msg").html("Success! Status is changed to inactive!");
                                $(".icon_change_"+id).removeClass("fas fa-ban");
                                $(".icon_change_"+id).addClass("far fa-check-circle");
                                $(".icon_change_"+id).attr("title","@lang('admin_lang.unblock')");
                            }else if(resp.status == 'W'){
                                st="Awaiting Approval";
                                $(".success_msg").html("@lang('admin_lang.suc_chng_awaiting_approval')");
                                // $(".icon_change_"+id).removeClass("fas fa-ban");
                                $(".icon_change_"+id).addClass("fa fa-thumbs-up");
                                $(".icon_change_"+id).attr("title","@lang('admin_lang.awaiting_approval')");
                            }
                            $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("Product status is not changed! Either product is associated with user! ");

                        }
                        $(".session_success_msg").hide();
                        $(".session_error_msg").hide();
                        table.row('.tr_class_'+id).draw( false );
                    },
                    error:function(err){
                        console.log("error");
                    }
                });
            }
        }); 
        $(document).on("click",".delete_product",function(e){
            var id = $(e.currentTarget).attr("data-id");
            var t = $(this);
            if(confirm("Do you want to delete this product ?")){
                $.ajax({
                    url:"{{ route('delete.product') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        $(".success_msg").html(resp.message);
                        if(resp.output == 1){
                            $(".success_msg").html("You have successfully deleted this product!");
                            // $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            // $(".tr_"+id).hide();
                            t.parent().parent().remove();
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $("#error_msg").html("This product is associated with cart so you cannot delete this product or this product is deleted product already");
                        }
                        $(".session_success_msg").hide();
                        $(".session_error_msg").hide();
                    }
                });
            }
        });

        $(document).on("click",".set_featured_product",function(e){
            var id = $(e.currentTarget).data("id"),
            obj = $(this),
            reqData = {
                'jsonrpc' : '2.0',                
                '_token'  : '{{csrf_token()}}',
                'data'    : {
                    'id'    : id
                }
            },
            temp = table.row('.tr_class_'+id).data();
            console.log(temp);
            var msg =  "Do you want to set/remove this product from featured product?";
            if(temp.is_featured == 'Y'){
                var msg = "Do you want to remove"+temp.product_by_language.title+" from featured product ?";
            }else{
                var msg = "Do you want to set "+temp.product_by_language.title+" from featured product ?"
            }
            if(confirm(msg)){
                $.ajax({
                    url:"{{ route('set.featured.product') }}",
                    dataType: 'json',
                    data: reqData,
                    type: 'post',
                    success:function(resp){
                        if(resp.status == 1){
                            table.row('.tr_class_'+id).draw( false );
                            $(".success_msg").html(resp.message);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            
                        }else{
                            $(".error_msg").html(resp.message);
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                        }
                        $(".session_success_msg").hide();
                        $(".session_error_msg").hide();
                    }
                });
            }
        });
    });
});
</script>
<script>
    
</script>
<script>
    
</script>
@endsection