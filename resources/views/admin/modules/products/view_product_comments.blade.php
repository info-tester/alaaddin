@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | View Customer Profile') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | Product review Ratings & comments
@endsection
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" href="{{ asset('public/admin/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
<link href="{{ asset('public/admin/assets/vendor/fonts/circular-std/style.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('public/admin/assets/libs/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('public/admin/assets/vendor/fonts/fontawesome/css/fontawesome-all.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/buttons.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/select.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/fixedHeader.bootstrap4.css') }}">
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== --> 
            <!-- pageheader --> 
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.prod_rev_comts')</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{ route('manage.product') }}" class="breadcrumb-link">@lang('admin_lang_static.product_management')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Product review & ratings</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== --> 
            <!-- end pageheader --> 
            <!-- ============================================================== -->
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="row">
                <!-- ============================================================== --> 
                <!-- basic table  --> 
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Product review & ratings<a class="adbtn btn btn-primary" href="{{ route('manage.product') }}">@lang('admin_lang.back')</a></h5>
                        <div class="card-body">
                            <div class="manager-dtls tsk-div">
                                <div class="row fld">
                                    <div class="col-md-12">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.pro_cmts')</h4>
                                    </div>
                                    <div class="col-md-2" style="padding-right:0px;">
                                        @if(!empty(@$product->defaultImage))
                                        @php
                                        $image_path = 'storage/app/public/products/'.@$product->defaultImage->image; 
                                        @endphp
                                        @if(file_exists(@$image_path))
                                        <div class="uplodpic">
                                            <li>
                                                <img src="{{ URL::to('storage/app/public/products/'.@$product->defaultImage->image) }}" style="width: 100px;height: 100px;">
                                            </li>
                                        </div>
                                        
                                        @endif
                                        @else
                                        <div class="uplodpic">
                                            <li>
                                                <img src="{{ URL::to('public/frontend/images/default_product.png') }}" style="width: 100px;height: 100px;">
                                            </li>
                                        </div>
                                        @endif 
                                    </div>
                                    <div class="col-md-5" style="padding-left:0px;">
                                        <p><span class="titel-span">@lang('admin_lang.pro_title') </span> <span class="deta-span"><strong>:</strong> {{ @$product->productByLanguage->title}} </span> </p>
                                        <p><span class="titel-span">@lang('admin_lang.pro_avg_rat')</span> <span class="deta-span"><strong>:{{ number_format(@$product->avg_review,1) }}</strong> 

                                        </span> </p>
                                        <p><span class="titel-span">@lang('admin_lang.tot_revi')</span> <span class="deta-span"><strong>:{{ @$product->total_no_reviews }}</strong> 

                                        </span></p>
                                        <!-- <p><span class="titel-span">@lang('admin_lang.total_order')</span> <span class="deta-span"><strong>:</strong> </span></p> -->
                                    </div>
                                    <div class="col-md-5" style="padding-left:0px;">
                                        <p><span class="titel-span">Price ({{ getCurrency() }}) </span> <span class="deta-span"><strong>:</strong> {{ @$product->price }} /{{ @$product->productUnitMasters->unit_name }} </span> </p>
                                        @if(@$product->discount_price > 0)
                                        <p><span class="titel-span">Discount Price ({{ getCurrency() }}) </span> <span class="deta-span"><strong>:</strong> {{ @$product->discount_price }}/{{ @$product->productUnitMasters->unit_name }} </span> </p>
                                        @endif
                                        @if(@$product->discount_price > 0)
                                        <p><span class="titel-span">Discount Percentage (%) </span> <span class="deta-span"><strong>:</strong> {{ ((@$product->price - @$product->discount_price) / @$product->price) * 100}}% </span> </p>
                                        @endif
                                    </div>
                                    @if(@$product->images->where('is_default','N')->isNotEmpty())
                                    <div class="col-md-12">
                                        <h4 class="fultxt" style="margin-bottom:10px;">Product Images</h4>
                                    </div>
                                    <div class="col-md-12" style="padding-right:0px;">
                                        @if(@$product->images->isNotEmpty())
                                        @foreach(@$product->images as $images)
                                        @if($images->is_default == 'N')
                                        @php
                                        $image_path = 'storage/app/public/products/'.@$images->image; 
                                        @endphp
                                        @if(file_exists(@$image_path))
                                        <div class="uplodpic" style="float: none;">
                                            <li>
                                                <img src="{{ URL::to('storage/app/public/products/'.@$images->image) }}" style="width: 100px;height: 100px;">
                                            </li>
                                        </div>
                                        @else
                                        <div class="uplodpic">
                                            <li>
                                                <img src="{{ URL::to('public/frontend/images/default_product.png') }}" style="width: 100px;height: 100px;">
                                            </li>
                                        </div>
                                        @endif
                                        @endif
                                        @endforeach
                                        @endif 
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.prod_rev_comts')</h4>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered first">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('admin_lang.avg_review_ratings_point')</th>
                                            <th>@lang('admin_lang.ratings')</th>
                                            <th>@lang('admin_lang.review_comments')</th>
                                            <th>@lang('admin_lang.Name')</th>
                                            <th>@lang('admin_lang.re_post_on')</th>
                                            <th>@lang('admin_lang.Action')</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @if(@$order)
                                            @foreach(@$order as $key=>$ord)
                                                <tr>
                                                    <td>{{ $key+1}}</td>
                                                    <td>{{ $ord->rate }}</td>
                                                    <td>
                                                        @if($ord->rate == 5)
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        @elseif($ord->rate == 4)
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        @elseif($ord->rate == 3)
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        @elseif($ord->rate == 2)
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        @elseif($ord->rate == 1)
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                        @endif
                                                    </td>
                                                    <td>{{ $ord->comment }}</td>
                                                    <td>{{ $ord->customer_name }}</td>
                                                    <td>{{ $ord->review_date }}</td>
                                                    <td>
                                                        <a href="{{ route('delete.product.review', @$ord->id) }}" onclick="return confirm('Do you want to delete this review & ratings ? ');">
                                                            <i class=" fa fa-trash"></i>
                                                        </a>
                                                    </td>
                                                    
                                                    
                                                </tr>
                                            @endforeach
                                        @endif
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('admin_lang.avg_review_ratings_point')</th>
                                            <th>@lang('admin_lang.ratings')</th>
                                            <th>@lang('admin_lang.review_comments')</th>
                                            <th>@lang('admin_lang.Name')</th>
                                            <th>@lang('admin_lang.re_post_on')</th>
                                            <th>@lang('admin_lang.Action')</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== --> 
                <!-- end basic table  --> 
                <!-- ============================================================== --> 
            </div>
        </div>
        
        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif

<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>
{{-- <script src="{{ asset('public/admin/assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script> --}}
<script src="{{ asset('public/admin/assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/slimscroll/jquery.slimscroll.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/multi-select/js/jquery.multi-select.js') }}"></script> 
<script src="{{ asset('public/admin/assets/libs/js/main-js.js') }}"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

    });
</script>
@endsection