@extends('admin.layouts.app')
@section('title', 'Alaaddin | Admin | Product Discount')
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<style type="text/css">
  #showVariants{
    width: 100%;
  }
  .adedfrm{
    width: 100%;
    float: left;
  }
  .adedfrm .form-group {
    width: 19%;
    float: left;
    margin-right: 1%;
  }
  .col-form-label { font-size: 15px; width: 100%; }
  .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
    border: 1px solid #003eff;
    background: #007fff;
    font-weight: normal;
    text-align: center;
    color: #ffffff;
  }
  .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {
    border: 1px solid #c5c5c5;
    background: #f6f6f6;
    font-weight: normal;
    color: #454545;
    text-align: center;
  }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
<div class="dashboard-wrapper">
  <div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
      <!-- ============================================================== -->
      <!-- pageheader  -->
      <!-- ============================================================== -->
      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <div class="page-header">
            <h2 class="pageheader-title">@lang('admin_lang_static.product_discount')</h2>
            <div class="page-breadcrumb">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang_static.dashboard')</a></li>
                  <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang_static.product_discount')</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <!-- ============================================================== -->
      <!-- end pageheader  -->
      <!-- ============================================================== -->
      @if (session()->has('success'))
      <div class="alert alert-success vd_hidden" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
          <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
      </div>
      @elseif ((session()->has('error')))
      <div class="alert alert-danger vd_hidden" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
          <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong>{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
      </div>
      @endif
      <div class="ecommerce-widget">
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
              <h5 class="card-header">@lang('admin_lang_static.product_discount')</h5>
              <div class="card-body">
                <form action="{{ route('store.product.discount') }}" method="POST" id="dependent_form">
                  @csrf
                  <div class="row" id="appendRow" style="margin: 0;">
                    <input type="hidden" name="product_id" value="{{ $product_id }}">
                    <input type="hidden" name="slug" id="slug" value="{{ $slug }}">
                    {{-- @dd($fetch_data) --}}
                    @foreach($fetch_data as $key=>$fd)
                    <div class="adedfrm">
                      @foreach($fd->productVariantDetails as $key1=>$pvd)
                      @if($pvd->getVariant->type == 'P')
                      <div class="form-group">
                        {{-- @if($key == 0) --}}
                        <label for="input-select" class="col-form-label">{{ $pvd->variantByLanguage->name }}</label>
                        {{-- @endif --}}
                        <input type="text" class="form-control required" name="" value="{{ $pvd->variantValueName->variantValueByLanguage->name }}" disabled="">
                      </div>
                      @endif
                      @endforeach
                      <div class="form-group">
                        {{-- @if($key == 0) --}}
                        <label for="price" class="col-form-label">@lang('admin_lang_static.price')</label>
                        {{-- @endif --}}
                        <input id="price" type="number" class="form-control required" name="price[{{ $fd->id }}]" placeholder="Stock Quantity" value="{{ $fd->price }}" min="0" readonly="">
                      </div>
                      <div class="form-group">
                        {{-- @if($key == 0) --}}
                        <label for="discount_price" class="col-form-label">@lang('admin_lang_static.discount_price')</label>
                        {{-- @endif --}}
                        <input id="discount_price" type="text" class="form-control required" name="discount_price[{{ $fd->id }}]" placeholder="Discount Price" value="{{ $fd->discount_price }}">
                      </div>
                      <div class="form-group">
                        {{-- @if($key == 0) --}}
                        <label for="from_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
                        {{-- @endif --}}
                        <input type="text" class="form-control from_date from_date{{ $fd->id }} datepicker required" name="from_date[{{ $fd->id }}]" data-val="{{ $fd->id }}" placeholder="From Date" value="{{ $fd->from_date }}" readonly="">
                        <span class="text-danger"></span>
                      </div>
                      <div class="form-group">
                        {{-- @if($key == 0) --}}
                        <label for="to_date" class="col-form-label">@lang('admin_lang_static.to_date')</label>
                        {{-- @endif --}}
                        <input type="text" class="form-control to_date to_date{{ $fd->id }} datepicker required" data-val="{{ $fd->id }}" name="to_date[{{ $fd->id }}]" placeholder="To Date" value="{{ $fd->to_date }}" readonly="">
                        <span class="text-danger"></span>
                      </div>
                    </div>
                    @endforeach
                  </div>
                  <div class="form-group">
                    <label for="inputPassword" class="col-form-label"></label>
                    {{-- <a href="{{ route('merchant.add.product.step.two',$slug) }}" class="btn btn-primary nxtop">Previous</a> --}}
                    <button type="submit" class="btn btn-primary nxtop" id="submit">@lang('admin_lang_static.save')</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- footer -->   
@include('admin.includes.footer')
<!-- end footer -->
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $(".datepicker").datepicker({dateFormat: "yy-mm-dd",
     defaultDate: new Date(),
     minDate: new Date(),
     changeMonth: true,
     changeYear: true,
     yearRange: '-100:+0'
   }); 

    $(".to_date").change(function(){
      var add_id    = $(this).data('val');
      var from_date = $(".from_date"+add_id).val();
      var to_date   = $(this).val();
      if(to_date < from_date && from_date != '') {
        $($(this).next().html('To date can not less than from date.'));
        $("#submit").attr("disabled", true);
      } else {
        $($(this).next().html(''));
        $($('.from_date'+add_id).next().html(''));
        $("#submit").attr("disabled", false);
      }
    })
    $(".from_date").change(function(){
      var add_id    = $(this).data('val');
      var from_date = $(this).val();
      var to_date   = $(".to_date"+add_id).val();
      if(to_date < from_date && to_date != '') {
        $($(this).next().html('From date can not greater than to date.'));
        $("#submit").attr("disabled", true);
      } else {
        $($(this).next().html(''));
        $($('.to_date'+add_id).next().html(''));
        $("#submit").attr("disabled", false);
      }
    })
  });
</script>
<script>
  $(document).ready(function(){ 
    $("#dependent_form" ).validate({
      rules: {
        "from_date[]":{
         required:true
       },
       "to_date[]":{
         required:true
       },
       "discount_price[]":{
         required:true,
         number:true
       }
     },
     errorPlacement: function (error , element) {
              //toastr:error(error.text());
            }
          });
  });
</script>
<script>
  $(document).ready(function() {
    $('.slct').chosen();
  })
</script>
{{-- for fetching models of brand  --}}
<script>
  $(document).ready(function(){

   $('#input-select').change(function(){
    if($(this).val() != '')
    {
     var value  = $(this).val();
     var _token = $('input[name="_token"]').val();
     $.ajax({
      url:"{{ route('sub.cat.fetch') }}",
      method:"POST",
      data:{value:value, _token:_token},
      success:function(result)
      {
       $('#subCategory').html(result);
     }

   })
   }
 });

   $('#input-select').change(function(){
    $('#subCategory').val('');
  }); 

 });
</script>
{{-- For insert variants with ajax --}}
{{-- <script type="text/javascript">
    $(document).ready(function(){
      $('#add').on('click', function(){
         $.ajax({
              url:"{{ route('store.product.variant') }}",
              method:"POST",
              data: { params:$('#dependent_form').serializeArray(), _token:'{{ @csrf_token() }}'},
              
              dataType:"json",
              success:function(data)
              {
               var html = '';
               if(data.errors)
               {
                html = '<div class="alert alert-danger">';
                for(var count = 0; count < data.errors.length; count++)
                {
                 html += '<p>' + data.errors[count] + '</p>';
                }
                html += '</div>';
               }
               if(data.success)
               {
                html = '<div class="alert alert-success">' + data.success + '</div>';
                $('#sample_form')[0].reset();
                $('#user_table').DataTable().ajax.reload();
               }
               $('#form_result').html(html);
              }
          })
      });
    });
  </script> --}}
  @endsection