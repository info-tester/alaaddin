@extends('admin.layouts.app')
@section('title', 'Alaaddin | Admin | Edit Product Step One')
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<style type="text/css">
    #showVariants{
        width: 100%;
    }

    .adedfrm{
        width: 100%;
        float: left;
    }
    .adedfrm .form-group {
        width: 19%;
        float: left;
        margin-right: 1%;
    }
    .col-form-label { font-size: 15px; width: 100%; }
    .custom-control-indicator {
        background-color: #e3e8ef;
        box-shadow: none !important;
    }
    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
        border: 1px solid #003eff;
        background: #007fff;
        font-weight: normal;
        text-align: center;
        color: #ffffff;
    }
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {
        border: 1px solid #c5c5c5;
        background: #f6f6f6;
        font-weight: normal;
        color: #454545;
        text-align: center;
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang_static.edit_product')</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ ('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang_static.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang_static.edit_product')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())                     
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>                                
                    {{ $errors->first() }}
                </strong>
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang_static.edit_product')<a class="adbtn btn btn-primary" href="{{ route('manage.product') }}">@lang('admin_lang_static.back')</a></h5>
                            <div class="card-body">
                                @if(@$total_orders > 0) 
                                <div class="alert alert-warning"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Cannot change category and sub category because the product is sold multiple times.</div>
                                @endif
                                @if (session()->has('success'))
                                <div class="alert alert-success vd_hidden" style="display: block;">
                                    <a class="close" data-dismiss="alert" aria-hidden="true">
                                    <i class="icon-cross"></i>
                                    </a>
                                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                                    <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                                </div>
                                @elseif ((session()->has('error')))
                                <div class="alert alert-danger vd_hidden" style="display: block;">
                                    <a class="close" data-dismiss="alert" aria-hidden="true">
                                    <i class="icon-cross"></i>
                                    </a>
                                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                                    <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                                </div>
                                @endif
                                <form action="{{ route('update.product',$product->slug) }}" method="post" id="storeProduct">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="input-select" class="col-form-label">@lang('admin_lang_static.merchant')</label>
                                            <select data-placeholder="Choose a Merchant..." class="select slt slct required" name="user_id" tabindex="3" id="slct">
                                                <option value="">@lang('admin_lang_static.select_merchant')</option>
                                                @foreach(@$user as $us)
                                                <option value="{{ @$us->id }}" @if(@$product->productMarchant->id == @$us->id) selected @endif>{{ @$us->fname }} {{ @$us->lname }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="input-select" class="col-form-label">@lang('admin_lang_static.category')</label>
                                            <select class="form-control required" name="category" id="category" @if(@$total_orders > 0) disabled="" @endif>
                                                <option value="">@lang('admin_lang_static.select_category')</option>
                                                @foreach(@$category as $cat)
                                                <option value="{{ @$cat->id }}" @if(@$pro_cat->category_id == $cat->id) selected @endif>{{ @$cat->categoryByLanguage->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="input-select" class="col-form-label">@lang('admin_lang_static.sub_category')</label>
                                            <select class="form-control required" name="sub_category" id="subCategory" @if(@$total_orders > 0) disabled="" @endif>
                                                <option value="">@lang('admin_lang_static.select_sub_category')</option>
                                                @foreach(@$subCategory as $subCat)
                                                <option value="{{ @$subCat->id }}" @if(@$pro_sub_cat->category_id == $subCat->id) selected @endif>{{ @$subCat->categoryByLanguage->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 price" @if(@$price_variant > 0) style="display: none;" @endif>
                                            <label for="inputText3" class="col-form-label required"> @lang('admin_lang_static.price')</label>
                                            <input type="text" class="form-control required" name="price" placeholder="Price" value="{{ $product->price }}" onkeypress='validate(event)'>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 stock" @if(@$price_variant > 0 || @$stock_variant > 0) style="display: none;" @endif>
                                            <label for="inputText3" class="col-form-label required"> @lang('admin_lang_static.stock')</label>
                                            <input type="text" class="form-control required" name="stock" placeholder="Stock" value="{{ $product->stock }}" onkeypress='validate(event)'>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 stock" @if(@$price_variant > 0 || @$stock_variant > 0) style="display: none;" @endif>
                                            <label for="inputText3" class="col-form-label required"> @lang('admin_lang_static.weight') (Grams)</label>
                                            <input type="text" class="form-control required" name="weight" placeholder="Weight" value="{{ $product->weight }}" onkeypress='validate(event)'>
                                        </div>

                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 price" @if(@$price_variant > 0) style="display: none;" @endif>
                                            <label for="discount_price" class="col-form-label">@lang('admin_lang_static.discount_price')</label>
                                            <input id="discount_price" type="text" class="form-control required" name="discount_price" placeholder="Discount Price" value="{{ $product->discount_price }}" onkeypress='validate(event)'>
                                        </div>

                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 price" @if(@$price_variant > 0) style="display: none;" @endif>
                                            <label for="from_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
                                            <input type="text" class="form-control from_date datepicker @if($product->discount_price > 0) required @endif" name="from_date" id="from_date" placeholder="From Date" @if($product->discount_price > 0) value="{{ $product->from_date }}" @endif readonly="">
                                            <span class="text-danger"></span>
                                        </div>

                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 price" @if(@$price_variant > 0) style="display: none;" @endif>
                                            <label for="to_date" class="col-form-label">@lang('admin_lang_static.to_date')</label>
                                            <input type="text" class="form-control to_date datepicker @if($product->discount_price > 0) required @endif" name="to_date" id="to_date" placeholder="To Date" @if($product->discount_price > 0) value="{{ $product->to_date }}" @endif readonly="">
                                            <span class="text-danger"></span>
                                        </div>
                                        {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="input-select" class="col-form-label">@lang('admin_lang_static.brand')</label>
                                            <select class="form-control required" name="brand_id" id="brand">
                                                <option value="">@lang('admin_lang_static.select_brand')</option>
                                                @foreach(@$brand as $b)
                                                <option value="{{ $b->id }}" @if($product->productBarnd->id == $b->id) selected @endif>{{ $b->brandDetailsByLanguage->title }}</option>
                                                @endforeach
                                            </select>
                                        </div> --}}
                                        @foreach(@$language as $lang)
                                        @foreach(@$pro_details as $proDetail)  
                                        @php
                                        if($lang->id == $proDetail->language_id){
                                        $title = $proDetail->title;
                                        @endphp      
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="inputText3" class="col-form-label required">@lang('admin_lang_static.title')</label>
                                            <input type="text" class="form-control required" name="title[{{ $lang->id }}]" placeholder="Title" value="{{ @$title }}">
                                        </div>
                                        @php
                                        }
                                    @endphp
                                    @endforeach
                                    @endforeach
                                    @foreach(@$language as $lang)
                                    @foreach(@$pro_details as $proDetail) 
                                    @php
                                    if($lang->id == $proDetail->language_id){
                                    $description = $proDetail->description;
                                    @endphp     
                                    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <label for="exampleFormControlTextarea1">@lang('admin_lang_static.description') </label>
                                        <textarea class="form-control required" name="description[{{ $lang->id }}]" rows="3">{{ @$description }}</textarea>
                                    </div>
                                    @php
                                    }
                                @endphp
                                @endforeach
                                @endforeach
                                {{-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" name="is_featured" @if($product->is_featured == 'Y') checked="" @endif><span class="custom-control-label">@lang('admin_lang_static.is_featured')</span>
                                    </label>
                                    <label class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" name="is_new" @if($product->is_new == 'Y') checked="" @endif>
                                        <span class="custom-control-label">@lang('admin_lang_static.is_new')</span>
                                    </label>
                                    <label class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" name="in_offers" @if($product->in_offers == 'Y') checked="" @endif>
                                        <span class="custom-control-label">@lang('admin_lang_static.in_offers')</span>
                                    </label>
                                </div> --}}
                                {{-- <div class="search_variant w-100"></div> --}}
                                {{-- <div class="informative_variant w-100"></div> --}}
                                {{-- <div class="existing_variant">
                                    @foreach(@$variant as $var)
                                    @if($var->variantValues->isNotEmpty())
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h4 class="fultxt">{{ $var->variantByLanguage->name }} 
                                            
                                        </h4>
                                    </div>
                                    <input type="hidden" name="variant_id[]">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        @foreach(@$var->variantValues as $varVal)
                                            <label class="custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" class="custom-control-input" name="variant_value_id[]" value="{{ $varVal->id }}"
                                                @foreach(@$pro_variants as $pro_var)
                                                @if(@$pro_var->variant_id == @$var->id && @$pro_var->variant_value_id == @$varVal->id)
                                                checked="" 
                                                @endif
                                                @endforeach
                                                ><span class="custom-control-label">{{ $varVal->variantValueByLanguage->name }}</span>
                                            </label>
                                            @endforeach
                                        </div>
                                        @endif
                                        @endforeach
                                    </div> --}}
                                    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <label for="inputPassword" class="col-form-label"></label>
                                        <button type="submit" class="btn btn-primary nxtop" id="submit">@lang('admin_lang_static.next')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- footer -->   
@include('admin.includes.footer')
<!-- end footer -->
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>  
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $(".datepicker").datepicker({dateFormat: "yy-mm-dd",
         defaultDate: new Date(),
         minDate: new Date(),
         changeMonth: true,
         changeYear: true,
         yearRange: '-100:+0'
     }); 

        $(".to_date").change(function(){
          var from_date = $(".from_date").val();
          var to_date   = $(this).val();
          if(to_date < from_date && from_date != '') {
            $($(this).next().html('To date can not less than from date.'));
            $("#submit").attr("disabled", true);
        } else {
            $($(this).next().html(''));
            $($('.from_date').next().html(''));
            $("#submit").attr("disabled", false);
        }
    })
        $(".from_date").change(function(){
          var from_date = $(this).val();
          var to_date   = $(".to_date").val();
          if(to_date < from_date && to_date != '') {
            $($(this).next().html('From date can not greater than to date.'));
            $("#submit").attr("disabled", true);
        } else {
            $($(this).next().html(''));
            $($('.to_date').next().html(''));
            $("#submit").attr("disabled", false);
        }
    })
    });
</script>
<script src="{{ URL::to('public/frontend/tiny_mce/tinymce.min.js') }}"></script>
<script>
    $(document).ready(function(){ 
        tinyMCE.init({
            mode : "textareas",
            // editor_selector : "desc",
            menubar: false,
            statusbar: false,
            toolbar: false,
            height: '320px',
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            toolbar: ' undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link ',
            images_upload_url: '{{ URL::to('storage/app/public/uploads/content_image/') }}',
        });

        $.validator.setDefaults({ ignore: ":hidden:not(.slct)" });
        $("#storeProduct" ).validate({
          rules:{
              "user_id":{
                  required:true
              },
              "discount_price":{
               required:true,
               number:true
           },
           "weight":{
               required:true,
               number:true
           }
       },
       errorPlacement: function (error , element) {
              //toastr:error(error.text());
          }
      });
  });
</script>
<script>
    $(document).ready(function() {
        $('.slct').chosen();
    })
</script>
{{-- for fetching models of brand  --}}
<script>
    $(document).ready(function(){
        $('#category').change(function(){
            if($(this).val() != '') {
                var value  = $(this).val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('sub.cat.fetch') }}",
                    method:"POST",
                    data:{value:value, _token:_token},
                    success:function(response) {
                        $('#subCategory').html(response.result.output);
                        $('#brand').html(response.result.brand);
                        $('.price').hide()
                        $('.stock').hide()
                    }
                })
            }
        });

        $('#input-select').change(function(){
            $('#subCategory').val('');
        }); 

        $('#subCategory').change(function() {
            $('.price').hide()
            $('.stock').hide()
            if($(this).val() != '') {
                var reqData = {
                    _token: '{{ csrf_token() }}',
                    params: {
                        category_id: $('#category').val(),
                        sub_category_id: $(this).val()
                    }
                }
                $.ajax({
                    url:"{{ route('check.price.dependency') }}",
                    method:"POST",
                    data: reqData,
                    success:function(response) {
                        if(response.error) {
                            console.log(response.error)
                        } else {
                            if(response.result.price_dependent == 0 && response.result.stock_dependent == 0) {
                                $('.price').show()
                                $('.stock').show()
                            }
                            else if(response.result.price_dependent == 0 && response.result.stock_dependent != 0) {
                                $('.price').show()
                            }
                        }
                        // $('#subCategory').html(result);
                    }
                })

                // ajax for fetching variants depending on category and sub category
                $.ajax({
                    url:"{{ route('get.variants') }}",
                    method:"POST",
                    data: reqData,
                    success:function(response) {
                        if(response.error) {
                            console.log(response.error)
                        } else {
                            if(response.result.search_variants) {
                                var html = ''
                                response.result.search_variants.forEach(function(item, index){
                                    html+= '<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">\
                                    <h4 class="fultxt">' +item.variant_by_language.name+ '</h4>\
                                    </div>\
                                    <input type="hidden" name="variant_id[]">\
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">'
                                    item.variant_values.forEach(function(item1, index1) {
                                        html+= '<label class="custom-control custom-checkbox custom-control-inline">\
                                        <input type="checkbox" class="custom-control-input required search_variant_field" name="variant_value_id[]" value="'+item1.id+'"><span class="custom-control-label">'+item1.variant_value_by_language.name+'</span>\
                                        </label>'
                                    })
                                    html+= '</div>'
                                })
                                $('.search_variant').html(html)
                            }

                            if(response.result.informative_variants) {
                                var html1 = ''
                                response.result.informative_variants.forEach(function(item, index){
                                    html1+= '<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">\
                                    <h4 class="fultxt">' +item.variant_by_language.name+ '</h4>\
                                    </div>\
                                    <input type="hidden" name="variant_id[]">\
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">'
                                    item.variant_values.forEach(function(item1, index1) {
                                        html1+= '<label class="custom-control custom-checkbox custom-control-inline">\
                                        <input type="checkbox" class="custom-control-input" name="variant_value_id[]" value="'+item1.id+'"><span class="custom-control-label">'+item1.variant_value_by_language.name+'</span>\
                                        </label>'
                                    })
                                    html1+= '</div>'
                                })
                                $('.informative_variant').html(html1)
                            }
                        }
                        $('.existing_variant').html('');
                    }
                })
            }
            
        });

$('#discount_price').blur(function() {
    if($(this).val() > 0) {
        $('#from_date').addClass('required');
        $('#to_date').addClass('required');
    } else {
        $('#from_date').removeClass('required');
        $('#to_date').removeClass('required');                
    }
});
});
function validate(evt) {
    var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>
@endsection