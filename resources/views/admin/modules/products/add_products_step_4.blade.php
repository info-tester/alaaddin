@extends('admin.layouts.app')
@section('title', 'Alaaddin | Admin | Add Product Step Four')
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang_static.add_product')</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang_static.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang_static.add_product')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang_static.add_product')<span class="sub-header-size"> > {{ @$productName }} > @lang('admin_lang_static.step4_of_4_image')</span></h5>
                            <div class="card-body">
                                <form action="{{ route('store.product.step.four') }}" method="POST" id="image_form" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <input type="file" class="custom-file-input inpt" name="image[]" id="gallery-photo-add" multiple="" accept="image/*">
                                            <label class="custom-file-label extrlft" for="customFile">@lang('admin_lang_static.upload_image')</label>
                                            <div class="errorImage" style="color: red;"></div>
                                        </div>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            @if(count($productImg)>0)
                                            <div class="uplodpic">
                                                @foreach($productImg as $proImg)
                                                <li class=" remove-rw-{{ $proImg->id }}">
                                                    <img src="{{url('storage/app/public/products/'.@$proImg->image)}}">
                                                    @if($proImg->is_default == 'Y')
                                                    <span class="default-image-span">@lang('admin_lang_static.default')</span>
                                                    @else
                                                    <div class="action-opt">
                                                        <span class="set-default-img">
                                                            <a href="{{ route('set.default.product',$proImg->id) }}" title="Set default" class="default-image" data-id="{{ $proImg->id }}">
                                                                <i class="fas fa-check"></i>
                                                            </a>
                                                        </span>
                                                        <span class="remove-img">
                                                            <a href="javascript:;" title="Remove" class="remove-image" data-id="{{ $proImg->id }}">
                                                                <i class="fas fa-times"></i>
                                                            </a>
                                                        </span>
                                                    </div>
                                                    @endif
                                                </li>
                                                @endforeach
                                            </div>
                                            @endif
                                            <div class="uplodpic gallery">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="col-form-label"></label>
                                        @if(@$price_variant > 0 || @$stock_variant > 0)
                                        <a href="{{ route('add.product.step.three',$product->slug) }}" class="btn btn-primary nxtop">Previous</a>
                                        @else
                                        <a href="{{ route('edit.product',$product->slug) }}" class="btn btn-primary nxtop">@lang('admin_lang_static.previous')</a>
                                        @endif
                                        {{-- <a href="#" class="btn btn-primary nxtop">Save</a> --}}
                                        <button type="submit" class="btn btn-primary nxtop">@lang('admin_lang_static.save')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script>
    $(document).ready(function(){ 
      $("#image_form" ).validate({
          errorPlacement: function (error , element) {
              //toastr:error(error.text());
          }
      });
  });
</script>
<script>
    $(document).ready(function() {
        $('.slct').chosen();
    })
</script>
{{-- for fetching models of brand  --}}
<script>
    $(document).ready(function(){
        
       $('#input-select').change(function(){
          if($(this).val() != '')
          {
             var value  = $(this).val();
             var _token = $('input[name="_token"]').val();
             $.ajax({
                url:"{{ route('sub.cat.fetch') }}",
                method:"POST",
                data:{value:value, _token:_token},
                success:function(result)
                {
                   $('#subCategory').html(result);
               }
               
           })
         }
     });
       
       $('#input-select').change(function(){
          $('#subCategory').val('');
      }); 
       
   });
</script>
<script type="text/javascript">
    $(function() {
        // Multiple images preview in browser
        var imagesPreview = function(input, placeToInsertImagePreview) {
            
            if (input.files) {
                var filesAmount = input.files.length;
                
                for (i = 0; i < filesAmount; i++) {
                    var file = input.files[i];
                    var fileName = file.name,
                    fileSize = file.size,
                    fileNameExtArr = fileName.split("."),
                    ext = fileNameExtArr[1];
                    console.log("fileSize : "+fileSize);
                    ext = ext.toLowerCase();
                    var reader = new FileReader();
                    if(ext == "jpg" || ext == "jpeg" || ext == "png"){
                        if(fileSize <= 152446){
                            reader.onload = function(event) {
                                var new_html = '<li><img src="'+event.target.result+'"></li>';
                                $('.gallery').append(new_html);
                                // $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                            }

                            reader.readAsDataURL(input.files[i]);

                        }else{
                         $(".errorImage").text("Image size is too big!!");
                        }
                    }else{
                        $(".errorImage").text("Image size is too big!");
                    }
                }
            }
            
        };
        
        $('#gallery-photo-add').on('change', function() {

            imagesPreview(this, 'div.gallery');
            $('.gallery').html('');
        });
    });
    
</script>
{{-- for remove row  --}}
<script>
    $(document).ready(function(){
        $('body').on('click','.remove-image',function(){
            var var_id = $(this).data('id');
            var reqDataa = {
                jsonrpc: '2.0'
            };
            
            $.ajax({
                type:'GET',
                url : '{{ url('admin/remove-image') }}/'+var_id,
                data:reqDataa,
                success:function(response){
                    if(response.sucess) {
                        // if(response.sucess.result == 'Add') {
                            $(".remove-rw-"+var_id).hide();
                        // }
                        
                    } else {
                        $(".remove-rw-"+var_id).hide();
                        // console.log(".remove-rw-"+var_id);
                    }
                }
            });     
            
        });   
        
    });
    
</script>
@endsection