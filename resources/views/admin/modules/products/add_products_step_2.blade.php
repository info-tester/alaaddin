@extends('admin.layouts.app')
@section('title', 'Alaaddin | Admin | Add Product Step Two')
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<style type="text/css">
    #showVariants{
    width: 100%;
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang_static.add_product')</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang_static.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang_static.add_product')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="alert alert-danger vd_hidden" id="erMsg" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang_static.add_product')<span class="sub-header-size"> > {{ @$productName }} > @lang('admin_lang_static.step2_of_4_pricing')</span></h5>
                            <div class="card-body">
                                {{-- action="{{ route('store.product.step.two') }}" --}}
                                <form action="" method="POST" id="dependent_form">
                                    @csrf
                                    <div class="row" id="appendRow" style="margin: 0;">
                                        <input type="hidden" name="product_id" value="{{ $product_id }}">
                                        <input type="hidden" name="slug" id="slug" value="{{ $slug }}">
                                        <div class="adedfrm">
                                            @foreach($price_variant as $pv)
                                            <div class="form-group">
                                                <label for="input-select" class="col-form-label">{{ @$pv->variantByLanguage->name }}</label>
                                                <select class="form-control required price_dependent" name="price_variant[]">
                                                    <option value="">Select {{ @$pv->variantByLanguage->name }}</option>
                                                    @foreach($pv->variantValues as $pvVal)
                                                    <option value="{{ $pvVal->id }}">{{ $pvVal->variantValueByLanguage->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @endforeach
                                            @foreach($stock_variant as $key=>$row)
                                            <div class="form-group">
                                                <label for="inputText3" class="col-form-label">{{ @$row->variantByLanguage->name }}</label>
                                                <select class="select slt slct required" name="stock_variant[{{ $key }}][]" tabindex="3"  multiple="">
                                                    @foreach(@$row->variantValues as $svVal)
                                                    <option value="{{ @$svVal->id }}">{{ @$svVal->variantValueByLanguage->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @endforeach
                                            @if($price_variant->count())
                                            <div class="form-group">
                                                <label for="price" class="col-form-label">@lang('admin_lang_static.price')</label>
                                                <input type="text" class="form-control required number" id="product_price" name="price" placeholder="Enter Price">
                                            </div>
                                            @endif
                                            <div class="form-group useadmrgn">
                                                <button type="submit" id="onSubmit" class="plsad" ><i class="fas fa-plus" id="add_btn"></i></button>
                                            </div>
                                        </div>
                                        <div id="showVariants">
                                            @php
                                            $i=0;
                                            @endphp
                                            @foreach($fetch_data as $fd)
                                            {{-- @dump($fd) --}}
                                            @if($i!=0)
                                            <div class="adedfrm remove-rw-{{ $fd->group_id }}">
                                                @else
                                                <div class="adedfrm">
                                                    @endif
                                                    @foreach($fd->productVariantDetails as $pvd)
                                                    <div class="form-group">
                                                        @if($i==0)
                                                        <label style="font-weight: 600" for="input-select" class="col-form-label">{{ $pvd->variantByLanguage->name }}</label>
                                                        @endif
                                                        <span class="show-detail remove-rw-{{ $fd->id }}">{{ variantValues($pvd->product_variant_id, $fd->group_id, $pvd->variant_id,  $pvd->getVariantValueId) }}</span>
                                                    </div>
                                                    @endforeach
                                                    @if($price_variant->count())
                                                    <div class="form-group">
                                                        @if($i==0)
                                                        <label style="font-weight:600" for="price" class="col-form-label">@lang('admin_lang_static.price')</label>
                                                        @endif
                                                        <span class="show-detail remove-rw-{{ $fd->id }}">{{ $fd->price }} {{ getCurrency() }}</span>
                                                    </div>
                                                    @endif
                                                    <div class="form-group">
                                                        @if($i==0)
                                                        <label style="font-weight:600" class="col-form-label">@lang('admin_lang_static.action') &nbsp;</label>
                                                        @endif
                                                        <a href="javascript:;" title="Remove" class="removeVar remove-rw-{{ $fd->group_id }}" data-id="{{ $fd->group_id }}"><i class="fas fa-times"></i></a>
                                                    </div>
                                                </div>
                                                @php
                                                $i++;
                                                @endphp
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword" class="col-form-label"></label>
                                            <a href="{{ route('edit.product',$slug) }}" class="btn btn-primary nxtop">@lang('admin_lang_static.previous')</a>
                                            <a href="{{ route('add.product.step.three',$slug) }}" class="btn btn-primary nxtop" @if(count($fetch_data) < 1) style="display: none;" @endif id="nxtBtn">@lang('admin_lang_static.next')</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script>
    $(document).ready(function(){ 
        $.validator.setDefaults({ ignore: ":hidden:not(.slct)" });
        $("#dependent_form" ).validate({
            rules:{
                "price":{
                    required:true,
                    number:true
                },
                "stock_variant[]":{
                    required:true,
                }
            },
            errorPlacement: function (error , element) {
              //toastr:error(error.text());
            },
            submitHandler: function(form) {
                console.log(form)
            } 
        });
      
        $('body').on('click','#add_btn',function(){
            
            // var counter = 0;
            /*$('.slct').each(function() {
                if($(this).val() == '') {
                    $(this).next().addClass('error')
                } else {
                    $(this).next().removeClass('error')    
                }
            })*//*
            if($('.slct').val() == null){
                $('.chosen-container').addClass('error');
            }
            if($('.slct').val() != null){
                $('.chosen-container').removeClass('error');
            }*/
        });
        $('body').on('click','#onSubmit',function(){
            var product_price = $('#product_price').val();
            if(product_price == 0){
                $('#product_price').addClass('error');
                return false;
            }else{
                $('#product_price').removeClass('error');
                return true;
            }
        })
    });
</script>
<script>
    $(document).ready(function() {
      $('.slct').chosen();
    })
</script>

{{-- For insert variants with ajax --}}
<script type="text/javascript">
    $(document).ready(function(){
        $('#dependent_form').submit(function(event){
            var emptyStockCount = 0, emptyPriceCount = 0;
            $('.slct').each(function() {
                if($(this).val() == '') {
                    emptyStockCount += 1;
                    $(this).next().addClass('error')
                } else {
                    console.log($(this).next())
                    $(this).next().removeClass('error')    
                }
            });

            $('.price_dependent').each(function() {
                if($(this).val() == '') {
                    emptyPriceCount += 1;
                    $(this).addClass('error')
                } else {
                    $(this).removeClass('error')    
                }
            })

            if(emptyStockCount > 0 || emptyPriceCount > 0) {
                return false;
            }

            var $this = $(this);
            event.preventDefault();
            $('#add_btn').attr('disabled', 'disabled');
            var form_data = new FormData(this);
            $.ajax({
                url:"{{ route('store.product.variant') }}",
                method:"POST",
                data: form_data,
                contentType: false,
                cache:false,
                processData: false,
                dataType:"json",
                success:function(response) {
                    $('#add_btn').removeAttr('disabled');
                    $(".slct").val('').trigger("chosen:updated");
                    $this[0].reset();
                    var html = '';
                    if(response.error) {
                        html = (response.error.message) ;
                        $('#erMsg').show();
                        $('#form_result').html(html);
                    } else {
                        $('#erMsg').hide();
                        $("#showVariants").html(response.result.output);
                        $('#nxtBtn').show();   
                    }
                }
            });
        });
    });
</script>
{{-- for remove row  --}}
<script>
    $(document).ready(function(){
      $('body').on('click','.removeVar',function(){
        var var_id = $(this).data('id');
        var reqDataa = {
          jsonrpc: '2.0'
        };
        
        $.ajax({
          type:'GET',
          url : '{{ url('admin/remove-variant') }}/'+var_id,
          data:reqDataa,
          success:function(response){
            if(response.sucess) {
              $(".remove-rw-"+var_id).hide();
              if(response.success.proVarCount < 1) {
                $('#nxtBtn').hide();          
              }              
            } else {
              console.log(response.success.proVarCount);
              $(".remove-rw-"+var_id).hide();
              if(response.success.proVarCount < 1) {
                $('#nxtBtn').hide();          
              }              
            }
          }
        });     
      });
    });

    function validate(evt) {
      var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
            key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
        }
        var regex = /[0-9]|\./;
        if( !regex.test(key) ) {
          theEvent.returnValue = false;
          if(theEvent.preventDefault) theEvent.preventDefault();
        }
      }
      
</script>
@endsection