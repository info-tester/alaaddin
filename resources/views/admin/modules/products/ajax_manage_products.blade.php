<table class="table table-striped table-bordered first">
    <thead>
        <tr>
            <th>@lang('admin_lang_static.image')</th>
            <th>@lang('admin_lang_static.category')</th>
            <th>@lang('admin_lang_static.sub_category')</th>
            <th>@lang('admin_lang_static.merchant')</th>
            <th>@lang('admin_lang_static.title')</th>
            <th>@lang('admin_lang_static.price')</th>
            <th>@lang('admin_lang_static.is_featured')</th>
            <th>@lang('admin_lang_static.status')</th>
            <th>@lang('admin_lang_static.merchant_status')</th>
            <th>@lang('admin_lang_static.action')</th>
        </tr>
    </thead>
    <tbody>
        @foreach($product as $pro)
        <tr>
            <td>
                <div class="m-r-10">
                    @if(@$pro->defaultImage->image)
                    <img src="{{url('storage/app/public/products/'.@$pro->defaultImage->image)}}" alt="user" class="rounded" width="45">
                    @else
                    <img src="{{url('public/admin/assets/images/eco-product-img-1.png')}}" alt="user" class="rounded" width="45">
                    @endif
                </div>
            </td>
            @if(count(@$pro->productCategory)>0)
            @foreach(@$pro->productCategory as $proCat)
            <td>{{ @$proCat->Category->categoryByLanguage->title }}</td>
            @endforeach
            @else
            <td></td>
            <td></td>
            @endif
            <td>{{ @$pro->productMarchant->fname }} {{ @$pro->productMarchant->lname }}</td>
            <td>{{ @@$pro->productByLanguage->title }}</td>
            <td>{{ @$pro->price }} {{ getCurrency() }}</td>
            <td>
                @if($pro->is_featured == 'Y')
                Yes
                @elseif($pro->is_featured == 'N')
                No
                @endif
            </td>
            <td>
                @if($pro->status == 'A')
                @lang('admin_lang_static.active')
                @elseif($pro->status == 'I')
                @lang('admin_lang_static.inactive')
                @elseif($pro->status == 'W')
                @lang('admin_lang_static.awaiting_approval')
                @endif
            </td>
            <td>
                @if($pro->seller_status == 'A')
                @lang('admin_lang_static.active')
                @elseif($pro->seller_status == 'I')
                @lang('admin_lang_static.inactive')
                @elseif($pro->seller_status == 'W')
                @lang('admin_lang_static.awaiting_approval')
                @endif
            </td>
            <td>
                @if($pro->status == 'A')
                <a href="{{ route('status.product',@$pro->id) }}" onclick="return confirm('Do you want to block this product ?');"><i class="fa fa-ban" title="Block"></i></a>
                @elseif($pro->status == 'I')
                <a href="{{ route('status.product',@$pro->id) }}" onclick="return confirm('Do you want to unblock this product ? ');"><i class="fa fa-check-circle" title="Unblock"></i></a>
                @endif
                <a href="{{ route('edit.product',@$pro->slug) }}"><i class="fa fa-edit" title="Edit"></i></a>
                @if($pro->status == 'W')
                <a href="{{ route('status.product',@$pro->id) }}" onclick="return confirm('Do you want to approve this product ? ');"><i class="fa fa-thumbs-up" title="Approve"></i></a>  
                @endif
                @if(count(@$pro->productSubCategory->Category->priceVariant) > 0)
                <a href="{{ route('product.discount',$pro->slug) }}"><i class="fa fa-percent" title="Set Discount Price"></i></a>  
                @endif
                <a href="{{ route('remove.product', $pro->id) }}" onclick="return confirm('Do you want to delete this product ? ');"><i class=" fa fa-trash"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>@lang('admin_lang_static.image')</th>
            <th>@lang('admin_lang_static.category')</th>
            <th>@lang('admin_lang_static.sub_category')</th>
            <th>@lang('admin_lang_static.merchant')</th>
            <th>@lang('admin_lang_static.title')</th>
            <th>@lang('admin_lang_static.price')</th>
            <th>@lang('admin_lang_static.is_featured')</th>
            <th>@lang('admin_lang_static.status')</th>
            <th>@lang('admin_lang_static.merchant_status')</th>
            <th>@lang('admin_lang_static.action')</th>
        </tr>
    </tfoot>
</table>


<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>