@extends('admin.layouts.app')
@section('title', 'Alaaddin | Admin | Add Product Step Three')
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang_static.add_product')</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang_static.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang_static.add_product')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang_static.add_product')<span class="sub-header-size"> > {{ @$productName }} > @lang('admin_lang_static.step3_of_4_stock')</span></h5>
                            <div class="card-body">
                                <form action="{{ route('store.product.step.three') }}" method="POST" id="dependent_form">
                                    @csrf
                                    <div class="row" id="appendRow" style="margin: 0;">
                                        <input type="hidden" name="product_id" value="{{ $product_id }}">
                                        <input type="hidden" name="slug" id="slug" value="{{ $slug }}">
                                        {{-- @dd($fetch_data) --}}
                                        @foreach($fetch_data as $key=>$fd)
                                        <div class="adedfrm">
                                            @foreach($fd->productVariantDetails as $key1=>$pvd)
                                            <div class="form-group">
                                                @if($key == 0)
                                                <label for="input-select" class="col-form-label">{{ $pvd->variantByLanguage->name }}</label>
                                                @endif
                                                <input type="text" class="form-control required" name="" value="{{ $pvd->variantValueName->variantValueByLanguage->name }}" disabled="">
                                            </div>
                                            @endforeach
                                            <div class="form-group">
                                                @if($key == 0)
                                                <label for="stock_quantity" class="col-form-label">@lang('admin_lang_static.stock_quantity')</label>
                                                @endif
                                                <input id="stock_quantity" type="number" class="form-control required" name="stock_quantity[{{ $fd->id }}]" placeholder="Stock Quantity" value="{{ $fd->stock_quantity }}" min="0">
                                            </div>
                                            <div class="form-group">
                                                @if($key == 0)
                                                <label for="weight" class="col-form-label">@lang('admin_lang_static.weight') (Grams)</label>
                                                @endif
                                                <input id="weight" type="text" class="form-control required" name="weight[{{ $fd->id }}]" placeholder="@lang('admin_lang_static.weight')" value="{{ $fd->weight }}" min="0" onkeypress='validate(event)'>
                                            </div>
                                            {{-- 
                                            <div class="form-group useadmrgn">
                                                <a class="crss" href="{{ route('delete.product.variant',$fd->id) }}"><i class="fas fa-times"></i></a>
                                            </div>
                                            --}}                                          
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="col-form-label"></label>
                                        <a href="{{ route('add.product.step.two',$slug) }}" class="btn btn-primary nxtop">@lang('admin_lang_static.previous')</a>
                                        {{-- <a href="{{ route('add.product.step.three',$slug) }}" class="btn btn-primary nxtop">Next</a> --}}
                                        <button type="submit" class="btn btn-primary nxtop">@lang('admin_lang_static.next')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- footer -->   
@include('admin.includes.footer')
<!-- end footer -->
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script>
    $(document).ready(function(){ 
      $("#dependent_form" ).validate({
        rules: {
         "weight":{
           required:true,
           number:true
         }
       },
       errorPlacement: function (error , element) {
                            //toastr:error(error.text());
                          }
                        });
    });
</script>
<script>
    $(document).ready(function() {
      $('.slct').chosen();
    })
</script>
{{-- for fetching models of brand  --}}
<script>
    $(document).ready(function(){
    
     $('#input-select').change(function(){
      if($(this).val() != '')
      {
       var value  = $(this).val();
       var _token = $('input[name="_token"]').val();
       $.ajax({
        url:"{{ route('sub.cat.fetch') }}",
        method:"POST",
        data:{value:value, _token:_token},
        success:function(result)
        {
         $('#subCategory').html(result);
       }
       
     })
     }
    });
     
     $('#input-select').change(function(){
      $('#subCategory').val('');
    }); 
     
    });
    function validate(evt) {
      var theEvent = evt || window.event;
            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
            // Handle key press
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
        }
        var regex = /[0-9]|\./;
        if( !regex.test(key) ) {
          theEvent.returnValue = false;
          if(theEvent.preventDefault) theEvent.preventDefault();
      }
    }
</script>
@endsection