@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Add Customer') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.add_zone')
@endsection
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.add_zone')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                @lang('admin_lang.add_zone')
                            </h5>
                            <div class="card-body">
                                <form id="addZoneForm" method="post"  action="{{ route('admin.zone.insert') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="zone_name" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.zone_name')</label>
                                            <input id="zone_name" name="zone_name" type="text" class="form-control required" placeholder="@lang('admin_lang.zone_name')">
                                            <span class="error_zone_name" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" >
                                            <label for="country" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.country')</label>
                                            @if(@$country)
                                            <select name="country[]" id="country"class="form-control required" multiple data-placeholder="@lang('admin_lang.select_country')">
                                                @foreach($country as $c)
                                                    <option value="{{ @$c->id }}"> {{ @$c->countryDetailsBylanguage->name }}</option>
                                                @endforeach
                                            </select>
                                            @endif
                                            <span class="country_error" style="color: red;"></span>
                                        </div>

                                        <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">

                                            <a href="javascript:void(0)" id="saveZone" class="btn btn-primary ">@lang('admin_lang.save')</a>
                                        </div>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.validator.setDefaults({ ignore: ":hidden:not(select)" })
        $("#saveZone").click(function(){

            $("#addZoneForm").validate({
            rules:{
                'zone_name':{
                    required:true
                },
                "country[]":{
                    required:true
                },
                
            },
            messages: { 
                "zone_name": { 
                    required: "@lang('validation.required')"
                },
                "country[]": { 
                    required: "@lang('validation.required')"
                },
                
            },
                errorPlacement: function (error, element) 
                {
                        if (element.attr("name") == "zone_name") {
                            var error = '<label for="zone_name" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                            $('.error_zone_name').html(error);
                        }
                        if (element.attr("name") == "country[]") {
                            var error = '<label for="country[]" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                            $('.country_error').html(error);
                        }
                        
                }
            });
            $("#addZoneForm").submit();
        });
      
        $('#country').chosen();
    });
</script>

@endsection