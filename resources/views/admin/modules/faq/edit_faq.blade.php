@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Edit Manufacturer') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.edit_manufacturer')
@endsection
@section('content')
@section('links')
    {{-- @include('admin.includes.links') --}}
    @if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
    @include('admin.includes.header')
@endsection
@section('sidebar')
    @include('admin.includes.sidebar')
@endsection
@section('content')
<!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title">@lang('admin_lang.E-commerceDashboard') 
                                </h2>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                            <li class="breadcrumb-item" aria-current="page"><a href="{{ route('admin.list.faq') }}" class="breadcrumb-link">Faq management</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Edit Faq</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session()->has('success'))
                    <div class="alert alert-success vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                    </div>
                    @elseif ((session()->has('error')))
                    <div class="alert alert-danger vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong>{{ session('error')['code']['message'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    @endif
                    <!-- ============================================================== -->
                    <!-- end pageheader  -->
                    <!-- ============================================================== -->
                    <div class="ecommerce-widget">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <h5 class="card-header">Edit Faq
                                        <a class="adbtn btn btn-primary" href="{{ route('admin.list.faq') }}">@lang('admin_lang.back')</a>
                                    </h5>
                                    <div class="card-body">
                                        <form id="addManufacturerForm" method="post" enctype="multipart/form-data" action="{{ route('admin.edit.faqpost',$brand->id) }}">
                                            @csrf
                                            <div class="row">

                                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                <label for="input-select" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.Category')</label>
                                                <select class="form-control" id="input-select" name="type">
                                                    <option value="">
                                                    Select Type
                                                    </option>
                                                    <option value="S" @if(@$brand->type == 'S') selected="" @endif>Seller</option>
                                                    <option value="B" @if(@$brand->type == 'B') selected="" @endif>Buyer</option>
                                                </select>
                                                <span class="errorCategory" style="color: red;"></span>
                                            </div>  

                                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                <label for="inputText3" class="col-form-label">Display Order</label>
                                                <input id="inputText3" type="text" class="form-control" name="display_order" placeholder="Display order" value="{{ @$brand->display_order }}">
                                            </div> 
                                            @if(@$language)
                                                @foreach(@$language as $key=>$lang)
                                                <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <label for="inputText3" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.faq_ques') </label>
                                                    <input id="inputText{{ $language[$key]->id }}" type="text" class="form-control brandName" placeholder="@lang('admin_lang.faq_ques')" name="faq_ques[]" value="{{ @$brand->faqDetails[$key]->faq_ques }}">
                                                    <span class="errorBrandName" style="color: red;"></span>
                                                </div>
                                                @endforeach
                                            @endif

                                            @if(@$language)
                                                @foreach(@$language as $key=>$lang)
                                                <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <label for="inputText3" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.faq_ans')</label>
                                                    <textarea id="inputText{{ $language[$key]->id }}" type="text" class="form-control brandName" placeholder="@lang('admin_lang.faq_ans')" name="faq_answer[]" >{{ @$brand->faqDetails[$key]->faq_answer }}</textarea>
                                                    <span class="errorBrandName" style="color: red;"></span>
                                                </div>
                                                @endforeach
                                            @endif
                                            
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label for="inputPassword" class="col-form-label"></label>
                                                <a href="javascript:void(0)" id="saveBrandDetails" class="btn btn-primary">
                                                @lang('admin_lang.Save')
                                                </a>
                                            </div>
                       
                                        </div>
                                        </form>
                                    </div>
                                    <!-- <div class="card-body border-top">
                                        <h3>Sizing</h3>
                                        <form>
                                            <div class="form-group">
                                                <label for="inputSmall" class="col-form-label">Small</label>
                                                <input id="inputSmall" type="text" value=".form-control-sm" class="form-control form-control-sm">
                                            </div>
                                            <div class="form-group">
                                                <label for="inputDefault" class="col-form-label">Default</label>
                                                <input id="inputDefault" type="text" value="Default input" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="inputLarge" class="col-form-label">Large</label>
                                                <input id="inputLarge" type="text" value=".form-control-lg" class="form-control form-control-lg">
                                            </div>
                                        </form>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
            <!-- ============================================================== -->
            @include('admin.includes.footer')
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
@endsection
@section('scripts')
    {{-- @include('admin.includes.scripts') --}}
    @if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script type="text/javascript">
    $(document).ready(function(){

        $("#customFile").change(function() {
            var filename = $.trim($("#customFile").val()),
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".errorpic").text("");
                readURL(this);
            }else{

                $(".errorpic").text('@lang('validation.image_extension_type')');
                $('#profilePicture').attr('src', "");

            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                  $('#profilePicture').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#saveBrandDetails").click(function(e){
            var len = $(".brandName").length,
            error = 0,
            category = $.trim($("#input-select").val()),
            brandLogo = $.trim($("#customFile").val());
            if(brandLogo != ""){
                var filename_arr = brandLogo.split("."),
                ext = filename_arr[1];
                ext = ext.toLowerCase();
                if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                }else{
                    error++;
                    $(".errorpic").text('@lang('validation.image_extension_type')');
                    $('#profilePicture').attr('src', "");

                }
            }else{
                // error++;
                // $(".errorpic").text('@lang('validation.image_extension_type')');
                    // $('#profilePicture').attr('src', "");
            }
            // category = $.trim($("#input-select").val())
            for(var i=0;i<len;i++){
                var brandname = $.trim($(".brandName").eq(i).val());
                if(brandname == ""){
                    error++;
                    $(".errorBrandName").eq(i).text('This field is required');
                }
            }
            if(category == ""){
                error++;
                $(".errorCategory").text('This field is required');
            }
            
            if(error == 0){
                $("#addManufacturerForm").submit();
            }
        });
    });
</script>
@endsection
