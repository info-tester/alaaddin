@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Add Manufacturer') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | FAQ Category
@endsection
@section('content')
@section('links')
    {{-- @include('admin.includes.links') --}}
    @if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
    @include('admin.includes.header')
@endsection
@section('sidebar')
    @include('admin.includes.sidebar')
@endsection
@section('content')
<!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title"> @lang('admin_lang.add_faq_category') </h2>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.add_faq_category')</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session()->has('success'))
                    <div class="alert alert-success vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong>{{ session()->get('success')['message'] }}</strong> {{ session()->get('success')['meaning'] }} 
                    </div>
                    @elseif ((session()->has('error')))
                    <div class="alert alert-danger vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">&times;</a>

                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        {{ @session()->get('error')['meaning'] }} 
                    </div>
                    @endif
                    <!-- ============================================================== -->
                    <!-- end pageheader  -->
                    <!-- ============================================================== -->
                    <div class="ecommerce-widget">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <h5 class="card-header">@lang('admin_lang.add_faq_category')</h5>
                                    <div class="card-body">
                                        <form method="post" id="faq_cat" action="{{ route('admin.faq.post.category') }}">
                                            @csrf
                                            <div class="row">
                                                @foreach(@$languages as $key=>$lang)
                                                {{-- @dump($key) --}}
                                                <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <label for="inputText1" class="col-form-label"><span style="color: red;">*</span>Category Name</label>
                                                    <input id="inputText{{ $lang->id }}" type="text" class="form-control required" placeholder="Category Name" name="categories[{{ $lang->id }}]" value="{{ @$faq_category->categoryDetails[$key]->faq_category }}">
                                                    <span class="errorBrandName" style="color: red;"></span>
                                                </div>
                                                @endforeach

                                                <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <label for="inputPassword" class="col-form-label"></label>
                                                    <button id="saveBrandDetails" class="btn btn-primary">
                                                    @lang('admin_lang.Save')
                                                    </button>
                                                </div>
                           
                                            </div>
                                            <input type="hidden" name="master_id" value="{{ @$faq_category->id }}">
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!-- ============================================================== -->
                            <!-- basic table  -->
                            <!-- ============================================================== -->
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <h5 class="card-header">FAQ Categories</h5>
                                    <div class="card-body">


                                        <div class="table-responsive listBody">
                                            <table class="table table-striped table-bordered first" id="myTable">
                                                <thead>
                                                    <tr>
                                                        <th>@lang('admin_lang.slno')</th>
                                                        <th>Category</th>
                                                        {{-- <th>@lang('admin_lang.Status')</th> --}}
                                                        <th>@lang('admin_lang.Action')</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(@$faq_categories)
                                                        @foreach(@$faq_categories as $key=>$val)
                                                            <tr class="tr_{{ @$data->id }}">
                                                                <td>{{ $key+1 }}</td>
                                                                <td>{{ @$val->categoryDetailsLanguage->faq_category}}</td>
                                                                {{-- <td>
                                                                    @if(@$val->status == 'A')
                                                                    @lang('admin_lang.Active')
                                                                    @elseif(@$val->status == 'I')
                                                                    @lang('admin_lang.Inactive')
                                                                    @endif
                                                                </td> --}}
                                                                <td>
                                                                    <a href="{{ route('admin.faq.add.category',@$val->id) }}"><i class="fas fa-edit" title="Edit"></i></a> 
                                                                    <a href="{{ route('admin.faq.delete.category',[@$val->id])}}" onclick="return confirm('Are you really want to delete faq category?')"  title="Delete" class="rjct delbtn"><i class="fas fa-trash" aria-hidden="true"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>@lang('admin_lang.slno')</th>
                                                        <th>Category</th>
                                                        {{-- <th>@lang('admin_lang.Status')</th> --}}
                                                        <th>@lang('admin_lang.Action')</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- end basic table  -->
                            <!-- ============================================================== -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
            <!-- ============================================================== -->
            @include('admin.includes.footer')
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
@endsection
@section('scripts')
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script>
    
$('#faq_cat').validate({
    errorPlacement: function() {

    }
});
</script>
@endsection
