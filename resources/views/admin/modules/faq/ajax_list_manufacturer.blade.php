<table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>@lang('admin_lang.Category')</th>
                                                <th>@lang('admin_lang.Brand')</th>
                                                <th>@lang('admin_lang.Logo')</th>
                                                <th>@lang('admin_lang.Status')</th>
                                                <th>@lang('admin_lang.Action')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(@$brands)
                                                @foreach(@$brands as $brand)
                                               <!--  <tr class="tr_{{ @$brand->id }}">
                                                    <td>{{ $brand->categoryDetailsByBrand[0]->title }}</td>
                                                    <td>{{ $brand->brandDetailsByLanguage->title }}</td>
                                                    <td>
                                                        <div class="m-r-10">
                                                            @if(@$brand->logo)
                                                            @php
                                                            $image_path = 'storage/app/public/brand_logo/'.@$brand->logo; 
                                                            @endphp
                                                            @if(file_exists(@$image_path))
                                                            <img src="{{ URL::to('storage/app/public/brand_logo/'.@$brand->logo) }}" alt="user" class="rounded" width="45">
                                                            @endif
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td class="status_{{ @$brand->id }}">
                                                        @if(@$brand->status == 'A')
                                                        @lang('admin_lang.Active')
                                                        @else
                                                        @lang('admin_lang.Inactive')
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{-- edit --}}
                                                        <a href="{{ route('admin.edit.manufacturer',@$brand->id) }}"><i class="fas fa-edit"></i></a>
                                                        {{-- delete --}}
                                                        <a class="delete_manufacturer" href="javascript:void(0)" data-id="{{ @$brand->id }}"><i class=" fas fa-trash"></i></a>
                                                    </td>
                                                </tr> -->
                                                @endforeach
                                            @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>@lang('admin_lang.Category')</th>
                                                <th>@lang('admin_lang.Brand')</th>
                                                <th>@lang('admin_lang.Logo')</th>
                                                <th>@lang('admin_lang.Status')</th>
                                                <th>@lang('admin_lang.Action')</th>
                                            </tr>
                                        </tfoot>
                                    </table>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>