@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | Admin | Customer Withdraw
@endsection
@section('content')
@section('links')
@include('admin.includes.links')
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            Dashboard
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('admin.list.customer') }}" class="breadcrumb-link">Manage Customers</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('admin.view.customer.profile',$customer->id) }}" class="breadcrumb-link">{{ @$customer->fname }} {{ @$customer->lname }} details</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        Customer Withdraw
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.includes.s_e_messages')
            {{-- @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                 {{ session('success') }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                 {{ session('error') }} 
            </div>
            @endif --}}
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                Customer Withdraw
                                <a class="adbtn btn btn-primary" href="{{ route('admin.view.customer.profile',$customer->id) }}">@lang('admin_lang.back')</a>
                            </h5>
                            <div class="card-body">
                                <form id="customerwithdraw" method="post" action="{{ route('admin.customer.withdraw', @$customer->id) }}">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="bank_name" class="col-form-label"><span style="color: red;">*</span>Bank name</label>
                                            <input id="bank_name" name="bank_name" type="text" class="form-control required" placeholder='Bank name' value="{{ @$customer->bank_name }}" readonly="">
                                            
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="branch_name" class="col-form-label"><span style="color: red;">*</span>Branch name</label>
                                            <input id="branch_name" name="branch_name" type="text" class="form-control required" placeholder='Branch name' value="{{ @$customer->branch_name }}" readonly="">
                                            
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="account_number" class="col-form-label"><span style="color: red;">*</span>Account Number</label>
                                            <input id="account_number" name="account_number" type="text" class="form-control required" placeholder='Account Number' value="{{ @$customer->account_number }}" readonly="">
                                            
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="account_name" class="col-form-label"><span style="color: red;">*</span>Account Name</label>
                                            <input id="account_name" name="account_name" type="text" class="form-control required" placeholder='Account Name' value="{{ @$customer->account_name }}" readonly="">
                                            
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="ifsc_code" class="col-form-label"><span style="color: red;">*</span>IFSC Code</label>
                                            <input id="ifsc_code" name="ifsc_code" type="text" class="form-control required" placeholder='IFSC Code' value="{{ @$customer->ifsc_code }}" readonly="">
                                            
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="description" class="col-form-label"><span style="color: red;">*</span>Description</label>
                                            <input id="description" name="description" type="text" class="form-control required" placeholder='Description' value="{{ @$customer->description }}">
                                            
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="amount" class="col-form-label"><span style="color: red;">*</span>Amount({{ getCurrency() }})</label>
                                            <input id="amount" name="amount" type="number" class="form-control required" placeholder='Amount' value="{{ @$customer->wallet_amount }}" min="1" max="{{ @$customer->wallet_amount }}">
                                            
                                        </div>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                            <input type="submit" class="btn btn-primary" value="Save">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>
@endsection
@section('scripts')
@include('admin.includes.scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#customerwithdraw").validate();
    });
</script>
@endsection