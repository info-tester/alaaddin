@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | Customer Withdraw
@endsection
@section('links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/arabic/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@include('admin.includes.links')
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">@lang('admin_lang.withdrawl_request')</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.withdrawl_request')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="mail_success" style="display: none;">
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
            </div>
        </div>
        <div class="mail_error" style="display: none;">
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>@lang('admin_lang.error')!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
            </div>
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
        </div>
        @elseif ((session()->has('error')))
        <div class="alert alert-danger vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
        </div>
        @endif
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- ============================================================== -->
            <!-- basic table  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">@lang('admin_lang.withdrawl_request')
                        <a class="rqstw adbtn btn btn-primary" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter3">@lang('admin_lang.request_withdrawl')</a>
                    </h5>
                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div data-column="0" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                    <label for="col0_filter" class="col-form-label">@lang('admin_lang.Keywords') </label>
                                    <input id="col0_filter" name="col0_filter" type="text" class="form-control keyword rs s_frm" placeholder="Search by name/email/phone/request id">
                                </div>
                                <div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                        <label for="col1_filter" class="col-form-label">@lang('admin_lang.merchant')</label>
                                        <select data-placeholder="@lang('admin_lang.choose_a_merchant')" class="form-control select slt slct merchant rs s_frm"  tabindex="1" id="col1_filter" name="col1_filter">
                                            <option value="">@lang('admin_lang.select_merchant')</option>
                                            @if(@$merchants)
                                            @foreach(@$merchants as $merchant)
                                            <option value="{{ @$merchant->id }}" @if(@$key['merchant'] == $merchant->id) selected @endif>{{ @$merchant->fname." ".@$merchant->lname }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                {{-- <div data-column="0" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="inputText3" class="col-form-label">@lang('admin_lang.Keywords') </label>
                                    <input id="keyword" type="text" class="form-control" placeholder="Search by name/email/phone/request id">
                                </div> --}}

                                {{-- <div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                    <label for="merchant_lb" class="col-form-label">@lang('admin_lang.merchant')</label>
                                    <select data-placeholder="@lang('admin_lang.choose_a_merchant')" class="form-control select slt slct merchant"  tabindex="7" id="col7_filter" name="merchant">
                                        <option value="">@lang('admin_lang.select_merchant')</option>
                                        @if(@$merchants)
                                        @foreach(@$merchants as $merchant)
                                        <option value="{{ @$merchant->id }}" @if(@$key['merchant'] == $merchant->id) selected @endif>{{ @$merchant->fname." ".@$merchant->lname }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div> --}}

                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    
                                    {{-- <label for="inputPassword" class="col-form-label"></label> --}}
                                    <!-- <h5 class="card-header"> -->
                                    <a href="javascript:void(0)" id="search_request" class="btn btn-primary fstbtncls">Search</a>
                                    <input type="reset" value="Reset Search" class="btn btn-default fstbtncls reset_search">
                                    <!-- </h5> -->
                                <!-- </div> -->
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive listBody">
                            <table class="table table-striped table-bordered first" id="mytable1">
                                <thead>
                                    <tr>
                                        <th>@lang('admin_lang.sl_no')</th>
                                        <th>@lang('admin_lang.request_id')</th>
                                        <th>@lang('admin_lang.request_date')</th>
                                        <th>@lang('admin_lang.payment_date')</th>
                                        <th>@lang('admin_lang.seller')</th>
                                        <th>@lang('admin_lang.seller') @lang('admin_lang.email_id_addr')</th>
                                        <th>@lang('admin_lang.seller') @lang('admin_lang.phone_number')</th>
                                        <th>@lang('admin_lang.total_payout_kwd')</th>
                                        <th>@lang('admin_lang.requested_withdrawl') ({{ getCurrency() }})</th>
                                        <th>Current Merchant @lang('admin_lang.balence_amt')</th>
                                        <th>@lang('admin_lang.Status')</th>
                                        <th>@lang('admin_lang.notes')</th>
                                        <th>@lang('admin_lang.Action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @if(@$withdraw)
                                    @foreach(@$withdraw as $key=>$w)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ @$w->id }}</td>
                                        <td>{{ @$w->request_date }}</td>
                                        <td>{{ @$w->payment_date }}</td>
                                        <td>{{ @$w->merchantDetails->fname." ".@$w->merchantDetails->lname }}</td>
                                        <td>{{ @$w->merchantDetails->email }}</td>
                                        <td>{{ @$w->merchantDetails->phone }}</td>
                                        <td>{{ @$w->merchantDetails->total_paid }}</td>
                                        <td>{{ @$w->amount }}</td>
                                        <td>{{ @$w->balance }}</td>
                                        
                                        <td>
                                            @if($w->status == 'N')
                                            @lang('admin_lang.new')
                                            @elseif($w->status == 'S')
                                            @lang('admin_lang.success')
                                            @elseif($w->status == 'C')
                                            @lang('admin_lang.Cancelled_1')
                                            @endif
                                            
                                        </td>
                                        <td>
                                            {!! strip_tags(@$w->description) !!}
                                        </td>
                                        <td>
                                            
                                            @if($w->status == 'N')
                                            <a href="javascript:void(0)" data-toggle="modal" class="approve_request" data-action="{{ route('admin.approve.request',$w->id) }}" id="approve_request">

                                            <i class="fas fa-thumbs-up" title="@lang('admin_lang.approve')"></i></a>

                                            <a href="javascript:void(0)" data-toggle="modal" data-id="" data-action="{{ route('admin.reject.request',@$w->id) }}" id="reject_request_notes" class="reject_request_notes">

                                            <i class="fas fa-ban" title="@lang('admin_lang.reject_request')"></i>
                                            </a>
                                            @endif 
                                            
                                            <a href="{{ route('admin.view.merchant.profile',@$w->seller_id) }}"><i class="fa fa-info" title="@lang('admin_lang.seller_dtlss')"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif --}}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>@lang('admin_lang.sl_no')</th>
                                        <th>@lang('admin_lang.request_id')</th>
                                        <th>@lang('admin_lang.request_date')</th>
                                        <th>@lang('admin_lang.payment_date')</th>
                                        <th>@lang('admin_lang.seller')</th>
                                        <th>@lang('admin_lang.seller') @lang('admin_lang.email_id_addr')</th>
                                        <th>@lang('admin_lang.seller') @lang('admin_lang.phone_number')</th>
                                        <th>@lang('admin_lang.total_payout_kwd')</th>
                                        <th>@lang('admin_lang.requested_withdrawl') ({{ getCurrency() }})</th>
                                        <th>Current Merchant @lang('admin_lang.balence_amt')</th>
                                        <th>@lang('admin_lang.Status')</th>
                                        <th>@lang('admin_lang.notes')</th>
                                        <th>@lang('admin_lang.Action')</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end basic table  -->
            <!-- ============================================================== -->
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('admin.includes.footer')
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
    <div class="loader" style="display: none;">
        <img src="{{url('public/loader.gif')}}">
    </div>
</div>
</div>
<div class="modal fade" id="exampleModalCenter3">
<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">@lang('merchant_lang.request')</h5>
            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="basic-form">
                <form id="request_withdraw_form" method="post" action="{{ route('admin.add.request.withdrawl.by.admin') }}">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="merchant_lb" class="col-form-label">@lang('admin_lang.merchant')</label>
                            <select class="form-control select merchant slct"  id="merchant" name="merchant">
                                <option value="">@lang('admin_lang.select_merchant')</option>
                                @if(@$merchants)
                                    @foreach(@$merchants as $merchant)
                                    <option value="{{ @$merchant->id }}" data-due="{{ @$merchant->total_due }}" data-paid="{{ @$merchant->total_paid}}" data-earning="{{ @$merchant->total_earning }}" data-total_commission="{{ @$merchant->total_commission}}">{{ @$merchant->fname." ".@$merchant->lname }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <span class="error_merchant" style="color: red;"></span>
                        </div>
                        <div class="form-group col-md-12">
                            <p class="shw">@lang('merchant_lang.balence'):<span class="balence_due_amt"></span>  @lang('merchant_lang.kwd')</p>
                            <!-- <input type="hidden" id="balence" name="balence" value=""> -->
                            <label>@lang('merchant_lang.enter_amt_kwd_withdraw')</label>
                            <input id="amount" name="amount" type="text" class="form-control" placeholder="@lang('admin_lang.enter_amount')" onkeypress='validate(event)' min="1">
                            <span class="error_amount" style="color: red;"></span>
                        </div>
                        <div class="form-group col-md-12">
                            <label></label>
                            <button type="button" id="send_request" class="btn btn-primary popbtntp">@lang('admin_lang.send_request')</button>
                        </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="exampleModalCenter">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.notes')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="approve_request_form" method="POST" action="">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <!-- <label>Notes</label> -->
                                <textarea class="form-control h-80px" rows="2" id="approve_comment" name="approve_comment"></textarea>
                                <span class="error_approve_comment" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" class="btn btn-primary popbtntp" id="approve_notes">@lang('admin_lang.save')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalCenter2">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.notes')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="reject_request_form" method="POST" action="">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea class="form-control h-80px" rows="2" id="reject_comment" name="reject_comment"></textarea>
                                <span class="error_reject_comment"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" class="btn btn-primary popbtntp" id="reject_notes">@lang('admin_lang.save')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@include('admin.includes.scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" />

<script type="text/javascript">
    function validate(evt) {
        var theEvent = evt || window.event;
              // Handle paste
              if (theEvent.type === 'paste') {
                  key = event.clipboardData.getData('text/plain');
              } else {
              // Handle key press
              var key = theEvent.keyCode || theEvent.which;
              key = String.fromCharCode(key);
          }
          var regex = /[0-9]|\./;
          if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
    $(document).ready(function(){
        $('.slct').chosen();
        $('body').on('click', '.reset_search', function() {
            $('#finance').DataTable().search('').columns().search('').draw();
            $(".slct").val("").trigger("chosen:updated");
        })
        // $("#keyword").on("keyup", function(e) {
        //     var value = $(this).val().toLowerCase();
        //     $("#myTable tbody tr").filter(function(){
        //       $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        //   });
        // });
        $(document).on("change","#merchant",function(e){
            
            var merchant = $.trim($(this).val());
            if(merchant != ""){
            var due = $("#merchant option:selected").attr("data-due");
                $(".balence_due_amt").text(due);
            }else{
                $(".balence_due_amt").text("0.000");
            }
            
        });
        $("#send_request").click(function(e){
            var merchant = $("#merchant").val(),
            amount = $.trim($("#amount").val()),
            balence = parseFloat($.trim($("#merchant option:selected").attr("data-due"))).toFixed(2);
            var error = 0;
            if(amount != ""){
                amount = parseFloat(amount);
                amount = amount.toFixed(2);
                var calculation = balence - amount;
                if(calculation > 0.00){
                    
                    $.ajax({
                        type:"GET",
                        url:"{{ route('check.duplicate.request.withdraw') }}",
                        data:{
                            seller_id: merchant
                        },
                        success:function(resp){
                            if(resp == 0){
                                error++;
                                $(".error_amount").text("@lang('admin_lang.u_created_request_withdrawl')");
                            }else{
                                $("#request_withdraw_form").submit();
                            }
                        }
                    });
                }else{
                    if(amount == 0.00){
                        $(".error_amount").text("@lang('admin_lang.amt_zero_chk')");
                    }else{
                        $(".error_amount").text("@lang('admin_lang.amt_sh_less_eq_bale')");
                    }
                }
            }else{
                error++;
                $(".error_amount").text("@lang('validation.required')");
            }
        });
        $("#search_request").click(function(){
            var keyword = $.trim($("#keyword").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.search.request.withdrawl') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    merchant: $('#col7_filter').val(),
                },
                success:function(resp){
                    if(resp != 0){
                        $(".listBody").html(resp);
                    }
                }
            });
        });
        $("#keyword").blur(function(){
            var keyword = $.trim($("#keyword").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.search.request.withdrawl') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    keyword:keyword,
                    merchant: $('#col7_filter').val(),
                },
                success:function(resp){
                    if(resp != 0){
                        $(".listBody").html(resp);
                    }
                }
            });
        });
        $("#col7_filter").change(function(){
            var keyword = $.trim($("#keyword").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.search.request.withdrawl') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    keyword: keyword,
                    merchant: $('#col7_filter').val(),
                },
                success:function(resp){
                    if(resp != 0){
                        $(".listBody").html(resp);
                    }
                }
            });
        });
        $("#approve_comment").blur(function(e){
            
            var cost = $.trim($("#approve_comment").val());
            if(cost != ""){
            }else{
                $(".error_approve_comment").text("@lang('admin_lang.please_provide_comment')");
            }
        });
        $("#approve_notes").click(function(){
            var cost = $.trim($("#approve_comment").val());
            //$('#exampleModalCenter').modal('hide')
            $(this).addClass('disabled');
            if(cost != ""){
                
                $("#approve_request_form").submit();
            }
        });
        $(document).on("click",".approve_request",function(e){
            var action= $.trim($(e.currentTarget).attr("data-action"));
            // $('#exampleModalCenter').attr("data-id",id);
            $('#exampleModalCenter').attr("data-action",action);
            $("#approve_request_form").attr("action",action);
            $('#exampleModalCenter').modal('show');
        });
        $("#reject_comment").blur(function(e){
            var cost = $.trim($("#reject_comment").val());
            if(cost != ""){
            }else{
                $(".error_reject_comment").text("@lang('admin_lang.please_provide_comment')");
            }
        });
        $("#reject_notes").click(function(){
            var cost = $.trim($("#reject_comment").val());
            if(cost != ""){
                $("#reject_request_form").submit();
            }
        });
        $(document).on("click",".reject_request_notes",function(e){
        // $(".reject_request_notes").click(function(e){
            // alert("hiii");
            var action= $.trim($(e.currentTarget).attr("data-action"));
            $('#exampleModalCenter').attr("data-action",action);
            $("#reject_request_form").attr("action",action);
            $('#exampleModalCenter2').modal('show');
        });
        $('.llk').click(function function_name(argument) {
            $('#exampleModalCenter1').modal('show');
        });
        $('#mytable1').DataTable( {
            stateSave: false,
            order: [
                [1, "desc"]
            ],
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "createdRow": function( row, data, dataIndex ) { 
                dataIndex = dataIndex+1;
                var id = (JSON.stringify(data.id)),
                r = JSON.stringify(dataIndex);
                $(row).addClass("tr_class_"+id);
                $(row).addClass("tr_row_class");
                $(row).children(':nth-child(1)').text(dataIndex);
            },
            "ajax": {
                "url": "{{ route('admin.viewlistRequestWithdrawl') }}",
                "data": function ( d ) {
                    console.log(d)
                    d.myKey = "myValue";
                    d._token = "{{ @csrf_token() }}";
                }
            },
            columnDefs: [ { targets: 0, type: 'natural' } ],
            'columns': [
         
            { 
                data: "id",
                render: function(data, type, full, meta){
                    return data;
                }
               
            },
            { 
                data: "id",
                render: function(data, type, full, meta){      
                    // if(full.no_of_use_sold) {
                    //     return full.no_of_use_sold;
                    // }else{
                    //     return 0;
                    // }
                return data;
                }
                
            },
            { 
                data: "request_date",
                render: function(data, type, full, meta){      
                    return data;
                }
                
            },
            { 
                data: "payment_date",
                render: function(data, type, full, meta){      
                    // if(full.no_of_use_sold) {
                    //     return full.no_of_use_sold;
                    // }else{
                    //     return 0;
                    // }
                return data;
                }
                
            },
            {
                data: 'id',
                render: function(data, type, full, meta){
                    if(full.merchant_details){
                        var n = full.merchant_details.fname;
                        n += " "+full.merchant_details.lname;
                        return n;
                    }else{
                        return "";
                    }
                    // return data;
                    // if(full.discount_type == 'F'){
                    //     return "<span class='is_trending_status_"+full.id+"'>@lang('admin_lang.flat')</span>";
                    // }else if(full.discount_type == 'P'){
                    //     return "<span class='is_trending_status_"+full.id+"'>@lang('admin_lang.percentage')</span>";
                    // }else{
                    //     return '';
                    // }
                }
            },
            {
                data: 'id',
                render: function(data, type, full, meta){
                    if(full.merchant_details){
                        var eml = full.merchant_details.email;
                        
                        return eml;
                    }else{
                        return "";
                    }
                    // return data;
                    // if(full.discount_type == 'F'){
                    //     return data;
                    // }else if(full.discount_type == 'P'){
                    //     return (data+"%");
                    // }else{
                    //     return '';
                    // }
                    
                }
            },
            { 
                data: "id",
                render: function(data, type, full, meta){      
                    if(full.merchant_details){
                        var phne = full.merchant_details.phone;
                        
                        return phne;
                    }else{
                        return "";
                    }
                }
                
            },
            { 
                data: "id",
                render: function(data, type, full, meta){      
                    if(full.merchant_details){
                        var total_paid = full.merchant_details.total_paid;
                        
                        return total_paid;
                    }else{
                        return "";
                    }
                }
                
            },
            
          { 
                data: "amount",
                render: function(data, type, full, meta){      
                    // if(full.no_of_use_sold) {
                    //     return full.no_of_use_sold;
                    // }else{
                    //     return 0;
                    // }
                return data;
                }
                
            },
            { 
                data: "balance",
                render: function(data, type, full, meta){   
                if(full.merchant_details){
                        var total_due = full.merchant_details.total_due;
                        
                        return total_due;
                    }else{
                        return "";
                    }   
                    // if(full.no_of_use_sold) {
                    //     return full.no_of_use_sold;
                    // }else{
                    //     return 0;
                    // }
                return data;
                }
                
            },
          
            { 
                data: "status",
                render: function(data, type, full, meta){      
                    if(full.status == 'N') {
                        return "New";
                    }
                    else if(full.status == 'S'){
                        return "Success";
                    }
                    else if(full.status == 'C'){
                        return "Cancelled";   
                    }
                    return data;
                }
                
            },
            { 
                data: "description",
                render: function(data, type, full, meta){      
                    // if(full.no_of_use_sold) {
                    //     return full.no_of_use_sold;
                    // }else{
                    //     return 0;
                    // }
                return data;
                }
                
            },
            { 
                data: "id",
                render: function(data, type, full, meta){
                    var a='';
                    if(full.status == 'N')
                    {
                        a += '<a href="javascript:void(0)" class="approve_request" data-action="{{ url('admin/approve-request').'/' }}'+full.id+'" title="Approve"><i class="fas fa-thumbs-up"></i></a> '
                        a += '<a href="javascript:void(0)" class="reject_request_notes" data-action="{{ url('admin/reject-request').'/' }}'+full.id+'" title="Reject"><i class="fas fa-ban"></i></a>'
                        
                    }
                    a += '<a href="{{ url('admin/view-merchant-profile').'/' }}'+full.id+'" title="Seller Details"><i class="fa fa-info"></i></a>'
                    return a;
                }
                
            },
            ]
        });
        $('body').on('click', '.reset_search', function() {
            $('#mytable1').DataTable().search('').columns().search('').draw();
        })

        function filterColumn ( i ) {
            console.log(i, $('#col'+i+'_filter').val())
            $('#mytable1').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }
        $('.s_frm').on( 'keyup click blur change', function () {
            filterColumn( $(this).parents('div').data('column')  );
        });
        $("#mytable1_filter").hide();
        // $(".approve_request").click(function(e){
        //     alert("approve request");
        //     var action= $.trim($(e.currentTarget).attr("data-action"));
        //     // $('#exampleModalCenter').attr("data-id",id);
        //     $('#exampleModalCenter').attr("data-action",action);
        //     $("#approve_request_form").attr("action",action);
        //     $('#exampleModalCenter').modal('show');
        // });
        // $("#reject_comment").blur(function(e){
        //     var cost = $.trim($("#reject_comment").val());
        //     if(cost != ""){
        //     }else{
        //         $(".error_reject_comment").text("@lang('admin_lang.please_provide_comment')");
        //     }
        // });
    });
function validate(evt) {
    var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}

</script>
@endsection