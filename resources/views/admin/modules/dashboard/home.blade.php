@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | Dashboard
@endsection
@section('content')
@section('links')
@include('admin.includes.links')
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
<div class="dashboard-wrapper">
    {{-- @dd($sum_value) --}}
    @if(@$accessDenied == '1')
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content mt-5">
            <h1 class="text-muted text-center" style="margin-top: 140px;">@lang('admin_lang.welcome_dashboard')</h1>
        </div>
        @else
        <div class="dashboard-ecommerce">
            <div class="container-fluid dashboard-content ">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Dashboard </h2>
                            <p class="pageheader-text">Alaaddin</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                                        <!-- <li class="breadcrumb-item active" aria-current="page"></li> -->
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="ecommerce-widget">
                    <div class="row">
                        <!-- ============================================================== -->
                        <!-- sales  -->
                        <!-- ============================================================== -->
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="card border-3 border-top border-top-primary">
                                <div class="card-body">
                                    <h5 class="text-muted">Total Order </h5>
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1">{{getCurrency()}} {{$sales}} </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end sales  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- new customer  -->
                        <!-- ============================================================== -->
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="card border-3 border-top border-top-primary">
                                <div class="card-body">
                                    <h5 class="text-muted">@lang('admin_lang.total_customer')</h5>
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1">{{$totalCustomers}} </h1>
                                    </div>
                                <!-- <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                                    <span class="icon-circle-small icon-box-xs text-success bg-success-light"><i class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1">10%</span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end new customer  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- visitor  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.total_sellers')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$totalMerchants}}</h1>
                                </div>
                                <!-- <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                                    <span class="icon-circle-small icon-box-xs text-success bg-success-light"><i class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1">5%</span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end visitor  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- total orders  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.total_orders')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$totalOrders}}</h1>
                                </div>
                                <!-- <div class="metric-label d-inline-block float-right text-danger font-weight-bold">
                                    <span class="icon-circle-small icon-box-xs text-danger bg-danger-light bg-danger-light "><i class="fa fa-fw fa-arrow-down"></i></span><span class="ml-1">4%</span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                            <div class="card border-3 border-top border-top-primary">
                                <div class="card-body">
                                    <h5 class="text-muted">Admin Earnings</h5>
                                    <div class="metric-value d-inline-block">
                                        <h1 class="mb-1">{{getCurrency()}}{{@$admin_will_get}} </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.total_commission_received')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$com}} {{getCurrency()}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.total_delivery_fee_admin')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$deliveryfee}} {{getCurrency()}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.total_external_orders_delivered')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$exto}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.total_internal_orders_delivered')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$intero}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                    {{-- <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted" title="Android Total Order"><i class="fa fa-android" aria-hidden="true"></i> @lang('admin_lang.total_internal_orders_delivered')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$android_order}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5  title="IOS Total Order" class="text-muted"><i class="fa fa-apple" ></i> @lang('admin_lang.total_internal_orders_delivered')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$ios_order}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 title="Website Total Order" class="text-muted"><i class="fa fa-globe" aria-hidden="true"></i> @lang('admin_lang.total_internal_orders_delivered')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$web_order}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <!-- ============================================================== -->
                    <!-- end total orders  -->
                    <!-- ============================================================== -->

                    {{-- This month information --}}

                    {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.this_month_sales') </h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$sales_month}} {{getCurrency()}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <!-- ============================================================== -->
                    <!-- end sales  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- new customer  -->
                    <!-- ============================================================== -->
                    {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.this_month_customer')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$totalCustomersMonth}} </h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <!-- ============================================================== -->
                    <!-- end new customer  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- visitor  -->
                    <!-- ============================================================== -->
                    {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.this_month_seller')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$totalMerchantsMonth}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <!-- ============================================================== -->
                    <!-- end visitor  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- total orders  -->
                    <!-- ============================================================== -->
                    {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.this_month_order')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$totalOrdersMonth}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.this_month_commission')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$commissionThisMonth}} {{getCurrency()}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.this_month_delivery_charge')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$deliveryfeeMonth}} {{getCurrency()}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.this_month_total_external_order')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$externalThisMonth}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted">@lang('admin_lang.this_month_total_internal_order')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$internalThisMonth}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                    {{-- <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 class="text-muted" title="Android Total Order"><i class="fa fa-android" aria-hidden="true"></i> @lang('admin_lang.this_month_total_internal_order')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$android_order_this_month}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5  title="IOS Total Order" class="text-muted"><i class="fa fa-apple" ></i> @lang('admin_lang.this_month_total_internal_order')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$ios_order_this_month}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="card border-3 border-top border-top-primary">
                            <div class="card-body">
                                <h5 title="Website Total Order" class="text-muted"><i class="fa fa-globe" aria-hidden="true"></i> @lang('admin_lang.this_month_total_internal_order')</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1">{{$web_order_this_month}}</h1>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <!-- ============================================================== -->
                    <!-- end total orders  -->
                    <!-- ============================================================== -->
                </div>
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- product sales  -->
                    <!-- ============================================================== -->
                    {{-- <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card" style="height: 94%">
                            <div class="card-header">
                                <h5 class="mb-0"> @lang('admin_lang.product_sales')</h5>
                            </div>
                            <div class="card-body">
                                <div class="ct-chart-product ct-golden-section">

                                </div>
                                <div class="text-center m-t-40">
                                    <span class="legend-item mr-3">
                                        <span class="fa-xs text-secondary mr-1 legend-tile"><i class="fa fa-fw fa-square" style="color: #5969ff;"></i></span>
                                        <span class="legend-text" style="color: #5969ff;">@lang('admin_lang.sales_for_each_month')</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <!-- ============================================================== -->
                    <!-- end product sales  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- product category  -->
                    <!-- ============================================================== -->

                    {{-- <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header"> @lang('admin_lang.product_category')</h5>

                            <div class="card-body">
                                @if(@$categoryNames[0] != "")
                                <div class="ct-chart-category ct-golden-section" style="height: 315px;"></div>
                                @else
                                <div class="" style="height: 315px;"></div>
                                @endif
                                <div class="text-center m-t-40">
                                    @if(@$categoryNames[0] != "")
                                    @foreach(@$categoryNames as $key=>$nms)
                                    @if(@$nms != "")
                                    <span class="legend-item mr-3">
                                        <span class="fa-xs text-secondary mr-1 legend-tile"><i class="fa fa-fw fa-square" @if($key == 0) style="color: #5969ff;" @endif @if($key == 1) style="color: #ff407b;" @endif @if($key == 2) style="color: #ffe174;;" @endif @if($key == 3) style="color: #64ced3;" @endif @if($key == 4) style="color: #ffa47f;" @endif></i></span>
                                        <span class="legend-text" @if($key == 0) style="color: #5969ff;" @endif @if($key == 1) style="color: #ff407b;" @endif @if($key == 2) style="color: #ffe174;;" @endif @if($key == 3) style="color: #64ced3;" @endif @if($key == 4) style="color: #ffa47f;" @endif>{{@$nms}}</span>
                                    </span>

                                    @endif
                                    @endforeach

                                    @else
                                    <span class="legend-item mr-3">
                                        <span class="fa-xs text-secondary mr-1 legend-tile"><i class="fa fa-fw fa-square"></i></span>
                                        <span class="legend-text">@lang('admin_lang.no_sales_for_product_category')</span>
                                    </span>
                                    @endif

                                    <span class="legend-item mr-3">
                                        <span class="fa-xs text-secondary mr-1 legend-tile"><i class="fa fa-fw fa-square"></i></span>
                                        <span class="legend-text">@lang('admin_lang.top_5_sales_pro_category')</span>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div> --}}

                    <!-- ============================================================== -->
                    <!-- end product category  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- customer acquistion  -->
                    <!-- @lang('admin_lang.customer_acquisition') -->
                    <!-- ============================================================== -->
                    {{-- @if(@$customerGraph)
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang.customer_reg_last_30_days')</h5>
                            <div class="card-body">
                                <div class="ct-chart ct-golden-section" style="height: 354px;"></div>
                                <div class="text-center">
                                    <span class="legend-item mr-2">
                                        <span class="fa-xs text-primary mr-1 legend-tile"><i class="fa fa-fw fa-square"></i></span>
                                        <span class="legend-text">@lang('admin_lang.customer_reg_last_30_days')</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif --}}
                    <!-- ============================================================== -->
                    <!-- end customer acquistion  -->
                    <!-- ============================================================== -->
                </div>
                <div class="row">
                    @if(@Auth::guard('admin')->user()->type == "A" || @$accessDenied6 == '6')
                    <!-- ============================================================== -->
                    <!-- recent orders  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang.recent_orders')</h5>
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="bg-light">
                                            <tr class="border-0">
                                                <th class="border-0">Order No</th>
                                                <th class="border-0">Date</th>
                                                <th class="border-0">Customer</th>
                                                <th class="border-0">Order Total ({{ getCurrency() }})</th>
                                                <th class="border-0">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($orders) > 0)
                                            @foreach($orders as $orders)
                                            <tr>
                                                <td><a href="{{route('admin.view.order', $orders->id)}}" style="font-weight: bold;"> {{$orders->order_no}}</a></td>
                                                <td>{{$orders->created_at}}</td>
                                                <td>{{$orders->customerDetails->fname}} {{$orders->customerDetails->lname}}</td>
                                                <td>{{$orders->order_total}}</td>
                                                <td>
                                                    @if(@$orders->status == 'I')
                                                    Incomplete
                                                    @elseif(@$orders->status == 'N')
                                                    New
                                                    @elseif(@$orders->status == 'INP')
                                                    In Progress
                                                    @elseif(@$orders->status == 'CM')
                                                    Completed
                                                    @elseif(@$orders->status == 'OA')
                                                    @lang('admin_lang.order_accepted')
                                                    @elseif(@$orders->status == 'DA')
                                                    @lang('admin_lang.driver_assigned')
                                                    @elseif(@$orders->status == 'RP')
                                                    @lang('admin_lang.ready_for_pickup')
                                                    @elseif(@$orders->status == 'OP')
                                                    Picked up
                                                    @elseif(@$orders->status == 'OD')
                                                    @lang('admin_lang.delivered_1')
                                                    @elseif(@$orders->status == 'PP')
                                                    @lang('admin_lang.partial_pickup')
                                                    @elseif(@$orders->status == 'PC')
                                                    @lang('admin_lang.partial_pickup_completed')
                                                    @elseif(@$orders->status == 'OC')
                                                    @lang('admin_lang.Cancelled_1')
                                                    @elseif(@$orders->status == 'F')
                                                    Payment Failed
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="5"><a href="{{route('admin.list.order')}}" class="btn btn-outline-light float-right">View All</a></td>
                                            </tr>
                                            @else
                                            <tr>
                                                <td colspan="5"><a href="javascript:;" class="text-center">No records found</a></td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(@Auth::guard('admin')->user()->type == "A" || @$accessDenied3 == '3')
                    <!-- ============================================================== -->
                    <!-- end recent orders  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                        <!-- ============================================================== -->
                        <!-- top perfomimg  -->
                        <!-- ============================================================== -->
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang.new_customers')</h5>
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table no-wrap p-table">
                                        <thead class="bg-light">
                                            <tr class="border-0">
                                                <th class="border-0">@lang('admin_lang.name')</th>
                                                <th class="border-0">@lang('admin_lang.email')</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($customer) > 0)
                                            @foreach($customer as $customer)
                                            <tr>
                                                <td><a href="{{ route('admin.view.customer.profile',@$customer->id) }}" style="font-weight: bold;"> {{$customer->fname}} {{$customer->lname}}</a></td>
                                                <td>{{$customer->email}}
                                                

                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="9"><a href="{{route('admin.list.customer')}}" class="btn btn-outline-light float-right">@lang('admin_lang.view_all')</a></td>
                                            </tr>
                                            @else
                                            <tr>
                                                <td colspan="5"><a href="javascript:;" class="text-center">@lang('admin_lang.no_records_found')</a></td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <!-- ============================================================== -->
                    <!-- end top perfomimg  -->
                    <!-- ============================================================== -->
                </div>
                @endif
            </div>      
        </div>
    </div>
    @endif
</div>
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
    </div>
</div>
    @endsection
    @section('scripts')
    @include('admin.includes.scripts')
    <script type="text/javascript">
        $(function() {
            "use strict";
        // product sales ordertotal for each month
        new Chartist.Bar('.ct-chart-product',
        {
            labels: {!! json_encode($product_sales['months']) !!},
            series: 
            [
            {!! json_encode($product_sales['amount']) !!}
            ]
        },
        {
            seriesBarDistance: 10,
            low: 0
        },
        {
            stackBars: true,
            axisY: {
                labelInterpolationFnc: function(value) {
                    return value;
                }
            },
            options: {
                title: {
                    display: true,
                    text: 'Custom Chart Title'
                }
            }
        }
        ).on('draw', function(data) {
            if(data.type === 'bar') {
                data.element.attr({
                    style: 'stroke-width: 40px'
                });
            }
        });

    //categories with most sales top 5 category
    var chart = new Chartist.Pie('.ct-chart-category', {
        series: {!! json_encode($sum_value, JSON_PRETTY_PRINT) !!},
        labels: {!! json_encode($categoryNames, JSON_PRETTY_PRINT) !!}
    }, 
    {
        donut: true,
        showLabel: true,
        donutWidth: 40,
        donutSolid: true,
        startAngle: 270,

    });
    
    chart.on('draw', function(data) {
        if (data.type === 'slice') {
            // Get the total path length in order to use for dash array animation
            var pathLength = data.element._node.getTotalLength();

            // Set a dasharray that matches the path length as prerequisite to animate dashoffset
            data.element.attr({
                'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
            });

            // Create animation definition while also assigning an ID to the animation for later sync usage
            var animationDefinition = {
                'stroke-dashoffset': {
                    id: 'anim' + data.index,
                    dur: 1000,
                    from: -pathLength + 'px',
                    to: '0px',
                    easing: Chartist.Svg.Easing.easeOutQuint,
                    // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                    fill: 'freeze'
                }
            };

            // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
            if (data.index !== 0) {
                animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
            }

            // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
            data.element.attr({
                'stroke-dashoffset': -pathLength + 'px'
            });

            // We can't use guided mode as the animations need to rely on setting begin manually
            // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
            data.element.animate(animationDefinition, false);
        }
    });

    // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
    chart.on('created', function() {
        if (window.__anim21278907124) {
            clearTimeout(window.__anim21278907124);
            window.__anim21278907124 = null;
        }
        window.__anim21278907124 = setTimeout(chart.update.bind(chart), 10000);
    });
    
    //customer chart
    var highestCustomer = parseInt('{{@$totalCustHighest}}')+80;

    new Chartist.Bar('.ct-chart',
    {
        labels: {!! json_encode($day) !!},
        series: 
        [
        {!! json_encode($noCustomer) !!}
        ]
    },
    {
      seriesBarDistance: 10,
      low: 0,
      high: highestCustomer
  },
  {
    stackBars: true,
    axisY: {
        labelInterpolationFnc: function(value) {
            return value;
        }
    },
    options: {
        title: {
            display: true,
            text: 'Custom Chart Title'
        }
    }
}).on('draw', function(data) {
        // alert(JSON.stringify(data));
        if(data.type === 'bar') {
            data.element.attr({
                style: 'stroke-width: 40px'
            });
        }
    });
});
</script>
@endsection
