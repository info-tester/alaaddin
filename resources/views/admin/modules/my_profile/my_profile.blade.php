@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Edit Customer') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.edit_customer')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<style type="text/css">
    label.error{
        background:none !important;
        color: red;
        border:none !important;
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.edit_my_profile')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang.edit_my_profile')</h5>
                            <div class="card-body">
                                <form id="editCustomerForm" method="post" enctype="multipart/form-data" action="{{ route('admin.view.profile',@Auth::guard('admin')->user()->id) }}">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <input type="hidden" id="old_email" value="{{ @$customer->email }}"/>
                                            <label for="fname" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.first_name')</label>
                                            <input id="fname" name="fname" type="text" class="form-control required" placeholder='@lang('admin_lang.first_name')' value="{{ @Auth::guard('admin')->user()->fname }}">
                                            <span class="error_fname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="lname" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.last_name')</label>
                                            <input id="lname" name="lname" type="text" class="form-control required" placeholder='@lang('admin_lang.last_name')' value="{{ @Auth::guard('admin')->user()->lname }}">
                                            <span class="error_lname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="email_lb" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.email')</label>
                                            <input id="email" name="email" type="email" placeholder='@lang('admin_lang.email')' class="form-control " value="{{ @Auth::guard('admin')->user()->email }}">
                                            <span class="error_email" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="phone_lb" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.phone_number')</label>
                                            <input id="phone" name="phone" type="text" class="form-control " placeholder='@lang('admin_lang.phone_number')' value="{{ @Auth::guard('admin')->user()->phone }}" onkeypress='validate(event)' maxlength="10">
                                            <span class="error_phone" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 fstbtncls">
                                            <input type="file" class="custom-file-input" id="profile_pic" name="profile_pic">
                                            <label class="custom-file-label extrlft" for="profile_pic">@lang('admin_lang.upload_picture')</label>
                                            <span> @lang('admin_lang.recommended_sze_100')</span>                                               
                                            <span class="errorPic" style="color: red;"></span>
                                            
                                            @if(Auth::guard('admin')->user()->profile_pic)
                                            @php
                                            $image_path = 'storage/app/public/sub_admin_pics/'.Auth::guard('admin')->user()->profile_pic; 
                                            @endphp
                                            @if(file_exists(@$image_path))
                                            <div class="profiles" style="display: block;">
                                                <img id="profilePictures" src="{{ URL::to('storage/app/public/sub_admin_pics/'.@Auth::guard('admin')->user()->profile_pic) }}" alt="" style="width: 100px;height: 100px;">
                                            </div>
                                            @endif
                                            @else
                                            <div class="profile" style="display: none;">
                                            <img src="" id="profilePicture" style="height: 100px;width: 100px;">
                                            </div>
                                            @endif
                                            
                                        </div>
                                        {{-- @if(@Auth::guard('admin')->user()->type == 'A')
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="email1_lb" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.email_ext_ord_notify_1')</label>
                                            <input id="email_1" name="email_1" type="text" class="form-control required" placeholder="@lang('admin_lang.email_ext_ord_notify_1')" value="{{ @Auth::guard('admin')->user()->email_1 }}">
                                            <span class="error_email_1" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="email_2_lb" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.email_ext_ord_notify_2')</label>
                                            <input id="email_2" name="email_2" type="text" class="form-control required" placeholder="@lang('admin_lang.email_ext_ord_notify_2')" value="{{ @Auth::guard('admin')->user()->email_2 }}">
                                            <span class="error_email_2" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="email_3_lb" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.email_ext_ord_notify_3')</label>
                                            <input id="email_3" name="email_3" type="text" class="form-control required" placeholder="@lang('admin_lang.email_ext_ord_notify_3')" value="{{ @Auth::guard('admin')->user()->email_3 }}">
                                            <span class="error_email_3" style="color: red;"></span>
                                        </div>
                                        @endif --}}
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="password_n" class="col-form-label">
                                                @lang('admin_lang.nw_password')
                                            </label>
                                            <input id="nw_password" name="nw_password" type="password" class="form-control " placeholder="@lang('admin_lang.nw_min_pass_len')" value="" >
                                            <span class="error_nw_password">@lang('admin_lang.pass_min_len_8')</span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="password_c" class="col-form-label">
                                                @lang('admin_lang.confirm_password')
                                            </label>
                                            <input id="confirm_password" name="confirm_password" type="password" class="form-control " placeholder="@lang('admin_lang.confirm_password')" value="" >
                                            <span class="error_confirm_password" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                            
                                            <a href="javascript:void(0)" id="addCustomer" class="btn btn-primary ">@lang('admin_lang.save')</a>
                                        </div>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
    @endsection
    @section('scripts')
    {{-- @include('admin.includes.scripts') --}}

    @if(Config::get('app.locale') == 'en')
    @include('admin.includes.scripts')
    @elseif(Config::get('app.locale') == 'ar')
    @include('admin.includes.arabic_scripts')
    @endif
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script type="text/javascript">
        
        $(document).ready(function(){
            $("#addCustomer").click(function(){
                $("#editCustomerForm").submit();
            });
        // $(document).on("blur",'#password',function(e){
        //     $("#password").val("");
        // });
        
        $("#profile_pic").change(function() {
            var filename = $.trim($("#profile_pic").val());
            if(filename != ""){
                filename_arr = filename.split("."),
                ext = filename_arr[1];
                ext = ext.toLowerCase();
                if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                    $(".profile").show();
                    $(".errorpic").text("");
                    readURL(this);
                }else{
                    $(".errorpic").html('@lang('validation.img_upload')');
                    $('#profilePictures').attr('src', "");
                }
            } 
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                $(".profiles").show();
                $('#profilePictures').attr('src', e.target.result);
              }
              reader.readAsDataURL(input.files[0]);
            }
        }
        
        /*$("#profile_pic").change(function() {
            var filename = $.trim($("#profile_pic").val()),
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".errorpic").text("");
                readURL(this);
            }else{

                $(".errorpic").text('@lang('validation.image_extension_type')');
                $('#profilePicture').attr('src', "");

            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                  $('#profilePicture').attr('src', e.target.result);
                  $(".profile").show();
              }
              reader.readAsDataURL(input.files[0]);
          }
      }*/
    $(document).on("blur","#email",function(){
        // alert("dfksjdkf");
        var email = $.trim($("#email").val()),
        old_email = "{{ Auth::guard('admin')->user()->email }}";

        if(email != "")
        {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(regex.test(email)){
                var rurl = '{{ route('admin.check.duplicate.email.update') }}';
                $.ajax({
                    type:"GET",
                    url:rurl,
                    data:{
                        email:email,
                        old_email:old_email
                    },
                    success:function(resp){
                            // alert(resp);
                            if(resp == 0){
                                $(".error_email").text('@lang('validation.email_id_already_exist')');
                                $("#email").val("");
                            }else{
                                $(".error_email").text("");
                            }
                        }
                    }); 
            }else{
                    // alert("if email format false");
                    $(".errorEmail").text('@lang('validation.please_provide_valid_email_id')');
                }
                
            }else{
                // alert("if blank");
                $(".errorEmail").text('@lang('validation.please_provide_valid_email_id')');
                
            }
        });

      $(document).on("blur","#phone",function(){
        // alert("jhdhgshdg");
        var phone = $.trim($("#phone").val()),
        old_phone = "{{ @Auth::guard('admin')->user()->phone }}";
        
        if(phone != "")
        {
            var rurl = '{{ route('admin.check.duplicate.phone.update') }}';
            $.ajax({
                type:"GET",
                url:rurl,
                data:{
                    phone:phone,
                    old_phone:$.trim(old_phone)
                },
                success:function(resp){
                    if(resp == 0){
                        
                        $(".error_phone").text('@lang('admin_lang.phone_number_already_exist')');
                        $("#phone").val("");
                    }
                }
            });  
        }
    });
    $("#editCustomerForm").validate({
        rules:{
            'fname':{
                required:true
            },
            'lname':{
                required:true
            },
            'email':{
                required:true,
                email:true
            },
            'email_1':{
                required:true,
                email:true
            },
            'email_2':{
                required:true,
                email:true
            },
            'email_3':{
                required:true,
                email:true
            },
            'phone':{
                required:true,
                maxlength: 10
            },
            'nw_password': {
                minlength: 8
            },            
            'confirm_password':{
                equalTo: '#nw_password'
            },
        },
        messages: { 
            fname: { 
                required: '@lang('validation.required')'
            },
            lname: { 
                required: '@lang('validation.required')'
            },
            email: { 
                required: '@lang('validation.required')',
                email: 'Please provide valid email-id'
            },
            email_1: { 
                // required: '@lang('validation.required')',
                email: 'Please provide valid email-id'
            },
            email_2: { 
                // required: '@lang('validation.required')',
                email: 'Please provide valid email-id'
            },
            email_3: { 
                // required: '@lang('validation.required')',
                email: 'Please provide valid email-id'
            },
            phone:{
                required: '@lang('validation.required')',
                maxlength: 'Please provide a valid 10 digit phone no.'
            },
            nw_password: {
              minlength: '@lang('validation.required')'  
            },
            // confirm_password: {
            //     equalTo: '@lang('validation.required')'  
            // }
        },
        // errorPlacement: function (error, element) 
        // {
        //         // console.log(element.attr("name"));
        //         // console.log(error);
        //         if (element.attr("name") == "fname") {
        //             var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
        //             $('.error_fname').html(error);
        //         }
        //         if (element.attr("name") == "lname") {
        //             var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
        //             $('.error_lname').html(error);
        //         }
        //         if (element.attr("name") == "email") {
        //             var error = '<label for="gn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
        //             $('.error_email').html(error);
        //         }
        //         if (element.attr("name") == "phone") {
        //             var error = '<label for="phn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
        //             $('.error_phone').html(error);
        //         }
        //         if (element.attr("name") == "profile_pic") {
        //             var error = '<label for="pro" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
        //             $('.errorPic').html(error);
        //         }
        //         if (element.attr("name") == "nw_password") {
        //             var error = '<label for="pro" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'Password is required & password length should be 8!'+'</label>';
        //             $('.error_nw_password').html(error);
        //         }
        //         if (element.attr("name") == "confirm_password") {
        //             var error = '<label for="pro" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'Password didnot match!'+'</label>';
        //             $('.error_confirm_password').html(error);
        //         }
        //     }
        });

  });
</script>
<script type="text/javascript">
    function validate(evt) {
    var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>
@endsection