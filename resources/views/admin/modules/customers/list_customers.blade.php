@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage Customer') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.manage_customer')
@endsection
@section('content')
@section('links')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@include('admin.includes.links')


@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
<div class="dashboard-ecommerce">
<div class="container-fluid dashboard-content ">
    <!-- ============================================================== -->
    <!-- pageheader  -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">@lang('admin_lang.manage_customers') </h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                            
                            <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.manage_customers')</li>
                            <!-- <li class="breadcrumb-item"><a href="{{ route('admin.add.customer') }}" class="breadcrumb-link"> Add @lang('admin_lang.customers') </a></li> -->
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="mail_success" style="display: none;">
        <div class="alert alert-success vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
        </div>
    </div>
    <div class="mail_error" style="display: none;">
        <div class="alert alert-danger vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong> @lang('admin_lang.com_err') !</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
        </div>
    </div>
    @if (session()->has('success'))
    <div class="alert alert-success vd_hidden session_suc_msg_show" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
    </div>
    @elseif ((session()->has('error')))
    <div class="alert alert-danger vd_hidden session_err_msg_show" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
    </div>
    @endif
    <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong class="success_msg">@lang('admin_lang.success')!</strong>
    </div>
    <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong class="error_msg">@lang('admin_lang.com_err')!</strong>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                {{-- <h5 class="card-header">@lang('admin_lang.customer_management') <a class="adbtn btn btn-primary" href="{{ route('admin.add.customer') }}"><i class="fas fa-plus"></i> @lang('admin_lang.Add')</a></h5> --}}
                <div class="card-body">
                    <form id="search_customers_form" method="post" action="{{ route('admin.list.customer') }}">
                        @csrf
                        <div class="row">
                            <div data-column="1" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <label for="inputText3" class="col-form-label">@lang('admin_lang.Keywords') </label>
                                <input id="col1_filter" name="keyword" type="text" class="form-control keyword" placeholder="Keywords" value="{{ @$key['keyword'] }}">
                            </div>
                            <div data-column="2" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <label for="input-select" class="col-form-label">@lang('admin_lang.status')</label>
                                <select class="form-control status" id="col2_filter" name="status">
                                    <option value="">@lang('admin_lang.select_status')</option>
                                    <option value="A" @if(@$key['status'] == 'A') selected @endif>@lang('admin_lang.Active')</option>
                                    <option value="I" @if(@$key['status'] == 'I') selected @endif>@lang('admin_lang.Inactive')</option>
                                    <option value="U" @if(@$key['status'] == 'U') selected @endif>@lang('admin_lang.un_verified')</option>
                                </select>
                            </div>
                            {{--<div data-column="3" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <label for="signup_from_lb" class="col-form-label">@lang('admin_lang.signup_from')</label>
                                <select class="form-control signup_from" id="col3_filter" name="signup_from">
                                    <option value="">@lang('admin_lang.select_signup_from')</option>
                                    <option value="S">@lang('admin_lang.web_site')</option>
                                    <option value="F">@lang('admin_lang.facebook')</option>
                                    <option value="G">@lang('admin_lang.google')</option>
                                    <option value="A">Apple</option>
                                </select>
                            </div>--}}
                            {{--<div data-column="4" class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                                <label for="tag-filter" class="col-form-label">@lang('admin_lang.tag')</label>
                                <select class="form-control tag" id="col4_filter" name="tag">
                                    <option value="">@lang('admin_lang_static.select_tag')</option>
                                    <option value="N">@lang('admin_lang.normal')</option>
                                    <option value="V">@lang('admin_lang.vip')</option>
                                    <option value="G">@lang('admin_lang.gold')</option>
                                </select>
                            </div>--}}
                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <a href="javascript:void(0)" id="search_customers" class="btn btn-primary fstbtncls">@lang('admin_lang.Search')</a>

                                <input type="reset" value="@lang('admin_lang.reset_search')" class="btn btn-default fstbtncls reset_search">
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive listBody tpmrgn">
                        <table class="table table-striped table-bordered" id="customers">
                            <thead>
                                <tr>
                                    <th>@lang('admin_lang.Name')</th>
                                    <th>@lang('admin_lang.email')</th>
                                    <th>@lang('admin_lang.phone')</th>
                                    <th>@lang('admin_lang.register_date')</th>
                                    {{-- <th>@lang('admin_lang.signup_from')</th> --}}
                                    {{-- <th>@lang('admin_lang.tag')</th> --}}
                                    <th>@lang('admin_lang.status')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>@lang('admin_lang.Name')</th>
                                    <th>@lang('admin_lang.email')</th>
                                    <th>@lang('admin_lang.phone')</th>
                                    <th>@lang('admin_lang.register_date')</th>
                                    {{-- <th>@lang('admin_lang.signup_from')</th> --}}
                                    {{-- <th>@lang('admin_lang.tag')</th> --}}
                                    <th>@lang('admin_lang.status')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- footer -->   
@include('admin.includes.footer')
<!-- end footer -->
<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>
<div class="modal fade" id="sendMailNotificationModalCenter" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    @lang('admin_lang.send_email_notification')
                </h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="sendEmailForm" method="post" action="">
                        @csrf
                        <div class="form-row">
                            {{-- 
                            <div class="form-group col-md-12"> --}}
                                {{-- <label id="title">Email-id</label> --}}
                                <input id="customer_email" name="customer_email" type="hidden" class="form-control" placeholder="" readonly>
                                <input id="customer_name" name="customer_name" type="hidden" class="form-control" placeholder="" readonly>
                                <input id="customer_id" name="customer_id" type="hidden" class="form-control" placeholder="" readonly>
                                {{-- <span class="error_title" style="color: red;"></span> --}}
                                {{-- 
                            </div>
                            --}}
                            <div class="form-group col-md-12">
                                <label id="titleLabel">@lang('admin_lang.title')</label>
                                <input id="title" name="title" type="text" class="form-control required" placeholder='@lang('admin_lang.title')'>
                                <span class="error_title" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label id="messageLabel">@lang('admin_lang.msg')</label>
                                <textarea id="message" name="message" class="form-control required" rows="5"></textarea>
                                <span class="error_message" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="sendEmailButton" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="showTag">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Customer Tag
                </h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <div class="basic-form">
                    
                    <form id="modalTagFrm" method="post" action="">
                        @csrf
                        <div class="form-row">
                            <div class="showModalMsg col-md-12">
        
                            </div>
                        </div>
                        <div class="form-row">
                                <input id="customer_tag" name="customer_tag" type="hidden" class="form-control" value="">
                                <input id="user_id" name="user_id" type="hidden" class="form-control" value="">
                            
                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <label for="tag" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.tag')</label>
                                <select name="tag" id="tag" class="form-control">
                                    <option value="">@lang('admin_lang_static.select_tag')</option>
                                    <option value="N">@lang('admin_lang.normal')</option>
                                    <option value="V">@lang('admin_lang.vip')</option>
                                    <option value="G">@lang('admin_lang.gold')</option>
                                </select>
                                <span class="error_tag" style="color: red;"></span>
                                </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="saveTag" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">

    function filterColumn ( i ) {
        $('#customers').DataTable().column( i ).search(
            $('#col'+i+'_filter').val(),
        ).draw();
    }

    $(document).ready(function(){
        $(document).on("click","#sendEmailButton",function(e){
            var title = $.trim($("#title").val()),
            id = $.trim($("#customer_id").val()),
            customer_email = $.trim($("#customer_email").val()),
            customer_name = $.trim($("#customer_name").val()),
            message = $.trim($("#message").val());
            // alert(merchant_email);
            if(title == ""){
                $(".error_title").text('@lang('validation.title_error')');
            }else{
                $(".error_title").text("");
            }
            if(message == ""){
                $(".error_message").text('@lang('validation.message_error')');
            }else{
                $(".error_message").text("");
            }
            if(title != "" && message != ""){
                // $("#sendEmailForm").submit();
                var rurl = '{{ route('admin.send.message.customer') }}';
                $('#sendMailNotificationModalCenter').modal('hide');
                $(".mail_success").css({
                    "display":"block"
                });
                $(".successMsg").text('@lang('admin_lang.mail_sending')');
                
                $(".mail_error").css({
                    "display":"none"
                });
                $.ajax({
                    type:"GET",
                    url:rurl,
                    data: {
                        id : id,
                        name : customer_name,
                        email : customer_email,
                        title : title,
                        message : message
                    },
                    success:function(resp){
                        // alert(resp);
                        if(resp==1){
                            $(".mail_success").css({
                                "display":"block"
                            });
                            $(".successMsg").text('@lang('admin_lang.msg_snt')');
                            
                            $(".mail_error").css({
                                "display":"none"
                            });
                            $("#title").val("");
                            $("#message").val("");
                            $('#sendMailNotificationModalCenter').modal('hide');
                        }else{
                            $(".mail_error").css({
                                "display":"block"
                            });
                            
                            $(".errorMsg").text("@lang('admin_lang.msg_not_sent')");
                            $(".mail_success").css({
                                "display":"none"
                            });
                            $("#title").val("");
                            $("#message").val("");
                            $('#sendMailNotificationModalCenter').modal('hide');
                        }
                    }
                });
            }
        });
    
        $(document).on("click",".send_email_notify",function(e){
            var id= $.trim($(e.currentTarget).attr("data-id")),
            email_id= $.trim($(e.currentTarget).attr("data-email")),
            name= $.trim($(e.currentTarget).attr("data-fname")),
            action= $.trim($(e.currentTarget).attr("data-action"));
    
            $('#sendMailNotificationModalCenter').attr("data-id",id);
            $('#sendMailNotificationModalCenter').attr("data-emailid",email_id);
            $('#sendMailNotificationModalCenter').attr("data-name",name);
            $('#sendMailNotificationModalCenter').modal('show');
            $("#customer_email").val(email_id);
            $("#customer_name").val(name);
            $("#customer_id").val(id);
            $("#sendEmailForm").attr("action",action);
        });

        $(document).on("click",".block_unblock_customer",function(e){
            var id = $(e.currentTarget).attr("data-id"),
            status = $(e.currentTarget).attr("data-status");
            if(confirm("@lang('admin_lang.do_u_ch_status_q_12')")){
                $.ajax({
                    url:"{{ route('admin.block.unblock.customer') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp != null){
                            var st="";
                            if(resp.status == 'A'){
                                st="Active";
                                $(".success_msg").html("@lang('admin_lang.suc_customer_unblocked')");
                                $(".icon_change_"+id).removeClass("far fa-check-circle");
                                $(".icon_change_"+id).addClass("fas fa-ban");
                                $(".icon_change_"+id).attr("title","@lang('admin_lang.blocck_cust')");
                            }else if(resp.status == 'I'){
                                st="Inactive";
                                $(".success_msg").html("@lang('admin_lang.suc_customer_blocked')");
                                $(".icon_change_"+id).removeClass("fas fa-ban");
                                $(".icon_change_"+id).addClass("far fa-check-circle");
                                $(".icon_change_"+id).attr("title","@lang('admin_lang.unbbb_cust')");
                            }
                            $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".session_err_msg_show").hide();
                            $(".session_suc_msg_show").hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".session_err_msg_show").hide();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.unauthh_accc_mer_changed')");
                        }
                    }
                });
            }
        });
        
        $(document).on("click",".delete_customer",function(e){
            var id = $(e.currentTarget).attr("data-id");
            var $this = $(this);
            if(confirm("@lang('admin_lang.do_u_delete_cust_qq')")){
                $.ajax({
                    url:"{{ route('admin.delete.customer.details') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".success_msg").html('@lang('admin_lang.success_cust_delete')');
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $this.parent().parent().remove();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('@lang('admin_lang.unauthorized_access_cust')');
                        }
                    }
                });
            }
        });

        $(document).on("click",".verify_customer",function(e){
            var id = $(e.currentTarget).attr("data-id");
            var $this = $(this);
            if(confirm("Do you really want to verify the customer?")){
                $.ajax({
                    url:"{{ route('admin.verify.customer') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp.error){
                            console.log(resp.error);
                        }else{
                            $(".status_"+id).html('Active');
                            $(".success_msg").html(resp.result.message.message);
                            $(".icon_change_"+id).removeClass("fa-check-circle");
                            $(".icon_change_"+id).addClass("fas fa-ban");
                            $(".icon_change_"+id).attr("title","@lang('admin_lang.blocck_cust')");
                        }
                    }
                });
            }
        });

        $('#saveTag').on('click',function(){
            $.ajax({
                url:"{{ route('admin.update.tag') }}",
                type:"POST",
                data:$('#modalTagFrm').serialize(),
                success:function(resp){
                    if(resp == 1){
                        $('.showModalMsg').addClass('alert alert-success');
                        $('.showModalMsg').html("@lang('admin_lang.tag_up_success')");
                        setInterval(function(){
                            location.reload();
                        },1500)
                    }else{
                        $('.showModalMsg').addClass('alert alert-danger');
                        $('.showModalMsg').html("@lang('admin_lang.tag_up_error')");
                    }
                    setInterval(function(){
                            location.reload();
                        },1500)
                }
            });
        })

        // datatable started
        $("#customers_filter").hide();
        var styles = `
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
`
var styleSheet = document.createElement("style");
styleSheet.innerText = styles;
document.head.appendChild(styleSheet);
        $('#customers').DataTable( {
            stateSave: true,
            "order": [[3, "desc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
                $('#col2_filter').val(data.search.status)
                $('#col3_filter').val(data.search.signup_from)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
                data.search.status = $('#col2_filter').val()
                data.search.signup_from = $('#col3_filter').val()
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('admin.list.customer') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
                { 
                    data: 0,
                    render: function(data, type, full) {
                        return full.fname+' '+ full.lname
                    }
                },
                {   data: 'email' },
                { 
                    data: 0,
                    render: function(data, type, full) {
                        if(full.phone == 0) {
                            return '--';
                        } else {
                            return full.phone;
                        }
                    }
                },
                { 
                    data: 'created_at',
                },
                // { 
                //     data: 'signup_from',
                //     render: function(data, type, full) {
                //         if(full.signup_from == 'S') {
                //             return 'Website';
                //         } else if(full.signup_from == 'F') {
                //             return 'Facebook';
                //         } else if(full.signup_from == 'G') {
                //             return 'Google+';
                //         } else if(full.signup_from == 'A') {
                //             return 'Apple';
                //         } else {
                //             return '--';
                //         }
                //     }
                // },
                // { 
                //     data: 'tag',
                //     render: function(data, type, full) {
                //         if(data == 'N') {
                //             return "<span class='text text-primary' >@lang('admin_lang.normal')</span>";
                //         } else if(data == 'V') {
                //             return "<span class='text text-success' >@lang('admin_lang.vip')</span>";
                //         } else if(data == 'G') {
                //             return "<span class='text text-warning' >@lang('admin_lang.gold')</span>";
                //         }  else {
                //             return '--';
                //         }
                //     }
                // },
                { 
                    data: 'status',
                    render: function(data, type, full) {
                        if(data == 'A') {
                            return '<span class="status_'+full.id+'">'+"@lang('admin_lang.Active')"+'</span>'
                        } else if(data == 'I') {
                            return '<span class="status_'+full.id+'">'+"@lang('admin_lang.Inactive')"+'</span>'
                        } else if(data == 'U') {
                            return '<span class="status_'+full.id+'">'+"@lang('admin_lang.unverified')"+'</span>'
                        }
                    }
                },
                { 
                    data: 0,
                    render: function(data, id, full, meta) {
                        var a = '';
                        // status
                        if(full.status == 'U') {
                            a +='<a title="Verify customer" href="javascript:void(0)" class="verify_customer" data-status="'+full.status+'" data-id="'+full.id+'"><i class="fa fa-check-circle icon_change_'+full.id+'" aria-hidden="true"></i></a>' 
                        }
                        if(full.status == 'A') {
                            a +='<a href="javascript:void(0)" class="block_unblock_customer" data-status="'+full.status+'" data-id="'+full.id+'"><i class="fas fa-ban icon_change_'+full.id+'" title='+"@lang('admin_lang.block')"+'></i></a>'
                        } else if(full.status == 'I') {
                            a += '<a class="block_unblock_customer b_u_'+full.id+'" href="javascript:void(0)" data-status="'+full.status+'" data-id="'+full.id+'"><i class="far fa-check-circle icon_change_'+full.id+'" title="'+"@lang('admin_lang.unblock')"+'"></i></a>'
                        } 
                        // view customer profile
                        a += ' <a href="{{ url('admin/view-customer-profile') }}/'+full.id+'"><i class="fas fa-eye" title="'+"@lang('admin_lang.view')"+'"></i></a>'
                        // edit customer details
                        a += ' <a href="{{ url('admin/edit-customer') }}/'+full.id+'"><i class="fas fa-edit" title='+"@lang('admin_lang.Edit')"+'></i></a>'
                        // Change Tag 
                        // a += ' <a href="javascript:;" class="showTagModal" data-id="'+full.id+'" data-tag="'+full.tag+'" ><i class="fa fa-tag" title='+"@lang('admin_lang.change_tag')"+'></i></a>'
                        // send email
                        // a += ' <a href="javascript:void(0)" data-toggle="modal" class="send_email_notify" data-id="'+full.id+'" data-email="'+full.email+'" data-fname="'+full.fname+'" data-lname="'+full.lname+'" data-action="{{ url('send-notification-customer') }}/'+full.id+'"><i class="fas fa-paper-plane" title='+"@lang('admin_lang.Send Email')"+'></i></a>'
                        // delete customer
                        a += ' <a class="delete_customer" href="javascript:void(0)" data-id="'+full.id+'"><i class=" fas fa-trash"  title='+"@lang('admin_lang.Delete')"+'></i></a>'

                        return a;
                    }
                }
            ]
        });
        
        // change event
        $('input.keyword').on( 'keyup click blur', function () {
            filterColumn( 1 );
        });
        $('.status').on( 'change', function () {
            filterColumn( 2 );
        }); 
        $('.signup_from').on( 'change', function () {
            filterColumn( 3 );
        });
        $('.tag').on( 'change', function () {
            filterColumn( 4 );
        });
        
        $('body').on('click', '.reset_search', function() {
            $('#customers').DataTable().search('').columns().search('').draw();
            $(".slct").val("").trigger("chosen:updated");
        })

        $('body').on('click','.showTagModal',function(){
            var id = $(this).attr('data-id');
            var tag = $(this).attr('data-tag');

            $('#customer_tag').val(tag);
            $('#user_id').val(id);
            $('#showTag').modal('show');
        });
    });
</script>
@endsection