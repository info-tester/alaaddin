<table class="table table-striped table-bordered first" id="myTable">
                            <thead>
                                <tr>
                                    <th>@lang('admin_lang.Name')</th>
                                    <th>@lang('admin_lang.email')</th>
                                    <th>@lang('admin_lang.phone')</th>
                                    <th>@lang('admin_lang.signup_from')</th>
                                    <th>@lang('admin_lang.status')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(@$customers)
                                @foreach(@$customers as $customer)
                                <tr class="tr_{{ @$customer->id }}">
                                    <td>{{ @$customer->fname." ".@$customer->lname }}</td>
                                    <td>{{ @$customer->email }}</td>
                                    <td>{{ @$customer->phone }}</td>
                                    <td>
                                        @if(@$customer->signup_from == 'S')
                                        @lang('admin_lang.web_site')
                                        @elseif(@$customer->signup_from == 'F')
                                        @lang('admin_lang.facebook')
                                        @elseif(@$customer->signup_from == 'G')
                                        @lang('admin_lang.google')
                                        @endif
                                    </td>
                                    <td class="status_{{ @$customer->id }}">
                                        @if(@$customer->status == 'A')
                                        @lang('admin_lang.Active')
                                        @elseif(@$customer->status == 'I')
                                        @lang('admin_lang.Inactive')
                                        @elseif(@$customer->status == 'U')
                                        @lang('admin_lang.un_verified')
                                        @endif
                                    </td>
                                    <td>
                                        @if(@$customer->status == 'I')
                                        <a class="block_unblock_customer b_u_{{ @$customer->id }}" href="javascript:void(0)" data-status="{{ @$customer->status }}" data-id="{{ @$customer->id }}"><i class="far fa-check-circle icon_change_{{ @$customer->id }}" title="Unblock"></i></a>
                                        @elseif(@$customer->status == 'A')
                                        <a href="javascript:void(0)" class="block_unblock_customer" data-status="{{ @$customer->status }}" data-id="{{ @$customer->id }}"><i class="fas fa-ban icon_change_{{ @$customer->id }}" title="Block"></i></a>
                                        @endif
                                        <a href="{{ route('admin.view.customer.profile',@$customer->id) }}"><i class="fas fa-eye" title="View"></i></a>
                                        <a href="{{ route('admin.edit.customer',@$customer->id) }}"><i class="fas fa-edit" title="Edit"></i></a>
                                        <a href="javascript:void(0)" data-toggle="modal" class="send_email_notify" data-id="{{ @$customer->id }}" data-email="{{ @$customer->email }}" data-fname="{{ @$customer->fname }}" data-lname="{{ @$customer->lname }}" data-action="{{ route('admin.send.notification.customer',@$customer->id) }}"><i class="fas fa-paper-plane" title="Send Email"></i></a>
                                        <a class="delete_customer" href="javascript:void(0)" data-id="{{ @$customer->id }}"><i class=" fas fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>@lang('admin_lang.Name')</th>
                                    <th>@lang('admin_lang.email')</th>
                                    <th>@lang('admin_lang.phone')</th>
                                    <th>@lang('admin_lang.signup_from')</th>
                                    <th>@lang('admin_lang.status')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </tfoot>
                        </table>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>