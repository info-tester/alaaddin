@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | Admin | View Customer Profile
@endsection
@section('content')
@section('links')
@include('admin.includes.links')
<link rel="stylesheet" href="{{ asset('public/admin/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
<link href="{{ asset('public/admin/assets/vendor/fonts/circular-std/style.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('public/admin/assets/libs/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('public/admin/assets/vendor/fonts/fontawesome/css/fontawesome-all.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/buttons.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/select.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/fixedHeader.bootstrap4.css') }}">

@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== --> 
            <!-- pageheader --> 
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.view_customer_profile')</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('admin.list.customer') }}" class="breadcrumb-link"> Manage Customers</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">View Customer Profile</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== --> 
            <!-- end pageheader --> 
            <!-- ============================================================== -->
            
            @include('admin.includes.s_e_messages')
            <div class="row">
                <!-- ============================================================== --> 
                <!-- basic table  --> 
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">@lang('admin_lang.view_customer_profile')
                            <a class="adbtn btn btn-primary" href="{{ route('admin.list.customer') }}">@lang('admin_lang.back')</a>
                            
                            @if(@$customer->wallet_amount > 0 && @$customer->bank_name && @$customer->branch_name && @$customer->account_number && @$customer->account_name && @$customer->ifsc_code)
                            <a class="adbtn btn btn-primary" href="{{ route('admin.customer.withdraw',@$customer->id) }}">Customer Withdraw</a>
                            @endif
                        </h5>
                        <div class="card-body">
                            <div class="manager-dtls tsk-div">
                                <div class="row fld">
                                    <div class="col-md-12">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.customer_details')</h4>
                                    </div>
                                    <div class="col-md-2" style="padding-right:0px;">
                                        @if(@$customer->image)
                                        @php
                                        $image_path = 'storage/app/public/customer/profile_pics/'.@$customer->image; 
                                        @endphp
                                        @if(file_exists(@$image_path))
                                        <div class="uplodpic">
                                            <li>
                                                <img src="{{ URL::to('storage/app/public/customer/profile_pics/'.@$customer->image) }}" style="max-width: 100%;max-height: 100%;">
                                            </li>
                                        </div>
                                        @endif
                                        @endif
                                    </div>
                                    <div class="col-md-5" {{--style="padding-left:0px;"--}}>
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong> {{ @$customer->fname." ".@$customer->lname }}</span> </p>
                                        <p><span class="titel-span">@lang('admin_lang.email_address_label')</span> <span class="deta-span"><strong>:</strong> {{ @$customer->email }}</span> </p>
                                        <p><span class="titel-span">@lang('admin_lang.phone_number')</span> <span class="deta-span"><strong>:</strong> {{ @$customer->phone }}</span></p>
                                        <p><span class="titel-span">SignUp From </span> <span class="deta-span"><strong>:</strong>

                                         @if(@$customer->signup_from == 'G')
                                         Google
                                         @elseif(@$customer->signup_from == 'F')
                                         Facebook
                                         @elseif(@$customer->signup_from == 'S')
                                         App
                                         @elseif(@$customer->signup_from == 'A')
                                         Apple
                                         @endif
                                        </span></p>
                                        <p><span class="titel-span">Wallet :</span> <span class="deta-span"><strong>:</strong>{{ getCurrency() }} {{ @$customer->wallet_amount }}</span></p>
                                        @if(!empty(@$customer->gender))
                                        <p>
                                            <span class="titel-span">Gender</span> 
                                            <span class="deta-span"><strong>:</strong>
                                                @if(@$customer->gender == 'M')
                                                Male
                                                @elseif(@$customer->gender == 'F')
                                                Female
                                                @else
                                                Others
                                                @endif
                                            </span>
                                        </p>
                                        @endif
                                        @if(!empty(@$customer->date_of_birth))
                                        <p>
                                            <span class="titel-span">Date Of Birth</span> 
                                            <span class="deta-span"><strong>:</strong>
                                                {{ @$customer->date_of_birth }}
                                            </span>
                                        </p>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <p><span class="titel-span">Country </span> <span class="deta-span"><strong>:</strong> {{ @$customer->userCountryDetails->countryDetailsBylanguage->name }}</span> </p>
                                        <p><span class="titel-span">State </span> <span class="deta-span"><strong>:</strong> {{ @$customer->state_details->name }}</span> </p>
                                        <p><span class="titel-span">City </span> <span class="deta-span"><strong>:</strong> {{ @$customer->city_details->name }} </span> </p>
                                        <p><span class="titel-span">Zipcode </span> <span class="deta-span"><strong>:</strong> {{ @$customer->zipcode }}</span> </p>
                                        <p><span class="titel-span">Bank Name </span> <span class="deta-span"><strong>:</strong> {{ @$customer->bank_name }}</span> </p>
                                        <p><span class="titel-span">Account Name  </span> <span class="deta-span"><strong>:</strong> {{ @$customer->account_name }}</span> </p>
                                        <p><span class="titel-span">Account Number  </span> <span class="deta-span"><strong>:</strong> {{ @$customer->account_number }}</span> </p>
                                        <p><span class="titel-span">IFSC Code  </span> <span class="deta-span"><strong>:</strong> {{ @$customer->ifsc_code }}</span> </p>
                                    </div> 
                                </div>
                            </div>
                            <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.order_of_this_customer')</h4>
                            <div class="tab wallet-tab form-group ">
                                <button class="tablinks" id="1tablinks" onclick="openV(event, '1')">Order </button>
                                {{-- location.href='{{ route('admin.customer.specific.wallet.history',@$customer->id) }}' --}}
                                <button class="tablinks" id="2tablinks" onclick="openV(event, '2')">Wallet History </button>
                                <button class="tablinks" id="3tablinks" onclick="openV(event, '3')">Customer Withdrawl </button>
                            </div>

                            <!-- Tab content -->
                            <div id="1" class="tabcontent" >
                                <h3>Order History</h3>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="orders">
                                    <thead>
                                        <tr>
                                            
                                            <th>@lang('admin_lang.order_no')</th>
                                            <th>@lang('admin_lang.date')</th>
                                            <th>@lang('admin_lang.customers')</th>
                                            <th>@lang('admin_lang.order_total') ({{ getCurrency() }})</th>
                                            <th>@lang('admin_lang.payment_1')</th>
                                            <th>@lang('admin_lang.Status')</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>@lang('admin_lang.order_no')</th>
                                            <th>@lang('admin_lang.date')</th>
                                            <th>@lang('admin_lang.customers')</th>
                                            <th>@lang('admin_lang.order_total') ({{ getCurrency() }})</th>
                                            <th>@lang('admin_lang.payment_1')</th>
                                            <th>@lang('admin_lang.Status')</th>
                                            
                                        </tr>
                                    </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div id="2" class="tabcontent" style="display:none;">
                                <h3>Wallet History</h3>
                                <div class="table-responsive listBody">
                                    <table class="table table-striped table-bordered first" id="myTable1">
                                    <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Order Number</th>
                                            <th>Customer Name</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            
                                            <th>Product Name</th>
                                            <th>Order Number</th>
                                            <th>Customer Name</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                        </tr>
                                    </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div id="3" class="tabcontent" style="display:none;">
                            <h3>Customer Withdrawl</h3>
                            <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="customer_withdrawl">
                                    <thead>
                                        <tr>
                                            <th>Bank Name</th>
                                            <th>Branch Name</th>
                                            <th>Account Number</th>
                                            <th>Account Name</th>
                                            
                                            <th>IFSC Code</th>
                                            <th>Amount(Rs)</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Bank Name</th>
                                            <th>Branch Name</th>
                                            <th>Account Number</th>
                                            <th>Account Name</th>
                                            <th>IFSC Code</th>
                                            <th>Amount(Rs)</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                        </tr>
                                    </tfoot>
                                    </table>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <!-- ============================================================== --> 
                <!-- end basic table  --> 
                <!-- ============================================================== --> 
            </div>
        </div>
        
        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
    </div>
</div>
@endsection
@section('scripts')
@include('admin.includes.scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" />

<script type="text/javascript">
$(document).ready(function(){
        
        
    var table = $('#orders').DataTable({
        "destroy": true, //use for reinitialize datatable
        stateSave: false,
        order: [
            [0, "desc"]
        ],
        stateLoadParams: function (settings, data) {
        },
        stateSaveParams: function (settings, data) {
        },
        "createdRow": function( row, data, dataIndex ) { 
            var id = (JSON.stringify(data.id));
            var r = JSON.stringify(dataIndex);
            $(row).addClass("tr_class_"+id);
            $(row).addClass("tr_row_class");
        },
        "processing": true,
        "serverSide": true,
        "serverMethod": 'post',
        "ajax": {
            "url": "{{ route('admin.view.customer.specific.orders',$customer->id) }}",
            "data": function (d) {
                d._token = "{{ @csrf_token() }}";
            },
            "dataSrc": function(response) {
                
                return response.aaData
            }
        },
        columnDefs: [ { targets: 0, type: 'natural' } ],
        'columns': [
            {
                data: 'order_no',
            },
            {
                data: 'created_at'
            },
            {
                data: 'customer_details.fname',
                render:function(data, type, full, meta){
                    if(full.customer_details){
                        var n = full.customer_details.fname+" "+full.customer_details.lname;
                        return n
                    }else{
                        return '';
                    }
                    
                }
            },
            {
                data: "order_total"
            },
            {
                data: 'payment_method',
                render: getPaymentMethod
            },
            {
                data: 'status',
                render:getStatus
            },
        ]
    });

    function getStatus(data, type, full, meta) {
        if(data == 'I'){
            return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.incomplete')"+"</span>"
        } else if(data == 'N'){
            return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.new')"+"</span>"
        } else if(data == 'OA'){
            return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.order_accepted')"+"</span>"
        } else if(data == 'DA'){
            return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.driver_assigned')"+"</span>"
        } else if(data == 'RP'){
            return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.ready_for_pickup')"+"</span>"
        } else if(data == 'OP'){
            return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.ord_picked_up')"+"</span>"
        } else if(data == 'OD'){
            return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.ord_dlv')"+"</span>"
        } else if(data == 'OC'){
            return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.order_canceled')"+"</span>"
        } else if(data == 'PP'){
            return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.partial_pickup')"+"</span>"
        } else if(data == 'PC'){
            return "<span class='status_"+full.id+"'>"+"@lang('admin_lang.partial_pickup_completed')"+"</span>"
        } else if(data == 'F'){
            return "<span class='status_"+full.id+"'>Payment Failed</span>"
        } else if(data == 'INP'){
            return "<span class='status_"+full.id+"'>In Progress</span>"
        } else if(data == 'CM'){
            return "<span class='status_"+full.id+"'>Completed</span>"
        }
    }
    function getPaymentMethod(data, type, full, meta){
        if(data == 'C'){
            return "<span class='change_payment_"+full.id+"'>"+"@lang('admin_lang.cod')"+"</span>"
        }
        else if(data == 'O'){
            return "<span class='change_payment_"+full.id+"'>"+"@lang('admin_lang.online')"+"</span>"
        }else{
            return "<span class='change_payment_"+full.id+"'>--</span>"
        }
    }
    function getOrderType(data, type, full, meta){
        if(data == 'I'){
            return "@lang('admin_lang.internal_1')"
        }
        else if(data == 'E'){
            return '@lang('admin_lang.external_1')'
        }
    }
    function getCustomerName(data, type, full, meta){
        return data.shipping_fname+' '+data.shipping_lname ;
    }
    $("#orders_filter").hide();
    $("#customer_withdrawl_filter").hide();
    $("#myTable1_filter").hide();
});
</script>
<script>
function openV(evt, nm) {

  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(nm).style.display = "block";
  evt.currentTarget.className += " active";
  if(nm == "3"){
    $('#customer_withdrawl').DataTable({
        "destroy": true, //use for reinitialize datatable
        stateSave: false,
        order: [
            [0, "desc"]
        ],
        stateLoadParams: function (settings, data) {
            
        },
        stateSaveParams: function (settings, data) {
        },
        processing: true,
        serverSide: true,
        serverMethod: 'post',
        ajax: {
            "url": "{{ route('admin.view.customer.withdraw.details',@$customer->id) }}",
            "data": function (d) {
                d._token = "{{ @csrf_token() }}";
            },
            "dataSrc": function(response) {
                return response.aaData
            }
        },
        'columns': [
            
            {
                data: 'bank_name',
                render:function(data, type, full, meta){
                    return data;
                }
            },
            {
                data: 'branch_name',
                render:function(data, type, full, meta){
                    return data;
                }
            },
            {
                data: 'account_number',
                render:function(data, type, full, meta){
                    return data;
                }
            },
            {
                data: 'account_name',
                render:function(data, type, full, meta){
                    

                    return data;
                }
            },
            {
                data: 'ifsc_code',
                render:function(data, type, full, meta){
                    return data;
                }
            },
            {
                data: 'amount',
                render:function(data, type, full, meta){
                    
                    return "Rs "+data;
                }
            },
            {
                data: 'description',
                render:function(data, type, full, meta){
                    
                    return data;
                }
            },
            {
                data: 'buyer_withdrawl_date',
                render:function(data, type, full, meta){
                    return data;
                }
            },
            
        ]
    });
    $("#orders_filter").hide();
    $("#customer_withdrawl_filter").hide();
    $("#myTable1_filter").hide();
  }
  if(nm == "2"){
    $('#myTable1').DataTable({
            "destroy": true, //use for reinitialize datatable
            stateSave: false,
            order: [
                [0, "desc"]
            ],
            stateLoadParams: function (settings, data) {
            },
            stateSaveParams: function (settings, data) {
            },
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            ajax: {
                "url": "{{ route('admin.customer.specific.view.wallet.details',$customer->id) }}",
                "data": function (d) {
                    d._token = "{{ @csrf_token() }}";
                },
                "dataSrc": function(response) {
                    return response.aaData
                }
            },
            'columns': [
                {
                    data: 'product_by_language.title',
                    render:function(data, type, full, meta){
                        return data;
                    }
                },
                {
                    data: 'get_order_master.order_no',
                    render:function(data, type, full, meta){
                        return data;
                    }
                },
                {
                    data: 'view_user.fname',
                    render:function(data, type, full, meta){
                        var nm = full.view_user.fname+" "+full.view_user.lname;

                        return nm;
                    }
                },
                {
                    data: 'amount',
                    render:function(data, type, full, meta){
                        return "Rs "+data;
                    }
                },
                {
                    data: 'w_d_date',
                    render:function(data, type, full, meta){
                        
                        return data;
                    }
                },
                
            ]
    });
    $("#orders_filter").hide();
    $("#customer_withdrawl_filter").hide();
    $("#myTable1_filter").hide();
  }
}
</script>
@endsection