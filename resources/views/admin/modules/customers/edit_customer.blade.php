@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | Admin | Edit Customer
@endsection
@section('content')
@section('links')
@include('admin.includes.links')
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('admin.list.customer') }}" class="breadcrumb-link"> Manage Customers</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        Edit Customer
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.includes.s_e_messages')
            {{-- @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif --}}
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                @lang('admin_lang.edit_customer')
                                <a class="adbtn btn btn-primary" href="{{ route('admin.list.customer') }}">
                                    <i class="fas fa-less-than"></i>
                                    Back
                                </a>
                            </h5>
                            <div class="card-body">
                                <form id="editCustomerForm" method="post" enctype="multipart/form-data" action="{{ route('admin.edit.customer',@$customer->id) }}">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            {{-- <input type="hidden" id="old_email" value="{{ @$customer->email }}"/> --}}
                                            <label for="fname" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.first_name')</label>
                                            <input id="fname" name="fname" type="text" class="form-control required" placeholder="@lang('admin_lang.first_name')" value="{{ @$customer->fname }}">
                                            <span class="error_fname error" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="lname" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.last_name')</label>
                                            <input id="lname" name="lname" type="text" class="form-control required" placeholder="@lang('admin_lang.last_name')" value="{{ @$customer->lname }}">
                                            <span class="error_lname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="email" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.email')</label>
                                            <input id="email" name="email" type="email" placeholder="@lang('admin_lang.email')" class="form-control " value="{{ @$customer->email }}">
                                            <span class="error_email" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="phone" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.phone_number')</label>
                                            <input id="phone" name="phone" type="text" class="form-control " placeholder="@lang('admin_lang.phone_number')" value="{{ @$customer->phone }}" onkeypress='validate(event)' maxlength="10">
                                            <span class="error_phone" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 fstbtncls">
                                            <input type="file" class="custom-file-input" id="profile_pic" name="profile_pic">
                                            <label class="custom-file-label extrlft" for="profile_pic">@lang('admin_lang.upload_picture')</label>
                                            <span> @lang('admin_lang.recommended_sze_100')</span>                                               
                                            <span class="errorPic" style="color: red;"></span>
                                            
                                            @if(@$customer->image)
                                            @php
                                            $image_path = 'storage/app/public/customer/profile_pics/'.@$customer->image; 
                                            @endphp
                                            @if(file_exists(@$image_path))
                                            <div class="profile" style="display: block;">
                                                <img id="profilePicture" src="{{ URL::to('storage/app/public/customer/profile_pics/'.@$customer->image) }}" alt="" style="width: 100px;height: 100px;">
                                            </div>
                                            @endif
                                            @endif
                                            {{-- <img src="" id="profilePicture" style="height: 100px;width: 100px;"> --}}
                                            
                                        </div>
                                            <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                                {{-- <a href="javascript:void(0)" id="addCustomer" class="btn btn-primary ">@lang('admin_lang.save')</a> --}}
                                                <input type="submit" value="Save" class="btn btn-primary">
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
        @endsection
        @section('scripts')
        @include('admin.includes.scripts')
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <script type="text/javascript">

            $(document).ready(function(){
                // $("#addCustomer").click(function(){
                    // $("#editCustomerForm").submit();
                // });
                jQuery.validator.addMethod("emailonly", function(value, element) {
                return this.optional(element) || /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/.test(value.toLowerCase());
                }, "Enter a valid email address");

                jQuery.validator.addMethod("mobileonly", function(value, element) {
                return this.optional(element) ||  /^[+]?\d+$/.test(value.toLowerCase());
                }, "Enter a valid 10 digit mobile number");
        $("#profile_pic").change(function() {
            var filename = $.trim($("#profile_pic").val()),
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".errorpic").text("");
                readURL(this);
            }else{

                $(".errorpic").text("@lang('validation.image_extension_type')");
                $('#profilePicture').attr('src', "");

            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                  $('#profilePicture').attr('src', e.target.result);
                  $(".profile").show();
              }
              reader.readAsDataURL(input.files[0]);
          }
        }
    $(document).on("blur","#email",function(){
        var email = $.trim($("#email").val()),
        old_email = $.trim($("#old_email").val());

        if(email != "")
        {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(regex.test(email)){
                var rurl = "{{ route('customer.duplicate.email') }}";
                $.ajax({
                    type:"GET",
                    url:rurl,
                    data:{
                        email:email,
                        old_email:old_email
                    },
                    success:function(resp){
                            // alert(resp);
                            if(resp == 0){
                                $(".error_email").text("@lang('validation.email_id_already_exist')");
                                $("#email").val("");
                            }else{
                                $(".error_email").text("");
                            }
                        }
                    }); 
            }
            else
            {
                // alert("if email format false");
                $(".errorEmail").text("@lang('validation.please_provide_valid_email_id')");
            }
        }else{
            // alert("if blank");
            $(".errorEmail").text("@lang('validation.please_provide_valid_email_id')");        
        }
    });

    $(document).on("blur","#phone",function(){
        var phone = $.trim($("#phone").val()),
        old_phone = '{{ @$customer->phone }}';
        
        if(phone != "")
        {
            var rurl = "{{ route('customer.duplicate.update.phone') }}";
            $.ajax({
                type:"GET",
                url:rurl,
                data:{
                    phone:phone,
                    old_phone:$.trim(old_phone)
                },
                success:function(resp){
                    if(resp == 0){

                        $(".error_phone").text("@lang('admin_lang.phone_number_already_exist')");
                        $("#phone").val("");
                    }
                }
            });  
        }
    });
    $("#editCustomerForm").validate({
    rules:{
        'fname':{
            required:true
        },
        'lname':{
            required:true
        },
        'email':{
            required:true,
            emailonly:true,
            email:true,
            remote:{
                url:"{{route('check.customer.duplicate.email.ajax')}}",
                type:"POST",
                data: {
                    user_id: function() {
                        return {{ @$customer->id }};
                    },
                    email: function() {
                        return $.trim($("#email").val());
                    },
                    _token: '{{ csrf_token() }}'
                }
            }
        },
        'phone':{
            required:true,
            digits:true,
            minlength:10,
            maxlength:12,
            remote:{
                url:"{{route('check.customer.duplicate.phone.ajax')}}",
                type:"POST",
                data: {
                    user_id: function() {
                        return {{ @$customer->id }};
                    },
                    phone: function() {
                        return $.trim($("#phone").val());
                    },
                    _token: '{{ csrf_token() }}'
                }
            },
            mobileonly:true
        },
        'nw_password': {
            minlength: 8
        },            
        'confirm_password':{
            equalTo: '#nw_password'
        },
        tag:{
            required:true
        }
    },
    messages: { 
        fname: { 
            required: "@lang('validation.required')"
        },
        lname: { 
            required: "@lang('validation.required')"
        },
        email: { 
            required: "@lang('validation.required')",
            remote: "This email-id is already exists",
            emailonly: "please provide valid email-id",
            email: "@lang('admin_lang.pl_pr_v_email')"
        },
        phone:{
            required: "@lang('validation.required')",
            remote: "This phone number is already exists",
            digits: "Please provide valid mobile number",
            minlength: "Please provide valid mobile number with minimum 10 digits only",
            maxlength: "Please provide valid mobile number with minimum 12 digits only",
            mobileonly: "Please provide valid mobile number"
        },
        'nw_password': {
            minlength: "@lang('validation.required')"
        },            
        'confirm_password':{
            equalTo: "@lang('validation.required')"
        },
        tag:{
            required:"@lang('validation.required')"
        }
    },
    errorPlacement: function (error, element) 
    {
        console.log(element.attr("name"));
            // console.log(error);
            if (element.attr("name") == "fname") {
                var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                $('.error_fname').html(error);
            }
            if (element.attr("name") == "lname") {
                var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                $('.error_lname').html(error);
            }
            if (element.attr("name") == "email") {
                var error = '<label for="gn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"Please provide valid email-id or this email-id is already exists"+'</label>';
                $('.error_email').html(error);
            }
            if (element.attr("name") == "phone") {
                var error = '<label for="phn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"Please provide valid 10 digits mobile number or this phone number is already exist"+'</label>';
                $('.error_phone').html(error);
            }
            if (element.attr("name") == "profile_pic") {
                var error = '<label for="pro" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                $('.errorPic').html(error);
            }
            if (element.attr("name") == "nw_password") {
                var error = '<label for="pro" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'Password is required & password length should be 8!'+'</label>';
                $('.error_nw_password').html(error);
            }
            if (element.attr("name") == "confirm_password") {
                var error = '<label for="pro" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'Password didnot match!'+'</label>';
                $('.error_confirm_password').html(error);
            }
            if (element.attr("name") == "tag") {
                var error = '<label for="tag" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+"@lang('validation.required')"+'</label>';
                $('.error_tag').html(error);
            }
        }
    });

});
</script>
<script type="text/javascript">
    function validate(evt) {
        var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>
@endsection