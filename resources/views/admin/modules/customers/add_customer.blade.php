@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Add Customer') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.add_customer')
@endsection
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.add_customer')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                @lang('admin_lang.add_customer')
                            </h5>
                            <div class="card-body">
                                <form id="addCustomerForm" method="post" enctype="multipart/form-data" action="{{ route('admin.add.customer') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="fname" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.first_name')</label>
                                            <input id="fname" name="fname" type="text" class="form-control required" placeholder="@lang('admin_lang.first_name')">
                                            <span class="error_fname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="lname" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.last_name')
                                                <span style="color: red;">*</span>
                                            </label>
                                            <input id="lname" name="lname" type="text" class="form-control required" placeholder="@lang('admin_lang.last_name')">
                                            <span class="error_lname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="email" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.email')</label>
                                            <input id="email" name="email" type="email" placeholder="@lang('admin_lang.email')" class="form-control required">
                                            <span class="error_email" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="tag" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.tag')</label>
                                            <select name="tag" id="tag" class="form-control">
                                                <option value="">@lang('admin_lang_static.select_tag')</option>
                                                <option value="N">@lang('admin_lang.normal')</option>
                                                <option value="V">@lang('admin_lang.vip')</option>
                                                <option value="G">@lang('admin_lang.gold')</option>
                                            </select>
                                            <span class="error_tag" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="phone" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.phone_number')</label>
                                            <input id="phone" name="phone" type="text" class="form-control required" placeholder="@lang('admin_lang.phone_number')" onkeypress='validate(event)'>
                                            <span class="error_phone" style="color: red;"></span>
                                        </div>
                                        
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="inputPassword" class="col-form-label">
                                                <span style="color: red;">*</span>@lang('admin_lang.password')
                                            </label>
                                            <input id="password" name="password" type="password" placeholder="@lang('admin_lang.password')" class="form-control required">
                                            <span>
                                                @lang('admin_lang.password_length_should_be_8')
                                            </span>
                                            <span class="errorPassword" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="inputPassword" class="col-form-label">
                                                <span style="color: red;">*</span>@lang('admin_lang.confirm_password')
                                            </label>
                                            <input id="confirm_password" name="confirm_password" type="password" placeholder="@lang('admin_lang.confirm_password')" class="form-control required">
                                            <span class="errorConfirmPassword" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 fstbtncls">
                                            <input type="file" class="custom-file-input" id="profile_pic" name="profile_pic">
                                            <label class="custom-file-label extrlft" for="profile_pic">@lang('admin_lang.upload_picture')</label>
                                            <span> @lang('admin_lang.recommended_sze_100')</span>
                                            <span class="errorPic" style="color: red;"></span>
                                            <div class="profile" id="profileSt" style="display: none;">
                                                <img src="" id="profilePicture">
                                            </div>
                                        </div>

                                        <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">

                                            <a href="javascript:void(0)" id="addCustomer" class="btn btn-primary ">@lang('admin_lang.save')</a>
                                        </div>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
    jQuery.validator.addMethod("checksidebar",function(value, element, params){
        var sidebar_checked = $(".cksidebar:checked").length;
        alert("addmethod");
        return this.optional(element) || (sidebar_checked > 0 ) 
        
    },
    '@lang('admin_lang.check_atleast_one_sidebar')'
    );
    $(document).ready(function(){
        $("#addCustomer").click(function(){
            $("#addCustomerForm").submit();
        });
        // $(document).on("blur",'#password',function(e){
        //     $("#password").val("");
        // });
        $("#profile_pic").change(function(){
            var filename = $.trim($("#profile_pic").val()),
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".errorpic").text("");
                readURL(this);
                $('#profilePicture').css({"height": "100px", "width": "100px"});
                $('#profileSt').css({"height": "110px"});
            }else{

                $(".errorpic").text("@lang('admin_lang.please_upload_profile_picture_with')");
                $('#profilePicture').attr('src', "");

            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                  $('#profilePicture').attr('src', e.target.result);
                  $(".profile").show();
              }
              reader.readAsDataURL(input.files[0]);
          }
      }
      $(document).on("blur","#email",function(){
            // alert("hiiio");
            var email = $.trim($("#email").val());
            if(email != "")
            {
                var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(regex.test(email)){

                    var rurl = "{{ route('check.customer.duplicate.email') }}";
                    $.ajax({
                        type:"GET",
                        url:rurl,
                        data:{
                            email:email
                        },
                        success:function(resp){
                            // alert(resp);
                            if(resp == 0){
                                $(".error_email").text("@lang('validation.email_id_already_exist')");
                                $("#email").val("");
                            }else{
                                $(".error_email").text("");
                            }
                        }
                    }); 
                }else{
                    // alert("if email format false");
                    $(".errorEmail").text("@lang('validation.please_provide_valid_email_id')");
                }
                
            }else{
                // alert("if blank");
                $(".errorEmail").text("@lang('validation.please_provide_valid_email_id')");
                
            }
        });

    /*$(document).on("blur","#phone",function(){
        var phone = $.trim($("#phone").val());
        
        if(phone != "")
        {
            var rurl = "{{ route('check.customer.duplicate.phone') }}";
            $.ajax({
                type:"GET",
                url:rurl,
                data:{
                    phone:phone
                },
                success:function(resp){
                    if(resp == 0){

                        $(".error_phone").text("@lang('admin_lang.phone_number_already_exist')");
                        $("#phone").val("");
                    }
                }
            });  
        }
    });*/
      $("#addCustomerForm").validate({
        rules:{
            'fname':{
                required:true
            },
            'lname':{
                required:true
            },
            'phone':{
                required:true,
                digits:true
            },
            'email':{
                required:true,
                email:true
            },
            'profile_pic':{
                accept:"image/jpg,image/jpeg,image/png"
            },
            'password': {
                minlength: 8
            },            
            'confirm_password':{
                equalTo: '#password'
            }
        },
        messages: { 
            fname: { 
                required: "@lang('validation.required')"
            },
            lname: { 
                required: "@lang('validation.required')"
            },
            phone: { 
                required: "@lang('validation.required')"
            },
            email: { 
                required: "@lang('validation.required')"
            },
            profile_pic: { 
                accept: "@lang('admin_lang.please_upload_img_with')"
            },
            confirm_password:"@lang('validation.required')"
        },
        errorPlacement: function (error, element) 
        {
                // console.log(element.attr("name"));
                // console.log(error);
                if (element.attr("name") == "fname") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.error_fname').html(error);
                }
                if (element.attr("name") == "lname") {
                    var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.error_lname').html(error);
                }
                if (element.attr("name") == "email") {
                    var error = '<label for="gn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.error_email').html(error);
                }
                if (element.attr("name") == "phone") {
                    var error = '<label for="phn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.error_phone').html(error);
                }
                if (element.attr("name") == "profile_pic") {
                    var error = '<label for="pro" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.errorPic').html(error);
                }
                if (element.attr("name") == "password") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
                    $('.errorPassword').html(error);
                }
                if (element.attr("name") == "confirm_password") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.confirm_password')"+'</label>';
                    $('.errorConfirmPassword').html(error);
                }
                
            }
        });

  });
</script>
<script type="text/javascript">
    function validate(evt) {
    var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}

</script>
@endsection