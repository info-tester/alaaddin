@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Add City') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.edit_coupon')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/jquery.datetimepicker.css') }}">
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.edit_coupon')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                @lang('admin_lang.edit_coupon')
                            </h5>
                            <div class="card-body">
                                <form id="addCoupon" method="post" action="{{ route('admin.update.coupon') }}">
                                    @csrf
                                    <input type="hidden" name="id" id="coupon_id" value="{{ @$coupon->id }}">
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="code" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.coupon')</label>
                                            <input  name="code" type="text" id="code" class="form-control required" value="{{ @$coupon->code }}" placeholder="@lang('admin_lang.coupon')">
                                            <span class="code-error" style="color: red;"></span>
                                        </div>

                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="start_date" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.start_date')</label>
                                            <input  name="start_date" id="start_date" type="text" class="form-control required" placeholder="@lang('admin_lang.start_date')" value="{{ @$coupon->start_date }}" readonly>
                                            <span class="start_date-error" style="color: red;"></span>
                                        </div>
                                        
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="end_date" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.end_date')</label>
                                            <input  name="end_date" id="end_date" type="text" class="form-control required" placeholder="@lang('admin_lang.end_date')" value="{{ @$coupon->end_date }}" readonly>
                                            <span class="end_date-error" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="no_of_times" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.no_of_times')</label>
                                            <input  name="no_of_times" type="text" class="form-control required" placeholder="@lang('admin_lang.no_of_times')" value="{{ @$coupon->no_of_times }}" >
                                            <span class="no_of_times-error" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="discount" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.discount')</label>
                                            <input  name="discount" type="text" class="form-control required" placeholder="@lang('admin_lang.discount')" value="{{ @$coupon->discount }}">
                                            <span class="discount-error" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="discount_type" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.discount_type')</label>
                                            <select name="discount_type" class="form-control required">
                                                <option value="">@lang('admin_lang.select_discount_type')</option>
                                                <option value="P" @if(@$coupon->discount_type == 'P') {{ "Selected" }} @endif>@lang('admin_lang.percentage')</option>
                                                <option value="F" @if(@$coupon->discount_type == 'F') {{ "Selected" }} @endif>@lang('admin_lang.flat')</option>
                                            </select>
                                            <span class="discount_type-error" style="color: red;"></span>
                                        </div>
                                        
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="applied_on" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.applied_on')</label>
                                            <select name="applied_on" id="applied_on" class="form-control required">
                                                <option value="">@lang('admin_lang.select_applied_on')</option>
                                                <option value="SUB" @if(@$coupon->applied_on == 'SUB') {{ "Selected" }} @endif>@lang('admin_lang.subtotal')</option>
                                                <option value="SHI" @if(@$coupon->applied_on == 'SHI') {{ "Selected" }} @endif>@lang('admin_lang.shipping')</option>
                                            </select>
                                            <span class="applied_on-error" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" id="showCouponBearer" @if(@$coupon->applied_on == 'SHI') style="display:none" @endif>
                                            <label for="coupon_bearer" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.coupon_bearer')</label>
                                            <select name="coupon_bearer" type="text" class="form-control required">
                                                <option value="">@lang('admin_lang.select_coupon_bearer')</option>
                                                <option value="A" @if(@$coupon->coupon_bearer == 'A') {{ "Selected" }} @endif>@lang('admin_lang.admin')</option>
                                                <option value="M" @if(@$coupon->coupon_bearer == 'M') {{ "Selected" }} @endif>@lang('admin_lang.mer')</option>
                                            </select>
                                            <span class="coupon_bearer-error" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" id="show_applied_for" @if(@$coupon->applied_on == 'SHI') style="display:none" @endif>
                                            <label for="applied_for" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.applied_for')</label>
                                            <select name="applied_for" class="form-control required" id="applied_for">
                                                <option value="">@lang('admin_lang.select_applied_for')</option>
                                                <option value="A" @if(@$coupon->applied_for == 'A') {{ "Selected" }} @endif>@lang('admin_lang.all')</option>
                                                <option value="M" @if(@$coupon->applied_for == 'M') {{ "Selected" }} @endif>@lang('admin_lang.merchant_specific')</option>
                                            </select>
                                            <span class="applied_for-error" style="color: red;"></span>
                                        </div>
                                        @php
                                            if(@$coupon->getMerchants){
                                                $allMerchant = array();
                                                foreach ($coupon->getMerchants as $v1) {
                                                    array_push($allMerchant,$v1->merchant_id);
                                                }
                                            }
                                            
                                        @endphp
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" id="show_merchant" @if(@$coupon->applied_for == 'A') style="display:none" @elseif(@$coupon->applied_for == NULL ) style="display:none" @endif>
                                            <label for="merchants" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.merchant')</label>
                                            @if(@$merchants)
                                            <select name="merchants[]" id="merchants"class="form-control required" multiple data-placeholder="@lang('admin_lang.select_merchant')">
                                                
                                                @foreach($merchants as $mer)
                                                    @php
                                                        $selected = "";
                                                        if(in_array($mer->id,$allMerchant)){
                                                            $selected = "selected";
                                                        }
                                                    @endphp
                                                    <option value="{{ @$mer->id }}" {{ $selected }}> {{ @$mer->fname }} {{ @$mer->lname }}</option>
                                                @endforeach
                                            </select>
                                            @endif
                                            <span class="merchants-error" style="color: red;"></span>
                                        </div>
                                        
                                        <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <a href="javascript:void(0)" id="saveCupon" class="btn btn-primary ">@lang('admin_lang.save')</a>
                                        </div>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/jquery.datetimepicker.full.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#saveCupon").click(function(){
            var coupon_code = $('#code').val();
            var reqData = {
                'jsonrpc' : '2.0',
                '_token'  : "{{ csrf_token() }}",
                'data'    :{
                    'coupon_code':coupon_code,
                    'id': $('#coupon_id').val()
                }
            }
            $.ajax({
                'url':"{{ route('admin.check.cupon.code') }}",
                'type':"POST",
                'data':reqData,
                success:function(res){
                    if(res.status == 'false'){
                        
                        var error = '<label for="code" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">Coupon code already added</label>';
                        $('.code-error').html(error);
                    }else{
                        $("#addCoupon").submit();
                    }
                }
            })
            
        });
        $('#addCoupon').validate({
            rules:{
                "code":{
                    required:true
                },
                "coupon_bearer":{
                    required:true
                },
                "start_date":{
                    required:true
                },
                "end_date":{
                    required:true
                },
                "no_of_times":{
                    required:true
                },
                "discount":{
                    required:true
                },
                "discount_type":{
                    required:true
                },
                "applied_on":{
                    required:true
                },
                "applied_for":{
                    required:true
                },
            },
            messages:{
                code:{
                    required:'@lang('validation.required')'
                },
                coupon_bearer:{
                    required:'@lang('validation.required')'
                },
                start_date:{
                    required:'@lang('validation.required')'
                },
                end_date:{
                    required:'@lang('validation.required')'
                },
                no_of_times:{
                    required:'@lang('validation.required')'
                },
                discount:{
                    required:'@lang('validation.required')'
                },
                discount_type:{
                    required:'@lang('validation.required')'
                },
                applied_on:{
                    required:'@lang('validation.required')'
                },
                applied_for:{
                    required:'@lang('validation.required')'
                },

            },
            errorPlacement: function (error, element) 
            {
                if (element.attr("name") == "code") {
                    var error = '<label for="code" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.code-error').html(error);
                }
                if (element.attr("name") == "start_date") {
                    var error = '<label for="start_date" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.start_date-error').html(error);
                }
                if (element.attr("name") == "end_date") {
                    var error = '<label for="end_date" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.end_date-error').html(error);
                }
                if (element.attr("name") == "no_of_times") {
                    var error = '<label for="no_of_times" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.no_of_times-error').html(error);
                }
                if (element.attr("name") == "discount") {
                    var error = '<label for="discount" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.discount-error').html(error);
                }
                if (element.attr("name") == "discount_type") {
                    var error = '<label for="discount_type" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.discount_type-error').html(error);
                }
                if (element.attr("name") == "applied_on") {
                    var error = '<label for="applied_on" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.applied_on-error').html(error);
                }
                if (element.attr("name") == "applied_for") {
                    var error = '<label for="applied_for" generated="true" class="error" style="color:#f85d2c;border:none !important;background:none !important;">'+'@lang('validation.required')'+'</label>';
                    $('.applied_for-error').html(error);
                }
                
            }
        });
        $('#merchants').chosen();
        //$('#start_date,#end_date').datetimepicker();
        $('#applied_for').on('change', function(){
            var value = $(this).val();
            console.log(value);
            if(value == 'M'){
                $('#show_merchant').show();
            }else{
                $('#show_merchant').hide();
            }
        })
        $('#applied_on').on('change', function(){
            var value = $(this).val();
            if(value == 'SHI'){
                $('#show_applied_for').hide();
                $('#showCouponBearer').hide();
            }else{
                $('#show_applied_for').show();
                $('#showCouponBearer').show();
            }
        })
    });
</script>
<script>
    jQuery(document).ready(function () {
        'use strict';

        jQuery('#start_date,#end_date').datetimepicker();
    });
</script>
@endsection