@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage Merchant') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.manage_coupon')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style type="text/css">
    
    #merchants_filter{
        display: none;
    }

    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
</style>
<style type="text/css">
    .action_col{
        display: inline-block;
        width: 310px;
    }
    .action_col a{
        margin: 2px;
        float: left;
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
{{-- content --}}
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">@lang('admin_lang.manage_sub_admin')</h2>
                <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                @lang('admin_lang.manage_coupon')
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="mail_success" style="display: none;">
        <div class="alert alert-success vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
        </div>
    </div>
    <div class="mail_error" style="display: none;">
        <div class="alert alert-danger vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong>Error!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
        </div>
    </div>
    @if (session()->has('success'))
    <div class="alert alert-success vd_hidden" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
    </div>
    @elseif ((session()->has('error')))
    <div class="alert alert-danger vd_hidden" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
    </div>
    @endif
    <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong class="success_msg">Success!</strong> 
    </div>
    <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong class="error_msg"></strong> 
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">
                    @lang('admin_lang.manage_coupon')
                    <a class="adbtn btn btn-primary" href="{{ route('admin.add.coupon') }}"><i class="fas fa-plus"></i> @lang('admin_lang.add')</a>
                </h5>
                <div class="card-body">
                    <form action="{{ route('admin.list.subadmin') }}" id="searchCoupon" method="post">
                        @csrf
                        <div class="row">
                            <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <label for="keyword_lb" class="col-form-label">@lang('admin_lang.keyword')</label>
                                <input id="col1_filter" name="keyword" type="text" class="form-control keyword" placeholder="@lang('admin_lang.search_coupon')" value="{{ @$key['keyword'] }}">
                            </div>
                            <div data-column="2" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <label for="applied_for" class="col-form-label">@lang('admin_lang.applied_for')</label>
                                <select name="applied_for" class="form-control applied_for" id="col2_filter">
                                    <option value="">@lang('admin_lang.select_applied_for')</option>
                                    <option value="A">@lang('admin_lang.all')</option>
                                    <option value="M">@lang('admin_lang.merchant_specific')</option>
                                </select>
                            </div>
                            <div data-column="3" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <label for="applied_on" class="col-form-label">@lang('admin_lang.applied_on')</label>
                                <select name="applied_on" class="form-control applied_on" id="col3_filter">
                                    <option value="">@lang('admin_lang.select_applied_on')</option>
                                    <option value="SUB">@lang('admin_lang.subtotal')</option>
                                    <option value="SHI">@lang('admin_lang.shipping')</option>
                                </select>
                            </div>
                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                {{-- <label for="inputPassword" class="col-form-label"></label> --}}
                                <a id="search" href="javascript:void(0)" class="btn btn-primary fstbtncls">@lang('admin_lang.search')</a>

                                <input type="reset" value="Reset Search" class="btn btn-default fstbtncls reset_search">
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive listBody">
                        <table class="table table-striped table-bordered first" id="coupons">
                            <thead>
                                <tr>
                                    <th>@lang('admin_lang.coupon')</th>
                                    <th>@lang('admin_lang.start_date')</th>
                                    <th>@lang('admin_lang.end_date')</th>
                                    <th>@lang('admin_lang.no_of_times')</th>
                                    <th>Times of Used</th>
                                    <th>@lang('admin_lang.discount')</th>
                                    <th>@lang('admin_lang.discount_type')</th>
                                    <th>@lang('admin_lang.applied_on')</th>
                                    <th>@lang('admin_lang.applied_for')</th>
                                    <th>@lang('admin_lang.coupon_bearer')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>@lang('admin_lang.coupon')</th>
                                    <th>@lang('admin_lang.start_date')</th>
                                    <th>@lang('admin_lang.end_date')</th>
                                    <th>@lang('admin_lang.no_of_times')</th>
                                    <th>Times of Used</th>
                                    <th>@lang('admin_lang.discount')</th>
                                    <th>@lang('admin_lang.discount_type')</th>
                                    <th>@lang('admin_lang.applied_on')</th>
                                    <th>@lang('admin_lang.applied_for')</th>
                                    <th>@lang('admin_lang.coupon_bearer')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table  -->
        <!-- ============================================================== -->
    </div>
</div>
<!-- footer -->
<!-- ============================================================== -->
@include('admin.includes.footer')
<!-- ============================================================== -->
<!-- end footer -->
<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        // datatable
        $('body').on('click', '.reset_search', function() {
            $('#coupons').DataTable().search('').columns().search('').draw();
        })

        function filterColumn ( i ) {
            console.log(i, $('#col'+i+'_filter').val())
            $('#coupons').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }
        $('#coupons').DataTable( {
            stateSave: true,
            "order": [[1, "asc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
                $('#col2_filter').val(data.search.applied_for)
                $('#col3_filter').val(data.search.applied_on)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
                data.search.applied_for = $('#col2_filter').val()
                data.search.applied_on = $('#col3_filter').val()
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('admin.manage.coupon.post') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
                { 
                    data: "code",
                },
                {   data: 'start_date' },
                {   data: 'end_date' },
                {   data: 'no_of_times' },
                {   data: 'used_times' },
                {   data: 'discount' },
                {   
                    data: 'discount_type',
                    render: function(data, type, full) {
                        if(data == 'P'){
                            return "Percentage";
                        }else{
                            return "Flat";
                        }
                    } 
                },
                {   
                    data: 'applied_on',
                    render: function(data, type, full) {
                        if(data == 'SUB'){
                            return "Subtotal";
                        }else{
                            return "Shipping";
                        }
                    }
                },
                {  
                    data: 'applied_for',
                    render: function(data, type, full) {
                        if(full.applied_for == "M"){
                            var html = '';
                            full.get_merchants.forEach(function(item, index) {
                                //console.log(index)
                                if(index != 0) {
                                    html += ', '
                                }
                                if(item.merchant_info != null){
                                    html += item.merchant_info.fname +" "+ item.merchant_info.lname;
                                }else{
                                    html += '';
                                }
                                
                            })
                            return html;
                        }else{
                            return 'All';
                        }
                        
                    }

                },
                {   
                    data: 'coupon_bearer',
                    render: function(data, type, full) {
                        if(data == 'A'){
                            return "@lang('admin_lang.admin')";
                        }else if(data == 'M'){
                            return "@lang('admin_lang.mer')";
                        }else{
                            return "--";
                        }
                    }
                },
                {  
                    data: "id" ,
                    render: function(data, type, full) {
                        var a = '';
                        a += ' <a href="{{ url('admin/edit-coupon') }}/'+full.id+'"><i class="fas fa-edit" title="@lang('admin_lang.Edit')"></i></a>';
                        a += ' <a class="delete_coupon" href="javascript:void(0)" data-id="'+full.id+'"><i class=" fas fa-trash" title="@lang('admin_lang.Delete')"></i></a>';

                        return a;
                    }
                },
            ],
            // "createdRow": function( row, data, dataIndex ) {
            //     console.log( row, data, dataIndex)
            //     // add your condition here...
            //     $(row).addClass( 'important' );
            // }
        });

        // change event
        $('input.keyword').on( 'keyup click blur', function () {
            filterColumn( 1 );
        });
        $('.applied_for').on( 'change', function () {
            filterColumn( 2 );
        });
        $('.applied_on').on( 'change', function () {
            filterColumn( 3 );
        });

        $(document).on("click",".delete_coupon",function(e){
            var id = $(e.currentTarget).attr("data-id");
            var $this = $(this);
            if(confirm("Do you want to delete this coupon ?")){

                var reqData = {
                    "jsonrpc":"2.0",
                    "_token":"{{ csrf_token() }}",
                    "data":{
                        id:id
                    }
                };
                $.ajax({
                    url:"{{ route('admin.delete.coupon') }}",
                    type:"post",
                    data:reqData,
                    success:function(resp){
                        console.log(resp)
                        if(resp == 1){
                            $(".success_msg").html('@lang('admin_lang.delete_coupon_success')');
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".tr_"+id).hide();
                            $this.parent().parent().remove();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('@lang('admin_lang.delete_coupon_failed')');
                        }
                        $(".session_success_div").hide();
                        $(".session_error_div").hide();
                    }
                });
            }
            // }
        });
    });
    // });
        
</script>
@endsection