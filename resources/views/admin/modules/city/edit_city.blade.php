@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Edit City') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.edit_city')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.edit_city')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                @lang('admin_lang.edit_city')
                                <a class="adbtn btn btn-primary" href="{{ route('admin.list.city') }}">
                                    <i class="fas fa-less-than"></i>
                                    @lang('admin_lang.back')
                                </a>
                            </h5>
                            <div class="card-body">
                                <form id="addCityForm" method="post" action="{{ route('admin.update.city', @$city->id) }}">
                                    @csrf
                                    <div class="row">
                                        @if(@$language)
                                            @foreach($language as $key=>$lang)
                                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                    <label for="name" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.name_of_the_city') [{{ $lang->name }}]</label>
                                                    <input id="name{{ $language[$key]->id }}" name="name[]" type="text" class="form-control required" placeholder="@lang('admin_lang.name_of_the_city') in {{ $lang->name }}" value="{{ @$city->cityNamesDetails[$key]->name }}">
                                                    <span class="error_name_{{ $language[$key]->id }}" style="color: red;"></span>
                                                </div>
                                            @endforeach
                                        @endif
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="delivery_fees" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.internal_delivery_fees')</label>
                                            <input id="delivery_fees" name="delivery_fees" type="text" class="form-control required" placeholder='@lang('admin_lang.delivery_fees')' value="{{ @$city->delivery_fees }}">
                                            <span class="error_delivery_fees" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="delivery_fees" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.external_delivery_fees')</label>
                                            <input id="external_delivery_fees" name="external_delivery_fees" type="text" class="form-control required" placeholder='@lang('admin_lang.external_delivery_fees')' value="{{ @$city->external_delivery_fee }}">
                                            <span class="error_external_delivery_fees" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                        <a href="javascript:void(0)" id="addCity" class="btn btn-primary ">@lang('admin_lang.save')</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- footer -->   
    @include('admin.includes.footer')
    <!-- end footer -->
</div>

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#addCity").click(function(){
            var country_name = $.trim($("#name1").val()); 
            // var country_names = $.trim($("#name2").val());
            var delivery_fees = $.trim($("#delivery_fees").val());
            var external_delivery_fees = $.trim($("#external_delivery_fees").val()); 
            if(country_name != "" && delivery_fees != "" && external_delivery_fees != ""){
                $("#addCityForm").submit();
            }else{
                if(country_name == "" && delivery_fees == "" && external_delivery_fees == ""){
                    $(".error_name_1").text("@lang('validation.required')");
                    $(".error_name_2").text("@lang('validation.required')");
                    $(".error_delivery_fees").text("@lang('validation.required')");
                    $(".error_external_delivery_fees").text("@lang('validation.required')");
                }else if(delivery_fees == ""){
                    $(".error_delivery_fees").text("@lang('validation.required')");
                    $(".error_delivery_fees").text("");
                }else if(external_delivery_fees == ""){
                    $(".error_external_delivery_fees").text("@lang('validation.required')");
                    $(".error_external_delivery_fees").text("");
                }else if(country_name == ""){
                    $(".error_name_1").text("@lang('validation.required')");
                    $(".error_name_2").text("");
                }else{
                    $(".error_name_1").text("");
                    $(".error_name_2").text("@lang('validation.required')");
                }
            }
            // $("#addCityForm").submit();
        });
        // $("#addCityForm").validate({
        //     rules:{
        //         'name':{
        //             required:true
        //         }
        //     },
        //     messages: { 
        //         name: { 
        //             required: '@lang('validation.required')'
        //         }
        //     },
        //     errorPlacement: function (error, element) 
        //     {
        //         if (element.attr("name") == "name") {
        //             var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
        //             $('.error_name').html(error);
        //         }
        //     }
        // });
    });
</script>
@endsection