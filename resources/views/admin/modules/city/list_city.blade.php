@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }}| Admin | Manage City
@endsection
@section('content')
@section('links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@include('admin.includes.links')

@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">Manage City</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link"> Manage City </a></li>
                                    
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong class="success_msg"></strong> 
            </div>
            <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong class="error_msg"></strong> 
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <!-- ============================================================== -->
                <!-- basic table  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                                <div class="row">
                                    <div data-column="2" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="keyword_lb" class="col-form-label">Select State</label>
                                        <select class="form-control state" id="col2_filter">
                                            <option value="">Select State</option>
                                            @if(@$state->isNotEmpty())
                                            @foreach(@$state as $st)
                                            <option value="{{ @$st->id }}">{{ @$st->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="keyword_lb" class="col-form-label">Keyword</label>
                                        <input id="col1_filter" name="keyword" type="text" class="form-control keyword" placeholder="Search with city name" value="">
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <a id="search" href="javascript:void(0)" class="btn btn-primary fstbtncls">Search</a>
                                        <input type="reset" value="Reset Search" class="btn btn-default fstbtncls reset_search">
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive listBody">
                                <table class="table table-striped table-bordered first" id="myTable2">
                                    <thead>
                                        <tr>
                                            <th>Name of the city</th>
                                            <th>State Name</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Name of the city</th>
                                            <th>State Name</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end basic table  -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- footer -->   
        @include('admin.includes.footer')
        @endsection
        @section('scripts')
        @include('admin.includes.scripts')
        <script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                // datatable
               
                function filterColumn ( i ) {
                    console.log(i, $('#col'+i+'_filter').val())
                    $('#myTable2').DataTable().column( i ).search(
                        $('#col'+i+'_filter').val(),
                    ).draw();
                }
                var styles = `
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
`
var styleSheet = document.createElement("style");
styleSheet.innerText = styles;
document.head.appendChild(styleSheet);
                var table = $('#myTable2').DataTable( {
                    stateSave: false,
                    sDom: 'lrtip',
                    "order": [0, "asc"],
                    "stateLoadParams": function (settings, data) {
                    },
                    stateSaveParams: function (settings, data) {
                    },
                    "processing": true,
                    "serverSide": true,
                    'serverMethod': 'post',
                    "ajax": {
                        "url": "{{ route('admin.list.city') }}",
                        "data": function ( d ) {
                            d._token = "{{ @csrf_token() }}";
                        }
                    },
                    "createdRow": function( row, data, dataIndex ) { 
                        var id = (JSON.stringify(data.id));
                        var r = JSON.stringify(dataIndex);
                        $(row).addClass("tr_class_"+id);
                        $(row).addClass("tr_row_class");
                    },
                    columnDefs: [ { targets: 0, type: 'natural' } ],
                    'columns': [
                        { 
                            render: function(data, type, full) {
                                return full.name;
                            }
                        },
                        {
                            render: function(data, type, full) {
                                return full.state.name;
                            }
                        },
                        
                        {   
                            orderable: false,
                            data: 'status',
                            render: function(data, type, full) {
                                if(full.status == 'A'){
                                    return "Active";
                                }else if(full.status == 'I'){
                                    return "Inactive";
                                }
                            } 
                        },
                        {  
                            orderable: false,
                            data: "id" ,
                            render: function(data, type, full) {
                                var a = '';
                                if(full.status == 'A'){
                                    a += '<a class="show_hide_city" href="javascript:void(0)" data-id="'+full.id+'" data-status="'+full.status+'" title="@lang('admin_lang.hide_city')"><i class="icon_change_'+full.id+' fas fa-ban"></i></a>';
                                }else if(full.status == 'I'){
                                    a += '<a class="show_hide_city" href="javascript:void(0)" data-id="'+full.id+'" data-status="'+full.status+'" title="@lang('admin_lang.hide_city')"><i class="icon_change_'+full.id+' fas fa-check"></i></a>';
                                }
                                return a;
                            }
                        },
                    ],
                });

                // change event
                $('input.keyword').on( 'keyup click blur', function () {
                    filterColumn( 1 );
                });
                $('select.state').on( 'change', function () {
                    filterColumn( 2 );
                });
                 $('body').on('click', '.reset_search', function() {
                    $('#myTable2').DataTable().search('').columns().search('').draw();
                    $(".slct").val("").trigger("chosen:updated");
                })

                
                $(document).on("click",".show_hide_city",function(e){
                    var id = $(e.currentTarget).attr("data-id"),
                    status = $(e.currentTarget).attr("data-status"),
                    icon_status = $(e.currentTarget).attr("data-status"),
                    obj = $(e.currentTarget);
                    sh = "";

                    var reqData = {
                        'jsonrpc' : '2.0',                
                        '_token'  : '{{csrf_token()}}',
                        'data'    : {
                            'id'    : id
                        }
                    },
                    temp = table.row('.tr_class_'+id).data();
                    // alert(JSON.stringify(temp.status));
                    if(status == 'A'){
                        status = "Inactive";
                        sh = "hide";
                    }else if(status == 'I'){
                        status = "Active";
                         sh = "show";
                    }
                    var m = "Are you sure ?";
                    if(temp.status == 'I'){
                        m = "Do you want to activate "+temp.name+" ?";
                    }else if(temp.status == 'A'){
                        m = "Do you want to inactivate "+temp.name+" ?";
                    }
                    if(confirm(m)){
                        $.ajax({
                            url:"{{ route('admin.change.status.of.city') }}",
                            type:"Post",
                            data:reqData,
                            success:function(resp){
                                if(resp == 1){
                                    $(".success_msg").html(resp.meaning);
                                    $(".success_msg_div").show();
                                    $(".error_msg_div").hide();
                                    $(".staus_"+id).text(status);
                                    if(icon_status == 'A'){
                                        
                                        obj.attr("data-status",'I');
                                        $(".icon_change_"+id).removeClass("fa-ban");
                                        $(".icon_change_"+id).addClass("fa-check");
                                    }else if(icon_status == 'I'){
                                        obj.attr("data-status",'A');
                                        $(".icon_change_"+id).removeClass("fa-check");
                                        $(".icon_change_"+id).addClass("fa-ban");
                                    }
                                    
                                }else{
                                    $(".error_msg_div").show();
                                    $(".success_msg_div").hide();
                                    $(".error_msg").html(resp.meaning);
                                }
                                $(".session_success_div").hide();
                                $(".session_error_div").hide();
                                table.row('.tr_class_'+id).draw( false );
                            }
                        });
                    }
                });
                $('#searchCoupon').on('keyup keypress', function(e) {
                  var keyCode = e.keyCode || e.which;
                  if (keyCode === 13) { 
                    e.preventDefault();
                    return false;
                  }
                }); 
            });
        </script>
        @endsection