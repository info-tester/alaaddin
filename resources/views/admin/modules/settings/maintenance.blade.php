@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Shipping Settings') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | Configuration
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<style type="text/css">
    label.error{
        color: red;
        border: none !important;
        background: none !important;
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.shipping_setting')</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Configuration</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">Configuration
                                <!-- <a class="adbtn btn btn-primary" href="{{ route('admin.list.shipping.cost') }}">
                                    <i class="fas fa-less-than"></i>
                                    @lang('admin_lang.back')
                                </a> -->
                            </h5>
                            <div class="card-body">
                                <form id="addShippingSettingForm" method="POST" action="{{ route('admin.manage.maintenance') }}">
                                    @csrf
                                    <div class="row">
                                        {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" >
                                            <h5><span style="color: red;">*</span>@lang('admin_lang.maintenence')</h5>
                                            <label id="maintenence_lb" class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" name="maintenence" @if(@$setting->maintenence == 'Y') checked="" @endif class="custom-control-input" value="Y"><span class="custom-control-label">@lang('admin_lang.yes')</span>
                                            </label>
                                            <label id="maintenence_lb2" class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" name="maintenence" @if(@$setting->maintenence == 'N') checked="" @endif class="custom-control-input" value="N"><span class="custom-control-label">@lang('admin_lang.no')</span>
                                            </label>
                                            <span class="error_international_order" style="color: red;"></span>
                                        </div> --}}

                                        {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" style="display: block;">
                                            <label for="maintenence_text_lb" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.maintenence_text')</label>
                                            <input id="maintenence_text" name="maintenence_text" type="text" class="form-control required" placeholder="@lang('admin_lang.maintenence_text')" value="{{ @$setting->maintenence_text }}">
                                            <span class="error_internal_order_cost" style="color: red;"></span>
                                        </div> --}}
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" style="display: block;">
                                            <label for="maintenence_text_lb" class="col-form-label">Insurance percentage (%)<span style="color: red;">*</span></label>
                                            <input id="insurance" name="insurance" type="number" class="form-control required" placeholder="Insurance percentage (%)" value="{{ @$setting->insurance }}" min="0" max="100">
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" style="display: block;">
                                            <label for="maintenence_text_lb" class="col-form-label">Admin Commission (%)<span style="color: red;">*</span></label>
                                            <input id="commission" name="commission" type="number" class="form-control required" placeholder="Admin Commission (%)" value="{{ @$setting->commission }}" min="0" max="100">
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" style="display: block;">
                                            <label for="maintenence_text_lb" class="col-form-label">Loading unloading price (Rs.) /Ton                                                
                                                {{-- <select id="select_unit" class="select_unit" class="form-control"> --}}
                                                {{-- <option value="">Select</option> --}}
                                                {{-- @if(@$unitmasters) --}}
                                                {{-- @foreach(@$unitmasters as $u) --}}
                                                {{-- <option value="{{ $u->id }}" @if(@$u->unit_name == 'Ton') selected="" @endif>{{ @$u->unit_name }}</option> --}}
                                                {{-- <option value="">Ton</option> --}}
                                                {{-- @endforeach --}}
                                                {{-- @endif --}}
                                                {{-- </select>  --}}
                                            <span style="color: red;">*</span></label>
                                            <input id="loading_unloading_price" name="loading_unloading_price" type="number" class="form-control required" placeholder="Loading unloading price per kg" value="{{ (@$setting->loading_unloading_price) }}">
                                        </div>
                                        
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" style="display: block;">
                                            <label for="maintenence_text_lb" class="col-form-label">Seller Commission (%)<span style="color: red;">*</span></label>
                                            <input id="seller_commission" name="seller_commission" type="number" class="form-control required" placeholder="Seller Commission (%)" value="{{ @$setting->seller_commission }}" min="0" max="100">
                                        </div>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <a href="javascript:void(0)" id="addShippingSetting" class="btn btn-primary">@lang('admin_lang.save')</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('admin.includes.footer')
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper  -->
<!-- ============================================================== -->
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        // $(document).on('blur',"#internal_order_cost",function(e){
        //     var cost = $.trim($("#internal_order_cost").val());
        //     if(cost != ""){
        //         cost = parseFloat(cost);
        //         cost = cost.toFixed(3);
        //         if(isNaN(cost)){
        //             $(".error_internal_order_cost").text('Please provide external order rate with 3 decimal places![Ex: 5.894]');
        //             $("#internal_order_cost").val("");
        //         }else{
        //             $("#internal_order_cost").val(cost);
        //         }
        //     }
        // });

        // $(document).on('keyup',"#internal_order_cost",function(e){
        //   var name = $(e.currentTarget).val();
        //     if(e.key != '.'){
        //       if(isNaN(e.key)){
        //         $(e.currentTarget).val("");
        //         $(".error_internal_order_cost").text('Please provide external order rate with 3 decimal places![Ex: 5.894]');
        //       }else{
        //         $(".error_internal_order_cost").text("");
        //       }
        //     }else{
        //         $(".error_internal_order_cost").text("");
        //     }
        // });
        $("#addShippingSetting").click(function(){
            var text = $.trim($("#maintenence_text").text());
            // if(text != ""){
            //     $(".error_internal_order_cost").text("@lang('admin_lang.please_pr_maintence_text')");
            // }else{
                $("#addShippingSettingForm").submit();    
            // }
        });

        $("#addShippingSettingForm").validate({
        rules:{
            'insurance':{
                required:true,
                max:99
            },
            'commission':{
                required:true,
                max:99
            },
            'loading_unloading_price':{
                required:true
            },
            'seller_commission':{
                required:true,
                max:99
            }
        },
        messages: { 
            insurance: { 
                required: '@lang('validation.required')'
            },
            commission: { 
                required: '@lang('validation.required')'
            },
            loading_unloading_price: { 
                required: '@lang('validation.required')'
            },
            seller_commission: { 
                required: '@lang('validation.required')'
            },
        },
        // errorPlacement: function (error, element) 
        // {
        //         if (element.attr("name") == "maintenence_text") {
        //             var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
        //             $('.error_internal_order_cost').html(error);
        //         }
        //         if (element.attr("name") == "maintenence") {
        //             var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
        //             $('.error_international_order').html(error);
        //         }
        //     }
        });
    });
</script>
@endsection