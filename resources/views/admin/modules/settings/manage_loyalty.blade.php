@extends('admin.layouts.app')
@section('title', 'Alaaddin | Admin | Manage Loyalty Point')
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
{{-- content --}}
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">@lang('admin_lang.loyalty_point')</h2>
                    <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);" class="breadcrumb-link">@lang('admin_lang.setting')</a></li>
                                <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.manage_loyalty_point')</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session()->has('success'))
        <div class="alert alert-success vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
        </div>
        @elseif ((session()->has('error')))
        <div class="alert alert-danger vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
        </div>
        @endif
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- ============================================================== -->
            <!-- basic table  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">@lang('admin_lang.loyalty_point')</h5>
                    <div class="card-body">

                        <form id="form" method="post" action="{{ route('admin.store.loyalty', $setting->id) }}">
                            @csrf
                            <div class="row">

                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="keyword" class="col-form-label">@lang('admin_lang.one_kwd_to_points') (@lang('admin_lang.in_decimal'))</label>
                                    <input id="one_kwd_to_point" name="one_kwd_to_point" type="text" class="form-control" placeholder='@lang('admin_lang.one_kwd_to_points')' value="{{ @$setting->one_kwd_to_point }}">
                                </div>
                                <div class="form-group col-xl-2 col-lg-2 col-md-6 col-sm-12 col-12">
                                    <label for="keyword" class="col-form-label">&nbsp;</label><span class="loyal">@lang('admin_lang.loyalty_point')</span>
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="keyword" class="col-form-label">@lang('admin_lang.one_point_in_kwd') (@lang('admin_lang.in_kwd')) </label>
                                    <input id="one_point_to_kwd" name="one_point_to_kwd" type="text" class="form-control" placeholder='@lang('admin_lang.one_point_in_kwd')' value="{{ @$setting->one_point_to_kwd }}">
                                </div>
                                <div class="form-group col-xl-2 col-lg-2 col-md-6 col-sm-12 col-12">
                                    <label for="keyword" class="col-form-label">&nbsp;</label><span class="loyal">@lang('admin_lang.kwd')</span>
                                </div>
                                <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <label for="keyword" class="col-form-label">@lang('admin_lang.minimum_points') </label>
                                    <input id="min_point" name="min_point" type="text" class="form-control" placeholder='@lang('admin_lang.minimum_points')' value="{{ @$setting->min_point }}">
                                </div>
                                <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <label for="keyword" class="col-form-label">@lang('admin_lang.maximum_discount') (@lang('admin_lang.in_kwd')) </label>
                                    <input id="max_discount" name="max_discount" type="text" class="form-control" placeholder='@lang('admin_lang.maximum_discount')' value="{{ @$setting->max_discount }}">
                                </div>

                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                 <button class="btn btn-primary">@lang('admin_lang.save')</a>
                                 </div>

                             </div>
                         </form>
                     </div>
                 </div>
             </div>
             <!-- ============================================================== -->
             <!-- end basic table  -->
             <!-- ============================================================== -->
         </div>




     </div>


     <!-- footer -->   
     @include('admin.includes.footer')
     <!-- end footer -->
 </div>




</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#form" ).validate({   
            rules:{             
                one_kwd_to_point : {
                    required: true,
                    number:true
                },
                one_point_to_kwd : {
                    required: true,
                    number: true
                },
                min_point : {
                    required: true,
                    number: true
                },
                max_discount : {
                    required: true,
                    number: true
                }
            },
            errorPlacement: function (error , element) {
                //toastr:error(error.text());
            },
        });
    });
</script>
@endsection