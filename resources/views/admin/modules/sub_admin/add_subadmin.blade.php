@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Add SubAdmin') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.add_subadmin')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            <!-- @lang('admin_lang.e_commerce_dashboard_template') -->
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.add_sub_admin')
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                @lang('admin_lang.add_sub_admin')
                                <a class="adbtn btn btn-primary" href="{{ route('admin.list.subadmin') }}">
                                    <i class="fas fa-less-than"></i>
                                    @lang('admin_lang.back')
                                </a>
                            </h5>
                            <div class="card-body">
                                <form id="addSubadmin" method="post" enctype="multipart/form-data" action="{{ route('admin.add.subadmin') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="fname" class="col-form-label">
                                                <span style="color: red;">*</span>@lang('admin_lang.first_name')
                                            </label>
                                            <input id="fname" name="fname" type="text" class="form-control required" placeholder='@lang('admin_lang.first_name')'>
                                            <span class="errorFname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="lname" class="col-form-label">
                                                <span style="color: red;">*</span>@lang('admin_lang.last_name')
                                            </label>
                                            <input id="lname" name="lname" type="text" class="form-control required" placeholder='@lang('admin_lang.last_name')'>
                                            <span class="errorLname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="inputEmail" class="col-form-label">
                                                <span style="color: red;">*</span>@lang('admin_lang.email')
                                            </label>
                                            <input id="email" name="email" type="email" placeholder='@lang('admin_lang.email')' class="form-control">
                                            <span class="errorEmail" style="color: red;"></span>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="" class="col-form-label">
                                                <span style="color: red;">*</span>@lang('admin_lang.phone_number')
                                            </label>
                                            <input id="phone" name="phone" type="text" class="form-control" placeholder='@lang('admin_lang.phone_number')'>
                                            <span class="errorPhone" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="inputPassword" class="col-form-label">
                                                <span style="color: red;">*</span>@lang('admin_lang.password')
                                            </label>
                                            <input id="password" name="password" type="password" placeholder='@lang('admin_lang.password')' class="form-control required">
                                            {{-- <span>
                                                @lang('admin_lang.password_length_should_be_8')
                                            </span> --}}
                                            <span class="errorPassword" style="color: red;">@lang('admin_lang.password_length_should_be_8')</span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="inputPassword" class="col-form-label">
                                                <span style="color: red;">*</span>@lang('admin_lang.confirm_password')
                                            </label>
                                            <input id="confirm_password" name="confirm_password" type="password" placeholder='@lang('admin_lang.confirm_password')' class="form-control required">
                                            <span class="errorConfirmPassword" style="color: red;"></span>
                                        </div>
                                            <!-- <div class="custom-file mb-3">
                                                <input type="file" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">File Input</label>
                                            </div> -->

                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                                                <h4 class="fultxt">
                                                    <span style="color: red;">*</span>@lang('admin_lang.permission')
                                                </h4>
                                            </div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                @if(@$menus)
                                                @foreach(@$menus as $menu)
                                                <label id="{{ @$menu->id }}" class="custom-control custom-checkbox custom-control-inline">
                                                    <input id="{{ @$menu->id }}" type="checkbox"  name="permission[]" class="custom-control-input cksidebar required" value="{{ @$menu->menu_id }}">
                                                    <span class="custom-control-label">{{ @$menu->name }}</span>
                                                </label>
                                                @endforeach
                                                @endif
                                                <span class="errorPermission" style="color: red;"></span>
                                            </div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <!-- <p for="" class="col-form-label">Category Picture</p> -->
                                                <input type="file" class="custom-file-input inpt" id="customFile" name="profile_pic" accept="image/jpg,image/jpeg,image/png">
                                                <label class="custom-file-label extrlft" for="customFile">
                                                    @lang('admin_lang.upload_profile_picture')
                                                </label>
                                                @lang('admin_lang.recommended_size_100')
                                                <div class="errorpic" style="color: red;">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="display: block;" style="width: 110px;height: 110px;position: relative;">
                                                <span class="after-upload">
                                                    <div class="uploadServicePicDiv">
                                                        <img id="profilePicture" src="" alt="" style="width: 100px;height: 100px;">
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="inputPassword" class="col-form-label"></label>
                                                <a href="javascript:void(0)" id="saveSubAdminDetails" class="btn btn-primary fstbtncls">@lang('admin_lang.save')</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
        @endsection
        @section('scripts')
        {{-- @include('admin.includes.scripts') --}}
        @if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <script type="text/javascript">
            jQuery.validator.addMethod("checksidebar",function(value, element, params){
                var sidebar_checked = $(".cksidebar:checked").length;
                alert("addmethod");
                return this.optional(element) || (sidebar_checked > 0 ) 
                
            },
            '@lang('admin_lang.check_atleast_one_sidebar')'
            );
            $(document).ready(function(){
                $("#saveSubAdminDetails").click(function(){
                    $("#addSubadmin").submit();
                });
        // $(document).on("blur",'#password',function(e){
        //     $("#password").val("");
        // });
        $("#customFile").change(function() {
            var filename = $.trim($("#customFile").val()),
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".errorpic").text("");
                readURL(this);
            }else{

                $(".errorpic").text('@lang('validation.image_extension_type')');
                $('#profilePicture').attr('src', "");

            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                  $('#profilePicture').attr('src', e.target.result);
              }
              reader.readAsDataURL(input.files[0]);
          }
      }
      $(document).on("blur","#email",function(){
        var email = $.trim($("#email").val());
        if(email != "")
        {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(regex.test(email)){

                var rurl = '{{ route('admin.check.email') }}';
                $.ajax({
                    type:"GET",
                    url:rurl,
                    data:{
                        email:email
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".errorEmail").text('@lang('validation.email_id_already_exist')');
                            $("#email").val("");
                        }else{
                            $(".errorEmail").text("");
                        }
                    }
                }); 
            }else{
                    // alert("if email format false");
                    $(".errorEmail").text('@lang('validation.please_provide_valid_email_id')');
                }
                
            }else{
                // alert("if blank");
                $(".errorEmail").text('@lang('validation.please_provide_valid_email_id')');
                
            }
        });

      $(document).on("blur","#phone",function(){
        var phone = $.trim($("#phone").val());
        
        if(phone != "")
        {
            var rurl = '{{ route('admin.check.phone') }}';
            $.ajax({
                type:"GET",
                url:rurl,
                data:{
                    phone:phone
                },
                success:function(resp){
                    if(resp == 1){
                        
                        $(".errorPhone").text('@lang('admin_lang.phone_number_already_exist')');
                        $("#phone").val("");
                    }
                }
            });  
        }
    });
      $("#addSubadmin").validate({
        rules:{
            'fname':{
                required:true
            },
            'password': {
                minlength: 8
            },            
            'confirm_password':{
                equalTo: '#password'
            },
            'lname':{
                required:true
            },
            'phone':{
                required:true,
                digits:true
            },
            'email':{
                required:true,
                email:true
            },
            'permission':{
                checksidebar:true
            },
            'profile_pic':{
                accept:"image/jpg,image/jpeg,image/png"
            }
        },
        messages: { 
            fname: { 
                required: '@lang('validation.required')'
            },
            lname: { 
                required: '@lang('validation.required')'
            },
            phone: { 
                required: '@lang('validation.required')'
            },
            email: { 
                required: '@lang('validation.required')'
            },
            permission: { 
                checksidebar: '@lang('validation.required')'
            },
            profile_pic: { 
                required: "@lang('admin_lang.valid_img_chk')"
            },
            confirm_password:'@lang('validation.required')'
        },
        errorPlacement: function (error, element) 
        {
                // console.log(element.attr("name"));
                // console.log(error);
                if (element.attr("name") == "fname") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorFname').html(error);
                }
                if (element.attr("name") == "password") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorPassword').html(error);
                }
                if (element.attr("name") == "confirm_password") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.confirm_password')'+'</label>';
                    $('.errorConfirmPassword').html(error);
                }
                if (element.attr("name") == "lname") {
                    var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorLname').html(error);
                }
                if (element.attr("name") == "email") {
                    var error = '<label for="gn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorEmail').html(error);
                }
                if (element.attr("name") == "phone") {
                    var error = '<label for="phn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorPhone').html(error);
                }
                if (element.attr("name") == "profile_pic") {
                    var error = '<label for="pro" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorpic').html(error);
                }
                if (element.attr("name") == "permission[]"){
                    var error = '<label for="sb" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
                    $('.errorPermission').html(error);
                }
            }
        });

});

</script>
@endsection