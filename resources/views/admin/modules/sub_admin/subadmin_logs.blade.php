@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage SubAdmin') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.manage_sub_admin')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style type="text/css">
    #sub_admin_filter{
        display: none;
    }

    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

        <div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">@lang('admin_lang.manage_sub_admin')</h2>
                            
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                        <li class="breadcrumb-item"><a href="{{ route('admin.list.subadmin') }}" class="breadcrumb-link">@lang('admin_lang.manage_sub_admin')</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.subadmin_logs')</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (session()->has('success'))
                <div class="alert alert-success vd_hidden session_success_div" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                </div>
                @elseif ((session()->has('error')))
                <div class="alert alert-danger vd_hidden session_error_div" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
                </div>
                @endif
                <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong class="success_msg"></strong>
                </div>
                <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong class="error_msg"></strong>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <form id="form" method="post" action="{{ route('admin.show.logs') }}">
                                    @csrf
                                    <div class="row">
                                        <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="keyword" class="col-form-label">@lang('admin_lang.keyword') </label>
                                            <input id="col1_filter" name="keyword" type="text" class="form-control keyword" placeholder='@lang('admin_lang.keyword')' value="{{ @$key['keyword'] }}">
                                        </div>

                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                           <a href="javascript:void(0)" id="searchSubAdmin" class="btn btn-primary fstbtncls">@lang('admin_lang.search')</a>

                                           <input type="reset" value="@lang('admin_lang.reset_search')" class="btn btn-default fstbtncls reset_search" style="margin-left: 20px">
                                       </div>
                                       
                                   </div>
                               </form>
                               <div class="table-responsive listBody">
                                <table class="table table-striped table-bordered" id="sub_admin">
                                    <thead>
                                        <tr>
                                            <th>@lang('admin_lang.date')</th>
                                            <th>@lang('admin_lang.ip')</th>
                                            <th>@lang('admin_lang.device')</th>
                                            <th>@lang('admin_lang.login_location')</th>
                                            <th>@lang('admin_lang.Type')</th>
                                            <th>@lang('admin_lang.browser')</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                           <th>@lang('admin_lang.date')</th>
                                           <th>@lang('admin_lang.ip')</th>
                                           <th>@lang('admin_lang.device')</th>
                                           <th>@lang('admin_lang.login_location')</th>
                                           <th>@lang('admin_lang.Type')</th>
                                           <th>@lang('admin_lang.browser')</th>
                                           
                                       </tr>
                                   </tfoot>
                               </table>
                           </div>
                       </div>
                   </div>
               </div>
               <!-- ============================================================== -->
               <!-- end basic table  -->
               <!-- ============================================================== -->
           </div>
        </div>


        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
        <div class="loader" style="display: none;">
            <img src="{{url('public/loader.gif')}}">
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click', '.reset_search', function() {
            $('#sub_admin').DataTable().search('').columns().search('').draw();
        })
        var segment = "{{ request()->segment(3) }}";
        function filterColumn ( i ) {
            $('#sub_admin').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }
        
        $('#sub_admin').DataTable( {
            stateSave: true,
            "order": [[0, "desc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('admin.show.logs')}}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                    d.id = segment;
                }
            },
            'columns': [
                {   data: 'created_at' },
                {   data: 'ip'    },
                {   
                    data: 'device',
                    render: function(data, type, full) {
                        if(data == 'D'){
                            return '@lang('admin_lang.desktop')'
                        }else if(data == 'M'){
                            return '@lang('admin_lang.mobile')'
                        }else{
                            return '--';
                        }
                    }
                },
                {   data: 'login_location' },
                {   
                    data: 'type',
                    render: function(data, type, full) {
                        if(data == 'LI'){
                            return '@lang('admin_lang.log_in')'
                        }
                        if(data == 'LO'){
                            return '@lang('admin_lang.log_out')'
                        }
                    }
                },
                {   data: 'browser' },
                
            ]
        });


        // change event
        $('input.keyword').on( 'keyup click', function () {
            filterColumn( 1 );
        });

        

    });
</script>
@endsection