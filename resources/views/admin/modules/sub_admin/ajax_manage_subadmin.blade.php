<table class="table table-striped table-bordered first" id="myTable">
                            <thead>
                                <tr>
                                    <th>@lang('admin_lang.name')</th>
                                    <th>@lang('admin_lang.email')</th>
                                    <th>@lang('admin_lang.phone_number')</th>
                                    <th>@lang('admin_lang.permission')</th>
                                    <th>@lang('admin_lang.status')</th>
                                    <th>@lang('admin_lang.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(@$sub_admins)
                                @foreach(@$sub_admins as $subadmin)
                                <tr class="tr_{{ @$subadmin->id }}">
                                    <td>{{ @$subadmin->fname }}  {{ @$subadmin->lname }}</td>
                                    <td>{{ @$subadmin->email }}</td>
                                    <td>{{ @$subadmin->phone }}</td>
                                    <td>
                                        @if(@$subadmin->sidebarAccess)
                                        @foreach(@$subadmin->sidebarAccess as $key=>$sidebar)
                                        @if($key != 0)
                                        ,
                                        @endif
                                        {{ @$sidebar->menuDetails[0]->name }}
                                        @endforeach
                                        @endif
                                    </td>
                                    <td class="status_{{ @$subadmin->id }}">
                                        @if(@$subadmin->status == 'A')
                                        @lang('admin_lang.Active')
                                        @else
                                        @lang('admin_lang.Inactive')
                                        @endif
                                    </td>
                                    <td>
                                        {{-- edit --}}
                                        <a href="{{ route('admin.edit.subadmin',$subadmin->id) }}"><i class="fas fa-edit" title='@lang('admin_lang.Edit')'></i></a>

                                        {{-- status --}}
                                        @if(@$subadmin->status == 'A')
                                        <a class="status_change" href="javascript:void(0)" data-id="{{ @$subadmin->id }}"><i class="fas fa-ban icon_change_{{ @$subadmin->id }}" title='@lang('admin_lang.Inactive')'></i></a>    
                                        @else
                                        <a class="status_change" href="javascript:void(0)" data-id="{{ @$subadmin->id }}"><i class="far fa-check-circle icon_change_{{ @$subadmin->id }}" title='@lang('admin_lang.Active')'></i></a>
                                        @endif

                                        <a class="delete_subadmin" href="javascript:void(0)" data-id="{{ @$subadmin->id }}"><i class=" fas fa-trash" title='@lang('admin_lang.Delete')'></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                   <th>@lang('admin_lang.name')</th>
                                   <th>@lang('admin_lang.email')</th>
                                   <th>@lang('admin_lang.phone_number')</th>
                                   <th>@lang('admin_lang.permission')</th>
                                   <th>@lang('admin_lang.status')</th>
                                   <th>@lang('admin_lang.action')</th>
                               </tr>
                           </tfoot>
                       </table>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>