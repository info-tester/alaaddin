@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Edit SubAdmin') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.edit_subadmin')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title"> @lang('admin_lang.e_commerce_dashboard_template') </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.edit_sub_admin')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang.edit_sub_admin')
                                <a class="adbtn btn btn-primary" href="{{ route('admin.list.subadmin') }}">
                                    <i class="fas fa-less-than"></i>
                                    @lang('admin_lang.back')
                                </a>
                            </h5>
                            <div class="card-body">
                                <form id="editSubadmin" method="post" enctype="multipart/form-data" action="{{ route('admin.edit.subadmin',$sub_admin->id) }}">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="fname" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.first_name')</label>
                                            <input id="fname" name="fname" type="text" class="form-control required" placeholder='@lang('admin_lang.first_name')' value="{{ @$sub_admin->fname }}">
                                            <span class="errorFname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="lname" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.last_name')</label>
                                            <input id="lname" name="lname" type="text" class="form-control required" placeholder='@lang('admin_lang.last_name')' value="{{ @$sub_admin->lname }}">
                                            <span class="errorLname" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="inputEmail" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.email')</label>
                                            
                                            <input id="email" name="email" type="email" placeholder='@lang('admin_lang.email')' class="form-control" value="{{ @$sub_admin->email }}">
                                            <span class="errorEmail" style="color: red;"></span>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.phone_number')</label>

                                            <input id="phone" name="phone" type="text" class="form-control" placeholder='@lang('admin_lang.phone_number')' value="{{ @$sub_admin->phone }}">
                                            <span class="errorPhone" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="inputPassword" class="col-form-label">@lang('admin_lang.password')</label>
                                            <input id="password" name="password" type="password" placeholder='@lang('admin_lang.password')' class="form-control">
                                            {{-- <span></span> --}}
                                            <span class="errorPassword">@lang('admin_lang.password_length_should_be_8')</span>
                                            
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="inputPassword" class="col-form-label">@lang('admin_lang.confirm_password')</label>
                                            <input id="confirm_password" name="confirm_password" type="password" placeholder='@lang('admin_lang.confirm_password')' class="form-control">
                                            <span class="errorConfirmPassword" style="color: red;"></span>
                                        </div>
                                            <!-- <div class="custom-file mb-3">
                                                <input type="file" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">File Input</label>
                                            </div> -->

                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <h4 class="fultxt"><span style="color: red;">*</span>@lang('admin_lang.permisssion')</h4>
                                            </div>
                                            @php
                                            $menuAcc = [];
                                            foreach(@$sub_admin->sidebarAccess as $accMenu) {
                                                @$menuAcc[] = $accMenu->menu_id;
                                            }
                                            @endphp
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                @if(@$menus)
                                                @foreach(@$menus as $key=>$menu)
                                                <label id="{{ @$menu->id }}" class="custom-control custom-checkbox custom-control-inline">
                                                    <input id="{{ @$menu->id }}" type="checkbox"  name="permission[]" class="custom-control-input cksidebar required" value="{{ @$menu->menu_id }}" @if(in_array(@$menu->menu_id, @$menuAcc)) checked 
                                                    @endif
                                                    >
                                                    <span class="custom-control-label">{{ @$menu->name }}</span>
                                                </label>
                                                @endforeach
                                                @endif
                                                <span class="errorPermission" style="color: red;"></span>
                                            </div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <!-- <p for="" class="col-form-label">Category Picture</p> -->
                                                <input type="file" class="custom-file-input inpt" id="customFile" name="profile_pic" accept="image/jpg,image/jpeg,image/png">
                                                <label class="custom-file-label extrlft" for="customFile">
                                                    @lang('admin_lang.upload_profile_picture')
                                                </label>
                                                @lang('admin_lang.recommended_size_100')
                                                <div class="errorpic" style="color: red;">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="display: block;" style="width: 110px;height: 110px;position: relative;">
                                                <span class="after-upload">
                                                    <div class="uploadServicePicDiv">
                                                        @if(@$sub_admin->profile_pic)
                                                        @php
                                                        $image_path = 'storage/app/public/sub_admin_pics/'.@$sub_admin->profile_pic; 
                                                        @endphp
                                                        @if(file_exists(@$image_path))
                                                        <img id="profilePicture" src="{{ URL::to('storage/app/public/sub_admin_pics/'.@$sub_admin->profile_pic) }}" alt="" style="width: 100px;height: 100px;">
                                                        @endif
                                                        @endif
                                                        <img id="profilePicture" src="" alt="" style="width: 100px;height: 100px;">
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <input type="hidden" id="old_phone" value="{{ @$sub_admin->phone }}">
                                                <input type="hidden" id="old_email" value="{{ @$sub_admin->email }}">
                                                <label for="inputPassword" class="col-form-label"></label>
                                                <a href="javascript:void(0)" id="saveSubAdminDetails" class="btn btn-primary fstbtncls">@lang('admin_lang.Save')</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
        @endsection
        @section('scripts')
        {{-- @include('admin.includes.scripts') --}}
        @if(Config::get('app.locale') == 'en')
            @include('admin.includes.scripts')
        @elseif(Config::get('app.locale') == 'ar')
            @include('admin.includes.arabic_scripts')
        @endif
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <script type="text/javascript">
            jQuery.validator.addMethod("checksidebar",function(value, element, params){
                var sidebar_checked = $(".cksidebar:checked").length;
                alert("addmethod");
                return this.optional(element) || (sidebar_checked > 0 ) 
                
            },
            "Please check atleast one sidebar"
            );
            $(document).ready(function(){
                $("#customFile").change(function() {
                    var filename = $.trim($("#customFile").val()),
                    filename_arr = filename.split("."),
                    ext = filename_arr[1];
                    ext = ext.toLowerCase();
                    if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                        $(".errorpic").text("");
                        readURL(this);
                    }else{

                        $(".errorpic").text('@lang('validation.image_extension_type')');
                        $('#profilePicture').attr('src', "");

                    }
                });
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                          $('#profilePicture').attr('src', e.target.result);
                      }
                      reader.readAsDataURL(input.files[0]);
                  }
                }
                $(document).on("blur","#email",function(e){
                    var email = $.trim($(this).val()),
                    old_email = $.trim($("#old_email").val());
                    if(email != ""){
                        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                            if(regex.test(email)){
                                var rurl = '{{ route('admin.check.change.email') }}';
                                $.ajax({
                                    type:"GET",
                                    url:rurl,
                                    data:{
                                        old_email:old_email,
                                        email:email
                                    },
                                    success:function(resp){
                                        // alert("response: "+resp);
                                        if(resp == 1){
                                            
                                            $(".errorEmail").text("Email-id already exist!");
                                            $("#email").val("");

                                        }else{
                                            $(".errorEmail").text("");
                                        }
                                    }
                                });
                            }else{
                                $(".errorEmail").text("Please provide a valid email-id!");
                            }
                        }else{
                            $(".errorEmail").text("Please provide a valid email-id");
                        }
                });
                $(document).on("blur","#phone",function(e){
                    var phone = $.trim($(this).val()),
                    old_phone = $.trim($("#old_phone").val());
                    if(phone != ""){
                            var rurl = '{{ route('admin.check.change.phone') }}';
                            $.ajax({
                                type:"GET",
                                url:rurl,
                                data:{
                                    old_phone:old_phone,
                                    phone:phone
                                },
                                success:function(resp){
                                    // alert("response: "+resp);
                                    if(resp == 1){
                                        
                                        $(".errorPhone").text("Phone number already exist!");
                                        $("#phone").val("");

                                    }else{
                                        $(".errorPhone").text("");
                                    }
                                }
                            });
                        }else{
                            $(".errorPhone").text("Please provide a valid phone number");
                        }
                });
                $("#editSubadmin").validate({
                rules:{
                    'fname':{
                        required:true
                    },
                    'lname':{
                        required:true
                    },
                    'password': {
                        minlength: 8
                    },            
                    'confirm_password':{
                        equalTo: '#password'
                    },
                    'phone':{
                        required:true,
                        digits:true
                    },
                    'email':{
                        required:true,
                        email:true
                    },
                    'permission':{
                        checksidebar:true
                    },
                    'profile_pic':{
                        accept:"image/jpg,image/jpeg,image/png"
                    }
                },
                messages: { 
                    fname: { 
                        required: '@lang('admin_lang.first_name_cannot_be_left_blank')'
                    },
                    lname: { 
                        required: '@lang('admin_lang.first_name_cannot_be_left_blank')'
                    },
                    phone: { 
                        required: '@lang('admin_lang.please_provide_valid_phone_number')'
                    },
                    email: { 
                        required: '@lang('admin_lang.email_id_cannot_be_left_blank')'
                    },
                    permission: { 
                        checksidebar: '@lang('admin_lang.please_check_atleast_one_checkbox')'
                    },
                    confirm_password:'@lang('admin_lang.password_didnot_match')'
                },
                errorPlacement: function (error, element) 
                {
                // console.log(element.attr("name"));
                // console.log(error);
                if (element.attr("name") == "fname") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('admin_lang.first_name_cannot_be_left_blank')'+'</label>';
                    $('.errorFname').html(error);
                }
                if (element.attr("name") == "password") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('admin_lang.password_length_should_be')'+'</label>';
                    $('.errorPassword').html(error);
                }
                if (element.attr("name") == "confirm_password") {
                    var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.password_did_not_match')'+'</label>';
                    $('.errorConfirmPassword').html(error);
                }
                if (element.attr("name") == "lname") {
                    var error = '<label for="ln" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('admin_lang.last_name_cannot_be_left_blank')'+'</label>';
                    $('.errorLname').html(error);
                }
                if (element.attr("name") == "email") {
                    var error = '<label for="gn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('admin_lang.please_provide_valid_email_id')'+'</label>';
                    $('.errorEmail').html(error);
                }
                if (element.attr("name") == "phone") {
                    var error = '<label for="phn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('admin_lang.please_provide_valid_phone_number')'+'</label>';
                    $('.errorPhone').html(error);
                }
                if (element.attr("name") == "profile_pic") {
                    var error = '<label for="pro" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('admin_lang.please_upload_your_profile_photo_with_extension_type_jpg_jpeg_png')'+'</label>';
                    $('.errorpic').html(error);
                }
                if (element.attr("name") == "permission[]"){
                    var error = '<label for="sb" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('admin_lang.error_for_subadmin_permission')'+'</label>';
                    $('.errorPermission').html(error);
                }
                }
            });
        $("#saveSubAdminDetails").click(function(){
            $("#editSubadmin").submit();
        });
    });

</script>
@endsection