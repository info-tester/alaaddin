@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage SubAdmin') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.manage_sub_admin')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style type="text/css">
    #sub_admin_filter{
        display: none;
    }

    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')

        <div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">@lang('admin_lang.manage_sub_admin')</h2>
                            
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">@lang('admin_lang.add_sub_admin')</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.manage_sub_admin')</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (session()->has('success'))
                <div class="alert alert-success vd_hidden session_success_div" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                </div>
                @elseif ((session()->has('error')))
                <div class="alert alert-danger vd_hidden session_error_div" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
                </div>
                @endif
                <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong class="success_msg"></strong>
                </div>
                <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong class="error_msg"></strong>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang.sub_admin') <a class="adbtn btn btn-primary" href="{{ route('admin.add.subadmin') }}"><i class="fas fa-plus"></i> @lang('admin_lang.add')</a></h5>
                            <div class="card-body">
                                <form id="form" method="post" action="{{ route('admin.list.subadmin') }}">
                                    @csrf
                                    <div class="row">
                                        <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="keyword" class="col-form-label">@lang('admin_lang.keyword') </label>
                                            <input id="col1_filter" name="keyword" type="text" class="form-control keyword" placeholder='@lang('admin_lang.keyword')' value="{{ @$key['keyword'] }}">
                                        </div>
                                        
                                        <div data-column="2" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="permission" class="col-form-label">@lang('admin_lang.permission')</label>
                                            <select class="form-control permission" id="col2_filter" name="permission">
                                                <option value="">@lang('admin_lang.select_permission')</option>
                                                @if(@$menus)
                                                @foreach(@$menus as $menu)
                                                <option value="{{ $menu->menu_id }}" @if(@$key['permission'] == @$menu->menu_id) selected @endif>{{ @$menu->name }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>

                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                           <a href="javascript:void(0)" id="searchSubAdmin" class="btn btn-primary fstbtncls">@lang('admin_lang.search')</a>

                                           <input type="reset" value="@lang('admin_lang.reset_search')" class="btn btn-default fstbtncls reset_search" style="margin-left: 20px">
                                       </div>
                                       
                                   </div>
                               </form>
                               <div class="table-responsive listBody">
                                <table class="table table-striped table-bordered" id="sub_admin">
                                    <thead>
                                        <tr>
                                            <th>@lang('admin_lang.name')</th>
                                            <th>@lang('admin_lang.email')</th>
                                            <th>@lang('admin_lang.phone_number')</th>
                                            <th>@lang('admin_lang.permission')</th>
                                            <th>@lang('admin_lang.status')</th>
                                            <th>@lang('admin_lang.action')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- @if(@$sub_admins)
                                        @foreach(@$sub_admins as $subadmin)
                                        <tr class="tr_{{ @$subadmin->id }}">
                                            <td>{{ @$subadmin->fname }}  {{ @$subadmin->lname }}</td>
                                            <td>{{ @$subadmin->email }}</td>
                                            <td>{{ @$subadmin->phone }}</td>
                                            <td>
                                                @if(@$subadmin->sidebarAccess)
                                                @foreach(@$subadmin->sidebarAccess as $key=>$sidebar)
                                                @if($key != 0)
                                                ,
                                                @endif
                                                {{ @$sidebar->menuDetails[0]->name }}
                                                @endforeach
                                                @endif
                                            </td>
                                            <td class="status_{{ @$subadmin->id }}">
                                                @if(@$subadmin->status == 'A')
                                                @lang('admin_lang.Active')
                                                @else
                                                @lang('admin_lang.Inactive')
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.edit.subadmin',$subadmin->id) }}"><i class="fas fa-edit" title='@lang('admin_lang.Edit')'></i></a>

                                                @if(@$subadmin->status == 'A')
                                                <a class="status_change" href="javascript:void(0)" data-id="{{ @$subadmin->id }}"><i class="fas fa-ban icon_change_{{ @$subadmin->id }}" title='@lang('admin_lang.Inactive')'></i></a>    
                                                @else
                                                <a class="status_change" href="javascript:void(0)" data-id="{{ @$subadmin->id }}"><i class="far fa-check-circle icon_change_{{ @$subadmin->id }}" title='@lang('admin_lang.Active')'></i></a>
                                                @endif

                                                <a class="delete_subadmin" href="javascript:void(0)" data-id="{{ @$subadmin->id }}"><i class=" fas fa-trash" title='@lang('admin_lang.Delete')'></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif --}}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                           <th>@lang('admin_lang.name')</th>
                                           <th>@lang('admin_lang.email')</th>
                                           <th>@lang('admin_lang.phone_number')</th>
                                           <th>@lang('admin_lang.permission')</th>
                                           <th>@lang('admin_lang.status')</th>
                                           <th>@lang('admin_lang.action')</th>
                                       </tr>
                                   </tfoot>
                               </table>
                           </div>
                       </div>
                   </div>
               </div>
               <!-- ============================================================== -->
               <!-- end basic table  -->
               <!-- ============================================================== -->
           </div>
        </div>


        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
        <div class="loader" style="display: none;">
            <img src="{{url('public/loader.gif')}}">
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click', '.reset_search', function() {
            $('#sub_admin').DataTable().search('').columns().search('').draw();
        })

        function filterColumn ( i ) {
            $('#sub_admin').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }
        
        $('#sub_admin').DataTable( {
            stateSave: true,
            "order": [[0, "asc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
                $('#col2_filter').val(data.search.permission)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
                data.search.permission = $('#col2_filter').val()
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('admin.list.subadmin') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
                { 
                    data: 0,
                    render: function(data, type, full) {
                        return full.fname+' '+ full.lname
                    }
                },
                {   data: 'email' },
                {   data: 'phone' },
                {   
                    data: '',
                    render: function(data, type, full) {
                        var html = '';
                        full.sidebar_access.forEach(function(item, index) {
                            if(index != 0) {
                                html += ', '
                            }
                            html += item.menu_details[0].name
                        })
                        return html;
                    }
                },
                {   
                    data: 'status',
                    render: function(data, type, full) {
                        if(data == 'A') {
                            return '<span class="status_'+full.id+'">@lang('admin_lang.Active')</span>'
                        } if(data == 'I') {
                            return '<span class="status_'+full.id+'">@lang('admin_lang.Inactive')</span>'
                        }
                    }
                },
                {   
                    data: 'status',
                    render: function(data, type, full) {
                        var a = '';
                        if(data == 'A') {
                            a += '<a class="status_change" href="javascript:void(0)" data-id="'+full.id+'"><i class="fas fa-ban icon_change_'+full.id+'" title="@lang('admin_lang.Inactive')"></i></a>'
                        }
                        if(data == 'I') {
                            a += '<a class="status_change" href="javascript:void(0)" data-id="'+full.id+'"><i class="far fa-check-circle icon_change_'+full.id+'" title="@lang('admin_lang.Active')"></i></a>'
                        }
                                                
                        a += ' <a href="{{ url('admin/edit-sub-admin') }}/'+full.id+'"><i class="fas fa-edit" title="@lang('admin_lang.Edit')"></i></a>'

                        a += ' <a href="{{ url('admin/show-subadmin-logs') }}/'+full.id+'"><i class="fa fa-user-circle" title="@lang('admin_lang.show_logs')"></i></a>'

                        a += ' <a class="delete_subadmin" href="javascript:void(0)" data-id="'+full.id+'"><i class=" fas fa-trash" title="@lang('admin_lang.Delete')"></i></a>'

                        return a;
                    }
                }
            ]
        });


        // change event
        $('input.keyword').on( 'keyup click', function () {
            filterColumn( 1 );
        });
        $('.permission').on( 'change', function () {
            filterColumn( 2 );
        });

        /*$("#searchSubAdmin").click(function(){
            var permission = $.trim($("#permission").val());
            var keyword = $.trim($("#keyword").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.subadmin') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    permission:permission,
                    keyword:keyword
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });
        $("#keyword").on("keyup", function(e) {
            var value = $(this).val().toLowerCase();
            $("#myTable tbody tr").filter(function(){
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
          });
        });
        $("#searchSubAdmin").click(function(){
            var permission = $.trim($("#permission").val());
            var keyword = $.trim($("#keyword").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.subadmin') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    permission:permission,
                    keyword:keyword
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });
        $(document).on("change","#permission",function(e){
            var permission = $.trim($("#permission").val());
            var keyword = $.trim($("#keyword").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.subadmin') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    permission:permission,
                    keyword:keyword
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });
        $(document).on("blur","#keyword",function(e){
            var permission = $.trim($("#permission").val());
            var keyword = $.trim($("#keyword").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.subadmin') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    permission:permission,
                    keyword:keyword
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });*/


        // ajax 
        $(document).on("click",".status_change",function(e){
            var id = $(e.currentTarget).attr("data-id");
            // $status_que = "";
            // alert(status);
            // if(status == 'A'){
            //     $status_que = "Do you want to block this customer ?";
            // }else{
            //     $status_que = "Do you want to unblock this customer ?";   
            // }
            if(confirm("Do you want to change status of this subadmin ?")){
                $.ajax({
                    url:"{{ route('admin.update.status.subadmin') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp != null){
                            var st="";
                            if(resp.status == 'A'){
                                st="Active";
                                $(".success_msg").html('Success!Subadmin is active!');
                                $(".icon_change_"+id).removeClass("far fa-check-circle");
                                // $(".icon_change_"+id).removeClass("");
                                $(".icon_change_"+id).addClass("fas fa-ban");
                                $(".icon_change_"+id).attr("title","Active");
                                // $(".b_u_"+id).attr('data-status','I');
                                // $(e.currentTarget).attr('data-status','A');
                            }else if(resp.status == 'I'){
                                st="Inactive";
                                $(".success_msg").html('Success!Subadmin is inactive!');
                                $(".icon_change_"+id).removeClass("fas fa-ban");
                                $(".icon_change_"+id).addClass("far fa-check-circle");
                                $(".icon_change_"+id).attr("title","Inactive");
                                // $(".b_u_"+id).attr('data-status','I');
                                // $(e.currentTarget).attr('data-status','I');
                            }
                            $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('Unauthorized access!Status of subadmin is not changed!');
    
                        }
                        $(".session_success_div").hide();
                        $(".session_error_div").hide();
                    }
                });
            }
            // }
        });
        
        $(document).on("click",".delete_subadmin",function(e){
            var id = $(e.currentTarget).attr("data-id");
            var $this = $(this);
            if(confirm("Do you want to delete this subadmin ?")){
                $.ajax({
                    url:"{{ route('admin.delete.subadmin.details') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".success_msg").html('@lang('admin_lang.success_cust_delete')');
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".tr_"+id).hide();
                            $this.parent().parent().remove();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('@lang('admin_lang.unauthorized_access_cust')');
                        }
                        $(".session_success_div").hide();
                        $(".session_error_div").hide();
                    }
                });
            }
            // }
        });

    });
</script>
@endsection