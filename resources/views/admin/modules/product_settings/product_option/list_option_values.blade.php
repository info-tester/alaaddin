@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | List Of Option Values') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.list_of_option_values')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">@lang('admin_lang.Add Option Value List')</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">
                                        @lang('admin_lang.Option Value')
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session()->has('success'))
                    <div class="alert alert-success vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                    </div>
                    @elseif ((session()->has('error')))
                    <div class="alert alert-danger vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    @endif
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <input type="hidden" id="v_id_nm_chk" value="{{ @$vid }}">
                            <h5 class="card-header">{{ @$variants->productCategoryDetail->categoryByLanguage->title }} > {{ @$variants->productSubcategoryDetail->categoryByLanguage->title }} > {{ @$variants->variantByLanguage->name }} [ 
                                @if($variants->type == 'P')
                                @lang('admin_lang.price_stock_dependent_text')
                                @elseif($variants->type == 'S')
                                @lang('admin_lang.stock_dependent_only_text')
                                @elseif($variants->type == 'SH')
                                @lang('admin_lang.show_in_search')
                                @elseif($variants->type == 'I')
                                @lang('admin_lang.informative')
                                @endif
                            ]
                            <a class="adbtn btn btn-primary" href="{{ route('admin.list.product.option') }}"><i class="fas fa-less-than"></i>
                            @lang('admin_lang.back')
                            </a>
                            </h5>
                            <div class="card-body">
                                <!-- dd($variant) -->
                                @if(Request::segment(2) =='edit-option-values')
                                <form id="saveOptionValueForm" method="post" enctype="multipart/form-data"  action="{{ route('admin.edit.option.value',$variants->id) }}">
                                @else
                                <form id="saveOptionValueForm" method="post" enctype="multipart/form-data" action="{{ route('admin.add.option.value',$variants->id) }}">
                                @endif    
                                    @csrf
                                            <div class="row">
                                                <input type="hidden" id="hid" name="hid" value="{{Request::segment(3)}}"/>
                                            @if(@$language)
                                                @foreach(@$language as $key=>$lang)    
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="inputText{{ $language[$key]->id }}" class="col-form-label">
                                                    <span style="color: red;">*</span>@lang('admin_lang.Option Value') [{{ $lang->name }} ]</label>
                                                <input id="inputText{{ $language[$key]->id }}" type="text" name="optionValueName[]" class="form-control optionValueName OptionNameBylanguage" placeholder="@lang('admin_lang.Option Value')" value="{{ @$option_value->variantValueDetails[$key]->name }}">
                                                <input type="hidden" name="details_ids[]" value="{{ @$option_value->variantValueDetails[$key]->id }}">
                                                <span class="errorOptionValueName" style="color: red;"></span>
                                            </div> 
                                                @endforeach
                                            @endif
                                            <input type="hidden" id="upload_img" name="upload_img" value="{{ @$variants->upload_image }}">
                                            </div>

                                            @if(@$variants->upload_image == 'Y')
                                            <div class="row"><br/></div>
                                            <div class="row" style="height: 80px;">
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                {{-- <p for="" class="col-form-label">@lang('admin_lang.category_picture')</p> --}}
                                                <input type="file" class="custom-file-input inpt" id="customFile" name="optionValueImage" accept="image/jpg,image/jpeg,image/png">
                                                <label class="custom-file-label extrlft" for="customFile">
                                                @lang('admin_lang.upload_img') 
                                                </label>
                                                @lang('admin_lang.recommended_size_100')
                                                <div class="errorpic" style="color: red;">
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" style="display: block;" style="width: 100px;height: 100px;position: relative;">
                                                <span class="after-upload">
                                                    <div class="uploadServicePicDiv">
                                                        @if(@$option_value->image)
                                                        @php
                                                        $image_path = 'storage/app/public/option_value_images/'.@$option_value->image; 
                                                        @endphp
                                                        @if(file_exists(@$image_path))
                                                        <img id="profilePicture" src="{{ URL::to('storage/app/public/option_value_images/'.@$option_value->image) }}" alt="" style="width: 100px;height: 100px;">
                                                        @endif
                                                        @endif
                                                        <img id="profilePicture" src="" alt="" style="width: 100px;height: 100px;">
                                                    </div>
                                                </span>
                                            </div>
                                            </div>
                                            @endif
                                            <div class="row">
                                            <div class="form-group col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                                
                                                <a href="javascript:void(0)" id="saveOptionValue" class="btn btn-primary fstbtncls">@lang('admin_lang.Save')</a>
                                            </div>
                                        </div>
                                        </form>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>@lang('admin_lang.Option Name')</th>
                                                <th>@lang('admin_lang.Value')</th>
                                                <th>@lang('admin_lang.Action')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(@$optionValues)
                                                @foreach(@$optionValues as $values)
                                                    <tr>
                                                        <td>{{ @$variants->variantByLanguage->name }}</td>
                                                        <td>
                                                        {{ @$values->variantValueByLanguage->name }}
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('admin.edit.option.value',$values->id) }}"><i class="fas fa-edit"></i></a>
                                                            <a href="{{ route('admin.delete.product.option.value',$values->id) }}" onclick="javascript:var c=confirm("@lang('admin_lang.do_u_want_delete_option_value')"); if(c == true){return true;} else { return false; }"><i class=" fas fa-trash"></i></a>
                                                        </td>
                                                    </tr>
                                           @endforeach
                                           @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>@lang('admin_lang.Option Name')</th>
                                                <th>@lang('admin_lang.Value')</th>
                                                <th>@lang('admin_lang.Action')</th>
                                            </tr>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
            </div>
<!-- footer -->
<!-- ============================================================== -->
@include('admin.includes.footer')
<!-- ============================================================== -->
<!-- end footer -->
<!-- ============================================================== -->
@endsection            
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
 <script src="{{ URL::to('public/assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script>
<script src="{{ URl::to('public/assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
<script src="{{ URl::to('public/assets/vendor/slimscroll/jquery.slimscroll.js') }}"></script>
<script src="{{ URL::to('public/assets/vendor/multi-select/js/jquery.multi-select.js') }}"></script>
<script src="{{ URL::to('public/assets/libs/js/main-js.js') }}"></script>
{{-- <script src="{{ URL::to('cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('public/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ URL::to('../../../../../cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::to('public/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
 <script src="{{ URL::to('public/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ URL::to('../../../../../cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ URL::to('../../../../../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ URL::to('../../../../../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ URL::to('../../../../../cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ URL::to('../../../../../cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ URL::to('../../../../../cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::to('../../../../../cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ URL::to('../../../../../cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ URL::to('../../../../../cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script> --}}
<script type="text/javascript">
    $(document).ready(function(){
        $("#customFile").change(function() {
        var filename = $.trim($("#customFile").val()),
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".errorpic").text("");
                readURL(this);
            }else{

                $(".errorpic").text("@lang('validation.image_extension_type')");
                $('#profilePicture').attr('src', "");

            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                  $('#profilePicture').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).on("blur",".OptionNameBylanguage",function(e){
            var name = $.trim($(e.currentTarget).val()),
            v_id = $.trim($("#v_id_nm_chk").val()),
            rurl = "{{ route('admin.check.duplicate.option.name') }}";
            // alert("v_id : "+v_id);
            $.ajax({
                type:"GET",
                url:rurl,
                data:{
                   name:name, 
                   vid:v_id 
                },
                success:function(resp){
                    if(resp == 0){
                        $(e.currentTarget).parent().find(".errorOptionValueName").text("@lang('admin_lang.option_value_name_exist')");
                        $(e.currentTarget).val("");
                    }else{
                        $(".errorOptionValueName").text("");
                    }
                }
            });
        });
        $("#saveOptionValue").click(function(e){
            var len = $(".optionValueName").length,
            errorCount = 0;
            for(var i=0;i<len;i++){
                var nm = $(".optionValueName").eq(i).val();
                if(nm == ""){
                    errorCount++;
                    $(".errorOptionValueName").eq(i).text("@lang('validation.required')");
                }else{
                    $(".errorOptionValueName").eq(i).text("");
                }
                
            }
            var isImage = $.trim($("#upload_img").val());
            if(isImage == 'Y'){
                var filename = $.trim($("#customFile").val());
                if(filename != ""){
                    var filename_arr = filename.split("."),
                    ext = filename_arr[1];
                    ext = ext.toLowerCase();

                    if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                        $(".errorpic").text("");
                        readURL(this);
                    }else{
                        errorCount++;
                        $(".errorpic").text("@lang('validation.image_extension_type')");
                        $('#profilePicture').attr('src', "");

                    }
                }else{
                    // errorCount++;
                        // $(".errorpic").text('@lang('validation.image_extension_type')');
                        // $('#profilePicture').attr('src', "");
                }
            }

            if(errorCount == 0){
               $("#saveOptionValueForm").submit();     
            }
        });
    });
</script>
@endsection