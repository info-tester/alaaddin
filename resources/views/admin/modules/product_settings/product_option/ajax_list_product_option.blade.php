<table class="table table-striped table-bordered first ">
                                        <thead>
                                            <tr>
                                                <th>@lang('admin_lang.Category')</th>
                                                <th>@lang('admin_lang.Sub Category')</th>
                                                <th>@lang('admin_lang.Option')</th>
                                                <th>@lang('admin_lang.Type')</th>
                                                <th>@lang('admin_lang.Status')</th>
                                                <th>@lang('admin_lang.Action')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                @if(@$productOptions)
                                                @foreach($productOptions as $option)
                                                <tr class="tr_{{ @$option->id }}">
                                                    <td>{{ @$option->productCategoryDetail->categorySubcategoryDetails[0]->title }}</td>
                                                    <td>
                                                     {{ @$option->productSubcategoryDetail->categorySubcategoryDetails[0]->title }}   

                                                    </td>
                                                    <td>
                                                    {{ @$option->variantByLanguage->name }}
                                                    </td>
                                                    <td>
                                                    @if(@$option->type == 'P')
                                                    @lang('admin_lang.price_stock_dependent_text')
                                                    @elseif(@$option->type == 'S')
                                                    @lang('admin_lang.stock_dependent_only_text')
                                                    @elseif(@$option->type == 'SH')
                                                    @lang('admin_lang.show_in_search')
                                                    @elseif(@$option->type == 'I')
                                                    @lang('admin_lang.informative')
                                                    @endif
                                                    </td>
                                                    <td class="status_{{ @$option->id }}">
                                                        @if(@$option->status == 'A')
                                                        @lang('admin_lang.Active')
                                                        @elseif(@$option->status == 'I')
                                                        @lang('admin_lang.Inactive')
                                                        @endif
                                                        
                                                    </td>
                                                    <td>
                                                        {{-- option value --}}
                                                        <a href="{{ route('admin.list.option.values',$option->id) }}"><i class="fas fa-hand-holding-usd" title='@lang('admin_lang.option_values')'></i></a>  
                                                        {{-- edit --}}
                                                        <a href="{{ route('admin.edit.product.option',$option->id) }}"><i class="fas fa-edit" title='@lang('admin_lang.Edit')'></i></a>

                                                        {{-- status --}}
                                                        @if(@$option->status == 'A')
                                                        <a class="status_change" href="javascript:void(0)" data-id="{{ @$option->id }}"><i class="fas fa-ban icon_change_{{ @$option->id }}" title='@lang('admin_lang.Inactive')'></i></a>
                                                            
                                                        @else
                                                            
                                                        <a class="status_change" href="javascript:void(0)" data-id="{{ @$option->id }}"><i class="far fa-check-circle icon_change_{{ @$option->id }}" title='@lang('admin_lang.Active')'></i></a>
                                                        @endif

                                                        {{-- delete --}}
                                                        <a class="delete_option" href="javascript:void(0)" data-id="{{ @$option->id }}"><i class=" fas fa-trash" title='@lang('admin_lang.Delete')'></i></a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>@lang('admin_lang.Category')</th>
                                                <th>@lang('admin_lang.Sub Category')</th>
                                                <th>@lang('admin_lang.Option')</th>
                                                <th>@lang('admin_lang.Type')</th>
                                                <th>@lang('admin_lang.Status')</th>
                                                <th>@lang('admin_lang.Action')</th>
                                            </tr>
                                            </tr>
                                        </tfoot>
                                    </table>

<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
 <script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>