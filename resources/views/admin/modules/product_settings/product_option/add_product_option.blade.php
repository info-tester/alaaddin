@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Add Product Option') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.AddProductOptions')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<style>
    /* The container */
    .container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 15px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-family: 'Circular Std Book';
    font-style: normal;
    font-weight: normal;
    }
    /* Hide the browser's default radio button */
    .container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    line-height: 1.42857143;
    }
    /* Create a custom radio button */
    .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
    border-radius: 50%;
    }
    /* On mouse-over, add a grey background color */
    .container:hover input ~ .checkmark {
    background-color: #ccc;
    }
    /* When the radio button is checked, add a blue background */
    .container input:checked ~ .checkmark {
    background-color: #2196F3;
    }
    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
    content: "";
    position: absolute;
    display: none;
    }
    /* Show the indicator (dot/circle) when checked */
    .container input:checked ~ .checkmark:after {
    display: block;
    }
    /* Style the indicator (dot/circle) */
    .container .checkmark:after {
    top: 9px;
    left: 9px;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background: white;
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
<div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
        <!-- ============================================================== -->
        <!-- pageheader  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">
                        @lang('admin_lang.E-commerceDashboard')
                    </h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    @lang('admin_lang.AddProductOptions')
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session()->has('success'))
        <div class="alert alert-success vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
        </div>
        @elseif ((session()->has('error')))
        <div class="alert alert-danger vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
        </div>
        @endif
        <!-- ============================================================== -->
        <!-- end pageheader  -->
        <!-- ============================================================== -->
        <div class="ecommerce-widget">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">
                            @lang('admin_lang.AddProductOptions')
                            <a class="adbtn btn btn-primary" href="{{ route('admin.list.product.option') }}">
                            <i class="fas fa-less-than"></i>
                            @lang('admin_lang.back')
                            </a>
                        </h5>
                        <div class="card-body">
                            <form id="addProductOptionForm" method="post" action="{{ route('admin.add.product.option') }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <label for="category" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.Category')</label>
                                        <select class="form-control required" id="category" name="category">
                                            <option value="">
                                                @lang('admin_lang.select_category')
                                            </option>
                                            @if(@$category)
                                            @foreach(@$category as $category)
                                            <option value="{{ @$category->id }}">{{ @$category->categoryDetailsByLanguage[0]->title }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <span class="errorCategory" style="color: red;"></span>
                                    </div>
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <label for="subcategory" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.SubCategory')</label>
                                        <select class="form-control required" id="subcategory" name="subcategory">
                                            <option value="">@lang('admin_lang.SelectSubCategory')</option>
                                        </select>
                                        <span class="errorSubcategory" style="color: red;"></span>
                                    </div>
                                    @if(@$language)
                                    @foreach(@$language as $key=>$lang)
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <label for="inputText{{ $language[$key]->id }}" class="col-form-label">
                                        <span style="color: red;">*</span>@lang('admin_lang.ProductOptionsName') [{{ $lang->name }}]</label>
                                        <input id="inputText{{ $language[$key]->id }}" type="text" class="form-control productOptionName required" placeholder='@lang('admin_lang.ProductOptionsName')' name="product_option_name[]">
                                        <span class="errorProductOptionName" style="color: red;"></span>
                                    </div>
                                    @endforeach
                                    @endif
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <label for="upload_image" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.upload_image')</label>
                                        <select class="form-control required" id="upload_image" name="upload_image">
                                            <option value="">@lang('admin_lang.select')</option>
                                            <option value="Y">@lang('admin_lang.yes')</option>
                                            <option value="N">@lang('admin_lang.no')</option>
                                        </select>
                                        <span class="errorUploadImage" style="color: red;"></span>
                                    </div>
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <label for="dependent_type" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.Type')</label>    
                                        <label class="container dependent">
                                        @lang('admin_lang.price_stock_dependent')
                                        <input type="radio" name="dependent" class="required type" value="P">
                                        <span class="checkmark"></span>
                                        </label>
                                        <label class="container dependent">
                                        @lang('admin_lang.stock_dependent_only')
                                        <input type="radio" name="dependent" class="required type" value="S">
                                        <span class="checkmark"></span>
                                        </label>
                                        <label class="container dependent">
                                        @lang('admin_lang.show_in_search')
                                        <input type="radio" name="dependent" class="required type" value="SH">
                                        <span class="checkmark"></span>
                                        </label>
                                        <label class="container dependent">@lang('admin_lang.informative')
                                        <input type="radio" name="dependent" class="required type" value="I">
                                        <span class="checkmark"></span>
                                        </label>
                                        </label>
                                        <input type="hidden" id="type" name="type_dependent" value=""/>
                                        <span class="errorDependent" style="color: red;"></span>
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <a href="javascript:void(0)" id="saveProductOptions" class="btn btn-primary ">@lang('admin_lang.Save')</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- footer -->
<!-- ============================================================== -->
@include('admin.includes.footer')
<!-- ============================================================== -->
<!-- end footer -->
<!-- ============================================================== -->
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $(document).on("change","#category",function (e){
        var category = $.trim($("#category").val());
        if (category != "") {
            $.ajax({
                type: "get",
                url: "{{ route('admin.get.subcategories') }}",
                data: {
                    category: category
                },
                success: function (resp) {
                    // alert(JSON.stringify(resp));
                    if (resp != 0) {
                        var len = resp.length,
                            html = '<option value="">' + "@lang('admin_lang.SelectSubCategory')" + '</option>';
                        for (var i = 0; i < len; i++) {
                            var newHtml = '<option value="' + resp[i].id + '">' + resp[i].category_details_by_language[0].title + '</option>';
                            html = html + newHtml;
                        }
                        $("#subcategory").html(html);
                    } else {
                        html = '<option value="">' + "@lang('admin_lang.SelectSubCategory')" + '</option>';
                        $("#subcategory").html(html);
                    }
                }
            });
        } else {
            html = '<option value="">' + "@lang('admin_lang.SelectSubCategory')"+ '</option>';
            $("#subcategory").html(html);
        }
    });
    $(document).on('change', 'input[type=radio]', function (e) {
        // $(this).parent().find('.container').removeClass('test');
        $('.dependent').removeClass('checked');
        $(this).parent().addClass('checked');
        var type = $(this).val();
        $("#type").val(type);
    });
    $("#saveProductOptions").click(function (e) {
        var category = $.trim($("#category").val()),
            subcategory = $.trim($("#subcategory").val()),
            upload_image = $.trim($("#upload_image").val()),
            errorCount = 0,
            len = $(".productOptionName ").length,
            type = $.trim($("#type").val());
        // alert(type);
        if (category == "") {
            errorCount++;
            $(".errorCategory").text("@lang('validation.required')");
        } else {
            $(".errorCategory").text("");
        }
        if (subcategory == "") {
            errorCount++;
            $(".errorSubcategory").text("@lang('validation.required')");
        } else {
            $(".errorSubcategory").text("");
        }
        if (upload_image == "") {
            errorCount++;
            $(".errorUploadImage").text("@lang('admin_lang.select_images_option_values')");
        } else {
            $(".errorUploadImage").text("");
        }

        for (var i = 0; i < len; i++) {
            var value = $.trim($(".productOptionName").eq(i).val());
            if (value == "") {
                errorCount++;
                $(".errorProductOptionName").eq(i).text("@lang('validation.required')");
            } else {
                $(".errorProductOptionName").eq(i).text("");
            }
        }
        var hasSelected = $('.dependent').hasClass("checked");

        if (!hasSelected) {
            errorCount++;
            $(".errorDependent").text("@lang('validation.required')");
        } else {
            // alert("type : "+type+" category : "+category+" subcategory : "+subcategory);
            if (type == 'P' || type == 'S') {
                if (category != "" || subcategory != "") {
                    $.ajax({
                        type: "get",
                        url: "{{ route('admin.check.type') }}",
                        data: {
                            type: type,
                            category: category,
                            subcategory: subcategory
                        },
                        success: function (resp) {
                            // alert("response from ajax call :"+resp);
                            if (resp == 0) {
                                errorCount++;
                                $(".errorDependent").text("@lang('admin_lang.combination_of_limit_exceed')");
                            } else {
                                $(".errorDependent").text("");
                                if (errorCount == 0) {
                                    $("#addProductOptionForm").submit();
                                }
                            }
                        }
                    });
                }
            } else {
                $(".errorDependent").text("");
                if (errorCount == 0) {
                    $("#addProductOptionForm").submit();
                }
            }
        }
    });
});
</script>
@endsection