@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | List Of Product Option') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.list_of_product_option')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<style type="text/css">
    .disabled{
        display: none;
    }
</style>
<style type="text/css">
    #myTable_filter{
        display: none;
    }
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
</style>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">
                            @lang('admin_lang.Manage Product Options')
                            </h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">@lang('admin_lang.AddProductOptions')</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.Manage Product Options')</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session()->has('success'))
                    <div class="alert alert-success vd_hidden session_success_div" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                    </div>
                    @elseif ((session()->has('error')))
                    <div class="alert alert-danger vd_hidden session_error_div" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    @endif
                    <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong class="success_msg"></strong> 
                    </div>
                    <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong class="error_msg"></strong> 
                    </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang.Manage Product Options') <a class="adbtn btn btn-primary" href="{{ route('admin.add.product.option') }}"><i class="fas fa-plus"></i> @lang('admin_lang.Add')</a></h5>
                            <div class="card-body">
                                <form id="searchProductOptionsForm" method="post" action="{{ route('admin.list.product.option') }}">
                                    @csrf
                                    <div class="row">
                                        <div data-column="0" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="cat_lb" class="col-form-label">@lang('admin_lang.Category')</label>
                                            <select class="form-control category" id="col0_filter" name="category">
                                                <option value="">
                                                @lang('admin_lang.select_category')
                                                </option>
                                                @if(@$category)
                                                    @foreach(@$category as $category)
                                                        <option value="{{ @$category->id }}" 
                                                            @if(@$key['category'] == @$category->id)
                                                            selected
                                                            @endif>{{ @$category->categoryDetailsByLanguage[0]->title }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <input type="hidden" id="subCatId" value="{{ @$key['subcategory'] }}">
                                        </div>
                                            
                                        <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="subcat_lb" class="col-form-label">@lang('admin_lang.SubCategory')</label>
                                            <select class="form-control subcategory" id="col1_filter" name="subcategory">
                                                <option value="">@lang('admin_lang.SelectSubCategory')</option>
                                                @if(@$subCategory)
                                                    @foreach(@$subCategory as $subCat)
                                                        <option value="{{ @$subCat->id }}">{{ @$subCat->categoryDetailsByLanguage[0]->title }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <a href="javascript:void(0)" id="searchProductOptions" class="btn btn-primary fstbtncls">@lang('admin_lang.Search')</a>
                                            <input type="reset" value="@lang('admin_lang.reset_search')" class="btn btn-default fstbtncls reset_search" style="margin-left: 25px;">
                                        </div>
                                    </div>
                                </form>

                                <div class="table-responsive listBody">
                                    <table class="table table-striped table-bordered first " id="myTable">
                                        <thead>
                                            <tr>
                                                <th>@lang('admin_lang.Category')</th>
                                                <th>@lang('admin_lang.Sub Category')</th>
                                                <th>@lang('admin_lang.Option')</th>
                                                <th>@lang('admin_lang.Type')</th>
                                                <th>@lang('admin_lang.Status')</th>
                                                <th>@lang('admin_lang.Action')</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>@lang('admin_lang.Category')</th>
                                                <th>@lang('admin_lang.Sub Category')</th>
                                                <th>@lang('admin_lang.Option')</th>
                                                <th>@lang('admin_lang.Type')</th>
                                                <th>@lang('admin_lang.Status')</th>
                                                <th>@lang('admin_lang.Action')</th>
                                            </tr>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
            </div>
                   
<!-- footer -->
<!-- ============================================================== -->
@include('admin.includes.footer')
<!-- ============================================================== -->
<!-- end footer -->
<!-- ============================================================== -->
<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>
@endsection            
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif

<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click', '.reset_search', function() {
            $('#myTable').DataTable().search('').columns().search('').draw();
        });

        function filterColumn ( i ) {
            $('#myTable').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }

        $('#myTable').DataTable({
            stateSave: true,
            "order": [
                [1, "desc"]
            ],
            "stateLoadParams": function (settings, data) {
                $('#col0_filter').val(data.search.category);
                $('#col1_filter').val(data.search.subcategory);
            },
            stateSaveParams: function (settings, data) {
                data.search.category = $('#col0_filter').val();
                data.search.subcategory = $('#col1_filter').val();
            },
            "processing": true,
            "serverSide": true,
            'serverMethod':'post',
            "ajax": {
                "url": "{{ route('admin.list.product.option') }}",
                "data": function (d) {
                    d.myKey = "myValue";
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
                {
                    data: 'product_category_detail.category_subcategory_details[0].title'
                },
                {
                    data: 'product_subcategory_detail.category_subcategory_details[0].title'
                },
                {
                    data: "variant_details_by_language[0].name"
                },
                {
                    data: 'type',
                    render: function(data, type, full){
                        if(data == 'P'){
                            return '@lang('admin_lang.price_stock_dependent_text')'
                        }else if(data == 'S'){
                            return '@lang('admin_lang.stock_dependent_only_text')'
                        }
                        else if(data == 'SH'){
                            return '@lang('admin_lang.show_in_search')'
                        }
                        else if(data == 'I'){
                            return '@lang('admin_lang.informative')'
                        }
                    }
                },
                {
                    data: 'status',
                    render: function(data, type, full){

                        if(data == 'A'){
                            return '<span class="status_'+full.id+'">@lang('admin_lang.Active')</span>'
                        }
                        else if(data == 'I'){
                            return '<span class="status_'+full.id+'">@lang('admin_lang.Inactive')</span>'
                        }
                    }
                },
                {
                    data: 'status',
                    render:function(data, type, full){
                        var a = '';
                        a += '&nbsp; <a href="{{ url('admin/list-option-values') }}/'+full.id+'" title="@lang('admin_lang.option_values')"><i class="fas fa-hand-holding-usd"></i></a>'
                        a += '&nbsp; <a href="{{ url('admin/edit-product-option') }}/'+full.id+'" title="@lang('admin_lang.Edit')"><i class="fas fa-edit"></i></a>'
                        if(full.status == 'A'){
                        a += '&nbsp; <a class="status_change" href="javascript:void(0)" data-id="'+full.id+'"><i class="fas fa-ban icon_change_'+full.id+'" title="@lang('admin_lang.Inactive')"></i></a>'
                        }else if(full.status == 'I'){
                        a += '&nbsp; <a class="status_change" href="javascript:void(0)" data-id="'+full.id+'"><i class="far fa-check-circle icon_change_'+full.id+'" title="@lang('admin_lang.Active')"></i></a>'
                        }

                        a += '&nbsp; <a class="delete_option" href="javascript:void(0)" data-id="'+full.id+'" title="@lang('admin_lang.Delete')"><i class="fas fa-trash"></i></a>'
                        return a;
                    }
                },
            ]
        });

        $('#col0_filter').on( 'change', function () {
            var category = $.trim($("#col0_filter").val());
            $("#col1_filter").val('')
            $('#myTable').DataTable().search('').columns().search('').draw();
            if(category != ""){
                $.ajax({
                    type:"get",
                    url:"{{ route('admin.get.subcategories') }}",
                    data:{
                        category: category
                    },
                    beforeSend:function(){
                        $(".loader").show();
                    },
                    success:function(resp){
                        if(resp != 0){
                            var len = resp.length,
                            html ='<option value="">Select Sub Category</option>';
                            for(var i=0;i<len;i++){
                                var newHtml = '<option value="'+resp[i].id+'">'+resp[i].category_details_by_language[0].title+'</option>';
                                html = html+newHtml;
                            }
                            $(".subcategory").html(html);
                        }else{
                            html = '<option value="">Select Sub Category</option>';
                            $(".subcategory").html(html);
                        }
                    }
                }); 
            } else {
                html = '<option value="">Select Sub Category</option>';
                $("#col1_filter").html(html);
            }
            $(".loader").hide();
            filterColumn( $(this).parents('div').data('column') );
        });

        $('#col1_filter').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        });

        $(document).on("click",".status_change",function(e){
            var id = $(e.currentTarget).attr("data-id");
            if(confirm("Do you want to change status of this product option ?")){
                $.ajax({
                    url:"{{ route('admin.status.update.option') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp != null){
                            var st="";
                            if(resp.status == 'A'){
                                st="Active";
                                $(".success_msg").html('Success!Status of the product option is changed to active!');
                                $(".icon_change_"+id).removeClass("far fa-check-circle");
                                $(".icon_change_"+id).addClass("fas fa-ban");
                                $(".icon_change_"+id).attr("title","Inactive");
                            }else if(resp.status == 'I'){
                                st="Inactive";
                                $(".success_msg").html('Success!Status of the product option is changed to inactive!');
                                $(".icon_change_"+id).removeClass("fas fa-ban");
                                $(".icon_change_"+id).addClass("far fa-check-circle");
                                $(".icon_change_"+id).attr("title","Active");
                            }
                            $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('Unauthorized access!Status of merchant is not changed!');
                        }
                    }
                });
            }
        });

        $(document).on("click",".delete_option",function(e){
            var id = $(e.currentTarget).attr("data-id");
            var $this = $(this);
            if(confirm("Do you want to delete this option ?")){
                $.ajax({
                    url:"{{ route('admin.delete.option.permanently') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp == 1){
                            $(".success_msg").html('Success! Product option is deleted successfully!');
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $this.parent().parent().remove();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('Error! Unauthorized access!Product option is not deleted');
                        }
                        $(".session_success_div").hide();
                        $(".session_error_div").hide();
                    }
                });
            }
        });
    });
</script>
@endsection