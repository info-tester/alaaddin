@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | Admin | Manage Categories
@endsection
@section('content')
@section('links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@include('admin.includes.links')
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Manage Categories</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">
                                Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Manage Categories</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (session()->has('success'))
    <div class="alert alert-success vd_hidden session_success_div" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
    </div>
    @elseif ((session()->has('error')))
    <div class="alert alert-danger vd_hidden session_error_div" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
    </div>
    @endif
    {{-- @if (session()->has('success')) --}}
    <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong class="success_msg"></strong>  
    </div>
    {{-- @elseif ((session()->has('error'))) --}}
    <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong class="error_msg"></strong> 
    </div>
    {{-- @endif --}}
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Manage Categories
                    <a class="adbtn btn btn-primary" href="{{ route('admin.add.category') }}"><i class="fas fa-plus"></i> @lang('admin_lang.Add')</a>
                </h5>
                <div class="card-body">
                    <form id="searchListForm" method="post" action="{{ route('admin.list.category') }}">
                        @csrf
                        <div class="row">
                            <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <label  class="col-form-label">
                                Keywords
                                </label>
                                <input id="col1_filter" type="text" name="keyword" class="form-control keyword" placeholder="Keywords" value="{{ @$key['keyword'] }}">
                            </div>
                            <div data-column="3" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <label   class="col-form-label">
                                Status
                                </label>
                                <select class="form-control status" id="col3_filter" name="status">
                                    <option value="">
                                        Select Status
                                    </option>
                                    <option value="A">Active</option>
                                    <option value="I">Inactive</option>
                                    
                                </select>
                            </div>
                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                
                                <a href="javascript:void(0)" id="searchList" class="btn btn-primary fstbtncls">
                                Search
                                </a>
                                <input type="reset" value="Reset Search" class="btn btn-default fstbtncls reset_search" style="margin-left: 25px;">
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive listBody">
                        <table class="table table-striped table-bordered first" id="myTable">
                            <thead>
                                <tr>
                                    <th>@lang('admin_lang.Category')</th>
                                    <th>@lang('admin_lang.Image')</th>
                                    
                                    <th>@lang('admin_lang.Status')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </thead>
                            
                            <tfoot>
                                <tr>
                                    <th>@lang('admin_lang.Category')</th>
                                    <th>@lang('admin_lang.Image')</th>
                                    
                                    <th>@lang('admin_lang.Status')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table  -->
        <!-- ============================================================== -->
    </div>
</div>
<!-- footer -->
<!-- ============================================================== -->
@include('admin.includes.footer')
<!-- ============================================================== -->
<!-- end footer -->
<!-- ============================================================== -->
<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>
@endsection            
@section('scripts')
@include('admin.includes.scripts')

<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click', '.reset_search', function() {
            $('#myTable').DataTable().search('').columns().search('').draw();
            $(".slct").val("").trigger("chosen:updated");
        });
        function filterColumn ( i ) {
        $('#myTable').DataTable().column( i ).search(
            $('#col'+i+'_filter').val(),
            ).draw();
        }
        
        var styles = `
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
`
var styleSheet = document.createElement("style");
styleSheet.innerText = styles;
document.head.appendChild(styleSheet);
        $('#myTable').DataTable({
            stateSave: false,
            "order": [
                [0, "asc"]
            ],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.category);
                $('#col2_filter').val(data.search.subcategory);
                $('#col3_filter').val(data.search.status);
                $('#col4_filter').val(data.search.add_in_menu);
                $('#col5_filter').val(data.search.is_popular);
            },
            stateSaveParams: function (settings, data) {
                data.search.category = $('#col1_filter').val();
                data.search.subcategory = $('#col2_filter').val();
                data.search.status = $('#col3_filter').val();
                data.search.add_in_menu = $('#col4_filter').val();
                data.search.is_popular = $('#col5_filter').val();
            },
            "processing": true,
            "serverSide": true,
            'serverMethod':'post',
            "ajax": {
                "url": "{{ route('admin.load.category.list') }}",
                "data": function (d) {
                    console.log(d);
                    d.myKey = "myValue";
                    d._token = "{{ @csrf_token() }}";
                }
            },
            columnDefs: [ { targets: 0, type: 'natural' } ],
            'columns': [
                {
                    data: 'category_name_by_lang.title'
                },
                {
                    data: 'picture',
                    render:function(data, type, full, meta){
                        if(data){
                            return '<img width="100" height="70" src="storage/app/public/category_pics/'+data+'"/ alt="user" class="rounded" width="45">';
                        }else{
                            return " ";
                        }
                    }
                },
                
                {
                    data: 'status',
                    render: function(data, type, full, meta){
                        if(data == 'A'){
                            // return "Active"
                            return "<span class='status_"+full.slug+"'>"+"@lang('admin_lang.Active')"+"</span>"
                        }else{
                            // return "Inactive"
                            return "<span class='status_"+full.slug+"'>"+"@lang('admin_lang.Inactive')"+"</span>"
                        }
                    }
                },
                {
                    data: 'is_popular',
                    render:function(data, slug, full, meta) {
                        var a = '';
                        a += '&nbsp; <a href="{{ url('admin/edit-category').'/' }}'+full.slug+'"><i class="fas fa-edit" title="@lang('admin_lang.Edit')"></i></a>';

                        if(full.status == 'A'){
                            a += '&nbsp; <a class="status_change" href="javascript:void(0)" data-id="'+full.slug+'"><i class="fas fa-times icon_change_'+full.slug+'" title="@lang('admin_lang.status_1')"></i></a>';
                        }else{
                            a += '&nbsp; <a class="status_change" href="javascript:void(0)" data-id="'+full.slug+'"><i class="fas fa-check icon_change_'+full.slug+'" title="@lang('admin_lang.status_1')"></i></a>';
                        }
                        
                        a += '&nbsp;   <a class="delete_category" href="javascript:void(0)" data-id="'+full.slug+'"><i class="fas fa-trash" title="@lang('admin_lang.Delete')"></i></a>';
                        return a;
                    }
                },
            ]
        });
        $("#myTable_filter").hide();
        $('input.keyword').on( 'keyup click', function () {
            filterColumn( $(this).parents('div').data('column') );
        });

        $('.parent_category_id').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        });
        $('.status').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        });
        $('.add_in_menus').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        });
        $('.popular_category').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        });

        $("#input-select").bind("change",function(e){
            var parent_category_id = $.trim($("#input-select").val()),
            keyword = $.trim($("#keyword").val());
            $.ajax({
                type:"GET",
                url:"{{ route('admin.search.category') }}",
                data:{
                    parent_category_id:parent_category_id,
                    keyword:keyword
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });
        $(document).on("blur","#keyword",function(e){
            var parent_category_id = $.trim($("#input-select").val()),
            keyword = $.trim($("#keyword").val());
            $.ajax({
                type:"GET",
                url:"{{ route('admin.search.category') }}",
                data:{
                    parent_category_id:parent_category_id,
                    keyword:keyword
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });
        $(document).on("blur","#input-select",function(e){
            var parent_category_id = $.trim($("#input-select").val()),
            keyword = $.trim($("#keyword").val());
            $.ajax({
                type:"GET",
                url:"{{ route('admin.search.category') }}",
                data:{
                    parent_category_id:parent_category_id,
                    keyword:keyword
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });
        $(document).on("click",".status_change",function(e){
            var slug = $(e.currentTarget).attr("data-id");
            if(confirm("@lang('admin_lang.do_u_change_status_category')")){
                $.ajax({
                    url:"{{ route('admin.update.status.category') }}",
                    type:"GET",
                    data:{
                        slug:slug
                    },
                    success:function(resp){
                        if(resp != null){
                            if(resp == 0 ){
                                $(".error_msg_div").show();
                                $(".success_msg_div").hide();
                                $(".error_msg").html("Category status is not changed ! category is associated with product!");
                            }else{
                                var st="";
                                if(resp.status == 'A'){
                                    st="Active";
                                    $(".success_msg").html("@lang('admin_lang.suc_st_cat_chn_active')");
                                    $(".icon_change_"+slug).removeClass("fas fa-check");
                                    $(".icon_change_"+slug).addClass("fas fa-times");
                                    $(".icon_change_"+slug).attr("title","@lang('admin_lang.Inactive')");
                                }else if(resp.status == 'I'){
                                    st="Inactive";
                                    $(".success_msg").html("@lang('admin_lang.suc_st_cat_chng')");
                                    $(".icon_change_"+slug).removeClass("fas fa-times");
                                    $(".icon_change_"+slug).addClass("fas fa-check");
                                    $(".icon_change_"+slug).attr("title","@lang('admin_lang.Active')");
                                }
                                $(".status_"+slug).text(st);
                                $(".success_msg_div").show();
                                $(".error_msg_div").hide();
                            }
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("Category status is not changed ! category is associated with product!");
                        }
                    }
                });
            }
            // }
        });
        $(document).on("click",".add_in_menu",function(e){
            var slug = $(e.currentTarget).attr("data-id");
            if(confirm("@lang('admin_lang.do_u_add_remove')")){
                $.ajax({
                    url:"{{ route('admin.update.add.in.menu') }}",
                    type:"GET",
                    data:{
                        slug:slug
                    },
                    success:function(resp){
                        if(resp != 0){
                            var st="";
                            if(resp.add_in_menu == 'Y'){
                                st="@lang('admin_lang.yes')";
                                $(".success_msg").html("@lang('admin_lang.suc_cat_added_in_menu')");
                                $(".add_m_"+slug).addClass("pinkColor");
                                $(".add_m_"+slug).attr("title","@lang('admin_lang.no')");

                            }else if(resp.add_in_menu == 'N'){
                                st="@lang('admin_lang.no')";
                                $(".success_msg").html("@lang('admin_lang.suc_cat_suce')");
                                $(".add_m_"+slug).removeClass("pinkColor");
                                $(".add_m_"+slug).attr("title","@lang('admin_lang.yes')");
                            }
                            $(".add_in_menu_"+slug).text(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.err_unauth_acc')");
                        }
                    }
                });
            }
            // }
        });
        $(document).on("click",".popular_menu",function(e){
            var slug = $(e.currentTarget).attr("data-id");
            if(confirm("@lang('admin_lang.do_u_popular_cat_non_cat')")){
                $.ajax({
                    url:"{{ route('admin.update.popular.category.menu') }}",
                    type:"GET",
                    data:{
                        slug:slug
                    },
                    success:function(resp){
                        if(resp != 0){
                            var st="";
                            if(resp.is_popular == 'Y'){
                                st="Yes";
                                $(".success_msg").html("@lang('admin_lang.suc_cat_popular')");
                                $(".pop_icon_"+slug).addClass("pinkColor");
                                $(".pop_icon_"+slug).attr("title","@lang('admin_lang.Inactive')");
                            }else if(resp.is_popular == 'N'){
                                st="No";
                                $(".success_msg").html("@lang('admin_lang.suc_cat_non_popular')");
                                $(".pop_icon_"+slug).removeClass("pinkColor");
                                $(".pop_icon_"+slug).attr("title","@lang('admin_lang.Active')");
                            }
                            $(".popular_category_"+slug).text(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.un_au_acc_12')");
                        }
                    }
                });
            }
            // }
        });
        $(document).on("click",".delete_category",function(e){
            var slug = $(e.currentTarget).attr("data-id"),
            obj = $(this),
            remainInfo = "";
            if(confirm("@lang('admin_lang.do_u_want_del_cat')")){
                $.ajax({
                    url:"{{ route('admin.category.delete.permanently') }}",
                    type:"GET",
                    data:{
                        slug:slug
                    },
                    success:function(resp){
                        console.log(JSON.stringify(resp));
                        if(resp.result == 1){
                            $(".success_msg").html("@lang('admin_lang.category_delete_success')");
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            obj.parent().parent().remove();
                            
                        }else if(resp.result == 0){
                            // $(".error_msg_div").show();
                            // $(".success_msg_div").hide();
                            // if(resp.cat_type == "P"){
                            //     remainInfo = remainInfo + "@lang('admin_lang.this_subcat_asso_brand') "+resp.product_categories_count+" . ";
                            // }else if(resp.cat_type == "S"){
                            //     remainInfo = remainInfo + "@lang('admin_lang.this_subcat_asso_pr_opt') "+resp.product_categories_count+" . ";
                            //     remainInfo = remainInfo + "@lang('admin_lang.this_cat_asso_pr_opt') "+resp.variant_count + " . ";
                            // }
                            // $(".error_msg").html(remainInfo);
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("This category cannot be deleted because if any category have the product then category can not be deleted  ");
                        }else if(resp.result == 500){
                            // alert("3");
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.unau_acc_category_delete_success')");
                        }
                        else{
                            // alert("4");
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("This category cannot be deleted because if any category have the product then category can not be deleted  ");
                        }
                        $(".session_success_div").hide();
                        $(".session_error_div").hide();
                    }
                });
            }
            // }
        });
        
    });
</script>
@endsection