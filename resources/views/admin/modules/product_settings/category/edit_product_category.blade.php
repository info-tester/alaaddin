@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Edit Category') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.edit_category')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="dashboard-wrapper">
<div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
        <!-- ============================================================== -->
        <!-- pageheader  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">@lang('admin_lang.E-commerce Dashboard Template') </h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                        <a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">
                                            @lang('admin_lang.Dashboard')
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item" aria-current="page">
                                        <a href="{{ route('admin.list.category') }}" class="breadcrumb-link">
                                            @lang('admin_lang.ManageCategory')
                                        </a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit Product Category</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session()->has('success'))
        <div class="alert alert-success vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
        </div>
        @elseif ((session()->has('error')))
        <div class="alert alert-danger vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
        </div>
        @endif
        <div class="ecommerce-widget">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">
                            @lang('admin_lang.edit_product_category')
                            <a class="adbtn btn btn-primary" href="{{ route('admin.list.category') }}"><i class="fas fa-less-than"></i> @lang('admin_lang.back')</a>
                        </h5>
                        <div class="card-body">
                            <form id="addProductCategory" method="post" action="{{ route('admin.edit.category',$cat_dtls->slug) }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    @if(@$cat_dtls->parent_id != 0)
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="input-select" class="col-form-label">@lang('admin_lang.parent_category')</label>
                                        <select class="form-control" id="input-select" name="category" disabled readonly>
                                            <option value="0" @if(@$cat_dtls->parent_id == 0) selected @endif>@lang('admin_lang.select_category')</option>
                                            @if(@$category)
                                            @foreach(@$category as $category)
                                            <option value="{{ @$category->id }}" @if(@$cat_dtls->parent_id == @$category->id) selected @endif>{{ @$category->categoryDetailsByLanguage[0]->title }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <span class="errorCategory" style="color: red;"></span>
                                    </div>
                                    @endif
                                    @if(@$cat_dtls)
                                    @foreach(@$cat_dtls->details as $key=>$lang)
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="categoryName" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.CategoryName')</label>
                                        <input id="categoryName" name="categoryName[]" type="text" class="form-control categoryName" data-lid="" placeholder="Category Name " value="{{ $lang->title }}">
                                        <span class="errorCategoryName" style="color: red;"></span>
                                    </div>
                                    @endforeach
                                    @endif
                                    @if(@$cat_dtls)
                                    @foreach(@$cat_dtls->details as $key=>$lang)
                                    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <label for=""><span style="color: red;">*</span>@lang('admin_lang.description') </label>
                                        <textarea id="desc" name="desc[]"  data-lid="" class="form-control desc" rows="3">{!! strip_tags($lang->description) !!}</textarea>
                                        <span class="errorDesc" style="color: red;"></span>
                                    </div>
                                    @endforeach
                                    @endif
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <!-- <p for="" class="col-form-label">Category Picture</p> -->
                                        <input type="file" class="custom-file-input inpt" id="customFile" name="categoryPicture" accept="image/jpg,image/jpeg,image/png">
                                        <label class="custom-file-label extrlft" for="customFile">@lang('admin_lang.upload_category_picture') </label>@lang('admin_lang.recommended_size_100')
                                        <div class="errorpic" style="color: red;">
                                        </div>
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="display: block;" style="width: 100px;height: 100px;position: relative;">
                                        <span class="after-upload">
                                            <div class="uploadServicePicDiv">
                                                @if(@$cat_dtls->picture)
                                                @php
                                                $image_path = 'storage/app/public/category_pics/'.@$cat_dtls->picture; 
                                                @endphp
                                                @if(file_exists(@$image_path))
                                                <img id="profilePicture" src="{{ URL::to('storage/app/public/category_pics/'.@$cat_dtls->picture) }}" alt="" style="">
                                                @else
                                                <img id="profilePicture" src="#" alt="" style="display: none;">
                                                @endif
                                                @else
                                                <img id="profilePicture" src="#" alt="" style="display: none;">
                                                @endif
                                            </div>
                                        </span>
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <label for="inputPassword" class="col-form-label"></label>
                                        <a href="javascript:void(0)" id="saveCategory" class="btn btn-primary ">@lang('admin_lang.Save')</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- footer -->
<!-- ============================================================== -->
@include('admin.includes.footer')
<!-- ============================================================== -->
<!-- end footer -->
<!-- ============================================================== -->
</div>
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script type="text/javascript">
    $(document).ready(function(){
    
        $("#customFile").change(function() {
        var filename = $.trim($("#customFile").val()),
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".errorpic").text("");
                readURL(this);
            }else{
    
                $(".errorpic").text("@lang('admin_lang.pl_up_img_ext_ty_jpeg')");
                $('#profilePicture').attr('src', "");
                $('#profilePicture').hide();
    
            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                  $('#profilePicture').attr('src', e.target.result);
                  $('#profilePicture').show();
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#saveCategory").click(function(e){
            var len = $(".categoryName").length,
            errorCount = 0;
            for(var i=0; i<len; i++){
               var cname = $.trim($(".categoryName").eq(i).val()),
               desc = $.trim($(".desc").eq(i).val());
               if(cname == ""){
                errorCount++;
                    $(".errorCategoryName").eq(i).text("@lang('validation.required')"); 
               }
               if(desc == ""){
                errorCount++;
                    $(".errorDesc").eq(i).text("@lang('validation.required')");
               }
            }
            var filename = $.trim($("#customFile").val());
            if(filename != ""){
                var filename_arr = filename.split("."),
                    ext = filename_arr[1];
                    ext = ext.toLowerCase();
                if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                    // $(".errorpic").text("");
                    // readURL(this);
                }else{
                    errorCount++;
                    $(".errorpic").text("@lang('admin_lang.pl_up_img_ext_ty_jpeg')");
                    $('#profilePicture').attr('src', "");
                    $('#profilePicture').hide();
        
                } 
            }
            if(errorCount == 0){
                $("#addProductCategory").submit();
            }
        });
    });
</script>
@endsection