@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | List Of Category') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.List_Of_Category')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
{{-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> --}}
{{-- <link rel="stylesheet" href="{{ URL::to('public/admin/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
<link href="{{ URL::to('public/admin/assets/vendor/fonts/circular-std/style.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::to('public/admin/assets/libs/css/style.css') }}">
<link rel="stylesheet" href="{{ URL::to('public/admin/assets/vendor/fonts/fontawesome/css/fontawesome-all.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::to('public/admin/assets/vendor/datatables/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::to('public/admin/assets/vendor/datatables/css/buttons.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::to('public/admin/assets/vendor/datatables/css/select.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::to('public/admin/assets/vendor/datatables/css/fixedHeader.bootstrap4.css') }}"> --}}
<style type="text/css">
    .pinkColor{
        color:#e83e8c;
    }
</style>
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<style type="text/css">
    #myTable_filter{
        display: none;
    }
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
</style>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">@lang('admin_lang.ManageCategory')</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">
                                @lang('admin_lang.Dashboard')
                                </a>
                            </li>
                            {{--<li class="breadcrumb-item"><a href="{{ route('admin.add.category') }}" class="breadcrumb-link">@lang('admin_lang.AddProductCategory')</a></li>--}}
                            <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.ManageCategory')</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (session()->has('success'))
    <div class="alert alert-success vd_hidden session_success_div" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
    </div>
    @elseif ((session()->has('error')))
    <div class="alert alert-danger vd_hidden session_error_div" style="display: block;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
    </div>
    @endif
    {{-- @if (session()->has('success')) --}}
    <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
        <strong class="success_msg"></strong>  
    </div>
    {{-- @elseif ((session()->has('error'))) --}}
    <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
        <a class="close" data-dismiss="alert" aria-hidden="true">
        <i class="icon-cross"></i>
        </a>
        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
        <strong class="error_msg"></strong> 
    </div>
    {{-- @endif --}}
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- ============================================================== -->
        <!-- basic table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">@lang('admin_lang.ManageCategory') 
                    <a class="adbtn btn btn-primary" href="{{ route('admin.add.category') }}"><i class="fas fa-plus"></i> @lang('admin_lang.Add')</a>
                </h5>
                <div class="card-body">
                    <form id="searchListForm" method="post" action="{{ route('admin.list.category') }}">
                        @csrf
                        <div class="row">
                            <div data-column="1" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <label  class="col-form-label">
                                @lang('admin_lang.Keywords')
                                </label>
                                <input id="col1_filter" type="text" name="keyword" class="form-control keyword" placeholder="Keywords" value="{{ $key['keyword'] }}">
                            </div>
                            {{-- <div data-column="2" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <label   class="col-form-label">
                                @lang('admin_lang.parent_category')
                                </label>
                                <select class="form-control parent_category_id" id="col2_filter" name="parent_category_id">
                                    <option value="">
                                        @lang('admin_lang.select_category')
                                    </option>
                                    @if(@$categories)
                                    @foreach(@$categories as $category)
                                    <option value="{{ @$category->id }}" @if(@$key['parent_category_id'] == @$category->id) selected @endif>{{ @$category->categoryDetailsByLanguage[0]->title }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div> --}}
                            <div data-column="3" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <label   class="col-form-label">
                                Status
                                </label>
                                <select class="form-control status" id="col3_filter" name="status">
                                    <option value="">
                                        @lang('admin_lang.select_status')
                                    </option>
                                    <option value="A">@lang('admin_lang.Active')</option>
                                    <option value="I">@lang('admin_lang.Inactive')</option>
                                    
                                </select>
                            </div>
                            {{-- <div data-column="4" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <label   class="col-form-label">
                                @lang('admin_lang.add_in_menu')
                                </label>
                                <select class="form-control add_in_menus" id="col4_filter" name="add_in_menus">
                                    <option value="">
                                        @lang('admin_lang.select_status')
                                    </option>
                                    <option value="Y">@lang('admin_lang.yes')</option>
                                    <option value="N">@lang('admin_lang.no')</option>
                                </select>
                            </div> --}}
                            {{-- <div data-column="5" class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <label   class="col-form-label">
                                @lang('admin_lang.popular_category')
                                </label>
                                <select class="form-control popular_category" id="col5_filter" name="popular_category">
                                    <option value="">
                                        @lang('admin_lang.select')
                                    </option>
                                    <option value="Y">@lang('admin_lang.yes')</option>
                                    <option value="N">@lang('admin_lang.no')</option>
                                </select>
                            </div> --}}
                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                {{-- <label for="inputPassword" class="col-form-label"></label> --}}
                                <a href="javascript:void(0)" id="searchList" class="btn btn-primary fstbtncls">
                                @lang('admin_lang.Search')
                                </a>
                                <input type="reset" value="Reset Search" class="btn btn-default fstbtncls reset_search" style="margin-left: 25px;">
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive listBody">
                        <table class="table table-striped table-bordered first" id="myTable">
                            <thead>
                                <tr>
                                    <th>@lang('admin_lang.Image')</th>
                                    <th>@lang('admin_lang.Category')</th>
                                    {{-- <th>@lang('admin_lang.ParentCategories')</th> --}}
                                    <th>@lang('admin_lang.Status')</th>
                                    {{-- <th>@lang('admin_lang.add_in_menu')</th> --}}
                                    {{-- <th>@lang('admin_lang.popular_category')</th> --}}
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </thead>
                            
                            <tfoot>
                                <tr>
                                    <th>@lang('admin_lang.Image')</th>
                                    <th>@lang('admin_lang.Category')</th>
                                    {{-- <th>@lang('admin_lang.ParentCategories')</th> --}}
                                    <th>@lang('admin_lang.Status')</th>
                                    {{-- <th>@lang('admin_lang.add_in_menu')</th> --}}
                                    {{-- <th>@lang('admin_lang.popular_category')</th> --}}
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end basic table  -->
        <!-- ============================================================== -->
    </div>
</div>
<!-- footer -->
<!-- ============================================================== -->
@include('admin.includes.footer')
<!-- ============================================================== -->
<!-- end footer -->
<!-- ============================================================== -->
<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>
@endsection            
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
{{-- <script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script> --}}
{{-- <script type="text/javascript" src="jquery.dataTables.js"></script> --}}
<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click', '.reset_search', function() {
            $('#myTable').DataTable().search('').columns().search('').draw();
        });
        function filterColumn ( i ) {
        // console.log(i, $('#col'+i+'_filter').val())
        $('#myTable').DataTable().column( i ).search(
            $('#col'+i+'_filter').val(),
            ).draw();
        }
        // $('#mytable').DataTable();
        $('#myTable').DataTable({
            stateSave: false,
            "order": [
                [1, "desc"]
            ],
            "stateLoadParams": function (settings, data) {
                // alert(JSON.stringify(data));
                // console.log('stateLoadParams', data);
                $('#col1_filter').val(data.search.category);
                $('#col2_filter').val(data.search.subcategory);
                $('#col3_filter').val(data.search.status);
                $('#col4_filter').val(data.search.add_in_menu);
                $('#col5_filter').val(data.search.is_popular);
                
            },
            stateSaveParams: function (settings, data) {
                // console.log('stateSaveParams', data)
                data.search.category = $('#col1_filter').val();
                data.search.subcategory = $('#col2_filter').val();
                data.search.status = $('#col3_filter').val();
                data.search.add_in_menu = $('#col4_filter').val();
                data.search.is_popular = $('#col5_filter').val();
                
            },
            "processing": true,
            "serverSide": true,
            'serverMethod':'post',
            "ajax": {
                "url": "{{ route('admin.load.category.list') }}",
                "data": function (d) {
                    console.log(d);
                    d.myKey = "myValue";
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
                {
                    data: 'picture',
                    render:function(data, type, full, meta){
                        if(data){
                            return '<img width="100" height="70" src="storage/app/public/category_pics/'+data+'"/ alt="user" class="rounded" width="45">';
                        }else{
                            return " ";
                        }
                    }
                },
                {
                    data: 'category_name_by_lang.title'
                },
                // {
                //     data: "parent_category_name_by_lang.title",
                //     render:function(data, type, full, meta){
                //         if(data){
                //             return data;
                //         }else{
                //             return " ";
                //         }
                        
                //     }
                // },
                {
                    data: 'status',
                    render: function(data, type, full, meta){
                        if(data == 'A'){
                            // return "Active"
                            return "<span class='status_"+full.slug+"'>"+"@lang('admin_lang.Active')"+"</span>"
                        }else{
                            // return "Inactive"
                            return "<span class='status_"+full.slug+"'>"+"@lang('admin_lang.Inactive')"+"</span>"
                        }
                    }
                },
                // {
                //     data: 'add_in_menu',
                //     render:function(data, type, full, meta){
                //         if(data == 'Y'){
                //             // return "Yes"
                //             return "<span class='add_in_menu_"+full.slug+"'>"+"@lang('admin_lang.yes')"+"</span>"
                //         }else{
                //             // return "No"
                //             return "<span class='add_in_menu_"+full.slug+"'>"+"@lang('admin_lang.no')"+"</span>"
                //         }
                //     }
                // },
                // {
                //     data: 'is_popular',
                //     render:function(data, type, full, meta){
                //         if(data == 'Y'){
                //             // return "Yes"
                //             return "<span class='popular_category_"+full.slug+"'>"+"@lang('admin_lang.yes')"+"</span>"
                //         }else{
                //             // return "No"
                //             return "<span class='popular_category_"+full.slug+"'>"+"@lang('admin_lang.no')"+"</span>"
                //         }
                //     }
                // },
                {
                    data: 'is_popular',
                    render:function(data, slug, full, meta) {
                        var a = '';
                        a += '&nbsp; <a href="{{ url('admin/edit-category').'/' }}'+full.slug+'"><i class="fas fa-edit" title="@lang('admin_lang.Edit')"></i></a>';

                        if(full.status == 'A'){
                            a += '&nbsp; <a class="status_change" href="javascript:void(0)" data-id="'+full.slug+'"><i class="fas fa-times icon_change_'+full.slug+'" title="@lang('admin_lang.status_1')"></i></a>';
                        }else{
                            a += '&nbsp; <a class="status_change" href="javascript:void(0)" data-id="'+full.slug+'"><i class="fas fa-check icon_change_'+full.slug+'" title="@lang('admin_lang.status_1')"></i></a>';
                        }
                        if(full.parent_id == 0){
                            // a += '&nbsp; <a class="add_in_menu c_'+full.slug+'" href="javascript:void(0)" data-id="'+full.slug+'"><i class="fas fa-plus add_m_'+full.slug+'" title="@lang('admin_lang.add_in_menu')"></i></a>';
                        }
                        if(full.parent_id == 0){
                            // a += '&nbsp; <a class="popular_menu" href="javascript:void(0)" data-id="'+full.slug+'"><i class="fas fa-flag pop_icon_'+full.slug+'" title="@lang('admin_lang.popular_category')"></i></a>';
                        }
                        a += '&nbsp;   <a class="delete_category" href="javascript:void(0)" data-id="'+full.slug+'"><i class="fas fa-trash" title="@lang('admin_lang.Delete')"></i></a>';
                        return a;
                    }
                },
            ]
        });
        $('input.keyword').on( 'keyup click', function () {
            // alert($(this).parents('div').data('column'));
        filterColumn( $(this).parents('div').data('column') );
        });

        $('.parent_category_id').on( 'change', function () {

            filterColumn( $(this).parents('div').data('column') );
        });
        $('.status').on( 'change', function () {

            filterColumn( $(this).parents('div').data('column') );
        });
        $('.add_in_menus').on( 'change', function () {
            // alert($(this).parents('div').data('column'));
            filterColumn( $(this).parents('div').data('column') );
        });
        $('.popular_category').on( 'change', function () {

            filterColumn( $(this).parents('div').data('column') );
        });
        // $("#searchList").click(function(e){
        //     // $("#searchListForm").submit();
        //     var parent_category_id = $.trim($("#input-select").val()),
        //     keyword = $.trim($("#keyword").val());
        //     // $("#searchList").attr("disabled",true);
        //     // $.fn.dataTable.ext.errMode = 'none';
        //     // $('#myTable').dataTable({
        //     //     "bServerSide": true,
        //     //     ....
        //     //     "bDestroy": true
        //     // });
        //     $.ajax({
        //         type:"GET",
        //         url:"{{ route('admin.search.category') }}",
        //         data:{
        //             parent_category_id:parent_category_id,
        //             keyword:keyword
        //         },
        //         beforeSend:function(){
        //             $(".loader").show();
        //         },
        //         success:function(resp){
        //             // $('#mytable').DataTable({ 
        //             //   "destroy": true, //use for reinitialize datatable
        //             // });
        //             // $('#mytable').DataTable();
        //             $(".listBody").html(resp);
        //             $(".loader").hide();
                    
        //         }
        //     });
        //     // $("#searchList").attr("disabled",false);
        // });
        // $("#keyword").on("keyup", function(e) {
        //     var value = $(this).val().toLowerCase();
        //     $("#myTable tbody tr").filter(function() {
        //       $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        //   });
        // });
        
        $("#input-select").bind("change",function(e){
            var parent_category_id = $.trim($("#input-select").val()),
            keyword = $.trim($("#keyword").val());
            // $.fn.dataTable.ext.errMode = 'none';
            // $('#myTable').dataTable({
            //     "bServerSide": true,
            //     ....
            //     "bDestroy": true
            // });
            $.ajax({
                type:"GET",
                url:"{{ route('admin.search.category') }}",
                data:{
                    parent_category_id:parent_category_id,
                    keyword:keyword
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                    
                  //   $('#mytable').DataTable({ 
                  //     "destroy": true, //use for reinitialize datatable
                  // });
                    // $('#mytable').DataTable();
                    // if ($.fn.DataTable.isDataTable("#mytable")) {
                    //     $('#mytable').DataTable().clear().destroy();
                    // }
    
                    // $("#mytable").dataTable();
                }
            });
        });
        $(document).on("blur","#keyword",function(e){
            var parent_category_id = $.trim($("#input-select").val()),
            keyword = $.trim($("#keyword").val());
            // $.fn.dataTable.ext.errMode = 'none';
            // $('#myTable').dataTable({
            //     "bServerSide": true,
            //     ....
            //     "bDestroy": true
            // });
            $.ajax({
                type:"GET",
                url:"{{ route('admin.search.category') }}",
                data:{
                    parent_category_id:parent_category_id,
                    keyword:keyword
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                  //   $('#mytable').DataTable({ 
                  //     "destroy": true, //use for reinitialize datatable
                  // });
                    // $('#mytable').DataTable();
                    // if ($.fn.DataTable.isDataTable("#mytable")) {
                    //     $('#mytable').DataTable().clear().destroy();
                    // }
    
                    // $("#mytable").dataTable();
                }
            });
        });
        $(document).on("blur","#input-select",function(e){
            var parent_category_id = $.trim($("#input-select").val()),
            keyword = $.trim($("#keyword").val());
            // $.fn.dataTable.ext.errMode = 'none';
            // $('#myTable').dataTable({
            //     "bServerSide": true,
            //     ....
            //     "bDestroy": true
            // });
            $.ajax({
                type:"GET",
                url:"{{ route('admin.search.category') }}",
                data:{
                    parent_category_id:parent_category_id,
                    keyword:keyword
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $(".loader").hide();
                  //   $('#mytable').DataTable({ 
                  //     "destroy": true, //use for reinitialize datatable
                  // });
                    // $('#mytable').DataTable();
                    // if ($.fn.DataTable.isDataTable("#mytable")) {
                    //     $('#mytable').DataTable().clear().destroy();
                    // }
    
                    // $("#mytable").dataTable();
                }
            });
        });
        $(document).on("click",".status_change",function(e){
            var slug = $(e.currentTarget).attr("data-id");
            if(confirm("@lang('admin_lang.do_u_change_status_category')")){
                $.ajax({
                    url:"{{ route('admin.update.status.category') }}",
                    type:"GET",
                    data:{
                        slug:slug
                    },
                    success:function(resp){
                        if(resp != null){
                            var st="";
                            if(resp.status == 'A'){
                                st="Active";
                                $(".success_msg").html("@lang('admin_lang.suc_st_cat_chn_active')");
                                $(".icon_change_"+slug).removeClass("fas fa-check");
                                // $(".icon_change_"+id).removeClass("");
                                $(".icon_change_"+slug).addClass("fas fa-times");
                                $(".icon_change_"+slug).attr("title","@lang('admin_lang.Inactive')");
                                // $(".status_"+slug).text(st);
                                // $(".b_u_"+id).attr('data-status','I');
                                // $(e.currentTarget).attr('data-status','A');
                            }else if(resp.status == 'I'){
                                st="Inactive";
                                $(".success_msg").html("@lang('admin_lang.suc_st_cat_chng')");
                                $(".icon_change_"+slug).removeClass("fas fa-times");
                                $(".icon_change_"+slug).addClass("fas fa-check");
                                $(".icon_change_"+slug).attr("title","@lang('admin_lang.Active')");
                                // $(".status_"+slug).text(st);
                                // $(".b_u_"+id).attr('data-status','I');
                                // $(e.currentTarget).attr('data-status','I');
                            }
                            $(".status_"+slug).text(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.unauthh_accc_mer_changed')");
                        }
                    }
                });
            }
            // }
        });
        $(document).on("click",".add_in_menu",function(e){
            var slug = $(e.currentTarget).attr("data-id");
            if(confirm("@lang('admin_lang.do_u_add_remove')")){
                $.ajax({
                    url:"{{ route('admin.update.add.in.menu') }}",
                    type:"GET",
                    data:{
                        slug:slug
                    },
                    success:function(resp){
                        if(resp != 0){
                            var st="";
                            if(resp.add_in_menu == 'Y'){
                                st="@lang('admin_lang.yes')";
                                $(".success_msg").html("@lang('admin_lang.suc_cat_added_in_menu')");
                                // alert($(e.currentTarget).attr("class"));
                                // $(e.currentTarget).addClass("pinkColor");
                                // alert($("c_"+slug).hasClass("pinkColor"));
                                // $("c_"+slug).addClass("pinkColor");
                                // $(this).addClass("pinkColor");
                                $(".add_m_"+slug).addClass("pinkColor");
                                $(".add_m_"+slug).attr("title","@lang('admin_lang.no')");
                                // $(".status_"+slug).text(st);
                                // $(".b_u_"+id).attr('data-status','I');

                            }else if(resp.add_in_menu == 'N'){
                                st="@lang('admin_lang.no')";
                                $(".success_msg").html("@lang('admin_lang.suc_cat_suce')");
                                // $(e.currentTarget).removeClass("pinkColor");
                                $(".add_m_"+slug).removeClass("pinkColor");
                                // $(".add_m_"+slug).removeClass("fas fa-times");
                                // $(".add_m_"+slug).addClass("fas fa-check");
                                $(".add_m_"+slug).attr("title","@lang('admin_lang.yes')");

                                // $("c_"+slug).removeClass("pinkColor");
                                // $(this).removeClass("pinkColor");
                                // $(".status_"+slug).text(st);
                                // $(".b_u_"+id).attr('data-status','I');
                                // $(e.currentTarget).attr('data-status','I');
                            }
                            // alert()
                            $(".add_in_menu_"+slug).text(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.err_unauth_acc')");
                        }
                    }
                });
            }
            // }
        });
        $(document).on("click",".popular_menu",function(e){
            var slug = $(e.currentTarget).attr("data-id");
            if(confirm("@lang('admin_lang.do_u_popular_cat_non_cat')")){
                $.ajax({
                    url:"{{ route('admin.update.popular.category.menu') }}",
                    type:"GET",
                    data:{
                        slug:slug
                    },
                    success:function(resp){
                        if(resp != 0){
                            var st="";
                            if(resp.is_popular == 'Y'){
                                st="Yes";
                                $(".success_msg").html("@lang('admin_lang.suc_cat_popular')");
                                // $(".pop_icon_"+slug).removeClass("fas fa-check");
                                // $(".icon_change_"+id).removeClass("");
                                $(".pop_icon_"+slug).addClass("pinkColor");
                                $(".pop_icon_"+slug).attr("title","@lang('admin_lang.Inactive')");
                                // $(".status_"+slug).text(st);
                                // $(".b_u_"+id).attr('data-status','I');
                                // $(e.currentTarget).attr('data-status','A');
                            }else if(resp.is_popular == 'N'){
                                st="No";
                                $(".success_msg").html("@lang('admin_lang.suc_cat_non_popular')");
                                $(".pop_icon_"+slug).removeClass("pinkColor");
                                // $(".pop_icon_"+slug).addClass("fas fa-check");
                                $(".pop_icon_"+slug).attr("title","@lang('admin_lang.Active')");
                                // $(".status_"+slug).text(st);
                                // $(".b_u_"+id).attr('data-status','I');
                                // $(e.currentTarget).attr('data-status','I');
                            }
                            $(".popular_category_"+slug).text(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            
                            // $(".int_st_icon_"+id).hide();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.un_au_acc_12')");
                        }
                    }
                });
            }
            // }
        });
        $(document).on("click",".delete_category",function(e){
            var slug = $(e.currentTarget).attr("data-id"),
            obj = $(this),
            remainInfo = "";
            if(confirm("@lang('admin_lang.do_u_want_del_cat')")){
                $.ajax({
                    url:"{{ route('admin.category.delete.permanently') }}",
                    type:"GET",
                    data:{
                        slug:slug
                    },
                    success:function(resp){
                        console.log(JSON.stringify(resp));
                        if(resp.result == 1){
                            // alert("2");
                            // $(".success_msg").html("@lang('admin_lang.category_delete_success')");
                            $(".success_msg").html("@lang('admin_lang.category_delete_success')");
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            // $(".tr_"+slug).hide();
                            obj.parent().parent().remove();
                            $('#myTable').DataTable().draw();
                            // $(".int_st_icon_"+id).hide();
                        }else if(resp.result == 0){
                            // alert("1");
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            
                            if(resp.cat_type == "P"){
                                // alert(resp.brand_count);
                                // if(resp.brand_count != 0){
                                    remainInfo = remainInfo + "@lang('admin_lang.this_subcat_asso_brand') "+resp.product_categories_count+" . ";        
                                // }
                                // if(resp.variant_count != 0){
                                    // remainInfo = remainInfo + "@lang('admin_lang.this_par_cat_ass_pr_opt_val_tot')"+resp.variant_count + " . ";        
                                // }
                                // if(resp.product_categories_count != 0){
                                    // remainInfo = remainInfo + "@lang('admin_lang.this_par_cat_ass_pr_opt_val_tot_1') "+resp.product_categories_count + " . ";        
                                // }
                                // if(resp.no_sub_count != 0){
                                    // remainInfo = remainInfo + "@lang('admin_lang.this_par_cat_ass_pr_opt_val_tot_2')"+resp.no_sub_count + " . ";        
                                // }

                                // alert("1: "+resp.brand_count+"2. : "+resp.variant_count+"3. "+resp.product_categories_count+" 4. "+resp.no_sub_count);
                            }else if(resp.cat_type == "S"){
                                // alert(JSON.stringify(resp));
                                // if(resp.product_categories_count != 0){
                                    remainInfo = remainInfo + "@lang('admin_lang.this_subcat_asso_pr_opt') "+resp.product_categories_count+" . ";        
                                // }
                                // if(resp.variant_count != 0){
                                    remainInfo = remainInfo + "@lang('admin_lang.this_cat_asso_pr_opt') "+resp.variant_count + " . ";        
                                // }
                                // remainInfo = remainInfo;    
                            }
                            // alert(remainInfo);
                            $(".error_msg").html(remainInfo);
                        }else if(resp.result == 500){
                            // alert("3");
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.unau_acc_category_delete_success')");
                        }
                        else{
                            // alert("4");
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("@lang('admin_lang.unau_acc_category_delete_success')");
                        }
                        $(".session_success_div").hide();
                        $(".session_error_div").hide();
                    }
                });
            }
            // }
        });
        
    });
</script>
@endsection