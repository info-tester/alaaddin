<table class="table table-striped table-bordered first" id="myTable">
                            <thead>
                                <tr>
                                    <th>@lang('admin_lang.Image')</th>
                                    <th>@lang('admin_lang.Category')</th>
                                    <th>@lang('admin_lang.ParentCategories')</th>
                                    <th>@lang('admin_lang.Status')</th>
                                    <th>@lang('admin_lang.add_in_menu')</th>
                                    <th>@lang('admin_lang.popular_category')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $pdtls = "";
                                $check_subc = NULL;
                                @endphp
                                @foreach(@$parent_category as $c)
                                <tr class="tr_{{ @$c->slug }}">
                                    <td>
                                        <div class="m-r-10">
                                            @if(@$c->picture)
                                            @php
                                            $image_path = 'storage/app/public/category_pics/'.@$c->picture; 
                                            @endphp
                                            @if(file_exists(@$image_path))
                                            <img src="{{ URL::to('storage/app/public/category_pics/'.@$c->picture) }}" alt="user" class="rounded" width="45">
                                            @endif
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                        @if(@$c->categorySubcategoryDetails) 
                                        @foreach(@$c->categorySubcategoryDetails as $subc)
                                        {{ $subc->title }}
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if($c->parent_id != 0)
                                        @foreach(@$c->parentCategoryDetails as $dtls)
                                        @php
                                        $pdtls = $dtls->title;
                                        @endphp
                                        @endforeach
                                        {{ $pdtls }}
                                        @else
                                        @lang('admin_lang.na')
                                        @endif
                                    </td>
                                    <td class="status_{{ @$c->slug }}">
                                        @if($c->status == 'A')
                                        @lang('admin_lang.Active')
                                        @elseif($c->status == 'I')
                                        @lang('admin_lang.Inactive')
                                        @elseif($c->status == 'D')
                                        @lang('admin_lang.deleted')
                                        @endif
                                    </td>
                                    <td class="add_in_menu_{{ @$c->slug }}">
                                        @if($c->add_in_menu == 'Y')
                                        @lang('admin_lang.yes')
                                        @elseif($c->add_in_menu == 'N')
                                        @lang('admin_lang.no')
                                        @endif
                                    </td>
                                    <td class="popular_category_{{ @$c->slug }}">
                                        @if($c->is_popular == 'Y')
                                        @lang('admin_lang.yes')
                                        @elseif($c->is_popular == 'N')
                                        @lang('admin_lang.no')
                                        @endif
                                    </td>
                                    <td>
                                        {{-- edit --}}
                                        <a href="{{ route('admin.edit.category',$c->slug) }}"><i class="fas fa-edit" title='@lang('admin_lang.Edit')'></i></a>
                                        {{-- delete --}}
                                        <a class="delete_category" href="javascript:void(0)" data-id="{{ @$c->slug }}"><i class="fas fa-trash" title='@lang('admin_lang.Delete')'></i></a>

                                        {{-- status --}}
                                        @if($c->status == 'A')
                                        <a class="status_change" href="javascript:void(0)" data-id="{{ @$c->slug }}"><i class=" fas fa-times icon_change_{{ @$c->slug }}" title='@lang('admin_lang.Status')'></i></a>
                                        @elseif($c->status == 'I')
                                        <a class="status_change" href="javascript:void(0)" data-id="{{ @$c->slug }}"><i class="fas fa-check icon_change_{{ @$c->slug }}" title='@lang('admin_lang.status_1')'></i></a>
                                        @endif

                                        {{-- add in menu --}}
                                        @if($c->parent_id == 0)
                                        <a data-id="{{ @$c->slug }}" class="add_in_menu c_{{ @$c->slug }} @if($c->add_in_menu == 'Y') pinkColor  @endif" href="javascript:void(0)"><i class=" fas fa-plus add_m_{{ @$c->slug }}" title='@lang('admin_lang.add_in_menu')'></i></a>
                                        @endif

                                        {{-- popular category --}}
                                        @if($c->parent_id == 0)
                                        <a data-id="{{ @$c->slug }}" class="popular_menu p_{{ @$c->slug }} @if($c->is_popular == 'Y') pinkColor  @endif" href="javascript:void(0)"><i class=" fas fa-flag pop_icon_{{ @$c->slug }}" title='@lang('admin_lang.popular_category')'></i></a>
                                        @endif

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>@lang('admin_lang.Image')</th>
                                    <th>@lang('admin_lang.Category')</th>
                                    <th>@lang('admin_lang.ParentCategories')</th>
                                    <th>@lang('admin_lang.Status')</th>
                                    <th>@lang('admin_lang.add_in_menu')</th>
                                    <th>@lang('admin_lang.popular_category')</th>
                                    <th>@lang('admin_lang.Action')</th>
                                </tr>
                            </tfoot>
                        </table>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>