@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.add_category')
@endsection
@section('content')
@section('links')
    {{-- @include('admin.includes.links') --}}
    @if(Config::get('app.locale') == 'en')
        @include('admin.includes.links')
    @elseif(Config::get('app.locale') == 'ar')
        @include('admin.includes.arabic_links')
    @endif
@endsection
@section('header')
    @include('admin.includes.header')
@endsection
@section('sidebar')
    @include('admin.includes.sidebar')
@endsection
@section('content')
<div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title">
                                    @lang('admin_lang.E-commerceDashboardTemplate')
                                </h2>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">

                                            @lang('admin_lang.dashboard')

                                            </a></li>
                                            <li class="breadcrumb-item" aria-current="page">
                                        <a href="{{ route('admin.list.category') }}" class="breadcrumb-link">
                                            @lang('admin_lang.ManageCategory')
                                        </a>
                                    </li>
                                            <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.add_product_category')</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				            <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				    @endif
				    @if (session()->has('success'))
				    <div class="alert alert-success vd_hidden" style="display: block;">
				        <a class="close" data-dismiss="alert" aria-hidden="true">
				        <i class="icon-cross"></i>
				        </a>
				        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
				        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
				    </div>
				    @elseif ((session()->has('error')))
				    <div class="alert alert-danger vd_hidden" style="display: block;">
				        <a class="close" data-dismiss="alert" aria-hidden="true">
				        <i class="icon-cross"></i>
				        </a>
				        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
				        <strong>{{ session('error')['code']['message'] }}</strong> {{ session('error')['meaning'] }} 
				    </div>
				    @endif
					<div class="ecommerce-widget">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <h5 class="card-header">
                                        @lang('admin_lang.add_product_category')
                                	<a class="adbtn btn btn-primary" href="{{ route('admin.list.category') }}">
                                        <i class="fas fa-less-than"></i>
                                        @lang('admin_lang.back')
                                    </a>
                                    </h5>
                                    <div class="card-body">
                                        <form id="addProductCategory" method="post" action="{{ route('admin.add.category') }}" enctype="multipart/form-data">
                                        	@csrf
                                            <div class="row">
                                            {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="input-select" class="col-form-label">
                                                @lang('admin_lang.parent_category')
                                                </label>
                                                <select class="form-control" id="input-select" name="category">
                                                    <option value="0">
                                                    @lang('admin_lang.select_category')
                                                    </option>
                                                	@if(@$category)
                                                		@foreach(@$category as $category)
                                                			<option value="{{ @$category->id }}">{{ @$category->categoryDetailsByLanguage[0]->title }}</option>
                                                		@endforeach
                                                	@endif
                                                </select>
                                                <span>@lang('admin_lang.leave_this_field')</span>
                                                <span class="errorCategory" style="color: red;"></span>
                                            </div> --}}
                                            @if(@$language)
	                                            @foreach(@$language as $lang)

		                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
		                                                <label for="categoryName{{ $lang->id }}" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.CategoryName')</label>
		                                                <input id="categoryName{{ $lang->id }}" name="categoryName{{ $lang->id }}" type="text" class="form-control categoryName categoryNameBylanguage" data-lid="{{ $lang->id }}" placeholder='@lang('admin_lang.CategoryName')'>
		                                                <span class="errorCategoryName{{ $lang->id }}  errorCategoryNm" style="color: red;"></span>
		                                            </div>
	                                            @endforeach
                                            @endif
                                            @if(@$language)
	                                            @foreach(@$language as $lang)
		                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		                                                <label for=""><span style="color: red;">*</span>@lang('admin_lang.description')</label>
		                                                <textarea id="desc_{{ $lang->id }}" name="desc_{{ $lang->id }}"  data-lid="{{ $lang->id }}" class="form-control desc" rows="3"></textarea>
		                                                <span class="errorDesc{{ $lang->id }}" style="color: red;"></span>
		                                            </div>
                                            	@endforeach
                                            @endif
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <!-- <p for="" class="col-form-label">Category Picture</p> -->
                                                <input type="file" class="custom-file-input inpt" id="customFile" name="categoryPicture" accept="image/jpg,image/jpeg,image/png">
                                                <label class="custom-file-label extrlft" for="customFile">
                                                @lang('admin_lang.upload_category_picture') 
                                                </label>
                                                @lang('admin_lang.recommended_size_100')
                                                <div class="errorpic" style="color: red;">
                                                	
                                                </div>
                                            </div>
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="display: block;" style="width: 100px;height: 100px;position: relative;">
                                                <span class="after-upload">
                                                    <div class="uploadServicePicDiv">

                                                        <img id="profilePicture" alt="" style="width: 100px;height: 100px;display: none;">
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                                            <label for="inputPassword" class="col-form-label"></label>
	                                            <a href="javascript:void(0)" id="saveCategory" class="btn btn-primary ">@lang('admin_lang.save')</a>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
            <!-- ============================================================== -->
            @include('admin.includes.footer')
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
@endsection
@section('scripts')
    {{-- @include('admin.includes.scripts') --}}
    @if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){

		$("#customFile").change(function() {
		var filename = $.trim($("#customFile").val()),
			filename_arr = filename.split("."),
			ext = filename_arr[1];
			ext = ext.toLowerCase();
            $(".errorpic").text("");
            readURL(this);
    	});
    	function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function(e) {
	              $('#profilePicture').attr('src', e.target.result);
                  $('#profilePicture').show();
	            }
	            reader.readAsDataURL(input.files[0]);
	        }
    	}
    	$("#saveCategory").click(function(e){

    		var category = $.trim($("#input-select").val()),
    		categoryNameLength = $(".categoryName").length,
    		categoryDescLength = $(".desc").length,
    		errorCount = 0;

    		for(var i=0;i<categoryNameLength;i++){
    			var title = $.trim($(".categoryName").eq(i).val());
    			if(title == ""){
    				errorCount++;
    				$(".errorCategoryName"+$(".categoryName").eq(i).attr("data-lid")).text('@lang('validation.required')');
    			}else{
    				$(".errorCategoryName"+$(".categoryName").eq(i).attr("data-lid")).text("");
    			}
    		}

    		for(var i=0;i<categoryDescLength;i++){
    			var title = $.trim($(".desc").eq(i).val());
    			if(title == ""){
    				errorCount++;
    				$(".errorDesc"+$(".desc").eq(i).attr("data-lid")).text('@lang('validation.required')');
    			}else{
    				$(".errorDesc"+$(".desc").eq(i).attr("data-lid")).text("");
    			}
    		}
    		var filename = $.trim($("#customFile").val());
    		if(filename == ""){

    		}else{
    			var filename_arr = filename.split("."),
				ext = filename_arr[1];
				ext = ext.toLowerCase();
    		}
    		if(errorCount == 0){
    			$("#addProductCategory").submit();
    		}
    	});
        $(document).on("blur",".categoryNameBylanguage",function(e){
            var name = $.trim($(e.currentTarget).val()),
            rurl = "{{ route('admin.check.category.name') }}";
            $.ajax({
                type:"GET",
                url:rurl,
                data:{
                   name:name,
                   cat_id: $('#input-select').val()
                },
                success:function(resp){
                    if(resp == 0){
                        $(e.currentTarget).parent().find(".errorCategoryNm").text("@lang('admin_lang.cat_nam_already_exist')");
                        $(e.currentTarget).val("");
                    }else{
                        $(".errorCategoryNm").text("");
                    }
                }
            });
        });
		
	});
</script>
@endsection
