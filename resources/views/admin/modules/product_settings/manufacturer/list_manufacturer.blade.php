@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | List Of Manufacturer') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.list_of_manufacturer')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style type="text/css">
    #brands_filter{
        display: none;
    }

    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">@lang('admin_lang.ManageManufacturer')</h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                        <li class="breadcrumb-item"><a href="{{ route('admin.add.manufacturer') }}" class="breadcrumb-link">@lang('admin_lang.AddManufacturer')</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.ManageManufacturer')</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (session()->has('success'))
                <div class="alert alert-success vd_hidden session_success_div" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                </div>
                @elseif ((session()->has('error')))
                <div class="alert alert-danger vd_hidden session_error_div" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
                </div>
                @endif
                <div class="alert alert-success vd_hidden session_success_div" style="display: none;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong class="success_msg">{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                </div>
                <div class="alert alert-danger vd_hidden session_error_div" style="display: none;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong class="error_msg">{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">
                                @lang('admin_lang.ManageManufacturer')
                                <a class="adbtn btn btn-primary" href="{{ route('admin.add.manufacturer') }}"><i class="fas fa-plus"></i> @lang('admin_lang.Add')</a></h5>
                            <div class="card-body">

                                <form id="searchBrandForm" method="post" action="{{ route('admin.list.manufacturer') }}">
                                    @csrf
                                    <div class="row">
                                        <!-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="inputText3" class="col-form-label">Keywords </label>
                                            <input id="inputText3" type="text" class="form-control" placeholder="Keywords">
                                        </div> -->
                                            
                                        <div data-column="1" class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="inputText3" class="col-form-label">@lang('admin_lang.Category')</label>
                                            <select id="col1_filter" class="form-control category" name="category">
                                                <option value="">
                                                @lang('admin_lang.select_category')
                                                </option>
                                                @if(@$category)
                                                    @foreach(@$category as $category)
                                                        <option value="{{ @$category->id }}"@if(@$key['category'] == @$category->id)
                                                            selected
                                                        @endif>{{ @$category->categoryDetailsByLanguage[0]->title }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>

                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            
                                            <a href="javascript:void(0)" id="searchBrands" class="btn btn-primary fstbtncls">
                                            @lang('admin_lang.Search')
                                            </a>

                                            <input type="reset" value="@lang('admin_lang.reset_search')" class="btn btn-default fstbtncls reset_search" style="margin-left: 25px;">
                                        </div>
                                    </div>
                                </form>

                                <div class="table-responsive listBody">
                                    <table class="table table-striped table-bordered first" id="brands">
                                        <thead>
                                            <tr>
                                                <th>@lang('admin_lang.Logo')</th>
                                                <th>@lang('admin_lang.Category')</th>
                                                <th>@lang('admin_lang.Brand')</th>
                                                <th>@lang('admin_lang.Status')</th>
                                                <th>@lang('admin_lang.Action')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>@lang('admin_lang.Logo')</th>
                                                <th>@lang('admin_lang.Category')</th>
                                                <th>@lang('admin_lang.Brand')</th>
                                                <th>@lang('admin_lang.Status')</th>
                                                <th>@lang('admin_lang.Action')</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
            </div>
<!-- footer -->
<!-- ============================================================== -->
@include('admin.includes.footer')
<!-- ============================================================== -->
<!-- end footer -->
<!-- ============================================================== -->
<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>
@endsection            
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('body').on('click', '.reset_search', function() {
            $('#brands').DataTable().search('').columns().search('').draw();
        })

        function filterColumn ( i ) {
            $('#brands').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }

        $('#brands').DataTable( {
            stateSave: true,
            "order": [[1, "asc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
                $('#col2_filter').val(data.search.status)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
                data.search.status = $('#col2_filter').val()
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('admin.list.manufacturer') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
                { 
                    data: 'logo',
                    render: function(data, type, full) {
                        if(data) {
                           return '<img width="100" height="70" src="storage/app/public/brand_logo/'+data+'" />';
                        } else {
                            return '<img width="100" height="70" src="public/frontend/images/default_product.png" />';
                        }
                    }
                },
                {   
                    data: '',
                    render: function(data, type, full) {
                        return full.category_by_brand.title
                    }
                },
                {   
                    data: 'name',
                    render: function(data, type, full) {
                        return full.brand_details_by_language.title
                    }
                },
                {   
                    data: 'status',
                    render: function(data, type, full) {
                        if(data == 'A') {
                            return '<span class="status_'+full.id+'">@lang('admin_lang.Active')</span>'
                        } if(data == 'I') {
                            return '<span class="status_'+full.id+'">@lang('admin_lang.Inactive')</span>'
                        }
                    }
                },
                {   
                    data: 'status',
                    render: function(data, type, full) {
                        var a = '';
                        a += '<a href="{{ url('admin/edit-manufacturer') }}/'+full.id+'"><i class="fas fa-edit"></i></a>'

                        a += ' <a class="delete_manufacturer" href="javascript:void(0)" data-id="'+full.id+'"><i class=" fas fa-trash"></i></a>'
                        return a;
                    }
                }
            ]
        });

        // change event
        $('.category').on( 'change', function () {
            filterColumn( 1 );
        });

        /*$("#searchBrands").click(function(e){
            var category = $.trim($("#input-select").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.manufacturer') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    category:category
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    
                    $(".listBody").html(resp);
                    $(".loader").hide();
                    // $("#searchProductOptions").attr("disabled",false);
                }
            });
        });

        $(document).on("change","#input-select",function(e){
            var category = $.trim($("#input-select").val());
            $.ajax({
                type:"POST",
                url:"{{ route('admin.list.manufacturer') }}",
                data:{
                    _token: '{{ csrf_token() }}',
                    category:category
                },
                beforeSend:function(){
                    $(".loader").show();
                },
                success:function(resp){
                    
                    $(".listBody").html(resp);
                    $(".loader").hide();
                }
            });
        });*/
        $(document).on("click",".delete_manufacturer",function(e){
            var id = $(e.currentTarget).attr("data-id"),
            obj = $(this);
            if(confirm("Do you want to delete this manufacturer ?")){
                $.ajax({
                    url:"{{ route('admin.delete.manufacturer.details.permanently') }}",
                    type:"GET",
                    data:{
                        id:id
                    },
                    success:function(resp){
                        if(resp.result == 1){
                            $(".success_msg").html("Success!Manufacturer deleted successfully!");
                            // $(".status_"+id).html(st);
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".session_success_div").show();
                            $(".session_error_div").hide();
                            // $(".tr_"+id).hide();
                            // $(".int_st_icon_"+id).hide();
                            obj.parent().parent().remove();
                        }else{
                            $(".error_msg_div").show();
                            $(".session_error_div").show();
                            $(".session_success_div").hide();
                            $(".success_msg_div").hide();
                            $(".error_msg").html("Error!Manufacturer is not deleted because it is asscociated with product or order!");
                        }
                        // $(".session_success_div").hide();
                        // $(".session_error_div").hide();
                    }
                });
            }
            // }
        });
    });
</script>
@endsection