@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Edit Manufacturer') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.edit_manufacturer')
@endsection
@section('content')
@section('links')
    {{-- @include('admin.includes.links') --}}
    @if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
    @include('admin.includes.header')
@endsection
@section('sidebar')
    @include('admin.includes.sidebar')
@endsection
@section('content')
<!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title">@lang('admin_lang.E-commerceDashboard') 
                                </h2>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.EditManufacturer')</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session()->has('success'))
                    <div class="alert alert-success vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                    </div>
                    @elseif ((session()->has('error')))
                    <div class="alert alert-danger vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong>{{ session('error')['code']['message'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    @endif
                    <!-- ============================================================== -->
                    <!-- end pageheader  -->
                    <!-- ============================================================== -->
                    <div class="ecommerce-widget">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <h5 class="card-header">@lang('admin_lang.edit_manufacturer')
                                        <a class="adbtn btn btn-primary" href="{{ route('admin.list.manufacturer') }}">@lang('admin_lang.back')</a>
                                    </h5>
                                    <div class="card-body">
                                        <form id="addManufacturerForm" method="post" enctype="multipart/form-data" action="{{ route('admin.edit.manufacturer',$brand->id) }}">
                                            @csrf
                                            <div class="row">

                                             <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="input-select" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.Category')</label>
                                                <select class="form-control" id="input-select" name="category" readonly disabled>
                                                    <option value="">
                                                    @lang('admin_lang.select_ategory')
                                                    </option>
                                                    @if(@$category)
                                                        @foreach(@$category as $category)
                                                            <option value="{{ @$category->id }}"
                                                                @if(@$brand->category_id == @$category->id)
                                                                    selected
                                                                @endif>{{ @$category->categoryDetailsByLanguage[0]->title }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <span class="errorCategory" style="color: red;"></span>
                                            </div>   
                                            @if(@$language)
                                                @foreach(@$language as $key=>$lang)
                                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                    <label for="inputText3" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.BrandName') [{{ $lang->name }}]</label>
                                                    <input id="inputText{{ $language[$key]->id }}" type="text" class="form-control brandName" placeholder="Brand Name" name="brand_name[]" value="{{ $brand->brandDetails[$key]->title }}">
                                                    <span class="errorBrandName" style="color: red;"></span>
                                                </div>
                                                @endforeach
                                            @endif
                                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 fstbtncls">
                                                <!-- <p for="" class="col-form-label">Category Picture</p> -->
                                                <input type="file" class="custom-file-input" id="customFile" name="brand_logo" accept="image/jpg,image/jpeg,image/png">
                                                <label class="custom-file-label extrlft" for="customFile">@lang('admin_lang.UploadBrandLogo') </label>
                                                @lang('admin_lang.recommended_size_100')
                                                <div class="errorpic" style="color: red;">
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="display: block;" style="width: 100px;height: 100px;position: relative;">
                                                <span class="after-upload">
                                                    <div class="uploadServicePicDiv">
                                                        @if(@$brand->logo)
                                                        @php
                                                        $image_path = 'storage/app/public/brand_logo/'.@$brand->logo; 
                                                        @endphp
                                                        @if(file_exists(@$image_path))
                                                        <img id="profilePicture" src="{{ URL::to('storage/app/public/brand_logo/'.@$brand->logo) }}" alt="" style="width: 100px;height: 100px;">
                                                        @endif
                                                        @endif
                                                        
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label for="inputPassword" class="col-form-label"></label>
                                                <a href="javascript:void(0)" id="saveBrandDetails" class="btn btn-primary">
                                                @lang('admin_lang.Save')
                                                </a>
                                            </div>
                       
                                        </div>
                                        </form>
                                    </div>
                                    <!-- <div class="card-body border-top">
                                        <h3>Sizing</h3>
                                        <form>
                                            <div class="form-group">
                                                <label for="inputSmall" class="col-form-label">Small</label>
                                                <input id="inputSmall" type="text" value=".form-control-sm" class="form-control form-control-sm">
                                            </div>
                                            <div class="form-group">
                                                <label for="inputDefault" class="col-form-label">Default</label>
                                                <input id="inputDefault" type="text" value="Default input" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="inputLarge" class="col-form-label">Large</label>
                                                <input id="inputLarge" type="text" value=".form-control-lg" class="form-control form-control-lg">
                                            </div>
                                        </form>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
            <!-- ============================================================== -->
            @include('admin.includes.footer')
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
@endsection
@section('scripts')
    {{-- @include('admin.includes.scripts') --}}
    @if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script type="text/javascript">
    $(document).ready(function(){

        $("#customFile").change(function() {
            var filename = $.trim($("#customFile").val()),
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".errorpic").text("");
                readURL(this);
            }else{

                $(".errorpic").text('@lang('validation.image_extension_type')');
                $('#profilePicture').attr('src', "");

            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                  $('#profilePicture').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#saveBrandDetails").click(function(e){
            var len = $(".brandName").length,
            error = 0,
            category = $.trim($("#input-select").val()),
            brandLogo = $.trim($("#customFile").val());
            if(brandLogo != ""){
                var filename_arr = brandLogo.split("."),
                ext = filename_arr[1];
                ext = ext.toLowerCase();
                if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                }else{
                    error++;
                    $(".errorpic").text('@lang('validation.image_extension_type')');
                    $('#profilePicture').attr('src', "");

                }
            }else{
                // error++;
                // $(".errorpic").text('@lang('validation.image_extension_type')');
                    // $('#profilePicture').attr('src', "");
            }
            // category = $.trim($("#input-select").val())
            for(var i=0;i<len;i++){
                var brandname = $.trim($(".brandName").eq(i).val());
                if(brandname == ""){
                    error++;
                    $(".errorBrandName").eq(i).text('This field is required');
                }
            }
            if(category == ""){
                error++;
                $(".errorCategory").text('This field is required');
            }
            
            if(error == 0){
                $("#addManufacturerForm").submit();
            }
        });
    });
</script>
@endsection
