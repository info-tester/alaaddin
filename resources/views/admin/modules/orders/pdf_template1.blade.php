@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | View Order Details') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.v_order_dtls')
@endsection
@section('links')
{{-- @include('admin.includes.links') --}}
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    /* The container */
    .container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 15px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-family: 'Circular Std Book';
    font-style: normal;
    font-weight: normal;
    }
    /* Hide the browser's default radio button */
    .container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    line-height: 1.42857143;
    }
    /* Create a custom radio button */
    .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
    border-radius: 50%;
    }
    /* On mouse-over, add a grey background color */
    .container:hover input ~ .checkmark {
    background-color: #ccc;
    }
    /* When the radio button is checked, add a blue background */
    .container input:checked ~ .checkmark {
    background-color: #2196F3;
    }
    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
    content: "";
    position: absolute;
    display: none;
    }
    /* Show the indicator (dot/circle) when checked */
    .container input:checked ~ .checkmark:after {
    display: block;
    }
    /* Style the indicator (dot/circle) */
    .container .checkmark:after {
    top: 9px;
    left: 9px;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background: white;
    }
</style>
{{-- 
<link rel="stylesheet" href="{{ asset('public/admin/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
--}}
{{-- 
<link href="{{ asset('public/admin/assets/vendor/fonts/circular-std/style.css') }}" rel="stylesheet">
--}}
{{-- 
<link rel="stylesheet" href="{{ asset('public/admin/assets/libs/css/style.css') }}">
--}}
{{-- 
<link rel="stylesheet" href="{{ asset('public/admin/assets/vendor/fonts/fontawesome/css/fontawesome-all.css') }}">
--}}
{{-- 
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/dataTables.bootstrap4.css') }}">
--}}
{{-- 
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/buttons.bootstrap4.css') }}">
--}}
{{-- 
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/select.bootstrap4.css') }}">
--}}
{{-- 
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/fixedHeader.bootstrap4.css') }}">
--}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')

@endsection
@section('sidebar')

@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper" style="margin-left: 0px;    margin-right: 0px;">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== --> 
            <!-- pageheader --> 
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.view_order_details')</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <!-- <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li> -->
                                    <!-- <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">@lang('admin_lang.edit_view_order_dtls')</a></li> -->
                                    <!-- <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.edit_view_order_dtls')</li> -->
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="mail_success" style="display: none;">
                <div class="alert alert-success vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
                </div>
            </div>
            <div class="mail_error" style="display: none;">
                <div class="alert alert-danger vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>@lang('admin_lang.error')!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
                </div>
            </div>
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== --> 
            <!-- end pageheader --> 
            <!-- ============================================================== -->
            <div class="row printDiv" style="">
                <!-- ============================================================== -->
                <!-- basic table  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="margin-top: -68px;">
                    <div class="card" >
                        <h5 class="card-header cardTest"><strong>@lang('admin_lang.order_number') - {{ @$order->order_no }}</strong> <img src="{{ URL::to('public/admin/assets/images/logo.png')}}" alt="" style="height: 50px;width: 143px;">
                            <a class="adbtn btn btn-primary" class="print_invoice" style="color: white;margin-left: 14px;" onclick="window.print()"><i class="fas fa-less-than"></i>@lang('admin_lang.print_invoice')</a>
                        </h5>
                        <div class="card-body">
                            <div class="manager-dtls tsk-div">
                                <div class="row fld">
                                    {{-- Order details --}}
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.order_details')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.order_number')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->order_no }}
                                            </span> 
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.no_of_item')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterDetails->count() }}
                                            </span> 
                                        </p>

                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.driver')</span> <span class="deta-span"><strong>:</strong>
                                            </span>
                                        </p>
                                        --}}
                                        <p><span class="titel-span">@lang('admin_lang.order_date')</span> <span class="deta-span"><strong>:</strong>{{ @$order->created_at }}</span></p>
                                        <p><span class="titel-span">Product total weight</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->product_total_weight }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.admin_notes_ord_dtls')</span> <span class="deta-span"><strong>:</strong>{!! strip_tags(@$order->notes) !!}</span></p>

                                        <p><span class="titel-span">@lang('admin_lang.driver_notes')</span> <span class="deta-span"><strong>:</strong>
                                            <b class="mydesign" >
                                            @foreach(@$order->orderMasterDetails as $tttt)
                                            @if(@$tttt->driver_notes != null)
                                                {{@$tttt->driverDetails->fname}} {{@$tttt->driverDetails->fname}} :  {{@$tttt->driver_notes}}<br/>
                                                @endif
                                            @endforeach
                                            </b>
                                        </span></p>
                                        <p><span class="titel-span">Original Price</span> <span class="deta-span"><strong>:</strong>{{ @$order->subtotal }} {{getCurrency()}}</span></p>

                                        <p><span class="titel-span">@lang('admin_lang.tot_disc_dtls')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->total_discount }} {{getCurrency()}}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">Sub-total</span> <span class="deta-span"><strong>:</strong>{{ @$order->subtotal  - @$order->total_discount }} {{getCurrency()}}</span></p>

                                        <p><span class="titel-span">@lang('admin_lang.shipping_kwd')</span> <span class="deta-span"><strong>:</strong>{{ @$order->shipping_price }} {{getCurrency()}}</span></p>

                                        <p><span class="titel-span">@lang('admin_lang.order_total')</span> <span class="deta-span"><strong>:</strong>{{ @$order->order_total }} @lang('admin_lang.kwd') </span></p>

                                        <p><span class="titel-span">@lang('admin_lang.payment_method')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->payment_method == 'C')
                                            @lang('admin_lang.cod')
                                            @else
                                            @lang('admin_lang.online')
                                            @endif
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.order_type')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->order_type == 'I')
                                            @lang('admin_lang.internal_1')
                                            @else
                                            @lang('admin_lang.external_1')
                                            @endif
                                            </span>
                                        </p>
                                        @if(@$order->order_type == 'E')
                                        <p><span class="titel-span">Invoice Number</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->invoice_no }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">Invoice Details</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->invoice_details }}
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.delivery_date')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->delivery_date }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.delivery_time')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->delivery_time }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">Loyalty point received</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->loyalty_point_received }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">Loyalty point used</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->loyalty_point_used }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">Loyalty amount</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->loyalty_amount }}
                                            </span>
                                        </p>
                                        
                                        
                                        <p><span class="titel-span">Overall Order @lang('admin_lang.Status')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->status == 'I')
                                            Incomplete
                                            @elseif(@$order->status == 'N')
                                            New
                                            @elseif(@$order->status == 'OA')
                                            @lang('admin_lang.order_accepted')
                                            @elseif(@$order->status == 'DA')
                                            @lang('admin_lang.driver_assigned')
                                            @elseif(@$order->status == 'RP')
                                            @lang('admin_lang.ready_for_pickup')
                                            @elseif(@$order->status == 'OP')
                                            Picked up
                                            @elseif(@$order->status == 'OD')
                                            @lang('admin_lang.delivered_1')
                                            @elseif(@$order->status == 'OC')
                                            @lang('admin_lang.Cancelled_1')
                                            @endif
                                            </span>
                                        </p>
                                        @if(@$order->order_type == 'E')
                                        <p><span class="titel-span">@lang('admin_lang.images')</span> <span class="deta-span"><strong>:</strong> 
                                            @if(@$order->getOrderAllImages)
                                            @foreach(@$order->getOrderAllImages as $imgs)
                                            @php
                                            $image_path = 'storage/app/public/merchant_portfolio/'.@$img->image; 
                                            @endphp
                                            @if(file_exists(@$image_path))
                                            <img id="merchantPic" src="{{ URL::to('storage/app/public/merchant/external_order/photo/'.@$imgs->images) }}" alt="" style="width: 100px;height: 100px;">
                                            @endif
                                            @endforeach
                                            @endif
                                            </span>
                                        </p>
                                        @endif
                                    </div>
                                    {{-- Customer details --}}
                                    @if(@$order->order_type == 'I')
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.customer_details')</h4>
                                        <!-- @if(@$order->order_type == 'I')
                                        <p><span class="titel-span">@lang('admin_lang.country')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->userCountryDetails->countryDetailsBylanguage->name }}    
                                            @else
                                            {{ @$order->countryDetails->countryDetailsBylanguage->name }}
                                            @endif
                                            </span>
                                        </p>
                                        @endif -->
                                        {{-- name --}}
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->fname }} {{ @$order->customerDetails->lname }}
                                            @else
                                            {{ @$order->fname }} {{ @$order->lname }}
                                            @endif
                                            </span>
                                        </p>
                                        {{-- email --}}
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->email }}    
                                            @else
                                            {{ @$order->email }}
                                            @endif
                                            </span>
                                        </p>
                                        {{-- phone --}}
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->phone }}    
                                            @else
                                            {{ @$order->shipping_phone }}
                                            @endif
                                            </span>
                                        </p>
                                        {{-- shipping address --}}
                                        {{-- 
                                        <!-- <p><span class="titel-span">@lang('admin_lang.state') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->state }}    
                                            @else
                                            {{ @$order->shipping_state }}
                                            @endif
                                            </span>
                                        </p> -->
                                        --}}
                                        @if(@$order->order_type == 'I')
                                        <!-- <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->city }}    
                                            @else
                                            {{ @$order->getCityNameByLanguage->name }}
                                            @endif
                                            </span>
                                        </p> -->
                                        <!-- <p><span class="titel-span">@lang('admin_lang.street') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            @else
                                            {{ @$order->shipping_street }}
                                            @endif
                                            </span>
                                        </p> -->
                                        <!-- <p><span class="titel-span">@lang('admin_lang.block') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_block }}
                                            </span>
                                        </p> -->
                                        <!-- <p><span class="titel-span">@lang('admin_lang.building') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_building }}
                                            </span>
                                        </p> -->
                                        <!-- @if($order->shipping_country != 134) -->
                                        <!-- <p><span class="titel-span">Postal Code</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->zipcode }}    
                                            @else
                                            {{ @$order->shipping_zip }}
                                            @endif
                                            </span>
                                        </p> -->
                                        <!-- @endif -->
                                        <!-- <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->address }}
                                            @else
                                            {{ @$order->shipping_address }}
                                            @endif
                                            </span>
                                        </p> -->
                                        @endif
                                    </div>
                                    @endif
                                    @if(@$order->order_type == 'E')
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.seller_dtlss')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.country')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->sellerDetails->Country->name }}
                                            </span>
                                        </p>
                                        {{-- name --}}
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->sellerDetails->fname }} {{ @$order->orderMasterExtDetails->sellerDetails->lname }}
                                            </span>
                                        </p>
                                        {{-- email --}}
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->sellerDetails->email }}
                                            </span>
                                        </p>
                                        {{-- phone --}}
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->sellerDetails->phone }}
                                            </span>
                                        </p>
                                        {{-- shipping address --}}
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->sellerDetails->merchantCityDetails->name }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.state') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->sellerDetails->state }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.street') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->sellerDetails->address }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.avenue') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->sellerDetails->avenue }}
                                            </span>
                                        </p>
                                        {{-- 
                                        <p><span class="titel-span">Block </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->sellerDetails->block }}
                                            </span>
                                        </p>
                                        --}}
                                        <p><span class="titel-span">@lang('admin_lang.building') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->sellerDetails->building_number }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.zip')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->sellerDetails->zipcode }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.address_note')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->sellerDetails->address_note }}
                                            </span>
                                        </p>
                                    </div>
                                    @endif
                                </div>
                                @if(@$order->order_type == 'E')
                                <div class="row fld">
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.customer_details')</h4>
                                        @if(@$order->order_type == 'I')
                                        <p><span class="titel-span">@lang('admin_lang.country')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->userCountryDetails->countryDetailsBylanguage->name }}    
                                            @else
                                            {{ @$order->countryDetails->countryDetailsBylanguage->name }}
                                            @endif
                                            </span>
                                        </p>
                                        @endif
                                        {{-- name --}}
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->fname }} {{ @$order->customerDetails->lname }}
                                            @else
                                            {{ @$order->fname }} {{ @$order->lname }}
                                            @endif
                                            </span>
                                        </p>
                                        {{-- email --}}
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->email }}    
                                            @else
                                            {{ @$order->email }}
                                            @endif
                                            </span>
                                        </p>
                                        {{-- phone --}}
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->phone }}    
                                            @else
                                            {{ @$order->shipping_phone }}
                                            @endif
                                            </span>
                                        </p>
                                        {{-- shipping address --}}
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.state') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->state }}    
                                            @else
                                            {{ @$order->shipping_state }}
                                            @endif
                                            </span>
                                        </p>
                                        --}}
                                        @if(@$order->order_type == 'I')
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->city }}    
                                            @else
                                            {{ @$order->getCityNameByLanguage->name }}
                                            @endif
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.street') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            @else
                                            {{ @$order->shipping_street }}
                                            @endif
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.block') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_block }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.building') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_building }}
                                            </span>
                                        </p>
                                        @if($order->shipping_country != 134)
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->zipcode }}    
                                            @else
                                            {{ @$order->shipping_zip }}
                                            @endif
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->address }}
                                            @else
                                            {{ @$order->shipping_address }}
                                            @endif
                                            </span>
                                        </p>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.shipping_address')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.country')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCountry->name }}
                                            </span>
                                        </p>
                                        {{-- name --}}
                                        @if(@$order->order_type == 'I')
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_fname }} {{ @$order->shipping_lname }}
                                            </span>
                                        </p>
                                        {{-- email --}}
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_email }}
                                            </span>
                                        </p>
                                        {{-- phone --}}
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_phone }}
                                            </span>
                                        </p>
                                        @endif
                                        {{-- shipping address --}}
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.state') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->state }}    
                                            @else
                                            {{ @$order->shipping_state }}
                                            @endif
                                            </span>
                                        </p>
                                        --}}
                                        <p><span class="titel-span">@lang('admin_lang.city')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCityNameByLanguage->name }}   
                                            </span>
                                        </p>
                                        @if(@$order->order_type == 'E')
                                        <p><span class="titel-span">@lang('admin_lang.street') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_street }}    
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.block') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_block }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.building') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_building }}
                                            </span>
                                        </p>
                                        @if($order->shipping_country != 134)
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_postal_code }}    
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_address }}
                                            </span>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                                @endif
                                @if(@$order->order_type == 'I')
                                <div class="row fld">
                                    @if(@$order->user_id != 0)
                                    {{-- billing address --}}
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.billing_address')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.country')</span>
                                            <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getBillingCountry->name }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_fname }} {{ @$order->billing_lname }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_email }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_phone }}
                                            </span>
                                        </p>
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.shipping_address')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billingAddress->address2 }}
                                            </span>
                                        </p>
                                        --}}
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.state') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billingAddress->state }} 
                                            </span>
                                        </p>
                                        --}}
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getBillingCityNameByLanguage->name }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.street')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_street }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.block')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_block }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.building')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_building }}
                                            </span>
                                        </p>
                                        @if(@$order->billingAddress->country != 134)
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_postal_code }}
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_more_address}}
                                            </span>
                                        </p>
                                    </div>
                                    {{-- shipping address --}}
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.shipping_address')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.country')</span>
                                            <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCountry->name }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_fname }} {{ @$order->shipping_lname }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_email }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_phone }}
                                            </span>
                                        </p>
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.shipping_address')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shippingAddress->address2 }}
                                            </span>
                                        </p>
                                        --}}
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.state') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shippingAddress->state }}  
                                            </span>
                                        </p>
                                        --}}
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCityNameByLanguage->name }}    
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.street')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_street }}    
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.block')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_block }}    
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.building')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_building }}    
                                            </span>
                                        </p>
                                        @if(@$order->shippingAddress->country == 134)
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_postal_code }}    
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_more_address }} 
                                            </span>
                                        </p>
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shippingAddress->city }}    
                                            </span>
                                        </p>
                                        --}}
                                        {{-- 
                                        <p><span class="titel-span">Postal code</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shippingAddress->zipcode }}    
                                            </span>
                                        </p>
                                        --}}
                                    </div>
                                    @endif
                                </div>
                                @endif
                                {{-- driver details for external order --}}
                                @if(@$order->order_type == 'E')

                                <!-- <div class="row fld">
                                    {{-- driver details --}}
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.driver_details')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->driverDetails->fname." ".@$order->orderMasterExtDetails->driverDetails->lname }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->driverDetails->email }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->driverDetails->phone }}
                                            </span>
                                        </p>
                                        <p>
                                            <span class="titel-span">@lang('admin_lang.pro_pic')</span> 
                                            <span class="deta-span">
                                                <strong>:</strong>
                                                @if(@$order->orderMasterExtDetails->driverDetails->image)
                                                @php
                                                $image_path = 'storage/app/public/driver/profile_pics/'.@$order->orderMasterExtDetails->driverDetails->image; 
                                                @endphp
                                                @if(file_exists(@$image_path))
                                        <div class="profile" style="display: block;">
                                        <img id="driverProfilePicture" src="{{ URL::to('storage/app/public/driver/profile_pics/'.@$order->orderMasterExtDetails->driverDetails->image) }}" alt="" style="width: 100px;height: 100px;">
                                        </div>
                                        @endif
                                        @endif    
                                        </span>
                                        </p>
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.street')</span> <span class="deta-span"><strong>:</strong>
                                            </span>
                                        </p>
                                        --}}
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.block')</span> <span class="deta-span"><strong>:</strong>
                                            </span>
                                        </p>
                                        --}}
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.building')</span> <span class="deta-span"><strong>:</strong>
                                            </span>
                                        </p>
                                        --}}
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            </span>
                                        </p>
                                        --}}
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            </span>
                                        </p>
                                        --}}
                                    </div>
                                </div> -->

                                @endif
                                {{-- end  --}}
                            </div>
                            <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.order_history') :</h4>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered first">
                                    <thead>
                                        @if(@$order->order_type == 'I')
                                        <tr>
                                            <th>@lang('admin_lang.product_image')</th>
                                            <th>@lang('admin_lang.product')</th>
                                            <th>@lang('admin_lang.attribute')</th>
                                            <th>@lang('admin_lang.seller')</th>
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight') (Gms)</th>
                                            <th>@lang('admin_lang.sub_total') ({{getCurrency()}})</th>
                                            <th>@lang('admin_lang.total_1') ({{getCurrency()}})</th>
                                            <th>@lang('admin_lang.payment_1')</th>
                                            <th>@lang('admin_lang.drivers')</th>
                                            <!-- <th>@lang('admin_lang.driver_notes')</th> -->
                                            <!-- <th>@lang('admin_lang.review_point_rate')</th> -->
                                            <th>@lang('admin_lang.review_ratings')</th>
                                            <!-- <th>@lang('admin_lang.review_comments')</th> -->
                                            <th>@lang('admin_lang.status_1')</th>
                                            <!-- <th>@lang('admin_lang.action_1')</th> -->
                                            <!-- <th>@lang('admin_lang.change_status_admin')</th> -->
                                        </tr>
                                        @else
                                        <tr>
                                            <th>@lang('admin_lang.seller')</th>
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight')</th>
                                            <th>@lang('admin_lang.shipping_cst')</th>
                                            <th>@lang('admin_lang.sub_total')</th>
                                            <th>@lang('admin_lang.total_1')</th>
                                            <th>@lang('admin_lang.payment_1')</th>
                                            
                                            <th>@lang('admin_lang.drivers')</th>
                                            <th>@lang('admin_lang.driver_notes')</th>
                                            <th>@lang('admin_lang.status_1')</th>
                                            <!-- <th>@lang('admin_lang.action_1')</th> -->
                                            <!-- <th>@lang('admin_lang.change_status_admin')</th> -->
                                        </tr>
                                        @endif
                                    </thead>
                                    <tbody>
                                        @if(@$order->orderMasterDetails)
                                        @foreach(@$order->orderMasterDetails as $key=>$details)
                                        @if(@$order->order_type == 'I')
                                        <tr>
                                            <td>
                                                @if(@$details->productDetails->defaultImage->image)
                                                @php
                                                $image_path = 'storage/app/public/products/'.@$details->productDetails->defaultImage->image; 
                                                @endphp
                                                @if(file_exists(@$image_path))
                                                <div class="profile" style="display: block;">
                                                    <img id="profilePicture" src="{{ URL::to('storage/app/public/products/'.@$details->productDetails->defaultImage->image) }}" alt="" style="width: 100px;height: 100px;">
                                                </div>
                                                @endif
                                                @endif
                                            </td>
                                            <td>
                                                {{ $details->productDetails->productByLanguage->title }}
                                            </td>
                                            <td>
                                                @php
                                                $attr = json_decode($details->variants,true);
                                                $attributes ="";
                                                $separator =" ";
                                                @endphp
                                                @if(@$attr)
                                                @foreach(@$attr as $value)
                                                @foreach(@$value as $key=>$v)
                                                @if($key == getLanguage()->id)
                                                @php
                                                $variant = $v['variant'];
                                                $variant_value = $v['variant_value'];
                                                @endphp
                                                @php
                                                $attributes = $attributes.$separator.$variant." :".$variant_value;
                                                @endphp
                                                @endif
                                                @endforeach
                                                @php
                                                $separator =" , ";
                                                @endphp
                                                @endforeach
                                                @endif
                                                {{ @$attributes }}
                                            </td>
                                            <td>
                                                {{ $details->sellerDetails->fname." ".$details->sellerDetails->lname }}
                                            </td>
                                            <td>
                                                {{ @$details->quantity  }}
                                            </td>
                                            <td>{{ @$details->weight  }}</td>

                                            <td>
                                                @if(@$details->discounted_price != 0.000)
                                                {{ @$details->discounted_price }}
                                                @else
                                                {{ @$details->original_price }}
                                                @endif
                                            </td>
                                            <td>
                                                {{ @$details->total }}
                                            </td>
                                            <td>
                                                @if(@$order->payment_method == 'C')
                                                @lang('admin_lang.cod')
                                                @else
                                                @lang('admin_lang.online')
                                                @endif
                                            </td>
                                            <td>
                                                {{ @$details->driverDetails->fname }} {{ @$details->driverDetails->lname }}
                                            </td>
                                            <!-- <td>{!!strip_tags(@$details->driver_notes)!!}</td> -->
                                            <!-- <td>
                                                @if(@$details->rate != 0)
                                                    {{ @$details->rate }}
                                                @endif
                                            </td> -->
                                            <td>
                                                @if(@$details->rate != 0)
                                                    @if(@$details->rate == 5 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    
                                                    @elseif(@$details->rate == 4 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    @elseif(@$details->rate == 3 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    @elseif(@$details->rate == 2 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    @elseif(@$details->rate == 1 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    @endif
                                                @endif
                                            </td>
                                            <!-- <td>
                                                @if(@$details->rate != 0)
                                                {{ @$details->comment }}
                                                @endif
                                            </td> -->
                                            <td>
                                                <!-- {{ @$details->status }} -->
                                                @if(@$details->status == 'I')
                                                Incomplete
                                                @elseif(@$details->status == 'N')
                                                New
                                                @elseif(@$details->status == 'OA')
                                                @lang('admin_lang.order_accepted')
                                                @elseif(@$details->status == 'DA')
                                                @lang('admin_lang.driver_assigned')
                                                @elseif(@$details->status == 'RP')
                                                @lang('admin_lang.ready_for_pickup')
                                                @elseif(@$details->status == 'OP')
                                                Picked up
                                                @elseif(@$details->status == 'OD')
                                                @lang('admin_lang.delivered_1')
                                                @elseif(@$details->status == 'OC')
                                                @lang('admin_lang.Cancelled_1')
                                                @endif
                                            </td>
                                           
                                            
                                        </tr>
                                        @else
                                        <tr>
                                            <td>
                                                {{ @$details->sellerDetails->fname }} {{ @$details->sellerDetails->lname }}
                                            </td>
                                            <td>
                                                {{ @$order->orderMasterDetails->count() }}
                                            </td>
                                            
                                            <td>
                                                {{ @$order->product_total_weight }}
                                            </td>
                                            <td>
                                                {{ @$order->shipping_price }}
                                            </td>
                                            <td>
                                                {{ @$order->subtotal }}
                                            </td>
                                            <td>
                                                {{ @$order->order_total }}
                                            </td>
                                            <td>
                                                @if(@$order->payment_method == 'C')
                                                @lang('admin_lang.cod')
                                                @else
                                                @lang('admin_lang.online')
                                                @endif
                                            </td>
                                            <td>
                                                {{ @$details->driverDetails->fname }} {{ @$details->driverDetails->lname }}
                                            </td>
                                            <td>{!!strip_tags(@$details->driver_notes)!!}</td>
                                            <td>
                                                {{-- {{ @$details->status }} --}}
                                                @if(@$details->status == 'I')
                                                Incomplete
                                                @elseif(@$details->status == 'N')
                                                New
                                                @elseif(@$details->status == 'OA')
                                                @lang('admin_lang.order_accepted')
                                                @elseif(@$details->status == 'DA')
                                                @lang('admin_lang.driver_assigned')
                                                @elseif(@$details->status == 'RP')
                                                @lang('admin_lang.ready_for_pickup')
                                                @elseif(@$details->status == 'OP')
                                                Picked up
                                                @elseif(@$details->status == 'OD')
                                                @lang('admin_lang.delivered_1')
                                                @elseif(@$details->status == 'OC')
                                                @lang('admin_lang.Cancelled_1')
                                                @endif
                                            </td>
                                            
                                            
                                        </tr>
                                        @endif
                                        @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        @if(@$order->order_type == 'I')
                                        <tr>
                                            <th>@lang('admin_lang.product_image')</th>
                                            <th>@lang('admin_lang.product')</th>
                                            <th>@lang('admin_lang.attribute')</th>
                                            <th>@lang('admin_lang.seller')</th>
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight') (Gms)</th>
                                            <th>@lang('admin_lang.sub_total') ({{getCurrency()}})</th>
                                            <th>@lang('admin_lang.total_1') ({{getCurrency()}})</th>
                                            <th>@lang('admin_lang.payment_1')</th>
                                            <th>@lang('admin_lang.drivers')</th>
                                            <!-- <th>@lang('admin_lang.driver_notes')</th> -->
                                            <!-- <th>@lang('admin_lang.review_point_rate')</th> -->
                                            <th>@lang('admin_lang.review_ratings')</th>
                                            <!-- <th>@lang('admin_lang.review_comments')</th> -->
                                            <th>@lang('admin_lang.status_1')</th>
                                            <!-- <th>@lang('admin_lang.action_1')</th> -->
                                            <!-- <th>@lang('admin_lang.change_status_admin')</th> -->
                                        </tr>
                                        @else
                                        <tr>
                                            <th>@lang('admin_lang.seller')</th>
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight')</th>
                                            <th>@lang('admin_lang.shipping_cst')</th>
                                            <th>@lang('admin_lang.sub_total')</th>
                                            <th>@lang('admin_lang.total_1')</th>
                                            <th>@lang('admin_lang.payment_1')</th>
                                            <th>@lang('admin_lang.driver')</th>
                                            <th>Driver Notes</th>
                                            <th>Status</th>
                                            <!-- <th>@lang('admin_lang.change_status_action')</th> -->
                                        </tr>
                                        @endif
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end basic table  -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        @include('admin.includes.footer')
        <!-- ============================================================== -->
        <!-- end footer -->
        <!-- ============================================================== -->
    </div>
</div>
<div class="modal fade" id="assign_driver_popup_show" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.assign_driver')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="driver_assign_form" method="post" action="{{ route('admin.driver.assign') }}">
                        @csrf
                        <div class="form-row">
                            <input id="order_id" name="order_id" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="order_type_driver" name="order_type_driver_driver" type="hidden" class="form-control" placeholder="" readonly>
                            <div class="form-group col-md-12">
                                <label>
                                @lang('admin_lang.assign_driver')
                                </label>
                                <select id="assign" name="assign" class="form-control">
                                    <option value="">@lang('admin_lang.select_driver')</option>
                                    @foreach(@$drivers as $driver)
                                    <option value="{{ @$driver->id }}">{{ @$driver->fname }} {{ @$driver->lname }}</option>
                                    @endforeach
                                </select>
                                <span class="error_assign_driver" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12 assign_type_div">
                                <label for="specific" class="col-form-label">Assign type</label>    
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;Assign entire order to this driver
                                <input type="radio" name="assign_type" class="type" value="EO">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;Assign All products of this Seller to this driver 
                                <input type="radio" name="assign_type" class="type" value="SS">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;None
                                <input type="radio" name="assign_type" class="type" value="N">
                                <span class="checkmark"></span>
                                </label>
                                {{-- Seller specific --}}
                                {{-- <label for="ord_sp">Order specific</label>
                                <input type="radio" id="ord_sp" name="order_specific" value="Order Specific" class="form-control" />Order specific
                                <label for="seller_sp">Seller specific</label>
                                <input type="radio" id="seller_sp" name="order_specific" value="Order Specific" class="form-control" />Seller specific --}}
                                {{-- <span class="error_assign_driver" style="color: red;"></span> --}}
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="assignDriver" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="reassign_driver_popup_show" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.assign_driver')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="driver_reassign_form" method="post" action="{{ route('admin.driver.reassign') }}">
                        @csrf
                        <div class="form-row">
                            <input id="order_id_reassign" name="order_id_reassign" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="order_type_driver_reassign" name="order_type_driver_reassign" type="hidden" class="form-control" placeholder="" readonly>
                            <div class="form-group col-md-12">
                                <label>
                                @lang('admin_lang.assign_driver')
                                </label>
                                <select id="reassign" name="reassign" class="form-control">
                                    <option value="">@lang('admin_lang.select_driver')</option>
                                    @foreach(@$drivers as $driver)
                                    <option value="{{ @$driver->id }}">{{ @$driver->fname }} {{ @$driver->lname }}</option>
                                    @endforeach
                                </select>
                                <span class="error_reassign_driver" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12 reassign_type_div">
                                <label for="specific" class="col-form-label">Assign type</label>    
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;Assign entire order to this driver
                                <input type="radio" name="assign_type" class="type" value="EO">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;Assign All products of this Seller to this driver 
                                <input type="radio" name="assign_type" class="type" value="SS">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;None
                                <input type="radio" name="assign_type" class="type" value="N">
                                <span class="checkmark"></span>
                                </label>
                                {{-- Seller specific --}}
                                {{-- <label for="ord_sp">Order specific</label>
                                <input type="radio" id="ord_sp" name="order_specific" value="Order Specific" class="form-control" />Order specific
                                <label for="seller_sp">Seller specific</label>
                                <input type="radio" id="seller_sp" name="order_specific" value="Order Specific" class="form-control" />Seller specific --}}
                                {{-- <span class="error_assign_driver" style="color: red;"></span> --}}
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="reassignDriver" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalCenter">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.notes')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="approveReasonForm" method="post" action="">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <!-- <label>Notes</label> -->
                                <textarea class="form-control h-80px" rows="2" id="comment"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" class="btn btn-primary popbtntp" id="approve_product">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalCenter2">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.notes')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="rejectReasonForm" method="post" action="">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <!-- <label>Notes</label> -->
                                <textarea class="form-control h-80px" rows="2" id="comment"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end main wrapper -->
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<!-- <script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script> -->
<!-- <script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script> -->
<!-- <script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script> -->
<!-- <script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script> -->
<!-- <script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script> -->
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<!-- <script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script> -->
<!-- <script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script> -->
<!-- <script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script> -->
<!-- <script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script> -->
<!-- <script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script> -->
<!-- <script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script> -->
<script src="{{ asset('public/admin/assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/slimscroll/jquery.slimscroll.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/multi-select/js/jquery.multi-select.js') }}"></script> 
<script src="{{ asset('public/admin/assets/libs/js/main-js.js') }}"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".footer").css({
            "margin-left": -147px;
        });
        // $(document).on('change', 'input[type=radio]', function (e) {
        //     $('.dependent').removeClass('checked');
        //     $(this).parent().addClass('checked');
        //     var type = $(this).val();
        // });
        $(document).on("click","#assignDriver",function(e){
            
            var assign = $.trim($("#assign").val());
            
            if(assign == ""){
                $(".error_assign_driver").text('@lang('validation.required')');
            }else{
                $(".error_assign_driver").text("");
            }
            
            if(assign != ""){
                $("#driver_assign_form").submit();
            }
        });
        
        $(document).on("click",".assign_driver_popup",function(e){
    
            var id= $.trim($(e.currentTarget).attr("data-id"));
            var otype= $.trim($(e.currentTarget).attr("data-otype"));
            // action= $.trim($(e.currentTarget).attr("data-action"));
            
            $('#assign_driver').attr("data-id",id);
            $('#assign_driver').attr("data-otype",id);
                        // new
            $('#reassign_driver_popup_show').modal('hide');
            $('#assign_driver_popup_show').modal('show');
            $("#order_id").val(id);
            $("#order_type_driver").val(otype);
            if(otype == 'E'){
                $(".assign_type_div").hide();
            }else{
                $(".assign_type_div").show();
            }
            // $("#sendEmailForm").attr("action",action);
            
        });
        $(document).on("click","#reassignDriver",function(e){
            
            var assign = $.trim($("#reassign").val());
            
            if(assign == ""){
                $(".error_reassign_driver").text('@lang('validation.required')');
            }else{
                $(".error_reassign_driver").text("");
            }
            
            if(assign != ""){
                $("#driver_reassign_form").submit();
            }
        });
        
        $(document).on("click",".reassign_driver_popup",function(e){
    
            var id= $.trim($(e.currentTarget).attr("data-id"));
            var otype= $.trim($(e.currentTarget).attr("data-otype"));
            // action= $.trim($(e.currentTarget).attr("data-action"));
            
            $('#reassign_driver').attr("data-id",id);
            $('#reassign_driver').attr("data-otype",id);
            $('#assign_driver_popup_show').modal('hide');
            $('#reassign_driver_popup_show').modal('show');
    
            $("#order_id_reassign").val(id);
            $("#order_type_driver_reassign").val(otype);
            if(otype == 'E'){
                $(".reassign_type_div").hide();
            }else{
                $(".reassign_type_div").show();
            }
            // $("#sendEmailForm").attr("action",action);
            
        });
    
        // $(document).on("click",".approve_product",function(e){
        //     var id= $.trim($(e.currentTarget).attr("data-id"));
        //     // alert(id);
        //     $('#exampleModalCenter').attr("data-id",id);
        //     $('#exampleModalCenter').modal('show');
        // });
        // $(document).on("click","#approve_product",function(e){
            
        //     var id = $('#exampleModalCenter').attr("data-id");
        //     alert("id : "+id);
        //     if(id != ""){
            
        //             var rurl = '{{ route('admin.approve.product.order') }}';
        //             $.ajax({
        //                 type:"GET",
        //                 url:rurl,
        //                 data: {
        //                     id : id
        //                 },
        //                 success:function(resp){
        //                     if(resp==1){
        //                         $(".mail_success").css({
        //                             "display":"block"
        //                         });
        //                         $(".successMsg").text('@lang('admin_lang.product_approved_success')');
        //                         $(".mail_error").css({
        //                             "display":"none"
        //                         });
        //                         $("#commission").val("");
        //                         $(".error_cost").text("");
        //                         $('#shippingCostModalCenter').modal('hide');
        //                         $("#search").trigger("click");
        
        //                     }else{
        //                         $(".mail_error").css({
        //                             "display":"block"
        //                         });
        //                         $(".errorMsg").text("Unauthorized access!Product doesn't approved!");
        //                         $(".mail_success").css({
        //                             "display":"none"
        //                         });
        //                     }
        //                 }
        //             });
        //     }else{
        //         $(".error_cost").text("This field is required");
        //     }
        // });
        // $(document).on("click",".reject_product",function(e){
        //     var id= $.trim($(e.currentTarget).attr("data-id"));
        //     $('#exampleModalCenter2').attr("data-id",id);
        //     $('#exampleModalCenter2').modal('show');
        // });
        // $(document).on("click","#reject_product",function(e){
            
        //     var id = $('#exampleModalCenter2').attr("data-id");
        //     if(id != ""){
            
        //             var rurl = '{{ route('admin.reject.product.order') }}';
        //             $.ajax({
        //                 type:"GET",
        //                 url:rurl,
        //                 data: {
        //                     id : id
        //                 },
        //                 success:function(resp){
        //                     if(resp==1){
        //                         $(".mail_success").css({
        //                             "display":"block"
        //                         });
        //                         $(".successMsg").text("Product rejected successfully!");
        //                         $(".mail_error").css({
        //                             "display":"none"
        //                         });
        //                         $("#commission").val("");
        //                         $(".error_cost").text("");
        //                         $('#shippingCostModalCenter').modal('hide');
        //                         $("#search").trigger("click");
        
        //                     }else{
        //                         $(".mail_error").css({
        //                             "display":"block"
        //                         });
        //                         $(".errorMsg").text("Unauthorized access!Product doesn't rejected!");
        //                         $(".mail_success").css({
        //                             "display":"none"
        //                         });
        //                     }
        //                 }
        //             });
        //     }else{
        //         $(".error_cost").text("This field is required");
        //     }
        // });
        
        $("#profile_pic").change(function() {
            var filename = $.trim($("#profile_pic").val()),
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".errorpic").text("");
                readURL(this);
            }else{
                
                $(".errorpic").text('@lang('validation.image_extension_type')');
                $('#profilePicture').attr('src', "");
                
            }
        });
        $(document).on("change",".any_status_change",function(e){
            var id = $(this).attr("data-id"),
            status = $(this).val();
            if(id != ""){
                // $("#status_change_detail"+id).val();
                $("#change_any_status_form"+id).submit();
            }else{
    
            }
        });
        $(document).on("click",".print_invoice",function(e){
            // alert("xddbg");
            var divContents = $(".printDiv").innerHTML; 
            var a = window.open('', '', 'height=500, width=500'); 
            a.document.write('<html>'); 
            a.document.write('<body > <h1>Div contents are <br>'); 
            a.document.write(divContents); 
            a.document.write('</body></html>'); 
            a.document.close(); 
            a.print(); 
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                  $('#profilePicture').attr('src', e.target.result);
                  $(".profile").show();
              }
              reader.readAsDataURL(input.files[0]);
          }
        }
        KWD('.llk').click(function function_name(argument) {
            KWD('#exampleModalCenter1').modal('show');
        });
    });
</script>
<!-- print page -->
<script type="text/javascript">

    function printDiv() { 
        
    } 
</script>
@endsection