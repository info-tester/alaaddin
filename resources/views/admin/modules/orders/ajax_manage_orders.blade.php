<table class="table table-striped table-bordered first" id="myTable">
	<thead>
		<tr>
			<th>@lang('admin_lang.order_no')</th>
			<th>@lang('admin_lang.date')</th>
			<th>@lang('admin_lang.customers')</th>
			<th>@lang('admin_lang.order_total')</th>
			<th>@lang('admin_lang.item')</th>
			<th>@lang('admin_lang.Type')</th>
			<th>@lang('admin_lang.payment_1')</th>
			{{-- <th>@lang('admin_lang.driver')</th> --}}
			<th>@lang('admin_lang.Status')</th>
			<th>@lang('admin_lang.Action')</th>
		</tr>
	</thead>
	<tbody>
		@if(@$orders)
		@foreach(@$orders as $order)
		<tr>
			<td>{{ @$order->order_no }}</td>
			<td>{{ @$order->created_at }}</td>
			<td>
				@if(@$order->user_id != "")
				{{ @$order->customerDetails->fname." ".@$order->customerDetails->lname }}
				@else
				{{ @$order->fname." ".@$order->lname }}
				@endif
			</td>
			<td>
				{{ @$order->order_total }}
			</td>
			<td>
				@if(@$order->orderMasterDetails)
				{{ @$order->orderMasterDetails->count() }}
				@endif
			</td>
			<td>
				@if(@$order->order_type == 'I')
				@lang('admin_lang.internal_1')    
				@else
				@lang('admin_lang.external_1')
				@endif
			</td>
			<td>
				@if(@$order->payment_method == 'C')
				@lang('admin_lang.cod')
				@else
				@lang('admin_lang.online')
				@endif
			</td>
			{{-- <td></td> --}}
			<td class="status_{{ @$order->id }}">
				@if(@$order->status == 'I')
				@lang('admin_lang.initiated_1')
				@elseif(@$order->status == 'N')
				@lang('admin_lang.new')
				@elseif(@$order->status == 'P')
				@lang('admin_lang.paid')
				@else
				@lang('admin_lang.Cancelled_1')
				@endif
			</td>
			<td>
				{{-- view details --}}
												<a href="{{ route('admin.view.order',@$order->id) }}"><i class=" fas fa-eye" title='@lang('admin_lang.view')'></i></a>

												{{-- edit external order details --}}
												@if(@$order->order_type == 'E')

												<a href="{{ route('admin.edit.order.details',@$order->id) }}"><i class="fas fa-edit" title='@lang('admin_lang.Edit')'></i></a>

												@endif
												{{-- cancel order --}}
												@if(@$order->status != 'C')
												<a href="javascript:void(0)" class="order_mstr_cancel" data-id="{{ @$order->id }}" data-ordno="{{ @$order->order_no }}"><i class="far fa-times-circle cn_or_{{ @$order->id }}" title='@lang('admin_lang.cancel')'></i></a> 
												@endif
												{{-- status change of order master--}}
												{{-- @if(@$order->order_type == 'E') --}}
													@if(@$order->status == 'N')
														{{-- <a href="{{ route('admin.status.order.master',@$order->id) }}" onclick="return confirm('Do you want to change status to order initiated ? ');"><i class="fas fa-check" title="Order Initiated"></i></a> --}}
														<a href="javascript:void(0)"  class="order_mstr_status_change" data-id="{{ @$order->id }}" data-changeStatus="Initiated"><i class="fas fa-check ord_{{ @$order->id }}" title='@lang('admin_lang.ord_initiated')'></i></a>

													@elseif(@$order->status == 'I') 
													<a href="javascript:void(0)"  class="order_mstr_status_change" data-id="{{ @$order->id }}" data-changeStatus="Paid"><i class="fas fa-check ord_{{ @$order->id }}" title='@lang('admin_lang.ord_paid')'></i></a>
													{{-- <a href="{{ route('admin.status.order.master',@$order->id) }}" onclick="return confirm('Do you want to change status to order paid ? ');"><i class="fas fa-check" title="Order Paid" ></i></a> --}}
													@endif
												{{-- @else --}}
													@if(@$order->status == 'I')
													<a href="{{ route('admin.status.order.master',@$order->id) }}" class="order_mstr_status_change"  data-changeStatus="Paid"><i class="fas fa-money ord_{{ @$order->id }}" title='@lang('admin_lang.ord_paid')'></i></a>
													@endif
												{{-- @endif --}}
			</td>
		</tr>
		@endforeach
		@endif
	</tbody>
	<tfoot>
		<tr>
			<th>@lang('admin_lang.order_no')</th>
			<th>@lang('admin_lang.date')</th>
			<th>@lang('admin_lang.customers')</th>
			<th>@lang('admin_lang.order_total')</th>
			<th>@lang('admin_lang.item')</th>
			<th>@lang('admin_lang.Type')</th>
			<th>@lang('admin_lang.payment_1')</th>
			{{-- <th>@lang('admin_lang.driver')</th> --}}
			<th>@lang('admin_lang.Status')</th>
			<th>@lang('admin_lang.Action')</th>
		</tr>
	</tfoot>
</table>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript"></script>