@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | Edit Internal Order
@endsection
@section('links')
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="{{ asset('public/frontend/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    label.error{
        border: none !important;
        background-color: #fff !important;
        color: #f00;
    }

    #cityList{
        position: absolute;
        width: 96%;
        /*height: 200px;*/
        z-index: 99;
    }
    #cityList ul{
        background: #fff;
        width: 96%;
        border: solid 1px #eee;
        padding: 0;
        max-height: 200px;
        overflow-y: scroll;
    }
    #cityList ul li{
        list-style: none;
        padding: 5px 15px;
        cursor: pointer;
        border-bottom: solid 1px #eee;
    }
    #cityList ul li:hover{
        background: #3a82c4;
        color: #fff;
    }

    .right-totals {
        float: right;
        width: 60%;
        padding-right:35px;
    }
    .totals{
        float:right;
        width:auto;
        margin-bottom:5px;
        }
    .totals p{
        color:#000;
        font-size:14px;
        font-weight:400;
        font-family: 'Roboto', sans-serif;
        text-transform:uppercase;
        line-height: 25px;
    }
    .totals p span {
        float: right;
        width: 130px;
        text-align: right;
    }
    .borders{
        float:left;
        width:100%;
        height:1px;
        background:#d1d1d1;
        margin:5px 0;
        }
    .totals h4{
        color:#000;
        font-size:20px;
        font-weight:500;
        font-family: 'Roboto', sans-serif;
        text-transform:uppercase;
        margin:0px;
        }
    .totals h4 span{
        float: right;
        width: 150px;
        text-align: right;
        }
    .check-out-bttns {
        float: right;
        width: auto;
        margin-bottom: 15px;
    }
    .check-out-bttns a {
        color: #fff;
        font-size: 16px;
        font-weight: 500;
        background: #0771d4;
        font-family: 'Poppins', sans-serif;
        padding: 8px 10px;
        display: inline-block;
        margin-left: 9px;
        border-radius: 2px;
    }
    .proc{
        background:#28a745 !important;
        /*background:#ef1600 !important;*/
        }
    .check-out-bttns a:hover{
        color:#fff;
        background:#000 !important;;
        }
    .shoping-cart-body{
        padding:30px 0 60px 0;
        }
    .sucss-loyality {
        border: 1px solid #5ed095;
        border-radius: 3px;
        padding: 10px 15px;
        background: #d7fcdb;
        color: #01631d;
        font-size: 16px;
        font-weight: 400;
        font-family: 'Roboto', sans-serif;
        display: inline-block;
        margin-top: 43px !important;
        float: left;
    }
    .sucss-loyality img{
        float:left;
        margin-right:12px;
    }
    .check-out-sucs .left-point{
        width:60%;
    }
    .check-out-sucs .right-totals{
        width:40%;
    }
    .cart-total-area{
        float: left;
        width: 100%;
        margin-top: 15px;
    }


    /*table css*/

    .table {
        display: table;
        margin-bottom:0px !important;
        max-width: 100%;
        width: 100% !important;
        box-shadow: 0px 1px 2px #dcdcdc;
    }
    .table .one_row1 {
        display: table-row;
    }
    .tab_head_sheet {
        background: #fff;
        border: medium none;
        color: #7f8080;
        font-size: 14px;
        font-family: 'Roboto', sans-serif;
        line-height: 20px;
        padding-left: 20px;
        text-align: center;
        font-weight: 400;
        text-transform: capitalize;
    }
    .cell1 {
        display: table-cell;
        padding: 12px 15px;
        position: relative;
        vertical-align: top;
        white-space: nowrap;
        border-bottom: 1px solid #dedede;
    }
    .tab_head_sheet_1 {
        border: medium none;
        height: 50px;
        padding: 10px 15px;
        text-align: center;
        white-space: normal;
        overflow-wrap: break-word;
        color: #454545;
        font-family: 'Roboto', sans-serif;
        border-bottom: 1px solid #dedede;
    }
    .tab_head_sheet_1 p {
        color: #272d2d;
        font-size: 14px;
        font-weight: 400;
        margin: 0;
        font-family: 'Roboto', sans-serif;
        /*float: left;*/
    }
    .pro-name {
        font-size: 16px !important;
        color: #f31a6f !important;
    }
    .W55_1 {
        display: none;
    }
    .item_img {
        float: left;
        width: 80px !important;
        height: 70px;
        overflow: hidden;
        position: relative;
        text-align: center;
        margin-right: 7px;
        background: #f4f5f7;
    }
    .item_img img{
        position:absolute;
        top:0;
        bottom:0;
        right:0;
        left:0;
        margin:auto;
        max-width:85%;
        padding:0px !important;
        }
    /*table css*/
    .cart-tabel {
        float: left;
        width: 100%;
        border: 1px solid #dedede;
    }
    .tabel-image {
        float: left;
        width: 65px;
        height: 85px;
        background: #fff;
        overflow: hidden;
        position: relative;
        margin-right: 10px;
        border: 1px solid #e3e3e3;
    }
    .tabel-image img{
        position:absolute;
        margin:auto;
        top:0;
        bottom:0;
        left:0;
        right:0;
        max-height:100%;
        max-width:100%;
        width:auto;
        height:auto;
        }
    .add_ttrr h5 {
        color: #171919;
        font-size: 15px;
        font-weight: 400;
        font-family: 'Roboto', sans-serif;
        margin-bottom: 5px;
    }
    .add_ttrr h6 {
        color: #474747;
        font-size: 14px;
        font-weight: 400;
        font-family: 'Roboto', sans-serif;
        margin-bottom: 3px;
    }
    .add_ttrr h4 {
        color: #272d2d;
        font-size: 14px;
        font-weight: 400;
        font-family: 'Roboto', sans-serif;
        margin-bottom: 3px;
    }


    .seller-logo {
        border: 1px solid #dedede;
        width: 65px;
        height: 65px;
        display: inline-block;
        position: relative;
        overflow: hidden;
        background: #fff;
    }
    .seller-logo img{
        position:absolute;
        margin:auto;
        top:0;
        bottom:0;
        left:0;
        right:0;
        max-height:100%;
        max-width:100%;
        width:auto;
        height:auto;
        }
    .dlt{
        font-size:25px;
        color:#717171; 
        }
    .dlt:hover{
        color:#d52d1c;
        }
    .dlt .fa{
        float:left;
        }
    .cart-total-area{
        float:left;
        width:100%;
        margin-top:15px;
        }
    .left-point{
        float:left;
        width:35%;
        padding:15px;
        }
    .point-border {
        float: left;
        width: 100%;
        text-align: left;
        border: 2px dashed #d7d7d7;
        padding: 15px 20px;
    }
    .point-border h6 {
        color: #000;
        font-size: 17px;
        font-weight: 400;
        font-family: 'Roboto', sans-serif;
        margin-bottom: 3px;
    }
    .point-border p{
        color: #606262;
        font-size: 14px;
        font-weight: 400;
        font-family: 'Poppins', sans-serif;
        margin-bottom: 3px;
    }
    .radio-custom{
        float:left;
        width:100%;
        margin-top:10px;
        }
    .radio-custom [type="radio"]:checked,
    .radio-custom [type="radio"]:not(:checked) {
        position: absolute;
        left: -9999px;
    }
    .radio-custom [type="radio"]:checked + label, 
    .radio-custom [type="radio"]:not(:checked) + label {
        position: relative;
        padding-left: 26px;
        cursor: pointer;
        line-height: 22px;
        display: inline-block;
        color: #202020;
        font-size: 16px;
        margin: 0;
        font-family: 'Poppins', sans-serif;
    }
    .radio-custom [type="radio"]:not(:checked) + label::before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 21px;
        height: 21px;
        border: 2px solid #909090;
        border-radius: 100%;
        background: #fff;
    }
    .radio-custom [type="radio"]:checked + label::before{
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 21px;
        height: 21px;
        border: 2px solid #ef1600;
        border-radius: 100%;
        background: #fff;
        }
    .radio-custom [type="radio"]:checked + label::after, .radio-custom [type="radio"]:not(:checked) + label::after {
        content: '';
        width: 11px;
        height: 11px;
        background: #ef1600;
        position: absolute;
        top: 5px;
        left: 5px;
        border-radius: 100%;
        -webkit-transition: all 0.2s ease;
        transition: all 0.2s ease;
    }
    .radio-custom [type="radio"]:not(:checked) + label:after {
        opacity: 0;
        -webkit-transform: scale(0);
        transform: scale(0);
    }
    .radio-custom [type="radio"]:checked + label:after {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }
    .radio-custom p {
        float: left;
        width: auto;
        margin-right: 10px !important;
        border: 1px solid #dedede;
        padding: 6px 8px;
    }
    .right-totals {
        float: right;
        width: 60%;
        padding-right:35px;
    }
    .totals{
        float:right;
        width:auto;
        margin-bottom:5px;
        }
    .totals p{
        color:#000;
        font-size:14px;
        font-weight:400;
        font-family: 'Roboto', sans-serif;
        text-transform:uppercase;
        line-height: 25px;
    }
    .totals p span {
        float: right;
        width: 130px;
        text-align: right;
    }
    .borders{
        float:left;
        width:100%;
        height:1px;
        background:#d1d1d1;
        margin:5px 0;
        }
    .totals h4{
        color:#000;
        font-size:20px;
        font-weight:500;
        font-family: 'Roboto', sans-serif;
        text-transform:uppercase;
        margin:0px;
        }
    .totals h4 span{
        float: right;
        width: 150px;
        text-align: right;
        }
    .check-out-bttns {
        float: right;
        width: auto;
        margin-bottom: 15px;
    }
    .check-out-bttns a {
        color: #fff;
        font-size: 16px;
        font-weight: 500;
        background: #0771d4;
        font-family: 'Poppins', sans-serif;
        padding: 8px 10px;
        display: inline-block;
        margin-left: 9px;
        border-radius: 2px;
    }
    .proc{
        background:#28a745 !important;
        /*background:#ef1600 !important;*/
        }
    .check-out-bttns a:hover{
        color:#fff;
        background:#000 !important;;
        }
    .shoping-cart-body{
        padding:30px 0 60px 0;
        }
    .sucss-loyality {
        border: 1px solid #5ed095;
        border-radius: 3px;
        padding: 10px 15px;
        background: #d7fcdb;
        color: #01631d;
        font-size: 16px;
        font-weight: 400;
        font-family: 'Roboto', sans-serif;
        display: inline-block;
        margin-top: 43px !important;
        float: left;
    }
    .sucss-loyality img{
        float:left;
        margin-right:12px;
        }
    .check-out-sucs .left-point{
        width:60%;
        }
    .check-out-sucs .right-totals{
        width:40%;
        }


    .coupon-area{
        /*width: 100%;*/
        float: left;
        margin-top: 15px;
        position: relative;
    }
    .coupon-area input{
        width: 300px
    }
    .apply-btn{
        position: absolute;
        right: 10px;
        top: 9px;
        background: none;
        border: none;
        color: #ef1600;
        font-weight: 500;
    }

    .out_stock{
        font-size: 11px;
    }

    .modify-cart-table .tab_head_sheet{
        background: #2486e1;
        color:#fff;
        }
    .modify-cart-table .tab_head_sheet_1 p{
        line-height:85px;
        }
    .modify-cart-table .ccount{
        margin-top:23px;
        }
    .modify-cart-table .dlt {
        margin-top: 31px;
        float: left;
    }
    .modify-cart-table .left-point {
        padding: 7px 15px;
    }
    .name_field .form-group {
        height: 75px;
        float: left;
        width: 100%;
        margin: 0px;
    }
    .name_field .form-group input{
        margin-bottom:0px !important;
        }
    .address-dtls p strong {
        font-weight: 400;
        float: left;
        width: 73%;
    }
    .modify-cart-table .tab_head_sheet_1{
        padding:10px 8px;
        }


    .info-area {
        float: left;
        width: 100%;
        border: 1px solid #dedede;
        padding: 20px 20px 10px 20px;
        margin-top: 35px;
    }
    .info-area h3{
        color: #000;
        font-size: 28px;
        font-weight:500;
        font-family: 'Roboto', sans-serif;
        margin-bottom: 5px;
        }
    .info-area .radio-custom{
        margin-bottom:25px;
        }
    .info-area .radio-custom p{
        padding:10px;
        }
    .chk-page {
        float: left;
        width: 100%;
        text-align: right;
        padding: 20px 0px;
        border-bottom: 1px solid #dcdcdc;
    }
    .address-bbox{
        float:left;
        width:49%;
        border:1px solid #dcdcdc;
        border-radius:1px;
        margin-top:35px;
        margin-right:2%;
        }
    .head-adrs-box {
        float: left;
        width: 100%;
        padding: 15px;
        border-bottom: 1px solid #dcdcdc;
    }
    .head-adrs-box h4{
        color: #000;
        font-size: 23px;
        font-weight:500;
        font-family: 'Roboto', sans-serif;
        margin:0px;
        float:left;
        width:auto;
        }
    .head-adrs-box a {
        color: #fff;
        font-size: 13px;
        font-weight: 400;
        background: #ef1600;
        font-family: 'Poppins', sans-serif;
        float: right;
        padding: 6px 10px 4px 10px;
    }
    .head-adrs-box a:hover{
        background:#2486e1;
        color:#fff;
        }
    .address-dtls {
        float: left;
        width: 100%;
    }
    .address-dtls p {
        color: #54585a;
        font-size: 14px;
        font-weight: 400;
        font-family: 'Roboto', sans-serif;
        line-height: 28px;
        padding: 0px 20px;
    }
    .address-dtls p span{
        float:left;
        width:120px;
        border-right:1px solid #dcdcdc;
        color:#454545;
        margin-right:15px;
        }
    .payment-method{
        float:left;
        width:100%;
        border:1px solid #dcdcdc;
        padding:20px;
        margin-top:35px;
        border-radius:1px;
        }
    .payment-method h4{
        color: #000;
        font-size:21px;
        font-weight: 500;
        font-family: 'Roboto', sans-serif;
        margin:0px;
        }
    .payment-method h4 span{
        color:#00bc1b;
        }
    .saved-address{
        float:left;
        width:100%;
        }
    .saved-address .shipping-box h3{
        font-size:17px;
        }
    .saved-address .shipping-box p{
        font-size:16px;
        }
    .saved-address .shipping-box{
        min-height:180px;
        }
    .pls-rev {
        float: left;
        width: 100%;
        color: #000;
        font-size: 18px;
        font-weight: 500;
        font-family: 'Roboto', sans-serif;
        margin-bottom: 13px !important;
    }
    .add {
        top: 0px;
        position: absolute;
        font-size: 22px;
        color: #7b7b7b;
        border: 1px solid #ccc;
        width: 33px;
        text-align: center;
        cursor: pointer;
        height: 40px;
        left: 0px;
        line-height: 34px;
        font-weight: 700;
    }
    .sub {
        top: 0px;
        position: absolute;
        font-size: 22px;
        color: #7b7b7b;
        border: 1px solid #ccc;
        width: 33px;
        text-align: center;
        cursor: pointer;
        height: 40px;
        right: 0px;
        line-height: 34px;
        font-weight: 700;
    }
    .spinner {
        width: 110px;
        position: relative;
        display: inline-block;
    }
    .login_typec {
        background: #fff;
        height: 40px;
        border: none;
        border-radius: 2px;
        width: 100%;
        padding: 7px 32px;
        color: #333;
        font-family: 'Poppins', sans-serif;
        font-size: 14px;
        font-weight: 400;
        float: left;
        appearance: none;
        -moz-appearance: none;
        -webkit-appearance: none;
        border: 1px solid #cecece;
        text-align: center;
    }

    .one-main {
        display: inline-block;
        width: 100%;
        margin-bottom: 14px;
        position: relative;
        float: left;
    }
    .from-label {
        float: left;
        width: 100%;
        text-align: left;
        color: #2f2f2f;
        font-size: 16px;
        font-weight: normal;
        font-family: 'Poppins', sans-serif;
        margin-bottom: 4px;
    }
    .login_type {
        background: #fff;
        height: 50px;
        border: none;
        border-radius: 2px;
        width: 100%;
        padding: 10px 15px;
        color: #333;
        font-family: 'Poppins', sans-serif;
        font-size: 14px;
        font-weight: 400;
        float: left;
        appearance: none;
        -moz-appearance: none;
        -webkit-appearance: none;
        border: 1px solid #454545;
    }
    .country_code {
        /* display: inline-block; */
        position: absolute;
        left: 0;
        top: 28px;
        background: #fff;
        padding: 12px;
        border: solid 1px #000;
        border-right: none;
        width: 80px;
        height: 50px;
    }
    .shipping-box {
        border: 1px solid #454545;
        border-radius: 4px;
        padding: 20px;
        float: left;
        width: 100%;
        min-height: 193px;
    }
    .saved-address .shipping-box {
        min-height: 180px;
    }
    .shipping-box h3 i {
        float: right;
        width: 22px;
        height: 22px;
        border-radius: 50%;
        background: #0771d4;
        line-height: 22px;
        color: #fff;
        font-size: 10px;
        text-align: center;
        cursor: pointer;
    }
    .shipping-box label {
        display: inline;
    }
    .shipping-box h3 i:hover {
        background: #d61401;
    }
    .mrgn_btm{
        margin-bottom: 15px;
    }
    .city-clear {
        position: absolute;
        right: 15px;
        top: 37px;
        font-size: 20px;
        cursor: pointer;
        z-index: 9;
    }
    .check-out-bttns button {
        color: #fff;
        font-size: 16px;
        font-weight: 500;
        background: #0771d4;
        font-family: 'Poppins', sans-serif;
        padding: 8px 10px;
        display: inline-block;
        margin-left: 9px;
        border-radius: 2px;
        cursor: pointer;
        border: navajowhite;
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">
                            @lang('admin_lang.e_commerce_dashboard_template')
                        </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        Edit Internal order
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">Edit Internal Order
                                <a class="adbtn btn btn-primary" href="{{ route('admin.list.order') }}"><i class="fas fa-less-than"></i> @lang('admin_lang.back')</a>
                            </h5>
                            @php
                                $p_date = "2021-03-24";
                            @endphp
                            <div class="card-body">
                                @if(@$orderMaster->created_at < $p_date)
                                    <div class="alert alert-warning">
                                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 
                                        You Can not update order created before date 24-03-2021
                                    </div>
                                @endif
                                <div class="main-dash">
                                   
                                    <form action="{{ route('admin.update.internal.order', $order->id) }}" method="post">
                                        @csrf
                                        {{-- @dd($order) --}}
                                        {{-- <h4 class="pls-rev">@lang('front_static.please_review_your_order')</h4> --}}
                                        <div class="cart-tabel modify-cart-table">
                                            <div class="table-responsive">
                                                <div class="table">
                                                    <div class="one_row1 hidden-sm-down only_shawo">
                                                        <div class="cell1 tab_head_sheet">@lang('front_static.product_details')</div>
                                                        <div class="cell1 tab_head_sheet">@lang('front_static.seller')</div>
                                                        <div class="cell1 tab_head_sheet">@lang('front_static.quantity')</div>
                                                        <div class="cell1 tab_head_sheet">@lang('front_static.unit_price') </div>
                                                        <div class="cell1 tab_head_sheet">@lang('front_static.total')</div>
                                                        <div class="cell1 tab_head_sheet"></div>
                                                    </div>
                                                    {{-- @dd($orderDetails)                         --}}
                                                    @foreach(@$orderDetails as $od)    
                                                    <div class="one_row1 small_screen31">
                                                        <div class="cell1 tab_head_sheet_1 lft">
                                                            <span class="W55_1">@lang('front_static.product_details')</span>
                                                            <span class="add_ttrr">
                                                                <a href="{{ route('product',@$od->productDetails->slug) }}"> 
                                                                    @if(@$od->defaultImage->image)
                                                                    <span class="tabel-image"><img src="{{url('storage/app/public/products/'.@$od->defaultImage->image)}}" alt=""></span>
                                                                    @else
                                                                    <span class="tabel-image">
                                                                        <img src="{{ getDefaultImageUrl() }}" alt="">
                                                                    </span>
                                                                    @endif
                                                                </a>
                                                                <a href="{{ route('product',@$od->productDetails->slug) }}"> 
                                                                    <h5>{{ $od->productByLanguage->title }}</h5>
                                                                </a>
                                                                @if(json_decode($od->variants) != NULL)
                                                                @foreach(json_decode($od->variants) as $row)
                                                                <h6>{{ $row->{$language_id}->variant }} : {{ $row->{$language_id}->variant_value }}</h6>
                                                                @endforeach
                                                                @endif
                                                            </span>
                                                        </div>
                                                        <div class="cell1 tab_head_sheet_1">
                                                            <span class="W55_1">@lang('front_static.seller')</span>
                                                            <span class="add_ttrr">
                                                                @if(@$od->sellerDetails->image)
                                                                <span class="seller-logo"><img src="{{url('storage/app/public/profile_pics/'.@$od->sellerDetails->image)}}" alt=""></span>
                                                                @else
                                                                <span class="seller-logo">
                                                                    <img src="{{ getDefaultImageUrl() }}" alt="">
                                                                </span>
                                                                @endif
                                                                <h4>{{ @$od->sellerDetails->company_name }}</h4>
                                                            </span>
                                                        </div>
                                                        <div class="cell1 tab_head_sheet_1">
                                                            <span class="W55_1">@lang('front_static.quantity')</span>
                                                            <span class="add_ttrr">
                                                                <div class="ccount">
                                                                    <input class="login_typec number-qty" type="number" min="1" max="100" value="{{ $od->quantity }}" data-id="{{ @$od->id }}" data-product="{{ $od->product_id }}" data-variant="{{ $od->product_variant_id }}" />
                                                                </div>
                                                            </span>
                                                        </div>
                                                        <div class="cell1 tab_head_sheet_1">
                                                            <span class="W55_1">@lang('front_static.unit_price') </span>
                                                            <p class="add_ttrr">{{ number_format($od->original_price, 3) }} {{ getCurrency() }}</p>
                                                        </div>
                                                        <div class="cell1 tab_head_sheet_1">
                                                            <span class="W55_1">@lang('front_static.total')</span>
                                                            <p class="add_ttrr">{{ number_format($od->sub_total, 3) }} {{ getCurrency() }}</p>
                                                        </div>

                                                        <div class="cell1 tab_head_sheet_1">
                                                            <span class="W55_1">@lang('front_static.action')</span>
                                                            <p class="add_ttrr"><a class="dlt" href="{{ route('admin.remove.item.from.order', [$od->id, $order->id]) }}" onclick="return confirm('Do you really want to remove the item from order?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a></p>
                                                        </div>
                                                    </div>
                                                    @endforeach 
                                                </div>
                                            </div>
                                            <div class="cart-total-area check-out-sucs">
                                                <div class="left-point">
                                                    {{-- <div class="point-border">
                                                        <div style="float: left; width: 100%">
                                                            @php $loyalty_discount = $order->loyalty_amount; @endphp
                                                            @if(@$order->customerDetails->loyalty_balance > 0 && @$order->customerDetails->loyalty_balance >= @$setting->min_point)
                                                            <h6>@lang('front_static.you_have') {{ @$order->customerDetails->loyalty_balance }} @lang('front_static.point_available')</h6>
                                                            <p>@lang('front_static.do_you_want_to_redeem_your_point')</p>
                                                            <div class="radio-custom">
                                                                <p id="yes">
                                                                    <input type="radio" id="test1" name="reward_point" class="reward_point" value="Y" checked>
                                                                    <label for="test1">@lang('front_static.yes')</label>
                                                                </p>
                                                                <p id="no">
                                                                    <input type="radio" id="test2" name="reward_point" class="reward_point" value="N">
                                                                    <label for="test2">@lang('front_static.no')</label>
                                                                </p>
                                                            </div>
                                                            @php
                                                            if(@$order->customerDetails->loyalty_balance*@$setting->one_point_to_kwd < $order->order_total) {
                                                                $loyalty_discount = @$order->customerDetails->loyalty_balance*@$setting->one_point_to_kwd;
                                                            } else if($setting->max_discount < $order->order_total) {
                                                                $loyalty_discount = @$order->customerDetails->loyalty_balance*@$setting->one_point_to_kwd;
                                                            } else {
                                                                $loyalty_discount = $order->order_total;
                                                            }

                                                            if($loyalty_discount > @$setting->max_discount) {
                                                                $loyalty_discount = @$setting->max_discount;
                                                                $point_used = $loyalty_discount/@$setting->one_point_to_kwd;
                                                            } else {
                                                                $point_used = @$order->customerDetails->loyalty_balance;
                                                            }
                                                            @endphp
                                                            <input type="hidden" name="loyalty_point" value="{{ number_format(@$point_used, 3, '.', '') }}">
                                                            @endif
                                                        </div>
                                                        <div class="coupon-area">
                                                            <input type="text" name="coupon_code" id="coupon_code" class="form-control" placeholder="Enter coupon code" autocomplete="off">
                                                            <input type="hidden" name="valid_coupon_code" id="valid_coupon_code">
                                                            <button class="apply-btn" id="apply" type="button">APPLY</button>
                                                            <span id="coupon_msg"></span>
                                                        </div>
                                                    </div> --}}
                                                </div>
                                                
                                                <div class="right-totals">
                                                    <div class="totals">
                                                        <p>@lang('front_static.subtotal') <span style="width: auto; margin-left: 5px; color: #2ea745;">{{ number_format($order->subtotal - $order->total_discount, 3) }} {{ getCurrency() }}</span> <span style="text-decoration: line-through; font-size: 12px">{{ number_format($order->subtotal, 3) }} {{ getCurrency() }}</span></p>
                                                        <p>@lang('front_static.loyalty_point_discount') <span class="text-success loyalty_discount">- {{ number_format(@$loyalty_discount, 3) }} {{ getCurrency() }}</span></p>
                                                        @if(@$order->coupon_code)
                                                        <p>Coupon Discount <span class="apply_coupon">- {{ number_format(@$order->coupon_discount, 3) }} {{ getCurrency() }}</span></p>
                                                        @endif
                                                        <p>@lang('front_static.shipping_charges') <span class="shipping_charges">{{ number_format($order->shipping_price, 3) }} {{ getCurrency() }}</span></p>
                                                        <p>@lang('front_static.total_discount') <span class="text-success total_discount"> - {{ number_format($order->total_discount + @$loyalty_discount, 3) }} {{ getCurrency() }}</span></p>
                                                        <div class="borders"></div>
                                                        <h4>@lang('front_static.total') <span class="order_total">{{ number_format($order->order_total-number_format(@$loyalty_discount, 3), 3) }} {{ getCurrency() }}</span></h4>
                                                        <div class="borders"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="info-area">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3>@lang('front_static.shipping_information')</h3>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="radio-custom">
                                                        <p>
                                                            <input type="radio" id="test1" name="radio-group1" checked="" class="ship_address" value="1">
                                                            <label for="test1">@lang('front_static.create_new_address')</label>
                                                        </p>
                                                        <p>
                                                            <input type="radio" id="test2" name="radio-group1" class="ship_address" value="2">
                                                            <label for="test2">@lang('front_static.use_saved_address')</label>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="new-adrs-area new1">
                                                {{-- <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="radio-custom">
                                                            Do you want to add it to address book?
                                                            <div style="display: inline-block;">
                                                                <input type="radio" id="save_address1" name="save_address" checked="" value="Y">
                                                                <label for="save_address1">Yes</label>
                                                            </div>
                                                            <div style="display: inline-block;">
                                                                <input type="radio" id="save_address2" name="save_address" value="N">
                                                                <label for="save_address2">No</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.first_name')</label>
                                                            <input type="text" class="login_type required shp_inp_msg" placeholder="@lang('front_static.enter_your_first_name')" name="shipping_fname" id="shipping_fname" value="{{ @$orderMaster->shipping_fname }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.last_name')</label>
                                                            <input type="text" class="login_type required shp_inp_msg" placeholder="@lang('front_static.enter_your_last_name')" name="shipping_lname" id="shipping_lname" value="{{ @$orderMaster->shipping_lname }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.email_address')</label>
                                                            <input type="text" class="login_type required shp_inp_msg" placeholder="@lang('front_static.enter_email_address')" name="shipping_email" id="shipping_email" value="{{ @$orderMaster->shipping_email }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.country')</label>
                                                            <select class="login_type login-select required shp_inp_msg" name="shipping_country" id="shipping_country">
                                                                <option value="">@lang('front_static.select_country')</option>
                                                                @foreach($country as $cn)
                                                                <option value="{{ $cn->id }}" @if(@$orderMaster->shipping_country == $cn->id) selected="" @endif data-country_code="{{ @$cn->countryDetailsBylanguage->country_code }}">{{ @$cn->countryDetailsBylanguage->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.phone_number')</label>
                                                            <span class="country_code" id="shipping_country_code">{{ @$orderMaster->getCountry->country_code }}</span><input style="width: 80%; float: right;" type="text" class="login_type required shp_inp_msg" placeholder="@lang('front_static.enter_your_mobile_number')" name="shipping_phone" id="shipping_phone" value="{{ @$orderMaster->shipping_phone }}" onkeypress='validate(event)'>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-lg-4 col-md-6 col-sm-12" id="shipping_kuCity" @if(@$orderMaster->shipping_country != 134) style="display: none;" @endif>
                                                        <div class="one-main" style="position: relative;">
                                                            <label class="from-label">@lang('front_static.city')</label>
                                                        
                                                            <input autocomplete="off" type="text" class="login-select login_type required shp_inp_msg" placeholder="@lang('front_static.enter_your_city_name')" name="shipping_city_id" id="shipping_city_id" @if(@$orderMaster->shipping_country == 134) value="{{ @$orderMaster->shipping_city }}"  readonly="" @endif>
                                                            <div id="shpCityList"></div>
                                                            @if(@$orderMaster->shipping_country == 134)
                                                            <span id="shpCityClr" class="city-clear"><i class="fa fa-times-circle"></i></span>
                                                            @else
                                                            <span id="shpCityClr" class="city-clear" style="display: none;"><i class="fa fa-times-circle"></i></span>
                                                            @endif
                                                            <span class="text-danger shipping_city_id_err"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12" @if(@$orderMaster->shipping_country == 134) style="display: none;" @endif id="shipping_notKuCity">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.city')</label>
                                                            <input type="text" class="login_type required shp_inp_msg" placeholder="@lang('front_static.enter_your_city_name')" name="shipping_city" id="shipping_city" @if(@$orderMaster->shipping_country != 134) value="{{ @$orderMaster->shipping_city }}" @endif>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.street')</label>
                                                            <input type="text" class="login_type required shp_inp_msg" placeholder="@lang('front_static.enter_your_street_here')" name="shipping_street" id="shipping_street" value="{{ @$orderMaster->shipping_street }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12" id="shipping_postal_code_div" @if(@$orderMaster->shipping_country == 134) style="display: none;" @endif>
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.postal_code') (@lang('front_static.optional'))</label>
                                                            <input type="text" class="login_type shp_inp_msg" placeholder="@lang('front_static.enter_postal_code')" name="shipping_postal_code" id="shipping_postal_code" value="{{ @$orderMaster->shipping_postal_code }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.block') <span class="shipping_optShow" @if(@$orderMaster->shipping_country == 134) style="display: none;" @endif> (@lang('front_static.optional'))</span></label>
                                                            <input type="text" class="login_type  @if(@$orderMaster->shipping_country == 134) required @endif shp_inp_msg" placeholder="@lang('front_static.enter_your_block_here')" name="shipping_block" id="shipping_block" value="{{ @$orderMaster->shipping_block }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.building') <span class="shipping_optShow" @if(@$orderMaster->shipping_country == 134) style="display: none;" @endif> (@lang('front_static.optional'))</span></label>
                                                            <input type="text" class="login_type  @if(@$orderMaster->shipping_country == 134) required @endif shp_inp_msg" placeholder="@lang('front_static.enter_your_building_here')" name="shipping_building" id="shipping_building" value="{{ @$orderMaster->shipping_building }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.location') (@lang('front_static.optional'))</label>
                                                            <input id="pac-input" type="text" placeholder="@lang('front_static.location')" name="location" class="login_type" value="{{ @$orderMaster->location }}">
                                                            <input type="hidden" name="lat" id="lat"  value="{{ @$orderMaster->lat }}">
                                                            <input type="hidden" name="lng" id="lng"  value="{{ @$orderMaster->lng }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.more_address_details') (@lang('front_static.optional'))</label>
                                                            <input type="text" class="login_type shp_inp_msg" placeholder="@lang('front_static.enter_your_more_address_details')" name="shipping_more_address" id="shipping_more_address" value="{{ @$orderMaster->shipping_more_address }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="saved-address save1" style="display:none">
                                                <strong id="shp_msg" class="text-danger"></strong>
                                                <label for="shp_save_addr" generated="true" class="error" style="display:none; color: red !important; background: none!important; border: none!important;">@lang('front_static.you_must_check_at_least_one_shipping_address')</label>
                                                <div class="row">
                                                    @foreach($userAddr as $key => $ua)
                                                    <div class="col-lg-4 col-md-6 col-sm-12 mrgn_btm">
                                                        <div class="shipping-box">
                                                            <h3>@lang('front_static.saved_address') {{ $key+1 }} 
                                                                <label for="addr{{ $key+1 }}">
                                                                    <i class="fa fa-check shp-label" aria-hidden="true" data-id="{{ $key+1 }}"></i>
                                                                </label>
                                                                <input type="checkbox" class="addr-inp" id="addr{{ $key+1 }}" name="shp_save_addr" value="{{ $ua->id }}" style="visibility: hidden;">
                                                            </h3>
                                                            <p>{{ $ua->shipping_fname }} {{ $ua->shipping_lname }}</p>
                                                            <p>{{ $ua->city }}, {{ $ua->street }},
                                                                @if($ua->block)
                                                                {{ $ua->block }},
                                                                @endif
                                                                @if($ua->building) {{ $ua->building }},
                                                                @endif
                                                                @if($ua->more_address) {{ $ua->more_address }},@endif
                                                                {{ @$ua->getCountry->name }}
                                                                @if($ua->postal_code), {{ $ua->postal_code }}@endif
                                                            </p>
                                                            <p>@lang('front_static.email') : {{ $ua->email }}</p>
                                                            <p>@lang('front_static.phone_no') : {{ $ua->phone }}</p>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="info-area">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3>@lang('front_static.billing_information')</h3>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="radio-custom">
                                                        <p>
                                                            <input type="radio" id="test3" name="radio-group" checked="" class="bill_address" value="1">
                                                            <label for="test3">@lang('front_static.create_new_address')</label>
                                                        </p>
                                                        <p>
                                                            <input type="radio" id="test4" name="radio-group" class="bill_address" value="2">
                                                            <label for="test4">@lang('front_static.use_saved_address')</label>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group check_group logchk">
                                                        <div class="checkbox-group"> 
                                                            <input id="checkiz" type="checkbox" name="same_as_shipping" checked=""> 
                                                            <label for="checkiz" class="Fs16">
                                                                <span class="check find_chek"></span>
                                                                <span class="box W25 boxx"></span>
                                                                @lang('front_static.billing_address_same_as_shipping')
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="new-adrs-area new2">
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.first_name')</label>
                                                            <input type="text" class="login_type required" placeholder="@lang('front_static.enter_your_first_name')" name="billing_fname" id="billing_fname" value="{{ @$orderMaster->billing_fname }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.last_name')</label>
                                                            <input type="text" class="login_type required" placeholder="@lang('front_static.enter_your_last_name')" name="billing_lname" id="billing_lname" value="{{ @$orderMaster->billing_lname }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.email_address')</label>
                                                            <input type="text" class="login_type required" placeholder="@lang('front_static.enter_email_address')" name="billing_email" id="billing_email" value="{{ @$orderMaster->billing_email }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.country')</label>
                                                            <select class="login_type login-select required" name="billing_country" id="billing_country">
                                                                <option value="">@lang('front_static.select_country')</option>
                                                                @foreach($country as $cn)
                                                                <option value="{{ $cn->id }}" @if(@$orderMaster->billing_country == $cn->id) selected="" @endif data-country_code="{{ @$cn->countryDetailsBylanguage->country_code }}">{{ @$cn->countryDetailsBylanguage->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.phone_number')</label>
                                                            <span class="country_code" id="billing_country_code">{{ @$orderMaster->getBillingCountry->country_code }}</span><input style="width: 80%; float: right;" type="text" class="login_type required" placeholder="@lang('front_static.enter_your_mobile_number')" name="billing_phone" id="billing_phone" value="{{ @$orderMaster->billing_phone }}" onkeypress='validate(event)'>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-lg-4 col-md-6 col-sm-12" id="billing_kuCity" @if(@$orderMaster->billing_country != 134) style="display: none;" @endif>
                                                        <div class="one-main" style="position: relative;">
                                                            <label class="from-label">@lang('front_static.city')</label>
                                                            <input type="text" class="login-select login_type required" placeholder="@lang('front_static.enter_your_city_name')" name="billing_city_id" id="billing_city_id" @if(@$orderMaster->billing_country == 134) value="{{ @$orderMaster->billing_city }}"  readonly="" @endif>
                                                            <div id="billCityList"></div>
                                                            @if(@$orderMaster->billing_country == 134)
                                                            <span id="billCityClr" class="city-clear"><i class="fa fa-times-circle"></i></span>
                                                            @else
                                                            <span id="billCityClr" class="city-clear" style="display: none;"><i class="fa fa-times-circle"></i></span>
                                                            @endif
                                                            <span class="text-danger billing_city_id_err"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12" @if(@$orderMaster->billing_country == 134) style="display: none;" @endif id="billing_notKuCity">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.city')</label>
                                                            <input type="text" class="login_type required" placeholder="@lang('front_static.enter_your_city_name')" name="billing_city" id="billing_city" @if(@$orderMaster->billing_country != 134) value="{{ @$orderMaster->billing_city }}" @endif>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.street')</label>
                                                            <input type="text" class="login_type required" placeholder="@lang('front_static.enter_your_street_here')" name="billing_street" id="billing_street" value="{{ @$orderMaster->billing_street }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12" id="billing_postal_code_div" @if(@$orderMaster->billing_country == 134) style="display: none;" @endif>
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.postal_code') (@lang('front_static.optional'))</label>
                                                            <input type="text" class="login_type" placeholder="@lang('front_static.enter_postal_code')" name="billing_postal_code" id="billing_postal_code" value="{{ @$orderMaster->billing_postal_code }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.block') <span class="billing_optShow" @if(@$orderMaster->billing_country == 134) style="display: none;" @endif> (@lang('front_static.optional'))</span></label>
                                                            <input type="text" class="login_type  @if(@$orderMaster->billing_country == 134) required @endif" placeholder="@lang('front_static.enter_your_block_here')" name="billing_block" id="billing_block" value="{{ @$orderMaster->billing_block }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.building') <span class="billing_optShow" @if(@$orderMaster->billing_country == 134) style="display: none;" @endif> (@lang('front_static.optional'))</span></label>
                                                            <input type="text" class="login_type @if(@$orderMaster->billing_country == 134) required @endif" placeholder="@lang('front_static.enter_your_building_here')" name="billing_building" id="billing_building" value="{{ @$orderMaster->billing_building }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-12">
                                                        <div class="one-main">
                                                            <label class="from-label">@lang('front_static.more_address_details') (@lang('front_static.optional'))</label>
                                                            <input type="text" class="login_type" placeholder="@lang('front_static.enter_your_more_address_details')" name="billing_more_address" id="billing_more_address" value="{{ @$orderMaster->billing_more_address }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="saved-address save2" style="display:none">
                                                <strong id="bill_msg" class="text-danger"></strong>
                                                <label for="bill_save_addr" generated="true" class="error"style="display:none; color: red !important; background: none!important; border: none!important;">@lang('front_static.please_select_a_billing_address')</label>
                                                <div class="row">
                                                    @foreach($userAddr as $key => $ua)
                                                    <div class="col-lg-4 col-md-6 col-sm-12 mrgn_btm">
                                                        <div class="shipping-box">
                                                            <h3>@lang('front_static.saved_address') {{ $key+1 }} 
                                                                <label for="addrB{{ $key+1 }}">
                                                                    <i class="fa fa-check bill-label" aria-hidden="true" data-id="{{ $key+1 }}"></i>
                                                                </label>
                                                                <input type="checkbox" class="addr-bill-inp" id="addrB{{ $key+1 }}" name="bill_save_addr" value="{{ $ua->id }}" style="visibility: hidden;">
                                                            </h3>
                                                            <p>{{ $ua->shipping_fname }} {{ $ua->shipping_lname }}</p>
                                                            <p>{{ $ua->city }}, {{ $ua->street }},
                                                                @if($ua->block)
                                                                {{ $ua->block }},
                                                                @endif
                                                                @if($ua->building) {{ $ua->building }},
                                                                @endif
                                                                @if($ua->more_address) {{ $ua->more_address }},@endif
                                                                {{ @$ua->getCountry->name }}
                                                                @if($ua->postal_code), {{ $ua->postal_code }}@endif
                                                            </p>
                                                            <p>@lang('front_static.email') : {{ $ua->email }}</p>
                                                            <p>@lang('front_static.phone_no') : {{ $ua->phone }}</p>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="payment-method">
                                            <h4>@lang('front_static.payment_method') : <span>@if($order->payment_method=='C') @lang('front_static.cash_on_delivery') @else @lang('front_static.online_payment') @endif</span></h4>
                                        </div>
                                        
                                        @if(@$orderMaster->created_at > $p_date)
                                        <div class="check-out-bttns chk-page">
                                            @if($order->payment_method=='C')
                                            <button type="submit" class="proc">@lang('front_static.confirm_and_place_order')</button>
                                            @else
                                            <button type="submit" class="proc">@lang('front_static.confirm_and_pay')</button>
                                            @endif
                                        </div>
                                        @endif
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.includes.footer')
</div>
<!-- footer -->

<!-- end footer -->
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif

<script src="{{ asset('public/frontend/toastr/toastr.js') }}"></script>
<script>
    $(document).ready(function() {
        var loyalty_discount        = parseFloat('{{ @$loyalty_discount }}')
        if(loyalty_discount > 0) {
            appliedLoyalty = true
        }
        var total_discount          = parseFloat('{{ $order->total_discount }}') + loyalty_discount
        var shipping_price          = parseFloat('{{ $order->shipping_price }}')
        var order_total             = parseFloat('{{ $order->order_total }}') - loyalty_discount
        var coupon_discount_on_sub  = 0;
        var coupon_discount_on_sip  = 0;
        var appliedLoyalty          = false;
        
        $('.reward_point').click(function(){
            if($(this).val() == 'Y') {
                loyalty_discount = parseFloat('{{ @$loyalty_discount }}')
                total_discount = parseFloat(total_discount) + parseFloat(loyalty_discount)
                order_total = order_total - loyalty_discount
                appliedLoyalty = true
                $('.loyalty_discount').html('- ' + parseFloat(loyalty_discount).toFixed(3) + ' KWD');
                $('.total_discount').html('- ' + (total_discount + coupon_discount_on_sub).toFixed(3) + ' KWD');
                $('.order_total').html((order_total - coupon_discount_on_sub).toFixed(3) + ' KWD');
            } else {
                total_discount = parseFloat(total_discount) - parseFloat(loyalty_discount)
                order_total = order_total + loyalty_discount
                loyalty_discount = 0;
                appliedLoyalty = false;
                $('.loyalty_discount').html('- ' + loyalty_discount.toFixed(3) + ' KWD');
                $('.total_discount').html('- ' + (total_discount + coupon_discount_on_sub).toFixed(3) + ' KWD');
                $('.order_total').html((order_total - coupon_discount_on_sub).toFixed(3) + ' KWD');
            }
        });
        
        $('#apply').click(function() {
            var couponCode = $('#coupon_code').val()
            var reqData = {
                jsonrpc: '2.0',
                _token: '{{ csrf_token() }}',
                params: {
                    order_id: '{{ $order->id }}',
                    coupon_code: couponCode
                }
            }
            $.ajax({
                url: '{{ route('apply.coupon') }}',
                type: 'POST',
                data: reqData,
                success: function(response) {
                    if(response.error) {
                        $('#coupon_msg').html('<i class="fa fa-exclamation-circle"></i> ' + response.error.message);
                        $('#coupon_msg').css('color', '#f00')
                        $('#coupon_code').val('')
                    } else {
                        $('#valid_coupon_code').val(couponCode)
                        if(response.result.discount_for == 'SUB') {
                            coupon_discount_on_sub = parseFloat(response.result.coupon_discount)
                            $('#coupon_msg').html('<i class="fa fa-check"></i> Get ' + (coupon_discount_on_sub).toFixed(3) + ' KWD OFF on subtotal!')
                            $('#coupon_msg').css('color', '#2ea745')
                            if(appliedLoyalty == false) {
                                $('.total_discount').html('- ' + (parseFloat(total_discount) + parseFloat(coupon_discount_on_sub)).toFixed(3) + ' KWD');
                                $('.order_total').html((order_total - coupon_discount_on_sub).toFixed(3) + ' KWD')
                            } else {
                                $('.total_discount').html('- ' + (parseFloat(total_discount) + parseFloat(loyalty_discount) + parseFloat(coupon_discount_on_sub)).toFixed(3) + ' KWD');
                                $('.order_total').html((order_total - coupon_discount_on_sub).toFixed(3) + ' KWD')
                            }
                        } else if(response.result.discount_for == 'SHI') {
                            coupon_discount_on_sip = response.result.coupon_discount
                            if((shipping_price - coupon_discount_on_sip) == 0) {
                                $('.shipping_charges').html('<span style="width: auto; float: left; text-decoration: line-through;">' + shipping_price + '</span> <span style="width: auto">FREE</span>')    
                            } else {
                                $('.shipping_charges').html('<span style="width: auto; float: left; text-decoration: line-through;">' + shipping_price + '</span> <span style="width: auto">' + (shipping_price - coupon_discount_on_sip) + '</span>')    
                            }
                            $('.order_total').html((order_total - coupon_discount_on_sip).toFixed(3) + ' KWD')
                            if(coupon_discount_on_sip == 100) {
                                $('#coupon_msg').html('<i class="fa fa-check"></i> Get free shipping')
                                $('#coupon_msg').css('color', '#2ea745')
                            } else {
                                $('#coupon_msg').html('<i class="fa fa-check"></i> Get ' + (coupon_discount_on_sip).toFixed(3) + ' KWD OFF on subtotal!')
                                $('#coupon_msg').css('color', '#2ea745')    
                            }
                        }
                        $('.apply_coupon').html('- ' + parseFloat(response.result.coupon_discount).toFixed(3) + ' KWD')
                        $('.apply_coupon').css('color', '#28a745')
                    }
                }
            })
        });

        $('.apply_coupon').click(function() {
            $('#coupon_code').focus();
        })
    });

    $(document).ready(function() {
        (function($) {
            $.fn.spinner = function() {
                this.each(function() {
                    var el = $(this);
                    // add elements
                    el.wrap('<span class="spinner"></span>');
                    el.after('<span class="add">+</span>');
                    el.before('<span class="sub">-</span>');

                    // substract
                    el.parent().on('click', '.sub', function () {
                      if (el.val() > parseInt(el.attr('min')))
                        el.val( function(i, oldval) { return --oldval; });
                        updateQty(el.val(), el.data('id'), el.data('product'), el.data('variant'))
                    });

                    // increment
                    el.parent().on('click', '.add', function () {
                      if (el.val() < parseInt(el.attr('max')))
                        el.val( function(i, oldval) { return ++oldval; });
                        updateQty(el.val(), el.data('id'), el.data('product'), el.data('variant'))
                    });
                });
            };
        })(jQuery);
        $('input[type=number]').spinner();
    });
    

    function updateQty(qty, orderDetailsId, productId, productVariantId) {
        var reqData = {
            jsonrpc: '2.0',
            _token: '{{ csrf_token() }}',
            params: {
                quantity: qty,
                order_details_id: orderDetailsId,
                order_master_id: '{{ @$order->id }}',
                product_id: productId,
                product_variant_id: productVariantId
            }
        };

        $.ajax({
            url: '{{ route('admin.update.internal.order.qty') }}',
            type: 'post',
            data: reqData,
            dataType: 'json',
            success: function(response) {
                console.log(response.error)
                if(response.error) {
                    console.log(response.error)
                    toastr.error('[' + response.error.code + ']: ' + response.error.meaning);
                    setTimeout(function(){
                        location.reload();
                    },2000);
                } else {
                    location.reload();
                }
            }
        })
    }

    $(document).ready(function() {
        $('.ship_address').click(function(){
            if($(this).is(':checked')) {
                if($(this).val()==1) {
                    $('.new1').show();
                    $('.save1').hide();
                }
                if($(this).val()==2) {
                    $('.new1').hide();
                    $('.save1').show();
                }
            }
        });
        $('.bill_address').click(function(){
            if($(this).is(':checked')) {
                if($(this).val()==1) {
                    $('.new2').show();
                    $('.save2').hide();
                }
                if($(this).val()==2) {
                    $('.new2').hide();
                    $('.save2').show();
                }
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#checkiz').click(function() {
            if($(this).is(':checked')) {
                var fname   = $('#shipping_fname').val();
                var lname   = $('#shipping_lname').val();
                var email   = $('#shipping_email').val();
                var phone   = $('#shipping_phone').val();
                var country = $('#shipping_country').val();
                var street  = $('#shipping_street').val();
                var block   = $('#shipping_block').val();
                var building= $('#shipping_building').val();
                var postal_code   = $('#shipping_postal_code').val();
                var more_address  = $('#shipping_more_address').val();

                var city    = $('#shipping_city').val();
                if(city == '') {
                    city = $('#shipping_city_id').val();
                }

                $('#billing_fname').val(fname);
                $('#billing_lname').val(lname);
                $('#billing_email').val(email);
                $('#billing_phone').val(phone);
                $('#billing_country').val(country);
                // $('#billing_city').val(city);
                $('#billing_street').val(street);
                $('#billing_block').val(block);
                $('#billing_building').val(building);
                $('#billing_postal_code').val(postal_code);
                $('#billing_more_address').val(more_address);
                $('#billing_country_code').html($('#shipping_country').find(':selected').data('country_code'));
                if(country == 134) {
                    $('#billing_notKuCity').hide();
                    $('#billing_kuCity').show();
                    $('#billing_city_id').val(city);
                    $('#billing_city_id').attr("disabled", true);
                } else {
                    $('#billing_kuCity').hide();
                    $('#billing_notKuCity').show();
                    $('#billing_city').val(city);                    
                    $('#billing_city').attr("disabled", true);
                }

                $('#billing_fname').attr("disabled", true);
                $('#billing_lname').attr("disabled", true);
                $('#billing_email').attr("disabled", true);
                $('#billing_phone').attr("disabled", true);
                $('#billing_country').attr("disabled", true);
                $('#billing_street').attr("disabled", true);
                $('#billing_block').attr("disabled", true);
                $('#billing_building').attr("disabled", true);
                $('#billing_postal_code').attr("disabled", true);
                $('#billing_more_address').attr("disabled", true);

                if(country != 134) {
                    $('.billing_optShow').show('required');
                } else {
                    $('.billing_optShow').hide('required');                    
                }
            } else {
                $('#billing_fname').val('');
                $('#billing_lname').val('');
                $('#billing_email').val('');
                $('#billing_phone').val('');
                $('#billing_country').val('');
                $('#billing_city').val('');
                $('#billing_city_id').val('');
                $('#billing_street').val('');
                $('#billing_block').val('');
                $('#billing_building').val('');
                $('#billing_postal_code').val('');
                $('#billing_more_address').val('');
                $('#billing_country_code').html('+');
                
                $('#billing_fname').attr("disabled", false);
                $('#billing_lname').attr("disabled", false);
                $('#billing_email').attr("disabled", false);
                $('#billing_phone').attr("disabled", false);
                $('#billing_country').attr("disabled", false);
                $('#billing_city').attr("disabled", false);
                // $('#billing_city_id').attr("disabled", false).trigger("chosen:updated");
                $('#billing_city_id').attr("disabled", false);
                $('#billing_street').attr("disabled", false);
                $('#billing_block').attr("disabled", false);
                $('#billing_building').attr("disabled", false);
                $('#billing_postal_code').attr("disabled", false);
                $('#billing_more_address').attr("disabled", false);
            }
        });

        $('.shp-label').click(function() {
            var shId = $(this).data('id');
            if(!$("#addr"+shId).is(':checked')) {
                $('.shp-label').css('background', '#0771d4');
                $('.addr-inp').prop('checked',false);
                $(this).prop('checked',true);  
                $(this).css('background', '#d61401'); 
                $('#shp_msg').html(''); 
            } else {
                $(this).css('background', '#0771d4');
            }          
        });

        $('.bill-label').click(function() {
            var shId = $(this).data('id');
            if(!$("#addrB"+shId).is(':checked')) {
                $('.bill-label').css('background', '#0771d4');
                $('.addr-bill-inp').prop('checked',false);
                $(this).prop('checked',true);  
                $(this).css('background', '#d61401');  
                $('#bill_msg').html('');
            } else {
                $(this).css('background', '#0771d4');
            }          
        });

        $('#shipping_country').change(function() {
            var cn = $(this).val();
            if(cn != 134) {
                $('#shipping_block').removeClass('required');
                $('#shipping_block').removeClass('error');
                $('#shipping_building').removeClass('required');
                $('#shipping_building').removeClass('error');
                $('.shipping_optShow').show('required');
                $('#shipping_postal_code_div').show();
                $('#shipping_kuCity').hide();
                $('#shipping_city_id').val('');
                $('#shipping_notKuCity').show();

                // showing only online payment option if customer outside kuwait
                $('#outside_payment').show();
                $('#all_payment').hide();
            } else {
                $('#shipping_block').addClass('required');
                $('#shipping_building').addClass('required');
                $('.shipping_optShow').hide('required');
                $('#shipping_postal_code_div').hide(); 
                $('#shipping_kuCity').show();
                $('#shipping_notKuCity').hide();              
                $('#shipping_city').val('');

                $('#outside_payment').hide();
                $('#all_payment').show();                  
            }

            $('#shipping_country_code').html($(this).find(':selected').data('country_code'));
        });

        $('#billing_country').change(function() {
            var cn = $(this).val();
            if(cn != 134) {
                $('#billing_block').removeClass('required');
                $('#billing_block').removeClass('error');
                $('#billing_building').removeClass('required');
                $('#billing_building').removeClass('error');
                $('.billing_optShow').show('required');
                $('#billing_postal_code_div').show();
                $('#billing_kuCity').hide();
                $('#billing_city_id').val('');              
                $('#billing_notKuCity').show();
            } else {
                $('#billing_block').addClass('required');
                $('#billing_building').addClass('required');
                $('.billing_optShow').hide('required');
                $('#billing_postal_code_div').hide(); 
                $('#billing_kuCity').show();
                $('#billing_notKuCity').hide();              
                $('#billing_city').val('');                  
            }
            $('#billing_country_code').html($(this).find(':selected').data('country_code'));
        });
    });

    function validate(evt) {
        var theEvent = evt || window.event;
              // Handle paste
              if (theEvent.type === 'paste') {
                  key = event.clipboardData.getData('text/plain');
              } else {
              // Handle key press
              var key = theEvent.keyCode || theEvent.which;
              key = String.fromCharCode(key);
          }
          var regex = /[0-9]|\./;
          if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
</script>

{{-- city autocomplete --}}

<script>
    $(document).ready(function(){
        var flag = 0;
        $('#shipping_city_id').keyup(function(){ 
            $('.shipping_city_id_err').html('');
            var city = $(this).val();
            if(city != '') {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('autocomplete.city') }}",
                    method:"POST",
                    data:{city:city, _token:_token},
                    success:function(response){
                        if(response.status == 'ERROR') {
                            $('#shpCityList').html('nothing found');
                        } else {
                            $('#shpCityList').fadeIn();  
                            $('#shpCityList').html(response.result);
                            flag = 1;
                        }
                    }
                });
            }
        });

        $('#billing_city_id').keyup(function(){ 
            $('.billing_city_id_err').html('');
            var city = $(this).val();
            if(city != '') {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('autocomplete.city') }}",
                    method:"POST",
                    data:{city:city, _token:_token},
                    success:function(response){
                        if(response.status == 'ERROR') {
                            $('#billCityList').html('nothing found');
                        } else {
                            $('#billCityList').fadeIn();  
                            $('#billCityList').html(response.result);
                            flag = 2;
                        }
                    }
                });
            }
        });

        $('body').on('click', 'li', function(){ 
            if(flag == 1) {
                $('#shipping_city_id').val($(this).text()); 
                $('#shipping_city_id').prop('readonly', true);
                $('#shpCityList').hide();   
                $('#shpCityClr').show();
            };
            if(flag == 2) {
                $('#billing_city_id').val($(this).text());
                $('#billing_city_id').prop('readonly', true);
                $('#billCityList').hide();     
                $('#billCityClr').show();
            };
        });  
        $("body").click(function(){
            $("#shpCityList").hide();
            $("#billCityList").hide();
        });

        $('body').on('click', '#shpCityClr', function(){ 
            $(this).hide();
            $('#shipping_city_id').val('');
            $('#shipping_city_id').prop('readonly', false);
        });  

        $('body').on('click', '#billCityClr', function(){ 
            $(this).hide();
            $('#billing_city_id').val('');
            $('#billing_city_id').prop('readonly', false);
        });
    });
</script>
@endsection