@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | View Order Details') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.v_order_dtls')
@endsection
@section('links')
{{-- @include('admin.includes.links') --}}
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    /* The container */
    .container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 15px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-family: 'Circular Std Book';
    font-style: normal;
    font-weight: normal;
    }
    /* Hide the browser's default radio button */
    .container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    line-height: 1.42857143;
    }
    /* Create a custom radio button */
    .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
    border-radius: 50%;
    }
    /* On mouse-over, add a grey background color */
    .container:hover input ~ .checkmark {
    background-color: #ccc;
    }
    /* When the radio button is checked, add a blue background */
    .container input:checked ~ .checkmark {
    background-color: #2196F3;
    }
    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
    content: "";
    position: absolute;
    display: none;
    }
    /* Show the indicator (dot/circle) when checked */
    .container input:checked ~ .checkmark:after {
    display: block;
    }
    /* Style the indicator (dot/circle) */
    .container .checkmark:after {
    top: 9px;
    left: 9px;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background: white;
    }
</style>
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')

@endsection
@section('sidebar')

@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper" style="margin-left: 0px;    margin-right: 0px;">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== --> 
            <!-- pageheader --> 
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.view_order_details')</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <!-- <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li> -->
                                    <!-- <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">@lang('admin_lang.edit_view_order_dtls')</a></li> -->
                                    <!-- <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.edit_view_order_dtls')</li> -->
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="mail_success" style="display: none;">
                <div class="alert alert-success vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
                </div>
            </div>
            <div class="mail_error" style="display: none;">
                <div class="alert alert-danger vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>@lang('admin_lang.error')!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
                </div>
            </div>
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <!-- ============================================================== --> 
            <!-- end pageheader --> 
            <!-- ============================================================== -->
            <div class="row printDiv" style="">
                <!-- ============================================================== -->
                <!-- basic table  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="margin-top: -68px;">
                    <div class="card" >
                        <h5 class="card-header cardTest" style="line-height: 50px">
                            <strong style="font-size:30px">DRIVER GROUP</strong> 
                            <img src="{{ URL::to('public/admin/assets/images/logo.png')}}" alt="" style="height: 50px;width: 143px;" class="pull-right">
                            {{-- <a class="adbtn btn btn-primary" class="print_invoice" style="color: white;margin-left: 14px;" onclick="window.print()"><i class="fas fa-less-than"></i>@lang('admin_lang.print_invoice')</a> --}}
                        </h5>
                        <div class="card-body">
                            <div class="manager-dtls tsk-div">
                                <div class="row fld">
                                    {{-- Order details --}}
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.order_details')</h4>
                                        <p>
                                            <h4>
                                                <span class="titel-span" style="font-weight:600;font-size:20px">@lang('admin_lang.order_number')</span> <span class="deta-span" style="font-weight:600;font-size:20px"><strong>:</strong>
                                                {{ @$order->order_no }}
                                                </span> 
                                            </h4>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.no_of_item')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterDetails->count() }}
                                            </span> 
                                        </p>

                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.driver')</span> <span class="deta-span"><strong>:</strong>
                                            </span>
                                        </p>
                                        --}}
                                        <p><span class="titel-span">@lang('admin_lang.order_date')</span> <span class="deta-span"><strong>:</strong>{{ @$order->created_at }}</span></p>
                                        <p><span class="titel-span">Product total weight</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$total_weight }}
                                            </span>
                                            <br><br><br>
                                        </p>
                                        
                                        <p><span class="titel-span">Original Price</span> <span class="deta-span"><strong>:</strong>{{ @$order->subtotal }} {{getCurrency()}}</span></p>

                                        <p><span class="titel-span">@lang('admin_lang.tot_disc_dtls')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->total_discount }} {{getCurrency()}}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">Sub-total</span> <span class="deta-span"><strong>:</strong>{{ @$order->subtotal  - @$order->total_discount }} {{getCurrency()}}</span></p>

                                        <p><span class="titel-span">@lang('admin_lang.shipping_kwd')</span> <span class="deta-span"><strong>:</strong>{{ @$order->shipping_price }} {{getCurrency()}}</span></p>

                                        <p><span class="titel-span">@lang('admin_lang.order_total')</span> <span class="deta-span"><strong>:</strong>{{ @$order->order_total }} @lang('admin_lang.kwd') </span></p>

                                        <p><span class="titel-span">@lang('admin_lang.payment_method')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->payment_method == 'C')
                                            @lang('admin_lang.cod')
                                            @elseif(@$order->payment_method == 'O')
                                            @lang('admin_lang.online')
                                            @else
                                            N/A
                                            @endif
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.order_type')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->order_type == 'I')
                                            @lang('admin_lang.internal_1')
                                            @else
                                            @lang('admin_lang.external_1')
                                            @endif
                                            </span>
                                        </p>
                                    </div>
                                    {{-- Customer details --}}
                                    @if(@$order->order_type == 'I')
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.customer_details')</h4>
                                        <!-- @if(@$order->order_type == 'I')
                                        <p><span class="titel-span">@lang('admin_lang.country')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->userCountryDetails->countryDetailsBylanguage->name }}    
                                            @else
                                            {{ @$order->countryDetails->countryDetailsBylanguage->name }}
                                            @endif
                                            </span>
                                        </p>
                                        @endif -->
                                        {{-- name --}}
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->fname }} {{ @$order->customerDetails->lname }}
                                            @else
                                            {{ @$order->fname }} {{ @$order->lname }}
                                            @endif
                                            </span>
                                        </p>
                                        {{-- email --}}
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->email }}    
                                            @else
                                            {{ @$order->email }}
                                            @endif
                                            </span>
                                        </p>
                                        {{-- phone --}}
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->phone }}    
                                            @else
                                            {{ @$order->shipping_phone }}
                                            @endif
                                            </span>
                                        </p>
                                        @if(@$order->order_type == 'I')
                                        <!-- <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->city }}    
                                            @else
                                            {{ @$order->getCityNameByLanguage->name }}
                                            @endif
                                            </span>
                                        </p> -->
                                        <!-- <p><span class="titel-span">@lang('admin_lang.street') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            @else
                                            {{ @$order->shipping_street }}
                                            @endif
                                            </span>
                                        </p> -->
                                        <!-- <p><span class="titel-span">@lang('admin_lang.block') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_block }}
                                            </span>
                                        </p> -->
                                        <!-- <p><span class="titel-span">@lang('admin_lang.building') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_building }}
                                            </span>
                                        </p> -->
                                        <!-- @if($order->shipping_country != 134) -->
                                        <!-- <p><span class="titel-span">Postal Code</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->zipcode }}    
                                            @else
                                            {{ @$order->shipping_zip }}
                                            @endif
                                            </span>
                                        </p> -->
                                        <!-- @endif -->
                                        <!-- <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->address }}
                                            @else
                                            {{ @$order->shipping_address }}
                                            @endif
                                            </span>
                                        </p> -->
                                        @endif

                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.shipping_address')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.country')</span>
                                            <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCountry->name }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_fname }} {{ @$order->shipping_lname }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_email }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_phone }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCityNameByLanguage->name }}    
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.street')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_street }}    
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.block')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_block }}    
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.building')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_building }}    
                                            </span>
                                        </p>
                                        @if(@$order->shippingAddress->country == 134)
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_postal_code }}    
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_more_address }} 
                                            </span>
                                        </p>
                                    </div>
                                    @endif
                                    @if(@$order->order_type == 'E')
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.customer_details')</h4>
                                        @if(@$order->order_type == 'I')
                                        <p><span class="titel-span">@lang('admin_lang.country')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->userCountryDetails->countryDetailsBylanguage->name }}    
                                            @else
                                            {{ @$order->countryDetails->countryDetailsBylanguage->name }}
                                            @endif
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->fname }} {{ @$order->customerDetails->lname }}
                                            @else
                                            {{ @$order->fname }} {{ @$order->lname }}
                                            @endif
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->email }}    
                                            @else
                                            {{ @$order->email }}
                                            @endif
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->phone }}    
                                            @else
                                            {{ @$order->shipping_phone }}
                                            @endif
                                            </span>
                                        </p>
                                        @if(@$order->order_type == 'I')
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->city }}    
                                            @else
                                            {{ @$order->getCityNameByLanguage->name }}
                                            @endif
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.street') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            @else
                                            {{ @$order->shipping_street }}
                                            @endif
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.block') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_block }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.building') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_building }}
                                            </span>
                                        </p>
                                        @if($order->shipping_country != 134)
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->zipcode }}    
                                            @else
                                            {{ @$order->shipping_zip }}
                                            @endif
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->address }}
                                            @else
                                            {{ @$order->shipping_address }}
                                            @endif
                                            </span>
                                        
                                        </p>
                                        @endif
                                        <br><br><br><br>
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.shipping_address')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.country')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCountry->name }}
                                            </span>
                                        </p>
                                        {{-- name --}}
                                        @if(@$order->order_type == 'I')
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_fname }} {{ @$order->shipping_lname }}
                                            </span>
                                        </p>
                                        {{-- email --}}
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_email }}
                                            </span>
                                        </p>
                                        {{-- phone --}}
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_phone }}
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.city')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCityNameByLanguage->name }}   
                                            </span>
                                        </p>
                                        @if(@$order->order_type == 'E')
                                        <p><span class="titel-span">@lang('admin_lang.street') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_street }}    
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.block') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_block }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.building') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_building }}
                                            </span>
                                        </p>
                                        @if($order->shipping_country != 134)
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_postal_code }}    
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_address }}
                                            </span>
                                        </p>
                                        @endif
                                    </div>
                                    @endif
                                </div>
                                @if(@$order->order_type == 'E')
                                <div class="row fld">
                                    @if(@$order->external_order_type == 'S')
                                        {{-- <div class="col-md-6">
                                            <h4 class="fultxt" style="margin-bottom:10px;">@lang('merchant_lang.pickup_address')</h4>
                                            <p><span class="titel-span">@lang('admin_lang.country')</span>
                                                <span class="deta-span"><strong>:</strong>
                                                    {{ @$order->getpickupCountryDetails->name }}
                                                </span>
                                            </p>
                                            
                                            @if(@$order->pickup_city)
                                            <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->pickup_city }}
                                                </span>
                                            </p>
                                            @else
                                            <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->getpickupCityDetails->name }}
                                                </span>
                                            </p>
                                            @endif

                                            <p><span class="titel-span">@lang('admin_lang.street')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->pickup_street }}
                                                </span>
                                            </p>
                                            <p><span class="titel-span">@lang('admin_lang.block')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->pickup_block }}
                                                </span>
                                            </p>
                                            <p><span class="titel-span">@lang('admin_lang.building')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->pickup_building }}
                                                </span>
                                            </p>
                                            @if(@$order->shipping_country != 134)
                                            <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->pickup_zip }}
                                                </span>
                                            </p>
                                            @endif
                                            <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                                    {{ @$order->pickup_address }}
                                                </span>
                                            </p>
                                        </div> --}}
                                    @endif
                                    <div class="col-md-6">
                                        
                                    </div>
                                </div>
                                @endif
                                @if(@$order->order_type == 'I')
                                <div class="row fld">
                                    @if(@$order->user_id != 0)
                                    
                                    {{-- shipping address --}}
                                    <div class="col-md-6">
                                        
                                    </div>
                                    @endif
                                </div>
                                @endif
                                {{-- driver details for external order --}}
                                @if(@$order->order_type == 'E')

                                <!-- <div class="row fld">
                                    {{-- driver details --}}
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.driver_details')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->driverDetails->fname." ".@$order->orderMasterExtDetails->driverDetails->lname }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->driverDetails->email }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->driverDetails->phone }}
                                            </span>
                                        </p>
                                        <p>
                                            <span class="titel-span">@lang('admin_lang.pro_pic')</span> 
                                            <span class="deta-span">
                                                <strong>:</strong>
                                                @if(@$order->orderMasterExtDetails->driverDetails->image)
                                                @php
                                                $image_path = 'storage/app/public/driver/profile_pics/'.@$order->orderMasterExtDetails->driverDetails->image; 
                                                @endphp
                                                @if(file_exists(@$image_path))
                                        <div class="profile" style="display: block;">
                                        <img id="driverProfilePicture" src="{{ URL::to('storage/app/public/driver/profile_pics/'.@$order->orderMasterExtDetails->driverDetails->image) }}" alt="" style="width: 100px;height: 100px;">
                                        </div>
                                        @endif
                                        @endif    
                                        </span>
                                        </p>
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.street')</span> <span class="deta-span"><strong>:</strong>
                                            </span>
                                        </p>
                                        --}}
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.block')</span> <span class="deta-span"><strong>:</strong>
                                            </span>
                                        </p>
                                        --}}
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.building')</span> <span class="deta-span"><strong>:</strong>
                                            </span>
                                        </p>
                                        --}}
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            </span>
                                        </p>
                                        --}}
                                        {{-- 
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            </span>
                                        </p>
                                        --}}
                                    </div>
                                </div> -->

                                @endif
                                {{-- end  --}}
                                <hr>
                            </div>
                            
                            <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.order_history') :</h4>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered first">
                                    <thead>
                                        @if(@$order->order_type == 'I')
                                        <tr>
                                            <th>@lang('admin_lang.product_image')</th>
                                            <th>@lang('admin_lang.product')</th>
                                            <th>@lang('admin_lang.attribute')</th>
                                            {{-- <th>@lang('admin_lang.seller')</th> --}}
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight') (Gms)</th>
                                            <th>@lang('admin_lang.sub_total') ({{getCurrency()}})</th>
                                            <th>@lang('admin_lang.total_1') ({{getCurrency()}})</th>
                                            <th>@lang('admin_lang.payment_1')</th>
                                            {{-- <th>@lang('admin_lang.drivers')</th> --}}
                                            {{-- <th>@lang('admin_lang.driver_notes')</th> --}}
                                            {{-- <th>@lang('admin_lang.review_point_rate')</th> --}}
                                            {{-- <th>@lang('admin_lang.review_ratings')</th> --}}
                                            {{-- <th>@lang('admin_lang.review_comments')</th> --}}
                                            {{-- <th>@lang('admin_lang.status_1')</th> --}}
                                            {{-- <th>@lang('admin_lang.action_1')</th>  --}}
                                            {{-- <th>@lang('admin_lang.change_status_admin')</th>  --}}
                                        </tr>
                                        @else
                                        <tr>
                                            {{-- <th>@lang('admin_lang.seller')</th> --}}
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight')</th>
                                            <th>@lang('admin_lang.shipping_cst')</th>
                                            <th>@lang('admin_lang.sub_total')</th>
                                            <th>@lang('admin_lang.total_1')</th>
                                            <th>@lang('admin_lang.payment_1')</th>
                                            
                                            {{-- <th>@lang('admin_lang.drivers')</th> --}}
                                            <th>@lang('admin_lang.driver_notes')</th>
                                            {{-- <th>@lang('admin_lang.status_1')</th> --}}
                                            {{-- <th>@lang('admin_lang.action_1')</th>  --}}
                                            {{-- <th>@lang('admin_lang.change_status_admin')</th>  --}}
                                        </tr>
                                        @endif
                                    </thead>
                                    <tbody>
                                        @if(@$order->orderMasterDetails)
                                        @foreach(@$order->orderMasterDetails as $key=>$details)
                                        @if(@$order->order_type == 'I')
                                        <tr>
                                            <td>
                                                @if(@$details->productDetails->defaultImage->image)
                                                @php
                                                $image_path = 'storage/app/public/products/'.@$details->productDetails->defaultImage->image; 
                                                @endphp
                                                @if(file_exists(@$image_path))
                                                <div class="profile" style="display: block;">
                                                    <img id="profilePicture" src="{{ URL::to('storage/app/public/products/'.@$details->productDetails->defaultImage->image) }}" alt="" style="width: 100px;height: 100px;">
                                                </div>
                                                @endif
                                                @endif
                                            </td>
                                            <td>
                                                {{ $details->productDetails->productByLanguage->title }}
                                            </td>
                                            <td>
                                                @php
                                                $attr = json_decode($details->variants,true);
                                                $attributes ="";
                                                $separator =" ";
                                                @endphp
                                                @if(@$attr)
                                                @foreach(@$attr as $value)
                                                @foreach(@$value as $key=>$v)
                                                @if($key == getLanguage()->id)
                                                @php
                                                $variant = $v['variant'];
                                                $variant_value = $v['variant_value'];
                                                @endphp
                                                @php
                                                $attributes = $attributes.$separator.$variant." :".$variant_value;
                                                @endphp
                                                @endif
                                                @endforeach
                                                @php
                                                $separator =" , ";
                                                @endphp
                                                @endforeach
                                                @endif
                                                {{ @$attributes }}
                                            </td>
                                            {{-- <td>
                                                {{ $details->sellerDetails->fname." ".$details->sellerDetails->lname }}
                                            </td> --}}
                                            <td>
                                                {{ @$details->quantity  }}
                                            </td>
                                            <td>{{ @$details->weight  }}</td>

                                            <td>
                                                @if(@$details->discounted_price != 0.000)
                                                {{ @$details->discounted_price }}
                                                @else
                                                {{ @$details->original_price }}
                                                @endif
                                            </td>
                                            <td>
                                                {{ @$details->total }}
                                            </td>
                                            <td>
                                                @if(@$order->payment_method == 'C')
                                                @lang('admin_lang.cod')
                                                @else
                                                @lang('admin_lang.online')
                                                @endif
                                            </td>
                                            {{-- <td>
                                                {{ @$details->driverDetails->fname }} {{ @$details->driverDetails->lname }}
                                            </td> --}}
                                           {{-- <td>{!!strip_tags(@$details->driver_notes)!!}</td>
                                           <td>
                                                @if(@$details->rate != 0)
                                                    {{ @$details->rate }}
                                                @endif
                                            </td> --}}
                                            {{-- <td>
                                                @if(@$details->rate != 0)
                                                    @if(@$details->rate == 5 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    
                                                    @elseif(@$details->rate == 4 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    @elseif(@$details->rate == 3 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    @elseif(@$details->rate == 2 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    @elseif(@$details->rate == 1 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    @endif
                                                @endif
                                            </td> --}}
                                            {{-- <td>
                                                @if(@$details->rate != 0)
                                                {{ @$details->comment }}
                                                @endif
                                            </td>  --}}
                                            {{-- <td>
                                                {{ @$details->status }} 
                                                @if(@$details->status == 'I')
                                                Incomplete
                                                @elseif(@$details->status == 'N')
                                                New
                                                @elseif(@$details->status == 'OA')
                                                @lang('admin_lang.order_accepted')
                                                @elseif(@$details->status == 'DA')
                                                @lang('admin_lang.driver_assigned')
                                                @elseif(@$details->status == 'RP')
                                                @lang('admin_lang.ready_for_pickup')
                                                @elseif(@$details->status == 'OP')
                                                Picked up
                                                @elseif(@$details->status == 'OD')
                                                @lang('admin_lang.delivered_1')
                                                @elseif(@$details->status == 'OC')
                                                @lang('admin_lang.Cancelled_1')
                                                @endif
                                            </td> --}}
                                           
                                            
                                        </tr>
                                        @else
                                        <tr>
                                            {{-- <td>
                                                {{ @$details->sellerDetails->fname }} {{ @$details->sellerDetails->lname }}
                                            </td> --}}
                                            <td>
                                                {{ @$order->orderMasterDetails->count() }}
                                            </td>
                                            
                                            <td>
                                                {{ @$order->product_total_weight }}
                                            </td>
                                            <td>
                                                {{ @$order->shipping_price }}
                                            </td>
                                            <td>
                                                {{ @$order->subtotal }}
                                            </td>
                                            <td>
                                                {{ @$order->order_total }}
                                            </td>
                                            <td>
                                                @if(@$order->payment_method == 'C')
                                                @lang('admin_lang.cod')
                                                @else
                                                @lang('admin_lang.online')
                                                @endif
                                            </td>
                                            {{-- <td>
                                                {{ @$details->driverDetails->fname }} {{ @$details->driverDetails->lname }}
                                            </td> --}}
                                            <td>{!!strip_tags(@$details->driver_notes)!!}</td>
                                            {{-- <td>
                                                 {{ @$details->status }}
                                                @if(@$details->status == 'I')
                                                Incomplete
                                                @elseif(@$details->status == 'N')
                                                New
                                                @elseif(@$details->status == 'OA')
                                                @lang('admin_lang.order_accepted')
                                                @elseif(@$details->status == 'DA')
                                                @lang('admin_lang.driver_assigned')
                                                @elseif(@$details->status == 'RP')
                                                @lang('admin_lang.ready_for_pickup')
                                                @elseif(@$details->status == 'OP')
                                                Picked up
                                                @elseif(@$details->status == 'OD')
                                                @lang('admin_lang.delivered_1')
                                                @elseif(@$details->status == 'OC')
                                                @lang('admin_lang.Cancelled_1')
                                                @endif
                                            </td> --}}
                                            
                                            
                                        </tr>
                                        @endif
                                        @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        @if(@$order->order_type == 'I')
                                        <tr>
                                            <th>@lang('admin_lang.product_image')</th>
                                            <th>@lang('admin_lang.product')</th>
                                            <th>@lang('admin_lang.attribute')</th>
                                            {{-- <th>@lang('admin_lang.seller')</th> --}}
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight') (Gms)</th>
                                            <th>@lang('admin_lang.sub_total') ({{getCurrency()}})</th>
                                            <th>@lang('admin_lang.total_1') ({{getCurrency()}})</th>
                                            <th>@lang('admin_lang.payment_1')</th>
                                            {{-- <th>@lang('admin_lang.drivers')</th> --}}
                                            {{-- <th>@lang('admin_lang.driver_notes')</th>
                                            <th>@lang('admin_lang.review_point_rate')</th> --}}
                                            {{-- <th>@lang('admin_lang.review_ratings')</th> --}}
                                             {{-- <th>@lang('admin_lang.review_comments')</th> --}}
                                            {{-- <th>@lang('admin_lang.status_1')</th> --}}
                                             {{-- <th>@lang('admin_lang.action_1')</th>
                                             <th>@lang('admin_lang.change_status_admin')</th> --}}
                                        </tr>
                                        @else
                                        <tr>
                                            {{-- <th>@lang('admin_lang.seller')</th> --}}
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight')</th>
                                            <th>@lang('admin_lang.shipping_cst')</th>
                                            <th>@lang('admin_lang.sub_total')</th>
                                            <th>@lang('admin_lang.total_1')</th>
                                            <th>@lang('admin_lang.payment_1')</th>
                                            {{-- <th>@lang('admin_lang.driver')</th> --}}
                                            <th>Driver Notes</th>
                                            {{-- <th>Status</th> --}}
                                            {{-- <th>@lang('admin_lang.change_status_action')</th> --}}
                                        </tr>
                                        @endif
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end basic table  -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        @include('admin.includes.footer')
        <!-- ============================================================== -->
        <!-- end footer -->
        <!-- ============================================================== -->
    </div>
</div>
<div class="modal fade" id="assign_driver_popup_show" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.assign_driver')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="driver_assign_form" method="post" action="{{ route('admin.driver.assign') }}">
                        @csrf
                        <div class="form-row">
                            <input id="order_id" name="order_id" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="order_type_driver" name="order_type_driver_driver" type="hidden" class="form-control" placeholder="" readonly>
                            <div class="form-group col-md-12">
                                <label>
                                @lang('admin_lang.assign_driver')
                                </label>
                                <select id="assign" name="assign" class="form-control">
                                    <option value="">@lang('admin_lang.select_driver')</option>
                                    @foreach(@$drivers as $driver)
                                    <option value="{{ @$driver->id }}">{{ @$driver->fname }} {{ @$driver->lname }}</option>
                                    @endforeach
                                </select>
                                <span class="error_assign_driver" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12 assign_type_div">
                                <label for="specific" class="col-form-label">Assign type</label>    
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;Assign entire order to this driver
                                <input type="radio" name="assign_type" class="type" value="EO">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;Assign All products of this Seller to this driver 
                                <input type="radio" name="assign_type" class="type" value="SS">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;None
                                <input type="radio" name="assign_type" class="type" value="N">
                                <span class="checkmark"></span>
                                </label>
                                {{-- Seller specific --}}
                                {{-- <label for="ord_sp">Order specific</label>
                                <input type="radio" id="ord_sp" name="order_specific" value="Order Specific" class="form-control" />Order specific
                                <label for="seller_sp">Seller specific</label>
                                <input type="radio" id="seller_sp" name="order_specific" value="Order Specific" class="form-control" />Seller specific --}}
                                {{-- <span class="error_assign_driver" style="color: red;"></span> --}}
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="assignDriver" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="reassign_driver_popup_show" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.assign_driver')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="driver_reassign_form" method="post" action="{{ route('admin.driver.reassign') }}">
                        @csrf
                        <div class="form-row">
                            <input id="order_id_reassign" name="order_id_reassign" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="order_type_driver_reassign" name="order_type_driver_reassign" type="hidden" class="form-control" placeholder="" readonly>
                            <div class="form-group col-md-12">
                                <label>
                                @lang('admin_lang.assign_driver')
                                </label>
                                <select id="reassign" name="reassign" class="form-control">
                                    <option value="">@lang('admin_lang.select_driver')</option>
                                    @foreach(@$drivers as $driver)
                                    <option value="{{ @$driver->id }}">{{ @$driver->fname }} {{ @$driver->lname }}</option>
                                    @endforeach
                                </select>
                                <span class="error_reassign_driver" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12 reassign_type_div">
                                <label for="specific" class="col-form-label">Assign type</label>    
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;Assign entire order to this driver
                                <input type="radio" name="assign_type" class="type" value="EO">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;Assign All products of this Seller to this driver 
                                <input type="radio" name="assign_type" class="type" value="SS">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;None
                                <input type="radio" name="assign_type" class="type" value="N">
                                <span class="checkmark"></span>
                                </label>
                                {{-- Seller specific --}}
                                {{-- <label for="ord_sp">Order specific</label>
                                <input type="radio" id="ord_sp" name="order_specific" value="Order Specific" class="form-control" />Order specific
                                <label for="seller_sp">Seller specific</label>
                                <input type="radio" id="seller_sp" name="order_specific" value="Order Specific" class="form-control" />Seller specific --}}
                                {{-- <span class="error_assign_driver" style="color: red;"></span> --}}
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="reassignDriver" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalCenter">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.notes')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="approveReasonForm" method="post" action="">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <!-- <label>Notes</label> -->
                                <textarea class="form-control h-80px" rows="2" id="comment"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" class="btn btn-primary popbtntp" id="approve_product">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalCenter2">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.notes')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="rejectReasonForm" method="post" action="">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <!-- <label>Notes</label> -->
                                <textarea class="form-control h-80px" rows="2" id="comment"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end main wrapper -->
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif

<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/slimscroll/jquery.slimscroll.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/multi-select/js/jquery.multi-select.js') }}"></script> 
<script src="{{ asset('public/admin/assets/libs/js/main-js.js') }}"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        window.print();
    });
</script>
@endsection