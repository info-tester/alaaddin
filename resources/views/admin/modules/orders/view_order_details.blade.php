@extends('admin.layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.v_order_dtls')
@endsection
@section('links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    /* The container */
    
</style>
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== --> 
            <!-- pageheader --> 
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.view_order_details')</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('admin.list.order') }}" class="breadcrumb-link">@lang('admin_lang.manage_orders')</a></li>
                                    <li class="breadcrumb-item active"><a href="#" class="breadcrumb-link">View order details</a></li>
                                    <!-- <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.edit_view_order_dtls')</li> -->
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="mail_success" style="display: none;">
                <div class="alert alert-success vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
                </div>
            </div>
            <div class="mail_error" style="display: none;">
                <div class="alert alert-danger vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>@lang('admin_lang.error')!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
                </div>
            </div>
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success') }}</strong> {{ session('success') }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error') }}</strong> 
            </div>
            @endif
            
             
            <!-- ============================================================== --> 
            <!-- end pageheader --> 
            <!-- ============================================================== -->
            <div class="row printDiv">
                <!-- ============================================================== -->
                <!-- basic table  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">@lang('admin_lang.view_order_details') - {{ @$order->order_no }}
                            <a class="adbtn btn btn-primary" href="{{ route('admin.list.order') }}"><i class="fas fa-less-than"></i> @lang('admin_lang.back')</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           
                        </h5>
                        <div class="card-body">
                            <div class="manager-dtls tsk-div">
                                <div class="row fld">
                                    {{-- Order details --}}
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.order_details')</h4>

                                        <p><span class="titel-span">@lang('admin_lang.order_number')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->order_no }}
                                            </span> 
                                        </p>
                                        
                                        <p><span class="titel-span">@lang('admin_lang.no_of_item')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterDetails->count() }}
                                            </span>
                                        </p>
                                        
                                        <p><span class="titel-span">@lang('admin_lang.order_date')</span> <span class="deta-span"><strong>:</strong>{{ @$order->created_at }}</span></p>

                                        
                                        <p><span class="titel-span">Product Price({{ getCurrency() }})</span> <span class="deta-span"><strong>:</strong>{{ number_format(@$order->total_product_price, 2) }} </span></p>
                                        <p><span class="titel-span">Seller Commission({{ getCurrency() }})</span> <span class="deta-span"><strong>:</strong>{{ number_format(@$order->total_seller_commission,2) }} </span></p>
                                        

                                        <p><span class="titel-span"> Insurance Price({{ getCurrency() }})</span> <span class="deta-span"><strong>:</strong>{{ number_format(@$order->insurance_price,2) }} </span></p>
                                        <p><span class="titel-span">Loading Unloading Price({{ getCurrency() }})</span> <span class="deta-span"><strong>:</strong> {{ number_format(@$order->loading_price,2) }} </span></p>
                                        
                                        <p><span class="titel-span">Admin Commission({{ getCurrency() }})</span> <span class="deta-span"><strong>:</strong>{{ number_format(@$order->admin_commission,2) }} </span></p>
                                        <p><span class="titel-span">Payable Amount({{ getCurrency() }})</span> <span class="deta-span"><strong>:</strong>{{ number_format(@$order->payable_amount,2) }} </span></p>
                                        <p><span class="titel-span">Total Weight</span> <span class="deta-span"><strong>:</strong>{{ @$order->product_total_weight }} Ton </span></p>
                                        @if(@$order->status == 'OD' || @$order->status == 'CM')
                                        <p><span class="titel-span">Delivery Date</span> <span class="deta-span"><strong>:</strong>{{ @$order->delivery_date }} </span></p>
                                        {{-- <p><span class="titel-span">Delivery Time</span> <span class="deta-span"><strong>:</strong>{{ @$order->delivery_time }} </span></p> --}}
                                        @endif
                                        
                                        

                                        <p><span class="titel-span">@lang('admin_lang.payment_method')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->payment_method == 'C')
                                                @lang('admin_lang.cod')
                                            @elseif(@$order->payment_method == 'O')
                                                @lang('admin_lang.online')
                                            @else
                                                {{ 'N.A.' }}
                                            @endif
                                            </span>
                                        </p>
                                        
                                        <p><span class="titel-span">Overall Order Status</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->status == 'I')
                                            Incomplete
                                            @elseif(@$order->status == 'N')
                                            New
                                            @elseif(@$order->status == 'INP')
                                            In Progress
                                            @elseif(@$order->status == 'CM')
                                            Completed
                                            @elseif(@$order->status == 'OA')
                                            @lang('admin_lang.order_accepted')
                                            @elseif(@$order->status == 'DA')
                                            @lang('admin_lang.driver_assigned')
                                            @elseif(@$order->status == 'RP')
                                            @lang('admin_lang.ready_for_pickup')
                                            @elseif(@$order->status == 'OP')
                                            Picked up
                                            @elseif(@$order->status == 'OD')
                                            @lang('admin_lang.delivered_1')
                                            @elseif(@$order->status == 'PP')
                                            @lang('admin_lang.partial_pickup')
                                            @elseif(@$order->status == 'PC')
                                            @lang('admin_lang.partial_pickup_completed')
                                            @elseif(@$order->status == 'OC')
                                            @lang('admin_lang.Cancelled_1')
                                            @elseif(@$order->status == 'F')
                                            Payment Failed
                                            @endif
                                            </span>
                                        </p>
                                        
                                        @if(@$order->paymentData->txn_id)
                                        <p><span class="titel-span">Payment gateway txn ID: </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->paymentData->txn_id }}    
                                            </span>
                                        </p>
                                        <p><span class="titel-span">RazorPayId: </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->paymentData->payment_initated_id }}    
                                            </span>
                                        </p>
                                        @endif
                                    </div>
                                    
                                    @if(@$order->order_type == 'I')
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.customer_details')</h4>
                                        
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            
                                            
                                            {{ @$order->fname }} {{ @$order->lname }}
                                            
                                            </span>
                                        </p>
                                        
                                        
                                        
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            
                                            {{ @$order->phone }}
                                            
                                            </span>
                                        </p>
                                        
                                    </div>
                                    @endif
                                    
                                </div>
                                
                                @if(@$order->order_type == 'I')
                                <div class="row fld">
                                    @if(@$order->user_id != 0)
                                    {{-- billing address --}}
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.billing_address')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_fname }} {{ @$order->billing_lname }}
                                            </span>
                                        </p>
                                        
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_phone }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.country')</span>
                                            <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getBillingCountry->name }}
                                            </span>
                                        </p>
                                        @if(@$order->billing_state)
                                        <p><span class="titel-span">@lang('admin_lang.state')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_state_details->name }}   
                                            </span>
                                        </p>
                                        @endif
                                        @if(@$order->billing_city)
                                        <p><span class="titel-span">@lang('admin_lang.city')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_city_details->name }}   
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_postal_code }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_more_address}}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">Nearest Landmark</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_street }} 
                                            </span>
                                        </p>
                                    </div>
                                    {{-- shipping address --}}
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.shipping_address')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_fname }} {{ @$order->shipping_lname }}
                                            </span>
                                        </p>
                                        
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_phone }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.country')</span>
                                            <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCountry->name }}
                                            </span>
                                        </p>
                                        @if(@$order->shipping_city)
                                        <p><span class="titel-span">@lang('admin_lang.city')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_city_details->name }}   
                                            </span>
                                        </p>
                                        @endif
                                        @if(@$order->shipping_state)
                                        <p><span class="titel-span">@lang('admin_lang.state')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_state_details->name }}    
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_postal_code }}    
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_more_address }} 
                                            </span>
                                        </p>
                                        <p><span class="titel-span">Nearest Landmark</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_street }} 
                                            </span>
                                        </p>
                                    </div>
                                    @endif
                                </div>
                                @endif
                            
                            </div>
                            
                            <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.order_history')</h4>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered first">
                                    <thead>
                                        
                                        <tr>
                                            <th>Product Image</th>
                                            <th>Product</th>
                                            <th>Seller</th>
                                            <th>Quantity</th>
                                            <th>Original Unit Price ({{getCurrency()}})</th>
                                            <th>Discounted Unit Price ({{getCurrency()}})</th>
                                            <th>Original Subtotal ({{getCurrency()}})</th>
                                            <th>Total Discount ({{getCurrency()}})</th>
                                            <th>Total({{getCurrency()}})</th>
                                            <th>Review Rate</th>
                                            <th>Ratings</th>
                                            <th>Review Comment</th>
                                            <th>Expected Delivery Date</th>
                                            <th>Delivery Date</th>
                                            <th>Seller Commission({{getCurrency()}})</th>
                                            <th>Loading Price({{getCurrency()}})</th>
                                            <th>Insurance Price({{getCurrency()}})</th>
                                            <th>Admin Commission({{getCurrency()}})</th>
                                            <th>Payable Amount({{getCurrency()}})</th>
                                            <th>Cancelled On</th>
                                            <th>Cancelled Note</th>
                                            <th>Refund Id</th>
                                            <th>Vehicle Number</th>
                                            <th>Driver Name & Mobile</th>
                                            <th>Shipping Date</th>
                                            <th>Received Date</th>
                                            <th>Comments</th>
                                            <th>Evidence File</th>
                                            <th>Status</th>
                                            
                                            
                                        </tr>
                                        
                                    </thead>
                                    <tbody>
                                        @if(@$order->orderMasterDetails)
                                        @foreach(@$order->orderMasterDetails as $key=>$details)
                                        @if(@$order->order_type == 'I')
                                        <tr>
                                            <td>
                                                @if(@$details->productDetails->defaultImage->image)
                                                    @php
                                                    $image_path = 'storage/app/public/products/80/'.@$details->productDetails->defaultImage->image; 
                                                    @endphp
                                                    @if(file_exists(@$image_path))
                                                    <div class="profile">
                                                        <img id="profilePicture" src="{{ URL::to('storage/app/public/products/80/'.@$details->productDetails->defaultImage->image) }}" alt="">
                                                    </div>
                                                    @endif
                                                @else
                                                    <div class="profile">
                                                        <img id="profilePicture" src="{{ getDefaultImageUrl() }}" alt="">
                                                    </div>
                                                @endif
                                            </td>
                                            <td>
                                                {{ @$details->productDetails->productByLanguage->title }}({{ @$details->productDetails->product_code }})
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.view.merchant.profile',@$details->sellerDetails->id) }}"> {{ @$details->sellerDetails->fname." ".@$details->sellerDetails->lname }}</a>
                                            </td>
                                            <td>
                                                {{ @$details->quantity  }}  {{ @$details->getOrderDetailsUnitMaster->unit_name }}
                                            </td>
                                            

                                            <td>
                                                {{ @$details->original_price }} /{{ @$details->getOrderDetailsUnitMaster->unit_name }}
                                            </td>
                                            <td>
                                                @if(@$details->discounted_price > 0)
                                                {{ @$details->discounted_price }} /{{ @$details->getOrderDetailsUnitMaster->unit_name }}
                                                @else
                                                {{ @$details->original_price }} /{{ @$details->getOrderDetailsUnitMaster->unit_name }}
                                                @endif
                                            </td>
                                            <td>
                                                {{ @$details->sub_total }} 
                                            </td>
                                            <td>
                                                {{ @$details->sub_total - @$details->total }} 
                                            </td>
                                            <td>
                                                {{ @$details->total }} 
                                            </td>
                                            
                                            <td>
                                                @if(@$details->rate != 0)
                                                    {{ @$details->rate }}
                                                @endif
                                            </td>
                                            <td>
                                                @if(@$details->rate != 0)
                                                    @if(@$details->rate == 5 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    
                                                    @elseif(@$details->rate == 4 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    @elseif(@$details->rate == 3 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    @elseif(@$details->rate == 2 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    @elseif(@$details->rate == 1 )
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                                    @endif
                                                @endif
                                            </td>
                                            
                                            <td>
                                                @if(@$details->rate != 0)
                                                {{ @$details->comment }}
                                                @endif
                                            </td>
                                            <td>
                                                
                                                {{ @$details->expected_delivery_date }}
                                                
                                            </td>
                                            <td>
                                                {{ @$details->delivery_date }}
                                            </td>
                                            <td>
                                                {{ @$details->seller_commission }}
                                            </td>
                                            <td>
                                                {{ @$details->loading_price }}
                                            </td>
                                            <td>
                                                {{ @$details->insurance_price }}
                                            </td>
                                            <td>
                                                {{ @$details->admin_commission }}
                                            </td>
                                            <td>
                                                {{ @$details->payable_amount }}
                                            </td>
                                            <td>
                                                {{ @$details->cancelled_on }}
                                            </td>
                                            <td>
                                                {{ @$details->cancel_note }}
                                            </td>
                                            <td>
                                                {{ @$details->refund_id }}
                                            </td>
                                            <td>{{ @$details->showOrderVehicleInformation->vehicle_no }}</td>
                                            <td>@if(@$details->showOrderVehicleInformation->driver_name){{ @$details->showOrderVehicleInformation->driver_name." (".@$details->showOrderVehicleInformation->driver_mobile_number.")" }} @endif</td>
                                            <td>{{ @$details->showOrderVehicleInformation->shipping_date }}</td>
                                            <td>{{ @$details->showOrderVehicleInformation->received_date }}</td>
                                            <td>{{ @$details->showOrderVehicleInformation->description }}</td>
                                            <td>
                                                @if(@$details->showOrderVehicleInformation->evidence_file)
                                                <div class="profile">
                                                <img src="{{ URL::to('storage/app/public/evidence_file/'.@$details->showOrderVehicleInformation->evidence_file) }}" alt="">
                                                </div>  
                                                @endif
                                                
                                            </td>
                                            
                                            <td>
                                                @if(@$details->status == 'OC')
                                                Cancelled
                                                @endif
                                                @if(@$details->status == 'OA')
                                                Order Accepted
                                                @endif
                                                @if(@$details->status == 'OP')
                                                Order Pickedup
                                                @endif
                                                @if(@$details->status == 'RP')
                                                Ready for picked up
                                                @endif
                                                @if(@$details->status == 'OD')
                                                Order delivered
                                                @endif
                                                @if(@$details->status == 'N')
                                                New
                                                @endif
                                                @if(@$details->status == 'I')
                                                Incomplete
                                                @endif
                                            </td>
                                            
                                            
                                        </tr>
                                        @endif
                                        @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                       <tr>
                                            <th>Product Image</th>
                                            <th>Product</th>
                                            <th>Seller</th>
                                            <th>Quantity</th>
                                            <th>Original Unit Price ({{getCurrency()}})</th>
                                            <th>Discounted Unit Price ({{getCurrency()}})</th>
                                            <th>Original Subtotal ({{getCurrency()}})</th>
                                            <th>Total Discount ({{getCurrency()}})</th>
                                            <th>Total({{getCurrency()}})</th>
                                            <th>Review Rate</th>
                                            <th>Ratings</th>
                                            <th>Review Comment</th>
                                            <th>Expected Delivery Date</th>
                                            <th>Delivery Date</th>
                                            <th>Seller Commission({{getCurrency()}})</th>
                                            <th>Loading Price({{getCurrency()}})</th>
                                            <th>Insurance Price({{getCurrency()}})</th>
                                            <th>Admin Commission({{getCurrency()}})</th>
                                            <th>Payable Amount({{getCurrency()}})</th>
                                            <th>Cancelled On</th>
                                            <th>Cancelled Note</th>
                                            <th>Refund Id</th>
                                            <th>Vehicle Number</th>
                                            <th>Driver Name & Mobile</th>
                                            <th>Shipping Date</th>
                                            <th>Received Date</th>
                                            <th>Comments</th>
                                            <th>Evidence File</th>
                                            <th>Status</th>
                                        </tr>
                                        
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end basic table  -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        @include('admin.includes.footer')
        <!-- ============================================================== -->
        <!-- end footer -->
        <!-- ============================================================== -->
    </div>
</div>

<div class="modal fade" id="reassign_driver_popup_show" data-id="" data-emailid="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.assign_driver')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="driver_reassign_form" method="post" action="{{ route('admin.driver.reassign') }}">
                        @csrf
                        <div class="form-row">
                            <input id="order_id_reassign" name="order_id_reassign" type="hidden" class="form-control" placeholder="" readonly>
                            <input id="order_type_driver_reassign" name="order_type_driver_reassign" type="hidden" class="form-control" placeholder="" readonly>
                            <div class="form-group col-md-12">
                                <label>
                                @lang('admin_lang.assign_driver')
                                </label>
                                <select id="reassign" name="reassign" class="form-control">
                                    <option value="">@lang('admin_lang.select_driver')</option>
                                    <option value="N">No Driver</option>
                                    @foreach(@$drivers as $driver)
                                    <option value="{{ @$driver->id }}">{{ @$driver->fname }} {{ @$driver->lname }}</option>
                                    @endforeach
                                </select>
                                <span class="error_reassign_driver" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12 reassign_type_div">
                                <label for="specific" class="col-form-label">Assign type</label>    
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Assign entire order to this driver
                                <input type="radio" name="assign_type" class="type" value="EO">
                                <span class="checkmark"></span>
                                </label>
                                <label class="container specific">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Assign All products of this Seller to this driver 
                                <input type="radio" name="assign_type" class="type" value="SS" checked="">
                                <span class="checkmark"></span>
                                </label>
                                {{-- <label class="container specific">
                                &nbsp;&nbsp;&nbsp;None
                                <input type="radio" name="assign_type" class="type" value="N">
                                <span class="checkmark"></span>
                                </label> --}}
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="reassignDriver" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalCenter">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.notes')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="approveReasonForm" method="post" action="">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <!-- <label>Notes</label> -->
                                <textarea class="form-control h-80px" rows="2" id="comment"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" class="btn btn-primary popbtntp" id="approve_product">@lang('admin_lang.sve')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalCenter2">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('admin_lang.notes')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="rejectReasonForm" method="post" action="">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <!-- <label>Notes</label> -->
                                <textarea class="form-control h-80px" rows="2" id="comment"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" class="btn btn-primary popbtntp">@lang('admin_lang.sve')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end main wrapper -->
@endsection
@section('scripts')

@include('admin.includes.scripts')


<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script> 
<!-- <script src="{{ asset('public/admin/assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>  -->
<script src="{{ asset('public/admin/assets/vendor/slimscroll/jquery.slimscroll.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/multi-select/js/jquery.multi-select.js') }}"></script> 
<script src="{{ asset('public/admin/assets/libs/js/main-js.js') }}"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".change_status_form").validate();
        $(document).on("click","#assignDriver",function(e){
            var assign = $.trim($("#assign").val());
            if(assign == ""){
                $(".error_assign_driver").text('@lang('validation.required')');
            }else{
                $(".error_assign_driver").text("");
            }
            if(assign != ""){
                $("#driver_assign_form").submit();
            }
        });
        
        $(document).on("click",".assign_driver_popup",function(e){
            var id= $.trim($(e.currentTarget).attr("data-id"));
            var otype= $.trim($(e.currentTarget).attr("data-otype"));
            $('#assign_driver').attr("data-id",id);
            $('#assign_driver').attr("data-otype",id);/*
            // new
            $('#reassign_driver_popup_show').modal('hide');
            $('#assign_driver_popup_show').modal('show');*/
            $("#order_id").val(id);
            $("#order_type_driver").val(otype);
            if(otype == 'E'){
                $(".assign_type_div").hide();
            }else{
                $(".assign_type_div").show();
            }
        });

        $(document).on("click","#reassignDriver",function(e){
            var assign = $.trim($("#reassign").val());
            if(assign == ""){
                $(".error_reassign_driver").text('@lang('validation.required')');
            }else{
                $(".error_reassign_driver").text("");
            }
            if(assign != ""){
                $("#driver_reassign_form").submit();
            }
        });
        
        $(document).on("click",".reassign_driver_popup",function(e){
            var id= $.trim($(e.currentTarget).attr("data-id"));
            var otype= $.trim($(e.currentTarget).attr("data-otype"));
            $("#order_id_reassign").val(id);
            $("#order_type_driver_reassign").val(otype);
            $('#reassign_driver').attr("data-id",id);
            $('#reassign_driver').attr("data-otype",otype);
            $('#assign_driver_popup_show').modal('hide');
            $('#reassign_driver_popup_show').modal('show');
        
            $("#order_id_reassign").val(id);
            $("#order_type_driver_reassign").val(otype);
            if(otype == 'E'){
                $(".reassign_type_div").hide();
            }else{
                $(".reassign_type_div").show();
            }
        });
        
        $("#profile_pic").change(function() {
            var filename = $.trim($("#profile_pic").val()),
            filename_arr = filename.split("."),
            ext = filename_arr[1];
            ext = ext.toLowerCase();
            if( ext == "jpg" || ext == "jpeg" || ext == "png"){
                $(".errorpic").text("");
                readURL(this);
            }else{
                $(".errorpic").text('@lang('validation.image_extension_type')');
                $('#profilePicture').attr('src', "");
            }
        });

        $(document).on("change",".any_status_change",function(e){
            var id = $(this).attr("data-id"),
            status = $(this).val();

            if(id != ""){
                if(status == 'C') {
                    if(confirm('Do you really want to cancel the specific item?')) {
                        $("#change_any_status_form"+id).submit();
                    }
                } else {
                    $("#change_any_status_form"+id).submit();
                }
            }
        });

        $(document).on("click",".print_invoice",function(e){
            var divContents = $(".printDiv").innerHTML; 
            var a = window.open('', '', 'height=500, width=500'); 
            a.document.write('<html>'); 
            a.document.write('<body > <h1>Div contents are <br>'); 
            a.document.write(divContents); 
            a.document.write('</body></html>'); 
            a.document.close(); 
            a.print(); 
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#profilePicture').attr('src', e.target.result);
                    $(".profile").show();
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $('.llk').click(function function_name(argument) {
            $('#exampleModalCenter1').modal('show');
        });
    });
</script>
<!-- print page -->
<script type="text/javascript">

    function printDiv() { 
        
    } 
</script>
@endsection