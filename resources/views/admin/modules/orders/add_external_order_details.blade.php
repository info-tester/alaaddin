@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Edit External Order Details') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.add_ext_order')
@endsection
@section('links')
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/arabic/assets/libs/css/chosen.css') }}">
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
    label.error{
    border: none !important;
    background-color: #fff !important;
    color: #f00;
    }
    #cityList{
    position: absolute;
    width: 96%;
    /*height: 200px;*/
    z-index: 99;
    }
    #cityList ul{
    background: #fff;
    width: 96%;
    border: solid 1px #eee;
    padding: 0;
    max-height: 200px;
    overflow-y: scroll;
    }
    #cityList ul li{
    list-style: none;
    padding: 5px 15px;
    cursor: pointer;
    border-bottom: solid 1px #eee;
    }
    #cityList ul li:hover{
    background: #3a82c4;
    color: #fff;
    }

    #pickup_cityList{
        position: absolute;
        width: 97%;
        /*height: 200px;*/
        z-index: 99;
    }
    #pickup_cityList ul{
        background: #fff;
        width: 97%;
        border: solid 1px #eee;
        padding: 0;
        max-height: 200px;
        overflow-y: scroll;
    }
    #pickup_cityList ul li{
        list-style: none;
        padding: 5px 15px;
        cursor: pointer;
        border-bottom: solid 1px #eee;
    }
    #pickup_cityList ul li:hover{
        background: #3a82c4;
        color: #fff;
    }
    .ad1{
        float: left;
        border: 1px solid #d7d3d3;
        width: 24%;
        margin: 18px 1% 15px 16px;
        padding: 10px 15px;
    }
    .ad1 h5{
        margin: 0 0 10px 0;
        font-size: 18px;
    }
    .ad1Span{
        margin: 0 0 0 15px;
    }
    .ad1Span2{
        margin: 0 0 12px 35px;
    }
    .addNot{
        font-size: 19px;
        margin: 0 0 8px 0;
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
<div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
        <!-- ============================================================== -->
        <!-- pageheader  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">
                        @lang('admin_lang.e_commerce_dashboard_template')
                    </h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    @lang('admin_lang.add_ext_order')
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (session()->has('success'))
        <div class="alert alert-success vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
            <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
        </div>
        @elseif ((session()->has('error')))
        <div class="alert alert-danger vd_hidden" style="display: block;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
            <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
        </div>
        @endif
        <!-- ============================================================== -->
        <!-- end pageheader  -->
        <!-- ============================================================== -->
        <div class="ecommerce-widget">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">@lang('admin_lang.add_ext_order')
                            <a class="adbtn btn btn-primary" href="{{ route('admin.list.order') }}"><i class="fas fa-less-than"></i> @lang('admin_lang.back')</a>
                        </h5>
                        <div class="card-body">
                            <form id="addExternalOrderForm" method="post" enctype="multipart/form-data" action="" autocomplete="off">
                                @csrf
                                <input type="hidden" name="from_address" id="from_address" value="T">
                                <input type="hidden" name="from_address_id" id="from_address_id" value="">
                                <input type="hidden" name="from_address_country_id" id="from_address_country_id" value="">
                                <input type="hidden" name="to_address" id="to_address" value="F">
                                <input type="hidden" name="to_address_id" id="to_address_id" value="">
                                <input type="hidden" name="to_address_country_id" id="to_address_country_id" value="">
                                <div class="row">
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                        <label for="merchant_label" class="col-form-label ">@lang('admin_lang.merchant') (@lang('admin_lang.required'))</label>
                                        <select class="custom-select form-control select slt slct merchant required" id="merchant" name="merchant">
                                            <option value="">@lang('admin_lang.select_merchant')</option>
                                            @if(@$merchants)
                                            @foreach(@$merchants as $merchant)
                                            <option value="{{ @$merchant->id }}" data-countryid="{{ @$merchant->country }}" data-paymentmethod="{{ @$merchant->payment_mode }}">{{ @$merchant->fname }} {{ @$merchant->lname }} </option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <input type="hidden" id="merchant_country" name="merchant_country" value=""/>
                                        <span class="error_merchant removeText" style="color: #f85d2c;"></span>
                                    </div>
                                </div>
                                <div class="form-row" id="showPickupAddressBlock" style="display: none;">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h5 class="fultxt addNot">@lang('merchant_lang.pickup_address')</h5>
                                        <div class="row" >
                                            <label class="ad1Span">
                                                <input type="radio" name="radio1"  id="input_address1" style="display: inline-block;" > Input Address
                                            </label>
                                            <label class="ad1Span2">
                                                <input type="radio" name="radio1"  id="address_book1" style="display: inline-block;" checked> Choose from address book
                                            </label>
                                            <span class="ad1Span2">
                                                <label id="from_address_show_error" class="error" style="display:none"></label>
                                            </span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="showField1" style="display:none;">
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="pickup_country" class="col-form-label">@lang('merchant_lang.pickup_country') (@lang('admin_lang.required'))</label>
                                            <select class="custom-select form-control required" id="pickup_country" name="pickup_country">
                                                <option value="">@lang('admin_lang.select_country')</option>
                                                @if(@$countries)
                                                @foreach(@$countries as $country)
                                                <option value="{{ @$country->id }}" @if($country->id == 134) selected  @elseif($country->status == 'I') style="display:none;" @endif>{{ @$country->countryDetailsBylanguage->name }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label class="col-form-label">@lang('admin_lang.pickup_phone') (@lang('admin_lang.required'))</label>
                                            <input id="pickup_phone" name="pickup_phone" class="form-control required number" placeholder='@lang('admin_lang.pickup_phone')' type="tel"  onkeypress="return isNumber(event)"> 
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 outside_kuwait_city1" style="display: none">
                                            <label for="pickup_city" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                            <input id="pickup_city" name="pickup_city" type="text" class="form-control" placeholder='@lang('admin_lang.city')' value="" >
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 inside_kuwait_city1">
                                            <label for="pickup_kuwait_city_label" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                            
                                            <input type="text" class="form-control required custom-select" name="pickup_kuwait_city" id="pickup_kuwait_city" placeholder="@lang('admin_lang.city')" value="">
                                            <div id="pickup_cityList"></div>
                                            <span class="text-danger pickup_city_id_err"></span>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 blockDiv">
                                            <label for="" class="col-form-label block_label1">@lang('admin_lang.block_required') </label>
                                            <input type="text" id="pickup_block" name="pickup_block" type="text" class="form-control required" placeholder='@lang('admin_lang.block')' value=""/>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="" class="col-form-label">@lang('admin_lang.street') (@lang('admin_lang.required'))</label>
                                            <input id="pickup_street" name="pickup_street" type="text" class="form-control required" placeholder='@lang('admin_lang.street')' value="">
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 zipDiv1" >
                                        <label for="pickup_zip" class="col-form-label zip_label1">@lang('admin_lang.postal_code_optional')</label>
                                        <input id="pickup_zip" name="pickup_zip" type="number" class="form-control" placeholder='@lang('admin_lang.postal_code')' onkeypress="return isNumber(event)" value="">
                                        </div>
                                        
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 buidingDiv">
                                            <label for="" class="col-form-label building_label1">@lang('admin_lang.build_required')</label>
                                            <input type="text" id="pickup_building" name="pickup_building" type="text" class="form-control required" placeholder='@lang('admin_lang.building')' value=""/>
                                        </div>
                                        
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label class="col-form-label">@lang('front_static.type_url') (@lang('front_static.optional'))</label>
                                            <input id="" type="text" placeholder="@lang('front_static.type_url')" name="pickup_location" class="form-control" value="{{ @$default_address->location }}">
                                            
                                            <input type="hidden" name="lat1" id="lat1" value="">
                                            <input type="hidden" name="lng1" id="lng1" value="">
                                        </div>
    
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="" class="col-form-label">@lang('admin_lang.more_add_details_opt')</label>
                                            <input type="text" id="pickup_address" name="pickup_address" type="text" class="form-control" placeholder='@lang('admin_lang.more_address_details')' value=""/>
                                        </div>
                                    </div>
                                    <div class="row col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="showAddressBook1" >
                                        
                                    </div>
                                </div>
                                <br>
                                <div class="form-row" id="showShippingAddressBlock" style="display: none">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h5 class="fultxt addNot">@lang('admin_lang.shipping_address')</h5>
                                        <div class="row" >
                                            <label class="ad1Span">
                                                <input type="radio" name="radio2"  id="input_address2" style="display: inline-block;" checked> Input Address
                                            </label>
                                            <label class="ad1Span2">
                                                <input type="radio" name="radio2"  id="address_book2" style="display: inline-block;"> Choose from address book
                                            </label>
                                            <span class="ad1Span2">
                                                <label id="to_address_show_error" class="error" style="display:none"></label>
                                            </span>
                                        </div>
                                    </div>
                                    <div id="showField2" class="row col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" >
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="shipping_country" class="col-form-label">@lang('merchant_lang.shipping_country') (@lang('admin_lang.required'))</label>
                                            <select class="custom-select form-control required" id="country" name="country">
                                                <option value="">@lang('admin_lang.select_country')</option>
                                                @if(@$countries)
                                                @foreach(@$countries as $country)
                                                <option value="{{ @$country->id }}" @if($country->id == 134) selected  @elseif($country->status == 'I') style="display:none;" @endif>{{ @$country->countryDetailsBylanguage->name }}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 outside_kuwait_city">
                                            <label for="city" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                            <input id="city" name="city" type="text" class="form-control" placeholder='@lang('admin_lang.city')'>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 inside_kuwait_city">
                                            <label for="kuwait_city_label" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                            
                                            <input type="text" class="form-control required custom-select" name="kuwait_city" id="kuwait_city" placeholder="@lang('admin_lang.city')" value="">
                                            <div id="cityList"></div>
                                            <span class="text-danger city_id_err"></span>
                                        </div>
                                        
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 blockDiv">
                                            <label for="" class="col-form-label block_label">@lang('admin_lang.block_required') </label>
                                            <input type="text" id="block" name="block" type="text" class="form-control required" placeholder='@lang('admin_lang.block')' />
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="" class="col-form-label">@lang('admin_lang.street') (@lang('admin_lang.required'))</label>
                                            <input id="street" name="street" type="text" class="form-control required" placeholder='@lang('admin_lang.street')'>
                                        </div>
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 zipDiv" >
                                            <label for="zip" class="col-form-label zip_label">@lang('admin_lang.postal_code_optional')</label>
                                            <input id="zip" name="zip" type="number" class="form-control" placeholder='@lang('admin_lang.postal_code')' onkeypress="return isNumber(event)">
                                        </div>
                                        
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 buidingDiv buidingDiv">
                                            <label for="" class="col-form-label building_label">@lang('admin_lang.build_required')</label>
                                            <input type="text" id="building" name="building" type="text" class="form-control required" placeholder='@lang('admin_lang.building')' />
                                        </div>
                                        
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label class="col-form-label">@lang('front_static.type_url') (@lang('front_static.optional'))</label>
                                            <input id="" type="text" placeholder="@lang('front_static.type_url')" name="location" class="form-control">
                                            
                                            <input type="hidden" name="lat" id="lat2">
                                            <input type="hidden" name="lng" id="lng2">
                                        </div>

                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <label for="" class="col-form-label">@lang('admin_lang.more_add_details_opt')</label>
                                            <input type="text" id="more_address_details" name="address" type="text" class="form-control" placeholder='@lang('admin_lang.more_address_details')' />
                                        </div>
                                    </div>
                                    <div class="row col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="showAddressBook2" style="display: none;">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-row">
                                    <span class="col-md-12">
                                        <label id="address_show_error" class="error" style="display:none"></label>
                                    </span>
                                    <div class="form-group col-md-6">
                                        <label for="country" class="col-form-label">@lang('admin_lang.delivery_date_required')</label>
                                        @php
                                        $todayDate = date("Y-m-d");
                                        @endphp
                                        <input type="text" id="datepicker" name="delivery_date" size="30" class="form-control from_date datepicker" placeholder="Date" value="{{ $todayDate }}" readonly="" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="country" class="col-form-label" style="padding-left: 10px;">@lang('admin_lang.delivery_time_required')</label>
                                        <div class="day_div">
                                            <div class="tymm width-set">
                                                <input type="hidden" name="day[]" value="">
                                                <span class="file_div">
                                                    <input type="text" class="form-control timepicker from_time required_remove required" placeholder="@lang('admin_lang.delivery_time_required')" name="from_time" value="" data-value="" readonly>
                                                    <span class="error_time_from from_time_error_1 text-danger error_from_1"></span>
                                                </span>
                                                <a href="#"><img src="{{ ('public/admin/assets/images/swp.png') }}"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label class="fname_label">@lang('admin_lang.first_name')  (@lang('admin_lang.optional')) </label>
                                        <input id="fname" name="fname" class="form-control" placeholder='@lang('admin_lang.first_name')' type="text">
                                    </div>
                                    <div class="form-group  col-md-4">
                                        <label class="lname_label">@lang('admin_lang.last_name') (@lang('admin_lang.optional'))</label>
                                        <input id="lname" name="lname" class="form-control" placeholder='@lang('admin_lang.last_name')' type="text">
                                    </div>
                                    <div class="form-group  col-md-4">
                                        <label>@lang('admin_lang.email') (@lang('admin_lang.optional'))</label>
                                        <input id="email" name="email" class="form-control" placeholder='@lang('admin_lang.email')' type="text">
                                    </div>
                                    <div class="form-group  col-md-4">
                                        <label>@lang('admin_lang.phone_required')</label>
                                        <input id="phone" name="phone" class="form-control required number" placeholder='@lang('admin_lang.phone_required')' type="tel"  onkeypress="return isNumber(event)"> 
                                    </div>
                                    <div class="form-group  col-md-4">
                                        <label id="order_totallabel">@lang('admin_lang.total_cost_required')</label>
                                        <input id="order_total" name="order_total" class="form-control required number" placeholder='@lang('admin_lang.total_cost_required')' type="text">
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label class=" product_weight_label">@lang('admin_lang.product_weight') @lang('admin_lang.optional_value')</label>
                                        <input  id="product_weight"  name="product_weight" class="form-control required number" placeholder="@lang('admin_lang.product_weight')" type="text" value="">
                                        <span class="error_product_weight removeText" style="color: red;"></span>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>@lang('admin_lang.invoice_number_optional')</label>
                                        <input id="invoice" name="invoice" class="form-control" placeholder='@lang('admin_lang.invoice_no')' type="text">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>@lang('admin_lang.invoice_details_optional')</label>
                                        <input id="invoice_details" name="invoice_details" class="form-control" placeholder='@lang('admin_lang.invoice_details')' type="text">
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                        <label for="country" class="">@lang('admin_lang.payment_way_required')</label>
                                        <select class="form-control required" id="payment_method" name="payment_method">
                                            <option value="">@lang('admin_lang.select_payment_way')</option>
                                            <option value="C">@lang('admin_lang.cod')</option>
                                            <option value="O">@lang('admin_lang.online')</option>
                                        </select>
                                        <span class="error_payment_method removeText" style="color: #f85d2c;;"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 up-btns">
                                        <input type="file" class="custom-file-input inpt" name="image[]" id="gallery-photo-add" multiple="">
                                        <label class="custom-file-label extrlft" for="customFile">@lang('admin_lang_static.upload_image')</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="uplodpic">
                                        </div>
                                        <div class="uplodpic gallery">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                    </div>
                                </div>
                                <div class="form-row" style="margin-top: 10px">
                                    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <input type="hidden" id="k_cityname" name="k_cityname" value="">
                                        <input type="hidden" id="pickup_k_cityname" name="pickup_k_cityname" value="">
                                        <button class="btn btn-primary" id="create_external_order" type="button">@lang('admin_lang.create_order')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- footer -->   
@include('admin.includes.footer')
<!-- end footer -->
@endsection
@section('scripts')
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

    $('input.timepicker').timepicker();
    $(".outside_kuwait_state").hide();
    $(".outside_kuwait_city").hide();
    $(".zipDiv").hide();
    $(".outside_kuwait_state1").hide();
    $(".outside_kuwait_city1").hide();
    $(".zipDiv1").hide();
    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd",
        defaultDate: new Date(),
        minDate: new Date(),
        changeMonth: true,
        changeYear: true,
        yearRange: '-100:+0'
    });
    $("#create_external_order").click(function () {
        var merchant = $.trim($("#merchant").val());
        var from_address = $('#from_address').val();
        var from_address_id = $('#from_address_id').val();
        var to_address = $('#to_address').val();
        var to_address_id = $('#to_address_id').val();
        if(merchant == "") {
            $(".error_merchant").text("@lang('validation.required')");
        } else {
            $(".error_merchant").text("");
            if((from_address == 'T' && from_address_id == "") || (to_address == 'T' && to_address_id == "")){
                $('#address_show_error').show();
                $('#address_show_error').text("@lang('merchant_lang.address_selection_error')");
            }else{
                $("#addExternalOrderForm").submit(); 
            }
        }
    });
    var imagesPreview = function (input, placeToInsertImagePreview) {
        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    var new_html = '<li><img src="' + event.target.result + '"></li>';
                    $('.gallery').append(new_html);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    };

    $('#gallery-photo-add').on('change', function () {
        imagesPreview(this, 'div.gallery');
        $('.gallery').html('');
    });

    $(document).on("change", "#country", function (e) {
        $(".removeText").text("");
        var country = $.trim($("#country").val());
        if (country == 134) { //if inside kuwait
            $(".outside_kuwait_state").hide();
            $(".outside_kuwait_city").hide();
            $(".inside_kuwait_state").show();
            $(".inside_kuwait_city").show();
            $(".zipDiv").hide();
        } else {
            $(".inside_kuwait_state").hide();
            $(".outside_kuwait_state").show();
            $(".inside_kuwait_city").hide();
            $(".outside_kuwait_city").show();
            $(".zipDiv").show();
        }
    });
    $(document).on("change","#pickup_country",function(e){
        $(".removeText").text("");
        var country = $.trim($("#pickup_country").val());
        if(country == 134){ //if inside kuwait
            $(".outside_kuwait_state1").hide();
            $(".outside_kuwait_city1").hide();
            $(".inside_kuwait_state1").show();
            $(".inside_kuwait_city1").show();
            $(".zipDiv1").hide();
        }else{
            $(".inside_kuwait_state1").hide();
            $(".outside_kuwait_state1").show();
            $(".inside_kuwait_city1").hide();
            $(".outside_kuwait_city1").show();
            $(".zipDiv1").show();
        }
    });
    $('#pickup_kuwait_city').change(function(event) {
        $('.pickup_city_id_err').html('');  
    });

    function checkRange(cost, country) {
        $.ajax({
            type: "GET",
            url: "{{ route('check.range.exist') }}",
            data: {
                product_weight: cost,
                country: country
            },
            success: function (resp) {
                if (resp == 0) {
                    $(".error_product_weight").text("@lang('admin_lang.weight_out_of_range ')");
                    $("#product_weight").val("");
                } else {
                    $(".error_product_weight").text('');
                }
            }
        });
    }


    $("#addExternalOrderForm").validate({
        messages: {
            fname: {
                required: "@lang('validation.required')"
            },
            merchant: {
                required: "@lang('validation.required')"
            },
            lname: {
                required: "@lang('validation.required')"
            },
            email: {
                required: "@lang('validation.required')",
                email: 'Please provide valid email'
            },
            phone: {
                required: "@lang('validation.required')",
                digits: "@lang('admin_lang.please_provide_valid_phone_number ')"
            },
            order_total: {
                required: "@lang('validation.required')",
                number: "@lang('admin_lang.provide_total_cost')"
            },
            street: {
                required: "@lang('validation.required')"
            },
            city: {
                required: "@lang('validation.required')"
            },
            kuwait_city: {
                required: "@lang('validation.required')"
            },
            zip: {
                required: "@lang('validation.required')"
            },
            country: {
                required: "@lang('validation.required')"
            },
            from_time: {
                required: "@lang('validation.required')"
            },
            delivery_date: {
                required: "@lang('validation.required')"
            },
            payment_method: {
                required: "@lang('validation.required')"
            },
            pickup_street: { 
                required: '@lang('validation.required')'
            },
            pickup_city: { 
                required: '@lang('validation.required')'
            },
            pickup_kuwait_city: { 
                required: '@lang('validation.required')'
            },
            pickup_zip: { 
                required: '@lang('validation.required')'
            },
            pickup_country: { 
                required: '@lang('validation.required')'
            }
        }
    });

    // Changes for address book start
    $('#address_book1').click(function(){
        $('#showAddressBook1').show();
        $('#showField1').hide();
        $('#from_address').val('T');
        var merchant_id = $.trim($("#merchant").val());
        var reqMerchantData = {
            "jsonrpc":"2.0",
            "_token":"{{ csrf_token() }}",
            "data"  :{
                merchant_id:merchant_id
            }
        };
        $.ajax({
            url:"{{ route('admin.get.merchant.address') }}",
            type:"POST",
            data:reqMerchantData,
            success:function(res){
                var pickupAddress = '';
                if(res.address != null){
                    $.each(res.address,function(i,v){
                        var count = i+1;
                        var is_default = '';
                        var city = '';
                        var street = '';
                        var building_no = '';
                        var location = '';
                        var more_address = '';

                        if(v.is_default == 'Y'){
                            is_default = 'checked';
                            $('#from_address_id').val(v.id);
                            $('#from_address_country_id').val(v.country);
                        }else{
                            is_default = '';
                        }

                        if(v.city != null){
                            city = v.city;
                        }else{
                            city = v.get_city_name_by_language.name;
                        }

                        if(v.street != null){
                            street = ','+v.street;
                        }else{
                            street = '';
                        }

                        if(v.building_no != null){
                            building_no = ','+v.building_no;
                        }else{
                            building_no = '';
                        }

                        if(v.location != null){
                            location = ','+v.location;
                        }else{
                            location = '';
                        }

                        if(v.more_address != null){
                            more_address = ','+v.more_address;
                        }else{
                            more_address = '';
                        }
                        if(v.phone != null){
                            phone = ','+v.phone+',<br>';
                        }else{
                            phone = '';
                        }
                        pickupAddress += '<div class="ad1">'+
                            '<h5 style="font-size: 17px;">Address '+ count +'&nbsp;<input type="radio" name="radio4" class="addressRadioFrom" value="'+ v.id +'" data-country="'+ v.country +'"'+is_default+' style="display:inline-block"></h5>'+
                            '<div>'+v.country_details_bylanguage.name+',<br>'+ phone + city + street + '<br>'+
                                building_no + location + '<br>'+ more_address +
                            '</div>'+
                        '</div>';
                    })
                }
                $('#showAddressBook1').html(pickupAddress);
            }
        })
    })
    $('#address_book2').click(function(){
        $('#showAddressBook2').show();
        $('#showField2').hide();
        $('#to_address').val('T');
    })
    
    $('#input_address1').click(function(){
        $('#showAddressBook1').hide();
        $('#showField1').show();
        $('#from_address').val('F');
        $('#from_address_id').val('');
        $('#from_address_country_id').val('');
        $('#from_address_show_error').hide();
        $('#from_address_show_error').text("");
        $('.addressRadioFrom').prop('checked',false);
        var country = $('#pickup_country').val();
        var coun = '';
        if($('#to_address').val() == 'F'){
            coun = $('#country').val();
        } else {
            coun = $('#to_address_country_id').val();;
        }
        if(country == 134 && coun == 134){
            $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_optional')');
            $('#product_weight').removeClass('required');
            $('#product_weight').removeClass('error');
        } else {
            $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_required')');
            $('#product_weight').addClass('required');
            $('#product_weight_label').text('@lang('admin_lang.product_weight_gms_required')');
        }
    })

    $('#input_address2').click(function(){
        $('#showAddressBook2').hide();
        $('#showField2').show();
        $('#to_address').val('F');
        $('#to_address_id').val('');
        $('#to_address_country_id').val('');
        $('#to_address_show_error').hide();
        $('#to_address_show_error').text("");
        $('.addressRadioTo').prop('checked',false)

        var country = $('#pickup_country').val();
        var coun = '';
        if($('#from_address').val() == 'F'){
            coun = $('#country').val();
        }else{
            coun = $('#from_address_country_id').val();;
        }

        if(country == 134 && coun == 134){
            $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_optional')');
            $('#product_weight').removeClass('required');
            $('#product_weight').removeClass('error');
        }else{
            $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_required')');
            $('#product_weight').addClass('required');
            $('#product_weight_label').text('@lang('admin_lang.product_weight_gms_required')');
        }
    })

    $('body').on('click','.addressRadioFrom',function(e){
        var id = $(this).val();
        var country = $(this).attr('data-country');
        var to_address_id = $('#to_address_id').val();
        var coun = '';
        if($('#to_address').val() == 'F'){
            coun = $('#country').val();
        }else{
            coun = $('#to_address_country_id').val();;
        }

        if(country == 134 && coun == 134){
            $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_optional')');
            $('#product_weight').removeClass('required');
            $('#product_weight').removeClass('error');
        }else{
            $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_required')');
            $('#product_weight').addClass('required');
            $('#product_weight_label').text('@lang('admin_lang.product_weight_gms_required')');
        }
        $('#from_address_country_id').val(country);
        $('#from_address_id').val(id);
        if(id == to_address_id){
            $('#from_address_show_error').show();
            $('#from_address_show_error').text("@lang('merchant_lang.select_different_address')");
        }else{
            $('#from_address_show_error').hide();
            $('#from_address_show_error').text("");
        }
    })

    $('body').on('click','.addressRadioTo',function(){
        var id = $(this).val();
        var country = $(this).attr('data-country');
        var from_address_id = $('#from_address_id').val();

        if($('#from_address').val() == 'F'){
            coun = $('#country').val();
        } else {
            coun = $('#from_address_country_id').val();
        }

        if(country == 134 && coun == 134){
            $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_optional')');
            $('#product_weight').removeClass('required');
            $('#product_weight').removeClass('error');
        }else{
            $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_required')');
            $('#product_weight').addClass('required');
            $('#product_weight_label').text('@lang('admin_lang.product_weight_gms_required')');
        }
        $('#to_address_country_id').val(country);
        $('#to_address_id').val(id);
        if(id == from_address_id){
            $('#to_address_show_error').show();
            $('#to_address_show_error').text("@lang('merchant_lang.select_different_address')");
        }else{
            $('#to_address_show_error').hide();
            $('#to_address_show_error').text("");
        }
    })
});


$(document).ready(function () {
    var country = $.trim($("#country").val());
    var sellerCountry = 134;
    $('#merchant').change(function (event) {
        country = $("#country option:selected").val();
        $('#showPickupAddressBlock').show();
        $('#showShippingAddressBlock').show();
        var merchant_id = $(this).val();
        var sellercountryid = $("#merchant option:selected").attr("data-countryid");
        var seller_payment = $("#merchant option:selected").attr("data-paymentmethod");
        if(seller_payment == 'C'){
            var html = '<option value="">'+"@lang('admin_lang.select_payment_method')"+'</option><option value="C">'+"@lang('admin_lang.cod')"+'</option>';
            $("#payment_method").html(html);
        }else if(seller_payment == 'O'){
            var html = '<option value="">'+"@lang('admin_lang.select_payment_method')"+'</option><option value="O">'+"@lang('admin_lang.only_online')"+'</option>';
            $("#payment_method").html(html);
        }else{
            var html = '<option value="">'+"@lang('admin_lang.select_payment_method')"+'</option><option value="C">'+"@lang('admin_lang.cod')"+'</option><option value="O">Online</option>';
            $("#payment_method").html(html);
        }
        // Get merchant address book
        var reqMerchantData = {
            "jsonrpc":"2.0",
            "_token":"{{ csrf_token() }}",
            "data"  :{
                merchant_id:merchant_id
            }
        };
        $.ajax({
            url:"{{ route('admin.get.merchant.address') }}",
            type:"POST",
            data:reqMerchantData,
            success:function(res){
                $('#pickup_country').val(134);
                $('#pickup_kuwait_city').val();
                $('#pickup_city').val();
                $('#pickup_block').val();
                $('#pickup_street').val();
                $('#pickup_zip').val();
                $('#pickup_building').val();
                $('#pac-input1').val();
                $('#lat1').val();
                $('#lng1').val();
                $('#pickup_address').val();

                var pickupAddress = '';
                var shippingAddress = '';
                if(res.address != null){
                    $.each(res.address,function(i,v){
                        var count = i+1;
                        var is_default = '';
                        var city = '';
                        var street = '';
                        var building_no = '';
                        var location = '';
                        var more_address = '';

                        if(v.is_default == 'Y'){
                            is_default = 'checked';
                            $('#from_address_id').val(v.id);
                            $('#from_address_country_id').val(v.country);
                        }else{
                            is_default = '';
                        }

                        if(v.city != null){
                            city = v.city;
                        }else{
                            city = v.get_city_name_by_language.name;
                        }

                        if(v.street != null){
                            street = ','+v.street;
                        }else{
                            street = '';
                        }

                        if(v.building_no != null){
                            building_no = ','+v.building_no;
                        }else{
                            building_no = '';
                        }

                        if(v.location != null){
                            location = ','+v.location;
                        }else{
                            location = '';
                        }

                        if(v.more_address != null){
                            more_address = ','+v.more_address;
                        }else{
                            more_address = '';
                        }
                        if(v.phone != null){
                            phone = ','+v.phone+',<br>';
                        }else{
                            phone = '';
                        }
                        pickupAddress += '<div class="ad1">'+
                            '<h5 style="font-size: 17px;">Address '+ count +'&nbsp;<input type="radio" name="radio4" class="addressRadioFrom" value="'+ v.id +'" data-country="'+ v.country +'"'+is_default+' style="display:inline-block"></h5>'+
                            '<div>'+v.country_details_bylanguage.name+',<br>'+ phone + city + street + '<br>'+
                                building_no + location + '<br>'+ more_address +
                            '</div>'+
                        '</div>';
                    })

                    $.each(res.address,function(i,v){
                        var count = i+1;
                        var is_default = '';
                        var city = '';
                        var street = '';
                        var building_no = '';
                        var location = '';
                        var more_address = '';

                        if(v.city != null){
                            city = v.city;
                        }else{
                            city = v.get_city_name_by_language.name;
                        }

                        if(v.street != null){
                            street = ','+v.street;
                        }else{
                            street = '';
                        }

                        if(v.building_no != null){
                            building_no = ','+v.building_no;
                        }else{
                            building_no = '';
                        }

                        if(v.location != null){
                            location = ','+v.location;
                        }else{
                            location = '';
                        }

                        if(v.more_address != null){
                            more_address = ','+v.more_address;
                        }else{
                            more_address = '';
                        }
                        if(v.phone != null){
                            phone = ','+v.phone+',<br>';
                        }else{
                            phone = '';
                        }
                        shippingAddress += '<div class="ad1">'+
                            '<h5 style="font-size: 17px;">Address '+ count +'&nbsp;<input type="radio" name="radio5" class="addressRadioTo" value="'+ v.id +'" data-country="'+ v.country +'" style="display:inline-block"></h5>'+
                            '<div>'+v.country_details_bylanguage.name+',<br>'+ phone + city + street + '<br>'+
                                building_no + location + '<br>'+ more_address +
                            '</div>'+
                        '</div>';
                    })
                }

                if(res.defaultAddress != null ){
                    $('#pickup_country').val(res.defaultAddress.country);
                    if(res.defaultAddress.city_id != null){
                        $('#pickup_kuwait_city').val(res.defaultAddress.city);
                        $('.inside_kuwait_city1').show();
                        $('.outside_kuwait_city1').hide();
                    }else{
                        $('#pickup_city').val(res.defaultAddress.city);
                        $('.inside_kuwait_city1').hide();
                        $('.outside_kuwait_city1').show();
                    }
                    $('#pickup_block').val(res.defaultAddress.block);
                    $('#pickup_street').val(res.defaultAddress.street);
                    $('#pickup_zip').val(res.defaultAddress.postal_code);
                    $('#pickup_building').val(res.defaultAddress.building_no);
                    $('#pac-input1').val(res.defaultAddress.location);
                    $('#lat1').val(res.defaultAddress.lat);
                    $('#lng1').val(res.defaultAddress.lng);
                    $('#pickup_address').val(res.defaultAddress.more_address);
                }
                $('#showAddressBook1').html(pickupAddress);
                $('#showAddressBook2').html(shippingAddress);
            }
        })
        $("#merchant_country").val(sellercountryid);
        $('#order_total').addClass('required');
        $('#phone').addClass('required');
        $('#street').addClass('required');
        $('#payment_method').addClass('required');
        if (country == 134 && sellercountryid == 134) {
            $(".fname_label").html("@lang('admin_lang.fname_optional')");
            $(".lname_label").html("@lang('admin_lang.lname_optional')");
            $(".block_label").html("@lang('admin_lang.block_required')");
            $(".building_label").html("@lang('admin_lang.build_required')");
            $(".product_weight_label").html("@lang('admin_lang.product_weight_optional')");
            $("#city").val("");
            $('#order_total').addClass('required');
            $('#phone').addClass('required');
            $('#street').addClass('required');
            $('#payment_method').addClass('required');
            $('#kuwait_city').addClass('required');
            $('#street').addClass('required');
            $('#block').addClass('required');
            $('#building').addClass('required');
            $('#fname').removeClass('required');
            $('#fname').removeClass('error'); 
            $('#lname').removeClass('required');
            $('#lname').removeClass('error');
            $('#city').removeClass('required');
            $('#city').removeClass('error');
            $("#fname-error").text("");
            $("#lname-error").text("");
            $("#product_weight-error").text("");
            $("#street-error").text("");
        }
        // out side kuwait
        else {
            $(".fname_label").html("@lang('admin_lang.first_name_required')");
            $(".lname_label").html("@lang('admin_lang.last_name_required')");
            $(".block_label").html("@lang('admin_lang.block_optional')");
            $(".building_label").html("@lang('admin_lang.building_optional')");
            $(".product_weight_label").html("@lang('admin_lang.product_weight_req')");
            $("#block-error").text("");
            $("#building-error").text("");
            $('#fname').addClass('required');
            $('#lname').addClass('required');
            $('#city').addClass('required');
            $('#product_weight').addClass('required');
            //remove required
            $('#block').removeClass('required');
            $('#block').removeClass('error');
            $('#building').removeClass('required');
            $('#building').removeClass('error');
            $('#kuwait_city').removeClass('required');
            $('#kuwait_city').removeClass('error');
            //remove text
        }
        if (country == 134 && sellercountryid == 134) {
            //product_weight
            $('#product_weight').removeClass('required');
            $('#product_weight').removeClass('error');
        } else {
            $('#product_weight').addClass('required');
            $('#product_weight_label').text("@lang('admin_lang.product_weight_req')");
        }
    });
    // when change country
    $('#country').change(function (event) {
        country = $("#country option:selected").val();
        var sellercountryid = $("#merchant option:selected").attr("data-countryid");
        $("#merchant_country").val(sellercountryid);
        // sellerCountry = $("#merchant option:selected").attr("data-countryid");
        // alert("seller country : "+sellerCountry);
        $('#order_total').addClass('required');
        $('#phone').addClass('required');
        $('#street').addClass('required');
        $('#payment_method').addClass('required');
        if (country == 134 && sellercountryid == 134) {
            $(".fname_label").html("@lang('admin_lang.fname_optional')");
            $(".lname_label").html("@lang('admin_lang.lname_optional')");
            $(".block_label").html("@lang('admin_lang.block_required')");
            $(".building_label").html("@lang('admin_lang.build_required')");
            $(".product_weight_label").html("@lang('admin_lang.product_weight_optional')");
            $("#city").val("");
            $('#order_total').addClass('required');
            $('#phone').addClass('required');
            $('#street').addClass('required');
            $('#payment_method').addClass('required');
            $('#kuwait_city').addClass('required');
            $('#street').addClass('required');
            $('#block').addClass('required');
            $('#building').addClass('required');
            $('#fname').removeClass('required');
            $('#fname').removeClass('error'); 
            $('#lname').removeClass('required');
            $('#lname').removeClass('error');
            $('#city').removeClass('required');
            $('#city').removeClass('error');
            $("#fname-error").text("");
            $("#lname-error").text("");
            $("#product_weight-error").text("");
            $("#street-error").text("");
        }
        // out side kuwait
        else {
            $(".fname_label").html("@lang('admin_lang.first_name_required')");
            $(".lname_label").html("@lang('admin_lang.last_name_required')");
            $(".block_label").html("@lang('admin_lang.block_optional')");
            $(".building_label").html("@lang('admin_lang.building_optional')");
            $(".product_weight_label").html("@lang('admin_lang.product_weight_req')");
            $("#block-error").text("");
            $("#building-error").text("");
            $('#fname').addClass('required');
            $('#lname').addClass('required');
            $('#city').addClass('required');
            $('#product_weight').addClass('required');
            //remove required
            $('#block').removeClass('required');
            $('#block').removeClass('error');
            $('#building').removeClass('required');
            $('#building').removeClass('error');
            $('#kuwait_city').removeClass('required');
            $('#kuwait_city').removeClass('error');
            //remove text
        }
        if (country == 134 && sellercountryid == 134) {
            //product_weight
            $('#product_weight').removeClass('required');
            $('#product_weight').removeClass('error');
        } else {
            $('#product_weight').addClass('required');
            $('#product_weight_label').text("@lang('admin_lang.product_weight_req')");
        }
    });

    $('#pickup_country').change(function(event) {
        country = $(this).val();
        $('#order_total').addClass('required');
        $('#phone').addClass('required');
        $('#pickup_street').addClass('required');
        $('#payment_method').addClass('required');

        // seller and buyer both are from kuwait
        if(country == 134) {
            //add text required
            
            $(".fname_label").html("@lang('admin_lang.fname_optional')");
            $(".lname_label").html("@lang('admin_lang.lname_optional')");
            $(".block_label1").html("@lang('admin_lang.block_required')");
            $(".building_label1").html("@lang('admin_lang.buildings_required')");
            $(".product_weight_label").html("@lang('admin_lang.product_weight_gms_optional')");
            //add text optional

            // add required
            $('#order_total').addClass('required');
            
            $('#phone').addClass('required');
            $('#pickup_street').addClass('required');
            $('#payment_method').addClass('required');
            $('#pickup_kuwait_city').addClass('required');
            $('#pickup_street').addClass('required');
            $('#pickup_block').addClass('required');
            $('#pickup_building').addClass('required');
            
            // remove required
            $('#fname').removeClass('required');
            $('#fname').removeClass('error');

            $('#lname').removeClass('required');
            $('#lname').removeClass('error');

            $('#pickup_city').removeClass('required');
            $('#pickup_city').removeClass('error');

            $("#fname-error").text("");
            $("#lname-error").text("");
            $("#product_weight-error").text("");
            $("#pickup_street-error").text("");
            $(".zipDiv1").hide();
            
        } 
        // out side kuwait
        else {
            $(".fname_label").html("@lang('admin_lang.first_name_required')");
            $(".lname_label").html("@lang('admin_lang.last_name_required')");
            $(".block_label1").html("@lang('admin_lang.block_optional')");
            $(".building_label1").html("@lang('admin_lang.building_optional')");
            $(".product_weight_label").html("@lang('admin_lang.product_weight_gms_required')");
            // alert("fgdfgfdgf");
            //add text required
            
            //add text optional

            //add required
            // block-error
            $("#pickup_block-error").text("");
            $("#pickup_building-error").text("");

            $('#fname').addClass('required');
            $('#lname').addClass('required');
            $('#pickup_city').addClass('required');
            
            // $('#street').addClass('required');
            $('#product_weight').addClass('required');

            //remove required
            $('#pickup_block').removeClass('required');
            $('#pickup_block').removeClass('error');
            $('#pickup_building').removeClass('required');
            $('#pickup_building').removeClass('error');
            $('#pickup_kuwait_city').removeClass('required');
            $('#pickup_kuwait_city').removeClass('error');
            $(".zipDiv1").show();
            //remove text

        }
        if(country == 134 && sellerCountry == 134){
            //product_weight
            $('#product_weight').removeClass('required');
            $('#product_weight').removeClass('error');
        }else{
            $('#product_weight').addClass('required');
            $('#product_weight_label').text("@lang('admin_lang.product_weight_gms_required')");
        }
    });

});


function validate(evt) {
    var theEvent = evt || window.event;
    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
<script type="text/javascript">
$(document).ready(function () {
    $('#kuwait_city').keyup(function () {
        var city = $(this).val();
        if (city != '') {
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('admin.fetch.city.all') }}",
                method: "POST",
                data: {
                    city: city,
                    _token: _token
                },
                success: function (response) {
                    if (response.error) {
                    } else {
                        var cityHtml = '<ul><li data-id="" class="kuwait_cities" data-nm="">Select City</li>';
                        response.result.cities.forEach(function (item, index) {
                            cityHtml = cityHtml + '<li class="kuwait_cities" data-id="' + item.city_details_by_language.city_id + '" data-nm="' + item.city_details_by_language.name + '">' + item.city_details_by_language.name + '</li>';
                        })
                        cityHtml = cityHtml + '</ul>';

                        $('#cityList').show();
                        $('#cityList').html(cityHtml);
                    }
                }
            });
        }
    });

    $('#pickup_kuwait_city').keyup(function(){ 
        var city = $(this).val();
        if(city != '')
        {
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url:"{{ route('admin.external.fetch.pickup.city') }}",
                method:"POST",
                data:{city:city, _token:_token},
                success:function(response){
                    // alert(JSON.stringify(response));
                    if(response.status == 'ERROR') {
                        $('#pickup_cityList').html('@lang('admin_lang.nothing_found')');
                    } else {
                    $('#pickup_cityList').fadeIn();  
                    $('#pickup_cityList').html(response.result);
                    }
                }
            });
        }
    });
    $('body').on('click', '.pickUpCityChange', function(){  
        $('#pickup_kuwait_city').val($(this).text());  
        $('#pickup_cityList').fadeOut();  
    });
    $(document).on("blur","#pickup_kuwait_city",function(){
        $("#pickup_k_cityname").val($.trim($("#pickup_kuwait_city").val()));
    });
    $(document).on("keyup", "#kuwait_city", function () {
        $("#k_cityname").val($.trim($("#kuwait_city").val()));
        var value = $.trim($(this).val());
        if (value == "") {
            $("#kuwait_city_value_id").val("");
        }
    });
    $(document).on("blur", "#kuwait_city", function () {
        var id = $.trim($("#kuwait_city_value_id").val());
        var name = $.trim($("#kuwait_city_value_name").val());
        var city = $.trim($("#kuwait_city").val());
        if (city != name) {
            $("#kuwait_city").val("");
            $("#kuwait_city_value_id").val("");
            $("#kuwait_city_value_name").val("");
            $(".city_id_err").text("@lang('admin_lang.please_select_name_city')");
        }else 
        {
            $(".city_id_err").text("");
        }
    });
    $('body').on('click', '.cityChange', function () {
        $('#kuwait_city').val($(this).text());
        $('#cityList').fadeOut();
    });
    $("body").click(function () {
        $("#cityList").fadeOut();
        $("#pickup_cityList").fadeOut();
    });
    $(document).on("click", ".kuwait_cities", function (e) {
        var id = $(e.currentTarget).attr("data-id");
        $("#kuwait_city_value_id").val(id);
    });
    $(document).on("click", ".kuwait_cities", function (e) {
        var id = $(e.currentTarget).attr("data-id");
        var name = $(e.currentTarget).attr("data-nm");
        $("#kuwait_city_value_id").val(id);
        $("#kuwait_city_value_name").val(name);
        $("#kuwait_city").val(name);
        $(".city_id_err").text("");
        $("error_kuwait_city").text("");
    });
    $('.slct').chosen();
});    
</script>

<script>
function initMap1() {
    var input = document.getElementById('pac-input1');
    var autocomplete = new google.maps.places.Autocomplete(input);
    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    autocomplete.setTypes(['address']);

    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
      $('#lat1').val(place.geometry.location.lat())
      $('#lng1').val(place.geometry.location.lng())
    });
}
function initMap2() {
    var input = document.getElementById('pac-input2');
    var autocomplete = new google.maps.places.Autocomplete(input);
    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    autocomplete.setTypes(['address']);

    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
      $('#lat2').val(place.geometry.location.lat())
      $('#lng2').val(place.geometry.location.lng())
    });
}
$(document).ready(function() {
    $('#pac-input1').blur(function() {
        if($(this).val() == '') {
            $('#lat1').val('')
            $('#lng1').val('')
        }
    })
    $('#pac-input2').blur(function() {
        if($(this).val() == '') {
            $('#lat2').val('')
            $('#lng2').val('')
        }
    })
})
</script>

<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API') }}&libraries=places&callback=initMap1" async defer></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API') }}&libraries=places&callback=initMap2" async defer></script>
@endsection