@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Manage Driver') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.manage_zone_rate')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<style type="text/css">
    #drivers_filter{
        display: none;
    }

    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
</style>
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.manage_zone_rate') </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.Dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.manage_zone_rate')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="mail_success" style="display: none;">
                <div class="alert alert-success vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>@lang('admin_lang.success')!</strong> <span class="successMsg">@lang('admin_lang.mail_sent')!</span>
                </div>
            </div>
            <div class="mail_error" style="display: none;">
                <div class="alert alert-danger vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>@lang('admin_lang.error')!</strong><span class="errorMsg">@lang('admin_lang.unauthorized_mail_not_sent')</span>
                </div>
            </div>
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong class="success_msg"></strong>  
            </div>
            <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong class="error_msg"></strong>  
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader -->
            <!-- ============================================================== -->
            <div class="row">
                <!-- ============================================================== -->
                <!-- basic table  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">@lang('admin_lang.manage_zone_rate') <a class="adbtn btn btn-primary" href="{{ route('admin.zone.rate.add') }}"><i class="fas fa-plus"></i> @lang('admin_lang.add')</a></h5>
                        <div class="card-body">
                            <div class="table-responsive listBody">
                                <table class="table table-striped table-bordered first" id="zoneRate">
                                    <thead>
                                        <tr>
                                            <th>@lang('admin_lang.zone1')</th>
                                            {{-- <th>@lang('admin_lang.zone2')</th> --}}
                                            <th>@lang('admin_lang.from_weight')</th>
                                            <th>@lang('admin_lang.to_weight')</th>
                                            <th>@lang('admin_lang.internal_order')</th>
                                            <th>@lang('admin_lang.external_order')</th>
                                            <th>@lang('admin_lang.Action')</th>
                                        </tr>
                                    </thead>
                                    
                                    <tfoot>
                                        <tr>
                                            <th>@lang('admin_lang.zone1')</th>
                                            {{-- <th>@lang('admin_lang.zone2')</th> --}}
                                            <th>@lang('admin_lang.from_weight')</th>
                                            <th>@lang('admin_lang.to_weight')</th>
                                            <th>@lang('admin_lang.internal_order')</th>
                                            <th>@lang('admin_lang.external_order')</th>
                                            <th>@lang('admin_lang.Action')</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end basic table  -->
                <!-- ============================================================== -->
            </div>
        </div>
        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
        <div class="loader" style="display: none;">
            <img src="{{url('public/loader.gif')}}">
        </div>
        <div class="loader" style="display: none;">
            <img src="{{url('public/loader.gif')}}">
        </div>

        
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click', '.reset_search', function() {
            $('#zoneRate').DataTable().search('').columns().search('').draw();
        })

        function filterColumn ( i ) {
            $('#zoneRate').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }

        $('#zoneRate').DataTable( {
            stateSave: true,
            "order": [[0, "asc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('admin.zone.rate.list') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
                {   
                    render: function(data, type, full) {
                        return full.get_zone.zone_name
                    }
                },
                // {  
                //     render: function(data, type, full) {
                //         return full.get_zone.get_zone2.zone_name
                //     }  
                // },
                {   
                    render: function(data, type, full) {
                        return full.from_weight
                    }  
                },
                {  
                    render: function(data, type, full) {
                        if(full.infinity_weight == 'Y'){
                            return "∞ @lang('admin_lang.infinity')";
                        }else{
                            return full.to_weight;
                        }
                        
                    }  
                },
                {  
                    render: function(data, type, full) {
                        return full.internal_outside_kuwait
                    } 
                },
                { 
                    render: function(data, type, full) {
                        return full.external_outside_kuwait
                    }  
                },
                {   
                    render: function(data, type, full) {
                        var a = '';

                        a += ' <a href="{{ url('admin/edit-zone-rate') }}/'+full.id+'"><i class="fas fa-edit" title="@lang('admin_lang.Edit')"></i></a>'

                        a += ' <a href="javascript:void(0)" class="delete_zone_rate" data-id="'+full.id+'"><i class=" fas fa-trash" title="@lang('admin_lang.Delete')"></i></a>'
                        return a;
                    }
                }
            ]
        });

        // change event
        $('input.keyword').on( 'keyup click blur', function () {
            filterColumn( 1 );
        });

    $(document).on("click",".delete_zone_rate",function(e){
        var id = $(e.currentTarget).attr("data-id");
        var obj = $(this);
        if(confirm("@lang('admin_lang.want_to_delete_zone_rate')")){
            var reqData = {
                'jsonrpc':"2.0",
                "_token":"{{ csrf_token() }}",
                "data":{
                    id:id
                }
            }
            $.ajax({
                url:"{{ route('admin.zone.rate.delete') }}",
                type:"post",
                data:reqData,
                success:function(resp){
                    if(resp.status == 1){
                        $(".success_msg").html("@lang('admin_lang.zone_rate_deleted_success')");
                        // $(".status_"+id).html(st);
                        $(".success_msg_div").show();
                        $(".error_msg_div").hide();
                        // $(".tr_"+id).hide();
                        // $(".int_st_icon_"+id).hide();
                        obj.parent().parent().hide();
                    }else{
                        $(".error_msg_div").show();
                        $(".success_msg_div").hide();
                        $(".error_msg").html("@lang('admin_lang.zone_rate_deleted_error')");
                    }
                }
            });
        }
        // }
    });

  
    });
</script>
@endsection