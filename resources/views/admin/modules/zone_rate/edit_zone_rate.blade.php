@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | Add Shipping Cost') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.edit_zone_rate')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.e_commerce_dashboard_template')  </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="breadcrumb-link">@lang('admin_lang.dashboard')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@lang('admin_lang.edit_zone_rate')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
            </div>
            @elseif ((session()->has('error')))
            <div class="alert alert-danger vd_hidden" style="display: block;">
                <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                </a>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
            </div>
            @endif
            <div class="ecommerce-widget">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">@lang('admin_lang.edit_zone_rate')</h5>
                            <div class="card-body">
                                <form id="editZoneRateFrm" method="POST" action="{{ route('admin.zone.rate.update') }}">
                                    @csrf
                                    {{-- <input type="hidden" name="zone_rate_master_id" value="{{ @$zoneRate->id }}"> --}}
                                    <input type="hidden" name="id" value="{{ @$zoneRate->id }}">
                                    <div class="row">
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="zone_id_1" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.zone1')</label>
                                            <select id="zone_id_1" name="zone_id_1" class="form-control">
                                                <option value="">@lang('admin_lang.select_zone1')</option>
                                                @if(@$zones)
                                                    @foreach($zones as $z)
                                                        <option value="{{ @$z->id }}" @if($z->id == @$zoneRate->zone_id) {{ 'selected' }} @endif>{{ @$z->zone_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <span class="error_zone_id_1" style="color: red;"></span>
                                        </div>
                                        {{-- <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="zone_id_2" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.zone2')</label>
                                            <select id="zone_id_2" name="zone_id_2" class="form-control">
                                                <option value="">@lang('admin_lang.select_zone2')</option>
                                                @if(@$zones)
                                                    @foreach($zones as $z)
                                                        <option value="{{ @$z->id }}" @if($z->id == $zoneRate->zone_id_2) {{ 'selected' }} @endif>{{ @$z->zone_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <span class="error_zone_id_2" style="color: red;"></span>
                                        </div> --}}
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="from_weight" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.from_weight_grm')</label>
                                            <input id="from_weight" name="from_weight" type="text" class="form-control" placeholder="From weight" onkeypress='validate(event)' value="{{ @$zoneRate->from_weight }}">
                                            <span class="error_from_weight" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" id="to_weight_div">
                                            <label for="to_weight" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.to_weight_grm')</label>
                                            <input id="to_weight" name="to_weight" type="text" class="form-control" placeholder="To weight" onkeypress='validate(event)' value="{{ @$zoneRate->to_weight }}">
                                            <span class="error_to_weight" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="internal_order_rate" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.internal_order_grm')</label>
                                            <input id="internal_order_rate" name="internal_order_rate" type="text" class="form-control" placeholder="Internal order rate" onkeypress='validate(event)' value="{{ @$zoneRate->internal_outside_kuwait }}">
                                            <span class="error_internal_order_rate" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label for="external_order_rate" class="col-form-label"><span style="color: red;">*</span>@lang('admin_lang.external_order_grm')</label>
                                            <input id="external_order_rate" name="external_order_rate" type="text" class="form-control" placeholder="External order rate" onkeypress='validate(event)' value="{{ @$zoneRate->external_outside_kuwait }}">
                                            <span class="error_external_order_rate" style="color: red;"></span>
                                        </div>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <label for="infinity_weight" class="col-form-label">
                                            <input id="infinity_weight" name="infinity_weight" type="checkbox" value="Y" @if(@$zoneRate->infinity_weight == 'Y') checked="" @endif>@lang('admin_lang.infinity_weight')</label>
                                        </div>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <a href="javascript:void(0)" id="editZoneRate" class="btn btn-primary">@lang('admin_lang.save')</a>
                                        </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer -->   
        @include('admin.includes.footer')
        <!-- end footer -->
    </div>
    <!-- ============================================================== -->
    <!-- end wrapper  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper  -->

@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $("#editZoneRate").click(function(){
            var rurl = "{{ route('check.duplicate.range.zone.rate') }}",
            from_weight = $.trim($("#from_weight").val()),
            to_weight = $.trim($("#to_weight").val()),
            from_zone = $.trim($("#zone_id_1").val());
            if(to_weight == '') {
                to_weight = 0.000;
            }
            if(from_weight !=""){
                from_weight =parseFloat(from_weight);
                to_weight =parseFloat(to_weight);
                var reqData = {
                    'jsonrpc':'2.0',
                    '_token' :"{{ csrf_token() }}",
                    'data'   :{
                        from_weight: from_weight,
                        to_weight: to_weight,
                        from_zone:from_zone,
                        old_id: "{{ @$zoneRate->id }}"
                    }
                }
                $.ajax({
                    type:"POST",
                    url:rurl,
                    data:reqData,
                    success:function(resp){
                        if(resp.status == 1){
                            $(".error_from_weight").text('');
                            $(".error_to_weight").text('');
                            if($('#zone_id_1').val() == $('#zone_id_2').val()){
                                $("#zone_id_1").val("");
                                $("#zone_id_2").val("");
                                $(".error_zone_id_2").text('@lang('admin_lang.zone_name_different')');
                            }else{
                                $("#editZoneRateFrm").submit();
                            }
                            
                        }else{
                            $("#from_weight").val("");
                            $("#to_weight").val("");
                            $(".error_from_weight").text('@lang('admin_lang.range_already_exist')');
                            $(".error_to_weight").text('@lang('admin_lang.range_already_exist')');
                        }
                    }
                });

            }else{
                if(from_weight == ""){
                    $(".error_from_weight").text('@lang('validation.error_weight')');
                }
                if(to_weight == ""){
                    $(".error_to_weight").text('@lang('validation.error_weight')');
                }
            }
            
        });
        jQuery.validator.addMethod("notEqual", function(value, element, param) {
            return this.optional(element) || value != param;
        }, "Please specify a different (non-default) value");
        $("#editZoneRateFrm").validate({
            rules:{
                'from_weight':{
                    required:true,
                    number:true
                },
                'to_weight':{
                    required:true,
                    number:true
                },
                'internal_order_rate':{
                    required:true,
                    number:true
                },
                'external_order_rate':{
                    required:true,
                    number:true
                },
                'zone_id_1':{
                    required:true,
                },
                'zone_id_2':{
                    required:true,
                },
            },
            messages: { 
                from_weight: { 
                    required: '@lang('validation.required')'
                },
                to_weight: { 
                    required: '@lang('validation.required')'
                },
                internal_order_rate: { 
                    required: '@lang('validation.required')'
                },
                external_order_rate: { 
                    required: '@lang('validation.required')'
                },
                zone_id_1: { 
                    required: '@lang('validation.required')'
                },
                zone_id_2: { 
                    required: '@lang('validation.required')'
                },
            },
            errorPlacement: function (error, element) 
            {
                // console.log(element.attr("name"));
                // console.log(error);
                if (element.attr("name") == "from_weight") {
                    var error = '<label for="from_weight" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.error_weight')'+'</label>';
                    $('.error_from_weight').html(error);
                }
                if (element.attr("name") == "to_weight") {
                    var error = '<label for="to_weight" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.error_weight')'+'</label>';
                    $('.error_to_weight').html(error);
                }
                if (element.attr("name") == "internal_order_rate") {
                    var error = '<label for="internal_order_rate" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.error_internal_order_rate')'+'</label>';
                    $('.error_internal_order_rate').html(error);
                }
                if (element.attr("name") == "external_order_rate") {
                    var error = '<label for="external_order_rate" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.error_external_order_rate')'+'</label>';
                    $('.error_external_order_rate').html(error);
                }
                if (element.attr("name") == "zone_id_1") {
                    var error = '<label for="zone_id_1" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
                    $('.error_zone_id_1').html(error);
                }
                if (element.attr("name") == "zone_id_2") {
                    var error = '<label for="zone_id_2" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+'@lang('validation.required')'+'</label>';
                    $('.error_zone_id_2').html(error);
                }
            }
        });

        $('#infinity_weight').click(function() {
            if($(this).is(':checked')) {
                $("#to_weight_div").hide();
            } else {
                $("#to_weight_div").show();
                $("#to_weight").val('');
            }
        });
    });

function validate(evt) {
    var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>
@endsection