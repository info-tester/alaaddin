
<div class="dashboard-header">
    <nav class="navbar navbar-expand-lg bgl-white fixed-top">
        <a class="navbar-brand" href="{{ route('admin.dashboard') }}"><img src="{{ asset('public/admin/assets/images/logo2.png') }}" alt=""></a>
        <button class="navbar-toggler ntifymnu" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class=" fas fa-align-right"></i></span>
        </button>
        <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto navbar-right-top">
                {{-- <li class="nav-item dropdown notification">
                    @if(Config::get('app.locale') == 'en')
                    <a class="nav-link nav-icons" href="javascript:;" onclick="location.href='{{ route('admin.change.lang') }}'" id="paste-lan">عربي</a>
                    @elseif(Config::get('app.locale') == 'ar')
                    <a class="nav-link nav-icons" href="javascript:;" onclick="location.href='{{ route('admin.change.lang') }}'" id="paste-lan">English</a>
                    @endif
                </li> --}}
                {{-- <li class="nav-item dropdown notification">
                    <a class="nav-link nav-icons" href="#" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-fw fa-bell"></i> <span class="indicator"></span></a>
                    <ul class="dropdown-menu dropdown-menu-right notification-dropdown">
                        <li>
                            <div class="notification-title"> Notification</div>
                            <div class="notification-list">
                                <div class="list-group">
                                    <a href="#" class="list-group-item list-group-item-action active">
                                        <div class="notification-info">
                                            <div class="notification-list-user-img"><img src="{{ asset('public/admin/assets/images/avatar-2.jpg') }}" alt="" class="user-avatar-md rounded-circle"></div>
                                            <div class="notification-list-user-block"><span class="notification-list-user-name">Jeremy Rakestraw</span>accepted your invitation to join the team.
                                                <div class="notification-date">2 min ago</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item list-group-item-action">
                                        <div class="notification-info">
                                            <div class="notification-list-user-img"><img src="{{ asset('public/admin/assets/images/avatar-3.jpg') }}" alt="" class="user-avatar-md rounded-circle"></div>
                                            <div class="notification-list-user-block"><span class="notification-list-user-name">{{ Auth::guard('admin')->user()->fname }} {{ Auth::guard('admin')->user()->lname }}</span>is now following you
                                                <div class="notification-date">2 days ago</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item list-group-item-action">
                                        <div class="notification-info">
                                            <div class="notification-list-user-img"><img src="{{ asset('public/admin/assets/images/avatar-4.jpg') }}" alt="" class="user-avatar-md rounded-circle"></div>
                                            <div class="notification-list-user-block"><span class="notification-list-user-name">Monaan Pechi</span> is watching your main repository
                                                <div class="notification-date">2 min ago</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item list-group-item-action">
                                        <div class="notification-info">
                                            <div class="notification-list-user-img"><img src="{{ asset('public/admin/assets/images/avatar-5.jpg') }}" alt="" class="user-avatar-md rounded-circle"></div>
                                            <div class="notification-list-user-block"><span class="notification-list-user-name">Jessica Caruso</span>accepted your invitation to join the team.
                                                <div class="notification-date">2 min ago</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <!-- <li> -->
                            <!-- <div class="list-footer"> <a href="#">View all notifications</a></div> -->
                        <!-- </li> -->
                    </ul>
                </li> --}}
                <li class="nav-item dropdown nav-user">
                    <a class="nav-link nav-user-img img_users" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(@Auth::guard('admin')->user()->profile_pic)
                        <!-- <img src="{{ asset('public/sub_admin_pics/').@Auth::guard('admin')->user()->profile_pic }}" alt="" class="user-avatar-md rounded-circle"> -->
                        <img  src="{{url('storage/app/public/sub_admin_pics/'.@Auth::guard('admin')->user()->profile_pic)}}" alt="" class="user-avatar-md rounded-circle">
                        @else
                        <img src="{{ asset('public/admin/assets/images/person_icon.png') }}" alt="" class="user-avatar-md rounded-circle">
                        @endif
                        <!-- <img src="{{ getPersonDefaultImageUrl() }}" alt="" class="user-avatar-md rounded-circle"> -->
                        <p>Hi, {{ @Auth::guard('admin')->user()->fname }}</p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                        <div class="nav-user-info">
                            <h5 class="mb-0 text-white nav-user-name">{{ Auth::guard('admin')->user()->fname }} {{ Auth::guard('admin')->user()->lname }}</h5>
                            <!-- <span class="status"></span><span class="ml-2">Available</span> -->
                        </div>
                        <a class="dropdown-item" href="{{ route('admin.view.profile') }}"><i class="fas fa-edit mr-2"></i>Edit Profile</a>
                        <a class="dropdown-item" href="{{ url('/admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-power-off mr-2"></i>Logout</a>
                        <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>