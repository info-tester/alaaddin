<div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light" >
            <a class="d-xl-none d-lg-none" href="{{ route('admin.dashboard') }}">@lang('admin_lang.Dashboard')</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class=" fas fa-align-justify"></i></span>
            </button>
            @if(Auth::guard('admin')->user()->type == 'A')
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav flex-column">
                    <li class="nav-divider">
                        @lang('admin_lang.menu')
                    </li>
                    <li class="nav-item">
                        <a class=" nav-link dashboard <?php if(Request::segment(2)==''){echo "active";}?> drpnon" href="{{ route('admin.dashboard') }}" onclick="location.href='{{ route('admin.dashboard') }}'" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1"><i class="fa fa-fw fa-user-circle"></i> @lang('admin_lang.Dashboard') <span class="badge badge-success"></span></a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="othersTab nav-link settings" href="#" data-toggle="collapse" 
                        @if(Request::segment(2)=='list-sub-admin' || Request::segment(2)=='add-sub-admin' || Request::segment(2)=='edit-sub-admin' || Request::segment(2)=='list-city' || Request::segment(2)=='edit-city' || Request::segment(2)=='add-city' || Request::segment(2)=='manage-maintenance')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif
                        data-target="#submenu-2" aria-controls="submenu-2"><i class="fa fa-fw fa-rocket"></i>@lang('admin_lang.setting')</a>
                        <div id="submenu-2" class="collapse submenu
                            @if(Request::segment(2)=='list-sub-admin'|| Request::segment(2)=='add-sub-admin' || Request::segment(2)=='edit-sub-admin' || Request::segment(2)=='update-loyalty-point' || Request::segment(2)=='list-city' || Request::segment(2)=='edit-city' || Request::segment(2)=='add-city' || Request::segment(2)=='manage-coupon' || Request::segment(2)=='add-coupon' || Request::segment(2)=='edit-coupon' || Request::segment(2)=='show-subadmin-logs' || Request::segment(2)=='manage-time-slot' || Request::segment(2)=='edit-unit' || Request::segment(2)=='list-unit' || Request::segment(2)=='manage-maintenance')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='list-unit')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.list.unit') }}">Manage Unit</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link 
                                        @if(Request::segment(2)=='list-city' || Request::segment(2)=='edit-city')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.list.city') }}">Manage City</a>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link 
                                        @if(Request::segment(2)=='manage-maintenance')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.manage.maintenance') }}">Configuration</a>
                                </li>
                                
                            </ul>
                        </div>
                    </li>

                    
                    <li class="nav-item">
                        <a class="othersTab nav-link product product_settings" href="#" data-toggle="collapse"
                        @if(Request::segment(2)=='list-category' || Request::segment(2)=='add-category' || Request::segment(2)=='edit-category'|| Request::segment(2)=='add-manufacturer'  || Request::segment(2)=='list-manufacturer' || Request::segment(2)=='manage-product'|| Request::segment(2)=='add-product-step-one'||Request::segment(2)=='add-product-step-two'|| Request::segment(2)=='add-product-step-three' || Request::segment(2)=='add-product-step-four' || Request::segment(2)=='list-product-options' || Request::segment(2)=='add-product-option' || Request::segment(2)=='list-option-values' || Request::segment(2)=='edit-product' ||Request::segment(2)=='edit-manufacturer' || Request::segment(2)=='edit-option-values' || Request::segment(2)=='product-comments-review-ratings')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif
                        data-target="#submenu-3" aria-controls="submenu-3"><i class="fas fa-fw fa-chart-pie"></i>@lang('admin_lang.product_settings') </a>
                        <div id="submenu-3" class="collapse submenu 
                            @if(Request::segment(2)=='list-category'|| Request::segment(2)=='add-category' || Request::segment(2)=='edit-category'|| 
                            Request::segment(2)=='add-manufacturer' ||  Request::segment(2)=='edit-product-option' || Request::segment(2)=='edit-manufacturer' || Request::segment(2)=='list-manufacturer' ||Request::segment(2)=='manage-product'|| Request::segment(2)=='add-product-step-one'|| Request::segment(2)=='add-product-step-two'|| Request::segment(2)=='add-product-step-three'|| Request::segment(2)=='add-product-step-four' || Request::segment(2)=='list-product-options' || Request::segment(2)=='add-product-option' || Request::segment(2)=='list-option-values' || Request::segment(2)=='edit-product' || Request::segment(2)=='edit-option-values' || Request::segment(2)=='product-comments-review-ratings')
                            show
                            @else
                            @endif
                            " style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link manageProduct
                                        @if(Request::segment(2)=='manage-product'|| Request::segment(2)=='add-product-step-one'|| Request::segment(2)=='add-product-step-two'|| Request::segment(2)=='add-product-step-three' || Request::segment(2)=='add-product-step-four' || Request::segment(2)=='edit-product' || Request::segment(2)=='product-discount' || Request::segment(2)=='product-comments-review-ratings')
                                        active
                                        @else
                                        @endif
                                        " href="{{ route('manage.product') }}">@lang('admin_lang.manage_product')</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link manageProductCategory 
                                        @if(Request::segment(2)=='list-category' || Request::segment(2)=='edit-category' || Request::segment(2)=='add-category')
                                        active
                                        @else
                                        @endif
                                        " href="{{ route('admin.list.category') }}">@lang('admin_lang.manage_product_category')</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="othersTab nav-link" href="#" data-toggle="collapse" @if(Request::segment(2)=='customer-wallet-history' || Request::segment(2)=='manage-customer' || Request::segment(2)=='edit-customer' || Request::segment(2)=='view-customer-profile' || Request::segment(2)=='customer-withdraw')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif data-target="#submenu-5" aria-controls="submenu-5"><i class="fas fa-fw fa-table"></i>@lang('admin_lang.customers')</a>
                        <div id="submenu-5" class="collapse submenu @if(Request::segment(2)=='customer-wallet-history' || Request::segment(2)=='manage-customer' || Request::segment(2)=='edit-customer' || Request::segment(2)=='view-customer-profile' || Request::segment(2)=='customer-wallet-history' || Request::segment(2)=='customer-withdraw')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='edit-customer' || Request::segment(2)=='manage-customer' || Request::segment(2)=='view-customer-profile' || Request::segment(2)=='customer-withdraw')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.list.customer') }}">@lang('admin_lang.manage_customers')</a>
                                    {{-- {{ route('admin.list.customer') }} --}}
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='customer-wallet-history')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.customer.wallet.history') }}">Wallet History</a>
                                
                                </li>
                                
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="othersTab nav-link" href="#" data-toggle="collapse" @if(Request::segment(2)=='add-merchant' || Request::segment(2)=='edit-merchant'|| Request::segment(2)=='view-merchant-profile' || Request::segment(2)=='manage-merchant')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif  data-target="#submenu-6" aria-controls="submenu-6"><i class="fas fa-fw fa-file"></i> @lang('admin_lang.merchant') </a>
                        <div id="submenu-6" class="collapse submenu @if(Request::segment(2)=='add-merchant' || Request::segment(2)=='edit-merchant'|| Request::segment(2)=='view-merchant-profile' || Request::segment(2)=='manage-merchant' || Request::segment(2)=='show-address-book' || Request::segment(2)=='add-merchant-address' || Request::segment(2)=='edit-merchant-address')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='manage-merchant' || Request::segment(2)=='edit-merchant' || Request::segment(2)=='view-merchant-profile' || Request::segment(2)=='show-address-book' || Request::segment(2)=='add-merchant-address' || Request::segment(2)=='edit-merchant-address')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.list.merchant') }}">@lang('admin_lang.mng_merchants')</a>
                                    
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="othersTab nav-link" href="#" data-toggle="collapse" @if(Request::segment(2)=='customer-cancel-request-history')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif data-target="#submenu-12" aria-controls="submenu-12"><i class="fas fa-fw fa-table"></i>Cancel Request</a>
                        <div id="submenu-12" class="collapse submenu @if(Request::segment(2)=='customer-cancel-request-history')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='customer-cancel-request-history')
                                    active
                                    @else
                                    @endif" href="{{ route('admin.customer.cancel.request.history') }}">Cancel Request </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="othersTab nav-link" href="#" data-toggle="collapse" @if(Request::segment(2)=='payment-tbl-show')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif data-target="#submenu-14" aria-controls="submenu-14"><i class="fas fa-fw fa-table"></i>Customer Payments</a>
                        <div id="submenu-14" class="collapse submenu @if(Request::segment(2)=='payment-tbl-show')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='payment-tbl-show')
                                    active
                                    @else
                                    @endif" href="{{ route('admin.show.payment.tble') }}">Customer Payments </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="othersTab nav-link" href="#" data-toggle="collapse" @if(Request::segment(2)=='view-order-details' || Request::segment(2)=='manage-orders' || Request::segment(2)=='view-payment-info-details' || Request::segment(2)=='view-order-wise-insurance-collection')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif data-target="#submenu-8" aria-controls="submenu-8"><i class="fas fa-fw fa-columns"></i>@lang('admin_lang.ords')</a>
                        <div id="submenu-8" class="collapse submenu @if(Request::segment(2)=='view-order-details' || Request::segment(2)=='manage-orders' || Request::segment(2)=='add-order' || Request::segment(2)=='edit-external-order-details' || Request::segment(2)=='view-product-wise-payment-info-details' || Request::segment(2)=='view-order-wise-insurance-collection')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='view-order-details' || Request::segment(2)=='manage-orders' || Request::segment(2)=='edit-external-order-details') active @endif" href="{{ route('admin.list.order') }}">@lang('admin_lang.manage_orders')</a>
                                </li> 
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='view-payment-info-details') active @endif" href="{{ route('admin.viewOrderWiseInfoDtls.view.info.details') }}">Payment Details</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='view-order-wise-insurance-collection') active @endif" href="{{ route('admin.view.order.wise.info.details') }}">Insurance Collection</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="othersTab nav-link" href="#" data-toggle="collapse"  @if(Request::segment(2)=='finance' || Request::segment(2)=='list-request-withdrawl')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif data-target="#submenu-9" aria-controls="submenu-9"><i class="fas fa-database"></i> @lang('admin_lang.fnc')</a>
                        <div id="submenu-9" class="collapse submenu @if(Request::segment(2)=='finance' || Request::segment(2)=='list-request-withdrawl' || Request::segment(2)=='add-invoice' || Request::segment(2)=='manage-invoice' || Request::segment(2)=='edit-invoice' || Request::segment(2)=='view-invoice')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='finance')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.finance') }}">@lang('admin_lang.view_earnings')</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='list-request-withdrawl')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.list.request.withdrawl') }}">@lang('admin_lang.process_withdrawl_reqst')</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="othersTab nav-link" href="#" data-toggle="collapse"  @if(Request::segment(2)=='add-faq' || Request::segment(2)=='about-page-content' || Request::segment(2)=='show-help')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif data-target="#submenu-11" aria-controls="submenu-11"><i class="fas fa-database"></i>@lang('admin_lang.content')</a>
                        <div id="submenu-11" class="collapse submenu @if(Request::segment(2)=='add-faq' || Request::segment(2)=='list-faq' || Request::segment(2)=='about-page-content' || Request::segment(2)=='terms-conditions-page-content' || Request::segment(2)=='contact-page-content' || Request::segment(2)=='faq-category' || Request::segment(2)=='app-settings' || Request::segment(2)=='edit-faq'|| Request::segment(2)=='privecy-page-content' || Request::segment(2)=='banner-listing' || Request::segment(2)=='add-banner' || Request::segment(2)=='banner-details' || Request::segment(2)=='show-help')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='show-help')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.get.help.page') }}">Help  </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='list-faq' || Request::segment(2)=='add-faq' || Request::segment(2)=='edit-faq')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.list.faq') }}">@lang('admin_lang.Faq')</a>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='about-page-content')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.manage.pageabout') }}">@lang('admin_lang.abou')</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='contact-page-content')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.manage.pagecontact') }}">@lang('admin_lang.contact')</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='banner-listing' || Request::segment(2)=='add-banner' || Request::segment(2)=='banner-details')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.manage.bannerlist') }}">Banner</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='privecy-page-content')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.manage.page.privecy',2) }}">Privacy policy</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='terms-conditions-page-content')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.manage.page.termsconditions',1) }}">Terms & conditions</a>
                                </li>
                                
                                
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            
            @elseif(Auth::guard('admin')->user()->type == 'S')
            <div class="collapse navbar-collapse" id="navbarNav">
                @php 
                $permission = array(); 
                $user = App\Admin::with('sidebarAccess.menuDetails')
                ->where('id', Auth::guard('admin')->user()->id)
                ->first();
                @endphp
                @if($user->sidebarAccess !=null)
                @foreach($user->sidebarAccess as $row)
                <?php array_push($permission, @$row->menu_id);  ?>
                @endforeach
                @endif
                <ul class="navbar-nav flex-column">
                    <li class="nav-divider">
                        @lang('admin_lang.menu')
                    </li>
                    @if(in_array(8, $permission))
                    <li class="nav-item ">
                        <a class="nav-link dashboard <?php if(Request::segment(2)==''){echo "active";}?> drpnon" href="{{ route('admin.dashboard') }}" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1"><i class="fa fa-fw fa-user-circle"></i> @lang('admin_lang.Dashboard')<span class="badge badge-success"></span></a>
                    </li>
                    @endif
                    @if(in_array(1, $permission))
                    <li class="nav-item">
                        <a class="nav-link settings" href="#" data-toggle="collapse" 
                        @if(Request::segment(2)=='list-sub-admin' || Request::segment(2)=='add-sub-admin' || Request::segment(2)=='edit-sub-admin' || Request::segment(2)=='list-city' || Request::segment(2)=='edit-city' || Request::segment(2)=='add-city')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif
                        data-target="#submenu-2" aria-controls="submenu-2"><i class="fa fa-fw fa-rocket"></i>@lang('admin_lang.setting')</a>
                        <div id="submenu-2" class="collapse submenu
                            @if(Request::segment(2)=='list-sub-admin'|| Request::segment(2)=='add-sub-admin' || Request::segment(2)=='edit-sub-admin' || Request::segment(2)=='update-loyalty-point' || Request::segment(2)=='list-city' || Request::segment(2)=='edit-city' || Request::segment(2)=='add-city' || Request::segment(2)=='manage-time-slot' || Request::segment(2)=='add-time-slot' || Request::segment(2)=='edit-time-slot' || Request::segment(2)=='manage-coupon' || Request::segment(2)=='add-coupon' || Request::segment(2)=='edit-coupon')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link 
                                        @if(Request::segment(2)=='add-country')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.add.country') }}">Add Country</a>
                                
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link 
                                        @if(Request::segment(2)=='add-country')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.list.country') }}">List of Country</a>
                                
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link 
                                        @if(Request::segment(2)=='add-sub-admin')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.add.subadmin') }}">@lang('admin_lang.add_sub_admin')</a>
                                    
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link 
                                        @if(Request::segment(2)=='list-sub-admin' || Request::segment(2)=='edit-sub-admin')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.list.subadmin') }}">@lang('admin_lang.manage_sub_admin')</a>
                                    
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link 
                                        @if(Request::segment(2)=='list-city' || Request::segment(2)=='edit-city')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.list.city') }}">@lang('admin_lang.manage_city')</a>
                                    
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link 
                                        @if(Request::segment(2)=='manage-maintenance')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.manage.maintenance') }}">Maintenance</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    @endif
                    @if(in_array(2, $permission))
                    <li class="nav-item">
                        <a class="nav-link product product_settings" href="#" data-toggle="collapse"
                        @if(Request::segment(2)=='list-category' || Request::segment(2)=='add-category' || Request::segment(2)=='edit-category'|| Request::segment(2)=='add-manufacturer'  || Request::segment(2)=='list-manufacturer' || Request::segment(2)=='manage-product'|| Request::segment(2)=='add-product-step-one'||Request::segment(2)=='add-product-step-two'|| Request::segment(2)=='add-product-step-three' || Request::segment(2)=='add-product-step-four' || Request::segment(2)=='list-product-options' || Request::segment(2)=='add-product-option' || Request::segment(2)=='list-option-values' || Request::segment(2)=='edit-product' ||Request::segment(2)=='edit-manufacturer' || Request::segment(2)=='edit-option-values' )
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif
                        data-target="#submenu-3" aria-controls="submenu-3"><i class="fas fa-fw fa-chart-pie"></i>@lang('admin_lang.product_settings') </a>
                        <div id="submenu-3" class="collapse submenu 
                            @if(Request::segment(2)=='list-category'|| Request::segment(2)=='add-category' || Request::segment(2)=='edit-category'|| 
                            Request::segment(2)=='add-manufacturer' ||  Request::segment(2)=='edit-product-option' || Request::segment(2)=='edit-manufacturer' || Request::segment(2)=='list-manufacturer' ||Request::segment(2)=='manage-product'|| Request::segment(2)=='add-product-step-one'|| Request::segment(2)=='add-product-step-two'|| Request::segment(2)=='add-product-step-three'|| Request::segment(2)=='add-product-step-four' || Request::segment(2)=='list-product-options' || Request::segment(2)=='add-product-option' || Request::segment(2)=='list-option-values' || Request::segment(2)=='edit-product' || Request::segment(2)=='edit-option-values')
                            show
                            @else
                            @endif
                            " style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link manageProduct
                                        @if(Request::segment(2)=='manage-product'|| Request::segment(2)=='add-product-step-one'|| Request::segment(2)=='add-product-step-two'|| Request::segment(2)=='add-product-step-three' || Request::segment(2)=='add-product-step-four' || Request::segment(2)=='edit-product' || Request::segment(2)=='product-discount')
                                        active
                                        @else
                                        @endif
                                        " href="{{ route('manage.product') }}">@lang('admin_lang.manage_product')</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link manageProductCategory 
                                        @if(Request::segment(2)=='list-category' || Request::segment(2)=='edit-category' || Request::segment(2)=='add-category')
                                        active
                                        @else
                                        @endif
                                        " href="{{ route('admin.list.category') }}">@lang('admin_lang.manage_product_category')</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    @endif
                    @if(in_array(3, $permission))
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" @if(Request::segment(2)=='add-customer' || Request::segment(2)=='manage-customer' || Request::segment(2)=='edit-customer' || Request::segment(2)=='view-customer-profile')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif data-target="#submenu-5" aria-controls="submenu-5"><i class="fas fa-fw fa-table"></i>Customers</a>
                        <div id="submenu-5" class="collapse submenu @if( Request::segment(2)=='manage-customer' || Request::segment(2)=='edit-customer' || Request::segment(2)=='view-customer-profile')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='add-customer')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.add.customer') }}">@lang('admin_lang.add_customer')</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='edit-customer' || Request::segment(2)=='manage-customer' || Request::segment(2)=='view-customer-profile')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.list.customer') }}">@lang('admin_lang.manage_customers')</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    @endif
                    <!-- <li class="nav-divider">
                        Features
                        </li> -->
                    @if(in_array(4, $permission))
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" @if(Request::segment(2)=='add-merchant' || Request::segment(2)=='edit-merchant'|| Request::segment(2)=='view-merchant-profile' || Request::segment(2)=='manage-merchant')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif  data-target="#submenu-6" aria-controls="submenu-6"><i class="fas fa-fw fa-file"></i> @lang('admin_lang.merchant') </a>
                        <div id="submenu-6" class="collapse submenu @if(Request::segment(2)=='add-merchant' || Request::segment(2)=='edit-merchant'|| Request::segment(2)=='view-merchant-profile' || Request::segment(2)=='manage-merchant')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='add-merchant')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.add.merchant') }}">@lang('admin_lang.add_nw_merchant')</a>
                                    {{-- <a class="nav-link" href="javascript:;">Add New Merchant</a> --}}
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='manage-merchant' || Request::segment(2)=='edit-merchant' || Request::segment(2)=='view-merchant-profile')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.list.merchant') }}">@lang('admin_lang.mng_merchants')</a>
                                
                                </li>
                                
                            </ul>
                        </div>
                    </li>
                    @endif
                    @if(in_array(5, $permission))
                    @endif
                    @if(in_array(6, $permission) || in_array(13, $permission))
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" @if(Request::segment(2)=='view-order-details' || Request::segment(2)=='manage-orders' || Request::segment(2)== 'add-order')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif data-target="#submenu-8" aria-controls="submenu-8"><i class="fas fa-fw fa-columns"></i>@lang('admin_lang.ords')</a>
                        <div id="submenu-8" class="collapse submenu @if(Request::segment(2)=='view-order-details' || Request::segment(2)=='manage-orders')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                @if(in_array(13, $permission))
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='add-order') active @endif" href="{{ route('admin.add.order') }}">Add External Order</a>
                                </li>
                                @endif
                                
                            </ul>
                        </div>
                    </li>
                    @endif
                    @if(in_array(9, $permission))
                    
                    @endif
                    @if(in_array(7, $permission))
                    <li class="nav-item">
                        <a class="othersTab nav-link" href="#" data-toggle="collapse"  @if(Request::segment(2)=='finance' || Request::segment(2)=='list-request-withdrawl')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif data-target="#submenu-9" aria-controls="submenu-9"><i class="fas fa-database"></i> @lang('admin_lang.fnc')</a>
                        <div id="submenu-9" class="collapse submenu @if(Request::segment(2)=='finance' || Request::segment(2)=='list-request-withdrawl' || Request::segment(2)=='add-invoice' || Request::segment(2)=='manage-invoice' || Request::segment(2)=='edit-invoice' || Request::segment(2)=='view-invoice')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='finance')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.finance') }}">@lang('admin_lang.view_earnings')</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='list-request-withdrawl')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.list.request.withdrawl') }}">@lang('admin_lang.process_withdrawl_reqst')</a>
                                </li>
                                
                            </ul>
                        </div>
                    </li>
                    @endif
                    
                    @if(in_array(11, $permission))
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse"  @if(Request::segment(2)=='add-faq' || Request::segment(2)=='about-page-content')
                        aria-expanded="true"
                        @else
                        aria-expanded="false"
                        @endif data-target="#submenu-11" aria-controls="submenu-11"><i class="fas fa-database"></i>Content</a>
                        <div id="submenu-11" class="collapse submenu @if(Request::segment(2)=='add-faq' || Request::segment(2)=='list-faq' || Request::segment(2)=='about-page-content' || Request::segment(2)=='terms-conditions-page-content' || Request::segment(2)=='contact-page-content' || Request::segment(2)=='faq-category' || Request::segment(2)=='edit-faq' || Request::segment(2)=='privecy-page-content' || Request::segment(2)=='banner-listing' || Request::segment(2)=='add-banner' || Request::segment(2)=='banner-details')
                            show
                            @else
                            @endif" style="">
                            <ul class="nav flex-column">
                                
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='list-faq' || Request::segment(2)=='add-faq' || Request::segment(2)=='edit-faq')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.list.faq') }}">@lang('admin_lang.Faq')</a>
                                </li>

                                
                                
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='about-page-content')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.manage.pageabout') }}">@lang('admin_lang.about_us')</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='contact-page-content')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.manage.pagecontact') }}">@lang('admin_lang.contact_us')</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='banner-listing' || Request::segment(2)=='add-banner' || Request::segment(2)=='banner-details')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.manage.bannerlist') }}">@lang('admin_lang.Banner')</a>
                                </li>

                                
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='terms-conditions-page-content')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.manage.page.privecy',2) }}">Privacy Policy</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if(Request::segment(2)=='terms-conditions-page-content')
                                        active
                                        @else
                                        @endif" href="{{ route('admin.manage.page.termsconditions',1) }}">Terms and conditions</a>
                                </li>
                                
                            </ul>
                        </div>
                    </li>
                    @endif
                </ul>
            </div>
            @endif
        </nav>
    </div>
</div>