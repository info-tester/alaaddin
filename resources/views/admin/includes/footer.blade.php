<!-- footer -->
<!-- ============================================================== -->
 <div class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                {{ date('Y') }} © @lang('admin_lang.Drivers_Group')
            </div>
           
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- end footer -->