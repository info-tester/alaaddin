<!-- Optional JavaScript -->
<script src="{{ asset('public/admin/assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
<!----- slimscroll js--->
<script src="{{ asset('public/admin/assets/vendor/slimscroll/jquery.slimscroll.js') }}"></script>
<!----- sparkline js--->
<script src="{{ asset('public/admin/assets/vendor/charts/sparkline/jquery.sparkline.js') }}"></script>
{{-- <script src="{{ asset('public/admin/assets/vendor/charts/sparkline/spark-js.js') }}"></script> --}}

<!----- morris js--->
<script src="{{ asset('public/admin/assets/vendor/charts/morris-bundle/raphael.min.js') }}"></script>
{{-- <script src="{{ asset('public/admin/assets/vendor/charts/morris-bundle/morris.js') }}"></script> --}}
<!----- main js--->
{{-- <script src="{{ asset('public/admin/assets/libs/js/main-js.js') }}"></script> --}}
<!----- chart chartist js--->
<script src="{{ asset('public/admin/assets/vendor/charts/chartist-bundle/chartist.min.js') }}"></script>
{{-- <script src="{{ asset('public/admin/assets/libs/js/dashboard-ecommerce.js') }}"></script> --}}


<!----- chart c3 js--->
<script src="{{ asset('public/admin/assets/vendor/charts/c3charts/c3.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/charts/c3charts/d3-5.4.0.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/charts/c3charts/C3chartjs.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/jquery.validate.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script>
  window.Laravel = <?php echo json_encode([
  	'csrfToken' => csrf_token(),
  ]); ?>
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".othersTab").click(function(e){
            $(this).toggleClass("showMenu");
            $(".othersTab").not(this).attr("aria-expanded", false);
            $(".othersTab").not(this).next().removeClass("show");
        });

        $(".showMenu").click(function(e){
        	var isShowMenu = $(this).hasClass("showMenu");
            if(!isShowMenu){
            	curObj.attr("aria-expanded",false);
            	var menuName = curObj.attr("data-target");       
            	$(menuName).removeClass("show"); 
            	curObj.addClass("collapsed");  
            	var issExpand = curObj.attr("aria-expanded");
            }
        });

    });
</script>