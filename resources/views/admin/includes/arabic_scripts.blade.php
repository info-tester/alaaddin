<!-- Optional JavaScript -->
<script src="{{ asset('public/admin/arabic/assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('public/admin/arabic/assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
<!----- slimscroll js--->
<script src="{{ asset('public/admin/arabic/assets/vendor/slimscroll/jquery.slimscroll.js') }}"></script>
<!----- sparkline js--->
<script src="{{ asset('public/admin/arabic/assets/vendor/charts/sparkline/jquery.sparkline.js') }}"></script>
{{-- <script src="{{ asset('public/admin/arabic/assets/vendor/charts/sparkline/spark-js.js') }}"></script> --}}
<!----- morris js--->
<script src="{{ asset('public/admin/arabic/assets/vendor/charts/morris-bundle/raphael.min.js') }}"></script>
{{-- <script src="{{ asset('public/admin/arabic/assets/vendor/charts/morris-bundle/morris.js') }}"></script> --}}
<!----- main js--->
{{-- <script src="{{ asset('public/admin/arabic/assets/libs/js/main-js.js') }}"></script> --}}
<!----- chart chartist js--->
<script src="{{ asset('public/admin/arabic/assets/vendor/charts/chartist-bundle/chartist.min.js') }}"></script>
{{-- <script src="{{ asset('public/admin/arabic/assets/libs/js/dashboard-ecommerce.js') }}"></script> --}}

<!----- chart c3 js--->
<script src="{{ asset('public/admin/arabic/assets/vendor/charts/c3charts/c3.min.js') }}"></script>
<script src="{{ asset('public/admin/arabic/assets/vendor/charts/c3charts/d3-5.4.0.min.js') }}"></script>
<script src="{{ asset('public/admin/arabic/assets/vendor/charts/c3charts/C3chartjs.js') }}"></script>
<script src="{{ asset('public/admin/arabic/assets/libs/js/jquery.validate.js') }}"></script>

<script>
  window.Laravel = <?php echo json_encode([
  	'csrfToken' => csrf_token(),
  ]); ?>
</script>