{{-- @extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.password.request') }}" aria-label="{{ __('Reset Password') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 --}}

 @extends('admin.layouts.app')
@section('title', 'Alaaddin | Admin | Reset Password')
@section('content')
@section('links')
@include('admin.includes.links')

@endsection

<div class="splash-container">
    <div class="card ">
        <div class="card-header text-center"><a href="{{ route('admin.login') }}"><img class="logo-img" src="{{ asset('public/admin/assets/images/logo.png') }}" alt="logo"></a><span class="splash-description">Reset Password</span></div>
        <div class="card-body">
            <form method="POST" action="{{route('admin.password.update') }}" id="Reset">
                @csrf
                <div class="message alert" style="display: none; text-align: center;">
    <a href="javascript:void(0);" class="close">&times;</a>
    <span></span>
</div>
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible" style="text-align: center;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach ($errors->all() as $error)
                <li style="list-style: none;">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session()->has('success'))
    <div class="alert alert-success alert-dismissible" style="text-align: center;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('success') }}
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger alert-dismissible" style="text-align: center;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('error') }}
    </div>
@endif
                <input type="hidden" name="id" value="{{@$token}}">
                {{-- <div class="form-group">
                    <input class="form-control form-control-lg required {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" type="text" placeholder="Username" value="{{ old('email') }}" autocomplete="off">
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong style="font-size: 14px;">{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div> --}}
                <div class="form-group">
                    <input class="form-control form-control-lg required {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" type="password" name="password" placeholder="Password">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg required {{ $errors->has('confirm_password') ? ' is-invalid' : '' }}" id="confirm_password" type="password" name="confirm_password" placeholder="Confirm Password">
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Save</button>
            </form>
        </div>
        
    </div>

    @section('scripts')
    @include('admin.includes.scripts')

    <script>
        $(document).ready(function(){
    validator = $('#Reset').validate({
        rules:{
        password: { required: true,minlength:5},
        confirm_password : {equalTo : "#password"}
       },
       submitHandler:function(form){
           form.submit();
        },
    });
});
    </script>
    @endsection
    @endsection
