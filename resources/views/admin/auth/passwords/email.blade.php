{{-- @extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('admin.password.email') }}" aria-label="{{ __('Reset Password') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 --}}
 @extends('admin.layouts.app')
@section('title', 'Alaaddin | Admin | Forgot Password ')
@section('content')
@section('links')
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection

<div class="splash-container">
    <div class="card ">
        <div class="card-header text-center"><a href="{{ route('admin.login') }}"><img class="logo-img" src="{{ asset('public/admin/assets/images/logo.png') }}" alt="logo"></a><span class="splash-description">Please enter your email-id</span></div>
        <div class="card-body">
            <form method="POST" action="{{ route('admin.password.email') }}" aria-label="{{ __('Reset Password') }}" id="emailForm">
                @csrf
                <div class="message alert" style="display: none; text-align: center;">
    <a href="javascript:void(0);" class="close">&times;</a>
    <span></span>
</div>
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible" style="text-align: center;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach ($errors->all() as $error)
                <li style="list-style: none;">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session()->has('success'))
    <div class="alert alert-success alert-dismissible" style="text-align: center;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('success') }}
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger alert-dismissible" style="text-align: center;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('error') }}
    </div>
@endif
                <div class="form-group">
                    <input class="form-control form-control-lg required {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" type="email" placeholder="Email-id" value="{{ old('email') }}" autocomplete="off">
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong style="font-size: 14px;">{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                
                <button type="submit" class="btn btn-primary btn-lg btn-block">Send Reset Link</button>
            </form>
        </div>
    </div>

    @section('scripts')
    @include('admin.includes.scripts')

    <script>
        $(document).ready(function(){ 
           $('#emailForm').validate({
    rules:{
        email: { required: true} 
    },
    submitHandler:function(form){
        
        form.submit();
        },
    });
        });
    </script>
    @endsection
    @endsection
