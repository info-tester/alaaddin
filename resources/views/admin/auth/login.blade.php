@extends('admin.layouts.app')
@section('title', 'Alaaddin | Admin | Login')
@section('content')
@section('links')
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection

<div class="splash-container">
    <div class="card ">
        <div class="card-header text-center"><a href="{{ route('admin.login') }}"><img class="logo-img" src="{{ asset('public/admin/assets/images/logo.png') }}" alt="logo"></a><span class="splash-description">Please enter your user information.</span></div>
        <div class="card-body">
            <form method="POST" action="{{ route('admin.login') }}" aria-label="{{ __('Login') }}" id="loginForm">
                @csrf
                <div class="message alert" style="display: none; text-align: center;">
    <a href="javascript:void(0);" class="close">&times;</a>
    <span></span>
</div>
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible" style="text-align: center;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach ($errors->all() as $error)
                <li style="list-style: none;">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session()->has('success'))
    <div class="alert alert-success alert-dismissible" style="text-align: center;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('success') }}
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger alert-dismissible" style="text-align: center;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('error') }}
    </div>
@endif
                <div class="form-group">
                    <input class="form-control form-control-lg required {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" type="text" placeholder="Username" value="{{ old('email') }}" autocomplete="off">
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong style="font-size: 14px;">{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg required {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" type="password" name="password" placeholder="Password">
                </div>
                <div class="form-group">
                    <label class="custom-control custom-checkbox">
                        <input class="custom-control-input" name="remember" type="checkbox" value="on" id="remember"{{ old('remember') ? 'checked' : '' }}><span class="custom-control-label">Remember Me</span>
                    </label>

                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
            </form>
        </div>
        <div class="card-footer bg-white p-0  ">
            <div class="card-footer-item card-footer-item-bordered">
                <!-- <a href="#" class="footer-link">Create An Account</a></div> -->
                <div class="card-footer-item card-footer-item-bordered">
                    <a href="{{ route('admin.password.request') }}" class="footer-link">Forgot Password ?</a>
                    {{-- <a href="javascript:;" class="footer-link">Forgot Password ?</a> --}}
                </div>
            </div>
        </div>
    </div>

    @section('scripts')
    @include('admin.includes.scripts')

    <script>
        $(document).ready(function(){ 
            $("#loginForm" ).validate({
                errorPlacement: function (error , element) {
                        //toastr:error(error.text());
                    }
                });
        });
    </script>
    @endsection
    @endsection
