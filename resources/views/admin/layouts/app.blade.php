<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <base href="{{URL::asset('public/')}}">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::to('public/admin/assets/images/logo.png')}}">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>
        
        @yield('links')
        
        <style type="text/css" media="screen">
          .error{
            border: solid 1px #fb8282 !important;
            background: #ff000014 !important;
          }  
        </style>
    </head>
    <body>
        <div class="dashboard-main-wrapper">
            @yield('header')
            @yield('sidebar')
            @yield('content')
        </div>
        @yield('scripts')
    </body>
</html>
