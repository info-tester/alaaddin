
        	<div class="left-dash">
            	<div class="user-image-area">
                    @if(@Auth::user()->image)
                		<span><img src="{{url('storage/app/public/customer/profile_pics/'.@Auth::user()->image)}}" alt=""></span>
                	@else
                		<span><img src="{{ asset('public/admin/assets/images/person_icon.png') }}" alt=""></span>
                	@endif
                    <h5>{{ @Auth::user()->fname }} {{ @Auth::user()->lname }}</h5>
                    <p>{{ @Auth::user()->email }} <img src="{{ asset('public/frontend/images/user-tick.png') }}" alt=""></p>
                </div>
                
                <div class="mobile_filter"> <i class="fa fa-filter" aria-hidden="true"></i>
                    <p>@lang('front_static.show_user_menu')</p>
                </div>
                
                <div class="user-menus">
                	<ul>
                    	<li class="{{ Route::is('dashboard') ? 'active' : '' }}"><a href="{{ route('dashboard') }}"><span><i class="fa fa-th" aria-hidden="true"></i></span> <strong>@lang('front_static.dashboard')</strong></a></li>
                        <li class="{{ Route::is('user.edit.profile') ? 'active' : '' }}"><a href="{{ route('user.edit.profile') }}"><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span> <strong>@lang('front_static.edit_profile')</strong></a></li>

                        <li class="<?php if(Request::segment(1)=='order-history' || Request::segment(1)=='order-details'){echo "active";}?>"><a href="{{ route('order.history.list') }}"><span><i class="fa fa-history" aria-hidden="true"></i></span> <strong>@lang('front_static.order_history')</strong></a></li>

                        <li class="{{ Route::is('user.show.address.book','user.add.address.book','user.edit.address.book') ? 'active' : '' }}"><a href="{{ route('user.show.address.book') }}"><span><i class="fa fa-address-book-o" aria-hidden="true"></i></span> <strong>@lang('front_static.address_book')</strong></a></li>

                        <li>
                            <a href="{{ route('order.online.payment.history') }}">
                                <span><i class="fa fa-credit-card-alt" aria-hidden="true"></i></span> <strong>@lang('front_static.payment_history')</strong>
                            </a>
                        </li>

                        <li class="{{ Route::is('rewards.point') ? 'active' : '' }}"><a href="{{ route('rewards.point') }}"><span><i class="fa fa-gift" aria-hidden="true"></i></span> <strong>@lang('front_static.my_reward_points')</strong></a></li>
                        <li class="{{ Route::is('user.wishlist') ? 'active' : '' }}"><a href="{{ route('user.wishlist') }}"><span><i class="fa fa-heart" aria-hidden="true"></i></span> <strong>@lang('front_static.my_wishlist')</strong></a></li>
                    </ul>
                </div>
            </div>