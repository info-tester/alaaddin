{{-- 			
@php
$contact_content = getContent();
@endphp
<section class="available-area">
	<div class="container">
		<h3>@lang('admin_lang.now_available_our_app')</h3>
		@if(getLanguage()->id == 1)
			@if(@$contact_content->app_desc)
			<p>{{@$contact_content->app_desc}}</p>
			@endif
		@else
			@if(@$contact_content->app_desc_arabic)
			<p>{{@$contact_content->app_desc_arabic}}</p>
			@endif
		@endif
		<div class="w-100"></div>
		<a target="_blank" href="{{@$contact_content->android_app_link}}"><img src="{{ asset('public/frontend/images/app1.png') }}" alt=""></a>
		<a target="_blank" href="{{@$contact_content->ios_app_link}}"><img src="{{ asset('public/frontend/images/app2.png') }}" alt=""></a>
	</div>
</section>  

<footer class="main_fotter">
	<div class="top_fotter">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12">
					<div class="fot_box">
						<a class="fot_logo" href="{{ url('/') }}"><img src="{{ asset('public/frontend/images/logo.png')}}" alt=""></a>
						<p></p>
						<a href="{{ route('about.us') }}">@lang('admin_lang.read_more')</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="fot_box fot_box2">
						<h3>@lang('admin_lang.quick_links')</h3>
						<ul>
							<li><a href="{{ url('/') }}">@lang('admin_lang.home')</a></li>
							<li><a href="{{ route('seller.search') }}">@lang('admin_lang.seller')</a></li>
							<li><a href="{{ route('about.us') }}">@lang('admin_lang.about_us')</a></li>
							<li><a href="{{ route('contact.us') }}">@lang('admin_lang.contact_us')</a></li>
							<li><a href="{{ route('terms') }}">@lang('admin_lang.terms_cond')</a></li>
							<li><a href="{{ route('privacy') }}">@lang('admin_lang.privacy_policy')</a></li>
							<li><a href="{{ route('help') }}">@lang('admin_lang.help')</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-2 col-md-6 col-sm-12">
					<div class="fot_box fot_box3">
						<h3>@lang('admin_lang.my_account')</h3>
						<ul>
							
							<li><a href="{{ route('dashboard') }}">@lang('admin_lang.my_account')</a></li>
							<li><a href="{{ route('user.wishlist') }}">@lang('admin_lang.wishlist')</a></li>
							
							<li><a href="{{ route('cart') }}">@lang('admin_lang.shopping_cart')</a></li>							
							<li><a href="{{ route('order.history.list') }}">@lang('admin_lang.my_order_history')</a></li>
							<!-- <li><a href="javascript:void(0);">@lang('admin_lang.track_order')</a></li> -->
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="fot_box need-help">
						<h3>@lang('admin_lang.need_help_q')</h3>
						<ul>
							<li>
								<span><img src="public/frontend/images/fot-icon1.png" alt=""> </span>
								<p><a style="text-decoration: none !important" href="tel:{{@$contact_information->phone}}">{{@$contact_information->phone}}</a></p>
							</li>
							<li>
								<span><img src="public/frontend/images/fot-icon2.png" alt=""> </span> 
								<p><a style="text-decoration: none !important; text-transform: lowercase;" href="mailto:{{@$contact_information->email}}"> {{@$contact_information->email}}</a> </p>
							</li>
							<li>
								<span><i class="fa fa-globe" style="font-size: 21px; color: #373d41;"></i> </span> 
								<p> {{@$contact_information->address}} </p>
							</li>
							<li>
								<span><img src="public/frontend/images/fot-icon3.png" alt=""></span> 
								<p><a style="text-decoration: none !important" href="{{ route('faq') }}">@lang('admin_lang.Faq')</a></p>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="middle_fotter">
				<div class="row">
					<div class="col-md-12">
						<span>@lang('admin_lang.popular_categories')</span>
						<ul>
							@if(@$categories)
							@foreach($categories as $row)
							@if($row->is_popular == 'Y')
							<li><a href="{{ route('search', $row->slug) }}">{{ $row->details[$language->id - 1]->title }}</a> </li>
							@endif
							@endforeach
							@endif
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="below_fotter">
		<div class="container">
			<p>© {{ date('Y') }} {{@$contact_information->email}}  |   @lang('admin_lang.all_rights_reserved').</p>
			<div class="right-payment">
				<div class="fot-social">
					<ul>
						<li>@lang('admin_lang.follow_us_on')</li>
						@if(@$contact_information->facebook_link)
						<li><a href="{{@$contact_information->facebook_link}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						@else
						<li><a href="javascript:void(0);"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						@endif
						@if(@$contact_information->youtube_link)
						<li><a href="{{@$contact_information->youtube_link}}"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
						@else
						<li><a href="javascript:void(0);"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
						@endif
						@if(@$contact_information->pinterest_link)
						<li><a href="{{@$contact_information->pinterest_link}}"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
						@else
						<li><a href="javascript:void(0);"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
						@endif
						@if(@$contact_information->twitter_link)
						<li><a href="{{@$contact_information->twitter_link}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						@else
						<li><a href="javascript:void(0);"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						@endif
						@if(@$contact_information->insta_link)
							<li><a href="{{@$contact_information->insta_link}}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						@else
							<li><a href="javascript:void(0);"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						@endif
					</ul>
				</div>
				<div class="fot-social fot-payment">
					<ul>
						<li>@lang('admin_lang.payment_options')</li>
						<li><a href="javascript:void(0);"><img src="public/frontend/images/pay1.png" alt=""></a></li>
						<li><a href="javascript:void(0);"><img src="public/frontend/images/pay2.png" alt=""></a></li>
						<li><a href="javascript:void(0);"><img src="public/frontend/images/pay3.png" alt=""></a></li>
						<li><a href="javascript:void(0);"><img src="public/frontend/images/pay4.png" alt=""></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>

 --}}