	<!--style-->
	<link href="{{ asset('public/frontend/css/style-'.Config::get('app.locale').'.css') }}" type="text/css" rel="stylesheet" media="all"/>
	<link href="{{ asset('public/frontend/css/responsive-'.Config::get('app.locale').'.css') }}" type="text/css" rel="stylesheet" media="all"/>
	<!--bootstrape-->
	<link href="{{ asset('public/frontend/css/bootstrap-'.Config::get('app.locale').'.min.css') }}" type="text/css" rel="stylesheet" media="all"/>
	<!--font-awesome-->
	<link href="{{ asset('public/frontend/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet" media="all"/>
	<!--fonts-->
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700&display=swap" rel="stylesheet">
	<!-- Owl Stylesheets -->
	<link rel="stylesheet" href="{{ asset('public/frontend/css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('public/frontend/css/owl.theme.default.min.css') }}">
	<style type="text/css">
		#blog-landing{
			box-shadow: 0px 2px 8px 2px rgba(0,0,0,0.1);		
		}
		.white-panel {
			position: absolute;
			background: #fff;
			border-left: 1px solid #e9e9e9;;				
		}

		
		.custom-notification{
			position: fixed;
		    right: 18px;
		    top: 12px;
		    background: #fff;
		    width: 330px;
		    height: 100px;
		    box-shadow: 1px 2px 4px 0px #bfbfbf;
		    cursor: pointer;
		}

		.noti-cross{
			position: absolute;
		    right: 8px;
		    top: 5px;
		    cursor: pointer;
		}
		.noti-img{
			float: left;
		    width: 20%;
		    flex-direction: row;
		    align-items: center;
		    padding: 22px 5px;
		}
		.noti-img img{
			width: 100%;
		}
		.noti-area{
			float: left;
		    width: 80%;
		    padding: 9px 5px;
		}
		.noti-title{
			font-weight: 600;
		}
		.noti-desc{
			    display: block;
		    display: -webkit-box;
		    max-width: 200px;
		    -webkit-line-clamp: 2;
		    -webkit-box-orient: vertical;
		    overflow: hidden;
		    text-overflow: ellipsis;
		}
	</style>


	