			{{--<header class="main-header">
				<div class="only-mobile-show">
					<div class="container">
						<p>{{@$annoucement->title}}</p>
					</div>
				</div>
				<div class="top-header">
					<div class="container">
						<p class="welcome-text">
							{{@$annoucement->title}}
						</p>
						@auth
						<div class="user-area">
							<div class="after_login">
								<a href="javascript:void(0)" class="user_llk">{{ @Auth::user()->fname[0] }}. {{ @Auth::user()->lname }} <i class="fa fa-angle-down" aria-hidden="true"></i></a>
								<div class="show01" style="display: none;">
									<ul>
										<li><a href="{{ route('dashboard') }}">@lang('front_static.dashboard')</a></li>
										<li><a href="{{ route('user.edit.profile') }}">@lang('front_static.edit_profile')</a></li>
										<li><a href="{{ route('order.history.list') }}">@lang('front_static.order_history')</a></li>
										<li><a href="{{ route('user.show.address.book') }}">@lang('front_static.address_book')</a></li>
										<li><a href="{{ route('order.online.payment.history') }}">@lang('front_static.payment_history')</a></li>
										<li><a href="{{ route('rewards.point') }}">@lang('front_static.my_reward_points')</a></li>
										<li><a href="{{ route('user.wishlist') }}">@lang('front_static.my_wishlist')</a></li>
										<li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@lang('front_static.logout')</a></li>               
										<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
											{{ csrf_field() }}
										</form>
									</ul>
								</div>
							</div>
							<div class="language-area">
								@if(Config::get('app.locale') == 'en')
								<span class="paste-lan"><a href="javascript:;" onclick="location.href='{{ route('change.lang') }}'" id="paste-lan">عربي</a></span>
								@elseif(Config::get('app.locale') == 'ar')
								<span class="paste-lan"><a href="javascript:;" onclick="location.href='{{ route('change.lang') }}'" id="paste-lan">English</a></span>
								@endif
						    </div>
			            </div>
			            @else
						<div class="user-area">
			            	<div class="favorite">
			            		<a href="{{ route('login') }}">@lang('front_static.login')</a>
			            	</div>
			            	<div class="favorite">
			            		<a href="{{ route('register') }}">@lang('front_static.signup')</a>
			            	</div>
			            	<div class="favorite">
			            		<a href="{{ route('login') }}">@lang('front_static.my_favorites')</a>
			            	</div>
			            	<div class="language-area">
			            		@if(Config::get('app.locale') == 'en')
			            		<span class="paste-lan"><a href="javascript:;" onclick="location.href='{{ route('change.lang') }}'" id="paste-lan">عربي</a></span>
			            		@elseif(Config::get('app.locale') == 'ar')
			            		<span class="paste-lan"><a href="javascript:;" onclick="location.href='{{ route('change.lang') }}'" id="paste-lan">English</a></span>
			            		@endif
							</div>
						</div>
						@endauth
					</div>
				</div>
				<div class="middle_head">
					<div class="container">
						<span class="logo"><a href="{{ url('/') }}"><img src="public/frontend/images/logo.png" alt=""></a></span>
						<div class="right_search ml-auto">
							<div class="left_search">
								<form action="{{ route('search') }}" autocomplete="off" method="get" id="header_search">
									<input type="text" class="search_type" placeholder="@lang('admin_lang.search_for_products')" name="keyword" id="keyword_header_search" autocomplete="off" value="{{ request()->keyword }}">
									<button type="submit" class="search_submit">@lang('front_static.search')</button>
								</form>
								<div id="keywordList" class="for_auto_complete" style="display: none;"></div>
							</div>
							
							<div class="right_cart">
								<a href="{{ route('cart') }}"><img src="public/frontend/images/cart-bag.png" alt=""> <span id="cart"> @lang('front_static.my') <br> @lang('front_static.cart') ({{ @$cart->total_item?$cart->total_item:0 }})</span> </a> 
							</div>
						</div>
					</div>
				</div>
				@if($categories)
				<div class="menu_area">
					<div class="container">
						<nav class="navbar navbar-expand-lg">
							<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"><img src="public/frontend/images/mobile-menu.png" alt=""></span> </button>
							<div id="navbarNavDropdown" class="navbar-collapse collapse menu">
								<ul class="navbar-nav">
									@if(@$categories)
									@foreach($categories as $row)
									@if($row->add_in_menu == 'Y')
									<li><a href="{{ route('search', $row->slug) }}">{{ $row->details[$language->id - 1]->title }}</a> </li>
									@endif
									@endforeach
									@endif
								</ul>
							</div>
							<li class="alctg"><a id="m1" class="#" href="javascript:void(0);"><img src="public/frontend/images/mobile-menu.png" alt=""> <span>@lang('front_static.all_categories')</span> </a></li>
						</nav>
						<div class="menu-expend">
							<div class="container">
								<div class="cnt all_menu">
									<div id="sm" class="nwsubmnu" style="display:none;">
										<!-- Product Section -->
										<ul id="blog-landing" style="position: relative;">
											@if(@$categories)
											@foreach($categories as $row)
											<li class="white-panel">
												<a class="title" href="{{ route('search', $row->slug) }}"> {{ $row->details[$language->id - 1]->title }}</a>
												<ul>
													@foreach($row->sub_categories as $row1)
													<li><a href="{{ route('search', $row1->slug) }}">{{ $row1->details[$language->id - 1]->title }}</a></li>
													@endforeach
												</ul>
											</li>
											@endforeach
											@endif
										</ul>
										<!-- Product Section -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endif
			</header>--}}