			<section class="available-area">
				<div class="container">
					<h3>@lang('admin_lang.now_available_our_app')</h3>
					@if(getLanguage()->id == 1)
						@if(@$contact_content->app_desc)
						<p>{{@$contact_content->app_desc}}</p>
						@endif
					@else
						@if(@$contact_content->app_desc_arabic)
						<p>{{@$contact_content->app_desc_arabic}}</p>
						@endif
					@endif
					<div class="w-100"></div>
					<a href="{{@$contact_content->android_app_link}}"><img src="public/frontend/images/app1.png" alt=""></a>
					<a href="{{@$contact_content->ios_app_link}}"><img src="public/frontend/images/app2.png" alt=""></a>
				</div>
			</section>