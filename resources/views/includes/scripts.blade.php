		<script type="text/javascript" src="{{ asset('public/frontend/js/jquery-3.2.1.js') }} "></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.js" integrity="sha256-DqUEt+3/iNJv2Kj9c+M7apLvAYr84YxGKmGa6qBa9Kc=" crossorigin="anonymous"></script>
		<script type="text/javascript" src="{{ asset('public/frontend/js/bootstrap.min.js') }}"></script>   
		<!-- Owl javascript -->
		<script src="{{ asset('public/frontend/js/pinterest_grid.js') }} "></script>
		<script src="{{ asset('public/frontend/js/owl.carousel.js') }}"></script>  
		<script src="{{ asset('public/frontend/js/jquery.validate.js') }}"></script>   
		<script>
			$(document).ready(function(){
				$(".user_llk").click(function(){
					$(".show01").slideToggle();
				});
			});
		</script>  
		<script>
			$(document).ready(function() {
				$("#owl-demo-1").owlCarousel({
					margin: 20,
					nav: true,
					loop: true,
					responsive: {
						0: {
							items: 1
						},
						400: {
							items:2
						},
						768: {
							items:3
						},
						992: {
							items:5
						},
						1200: {
							items:6
						},
					}
				});
				
				$("#owl-demo-2").owlCarousel({
					margin:20,
					nav: true,
					loop: true,
					responsive: {
						0: {
							items: 1
						},
						479: {
							items: 1
						},
						480: {
							items:2
						},
						768: {
							items:3
						},
						992: {
							items:4
						}
					}
				});
				
				$("#owl-demo-3").owlCarousel({
					margin:0,
					nav: true,
					loop: true,
					autoplay:true,
					stagePadding:80,
					autoplayTimeout:5000,
					autoplayHoverPause:false,
					responsive: {
						0: {
							items: 1
						},
						400: {
							items:2
						},
						576: {
							items:3
						},
						768: {
							items:4
						},
						992: {
							items:5
						},
						1200: {
							items:7
						},
					}
				});
				
				$("#owl-demo-4").owlCarousel({
					margin:20,
					nav: true,
					loop: true,
					responsive: {
						0: {
							items: 1
						},
						479: {
							items: 1
						},
						480: {
							items:1
						},
						768: {
							items:2
						},
						992: {
							items:2
						}
					}
				});
			})
			
		</script> 
		<script type="text/javascript">
			$(document).ready(function(){
				/* Menu Start */
				$('#sm').mouseover(function(){
					$(this).show();
				});
				$('#sm').mouseout(function(){
					$(this).hide();
				});
				
				$('#m1').mouseover(function(){
					$('.all_menu').show();
					$('#sm').show();
					
				});
				var column = 4
				var wWidth = $(window).width()
				if(wWidth >= 767 && wWidth <= 991){
					column = 3
				} else if(wWidth >= 575 && wWidth < 767) {
					column = 2
				}
				$('#blog-landing').pinterest_grid({
					no_columns: column,
					padding_x: 0,
					padding_y: 0,
					margin_bottom: 0,
					single_column_breakpoint: 700
				});
				
				$('#m1, #m2 ').mouseleave(function(){
					$('#sm').hide();
					$('#sm1, #sm2').hide();
				});
				/* Menu Start */
				
				$('#paste-lan').click(function(){
					$('.open-lan').toggle();
					/*var lang = '';// $('.open-lan').children('li').children('a').data('id');
					//alert(lang);
					if(lang == 'english') {
						$('#paste-lan').html('<img src="images/english-flag1.png" alt="">')
						$('.open-lan').css('display', 'none');
					} else {
						if(lang == 'arabic') {
							$('#paste-lan').html('src="images/arabic-flag1.png"')
							$('.open-lan').css('display', 'none');
						} else {
							$('.open-lan').css('display', 'none');
						}
					}*/
				});
				$('.open-lan').find('a').click(function() {
					var lang = $(this).data("id");
					if(lang == 'english') {
						$('#paste-lan').html('<img src="images/english-flag1.png" alt="">');
					} else {
						$('#paste-lan').html('<img src="images/arabic-flag1.png" alt="">');
					}
					$('.open-lan').hide();
				});
			});
		</script>

		
		<script>
			$(document).ready(function(){
				$(".mobile_filter").click(function(){
					$(".user-menus").slideToggle();
				});
				
			});
		</script>


		{{-- city autocomplete --}}
		{{-- <style type="text/css">
			#keywordList{
				position: absolute;
			    top: 66px;
			    background: #fff;
			    width: 63%;
			    /*height: 200px;*/
			    z-index: 9;
			}
			#keywordList ul li{
				
			}
		</style> --}}
		<script>
			$(document).ready(function(){
				$('#header_search').submit(function(e){
					if($('#keyword_header_search').val() != '') {
						return true;
					}
					e.preventDefault();
					return false;
				})
				$('#keyword_header_search').keyup(function(e){ 
					var keyword = $(this).val();
					if(keyword != '')
					{
						var _token = $('input[name="_token"]').val();
						var reqData = {
							_token: '{{ @csrf_token() }}',
							params: {
								keyword: keyword
							}
						}
						$.ajax({
							url:"{{ route('autocomplete.keyword') }}",
							method: "POST",
							data: reqData,
							success:function(response){
								if(response.error) {
									console.log(response.error.message);
								} else {
									if(response.result.parent_categories.length > 0 || response.result.sub_categories.length > 0 || response.result.products.length > 0) {
										var searchHtml = '<ul>';
										response.result.parent_categories.forEach(function(item, index) {
											searchHtml += '<li><a href="search/'+item.slug+'"><img style="margin-right:15px" src="storage/app/public/category_pics/'+item.picture+'"> '+item.category_details.title+'</a></li>';
											if(item.sub_categories.length > 0) {
												item.sub_categories.forEach(function(item2, index) {
													searchHtml += '<li><a href="search/'+item2.slug+'"><img style="margin-right:15px" src="storage/app/public/category_pics/'+item.picture+'"> '+item2.category_details.title+' in <b>'+item.category_details.title+'</b></a></li>';
												});
											}
										});
										response.result.sub_categories.forEach(function(item, index) {
											searchHtml += '<li><a href="search/'+item.slug+'">'+item.category_details.title+' in <b>'+item.parent_cat_details.title+'</b></a></li>';
										});

										// products
										response.result.products.forEach(function(item, index) {
											searchHtml += '<li><a href="product/'+item.slug+'"><i style="margin-right:15px" class="fa fa-search"></i> '+item.product_by_language.title+' in <b>'+item.product_sub_category.category_by_language.title+'</b></a></li>';
										});
										searchHtml += '</ul>';
										$('#keywordList').html(searchHtml);
										$('#keywordList').show();  
									} else {
										$('#keywordList').hide();
									}
								}
							}
						});
					} else {
						$('#keywordList').hide();
					}
				});

				$('body').on('click', 'li', function(){  
					$('#keyword_search').val($(this).text());  
					$('#keywordList').hide();
				});  
				$("body").click(function(){
					$("#keywordList").hide();
				});

			});
		</script>

<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js"></script>

<!-- stackoverflow solution -->
<script src='https://cdn.firebase.com/js/client/2.2.1/firebase.js'></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-messaging.js"></script>
<script src="{{ asset('public/js/product-firebase-notification.js') }}"></script>