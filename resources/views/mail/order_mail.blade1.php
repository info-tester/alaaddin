<!DOCTYPE html>
<html>
<head>
    <title>Mail</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style type="text/css">
        .white-panel {
            position: absolute;
            background: #fff;
            border-left: 1px solid #e9e9e9;                
        }
        .shoping-cart-body {
            padding: 30px 0 60px 0;
        }
        .container {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }
        .bed-cumb {
            float: left;
            width: 100%;
        }
        *, ::after, ::before {
            box-sizing: border-box;
        }
        * {
            margin: 0;
            padding: 0;
        }
        div {
            display: block;
        }
        .main-dash {
            float: left;
            width: 100%;
            margin-top: 20px;
        }
        .pls-rev {
            float: left;
            width: 100%;
            color: #000;
            font-size: 18px;
            font-weight: 500;
            font-family: 'Roboto', sans-serif;
            margin-bottom: 13px !important;
        }
        .cart-tabel {
            float: left;
            width: 100%;
            border: 1px solid #dedede;
        }
        .address-bbox {
            float: left;
            width: 49%;
            border: 1px solid #dcdcdc;
            border-radius: 1px;
            margin-top: 35px;
            margin-right: 2%;
        }

        .payment-method {
            float: left;
            width: 100%;
            border: 1px solid #dcdcdc;
            padding: 20px;
            margin-top: 35px;
            border-radius: 1px;
        }
        .chk-page {
            float: left;
            width: 100%;
            text-align: right;
            padding: 20px 0px;
            border-bottom: 1px solid #dcdcdc;
        }
        .check-out-bttns {
            float: right;
            width: auto;
            margin-bottom: 15px;
        }
        .bed-cumb .breadcrumb {
            background: none;
            padding: 0px;
            margin: 0px;
        }
        .breadcrumb {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            padding: .75rem 1rem;
            margin-bottom: 1rem;
            list-style: none;
            background-color: #e9ecef;
            border-radius: .25rem;
        }
        dl, ol, ul {
            margin-top: 0;
            margin-bottom: 1rem;
        }
        .bed-cumb .breadcrumb-item {
            color: #626567;
            font-weight: normal;
            font-size: 15px;
            font-family: 'Roboto', sans-serif;
            padding-left: 0px !important;
        }
        .bed-cumb .breadcrumb-item a {
            color: #626567;
        }
        a {
            color: #007bff;
            text-decoration: none;
            background-color: transparent;
            -webkit-text-decoration-skip: objects;
        }
        .table-responsive {
            display: block;
            width: 100%;
            overflow-x: auto;
            -webkit-overflow-scrolling: touch;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }
        .table {
            width: 100%;
            margin-bottom: 1rem;
            background-color: transparent;
        }
        .table {
            display: table;
            margin-bottom: 0px !important;
            max-width: 100%;
            width: 100% !important;
            box-shadow: 0px 1px 2px #dcdcdc;
        }
        .table .one_row1 {
            display: table-row;
        }
        .table .one_row1 {
            display: table-row;
        }
        .cell1 {
            display: table-cell;
            padding: 12px 15px;
            position: relative;
            vertical-align: top;
            white-space: nowrap;
            border-bottom: 1px solid #dedede;
        }
        .tab_head_sheet {
            background: #fff;
            border: medium none;
            color: #7f8080;
            font-size: 14px;
            font-family: 'Roboto', sans-serif;
            line-height: 20px;
            padding-left: 20px;
            text-align: left;
            font-weight: 400;
            text-transform: capitalize;
        }
        .tab_head_sheet_1 {
            border: medium none;
            height: 50px;
            padding: 10px 15px;
            text-align: left;
            white-space: normal;
            overflow-wrap: break-word;
            color: #454545;
            font-family: 'Roboto', sans-serif;
            border-bottom: 1px solid #dedede;
        }

        .cell1 {
            display: table-cell;
            padding: 12px 15px;
            position: relative;
            vertical-align: top;
            white-space: nowrap;
            border-bottom: 1px solid #dedede;
        }
        .check-out-sucs .left-point {
            width: 60%;
        }
        .left-point {
            float: left;
            width: 35%;
            padding: 15px;
        }
        .check-out-sucs .right-totals {
            width: 40%;
        }
        .right-totals {
            float: right;
            width: 60%;
            padding-right: 35px;
        }
        .totals {
            float: right;
            width: auto;
            margin-bottom: 5px;
        }
        .totals p {
            color: #000;
            font-size: 14px;
            font-weight: 400;
            font-family: 'Roboto', sans-serif;
            text-transform: uppercase;
        }
        p {
            margin-top: 0;
            margin-bottom: 1rem;
        }
        p {
            font-family: 'Open Sans', sans-serif;
            font-size: 12px;
            color: #454545;
            margin: 0 !important;
            padding: 0;
        }
        .borders {
            float: left;
            width: 100%;
            height: 1px;
            background: #d1d1d1;
            margin: 5px 0;
        }
        .totals h4 {
            color: #000;
            font-size: 22px;
            font-weight: 500;
            font-family: 'Roboto', sans-serif;
            text-transform: uppercase;
            margin: 0px;
        }
        .head-adrs-box {
            float: left;
            width: 100%;
            padding: 15px;
            border-bottom: 1px solid #dcdcdc;
        }
        .address-dtls {
            float: left;
            width: 100%;
        }
        .head-adrs-box h4 {
            color: #000;
            font-size: 23px;
            font-weight: 500;
            font-family: 'Roboto', sans-serif;
            margin: 0px;
            float: left;
            width: auto;
        }
        .head-adrs-box a {
            color: #fff;
            font-size: 13px;
            font-weight: 400;
            background: #ef1600;
            font-family: 'Poppins', sans-serif;
            float: right;
            padding: 6px 10px 4px 10px;
        }
        a {
            color: #007bff;
            text-decoration: none;
            background-color: transparent;
            -webkit-text-decoration-skip: objects;
        }
        .address-dtls p {
            color: #54585a;
            font-size: 14px;
            font-weight: 400;
            font-family: 'Roboto', sans-serif;
            line-height: 28px;
            padding: 0px 20px;
        }
        .head-adrs-box {
            float: left;
            width: 100%;
            padding: 15px;
            border-bottom: 1px solid #dcdcdc;
        }
        .address-dtls {
            float: left;
            width: 100%;
        }
        .payment-method h4 {
            color: #000;
            font-size: 21px;
            font-weight: 500;
            font-family: 'Roboto', sans-serif;
            margin: 0px;
        }
        .check-out-bttns a {
            color: #fff;
            font-size: 16px;
            font-weight: 500;
            background: #0771d4;    
            font-family: 'Poppins', sans-serif;
            padding: 8px 10px;
            display: inline-block;
            margin-left: 9px;
            border-radius: 2px;
        }
        .proc {
            background: #ef1600 !important;
        }
    </style>
</head>
<body>
    <div style="max-width:640px; margin:0 auto;">
        <div style="/*width:620px;*/background:#F5F5F3; /*padding: 0px 10px;*/ border:1px solid #dcd7d7;">
            <div style="float: none; text-align: center; margin-top: 0px; background:url('{{ URL::to('#') }}') repeat center center">
             <img src="{{ URL::to('public/merchant/assets/images/logo.png') }}" width="240" alt="">
         </div>
     </div>
     <div style="max-width:620px; border:1px solid #f0f0f0; margin:0 0; padding:15px; ">
        <h1 style="font-family:Arial; font-size:16px; font-weight:500; /*color:#8ccd56;*/ margin:5px 0 12px 0;">Dear {{@$data['fname']}} ,</h1>





        <section class="search-body shoping-cart-body" style="padding: 30px 0 60px 0;">
            <div class="container" style="width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;">

              <div class="main-dash">
                <h4 class="pls-rev">Order Details</h4>
                <div class="cart-tabel">
                    <div class="table-responsive">
                        <div class="table">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet">Product details</div>
                                <div class="cell1 tab_head_sheet">Seller</div>
                                <div class="cell1 tab_head_sheet">Quantity</div>
                                <div class="cell1 tab_head_sheet">Unit Price </div>
                                <div class="cell1 tab_head_sheet">Total</div>
                            </div>                            
                            @foreach(@$data['orderDetails'] as $od)                          
                            <!--row 1-->                        
                            <div class="one_row1 small_screen31">
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">Product details</span>
                                    <span class="add_ttrr">
                                        @if(@$od->defaultImage->image)
                                        <span class="tabel-image"><img src="{{url('storage/app/public/products/80/'.@$od->defaultImage->image)}}" alt=""></span>
                                        @else
                                        <span class="tabel-image"><img src="{{ asset('public/frontend/images/tabel-image1.png') }}" alt=""></span>
                                        @endif
                                        <h5>{{ $od->productByLanguage->title }}</h5>

                                        @if($od->variants != "null")
                                        @foreach(json_decode($od->variants) as $row)
                                        <h6>{{ $row->{$data['language_id']}->variant }} : {{ $row->{$data['language_id']}->variant_value }}</h6>
                                        @endforeach
                                        @else
                                        N/A
                                        @endif
                                            {{-- 
                                            <h6>Color : Red</h6>
                                            <h6>Size : XL</h6>
                                            --}}
                                        </span>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">Seller</span>
                                        <span class="add_ttrr">
                                            @if(@$od->sellerDetails->image)
                                            <span class="seller-logo"><img src="{{url('storage/app/public/profile_pics/'.@$od->sellerDetails->image)}}" alt=""></span>
                                            @else
                                            <span class="seller-logo"><img src="images/seller-image1.png" alt=""></span>
                                            @endif
                                            <h4>{{ @$od->sellerDetails->company_name }}</h4>
                                        </span>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">Quantity</span>
                                        <p class="add_ttrr">{{ $od->quantity }}</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">Unit Price </span>
                                        <p class="add_ttrr">{{ number_format($od->original_price, 3) }} {{ getCurrency() }}</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">Total</span>
                                        <p class="add_ttrr">{{ number_format($od->sub_total, 3) }} {{ getCurrency() }}</p>
                                    </div>
                                </div>
                                <!--row 1-->
                                @endforeach 


                            </div>
                        </div>

                        <div class="cart-total-area check-out-sucs">

                            <div>
                                <div class="totals" style="padding: 10px; float: none;">

                                    <p>Subtotal <span>{{ number_format($data['order']->subtotal, 3) }} {{ getCurrency() }}</span></p>
                                    
                                    <p>SHIPPING CHARGES <span>{{ number_format($data['order']->shipping_price, 3) }} {{ getCurrency() }}</span></p>
                                    <p>Total DISCOUNT <span> - {{ number_format($data['order']->total_discount, 3) }} {{ getCurrency() }}</span></p>
                                    {{-- 
                                    <p>loyalty point DISCOUNT <span>$5.00</span></p>
                                    --}}
                                    <div class="borders"></div>
                                    <h4>TOTAL <span>{{ number_format($data['order']->order_total, 3) }} {{ getCurrency() }}</span></h4>
                                    <div class="borders"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                    @if($data['order']->shipping_address_id)

                    <div class="address-bbox">
                        <div class="head-adrs-box">
                            <h4>Shipping Information</h4>
                        </div>
                        <div class="address-dtls">
                            <p><span>Name</span> {{ $data['order']->shippingAddress->shipping_fname }} {{ $data['order']->shippingAddress->shipping_lname }}</p>
                            <p><span>Email Address</span>{{ $data['order']->shippingAddress->email }}</p>
                            <p><span>Phone Number</span> {{ $data['order']->shippingAddress->phone }}</p>
                            <p><span>Country</span> {{ $data['order']->shippingAddress->getCountry->country_name }}</p>
                            <p><span>State</span> {{ $data['order']->shippingAddress->state }}</p>
                            <p><span>City</span>{{ $data['order']->shippingAddress->city }}</p>
                            <p><span>Post Code</span> {{ $data['order']->shippingAddress->zipcode }}</p>
                            <p><span>Full Address</span> {{ $data['order']->shippingAddress->city }}, {{ $data['order']->shippingAddress->state }} - {{ $data['order']->shippingAddress->zipcode }}, {{ $data['order']->shippingAddress->getCountry->country_name }}</p>

                        </div>
                    </div>

                    @else

                    <div class="address-bbox">
                        <div class="head-adrs-box">
                            <h4>Shipping Information</h4>
                        </div>
                        @dd($data['order']->shipping_fname)
                        <div class="address-dtls">
                            <p><span>Name</span> {{ $data['order']->shipping_fname }} {{ $order->shipping_lname }}</p>
                            <p><span>Email Address</span>{{ $data['order']->shipping_email }}</p>
                            <p><span>Phone Number</span> {{ $data['order']->shipping_phone }}</p>
                            <p><span>Country</span> {{ $data['order']->getCountry->country_name }}</p>
                            <p><span>State</span> {{ $data['order']->shipping_state }}</p>
                            <p><span>City</span>{{ $data['order']->shipping_city }}</p>
                            <p><span>Post Code</span> {{ $data['order']->shipping_zipcode }}</p>
                            <p><span>Full Address</span> {{ $data['order']->shipping_city }}, {{ $data['order']->shipping_state }} - {{ $data['order']->shipping_zipcode }}, {{ $data['order']->getCountry->country_name }}</p>

                        </div>
                    </div>

                    @endif

                    @if($data['order']->billingAddress)

                    <div class="address-bbox" style="margin-right:0px;">
                        <div class="head-adrs-box">
                            <h4>Billing Address</h4>
                        </div>
                        <div class="address-dtls">
                            <p><span>Name</span> {{ $data['order']->billingAddress->shipping_fname }} {{ $data['order']->billingAddress->shipping_lname }}</p>
                            <p><span>Email Address</span>{{ $data['order']->billingAddress->email }}</p>
                            <p><span>Phone Number</span> {{ $data['order']->billingAddress->phone }}</p>
                            <p><span>Country</span> {{ $data['order']->billingAddress->getCountry->country_name }}</p>
                            <p><span>State</span> {{ $data['order']->billingAddress->state }}</p>
                            <p><span>City</span>{{ $data['order']->billingAddress->city }}</p>
                            <p><span>Post Code</span> {{ $data['order']->billingAddress->zipcode }}</p>
                            <p><span>Full Address</span> {{ $data['order']->billingAddress->city }}, {{ $data['order']->billingAddress->state }} - {{ $data['order']->billingAddress->zipcode }}, {{ $data['order']->billingAddress->getCountry->country_name }}</p>

                        </div>
                    </div>

                    @else

                    <div class="address-bbox" style="margin-right:0px;">
                        <div class="head-adrs-box">
                            <h4>Billing Address</h4>
                        </div>
                        <div class="address-dtls">
                            <p><span>Name</span> {{ $data['order']->billing_fname }} {{ $data['order']->billing_lname }}</p>
                            <p><span>Email Address</span>{{ $data['order']->billing_email }}</p>
                            <p><span>Phone Number</span> {{ $data['order']->billing_phone }}</p>
                            <p><span>Country</span> {{ $data['order']->getCountry->country_name }}</p>
                            <p><span>State</span> {{ $data['order']->billing_state }}</p>
                            <p><span>City</span>{{ $data['order']->billing_city }}</p>
                            <p><span>Post Code</span> {{ $data['order']->billing_zipcode }}</p>
                            <p><span>Full Address</span> {{ $data['order']->billing_city }}, {{ $data['order']->billing_state }} - {{ $data['order']->billing_zipcode }}, {{ $data['order']->getCountry->country_name }}</p>

                        </div>
                    </div>

                    @endif

                    <div class="payment-method">
                        <h4>Payment method : <span>@if($data['order']->payment_method=='C') Cash on delivery @else Online @endif</span></h4>
                    </div>


                </div>
            </div>
        </section>






        <p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Thank you,</p>
        <p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Team Aswagna (for all emails)</p>

    </div>
</div>
</body>
</html>
@dd()
<tr>
          <td colspan="10" style="padding:7px 10px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:right; border-bottom:1px solid #ddd; background:#f4f4f4;">
            <p style="margin:0 0 5px;">Sub Total  <strong> {{ number_format($data['order']->subtotal, 3) }} {{ getCurrency() }}</strong></p>
            @if($data['order']->coupon_discount!=0.00)
            <p style="margin:0 0 5px;">Discount -<strong> {{ number_format($data['order']->total_discount, 3) }} {{ getCurrency() }}</strong></p>
            {{-- @endif() --}}
            <p style="margin:0 0 5px;">Total <strong> {{ number_format($data['order']->order_total, 3) }} {{ getCurrency() }} </strong></p>
        </td>
    </tr>

    
    public function placeOrder($orderNo) {  
        $order['status']    =   'I';    
        $order = $this->order->where(['order_no' => $orderNo])->update($order);

        if(@Auth::user()) {
            $cart = $this->cart->where('user_id', Auth::user()->id)->first();
            // $this->cartDetails->where('cart_master_id', $cart->id)->delete(); 
            // $this->cart->where('id', $cart->id)->delete();
        } else {
            $cart = $this->cart->where('session_id', session()->getId())->first();
            // $this->cartDetails->where('cart_master_id', $cart->id)->delete(); 
            // $this->cart->where('id', $cart->id)->delete();
        }


        $data = [];
        $data['order'] = $order = $this->order->where('order_no', $orderNo)
        ->with('shippingAddress.getCountry','billingAddress.getCountry','getCountry')->first();
        // to get the products informationa from order_details
        $data['orderDetails'] = $this->orderDetails->where(['order_master_id' => $order->id])
        ->with('sellerDetails','defaultImage','productByLanguage','productVariantDetails')
        ->get();


        // dd($data['orderDetails']);
        $contactList = [];
        $i=0;
        if(@Auth::user()) {
            $data['fname'] = @Auth::user()->fname;
            $data['lname'] = @Auth::user()->lname;
            $data['email'] = @Auth::user()->email;
            $contactList[$i] = @Auth::user()->email;
        } else {
            $user = $this->user->whereId($order->user_id)->first();
            // dd($user);   
            $data['fname'] = $user->fname;
            $data['lname'] = $user->lname;  
            $data['email'] = $user->email;
            $contactList[$i] = $user->email;    
        }
        $data['orderNo'] = $orderNo;
        $data['order'] = $this->country->get();
        $data['language_id'] = getLanguage()->id;

        // Fill the Bcc email address
        foreach($data['orderDetails'] as $contact){
            $i++;
            $contactList[$i] = $contact->sellerDetails->email;
            $contactListEmail1[$i] = $contact->sellerDetails->email1;

            if($contact->sellerDetails->email2) {
                $contactListEmail2[$i-1] = $contact->sellerDetails->email2;
            }
            if($contact->sellerDetails->email3) {
                $contactListEmail3[$i-1] = $contact->sellerDetails->email3;
            }
        }
        $contactListEmail = array_unique($contactList);
        $contactListEmail1 = array_unique($contactListEmail1);

        $contactList = array_merge($contactListEmail, $contactListEmail1);

        if(@$contactListEmail2 != '') {
            $contactListEmail2 = array_unique($contactListEmail2);
            $contactList = array_merge($contactList, $contactListEmail2);
        }
        if(@$contactListEmail3 != '') {
            $contactListEmail3 = array_unique($contactListEmail3);
            $contactList = array_merge($contactList, $contactListEmail3);
        }
        // dd($contactList);
        $data['bcc'] = $contactList;
        // dd($data['bcc']);

        Session::forget('orderNo');
        // dd($data);
        Mail::send(new OrderMail($data));
        return redirect()->route('order.success');
    }