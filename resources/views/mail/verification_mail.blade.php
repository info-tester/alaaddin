<!DOCTYPE html>
<html>
    <head>
        <title>Mail</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div style="max-width:640px; margin:0 auto;">
            <div style="/*width:620px;*/background:#F5F5F3; /*padding: 0px 10px;*/ border:1px solid #dcd7d7;">
                <div style="float: none; text-align: center; margin-top: 0px; background:url('{{ URL::to('#') }}') repeat center center">
                     <img src="{{ URL::to('public/merchant/assets/images/logo.png') }}" width="100" alt="">
                </div>
            </div>
            <div style="max-width:620px; border:1px solid #f0f0f0; margin:0 0; padding:15px; ">
                <h1 style="font-family:Arial; font-size:16px; font-weight:500; /*color:#8ccd56;*/ margin:5px 0 12px 0;">Dear {{@$user['fname']}} ,</h1>
                <div style="display:block;overflow:hidden; width:100%; margin: 5px 0px 10px 0px;">
                    <p style="font-family:Arial; font-size:14px; font-weight:500; text-align:center; color:#000;padding: 4px; background:#f5f5f5;">
                        @if(@$user['mailBody'] == 'forget_password')
                        Your forget password OTP :
                        @else
                        Your email verification OTP :
                        @endif
                    </p>
                </div>
                <div style="display:block;overflow:hidden; width:100%; text-align:center; margin: 0px 0px 10px 0px;">
                    <a href="javascript:void(0);" style="font-family:Arial; border-radius:17px;font-size:15px; font-weight:500; color:#FFF; display:inline-block; padding: 7px 12px; background:#ff0000; text-decoration:none;">{{@$user['email_vcode']}}</a>
                </div>
                
                <div style="display:block; overflow:hidden; width:100%;">

                    @if(@$user['mailBody'] == 'registered')
                        <p style="font-family:Arial; font-size:14px; font-weight:500; color:#000;margin: 15px 0px 15px;">
                            You have successfully {{@$user['mailBody']}} your account.To verify your email id.
                        </p>
                    @elseif(@$user['mailBody'] == 'forget_password')
                        <p style="font-family:Arial; font-size:14px; font-weight:500; color:#000;margin: 15px 0px 15px;">
                            You have receiving this email because we received a password reset request for your account!</br>
                            To verify your email id.
                        </p>
                    @else
                        <p style="font-family:Arial; font-size:14px; font-weight:500; color:#000;margin: 15px 0px 15px;">
                            You have successfully {{@$user['mailBody']}} your email address. To verify your email id please provide OTP.
                        </p>
                    @endif
                </div>
                {{-- @if(@$user['mailBody'] != 'forget_password')
                <div style="display:block;overflow:hidden; width:100%; margin: 5px 0px 10px 0px;">
                    <p style="font-family:Arial; font-size:14px; font-weight:500; text-align:center; color:#000;padding: 4px; background:#f5f5f5;">
                        Please click the button below :
                    </p>
                </div>
                <div style="display:block;overflow:hidden; width:100%; text-align:center; margin: 0px 0px 10px 0px;">
                    @if(@$user['customer'] == 'customer')
                        @if(@$user['mailBody'] == 'registered')
                            <a href="{{ url('user/verify/'.$user->email_vcode.'/'.md5($user->id)) }}" style="font-family:Arial; border-radius:17px;font-size:15px; font-weight:500; color:#FFF; display:inline-block; padding: 7px 12px; background:#ff0000; text-decoration:none;">Click here</a>
                        @else
                            <a href="{{ url('user/verify/new/'.$user['email_vcode'].'/'.md5($user['id'])) }}" style="font-family:Arial; border-radius:17px;font-size:15px; font-weight:500; color:#FFF; display:inline-block; padding: 7px 12px; background:#ff0000; text-decoration:none;">Click here</a>
                        @endif
                    @else
                        @if(@$user['mailBody'] == 'registered')
                            <a href="{{ url('merchant/verify/'.$user->email_vcode.'/'.md5($user->id)) }}" style="font-family:Arial; border-radius:17px;font-size:15px; font-weight:500; color:#FFF; display:inline-block; padding: 7px 12px; background:#ff0000; text-decoration:none;">Click here</a>
                        @else
                            <a href="{{ url('merchant/verify/new/'.$user['email_vcode'].'/'.md5($user['id'])) }}" style="font-family:Arial; border-radius:17px;font-size:15px; font-weight:500; color:#FFF; display:inline-block; padding: 7px 12px; background:#ff0000; text-decoration:none;">Click here</a>
                        @endif                           
                    @endif                    
                </div>
                @endif   --}}
                <p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Thank you,</p>
                <p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Team Alaaddin</p>
            
            </div>
        </div>
    </body>
</html>