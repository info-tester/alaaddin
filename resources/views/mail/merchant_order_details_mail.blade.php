<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Order Details</title>
    <style>
      .tabs_tt{
      max-width:650px; margin:0 auto;
      }
      .tabs_header{
      width:100%; display:block; padding:20px 0; background:#000; text-align:center; margin-bottom:10px;
      }
      .name_ttag{
      float:left; padding:0 10px; color:#000; font-family:Arial, Helvetica, sans-serif;}
      .order_d_block{
      width:96.5%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px;
      }
      .order_d_block h3{
      font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;
      }
      .heading{
      font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;
      }
      .order_d_block ul{
      margin:0;
      padding:0;
      }
      .order_d_block ul li{
      list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px
      }
      .half_ddiv{
      width:45.2%; float:left; border:1px solid #ddd; padding:10px;
      }
      .half_ddiv ul{
      margin:0;
      padding:0;}
      .half_ddiv ul li{
      list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px;}
      .table_res{
      max-width:100%;
      overflow:auto;
      }
      .table_res table{
      margin:15px 0 0;
      padding:0;
      width:100%;
      float:left;
      }
      .table_res table th{
      padding:10px 4px; background:#000; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;
      }
      .table_res table td{
      padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;
      }
      .margL20{
      margin-left:15px;
      }
      @media(max-width:590px) {
      .half_ddiv{
      width:100% !important;
      }
      .margL20{
      margin-left:0 !important;
      }
      }
      .color_pallete{
      width: 27px;
      height: 25px;
      border: 1px solid #000;
      display: inline-block;
      margin: 1px 5px 5px 0;
      float: left;
      }
    </style>
  </head>
  <body style="margin:0; padding:0;">
    <div class="tabs_tt" style="max-width:650px; margin:0 auto;">
    <div class="tabs_header" style=" width: 100%; display: block; padding: 30px 0 0px 0;  background: #fff; text-align: center; margin-bottom: 0;">
      <img src="{{ URL::to('public/merchant/assets/images/logo.png') }}" width="80px" />
    </div>
    <div class="name_ttag" style="float:left; padding:0 10px; color:#000; font-family:Arial, Helvetica, sans-serif;">
      <h1 style="margin: 15px 0 10px 0;">
        <p style="font-weight:600; margin: 0px;"><u><b>Order Successful</b></u></p>
      </h1>
      <!-- <h2><p style="font-weight:600;">Your Order:  has been </p></h2> -->
      {{-- <h2 style="margin:0 0 10px 0;">Buyer Name: {{@$data['order']['customerDetails']['fname']}} {{@$data['order']['customerDetails']['lname']}}</h2> --}}
    </div>
    <div class="order_d_block" style="width:96.5%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px;">
      <h3 style="font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;">Order Details</h3>
      <ul style="margin:0; padding:0; width: 45%; float: left;">
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Order Number :{{$data['order']->order_no}}</strong></li>
        
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Order Date :{{ date('jS M Y',strtotime($data['order']->created_at)) }}</strong></li>
        @if(@$data['order']->payment_method=='C')
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Payment Method : COD</strong></li>
        @elseif(@$data['order']->payment_method=='O')
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Payment Method : Online</strong></li>
        @endif
        
        

        @if(@$data['order']->status == 'I')
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Status : Incomplete</strong></li>
        @elseif($data['order']->status == 'N')
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Status : New</strong></li>
        @elseif($data['order']->status == 'INP')
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Status : In Progress</strong></li>
        @elseif($data['order']->status == 'CM')
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Status : Completed</strong></li>
        @elseif($data['order']->status == 'OA')
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Status : Order Accepted</strong></li>
        @elseif($data['order']->status == 'DA') 
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px;   margin-bottom:5px; margin-left: 0px;"><strong>Status : Out for delivery</strong></li>
        @elseif($data['order']->status == 'RP')
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px;   margin-bottom:5px; margin-left: 0px;"><strong>Status : Ready to be picked up</strong></li>
        @elseif($data['order']->status == 'OP')
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px;   margin-bottom:5px; margin-left: 0px;"><strong>Status : Order picked up</strong></li>
        @elseif($data['order']->status == 'OD')                  
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px;   margin-bottom:5px; margin-left: 0px;"><strong>Status : Order delivered</strong></li>
        @elseif($data['order']->status == 'OC')                  
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px;   margin-bottom:5px; margin-left: 0px;"><strong>Status : Order cancelleds</strong></li>
        @endif
      </ul>

      <ul style="margin:0; padding:0; width: 45%; float: right;">
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong> Product Price: {{ @$data['order']['orderMerchants'][0]['subtotal'] - @$data['order']['orderMerchants'][0]['total_discount'] }}
          {{ getCurrency() }}</strong>
        </li>

        
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Seller Commission  : 
            {{ number_format(@$data['order']['orderMerchants'][0]['seller_commission'], 2) }}
            {{ getCurrency() }}</strong>
        </li>
        
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong> Loading Unloading Price  : 
            {{ number_format(@$data['order']['orderMerchants'][0]['loading_price'], 2) }}
            {{ getCurrency() }}</strong>
        </li>
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Admin Commission  : 
            {{ number_format(@$data['order']['orderMerchants'][0]['admin_commission'], 2) }}
            {{ getCurrency() }}</strong>
        </li>
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Insurance Price  : 
            {{ number_format(@$data['order']['orderMerchants'][0]['insurance_price'], 2) }}
            {{ getCurrency() }}</strong>
        </li>
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong> Total   : 
            {{ number_format(@$data['order']['orderMerchants'][0]['payable_amount'], 2) }}
            {{ getCurrency() }}</strong>
        </li>
        
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Net Earning  : 
            {{ number_format(@$data['order']['orderMerchants'][0]['earning'], 2) }}
            {{ getCurrency() }}</strong>
        </li>
      </ul>
    </div>
    <div class="half_ddiv" style="width:45.2%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px; margin-right: 17px;">
      <h3 class="heading" style="font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;">Shipping Details</h3>
      <ul style="margin:0; padding:0;">
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;">{{ $data['order']->shipping_fname }} {{ $data['order']->shipping_lname }}</li>
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Address: 
          {{ @$data['order']->getCountry->name }},
          {{ $data['order']->shipping_state_details->name }},
          {{ $data['order']->shipping_city_details->name }}
          @if($data['order']->shipping_more_address), {{ $data['order']->shipping_more_address }}@endif
          @if($data['order']->shipping_postal_code), {{ $data['order']->shipping_postal_code }}@endif
          </strong>
        </li>
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Email: {{ $data['order']->shipping_email }}</strong></li>
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Phone: {{ $data['order']->shipping_phone }}</strong></li>
      </ul>
    </div>
    <div class="half_ddiv" style="width:45.2%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px;">
      <h3 class="heading" style="font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;">Billing Details</h3>
      <ul style="margin:0; padding:0;">
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;">{{ $data['order']->billing_fname }} {{ $data['order']->billing_lname }}</li>
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Address: 
          {{ @$data['order']->getBillingCountry->name }},
          {{ $data['order']->billing_state_details->name }},
          {{ $data['order']->billing_city_details->name }}
          @if($data['order']->billing_more_address), {{ $data['order']->billing_more_address }}@endif
          @if($data['order']->billing_postal_code), {{ $data['order']->billing_postal_code }}@endif
          </strong>
        </li>
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Email: {{ $data['order']->billing_email }}</strong></li>
        <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Phone: {{ $data['order']->billing_phone }}</strong></li>
      </ul>
    </div>
    {{-- @endif --}}
    <div style="clear:both">
      <div class="table_res" style="max-width:100%; overflow:auto;">
        <table cellpadding="0" cellspacing="0" style="margin:15px 0 0; padding:0; width:100%; float:left;">
          <tr>
            <th style="padding:10px 4px; background:#ee1c25; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">Product</th>
            <th style="padding:10px 4px; background:#ee1c25; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">Product Quantity</th>
            <th style="padding:10px 4px; background:#ee1c25; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">Seller</th>
            <th style="padding:10px 4px; background:#ee1c25; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">Unit price</th>
            <th style="padding:10px 4px; background:#ee1c25; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">Sub total price</th>
            <th style="padding:10px 4px; background:#ee1c25; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">Total </th>
            <th style="padding:10px 4px; background:#ee1c25; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">Status</th>
          </tr>
          @foreach(@$data['order']['orderMasterDetails'] as $od)
          <tr>
            <td style="padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;"  >
              @if(@$od->defaultImage->image)
              <span class="tabel-image"><img src="{{url('storage/app/public/products/80/'.@$od->defaultImage->image)}}" alt="" style="width: 80px; float: left;"></span>
              @else
              <span class="tabel-image"><img src="{{ asset('public/frontend/images/default_product.png') }}" alt="" style="width: 80px; float: left;"></span>
              @endif
              <br>
              <p style="margin: 5px 0; float: left; text-align: left;">{{ $od->productByLanguage->title }}</p>
              <br>
              @if(json_decode($od->variants) != NULL)
              @foreach(json_decode($od->variants) as $row)
              <p style="margin: 3px 0; float: left; text-align: left;">{{ $row->{$data['language_id']}->variant }} : {{ $row->{$data['language_id']}->variant_value }}</p>
              <br>
              @endforeach
              @endif
            </td>
            <td style="padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;"  >
              <p>{{ $od->quantity }}</p>
            </td>
            <td style="padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;"  >
              <p>{{ @$od->sellerDetails->company_name }}</p>
            </td>
            <td style="padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;"  >
              @if(@$od->discounted_price <=0)
              <p>{{ number_format($od->original_price, 2) }} {{ getCurrency() }}</p>
              @else
              <p><span style="text-decoration: line-through;">{{ number_format($od->original_price, 2) }}</span>&nbsp;&nbsp;&nbsp;&nbsp;{{ number_format($od->discounted_price, 2) }} {{ getCurrency() }}</p>
              @endif
            </td>
            <td style="padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;">
              @if(@$od->discounted_price <=0)
              <p>{{ number_format($od->sub_total, 2) }} {{ getCurrency() }}</p>
              @else
              <p><span style="text-decoration: line-through;">{{ number_format($od->sub_total, 2) }}</span> &nbsp;&nbsp;&nbsp;&nbsp;{{ number_format($od->total, 2) }} {{ getCurrency() }}</p>
              @endif
              <!-- <p>{{ number_format($od->sub_total, 2) }} {{ getCurrency() }}</p> -->
            </td>
            <td style="padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;"  >
              <p>{{ number_format($od->total, 2) }} {{ getCurrency() }}</p>
            </td>
            <td style="padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;"  >
              <p>
                @if(@$od->status == 'I')
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong> Incomplete</strong></li>
                @elseif(@$od->status == 'N')
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong> New</strong></li>
                @elseif(@$od->status == 'OA')
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong> Order Accepted</strong></li>
                @elseif(@$od->status == 'DA') 
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px;   margin-bottom:5px; margin-left: 0px;"><strong> Out for delivery</strong></li>
                @elseif(@$od->status == 'RP')
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px;   margin-bottom:5px; margin-left: 0px;"><strong> Ready to be picked up</strong></li>
                @elseif(@$od->status == 'OP')
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px;   margin-bottom:5px; margin-left: 0px;"><strong> Order picked up</strong></li>
                @elseif(@$od->status == 'OD')                  
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px;   margin-bottom:5px; margin-left: 0px;"><strong> Order delivered</strong></li>
                @elseif(@$od->status == 'OC')                  
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px;   margin-bottom:5px; margin-left: 0px;"><strong>Order cancelleds</strong></li>
                @endif
              </p>
            </td>
          </tr>
          @endforeach()
        </table>
      </div>
      <p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 20px 0px 10px 0px;">Thank you,</p>
      <p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Team Alaaddin</p>
    </div>
  </body>
</html>