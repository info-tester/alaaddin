<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Untitled Document</title>
  <style>
    .tabs_tt{
      max-width:650px; margin:0 auto;
    }
    .tabs_header{
      width:100%; display:block; padding:20px 0; background:#000; text-align:center; margin-bottom:10px;
    }
    .name_ttag{
      float:left; padding:0 10px; color:#000; font-family:Arial, Helvetica, sans-serif;}
      .order_d_block{
        width:96.5%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px;
      }
      .order_d_block h3{
        font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;
      }
      .heading{
        font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;
      }
      .order_d_block ul{
        margin:0;
        padding:0;
      }

      .order_d_block ul li{
        list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px
      }
      .half_ddiv{
        width:45.2%; float:left; border:1px solid #ddd; padding:10px;
      }

      .half_ddiv ul{
        margin:0;
        padding:0;}

        .half_ddiv ul li{
          list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px;}
          .table_res{
            max-width:100%;
            overflow:auto;
          }
          .table_res table{
            margin:15px 0 0;
            padding:0;
            width:100%;
            float:left;
          }
          .table_res table th{
            padding:10px 4px; background:#000; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;
          }

          .table_res table td{
            padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;
          }
          .margL20{
            margin-left:15px;
          }
          @media(max-width:590px) {
            .half_ddiv{
              width:100% !important;
            }
            .margL20{
              margin-left:0 !important;
            }
          }
          .color_pallete{
            width: 27px;
            height: 25px;
            border: 1px solid #000;
            display: inline-block;
            margin: 1px 5px 5px 0;
            float: left;
          }
        </style>
      </head>
      <body style="margin:0; padding:0;">
        <div class="tabs_tt" style="max-width:650px; margin:0 auto;">
          <div class="tabs_header" style=" width: 100%; display: block; padding: 30px 0 0px 0;  background: #fff; text-align: center; margin-bottom: 0;">
            <img src="{{ URL::to('public/merchant/assets/images/logo.png') }}" width="150px" />
          </div>

          <div class="name_ttag" style="float:left; padding:0 10px; color:#000; font-family:Arial, Helvetica, sans-serif;">

            <h1 style="margin: 15px 0 10px 0;"><p style="font-weight:600; margin: 0px;"><u><b>Order Successful</b></u></p></h1>
            <!-- <h2><p style="font-weight:600;">Your Order:  has been </p></h2> -->
            <h2 style="margin:0 0 10px 0;">Buyer Name: {{@$data['fname']}} {{@$data['lname']}}</h2>
          </div>
          <div class="order_d_block" style="width:96.5%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px;">
            <h3 style="font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;">Order Details</h3>
            <ul style="margin:0; padding:0; width: 45%; float: left;">
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Order Number :{{$data['order']->order_no}}</strong></li>
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Order Date :{{ date('jS M Y',strtotime($data['order']->created_at)) }}</strong></li>
              @if($data['order']->payment_method=='C')
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Payment Method : COD</strong></li>
              @else()
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Payment Method : Online</strong></li>
              @endif()
            </ul>
            <ul style="margin:0; padding:0; width: 45%; float: right;">
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Total Product Price : {{number_format(@$data['order']['total_product_price'],2)}}
                
                 {{ getCurrency() }}</strong></li>
              }
              }
              {{-- <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Shipping Charges : {{ number_format($data['order']->shipping_price,2) }} {{ getCurrency() }}</strong></li> --}}
              {{-- <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Discount : {{ number_format($data['order']->total_discount, 2) }} {{ getCurrency() }}</strong></li> --}}
              {{-- <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Total : {{number_format(@$data['order_total'],2)}}
                
                 {{ getCurrency() }}</strong></li> --}}
              }
              }
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Admin Commission  : 
            {{ number_format(@$data['order']['admin_commission'], 2) }}
            {{ getCurrency() }}</strong>
        </li>
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Total Seller Commission  : 
            {{ number_format(@$data['order']['total_seller_commission'], 2) }}
            {{ getCurrency() }}</strong>
        </li>
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Insurance Price  : 
                    {{ number_format(@$data['order']['insurance_price'], 2) }}
                    {{ getCurrency() }}</strong>
                </li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Loading Unloading Price  : 
                    {{ number_format(@$data['order']['loading_price'], 2) }}
                    {{ getCurrency() }}</strong>
                </li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Payable Amount  : 
                    {{ number_format(@$data['order']['payable_amount'], 2) }}
                    {{ getCurrency() }}</strong>
                </li>
            </ul>
          </div>
  {{-- @if($data['order']->shipping_address_id)
  <div class="half_ddiv" style="width:45.2%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px; margin-right: 17px;">
    <h3 class="heading" style="font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;">Shipping Details</h3>
    <ul style="margin:0; padding:0;">
      <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;">{{ $data['order']->shippingAddress->shipping_fname }} {{ $data['order']->shippingAddress->shipping_lname }}</li>
      <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Address: {{ $data['order']->shippingAddress->street }},  {{ $data['order']->shippingAddress->avenue }},  {{ $data['order']->shippingAddress->building_no }},  {{ $data['order']->shippingAddress->city }}, {{ $data['order']->shippingAddress->state }} @if($data['order']->shippingAddress->zipcode)- {{ $data['order']->shippingAddress->zipcode }}@endif, {{ @$data['order']->shippingAddress->getCountry->name }}</strong></li>
      <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Email: {{ $data['order']->shippingAddress->email }}</strong></li>
      <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Phone: {{ $data['order']->shippingAddress->phone }}</strong></li>
  </ul>
</div>
@else --}}
<div class="half_ddiv" style="width:45.2%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px; margin-right: 17px;">
  <h3 class="heading" style="font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;">Shipping Details</h3>
  <ul style="margin:0; padding:0;">
    <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;">{{ $data['order']->shipping_fname }} {{ $data['order']->shipping_lname }}</li>
    <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Address: 
     {{ $data['order']->shipping_city }}, {{ $data['order']->shipping_street }},
     @if($data['order']->shipping_block)
     {{ $data['order']->shipping_block }},
     @endif
     @if($data['order']->shipping_building) {{ $data['order']->shipping_building }},
     @endif
     @if($data['order']->shipping_more_address) {{ $data['order']->shipping_more_address }},@endif
     {{ @$data['order']->getCountry->name }}
     @if($data['order']->shipping_postal_code), {{ $data['order']->shipping_postal_code }}@endif

   </strong></li>
   <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Email: {{ $data['order']->shipping_email }}</strong></li>
   <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Phone: {{ $data['order']->shipping_phone }}</strong></li>
 </ul>
</div>
{{-- @endif --}}
{{-- @if($data['order']->billingAddress)
<div class="half_ddiv" style="width:45.2%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px;">
    <h3 class="heading" style="font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;">Billing Details</h3>
    <ul style="margin:0; padding:0;">
      <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;">{{ $data['order']->billingAddress->shipping_fname }} {{ $data['order']->billingAddress->shipping_lname }}</li>
      <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Address: {{ $data['order']->billingAddress->street }},  {{ $data['order']->billingAddress->avenue }},  {{ $data['order']->billingAddress->building_no }},  {{ $data['order']->billingAddress->city }}, {{ $data['order']->billingAddress->state }} @if($data['order']->billingAddress->zipcode)- {{ $data['order']->billingAddress->zipcode }}@endif, {{ @$data['order']->billingAddress->getCountry->name }}</strong></li>
      <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Email: {{ $data['order']->billingAddress->email }}</strong></li>
      <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Phone: {{ $data['order']->billingAddress->phone }}</strong></li>
  </ul>
</div>
@else --}}
<div class="half_ddiv" style="width:45.2%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px;">
  <h3 class="heading" style="font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;">Billing Details</h3>
  <ul style="margin:0; padding:0;">
    <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;">{{ $data['order']->billing_fname }} {{ $data['order']->billing_lname }}</li>
    <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Address: 
      {{ $data['order']->billing_city }}, {{ $data['order']->billing_street }},
      @if($data['order']->billing_block)
      {{ $data['order']->billing_block }},
      @endif
      @if($data['order']->billing_building) {{ $data['order']->billing_building }},
      @endif
      @if($data['order']->billing_more_address) {{ $data['order']->billing_more_address }},@endif
      {{ @$data['order']->getBillingCountry->name }}
      @if($data['order']->billing_postal_code), {{ $data['order']->billing_postal_code }}@endif
    </strong></li>
    <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Email: {{ $data['order']->billing_email }}</strong></li>
    <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Phone: {{ $data['order']->billing_phone }}</strong></li>
  </ul>
</div>
{{-- @endif --}}
<div style="clear:both">
  <div class="table_res" style="max-width:100%; overflow:auto;">
    <table cellpadding="0" cellspacing="0" style="margin:15px 0 0; padding:0; width:100%; float:left;">
      <tr>
        <th style="padding:10px 4px; background:#2486e1; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">Product</th>
        <th style="padding:10px 4px; background:#2486e1; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">Product Quantity</th>
        <th style="padding:10px 4px; background:#2486e1; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">Seller</th>
        <th style="padding:10px 4px; background:#2486e1; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">SUBTOTAL</th>
        <th style="padding:10px 4px; background:#2486e1; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">TOTAL PRICE</th>
      </tr>
      @foreach(@$data['orderDetails'] as $od)
      <tr>
        <td style="padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;"  >
          @if(@$od->defaultImage->image)
          <span class="tabel-image"><img src="{{url('storage/app/public/products/80/'.@$od->defaultImage->image)}}" alt="" style="width: 80px; float: left;"></span>
          @else
          <span class="tabel-image"><img src="{{ asset('public/frontend/images/default_product.png') }}" alt="" style="width: 80px; float: left;"></span>
          @endif
          <br>
          <p style="margin: 5px 0; float: left; text-align: left;">{{ $od->productByLanguage->title }}</p>
          <br>
          @if(json_decode($od->variants) != NULL)
          @foreach(json_decode($od->variants) as $row)
          <p style="margin: 3px 0; float: left; text-align: left;">{{ $row->{$data['language_id']}->variant }} : {{ $row->{$data['language_id']}->variant_value }}</p>
          <br>
          @endforeach
          @endif
        </td>
        <td style="padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;"  >
          <p>{{ $od->quantity }}</p>
        </td>
        <td style="padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;"  >
          <p>{{ @$od->sellerDetails->company_name }}</p>
        </td>
        <td style="padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;"  >
          <p>{{ number_format($od->original_price,2) }} {{ getCurrency() }}</p>
        </td>
        <td style="padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;"  >
          <p> {{ number_format($od->sub_total,2) }} {{ getCurrency() }} </p>
        </td>

      </tr>
      @endforeach()
      
    </table>
  </div>

  <p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 20px 0px 10px 0px;">Thank you,</p>
  <p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Team Alaaddin</p>
</div>
</body>
</html>