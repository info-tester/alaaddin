<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Order Details</title>
        <style>
            .tabs_tt{
            max-width:650px; margin:0 auto;
            }
            .tabs_header{
            width:100%; display:block; padding:20px 0; background:#000; text-align:center; margin-bottom:10px;
            }
            .name_ttag{
            float:left; padding:0 10px; color:#000; font-family:Arial, Helvetica, sans-serif;}
            .order_d_block{
            width:96.5%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px;
            }
            .order_d_block h3{
            font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;
            }
            .heading{
            font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;
            }
            .order_d_block ul{
            margin:0;
            padding:0;
            }
            .order_d_block ul li{
            list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px
            }
            .half_ddiv{
            width:45.2%; float:left; border:1px solid #ddd; padding:10px;
            }
            .half_ddiv ul{
            margin:0;
            padding:0;}
            .half_ddiv ul li{
            list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px;}
            .table_res{
            max-width:100%;
            overflow:auto;
            }
            .table_res table{
            margin:15px 0 0;
            padding:0;
            width:100%;
            float:left;
            }
            .table_res table th{
            padding:10px 4px; background:#000; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;
            }
            .table_res table td{
            padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;
            }
            .margL20{
            margin-left:15px;
            }
            @media(max-width:590px) {
            .half_ddiv{
            width:100% !important;
            }
            .margL20{
            margin-left:0 !important;
            }
            }
            .color_pallete{
            width: 27px;
            height: 25px;
            border: 1px solid #000;
            display: inline-block;
            margin: 1px 5px 5px 0;
            float: left;
            }
        </style>
    </head>
    <body style="margin:0; padding:0;">
        <div class="tabs_tt" style="max-width:650px; margin:0 auto;">
        <div class="tabs_header" style=" width: 100%; display: block; padding: 30px 0 0px 0;  background: #fff; text-align: center; margin-bottom: 0;">
            <img src="{{ URL::to('public/merchant/assets/images/logo.png') }}" width="150px" />
        </div>
        <div class="name_ttag" style="float:left; padding:0 10px; color:#000; font-family:Arial, Helvetica, sans-serif;">
            <h1 style="margin: 15px 0 10px 0;">
                <p style="font-weight:600; margin: 0px;"><u><b>Order Details</b></u></p>
            </h1>
            <h2 style="margin:0 0 10px 0;">Buyer Name: {{ @$orderMasterDetails->shipping_fname }}  {{@$orderMasterDetails->shipping_lname }}</h2>
        </div>
        <div class="order_d_block" style="width:96.5%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px;">
            <h3 style="font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;">Order Details</h3>
            <ul style="margin:0; padding:0; width: 45%; float: left;">
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Order Number :{{ @$orderMasterDetails->order_no }}</strong></li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Order created Date :{{ date('jS M Y',strtotime($orderMasterDetails->created_at)) }}</strong></li>
                @if(@$orderMasterDetails->payment_method=='C')
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Payment Method : COD(Cash on Delivery)</strong></li>
                @else
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Payment Method : Online</strong></li>
                @endif
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Status : 
                    @if(@$orderMasterDetails->status == 'I')
                    Incomplete
                    @elseif(@$orderMasterDetails->status == 'N')
                    New
                    @elseif(@$orderMasterDetails->status == 'OA')
                    Order Accepted
                    @elseif(@$orderMasterDetails->status == 'DA')
                    Driver Assigned
                    @elseif(@$orderMasterDetails->status == 'RP')
                    Ready for Picked up
                    @elseif(@$orderMasterDetails->status == 'OP')
                    Order Picked up
                    @elseif(@$orderMasterDetails->status == 'OD')
                    Order Delivered
                    @elseif(@$orderMasterDetails->status == 'OC')
                    Order Cancelled
                    @endif
                    </strong>
                </li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Driver Name :{{ @$orderMasterDetails->driverDetails->fname }} {{ @$orderMasterDetails->driverDetails->lname }}</strong></li>
            </ul>
            <ul style="margin:0; padding:0; width: 45%; float: right;">
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Product Total Weight : {{ number_format(@$orderMasterDetails->product_total_weight, 3) }} Gms</strong></li>
                
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Sub Total : {{ number_format(@$orderMasterDetails->subtotal, 3) }} {{ getCurrency() }}</strong> </li>
                
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Shipping Charges : {{ number_format(@$orderMasterDetails->shipping_price, 3) }} {{ getCurrency() }}</strong></li>
                
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Total : {{ $orderMasterDetails->order_total }} KWD</strong></li>
                
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Invoice Number : {{ @$orderMasterDetails->invoice_no }} </strong></li>
                
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Invoice Details : {{ @$orderMasterDetails->invoice_details }} </strong></li>
                
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Expected delivery date & time : {{ @$orderMasterDetails->delivery_date }} {{ @$orderMasterDetails->delivery_time }} </strong></li>
            </ul>
        </div>
        <div class="half_ddiv" style="width:45.2%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px; margin-right: 17px;">
            <h3 class="heading" style="font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;">Shipping Details</h3>
            <ul style="margin:0; padding:0;">
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Shipping Name: {{ @$orderMasterDetails->shipping_fname }} {{ $orderMasterDetails->shipping_lname }}</strong></li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Email: {{ @$orderMasterDetails->shipping_email }}</strong></li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Phone: {{ @$orderMasterDetails->shipping_phone }}</strong></li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Country: {{@$orderMasterDetails->shipping_country }} </strong></li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>City: {{ @$orderMasterDetails->shipping_city }}</strong></li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Block: {{ @$orderMasterDetails->shipping_block }}</strong></li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Street: {{ @$orderMasterDetails->shipping_street }}</strong></li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Building: {{ @$orderMasterDetails->shipping_building }}</strong></li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Postal code: {{ @$orderMasterDetails->shipping_zip }}</strong></li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>More address: {{ @$orderMasterDetails->shipping_address }}</strong></li>
                <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Payment method: @if(@$orderMasterDetails->payment_method == 'C') Cash on delivery @elseif(@$orderMasterDetails->payment_method == 'O') Online @else N/A @endif</strong></li>
            </ul>
            
        </div>

        <div style="clear:both">
            @if(@$driver_rating_link)
            <p style="text-align: center; font-weight: 600; margin-top: 10px;">Give a rating to our driver <a href="{{ @$driver_rating_link }}">Click here!</a></p>
            @endif
        </div>
        <div style="clear:both">
            <p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 20px 0px 10px 0px;">Thank you,</p>
            <p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Team Alaaddin</p>
        </div>
    </body>
</html>