<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>New withdrawal request received</title>
  <style>
    .tabs_tt{
      max-width:650px; margin:0 auto;
    }
    .tabs_header{
      width:100%; display:block; padding:20px 0; background:#000; text-align:center; margin-bottom:10px;
    }
    .name_ttag{
      float:left; padding:0 10px; color:#000; font-family:Arial, Helvetica, sans-serif;}
      .order_d_block{
        width:96.5%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px;
      }
      .order_d_block h3{
        font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;
      }
      .heading{
        font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;
      }
      .order_d_block ul{
        margin:0;
        padding:0;
      }

      .order_d_block ul li{
        list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px
      }
      .half_ddiv{
        width:45.2%; float:left; border:1px solid #ddd; padding:10px;
      }

      .half_ddiv ul{
        margin:0;
        padding:0;}

        .half_ddiv ul li{
          list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px;}
          .table_res{
            max-width:100%;
            overflow:auto;
          }
          .table_res table{
            margin:15px 0 0;
            padding:0;
            width:100%;
            float:left;
          }
          .table_res table th{
            padding:10px 4px; background:#000; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;
          }

          .table_res table td{
            padding:7px 4px; color:#323232; font-family:Arial, Helvetica, sans-serif; font-size:13px; text-align:center; border-bottom:1px solid #ddd;
          }
          .margL20{
            margin-left:15px;
          }
          @media(max-width:590px) {
            .half_ddiv{
              width:100% !important;
            }
            .margL20{
              margin-left:0 !important;
            }
          }
          .color_pallete{
            width: 27px;
            height: 25px;
            border: 1px solid #000;
            display: inline-block;
            margin: 1px 5px 5px 0;
            float: left;
          }
        </style>
      </head>
      <body style="margin:0; padding:0;">
        <div class="tabs_tt" style="max-width:650px; margin:0 auto;">
          <div class="tabs_header" style=" width: 100%; display: block; padding: 30px 0 0px 0;  background: #fff; text-align: center; margin-bottom: 0;">
            <img src="{{ URL::to('public/merchant/assets/images/logo.png') }}" width="150px" />
          </div>

          <div class="name_ttag" style="float:left; padding:0 10px; color:#000; font-family:Arial, Helvetica, sans-serif;">

            <h1 style="margin: 15px 0 10px 0;"><p style="font-weight:600; margin: 0px;"><u><b>{{ @$data['merchant_name']}} request for withdrawal</b></u></p></h1>
            <!-- <h2><p style="font-weight:600;">Your Order:  has been </p></h2> -->
            <h2 style="margin:0 0 10px 0;">Requested By: {{ @$data['merchant_name']}}  </h2>
          </div>
          <div class="order_d_block" style="width:96.5%; float:left; border:1px solid #ddd; padding:10px; margin-bottom:10px;">
            <h3 style="font-family:Arial, Helvetica, sans-serif; font-size:19px; font-weight:normal; margin:0 0 5px; padding:0;">withdrawal request details</h3>
            <ul style="margin:0; padding:0; width: 45%; float: left;">
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Requested ID :{{ @$data['request_id'] }}</strong></li>
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Seller email :{{ @$data['merchant_email'] }}</strong></li>
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong> Seller phone number : {{@$data['merchant_phone']}}</strong></li>
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>withdrawal Status : 
              	{{@$data['wstatus']}}
              </strong></li>
            </ul>
            <ul style="margin:0; padding:0; width: 45%; float: right;">
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Seller account number  : {{ @$data['account_no'] }}</strong></li>		
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Seller Account name : {{@$data['account_name']}}</strong></li>
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Seller bank name : {{@$data['bank_name']}}</strong></li>
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>IFSC number : {{@$data['iban_number']}}</strong></li>
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Branch Name : {{@$data['branch_name']}}</strong></li>
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Requested money : {{ number_format(@$data['amount'],2) }} Rs</strong></li>
              <li style="list-style:none; font-family:Arial, Helvetica, sans-serif; font-size:13px; margin-bottom:5px; margin-left: 0px;"><strong>Balance : {{ number_format(@$data['balance'],2) }} Rs </strong></li>
            </ul>
          </div>
          
  
          
<div style="clear:both">
  <div class="table_res" style="max-width:100%; overflow:auto;">
    <!-- <table cellpadding="0" cellspacing="0" style="margin:15px 0 0; padding:0; width:100%; float:left;">
      <tr>
        <th style="padding:10px 4px; background:#2486e1; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">Product</th>
        <th style="padding:10px 4px; background:#2486e1; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">Product Quantity</th>
        <th style="padding:10px 4px; background:#2486e1; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">Seller</th>
        <th style="padding:10px 4px; background:#2486e1; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">SUBTOTAL</th>
        <th style="padding:10px 4px; background:#2486e1; color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:12px; text-align:center; text-transform:uppercase;">TOTAL PRICE</th>
      </tr>
      
    </table> -->
  </div>

  <p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 20px 0px 10px 0px;">Thank you,</p>
  <p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Team Alaaddin</p>
</div>
</body>
</html>