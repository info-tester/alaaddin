<!DOCTYPE html>
<html>
	<head>
		<title>Mail</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body style="background:#FFF;">
		<div style="width:640px; margin:0 auto;">
			<div style="width:620px; min-height:101px; background:#F5F5F3; padding: 0px 10px; border:1px solid #999;">
				<div style="float: none; text-align: center;">
					<img src="{{ URL::to('public/merchant/assets/images/logo.png') }}" width="120" alt="">
				</div>
			</div>
			<div style="width:620px; margin:0 auto; min-height:20px; padding:0 10px; margin:15px 0; background:#f2f2f2; border:1px solid #999;">
				<p style="font-family:Arial; text-align:center; margin:9px 0; color:#000;">Thank You for connecting with Alaaddin</p>
			</div>
			<div style="width:620px; border:1px solid #000; margin: 15px 0; padding:10px; ">
				<h1 style="font-family:Arial; font-size:16px; font-weight:500; color:#1fae66; margin:5px 0 12px 0;;">Customer Details</h1>
				<div style="display:block;overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;width:77px;">Name :</p>
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;">{{ @$customer->name }}</p>
				</div>
				<div style="display:block;overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;width:77px;">Email-id :</p>
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;">{{ @$customer->email }}</p>
				</div>
				<div style="display:block;overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;width:77px;">Phone :</p>
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;">{{ @$customer->phone }}</p>
				</div>
				<div style="display:block;overflow:hidden; width:100%;">
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;width:77px;">Password :</p>
					<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839; float:left;margin: 12px 0 0 0;">{{ @$customer->password }}</p>
				</div>
				<div style="display:block;overflow:hidden; width:100%; text-align:center; margin: 0px 0px 10px 0px;">
					<a href="{{ route('login') }}" style="font-family:Arial; border-radius:17px;font-size:15px; font-weight:500; color:#FFF; display:inline-block; padding: 7px 12px; background:#ff0000; text-decoration:none;">Login</a>                       
				</div>
				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Thank you</p>
				<p style="font-family:Arial; font-size:14px; font-weight:500; color:#363839;margin: 0px 0px 10px 0px;">Team Alaaddin</p>
			</div>
		</div>
	</body>
</html>