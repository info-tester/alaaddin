<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{ url('') }}/">
  
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('public/frontend/images/logo.png') }}">
    <link rel="manifest" href="public/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="public/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5e79a6f6da97820012e00be5&product=inline-share-buttons&cms=sop' async='async'></script>
    <title>@yield('title')</title>
    @yield('meta')

   {{-- @if(app()->getLocale() == 'en')
    <meta name="Description" content="Aswagna is an ecommerce website and apps where you can buy from different sellers in one checkout.">
    <meta name="Keywords" content="ecommerce, Kuwait, gulf region, store, shopping, aswagna, online shopping, online store">
    @else 
    <meta name="Description" content="أسواقنا هو متجر و تطبيقات الكترونية حيث يمكنك التسوق و الشراء من أكثر من بائع بكل سهولة و سلة طبلبات واحدة">
    <meta name="Keywords" content="متجر، متجر الكتروني، اسواقنا، أسواقنا، شراء اونلاين">
    @endif --}}

    @yield('links')
    
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-169395352-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-169395352-1');
    </script>
</head>
<body>
	<div id="app">
        @yield('content')
    </div>
    <div class="custom-notification" style="display: none;">
        <i class="fa fa-times noti-cross"></i>
        <div class="noti-img">
            <img src="http://localhost/aswagna/firebase-logo.png">
        </div>

        <div class="noti-area">
            <div class="noti-title"></div>
            <div class="noti-desc">New stock has been added for - SEQUENCE Games. Hurry on!</div>
        </div>
    </div>
    @yield('scripts')
</body>
</html>
