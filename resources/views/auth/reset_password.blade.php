@extends('layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | Signup
@endsection
@section('links')
@include('includes.links')
<style type="text/css">
    @if(Config::get('app.locale') == 'en')
    .error{
        color: red;
    }
    @elseif(Config::get('app.locale') == 'ar')
    .error{
        color: red;
        text-align: right;
        width: 100%;
    }
    .chck_eml_rd{
        text-align: right;
        width: 100%;
        float: right;
        margin-bottom: 10px;
    }
    .chck_eml_grn{
        text-align: right;
        width: 100%;
        float: right;
        margin-bottom: 10px;
    }
    @endif
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="banner">
        <div class="sgnupbnr">
            <div class="container">
                <div class="login_body">
                    <div class="login_sec">
                        <h3>Reset password</h3>
                        <p>Welcome to Aswagna.</p>
                        @if (session()->has('success'))
                            <div class="alert alert-success vd_hidden" style="display: block;">
                                <a class="close" data-dismiss="alert" aria-hidden="true">
                                <i class="icon-cross"></i>
                                </a>
                                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                                <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                            </div>
                            @elseif ((session()->has('error')))
                            <div class="alert alert-danger vd_hidden" style="display: block;">
                                <a class="close" data-dismiss="alert" aria-hidden="true">
                                <i class="icon-cross"></i>
                                </a>
                                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                                <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
                            </div>
                            @endif
                        @if ($errors->any())                     
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>                                
                                {{ $errors->first() }}
                            </strong>
                        </div>
                        @endif
                            
                        <form method="POST" action="{{ route('user.link.forget.password.set') }}" id="myForm">
                            @csrf
                            <div class="name_field">
                                
                                <div class="form-group">
                                    <input type="text" class="login_type required" name="email" id="email" placeholder="Email id">
                                    <div class="error_email" style="color: red;"></div>
                                </div>
                                
                                <button type="button" id="reset_link" class="login_submit">Send reset password link</button>
                            
                                </form>
                            </div>
                        </div>
                        <!-- <p>Have an account? <a href="{{ route('login') }}">Login!</a></p> -->
                    </div>
                </div>
            </section>
            @include('includes.new_footer')
        </div>
        <!--wrapper end-->
        @endsection
        @section('scripts')
        @include('includes.scripts')
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
               
        $('#email').blur(function(event) {

            if($('#email').val() != ''){
                // alert('hi');
                $("#chck_eml_rd").html('');
                $("#chck_eml_grn").html('');
                var $this = $(this);
                var email = $(this).val();
                var re =/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(!re.test(email)) {
                    $("#chck_eml_rd").html('Please enter a valid email address');
                    $('#email').val('');
                } else {
                    var reqData = {
                        '_token': '{{ @csrf_token() }}', 
                        'params': {
                            'email': email
                        }
                    };
                    if(email != '') {
                        $.ajax({
                            url: '{{ route('user.check.email.forget.password') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: reqData,
                        })
                        .done(function(response) {
                            if(response.result == 0) {
                                $("#chck_eml_rd").html("This email doesnot exist!Please register your account first!");
                                $('#email').val('');
                                $($(this).next().html(''));
                            } else {
                                // $("#myform").submit();
                                // $("#chck_eml_grn").html(response.result.message);
                            }
                        })
                        .fail(function(error) {
                            // console.log(error);
                        });
                    }
                }     
            } else {
                $("#chck_eml_grn").html('');
                $("#chck_eml_rd").html('');
            }  
        });
        // $("#myform").validate();
        $("#reset_link").click(function(e){
            var email = $.trim($("#email").val());
            if(email != ""){
                // $("#myform").submit();
                $("#reset_link").attr("type","submit");
                $("#reset_link").trigger("click");
            }else{
                $(".error_email").html("Please provide valid email id ");
            }
        });
     //    $('#myForm').validate({
     //        rules: {              
     //            email : {
     //                required: true,
     //                email:true
     //            }
     //            // password : {
     //            //     required: true
     //            // }
     //        },
     //        // messages: {             
     //        //     email : {
     //        //         required: '@lang('merchant_lang.this_field_req')',
     //        //         email:'@lang('merchant_lang.valid_email')'
     //        //     },
     //        //     password : {
     //        //         required: '@lang('merchant_lang.this_field_req')'
     //        //     }
     //        // },
     //        errorPlacement: function(error, element) {
     //         // error.appendTo('#errordiv');
     //     },
         
     // });
        });
</script>
@endsection