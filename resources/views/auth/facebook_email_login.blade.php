@extends('layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | Signup
@endsection
@section('links')
@include('includes.links')
<style type="text/css">
    .error {
        border: solid 1px #fb8282 !important;
        background: #ff000014 !important;
    }
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="login-page">
        <div class="container">
            <div class="login-section">
                <div class="lgn-lftfrm">
                    <h3>Signin</h3>
                    <p>Provide email to signin</p>
                    <div class="login-from">
                        <form method="POST" action="{{ route('facebook.email.login', $id) }}" id="myForm">
                            @csrf
                            @if ($errors->any())
                            <div class="form-group">
                                <div class="alert alert-danger alert-dismissible" style="text-align: center;">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul style="margin-bottom: 0; padding-left: 0; ">
                                        @foreach ($errors->all() as $error)
                                        <li style="list-style: none; font-size: 14px; color: #fff;">{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email address" value="{{ old('email') }}" autofocus>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Log in">
                            </div>
                        </form>
                    </div>
                    {{-- <div class="or"><span>or</span></div> --}}
                </div>
            </div>
        </div>
    </section>
    @include('includes.footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#myForm').validate({
            rules: {              
                email : {
                    required: true,
                    email:true
                }
            },
            // messages: {             
            //     email : {
            //         required: '@lang('merchant_lang.this_field_req')',
            //         email:'@lang('merchant_lang.valid_email')'
            //     },
            //     password : {
            //         required: '@lang('merchant_lang.this_field_req')'
            //     }
            // },
            errorPlacement: function(error, element) {
             // error.appendTo('#errordiv');
         },

     });
    });
</script>
@endsection