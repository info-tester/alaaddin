@extends('layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | Signup
@endsection
@section('links')
@include('includes.links')
<style type="text/css">
    @if(Config::get('app.locale') == 'en')
    .error{
        color: red;
    }
    @elseif(Config::get('app.locale') == 'ar')
    
    .error{
        color: red;
        text-align: right;
        width: 100%;
    }
    .chck_eml_rd{
        text-align: right;
        width: 100%;
        float: right;
        margin-bottom: 10px;
    }
    .chck_eml_grn{
        text-align: right;
        width: 100%;
        float: right;
        margin-bottom: 10px;
    }
    @endif
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="banner">
        <div class="sgnupbnr">
            <div class="container">
                <div class="login_body">
                    <div class="login_sec">
                        <h3>Register</h3>
                        <p>Welcome to Aswagna.</p>
                        @if ($errors->any())                     
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>                                
                                {{ $errors->first() }}
                            </strong>
                        </div>
                        @endif
                        <form method="POST" action="{{ route('register') }}" id="myForm">
                            @csrf
                            <div class="name_field">
                                <div class="form-group">
                                    <input type="text" class="" name="fname" placeholder="First Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="" name="lname" placeholder="Last Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="" name="email" id="email" placeholder="Email Address">
                                    <span id="chck_eml_rd" class="chck_eml_rd text-danger"></span>
                                    <span id="chck_eml_grn" class="chck_eml_grn text-success"></span>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="login_type" name="phone" placeholder="Mobile Number" onkeypress='validate(event)'>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="login_type" name="password" id="password" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="login_type" name="password_confirmation" placeholder="Confirm Password">
                                </div>
                                <div class="form-group check_group logchk">
                                    <div class="checkbox-group"> 
                                        <input type="checkbox" class="required" name="terms" id="terms_and_conditions"> 
                                        <label for="terms_and_conditions" class="Fs16 sgnupchk">
                                            <span class="check find_chek"></span>
                                            <span class="box W25 boxx"></span>
                                            I agree to the Drivers Group <b>Terms of Use</b>
                                            and <b>Privacy Policy.</b>
                                        </label>
                                        <span id="terms" class="chck_eml_rd text-danger"></span>
                                    </div>
                                </div>
                                <button type="submit" class="login_submit">Register</button>
                            <!--<div class="orbdr"><span>or</span></div>
                                <button type="submit" class="face_bk clearfix">
                                        <span class="icon_social"><i class="fa fa-facebook" aria-hidden="true"></i></span>
                                        Signin with Facebook
                                    </button>-->
                                    <div class="orbdr"><span>OR Sign up with</span></div>
                                    <div class="sgupsocal">
                                        <div class="f_name">
                                            <a href="{{ url('login/facebook') }}"><img src="{{ asset('public/frontend/images/fb2.png') }}"></a>
                                        </div>
                                        <div class="l_name">
                                            <a href="{{ url('login/google') }}"><img src="{{ asset('public/frontend/images/gpls2.png') }}"></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <p>Have an account? <a href="{{ route('login') }}">Login!</a></p>
                    </div>
                </div>
            </section>
            @include('includes.new_footer')
        </div>
        <!--wrapper end-->
        @endsection
        @section('scripts')
        @include('includes.scripts')
        <script type="text/javascript">
            $(document).ready(function() {
               var terms = false;
        // console.log(terms);
        $("#chck_eml_rd1").html('');
        $("#chck_eml_grn1").html('');
        
        $('#terms_and_conditions').click(function() {
            if(!$(this).is(':checked')) {
                $("#terms").html('Please select terms and conditions.');
                terms = false;
            } else {
                terms = true;
                $("#terms").html('');
            }
        });
        $('#email').blur(function(event) {

            if($('#email').val() != ''){
                // alert('hi');
                $("#chck_eml_rd").html('');
                $("#chck_eml_grn").html('');
                var $this = $(this);
                var email = $(this).val();
                var re =/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(!re.test(email)) {
                    $("#chck_eml_rd").html('Please enter a valid email address');
                    $('#email').val('');
                } else {
                    var reqData = {
                        '_token': '{{ @csrf_token() }}', 
                        'params': {
                            'email': email
                        }
                    };
                    if(email != '') {
                        $.ajax({
                            url: '{{ route('user.check.email') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: reqData,
                        })
                        .done(function(response) {
                            if(!!response.error) {
                                $("#chck_eml_rd").html(response.error.merchant);
                                $('#email').val('');
                                $($(this).next().html(''));
                            } else {
                                // $("#chck_eml_grn").html(response.result.message);
                            }
                        })
                        .fail(function(error) {
                            console.log(error);
                        });
                    }
                }     
            } else {
                $("#chck_eml_grn").html('');
                $("#chck_eml_rd").html('');
            }  
        });
        $('#myForm').validate({
            rules: {
                fname : {
                    required: true,
                },  
                lname : {
                    required: true,
                },                
                email : {
                    required: true,
                    email:true
                },
                phone : {
                    required: true,
                    digits: true
                },
                password : {
                    required: true,
                    minlength: 6
                },
                terms : {
                    required: true,
                },    
                password_confirmation : {          
                    required: true,
                    equalTo : "#password"
                }
            },
            messages: {
                fname: {
                    required: '@lang('merchant_lang.this_field_req')'
                },      
                lname: {
                    required: '@lang('merchant_lang.this_field_req')'
                },                
                email : {
                    required: '@lang('merchant_lang.this_field_req')',
                    email:'@lang('merchant_lang.valid_email')'
                },
                phone : {
                    required: '@lang('merchant_lang.this_field_req')',
                    digits: '@lang('merchant_lang.only_digit')'
                },
                password : {
                    required: '@lang('merchant_lang.this_field_req')',
                    minlength: '@lang('merchant_lang.pass_char_list')'
                },
                terms : {
                    required: '@lang('merchant_lang.terms_cond')',
                },    
                password_confirmation : {          
                    required: '@lang('merchant_lang.this_field_req')',
                    equalTo : '@lang('merchant_lang.confirm_pass')'
                }
            },
            
            submitHandler: function (form) {
                // alert();
                // console.log(terms);
                //terms and condition checking
                if(terms == true) {
                    $("#terms").html('');
                    form.submit();
                } else {
                    if(terms == false) {
                        $("#terms").html('Please accept terms and conditions');
                        return false;
                    }
                }
            }
            
        });
    });

    function validate(evt) {
        var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          }
          var regex = /[0-9]|\./;
          if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
</script>
@endsection