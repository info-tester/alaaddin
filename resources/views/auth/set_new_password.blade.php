@extends('layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | Signup
@endsection
@section('links')
@include('includes.links')
<style type="text/css">
    @if(Config::get('app.locale') == 'en')
    .error{
        color: red;
    }
    @elseif(Config::get('app.locale') == 'ar')
    .error{
        color: red;
        text-align: right;
        width: 100%;
    }
    .chck_eml_rd{
        text-align: right;
        width: 100%;
        float: right;
        margin-bottom: 10px;
    }
    .chck_eml_grn{
        text-align: right;
        width: 100%;
        float: right;
        margin-bottom: 10px;
    }
    @endif
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="banner">
        <div class="sgnupbnr">
            <div class="container">
                <div class="login_body">
                    <div class="login_sec">
                        <h3>Reset password</h3>
                        <p>Welcome to Aswagna.</p>
                        @if ($errors->any())                     
                        <div class="alert alert-danger">
                            <a href="javascript:void(0)" class="close" data-dismiss="alert">&times;</a>
                            <strong>                                
                                {{ $errors->first() }}
                            </strong>
                        </div>
                        @endif
                        @if (session()->has('success'))
                            <div class="alert alert-success vd_hidden" style="display: block;">
                                <a class="close" data-dismiss="alert" aria-hidden="true">
                                <i class="icon-cross"></i>
                                </a>
                                <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                                <strong>{{ session('success')['meaning'] }}</strong> {{ session('success')['message'] }} 
                            </div>
                            @elseif ((session()->has('error')))
                            <div class="alert alert-danger vd_hidden" style="display: block;">
                                <a class="close" data-dismiss="alert" aria-hidden="true">
                                <i class="icon-cross"></i>
                                </a>
                                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                                <strong>{{ session('error')['meaning'] }}</strong> {{ session('error')['message'] }} 
                            </div>
                            @endif
                        <form method="POST" action="{{ route('user.update.forget.password.set',@$id) }}" id="myForm">
                            @csrf
                            <div class="name_field">
                                <!-- <div class="form-group"> -->
                                    <!-- <input type="text" class="" id="old_password" name="fname" placeholder="Old password"> -->
                                <!-- </div> -->
                                <!-- <div class="form-group"> -->
                                    <!-- <input type/="text" id="new_password" name="new_password" placeholder="New password"> -->
                                <!-- </div> -->
                                <!-- <div class="form-group"> -->
                                    <!-- <input type="text" class="" name="confirm_password" id="confirm_password" placeholder="Confirm password"> -->
                                    <!-- <span id="chck_eml_rd" class="chck_eml_rd text-danger"></span> -->
                                    <!-- <span id="chck_eml_grn" class="chck_eml_grn text-success"></span> -->
                                <!-- </div> -->
                                <!-- <div class="form-group">
                                    <input type="text" class="login_type" name="phone" placeholder="Mobile Number" onkeypress='validate(event)'>
                                </div> -->
                                <!-- <div class="form-group">
                                    <input type="text" class="login_type" name="email" id="email" placeholder="Email id">
                                </div> -->
                                <div class="form-group">
                                    <input type="password" class="login_type required" id="new_password" name="new_password" placeholder="New Password">
                                    <div class="errorPassword" style="color: red;"></div>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="login_type" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password">
                                    <div class="errorConfirmPassword" style="color: red;"></div>
                                </div>
                                <!-- <div class="form-group check_group logchk">
                                    <div class="checkbox-group"> 
                                        <input type="checkbox" class="required" name="terms" id="terms_and_conditions"> 
                                        <label for="terms_and_conditions" class="Fs16 sgnupchk">
                                            <span class="check find_chek"></span>
                                            <span class="box W25 boxx"></span>
                                            I agree to the Drivers Group <b>Terms of Use</b>
                                            and <b>Privacy Policy.</b>
                                        </label>
                                        <span id="terms" class="chck_eml_rd text-danger"></span>
                                    </div>
                                </div> -->
                                <button type="button" id="reset_link" class="login_submit">Reset password</button>
                            <!--<div class="orbdr"><span>or</span></div>
                                <button type="submit" class="face_bk clearfix">
                                        <span class="icon_social"><i class="fa fa-facebook" aria-hidden="true"></i></span>
                                        Signin with Facebook
                                    </button>-->
                                    <!-- <div class="orbdr"><span>OR Sign up with</span></div> -->
                                    <!-- <div class="sgupsocal">
                                        <div class="f_name">
                                            <a href="{{ url('login/facebook') }}"><img src="{{ asset('public/frontend/images/fb2.png') }}"></a>
                                        </div>
                                        <div class="l_name">
                                            <a href="{{ url('login/google') }}"><img src="{{ asset('public/frontend/images/gpls2.png') }}"></a>
                                        </div>
                                    </div> -->
                                </form>
                            </div>
                        </div>
                        <!-- <p>Have an account? <a href="{{ route('login') }}">Login!</a></p> -->
                    </div>
                </div>
            </section>
            @include('includes.new_footer')
        </div>
        <!--wrapper end-->
        @endsection
        @section('scripts')
        @include('includes.scripts')
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <script type="text/javascript">

            $(document).ready(function() {
               var terms = false;
        // console.log(terms);
        $("#chck_eml_rd1").html('');
        $("#chck_eml_grn1").html('');
        
        // $('#terms_and_conditions').click(function() {
        //     if(!$(this).is(':checked')) {
        //         $("#terms").html('Please select terms and conditions.');
        //         terms = false;
        //     } else {
        //         terms = true;
        //         $("#terms").html('');
        //     }
        // });
        $('#email').blur(function(event) {

            if($('#email').val() != ''){
                // alert('hi');
                $("#chck_eml_rd").html('');
                $("#chck_eml_grn").html('');
                var $this = $(this);
                var email = $(this).val();
                var re =/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(!re.test(email)) {
                    $("#chck_eml_rd").html('Please enter a valid email address');
                    $('#email').val('');
                } else {
                    var reqData = {
                        '_token': '{{ @csrf_token() }}', 
                        'params': {
                            'email': email
                        }
                    };
                    if(email != '') {
                        $.ajax({
                            url: '{{ route('user.check.email.forget.password') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: reqData,
                        })
                        .done(function(response) {
                            if(response.result == 0) {
                                $("#chck_eml_rd").html("This email doesnot exist!Please register your account first!");
                                $('#email').val('');
                                $($(this).next().html(''));
                            } else {
                                // $("#chck_eml_grn").html(response.result.message);
                            }
                        })
                        .fail(function(error) {
                            console.log(error);
                        });
                    }
                }     
            } else {
                $("#chck_eml_grn").html('');
                $("#chck_eml_rd").html('');
            }  
        });
        // $('#myForm').validate({
        //     rules: {
                
        //         password_confirmation : {          
        //             required: true,
        //             equalTo : "#password"
        //         }
        //     },
        //     messages: {
                
        //         password_confirmation : {          
        //             required: '@lang('merchant_lang.this_field_req')',
        //             equalTo : '@lang('merchant_lang.confirm_pass')'
        //         }
        //     },
        //     errorPlacement: function (error, element) 
        //     {
                    
        //             if (element.attr("name") == "password") {
        //                 var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.required')"+'</label>';
        //                 $('.errorPassword').html(error);
        //             }
        //             if (element.attr("name") == "confirm_password") {
        //                 var error = '<label for="fn" generated="true" class="error" style="color:#f85d2c;border:none!important;background:none!important;">'+"@lang('validation.confirm_password')"+'</label>';
        //                 $('.errorConfirmPassword').html(error);
        //             }
                    
        //         }
        //     });
            $("#reset_link").click(function(e){
                var new_password = $.trim($("#new_password").val());
                var password_confirmation = $.trim($("#password_confirmation").val());
                    if(new_password != "" && password_confirmation != "" && password_confirmation == new_password){
                        $("#reset_link").attr("type","submit");
                        $("#reset_link").trigger("click");
                    }else if(password_confirmation == ""){
                        $(".errorConfirmPassword").text("Please provide same password as new password!Password should match with new password!");
                    }else if(new_password == ""){
                        $(".errorPassword").text("Please provide new password!")
                    }
                    else if(password_confirmation != new_password){
                        $("#password_confirmation").val("");
                        $(".errorConfirmPassword").text("Please provide same password as new password!Password should match with new password!");
                    }
            });
            
        });

    function validate(evt) {
        var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          }
          var regex = /[0-9]|\./;
          if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
</script>
@endsection