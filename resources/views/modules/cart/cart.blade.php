@extends('layouts.app')

@section('title')
{{ config('app.name', 'Aswagna') }} | Shopping Cart
@endsection

@section('links')
@include('includes.links')
<link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<style>
    .blur_row_stock{
        filter: opacity(0.5);
    }
</style>
@endsection

@section('content')
<!--wrapper start-->
<div class="wrapper">

    @include('includes.header')

    <section class="search-body shoping-cart-body">
        <div class="container">
            <div class="bed-cumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('front_static.home')</a></li>
                    <li class="breadcrumb-item active">@lang('front_static.my_cart')</li>
                </ol>
            </div>
            <div class="main-dash">
                @if(@$cart)
                @php @$totUnavailableItem =0; @endphp
                <div class="cart-tabel modify-cart-table">
                    <div class="table-responsive">
                        <div class="table kwdarabic">
                            <div class="one_row1 hidden-sm-down only_shawo">
                                <div class="cell1 tab_head_sheet lft" style="width: 25%;">@lang('front_static.product_details')</div>
                                <div class="cell1 tab_head_sheet">@lang('front_static.seller')</div>
                                <div class="cell1 tab_head_sheet">@lang('front_static.quantity')</div>
                                <div class="cell1 tab_head_sheet">@lang('front_static.unit_price') </div>
                                <div class="cell1 tab_head_sheet">@lang('front_static.total')</div>
                                <div class="cell1 tab_head_sheet">&nbsp;</div>
                            </div>
                            <!--row 1-->

                            @foreach($cart->cartDetails as $key => $value)
                            <div class="one_row1 small_screen31 @if(@$value->product_variant_id) @if(@$value->productVariantDetails->stock_quantity < $value->quantity) blur_row_stock @endif @else @if(@$value->getProduct->stock < @$value->quantity) blur_row_stock @endif @endif">
                                <div class="cell1 tab_head_sheet_1 lft">
                                    <span class="add_ttrr" style="width: 100% !important;">
                                        <a href="{{ route('product', $value->getProduct->slug) }}">
                                            <span class="tabel-image">
                                                @if(@$value->defaultImage->image)
                                                <img src="{{ url('storage/app/public/products/' . @$value->defaultImage->image) }}" alt="">
                                                @else
                                                <img src="{{ getDefaultImageUrl() }}" alt="">
                                                @endif
                                            </span>
                                        </a>
                                        <h5>{{ $value->productByLanguage->title }}</h5>
                                        @if(@$value->variants != 'null')
                                        @foreach(json_decode($value->variants) as $row)
                                        <h6>{{ $row->{$language_id}->variant }} : {{ $row->{$language_id}->variant_value }}</h6>
                                        @endforeach                                        
                                        @endif
                                    </span>
                                    <div class="out_stock" style="color: #f00"> 
                                        @if(@$value->product_variant_id)
                                            @if(@$value->productVariantDetails->stock_quantity == 0)
                                            <span>This product is currently out of stock.</span>
                                            @php $totUnavailableItem += 1; @endphp
                                            @elseif(@$value->productVariantDetails->stock_quantity < $value->quantity)
                                            <span>only {{ @$value->productVariantDetails->stock_quantity }} item is available.</span>
                                            @php $totUnavailableItem += 1; @endphp
                                            @endif
                                        @else
                                            @if(@$value->getProduct->stock < @$value->quantity && @$value->getProduct->stock)
                                            <span>only {{ @$value->getProduct->stock }} item is available.</span>
                                            @php $totUnavailableItem += 1; @endphp
                                            @elseif(@$value->getProduct->stock == 0)
                                            <span>This product is currently out of stock.</span>
                                            @php $totUnavailableItem += 1; @endphp
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="cell1 tab_head_sheet_1" style="width: 25%;">
                                    <span class="W55_1">@lang('front_static.seller')</span>
                                    <span class="add_ttrr">
                                        <span class="seller-logo"><img src="{{ url('storage/app/public/profile_pics/' . $value->productMarchant->image ) }}" alt=""></span>
                                        <h4>{{ $value->productMarchant->company_name }}</h4>
                                    </span>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">@lang('front_static.quantity')</span>
                                    <span class="add_ttrr">
                                        <div class="ccount">
                                            <input class="login_typec number-qty quantity" type="number" min="1" max="100" value="{{ $value->quantity }}" data-id="{{ @$value->id }}" data-product="{{ $value->product_id }}" data-variant="{{ $value->product_variant_id }}" />
                                        </div>
                                    </span>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">@lang('front_static.unit_price') </span>
                                    <p class="add_ttrr kwdarabic">{{ number_format(@$value->original_price, 3) }} {{ getCurrency() }}</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">@lang('front_static.total')</span>
                                    <p class="kwdarabic add_ttrr cart_dtl_{{ $value->id }}">{{ number_format(@$value->subtotal, 3) }} {{ getCurrency() }}</p>
                                </div>
                                <div class="cell1 tab_head_sheet_1">
                                    <span class="W55_1">@lang('front_static.action')</span>
                                    <p class="add_ttrr"><a class="dlt" href="{{ route('remove.cart', $value->id) }}" onclick="return confirm('@lang('front_static.remove_cart')')"><i class="fa fa-trash-o" aria-hidden="true"></i></a></p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @if(@$cart)
                    <div class="cart-total-area">
                        <div class="right-totals">
                            <div class="totals">
                                <p>@lang('front_static.subtotal') <span id="subtotal" class="kwdarabics">{{ number_format(@$cart->subtotal, 3) }} {{ getCurrency() }}</span></p>
                                <p>@lang('front_static.total_discount') <span class="text-success kwdarabic" id="total_discount">- {{ number_format(@$cart->total_discount, 3) }} {{ getCurrency() }}</span></p>
                                <p>@lang('front_static.shipping_charges') <small class="text-success kwdarabic" id="shipping1">@lang('front_static.shipping_calculated')</small></p>
                                <div class="borders"></div>
                                <h4>@lang('front_static.total') <span id="total" class="kwdarabic">{{ number_format(@$cart->total, 3) }} {{ getCurrency() }}</span></h4>
                                <div class="borders"></div>
                            </div>
                            <div class="check-out-bttns">
                                <a href="#">@lang('front_static.continue_shopping')</a>
                                @auth
                                    @if($totUnavailableItem)
                                    <a class="proc out_of_stock_msg" href="javascript:;">@lang('front_static.proceed_to_secure_checkout')</a>
                                    @else
                                    <a class="proc" href="{{ route('check.out') }}">@lang('front_static.proceed_to_secure_checkout')</a>
                                    @endif
                                @else
                                    @if($totUnavailableItem)
                                    <a class="proc out_of_stock_msg" href="javascript:;">@lang('front_static.proceed_to_secure_checkout')</a>
                                    @else
                                    <a class="proc" href="javascript:;" data-toggle="modal" data-target="#myModal">@lang('front_static.proceed_to_secure_checkout')</a>
                                    @endif
                                @endauth
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                @else
                <div class="no_item">
                    <img src="public/frontend/images/no_cart.png">
                    <h3>@lang('front_static.empty_cart')</h3>
                    <small>@lang('front_static.add_item_cart')</small>
                    <a href="{{ url('/') }}" class="btn btn-success">Shop Now</a>
                </div>
                @endif
            </div>
        </div>
    </section>
    @include('includes.new_footer')

    <!-- The Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog welcome-pop">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-4 col-sm-6 col-sm-6">
                            <div class="pop-box">
                                <a href="{{ route('guest.check.out') }}">
                                    <img src="{{ asset('public/frontend/images/male.png') }}" alt="">
                                    <p>New user <br> continue as guest</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 col-sm-6">
                            <div class="pop-box">
                                <a href="{{ route('register') }}">
                                    <img src="{{ asset('public/frontend/images/edit.png') }}" alt="">
                                    <p>New user <br> create an account</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 col-sm-6">
                            <div class="pop-box">
                                <a href="{{ route('login') }}">
                                    <img src="{{ asset('public/frontend/images/login.png') }}" alt="">
                                    <p>Existing user <br> please login</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!--wrapper end-->
@endsection

@section('scripts')
@include('includes.scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
    $(document).ready(function() {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        (function($) {
            $.fn.spinner = function() {
                this.each(function() {
                    var el = $(this);
                    // add elements
                    el.wrap('<span class="spinner"></span>');     
                    el.after('<span class="add">+</span>');
                    el.before('<span class="sub">-</span>');

                    // substract
                    el.parent().on('click', '.sub', function () {
                      if (el.val() > parseInt(el.attr('min')))
                        el.val( function(i, oldval) { return --oldval; });
                        updateQty(el.val(), el.data('id'), el.data('product'), el.data('variant'))
                    });

                    // increment
                    el.parent().on('click', '.add', function () {
                      if (el.val() < parseInt(el.attr('max')))
                        el.val( function(i, oldval) { return ++oldval; });
                        updateQty(el.val(), el.data('id'), el.data('product'), el.data('variant'))
                    });
                });
            };
        })(jQuery);
        $('input[type=number]').spinner();

        $('.out_of_stock_msg').click(function() {
            toastr.warning('Please remove unavailable item(s) from cart and try again.');
        })
    });

    function updateQty(qty, cartDetailsId, productId, productVariantId) {
        var reqData = {
            jsonrpc: '2.0',
            _token: '{{ csrf_token() }}',
            params: {
                quantity: qty,
                cart_details_id: cartDetailsId,
                cart_master_id: '{{ @$cart->id }}',
                product_id: productId,
                product_variant_id: productVariantId
            }
        };

        $.ajax({
            url: '{{ route('update.cart') }}',
            type: 'post',
            data: reqData,
            dataType: 'json',
            success: function(response) {
                if(response.error) {
                    toastr['error']('[' + response.error.code + ']: ' + response.error.meaning)
                } else {
                    $('.cart_dtl_' + cartDetailsId).html(response.result.cart_details.subtotal + ' KWD')
                    $('#total').html(response.result.cart.total + ' KWD')
                    $('#subtotal').html(response.result.cart.subtotal + ' KWD')
                    $('#total_discount').html('-' + response.result.cart.total_discount + ' KWD')
                    $('#shipping').html(response.result.cart.shipping_price + ' KWD')
                }
            }
        })
    }
</script> 
@endsection