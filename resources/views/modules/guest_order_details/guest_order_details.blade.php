@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.ord_dtls')
@endsection
@section('links')
@include('includes.links')
<style type="text/css">
    .error {
        border: solid 1px #fb8282 !important;
        background: #ff000014 !important;
    }
</style>
@endsection
@section('content')
<!--wrapper start-->
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="search-body">
    <div class="container">
        <div class="main-dash">
            <!-- @include('includes.user_sidebar') -->
            <div class="right-dashboard">
                <div class="shipping-area order-his">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="shipping-box" style="margin-bottom: 15px;">
                                <h3>@lang('front_static.order_summery')</h3>
                                <p>@lang('front_static.order_no') : <span>{{ @$order->order_no }} </span></p>
                                <p>@lang('front_static.ordered_date_time') : <span>{{ @$order->created_at }}  </span></p>
                                @if(@$order->status == 'OD')
                                <p>Delivery Date: <span>{{ @$order->delivery_date }}  </span></p>
                                <p>Delivery Time: <span>{{ @$order->delivery_time }}  </span></p>
                                @endif
                                <p>@lang('front_static.number_of_item') : <span>{{ @$order->orderMasterDetails->count() }} </span></p>
                                @if(@$order->coupon_discount != 0)
                                <p>Coupon Discount: <span>{{ @$order->coupon_discount }} KWD ({{ @$order->coupon_code }})</span></p>
                                @endif
                                <p>Order Subtotal: <span>{{ number_format(@$order->subtotal,3) }} KWD</span></p>
                                <p>Product Discount: <span>{{ number_format(@$order->total_discount,3) }} KWD</span></p>
                                <p>Shipping Cost: <span>{{ number_format(@$order->shipping_price,3) }} KWD</span></p>
                                <p>Order @lang('front_static.total') : <span>{{ number_format(@$order->order_total,3) }} KWD</span></p>
                                <p>Payment Method: 
                                    <span>
                                        @if(@$order->payment_method == 'C')
                                        (COD) Cash on Delivery
                                        @elseif(@$order->payment_method == 'O')
                                        Online
                                        @elseif(@$order->payment_method == 'F')
                                        Payment failed
                                        @endif
                                    </span>
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="shipping-box">
                                <h3>@lang('front_static.shipping_address')</h3>
                                <p>@lang('front_static.country')  : {{ @$order->getCountry->name }}</p>
                                <p>@lang('front_static.name')  : {{ @$order->shipping_fname }} {{ @$order->shippingAddress->shipping_lname }}</p>
                                <p>@lang('front_static.email') : <span>{{ @$order->shipping_email }}</span></p>
                                <p>Phone : <span>{{ @$order->shipping_phone }}</span></p>
                                <p>@lang('front_static.city') : <span>@if(@$order->shipping_country == 134){{ @$order->getCityNameByLanguage->name }} @else {{ @$order->shipping_city }}  @endif</span></p>
                                <p>@lang('front_static.block') : <span>{{ @$order->shipping_block }}</span></p>
                                <p>@lang('front_static.street') : <span>{{ @$order->shipping_street }}</span></p>
                                @if(@$order->shipping_country != 134)
                                <p>@lang('front_static.postal_code') : <span>{{ @$order->shipping_postal_code }}</span></p>
                                @endif
                                <p>@lang('front_static.building') : <span>{{ @$order->shipping_building }}</span></p>
                                <p>@lang('front_static.more_address_details') : <span>{{ @$order->shipping_more_address }}</span></p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="shipping-box">
                                <h3>@lang('front_static.billing_address')</h3>
                                <p>@lang('front_static.country')  : {{ @$order->getBillingCountry->name }}</p>
                                <p>@lang('front_static.name')  : {{ @$order->billing_fname }} {{ @$order->billing_lname }}</p>
                                <p>@lang('front_static.email') : <span>{{ @$order->billing_email }}</span></p>
                                <p>@lang('front_static.phone') : <span>{{ @$order->billing_phone }}</span></p>
                                <p>@lang('front_static.city') : <span>{{ @$order->getBillingCityNameByLanguage->name }} </span></p>
                                <p>@lang('front_static.block') : <span>{{ @$order->billing_block }} </span></p>
                                <p>@lang('front_static.street') : <span>{{ @$order->billing_street }} </span></p>
                                @if(@$order->billingAddress->country)
                                <p>@lang('front_static.postal_code') : <span>{{ @$order->billing_postal_code }}</span></p>
                                @endif
                                <p>@lang('front_static.building') : <span>{{ @$order->billing_building }} </span></p>
                                <p>@lang('front_static.more_address_details') : <span>{{ @$order->billing_more_address }}</span></p>
                            </div>
                        </div>
                    </div>
                </div>

                @if(@$order->status == 'OD' && @$order->is_review_done != 'Y')
                <form id="post_review" class="post_review" method="post" action="{{ route('post.review.customer',@$order->id) }}">
                @csrf
                @endif
                @if(@$order->orderMasterDetails)
                @foreach(@$order->orderMasterDetails as $key=>$details)
                <div class="singel-order his-detls">
                    <div class="product-show-left">
                        <span class="ppro-img">
                            @if(@$details->productDetails->defaultImage->image)
                            @php
                            $image_path = 'storage/app/public/products/'.@$details->productDetails->defaultImage->image; 
                            @endphp
                            @if(file_exists(@$image_path))
                            <div class="profile" style="display: block;">
                                <img id="profilePicture" src="{{ URL::to('storage/app/public/products/'.@$details->productDetails->defaultImage->image) }}" alt="" style="width: 100px;height: 100px;">
                            </div>
                            @endif
                            @endif
                            {{-- <img src="images/pro1.png" alt=""> --}}

                        </span>
                        <h5>{{ $details->productDetails->productByLanguage->title }}</h5>
                        <span class="error_review_customer" style="color: red;"></span>

                        <input type="hidden" class="seller{{ @$details->id }}" name="sellerids[]" value="{{ @$details->seller_id }}">
                        <input type="hidden" class="variant{{ @$details->id }}" name="variantids[]" value="{{ @$details->product_variant_id }}">
                        <input type="hidden" class="pro{{ @$details->id }}" name="productids[]" value="{{ @$details->product_id }}">
                        <input type="hidden" class="dltlid{{ @$details->id }}" name="ordrdtlids[]" value="{{ @$details->id }}">
                        <input type="hidden" class="ordid{{ @$details->id }}" name="ordermasterid" value="{{ @$details->order_master_id }}">
                        <input type="hidden" class="ordid{{ @$details->id }}" name="ordermasterid" value="{{ @$details->order_master_id }}">
                        <input type="hidden" class="customer_name{{ @$details->id }}" name="customer_name[]" value="{{ @Auth::user()->fname.' '.@Auth::user()->lname }}">

                        <input type="hidden" class="rate_product{{ @$details->id }}" name="rate_product[]" value="">
                        @if(@$order->status == 'OD' && @$order->is_review_done != 'Y')
                        <!-- <ul class="review_customer review_product{{ @$details->id }} " data-detailid="{{ @$details->id }}" data-sellerid="{{ @$details->seller_id }}" data-productid="{{ @$details->product_id }}" data-variantid="{{ @$details->product_id }}" data-rate="0">
                            <li data-rate="1">
                                <a href="javascript:void(0)" class="starone{{ @$details->id }} star star{{ @$details->id }} star_1 star1{{ @$details->id }}"><img src="public/frontend/images/star2.png" alt=""></a>
                            </li>
                            <li data-rate="2">
                                <a href="javascript:void(0)" class="startwo{{ @$details->id }} star star{{ @$details->id }} star_2 star_2{{ @$details->id }}"><img src="public/frontend/images/star2.png" alt=""></a>
                            </li>
                            <li data-rate="3">
                                <a href="javascript:void(0)" class="starthree{{ @$details->id }} star star{{ @$details->id }} star_3 star_3{{ @$details->id }}"><img src="public/frontend/images/star2.png" alt=""></a>
                            </li>
                            <li data-rate="4">
                                <a href="javascript:void(0)" class="starfour{{ @$details->id }} star star{{ @$details->id }} star_4 star_4{{ @$details->id }}"><img src="public/frontend/images/star2.png" alt=""></a>
                            </li>
                            <li data-rate="5">
                                <a href="javascript:void(0)" class="starfive{{ @$details->id }} star star{{ @$details->id }} star_5 star_5{{ @$details->id }}"><img src="public/frontend/images/star2.png" alt=""></a>
                            </li>
                        </ul> -->
                        <br/>
                        @elseif(@$order->status == 'OD' && @$order->is_review_done == 'Y')
                            <!-- @if(@$details->rate == 5)
                            <ul class="review_customer">
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                            </ul>
                            <br/>
                            @elseif(@$details->rate == 4)
                            <ul class="review_customer">
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                </li>
                            </ul>
                            <br/>
                            @elseif(@$details->rate == 3)
                            <ul class="review_customer">
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                </li>
                            </ul>
                            <br/>
                            @elseif(@$details->rate == 2)
                            <ul class="review_customer">
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                </li>
                            </ul>
                            <br/>
                            @elseif(@$details->rate == 1)
                            <ul class="review_customer">
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star1.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="public/frontend/images/star2.png" alt=""></a>
                                </li>
                            </ul>
                            <br/>
                            @endif -->
                        @endif
                        @php
                        $attr = json_decode($details->variants,true);
                        @endphp
                        
                        @if(@$attr)
                            @foreach(@$attr as $value)
                                @foreach(@$value as $key=>$v)
                                @if($key == getLanguage()->id)
                                @php
                                $variant = $v['variant'];
                                $variant_value = $v['variant_value'];
                                @endphp
                                <p class="half-p">{{ @$variant }} : <span> {{ @$variant_value }}</span></p>
                                @endif
                                @endforeach
                            @endforeach
                        @endif

                        @if(@$order->status == 'OD' && @$order->is_review_done != 'Y')
                        <div class="comment_div">
                            <!-- <textarea id="review_comment{{ @$details->id }}" name="product_comment[]" class="login_type reviewcomment required" placeholder="Please write comments for this product..." style="resize: none;"></textarea> -->
                            <br/>
                            <span class="error_review_comment" style="color: red;"></span>
                        </div>
                        @elseif(@$order->status == 'OD' && @$order->is_review_done == 'Y')
                        <div class="comment_div">
                            <!-- <textarea  class="login_type reviewcomment" placeholder="Please write comments for this product..." style="resize: none;" disabled readonly>{{ @$details->comment}}</textarea> -->
                            <br/>
                            <span class="error_review_comment" style="color: red;"></span>
                        </div>
                        @endif
                    </div>
                    <div class="qunty">
                        <p>@lang('front_static.quantity') : <span> {{ @$details->quantity }}</span></p>
                        <p>Unit Price: <span> {{ @$details->original_price }} KWD</span></p>
                        <p>Subtotal: <span> {{ @$details->sub_total }} KWD</span></p>
                        <p>Discounted Price: <span> {{ @$details->discounted_price }} KWD</span></p>
                        <p>Total: <span> {{ @$details->total }} KWD</span></p>
                    </div>
                    <div class="qunty delivry">
                        <p>
                            <span style="font-size:14px; text-transform:capitalize;">
                                Status:
                            @if(@$details->status == 'I')
                            Incomplete
                            @elseif(@$details->status == 'N')
                            New
                            @elseif(@$details->status == 'OA')
                            @lang('admin_lang.order_accepted')
                            @elseif(@$details->status == 'DA')
                            Out for delivery
                            @elseif(@$details->status == 'RP')
                            @lang('admin_lang.ready_for_pickup')
                            @elseif(@$details->status == 'OP')
                            Picked up
                            @elseif(@$details->status == 'OD')
                            @lang('admin_lang.delivered_1')
                            @elseif(@$details->status == 'OC')
                            @lang('admin_lang.Cancelled_1')
                            @endif @lang('front_static.on') : <span>{{ @$details->updated_at }} </span>
                            </span>
                        </p>
                    </div>
                </div>
                @endforeach
                @endif

                @if(@$order->status == 'OD' && @$order->is_review_done != 'Y')
                 <!-- <a id="submitreviewbutton" class="view cmt" href="javascript:void(0)">Submit Review</a>  -->
                </form>  
                @endif
            </div>
            
        </div>
    </div>
</section>
    @include('includes.new_footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
<script>
    $(document).ready(function(){
        $(document).on("click","#submitreviewbutton",function(e){
            var len = $(".review_customer").length,text,review,error=0;
            for(var i=0;i<len;i++){
                // alert($.trim($(".reviewcomment").eq(i).val()));
                text = $.trim($(".reviewcomment").eq(i).val());
                rate = parseInt($.trim($(".review_customer").eq(i).attr("data-rate")));
                if(text == ""){
                    error++;
                    $(".error_review_comment").eq(i).text("Please comment for this product!");
                }else{
                    $(".error_review_comment").eq(i).text("");
                }
                if(rate == 0){
                    error++;
                    $(".error_review_customer").eq(i).text("Please rate this product!");
                }else{
                    $(".error_review_customer").eq(i).text("");
                }
            }
            if(error == 0){
                $("#post_review").submit();    
            }
        });
        $('#password').keyup(function(event) {
            $('#new_password').addClass('required');
        });
        $("#edit_form" ).validate({
         rules:{ 

            email : {
                required: true,
                email:true
            },
            phone : {
                required: true,
                digits: true
            },
            zipcode : {
                required: true,
                digits: true
            },
            new_password : {
                minlength : 6,
                maxlength: 15
            },
            confirm_password : {          
                required: false,
                equalTo : "#new_password"
            }
        },
        errorPlacement: function (error , element) {
                //toastr:error(error.text());
            },
        });
    });
</script>
{{-- For show image --}}
<script>
    $(document).ready(function() {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').css({"width": "100px", "height": "100px"});
            $('#blah').attr('src', e.target.result);
            $('#blah').hide();
            $('#blah').fadeIn(500);
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#original").change(function(){
            readURL(this);
        });
    });
</script>
{{-- For email validation --}}
<script>
    $(document).ready(function() {
        $('#email').blur(function(event) {
            if($('#email').val() != ''){
                // alert('hi');
                $("#chck_eml_rd").html('');
                $("#chck_eml_grn").html('');
                var $this = $(this);
                var email = $(this).val();
                var re =/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(!re.test(email)) {
                    $("#chck_eml_rd").html('Please enter a valid email address');
                    $('#email').val('');
                } else {
                    var reqData = {
                        '_token': '{{ @csrf_token() }}', 
                        'params': {
                            'email': email
                        }
                    };
                    if(email != '') {
                        $.ajax({
                            url: '{{ route('user.check.new.email') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: reqData,
                        })
                        .done(function(response) {
                            if(!!response.error) {
                                $("#chck_eml_rd").html(response.error.user);
                                $('#email').val('');
                            } else {
                                // $("#chck_eml_grn").html(response.result.message);
                            }
                        })
                        .fail(function(error) {
                            console.log(error);
                        });
                    }
                }     
            } else {
                $("#chck_eml_grn").html('');
            }  
        });
        $(document).on("click",".star",function(e){
            var id = $(this).parent().parent().attr("data-detailid"),
            rate = $(this).parent().parent().attr("data-rate"),
            star1 = $(this).hasClass("star_1"),
            star2 = $(this).hasClass("star_2"),
            star3 = $(this).hasClass("star_3"),
            star4 = $(this).hasClass("star_4"),
            star5 = $(this).hasClass("star_5");
            if(star1){
                $(".star"+id).html('<img src="public/frontend/images/star2.png" alt="">');
                $(".starone"+id).html('<img src="public/frontend/images/star1.png" alt="">');
                $(".review_product"+id).attr("data-rate","1");
                $(".rate_product"+id).val("1");
                // $(".rate_product"+id).attr(value,"1");
            }
            else if(star2){
                $(".star"+id).html('<img src="public/frontend/images/star2.png" alt="">');
                $(".starone"+id).html('<img src="public/frontend/images/star1.png" alt="">');
                $(".startwo"+id).html('<img src="public/frontend/images/star1.png" alt="">');
                $(".review_product"+id).attr("data-rate","2");
                $(".rate_product"+id).val("2");
            }
            else if(star3){
                $(".star"+id).html('<img src="public/frontend/images/star2.png" alt="">');
                $(".starone"+id).html('<img src="public/frontend/images/star1.png" alt="">');
                $(".startwo"+id).html('<img src="public/frontend/images/star1.png" alt="">');
                $(".starthree"+id).html('<img src="public/frontend/images/star1.png" alt="">');
                $(".review_product"+id).attr("data-rate","3");
                $(".rate_product"+id).val("3");
            }
            else if(star4){
                $(".star"+id).html('<img src="public/frontend/images/star2.png" alt="">');
                $(".starone"+id).html('<img src="public/frontend/images/star1.png" alt="">');
                $(".startwo"+id).html('<img src="public/frontend/images/star1.png" alt="">');
                $(".starthree"+id).html('<img src="public/frontend/images/star1.png" alt="">');
                $(".starfour"+id).html('<img src="public/frontend/images/star1.png" alt="">');
                $(".review_product"+id).attr("data-rate","4");
                $(".rate_product"+id).val("4");
            }
            else if(star5){
                $(".star"+id).html('<img src="public/frontend/images/star1.png" alt="">');
                $(".review_product"+id).attr("data-rate","5");
                $(".rate_product"+id).val("5");
            }
        });
        $(document).on("click",".starmark",function(e){
            $(this).removeClass("starmark");
            $(this).html('<img src="public/frontend/images/star2.png" alt="">');
        });
        // $(document).on("click",".star",function(e){
        //     $(this).addClass("starmark");
        //     $(this).html('<img src="public/frontend/images/star1.png" alt="">');
        // });
        // $(document).on("click",".starmark",function(e){
        //     $(this).removeClass("starmark");
        //     $(this).html('<img src="public/frontend/images/star2.png" alt="">');
        // });
    });

    function validate(evt) {
        var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          }
          var regex = /[0-9]|\./;
          if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
</script>
@endsection