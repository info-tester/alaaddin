@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.check_out')
@endsection
@section('links')
@include('includes.links')
<link href="{{ asset('public/frontend/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
    .country_code {
            /* display: inline-block; */
        position: absolute;
        left: 0;
        top: 28px;
        background: #fff;
        padding: 12px;
        border: solid 1px #000;
        border-right: none;
        width: 67px;
    }
    .error {
        border: solid 1px #fb8282 !important;
        background: #ff000014 !important;
    }
    .check-out-bttns button {
        color: #fff;
        font-size: 16px;
        font-weight: 500;
        background: #0771d4;
        font-family: 'Poppins', sans-serif;
        padding: 8px 10px;
        display: inline-block;
        margin-left: 9px;
        border-radius: 2px;
        cursor: pointer;
        border: navajowhite;
    }
    .check-out-bttns button:hover {
        background: #454545;
    }

    .chosen-container {
        background: #fff;
        height: 50px;
        border: none;
        border-radius: 2px;
        width: 100%;
        padding: 10px 15px;
        color: #333;
        font-family: 'Poppins', sans-serif;
        font-size: 14px;
        font-weight: 400;
        float: left;
        appearance: none;
        -moz-appearance: none;
        -webkit-appearance: none;
        border: 1px solid #cccbcb;
        margin-bottom: 0px;
    }
    .chosen-container-single .chosen-drop {
        margin-top: -1px;
        border-radius: 0 0 4px 4px;
        left: 0;
        background-clip: padding-box;
    }
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="search-body shoping-cart-body">
        <div class="container">
            <div class="bed-cumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">@lang('front_static.home')</a></li>
                    <li class="breadcrumb-item ">@lang('front_static.my_cart')</li>
                    <li class="breadcrumb-item active">@lang('front_static.check_out')</li>
                </ol>
            </div>
            <div class="main-dash">
                @if ((session()->has('error')))
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="alert alert-danger vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong id="form_result">[{{ session('error')['code'] }}] :</strong> {{ session('error')['meaning'] }} 
                        
                        @if(Session::get('merchantCn'))
                        @foreach(@Session::get('merchantCn') as $key => $mn)
                        <span>{{ $mn->company_name }} @if(count(Session::get('merchantCn')) > $key+1) , @else ! @endif</span>
                        @endforeach
                        @endif
                        @php
                        if(Session::get('merchantCn')) {
                            Session::forget('merchantCn');     
                        }
                        @endphp
                    </div>
                </div>
                @endif
                <form action="{{ route('store.oreder') }}" method="post" id="order-form">
                    @csrf
                    <div class="cart-tabel modify-cart-table">
                        <div class="table-responsive">
                            <div class="table">
                                <div class="one_row1 hidden-sm-down only_shawo">
                                    <div class="cell1 tab_head_sheet">@lang('front_static.product_details')</div>
                                    <div class="cell1 tab_head_sheet">@lang('front_static.seller')</div>
                                    <div class="cell1 tab_head_sheet">@lang('front_static.quantity')</div>
                                    <div class="cell1 tab_head_sheet">@lang('front_static.unit_price') </div>
                                    <div class="cell1 tab_head_sheet">@lang('front_static.total')</div>
                                </div>
                                @foreach(@$cartDetails as $cd)       
                                <input type="hidden" name="product_id[]" value="{{ $cd->product_id }}">
                                <input type="hidden" name="product_variant_id[{{ $cd->product_id }}][]" value="{{ $cd->product_variant_id }}">
                                <input type="hidden" name="variants[{{ $cd->product_id }}][]" value="{{ $cd->variants }}">
                                <input type="hidden" name="seller_id[{{ $cd->product_id }}][]" value="{{ $cd->productMarchant->id }}">
                                <input type="hidden" name="quantity[{{ $cd->product_id }}][]" value="{{ $cd->quantity }}">
                                @if(@$cd->productVariantDetails->weight)
                                <input type="hidden" name="weight[{{ $cd->product_id }}][]" value="{{ @$cd->productVariantDetails->weight }}">
                                @else
                                <input type="hidden" name="weight[{{ $cd->product_id }}][]" value="{{ @$cd->getProduct->weight }}">
                                @endif
                                <input type="hidden" name="original_price[{{ $cd->product_id }}][]" value="{{ $cd->original_price }}">
                                <input type="hidden" name="discounted_price[{{ $cd->product_id }}][]" value="{{ $cd->discounted_price }}">
                                <input type="hidden" name="sub_total[{{ $cd->product_id }}][]" value="{{ $cd->subtotal }}">
                                <input type="hidden" name="total[{{ $cd->product_id }}][]" value="{{ $cd->total }}">
                                <!--row 1-->                        
                                <div class="one_row1 small_screen31">
                                    <div class="cell1 tab_head_sheet_1 lft">
                                        <span class="W55_1">@lang('front_static.product_details')</span>
                                        <span class="add_ttrr">
                                            <a href="{{ route('product',@$cd->getProduct->slug) }}">
                                                @if(@$cd->defaultImage->image)
                                                <span class="tabel-image"><img src="{{url('storage/app/public/products/'.@$cd->defaultImage->image)}}" alt=""></span>
                                                @else
                                                <span class="tabel-image">
                                                    <img src="{{ getDefaultImageUrl() }}" alt="">
                                                </span>
                                                @endif
                                            </a>
                                            <a href="{{ route('product',@$cd->getProduct->slug) }}">
                                                <h5>{{ $cd->productByLanguage->title }}</h5>
                                            </a>
                                            @if($cd->variants != "null")
                                            @foreach(json_decode($cd->variants) as $row)
                                            <h6>{{ $row->{$language_id}->variant }} : {{ $row->{$language_id}->variant_value }}</h6>
                                            @endforeach
                                            @endif
                                        </span>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('front_static.seller')</span>
                                        <span class="add_ttrr">
                                            @if(@$cd->productMarchant->image)
                                            <span class="seller-logo"><img src="{{url('storage/app/public/profile_pics/'.@$cd->productMarchant->image)}}" alt=""></span>
                                            @else
                                            <span class="seller-logo">
                                                <img src="{{ getDefaultImageUrl() }}" alt="">
                                            </span>
                                            @endif
                                            <h4>{{ @$cd->productMarchant->company_name }}</h4>
                                        </span>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('front_static.quantity')</span>
                                        <p class="add_ttrr">{{ $cd->quantity }}</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('front_static.unit_price') </span>
                                        <p class="add_ttrr">{{ number_format($cd->original_price, 3) }} {{ getCurrency() }}</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('front_static.total')</span>
                                        <p class="add_ttrr">{{ number_format($cd->subtotal, 3) }} {{ getCurrency() }}</p>
                                    </div>
                                </div>
                                <!--row 1-->
                                @endforeach    
                            </div>
                        </div>
                        <div class="cart-total-area check-out-sucs">
                            <div class="right-totals">
                                <div class="totals">
                                    <p>@lang('front_static.subtotal') <span>{{ number_format($cart->subtotal, 3) }} {{ getCurrency() }}</span></p>
                                    <p>@lang('front_static.total_discount') <span class="text-success"> - {{ number_format($cart->total_discount, 3) }} {{ getCurrency() }}</span></p>
                                    <p>@lang('front_static.shipping_charges') <small class="text-success">@lang('front_static.shipping_calculated')</small></p>
                                    <div class="borders"></div>
                                    <h4>@lang('front_static.total') <span>{{ number_format($cart->total, 3) }} {{ getCurrency() }}</span></h4>
                                    <div class="borders"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="total_discount" value="{{ $cart->total_discount }}">
                    <input type="hidden" name="subtotal" value="{{ $cart->subtotal }}">
                    <input type="hidden" name="order_total" value="{{ $cart->total }}">
                    <div class="info-area guest">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>@lang('front_static.personal_information')</h3>
                            </div>
                        </div>
                        <div class="new-adrs-area new1">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.first_name')</label>
                                        <input type="text" class="login_type required" placeholder="@lang('front_static.enter_your_first_name')" name="fname" id="fname" value="{{ @$user->fname }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.last_name')</label>
                                        <input type="text" class="login_type required" placeholder="@lang('front_static.enter_your_last_name')" name="lname" id="lname" value="{{ @$user->lname }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.email_address')</label>
                                        <input type="text" class="login_type required" placeholder="@lang('front_static.enter_email_address')" name="email" id="email" value="{{ @$user->email }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="info-area guest">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>@lang('front_static.shipping_information')</h3>
                            </div>
                        </div>
                        <div class="new-adrs-area new1">
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.first_name')</label>
                                        <input type="text" class="login_type required shp_inp_msg" placeholder="@lang('front_static.enter_your_first_name')" name="shipping_fname" id="shipping_fname" value="{{ @$orderMaster->shipping_fname }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.last_name')</label>
                                        <input type="text" class="login_type required shp_inp_msg" placeholder="@lang('front_static.enter_your_last_name')" name="shipping_lname" id="shipping_lname" value="{{ @$orderMaster->shipping_lname }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.email_address')</label>
                                        <input type="text" class="login_type required shp_inp_msg" placeholder="@lang('front_static.enter_email_address')" name="shipping_email" id="shipping_email" value="{{ @$orderMaster->shipping_email }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.country')</label>
                                        <select class="login_type login-select required shp_inp_msg" name="shipping_country" id="shipping_country">
                                            <option value="">@lang('front_static.select_country')</option>
                                            @foreach($country as $cn)
                                            <option value="{{ $cn->id }}" @if(@$orderMaster->shipping_country == $cn->id) selected="" @endif  data-country_code="{{ @$cn->countryDetailsBylanguage->country_code }}">{{ @$cn->countryDetailsBylanguage->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.phone_number')</label>
                                        <span class="country_code" id="shipping_country_code">+</span><input  style="width: 80%; float: right;" type="text" class="login_type required shp_inp_msg" placeholder="@lang('front_static.enter_your_mobile_number')" name="shipping_phone" id="shipping_phone" value="{{ @$orderMaster->shipping_phone }}" onkeypress='validate(event)'>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4 col-md-6 col-sm-12" id="shipping_kuCity"  @if(@$orderMaster->shipping_country != 134) style="display: none;" @endif>
                                    <div class="one-main" style="position: relative;">
                                        <label class="from-label">@lang('front_static.city')</label>
                                        {{-- <select class="login_type login-select selectCh required" name="shipping_city_id" id="shipping_city_id" tabindex="3">
                                            <option value="">Select City</option>
                                            @foreach($city as $city)
                                            <option value="{{ $city->name }}" @if($city->name == @$orderMaster->shipping_city) selected="" @endif>{{ $city->name }}</option>
                                            @endforeach
                                        </select> --}}
                                        <input autocomplete="off" type="text" class="login-select login_type required shp_inp_msg" placeholder="@lang('front_static.enter_your_city_name')" name="shipping_city_id" id="shipping_city_id" @if(@$orderMaster->shipping_country == 134) value="{{ @$orderMaster->shipping_city }}" readonly="" @endif>
                                        <div id="shpCityList"></div>
                                        @if(@$orderMaster->shipping_country == 134)
                                        <span id="shpCityClr" class="city-clear"><i class="fa fa-times-circle"></i></span>
                                        @else
                                        <span id="shpCityClr" class="city-clear" style="display: none;"><i class="fa fa-times-circle"></i></span>
                                        @endif
                                        <span class="text-danger shipping_city_id_err"></span>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12" @if(@$orderMaster->shipping_country == 134) style="display: none;" @endif id="shipping_notKuCity">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.city')</label>
                                        <input type="text" class="login_type required shp_inp_msg" placeholder="@lang('front_static.enter_your_city_name')" name="shipping_city" id="shipping_city" @if(@$orderMaster->shipping_country != 134) value="{{ @$orderMaster->shipping_city }}" @endif>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.street')</label>
                                        <input type="text" class="login_type required shp_inp_msg" placeholder="@lang('front_static.enter_your_street_here')" name="shipping_street" id="shipping_street" value="{{ @$orderMaster->shipping_street }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12" id="shipping_postal_code_div" @if(@$orderMaster->shipping_country == 134) style="display: none;" @endif>
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.postal_code') (@lang('front_static.optional'))</label>
                                        <input type="text" class="login_type shp_inp_msg" placeholder="@lang('front_static.enter_postal_code')" name="shipping_postal_code" id="shipping_postal_code" value="{{ @$orderMaster->shipping_postal_code }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.block') <span class="shipping_optShow" @if(@$orderMaster->shipping_country == 134) style="display: none;" @endif> (@lang('front_static.optional'))</span></label>
                                        <input type="text" class="login_type  @if(@$orderMaster->shipping_country == 134) required @endif shp_inp_msg" placeholder="@lang('front_static.enter_your_block_here')" name="shipping_block" id="shipping_block" value="{{ @$orderMaster->shipping_block }}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.building') <span class="shipping_optShow" @if(@$orderMaster->shipping_country == 134) style="display: none;" @endif> (@lang('front_static.optional'))</span></label>
                                        <input type="text" class="login_type  @if(@$orderMaster->shipping_country == 134) required @endif shp_inp_msg" placeholder="@lang('front_static.enter_your_building_here')" name="shipping_building" id="shipping_building" value="{{ @$orderMaster->shipping_building }}">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">Enter URL or Text (@lang('front_static.optional'))</label>
                                        <input type="text" placeholder="Enter URL or Text" name="location" class="login_type" value="{{ @$orderMaster->location }}">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.more_address_details') (@lang('front_static.optional'))</label>
                                        <input type="text" class="login_type shp_inp_msg" placeholder="@lang('front_static.enter_your_more_address_details')" name="shipping_more_address" id="shipping_more_address" value="{{ @$orderMaster->shipping_more_address }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="info-area guest">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>@lang('front_static.billing_information')</h3>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group check_group logchk">
                                    <div class="checkbox-group"> 
                                        <input id="checkiz" type="checkbox" name="same_as_shipping" checked=""> 
                                        <label for="checkiz" class="Fs16">
                                            <span class="check find_chek"></span>
                                            <span class="box W25 boxx"></span>
                                            @lang('front_static.billing_address_same_as_shipping')
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row new2" style="display: none;">
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="one-main">
                                    <label class="from-label">@lang('front_static.first_name')</label>
                                    <input type="text" class="login_type required" placeholder="@lang('front_static.enter_your_first_name')" name="billing_fname" id="billing_fname" value="{{ @$orderMaster->billing_fname }}">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="one-main">
                                    <label class="from-label">@lang('front_static.last_name')</label>
                                    <input type="text" class="login_type required" placeholder="@lang('front_static.enter_your_last_name')" name="billing_lname" id="billing_lname" value="{{ @$orderMaster->billing_lname }}">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="one-main">
                                    <label class="from-label">@lang('front_static.email_address')</label>
                                    <input type="text" class="login_type required" placeholder="@lang('front_static.enter_email_address')" name="billing_email" id="billing_email" value="{{ @$orderMaster->billing_email }}">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="one-main">
                                    <label class="from-label">@lang('front_static.country')</label>
                                    <select class="login_type login-select required" name="billing_country" id="billing_country">
                                        <option value="">@lang('front_static.select_country')</option>
                                        @foreach($country as $cn)
                                        <option value="{{ $cn->id }}" @if(@$orderMaster->billing_country == $cn->id) selected="" @endif  data-country_code="{{ @$cn->countryDetailsBylanguage->country_code }}">{{ @$cn->countryDetailsBylanguage->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="one-main">
                                    <label class="from-label">@lang('front_static.phone_number')</label>
                                    <span class="country_code" id="billing_country_code">+</span><input style="width: 80%; float: right;" type="text" class="login_type" placeholder="@lang('front_static.enter_your_mobile_number')" name="billing_phone" id="billing_phone" value="{{ @$orderMaster->billing_phone }}" onkeypress='validate(event)'>
                                </div>
                            </div>
                            

                            <div class="col-lg-4 col-md-6 col-sm-12" id="billing_kuCity"  @if(@$orderMaster->billing_country != 134) style="display: none;" @endif>
                                <div class="one-main" style="position: relative;">
                                    <label class="from-label">@lang('front_static.city')</label>
                                    {{-- <select class="login_type login-select selectCh required" name="billing_city_id" id="billing_city_id" tabindex="3">
                                        <option value="">Select City</option>
                                        @foreach($bcity as $bcity)
                                        <option value="{{ $bcity->name }}" @if($bcity->name == @$orderMaster->billing_city) selected="" @endif>{{ $bcity->name }}</option>
                                        @endforeach
                                    </select> --}}
                                    <input type="text" class="login_type required" placeholder="@lang('front_static.enter_your_city_name')" name="billing_city_id" id="billing_city_id" @if(@$orderMaster->billing_country == 134) value="{{ @$orderMaster->billing_city }}"  readonly="" @endif>
                                    <div id="billCityList"></div>
                                    @if(@$orderMaster->billing_country == 134)
                                    <span id="billCityClr" class="city-clear"><i class="fa fa-times-circle"></i></span>
                                    @else
                                    <span id="billCityClr" class="city-clear" style="display: none;"><i class="fa fa-times-circle"></i></span>
                                    @endif
                                    <span class="text-danger billing_city_id_err"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12" @if(@$orderMaster->billing_country == 134) style="display: none;" @endif id="billing_notKuCity">
                                <div class="one-main">
                                    <label class="from-label">@lang('front_static.city')</label>
                                    <input type="text" class="login_type required" placeholder="@lang('front_static.enter_your_city_name')" name="billing_city" id="billing_city" @if(@$orderMaster->billing_country != 134) value="{{ @$orderMaster->billing_city }}" @endif>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="one-main">
                                    <label class="from-label">@lang('front_static.street')</label>
                                    <input type="text" class="login_type required" placeholder="@lang('front_static.enter_your_street_here')" name="billing_street" id="billing_street" value="{{ @$orderMaster->billing_street }}">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12" id="billing_postal_code_div" @if(@$orderMaster->billing_country == 134) style="display: none;" @endif>
                                <div class="one-main">
                                    <label class="from-label">@lang('front_static.postal_code') (@lang('front_static.optional'))</label>
                                    <input type="text" class="login_type" placeholder="@lang('front_static.enter_postal_code')" name="billing_postal_code" id="billing_postal_code" value="{{ @$orderMaster->billing_postal_code }}">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="one-main">
                                    <label class="from-label">@lang('front_static.block') <span class="billing_optShow" @if(@$orderMaster->billing_country == 134) style="display: none;" @endif> (@lang('front_static.optional'))</span></label>
                                    <input type="text" class="login_type  @if(@$orderMaster->billing_country == 134) required @endif" placeholder="@lang('front_static.enter_your_block_here')" name="billing_block" id="billing_block" value="{{ @$orderMaster->billing_block }}">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="one-main">
                                    <label class="from-label">@lang('front_static.building') <span class="billing_optShow" @if(@$orderMaster->billing_country == 134) style="display: none;" @endif> (@lang('front_static.optional'))</span></label>
                                    <input type="text" class="login_type @if(@$orderMaster->billing_country == 134) required @endif" placeholder="@lang('front_static.enter_your_building_here')" name="billing_building" id="billing_building" value="{{ @$orderMaster->billing_building }}">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="one-main">
                                    <label class="from-label">@lang('front_static.more_address_details') (@lang('front_static.optional'))</label>
                                    <input type="text" class="login_type" placeholder="@lang('front_static.enter_your_more_address_details')" name="billing_more_address" id="billing_more_address" value="{{ @$orderMaster->billing_more_address }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    @php
                        $sellerPayMthd = array();
                        foreach(@$cartDetails as $key => $mn) {
                            $sellerPayMthd[] = $mn->productMarchant->payment_mode;
                        }
                    @endphp
                    <div class="info-area">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>@lang('front_static.select_payment_method')</h3>
                            </div>
                            <div class="col-sm-12">
                                <div class="radio-custom" id="all_payment" @if((@$orderMaster->shipping_country && @$orderMaster->shipping_country != 134)  || (@$outsideMerchant == 'Y' )) style="display:none" @else style="display:block"  @endif >
                                    @if(in_array("C", $sellerPayMthd))
                                        <p>
                                            <input type="radio" id="test5" name="payment_method" checked="" value="C">
                                            <label for="test5">@lang('front_static.cash_on_delivery')</label>
                                        </p>
                                        @if(in_array("A", $sellerPayMthd) || in_array("O", $sellerPayMthd))
                                        <p>
                                            <input type="radio" id="test6" name="payment_method" value="O">
                                            <label for="test6">@lang('front_static.online_payment')</label>
                                        </p>
                                        @endif
                                        
                                    @elseif(in_array("O", $sellerPayMthd))
                                        <p>
                                            <input type="radio" id="test6" name="payment_method" value="O" checked="">
                                            <label for="test6">@lang('front_static.online_payment')</label>
                                        </p>
                                        @if(in_array("A", $sellerPayMthd) || in_array("C", $sellerPayMthd))
                                        <p>
                                            <input type="radio" id="test5" name="payment_method" value="C">
                                            <label for="test5">@lang('front_static.cash_on_delivery')</label>
                                        </p>
                                        @endif
                                        
                                    
                                    @elseif(in_array("A", $sellerPayMthd))
                                    <p>
                                        <input type="radio" id="test5" name="payment_method" checked="" value="C">
                                        <label for="test5">@lang('front_static.cash_on_delivery')</label>
                                    </p>
                                    <p>
                                        <input type="radio" id="test6" name="payment_method" value="O">
                                        <label for="test6">@lang('front_static.online_payment')</label>
                                    </p>
                                    @endif
                                </div>
                                <div class="radio-custom" id="outside_payment" @if((@$orderMaster->shipping_country && @$orderMaster->shipping_country != 134)  || (@$outsideMerchant == 'Y' )) style="display:block" @elseif(@$orderMaster->shipping_country == 134) style="display:none" @else style="display:none" @endif>
                                    <p>
                                        <input type="radio" checked="" id="test7" name="payment_method" value="O">
                                        <label for="test7">@lang('front_static.online_payment')</label>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="check-out-bttns chk-page">
                        {{-- <a href="javascript:void(0);" onclick="event.preventDefault(); document.getElementById('order-form').submit();">Continue </a> --}}
                        <button type="submit">@lang('front_static.continue')</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    @include('includes.new_footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{ asset('public/frontend/toastr/toastr.js') }}"></script>
<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
{{-- 
<script>
    $(document).ready(function() {
        $('.selectCh').chosen();
    })
</script> --}}

<script type="text/javascript">
    $(document).ready(function() {
        $('#order-form').validate({
            rules: {              
                email : {
                    required: true,
                    email:true
                },          
                shipping_email : {
                    required: true,
                    email:true
                },          
                billing_email : {
                    required: true,
                    email:true
                },        
                shipping_phone : {
                    required: true,
                    digits:true
                },      
                billing_phone : {
                    required: true,
                    digits:true
                },      
                shipping_postal_code : {
                    digits:true
                },      
                billing_postal_code : {
                    digits:true
                }
            },
            submitHandler: function (form) { 
                flag1 = 0;
                flag2 = 0;
                var cn = $('#shipping_country').val();
                var bcn = $('#billing_country').val();
                if(cn == 134 || bcn == 134) {
                    if(cn == 134) {
                        if($('#shipping_city_id').val()) {
                            var _token = $('input[name="_token"]').val();
                            $.ajax({
                                url:"{{ route('exist.city.check') }}",
                                method:"POST",
                                data:{city:$('#shipping_city_id').val(), _token:_token},
                                dataType:'json',
                                async : false,
                                success:function(response){
                                    if(response.status == 'ERROR') {
                                        $('.shipping_city_id_err').html('@lang('front_static.city_check')');
                                        flag1 = 1;                                    
                                    }
                                }
                            });
                        }
                    }

                    if(bcn == 134) {
                        if($('#billing_city_id').val()) {
                            var _token = $('input[name="_token"]').val();
                            $.ajax({
                                url:"{{ route('exist.city.check') }}",
                                method:"POST",
                                data:{city:$('#billing_city_id').val(), _token:_token},
                                dataType:'json',
                                async : false,
                                success:function(response){
                                    if(response.status == 'ERROR') {
                                        $('.billing_city_id_err').html('@lang('front_static.city_check')');
                                        flag2 = 1;                                    
                                    }

                                }
                            });
                        }
                    }
                    if(flag1 == 1) {
                        return false;
                    }
                    if(flag2 == 1) {
                        return false;
                    }
                }
                form.submit();
            },
            errorPlacement: function(error, element) {
             // error.appendTo('#errordiv');
         },
     });
        $('#shipping_city_id').change(function() {
            $('.shipping_city_id_err').html('');
        });
        $('#billing_city_id').change(function() {
            $('.billing_city_id_err').html('');
        });
    });
</script>
<script>
    $(document).ready(function() {
        (function($) {
            $.fn.spinner = function() {
                this.each(function() {
                    var el = $(this);

        // add elements
        el.wrap('<span class="spinner"></span>');     
        el.after('<span class="add">+</span>');
        el.before('<span class="sub">-</span>');

        // substract
        el.parent().on('click', '.sub', function () {
          if (el.val() > parseInt(el.attr('min')))
            el.val( function(i, oldval) { return --oldval; });
    });

        // increment
        el.parent().on('click', '.add', function () {
          if (el.val() < parseInt(el.attr('max')))
            el.val( function(i, oldval) { return ++oldval; });
    });
    });
            };
        })(jQuery);
        $('input[type=number]').spinner();
    });
    $(document).ready(function() {
        var outsideMerchant = "{{@$outsideMerchant}}";
        $('.ship_address').click(function(){
            if($(this).is(':checked'))
            {
                if($(this).val()==1)
                {
                    $('.new1').show();
                    $('.save1').hide();
                }
                if($(this).val()==2)
                {
                    $('.new1').hide();
                    $('.save1').show();
                }
            }
        });
        $('.bill_address').click(function(){
            if($(this).is(':checked'))
            {
                if($(this).val()==1)
                {
                    $('.new2').show();
                    $('.save2').hide();
                }
                if($(this).val()==2)
                {
                    $('.new2').hide();
                    $('.save2').show();
                }
            }
        });

        $('#checkiz').click(function() {
            if($(this).is(':checked')) {
                $('.new2').slideUp()
                var fname   = $('#shipping_fname').val();
                var lname   = $('#shipping_lname').val();
                var email   = $('#shipping_email').val();
                var phone   = $('#shipping_phone').val();
                var country = $('#shipping_country').val();
                var street  = $('#shipping_street').val();
                var block   = $('#shipping_block').val();
                var building= $('#shipping_building').val();
                var postal_code   = $('#shipping_postal_code').val();
                var more_address  = $('#shipping_more_address').val();

                var city    = $('#shipping_city').val();
                if(city == '') {
                    city = $('#shipping_city_id').val();
                }


                $('#billing_fname').val(fname);
                $('#billing_lname').val(lname);
                $('#billing_email').val(email);
                $('#billing_phone').val(phone);
                $('#billing_country').val(country);
                $('#billing_street').val(street);
                $('#billing_block').val(block);
                $('#billing_building').val(building);
                $('#billing_postal_code').val(postal_code);
                $('#billing_more_address').val(more_address);
                $('#billing_country_code').html($('#shipping_country').find(':selected').data('country_code'));
                if(country == 134) {
                    $('#billing_notKuCity').hide();
                    $('#billing_kuCity').show();
                    $('#billing_city_id').val(city);
                    $('#billing_city_id').attr("disabled", true);
                } else {
                    $('#billing_kuCity').hide();
                    $('#billing_notKuCity').show();
                    $('#billing_city').val(city);                    
                    $('#billing_city').attr("disabled", true);
                }

                $('#billing_fname').attr("disabled", true);
                $('#billing_lname').attr("disabled", true);
                $('#billing_email').attr("disabled", true);
                $('#billing_phone').attr("disabled", true);
                $('#billing_country').attr("disabled", true);
                $('#billing_street').attr("disabled", true);
                $('#billing_block').attr("disabled", true);
                $('#billing_building').attr("disabled", true);
                $('#billing_postal_code').attr("disabled", true);
                $('#billing_more_address').attr("disabled", true);

                if(country != 134) {
                    $('.billing_optShow').show('required');
                } else {
                    $('.billing_optShow').hide('required');                    
                }
            } else {
                $('.new2').slideDown()
                $('#billing_fname').val('');
                $('#billing_lname').val('');
                $('#billing_email').val('');
                $('#billing_phone').val('');
                $('#billing_country').val('');
                $('#billing_city').val('');
                $('#billing_city_id').val('');
                $('#billing_street').val('');
                $('#billing_block').val('');
                $('#billing_building').val('');
                $('#billing_postal_code').val('');
                $('#billing_more_address').val('');
                $('#billing_country_code').html('+');
                $('#billing_fname').attr("disabled", false);
                $('#billing_lname').attr("disabled", false);
                $('#billing_email').attr("disabled", false);
                $('#billing_phone').attr("disabled", false);
                $('#billing_country').attr("disabled", false);
                $('#billing_city').attr("disabled", false);
                $('#billing_city_id').attr("disabled", false);
                // $('#billing_city_id').attr("disabled", false).trigger("chosen:updated");
                $('#billing_street').attr("disabled", false);
                $('#billing_block').attr("disabled", false);
                $('#billing_building').attr("disabled", false);
                $('#billing_postal_code').attr("disabled", false);
                $('#billing_more_address').attr("disabled", false);
            }
        });

$('#shipping_country').change(function() {
    var cn = $(this).val();
    if(cn != 134) {
        $('#shipping_block').removeClass('required');
        $('#shipping_block').removeClass('error');
        $('#shipping_building').removeClass('required');
        $('#shipping_building').removeClass('error');
        $('.shipping_optShow').show('required');
        $('#shipping_postal_code_div').show();
        $('#shipping_kuCity').hide();
        $('#shipping_city_id').val('');
        $('#shipping_notKuCity').show();

        $('#outside_payment').show();
        $('#all_payment').hide();
    } else {
        $('#shipping_block').addClass('required');
        $('#shipping_building').addClass('required');
        $('.shipping_optShow').hide('required');
        $('#shipping_postal_code_div').hide(); 
        $('#shipping_kuCity').show();
        $('#shipping_notKuCity').hide();              
        $('#shipping_city').val('');  
        if(outsideMerchant == 'Y'){
            $('#outside_payment').show();
            $('#all_payment').hide();
        }else{
            $('#outside_payment').hide();
            $('#all_payment').show(); 
        }                
    }
    $('#shipping_country_code').html($(this).find(':selected').data('country_code'));
});

$('#billing_country').change(function() {
    var cn = $(this).val();
    if(cn != 134) {
        $('#billing_block').removeClass('required');
        $('#billing_block').removeClass('error');
        $('#billing_building').removeClass('required');
        $('#billing_building').removeClass('error');
        $('.billing_optShow').show('required');
        $('#billing_postal_code_div').show();
        $('#billing_kuCity').hide();
        $('#billing_city_id').val('');              
        $('#billing_notKuCity').show();
    } else {
        $('#billing_block').addClass('required');
        $('#billing_building').addClass('required');
        $('.billing_optShow').hide('required');
        $('#billing_postal_code_div').hide(); 
        $('#billing_kuCity').show();
        $('#billing_notKuCity').hide();              
        $('#billing_city').val('');                  
    }
    $('#billing_country_code').html($(this).find(':selected').data('country_code'));
});
});

function validate(evt) {
    var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script> 

{{-- city autocomplete --}}

<script>
    $(document).ready(function(){

        window.addEventListener( "pageshow", function ( event ) {
            var historyTraversal = event.persisted || 
                                    ( typeof window.performance != "undefined" && 
                                        window.performance.navigation.type === 2 );
            if ( historyTraversal ) {
                // Handle page restore.
                window.location.reload();
            }
        });
        var flag = 0;
        $('#shipping_city_id').keyup(function(){ 
            $('.shipping_city_id_err').html('');
            var city = $(this).val();
            if(city != '') {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('autocomplete.city') }}",
                    method:"POST",
                    data:{city:city, _token:_token},
                    success:function(response){
                        if(response.status == 'ERROR') {
                            $('#shpCityList').html('nothing found');
                        } else {
                            $('#shpCityList').fadeIn();  
                            $('#shpCityList').html(response.result);
                            flag = 1;
                        }
                    }
                });
            }
        });

        $('#billing_city_id').keyup(function(){ 
            $('.billing_city_id_err').html('');
            var city = $(this).val();
            if(city != '') {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('autocomplete.city') }}",
                    method:"POST",
                    data:{city:city, _token:_token},
                    success:function(response){
                        if(response.status == 'ERROR') {
                            $('#billCityList').html('nothing found');
                        } else {
                            $('#billCityList').fadeIn();  
                            $('#billCityList').html(response.result);
                            flag = 2;
                        }
                    }
                });
            }
        });

        $('body').on('click', 'li', function(){ 
            if(flag == 1) {
                $('#shipping_city_id').val($(this).text()); 
                $('#shipping_city_id').prop('readonly', true);
                $('#shpCityList').hide();   
                $('#shpCityClr').show();
            };
            if(flag == 2) {
                $('#billing_city_id').val($(this).text());
                $('#billing_city_id').prop('readonly', true);
                $('#billCityList').hide();     
                $('#billCityClr').show();
            };
        });   
        $("body").click(function(){
            $("#shpCityList").hide();
            $("#billCityList").hide();
        });

        $('body').on('click', '#shpCityClr', function(){ 
            $(this).hide();
            $('#shipping_city_id').val('');
            $('#shipping_city_id').prop('readonly', false);
        });  

        $('body').on('click', '#billCityClr', function(){ 
            $(this).hide();
            $('#billing_city_id').val('');
            $('#billing_city_id').prop('readonly', false);
        });  

    });
</script>

<script>
function initMap() {
    var input = document.getElementById('pac-input');
    var autocomplete = new google.maps.places.Autocomplete(input);
    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    autocomplete.setTypes(['address']);

    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
      $('#lat').val(place.geometry.location.lat())
      $('#lng').val(place.geometry.location.lng())
    });
}
$(document).ready(function() {
    $('#pac-input').blur(function() {
        if($(this).val() == '') {
            $('#lat').val('')
            $('#lng').val('')
        }
    })
})
</script>

<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API') }}&libraries=places&callback=initMap" async defer></script>
@endsection