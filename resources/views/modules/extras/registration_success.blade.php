@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | Registration Success
@endsection
@section('links')
@include('includes.links')
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="banner">
        <div class="sgnupbnr">
            <div class="container">
                <div class="login_body">
                    <div class="login_sec" style="min-height: auto;">
                        <h3>Registration successful!</h3>
                        <p>You have successfully registered your account! Please verify your email to access your account.</p>
                        <button type="submit" class="login_submit" onclick="location.href='{{ route('login') }}'">Login</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('includes.new_footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
@endsection