@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | Mail Updated Success
@endsection
@section('links')
@include('includes.links')
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="banner">
        <div class="sgnupbnr">
            <div class="container">
                <div class="login_body">
                    <div class="login_sec" style="min-height: auto;">
                        <h3>Verification successful!</h3>
                        <p>You have successfully updated your email address.Now you can access your account using updated email address.</p>
                        @auth
                        @else
                            <button type="submit" class="login_submit" onclick="location.href='{{ route('login') }}'">Login</button>
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('includes.new_footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
@endsection