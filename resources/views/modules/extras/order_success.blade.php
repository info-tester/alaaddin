@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | Order Success
@endsection
@section('links')
@include('includes.links')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />
<style>
.order_h{
    text-align: center !important;
    margin-top: 20px;
} 
.order_p {
    text-align:justify !important;
}
.order_i{
    text-align: center !important;
    background: #2486e1;
    padding: 5px;
    border-radius: 50%;
    color: #ffffff;
    font-size: 50px;
}
.login_sec{
    text-align: center;
}
.login_body p a {
    color: #007bff !important;
}
.guest_success i{
    position: absolute;
    right: 13px;
    top: 27px;
    font-size: 19px;
    cursor: pointer;
}
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="banner">
        <div class="sgnupbnr">
            <div class="container">
                <div class="login_body">
                    <div class="login_sec" style="min-height: auto;">
                        <i class="order_i fa fa-check" aria-hidden="true"></i>
                        <h3 class="order_h">Order Placed!</h3>
                        <p class="order_p">Thank you for your purchase! Your order has now been received, and you will shortly receive a confirmation email containing details of your order. If you have any questions, please contact us and we will be happy to assist you.Your Order Number : {{ @$order->order_no }}</p>
                        @if($order->customerDetails->user_type == 'G')
                        <div class="guest_success">
                            <p class="alert alert-warning">You can track your order at any time by clicking the below link.</p>
                            <p class="alert alert-primary">
                               
                                <a id="track_link" href="{{route('guest.order.details', ['trackId' => encrypt(@$order->order_no)])}}">{{route('guest.order.details', ['trackId' => encrypt(@$order->order_no)])}}</a>
                                <i class="fa fa-clone" id="copy_link" aria-hidden="true" title="Copy link" data-toggle="tooltip"></i>
                            </p>
                        </div>

                        <button type="submit" class="login_submit" onclick="location.href='{{route('guest.order.details', ['trackId' => encrypt(@$order->order_no)])}}'">View Details</button>
                        @else                        
                        <button type="submit" class="login_submit" onclick="location.href='{{ route('order.history.order.details',$order->order_no) }}'">View Details</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('includes.new_footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha256-3blsJd4Hli/7wCQ+bmgXfOdK7p/ZUMtPXY08jmxSSgk=" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#copy_link').click(function() {
            CopyToClipboard('track_link')
        })
    })
    function CopyToClipboard(containerid) {
        if (window.getSelection) {
            if (window.getSelection().empty) { // Chrome
                window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) { // Firefox
                window.getSelection().removeAllRanges();
            }
        } else if (document.selection) { // IE?
            document.selection.empty();
        }

        if (document.selection) {
            var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById(containerid));
            range.select().createTextRange();
            document.execCommand("copy");
        } else if (window.getSelection) {
            var range = document.createRange();
            range.selectNode(document.getElementById(containerid));
            window.getSelection().addRange(range);
            document.execCommand("copy");
        }
        toastr.success('Link copied!');
    }
</script>
@endsection