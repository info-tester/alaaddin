@extends('layouts.app')

@section('title')
{{ $product->productByLanguage->title }}
@endsection

@section('links')
@include('includes.links')
<link href="{{ asset('public/frontend/css/ninja-slider.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('public/frontend/css/thumbnail-slider.css') }}" rel="stylesheet" type="text/css">
{{-- <link rel="stylesheet" href="{{ asset('public/css/app.css') }}"> --}}
<link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
<link href="{{ asset('public/frontend/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .rate_show{
        border-radius: 8px;
        color: #fff;
        text-align: center;
        border: 1px solid white;
        line-height: normal;
        display: inline-block;
        padding: 2px 4px 2px 6px;
        font-weight: 500;
        font-size: 12px;
        vertical-align: middle;
        background-color: #388e3c;border-radius: 5px;font-weight: bold;
    }
    
</style>
@endsection

@section('meta')
    <meta property="og:url"                content="{{ url('') }}" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="Aswagna" />
    <meta property="og:description"        content="Aswagna is an ecommerce website and apps where you can buy from different sellers in one checkout." />
    @if(@$product->images->isNotEmpty())
    <meta property="og:image"              content="{{ url('storage/app/public/products/' . $product->images[0]->image) }}" />
    @else
    <meta property="og:image"              content="{{ url('public/frontend/images/default_product.png') }}" />
    @endif
    
@endsection

@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="pro-detls-body">
        <div class="container">
            <div class="bed-cumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('admin_lang.home')</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('search', @$product->productParentCategory->Category->slug ) }}">{{ @$product->productParentCategory->Category->categoryByLanguage->title }}</a>
                    </li>
                    <li class="breadcrumb-item active">{{ @$product->productSubCategory->Category->categoryByLanguage->title }}</li>
                </ol>
            </div>
            <div class="main-dtls">
                <div class="pro_slider">
                    <div class="ninja_trail_remove">
                        <div class="left_slider_area">
                            <div id="thumbnail-slider" style="float:left;">
                                <div class="inner">
                                    <ul>
                                        @if(@$product->images->isNotEmpty())
                                        @foreach($product->images as $row)
                                        <li><a class="thumb" href="{{ url('storage/app/public/products/80/' . $row->image) }}"></a></li>
                                        @endforeach
                                        @else
                                        <li><a class="thumb" href="{{ getDefaultImageUrl() }}"></a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div id="ninja-slider" style="float:left;">
                                <div class="slider-inner">
                                    <ul>
                                        @if(@$product->images->isNotEmpty())
                                        @foreach($product->images as $row)
                                        <li><a class="ns-img" href="{{ url('storage/app/public/products/' . $row->image) }}"></a></li>
                                        @endforeach
                                        @else
                                        <li><a class="ns-img" href="{{ getDefaultImageUrl() }}"></a></li>
                                        @endif
                                    </ul>
                                    @auth
                                    <a class="fav-span removeAddS fav-spans{{ @$product->id }}" @if(@$product->wishlist) style="color: #d90000" @endif href="javascript:void(0);" data-id="{{ @$product->id }}"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                    @else
                                    <a class="fav-span" href="{{ route('login') }}"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                    @endauth
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <product-component :slug="'{{ $slug }}'"></product-component>
            </div>
            <div class="about-company dtls-company">
                <div class="left-about1">
                    <h3>@lang('front_static.product_desc')</h3>
                    <p>{!! @$product->productByLanguage->description !!}</p>

                    @if(@$product_features->isNotEmpty())
                    <h3>@lang('front_static.product_features')</h3>
                    @foreach($product_features as $key=>$value)
                    <p>
                        <strong>
                            {{ @$value->productVariantByLanguage->variantByLanguage->name }} 
                        </strong> 
                        @php
                        $variantValue = [];
                        @endphp
                        @foreach($value->productVariantDetails as $key1=>$value1)

                        @php 
                        array_push($variantValue, $value1->variantValueByLanguage->name);
                        @endphp 
                        @endforeach
                        {{ implode(', ', $variantValue) }}
                    </p>
                    @endforeach
                    @endif

                    @if(@$product->avg_review != 0.000)
                    <h3>Ratings & Reviews</h3>
                    @if(@$product->avg_review == 5.000)
                    <ul class="review_customer">
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                    </ul>
                    @elseif(@$product->avg_review >=4.000)
                    <ul class="review_customer">
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/nostar.png') }}" alt=""></a></li>
                    </ul>
                    @elseif(@$product->avg_review >=3.000)
                    <ul class="review_customer">
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/nostar.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                    </ul>
                    @elseif(@$product->avg_review >=2.000)
                    <ul class="review_customer">
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/nostar.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                    </ul>
                    @elseif(@$product->avg_review >=1.000)
                    <ul class="review_customer"><li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/nostar.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                    </ul>
                    @elseif(@$product->avg_review == 1.000)
                    <ul class="review_customer"><li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                        <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                    </ul>
                    @endif 
                    {{-- @dd($reviews_product) --}}
                    <p>{{ @$product->total_no_reviews }} @lang('admin_lang.reviews_ratings')</p>
                    <div class="review_section">
                        @if(@$reviews_product)
                            @foreach(@$reviews_product as $review)
                            <div class="reviews">
                                <label class="badge @if(@$review->rate >=4) badge-success @elseif(@$review->rate >= 2)badge-warning @elseif(@$review->rate ==1) badge-danger @endif">{{ @$review->rate }} <i class="fa fa-fw fa-star"></i></label>
                                <span class="rvw_cmt">{{ @$review->order_no }} <br/>{{ @$review->comment }}<br/></span>
                                <div class="reviewr">{{ @$review->customer_name }} -- <br/>@lang('admin_lang.re_post_on') : {{ @$review->review_date }} </div>
                            </div>
                            @endforeach
                        @endif
                    </div>
                    @endif
                </div>
            </div>
            <div class="received-review dtls-rev">
                <h2>@lang('front_static.seller_information')</h2>
                <div class="all-reviews">
                    <div class="seller-image">
                        <span>
                            @if(@$product->productMarchant->image)
                            <img src="{{ url('storage/app/public/profile_pics/' . $product->productMarchant->image) }}" alt="">
                            @else
                            <img src="{{ getDefaultImageUrl() }}" alt="">
                            @endif
                        </span>
                    </div>
                    <div class="seller-dtls">
                        <h3>{{ $product->productMarchant->fname .' '.$product->productMarchant->lname }}</h3>
                        {{-- <p>Kolkata, India</p> --}}
                        <p><strong>@lang('front_static.company') : </strong>{{ $product->productMarchant->company_name }}</p>
                        <p>
                            <strong>Selling</strong>
                            <ul class="selling-ul">
                                @foreach($product_cat as $proCat)
                                <li><a href="{{ route('search', $proCat->Category->slug) }}">{{ $proCat->Category->categoryByLanguage->title }}</a></li>
                                @endforeach
                            </ul>
                        </p>
                    </div>
                    <div class="seller-rat">
                        <ul>
                            @if(@$product->productMarchant->rate == 5)
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <!-- <li><a href="#"><img src="{{ url('public/sfrontend/images/nostar.png') }}" alt=""></a></li> -->
                            @elseif(@$product->productMarchant->rate >=4)
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/nostar.png') }}" alt=""></a></li>
                            @elseif(@$product->productMarchant->rate >=3)
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/nostar.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                            @elseif(@$product->productMarchant->rate >=2)
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/nostar.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                            @elseif(@$product->productMarchant->rate >=1)
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/nostar.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                            @elseif(@$product->productMarchant->rate ==1)
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/star3.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="{{ url('public/frontend/images/blank-star.png') }}" alt=""></a></li>
                            @endif
                            <li>({{ @$product->productMarchant->total_no_review }} @lang('admin_lang.reviews_ratings'))</li>
                        </ul>
                        <p><img src="{{ url('public/frontend/images/sell1.png') }} " alt=""> @lang('front_static.seller_since') : {{ date('jS M Y', strtotime($product->productMarchant->created_at)) }}</p>
                        <a class="view-sell" href="{{ route('merchant.profile', $product->productMarchant->slug) }}">@lang('front_static.view_seller_profile')</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if(@$seller_products->isNotEmpty())
    <section class="best-seller" id="faq_3">
        <div class="container">
            <div class="section-header">
                <h2><span>@lang('front_static.products_of_this_seller')</span></h2>
                <span class="section-header-line"></span>
            </div>
            <div class="main-deals">
                <div id="owl-demo-2" class="owl-carousel">
                    {{-- @dd($seller_products) --}}
                    @foreach($seller_products as $row)
                    <div class="item">
                        <div class="product-box">
                            <div class="product-image">
                                <a href="{{ route('product', $row->slug) }}">
                                    @if(@$row->defaultImage->image)
                                    <img src="{{ url('storage/app/public/products/300/' . @$row->defaultImage->image) }}" alt="">
                                    @else
                                    <img src="{{ getDefaultImageUrl() }}" alt="">
                                    @endif
                                </a>
                                @if($row->discount_price != 0 && date('Y-m-d')>=@$row->from_date && date('Y-m-d')<=@$row->to_date)
                                <span class="new-sale">{{ number_format((($row->price - $row->discount_price)/$row->discount_price) * 100, 3) }}% OFF</span>
                                @endif
                                @auth
                                <a class="fav-span removeAdd fav-span{{ @$row->id }}" @if(@$row->wishlist) style="color: #d90000" @endif href="javascript:void(0);" data-id="{{ @$row->id }}"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                @else
                                <a class="fav-span" href="{{ route('login') }}"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                @endauth
                            </div>
                            <div class="product-dtls">
                                <h3>@lang('front_static.sold_by') : {{ $row->productMarchant->fname .' '.$row->productMarchant->lname }}</h3>

                                <h4>
                                    <a href="{{ route('product', $row->slug) }}">{{ @$row->productByLanguage->title }} <br/>
                                        @if(@$row->avg_review > 0.000)
                                        <div class="rate_show">{{ @$row->avg_review }} <i class="fa fa-fw fa-star"></i>
                                        </div>
                                        @endif
                                    </a>
                                </h4>

                                @if($row->discount_price != 0 && date('Y-m-d')>=@$row->from_date && date('Y-m-d')<=@$row->to_date)
                                <p><strong>{{ $row->price }} {{ getCurrency() }} </strong> - <span>{{ $row->discount_price }} {{ getCurrency() }}</span></p>
                                @else
                                <p><span>{{ $row->price }} {{ getCurrency() }}</span></p>
                                {{-- @if(@$row->total_no_reviews != 0)<p><span>{{ @$row->total_no_reviews }}</span></p>@endif --}}
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @endif
    @include('includes.footer')
</div>

@endsection

@section('scripts')
<script src="{{ asset('public/frontend/js/site_lang-'.Config::get('app.locale').'.js') }}"></script>
<script src="{{ asset('public/js/app.js') }}"></script>
<script src="{{ asset('public/frontend/js/ninja-slider.js') }}"></script> 
<script src="{{ asset('public/frontend/js/thumbnail-slider.js') }}"></script>
@include('includes.scripts')
<script src="{{ asset('public/frontend/toastr/toastr.js') }}"></script>


{{-- <script src="{{ asset('firebase-messaging-sw.js') }}"></script> --}}
<script>
    $(document).ready(function() {
    /*var nslider = new NinjaSlider(nsOptions)
    var mcThumbnailSlider = new ThumbnailSlider(thumbnailSliderOptions)*/

    /*setTimeout(function(){ 
        nslider.init();
        mcThumbnailSlider.init()
    }, 2000);*/
	/*(function($) {
        $.fn.spinner = function() {
          this.each(function() {
            var el = $(this);

            // add elements
            el.wrap('<span class="spinner"></span>');     
            el.after('<span class="add">+</span>');
            el.before('<span class="sub">-</span>');

            // substract
            el.parent().on('click', '.sub', function () {
              if (el.val() > parseInt(el.attr('min')))
                el.val( function(i, oldval) { return --oldval; });
            });

            // increment
            el.parent().on('click', '.add', function () {
              if (el.val() < parseInt(el.attr('max')))
                el.val( function(i, oldval) { return ++oldval; });
            });
            });
        };
    })(jQuery);
    $('input[type=number]').spinner();*/
    });
</script> 

<script>
    $(document).ready(function(){
        $(".removeAdd").click(function(){
            var add_id = $(this).data('id');
            var reqDataa = {
                jsonrpc: '2.0'
            };

            $.ajax({
                type:'GET',
                url: '{{ url('add-to-fevourite') }}/'+add_id,
                data:reqDataa,
                success:function(response){
                    if(response.sucess) {
                        if(response.sucess.result == 'Add') {
                            $(".fav-span"+add_id).css('color','#d90000');
                            toastr.success("@lang('admin_lang.pro_suc_added_to_wishlist')");
                        }
                        if(response.sucess.result == 'Remove') {   
                            $(".fav-span"+add_id).css('color','#ececec');
                            toastr.success('Product successfully removed from wishlist.');
                        }
                    } else {
                        console.log(response.error.message);
                    }
                }
            });
        }); 
    }); 

</script>
<script type="text/javascript" src="public/js/jquery.slimscroll.js"></script>
<script>
    $(document).ready(function(){
        $(".removeAddS").click(function(){
            var add_id = $(this).data('id');
            var reqDataa = {
                jsonrpc: '2.0'
            };

            $.ajax({
                type:'GET',
                url: '{{ url('add-to-fevourite') }}/'+add_id,
                data: reqDataa,
                success:function(response){
                    if(response.sucess) {
                        if(response.sucess.result == 'Add') {
                            $(".fav-spans"+add_id).css('color','#d90000');
                            toastr.success('Product successfully added to wishlist.');
                        }
                        if(response.sucess.result == 'Remove') {   
                            $(".fav-spans"+add_id).css('color','#ececec');
                            toastr.success('Product successfully removed from wishlist.');
                        }
                    } else {
                        console.log(response.error.message);
                    }
                }
            });
        }); 
    }); 
    
    $('.review_section').slimscroll({
        height: '428px'
    });
</script>
@endsection