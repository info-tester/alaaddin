@extends('layouts.app')

@section('title')
{{ $merchant->company_name }}
@endsection

@section('links')
@include('includes.links')
<link href="{{ asset('public/frontend/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!--wrapper start-->
<div class="wrapper">

	@include('includes.header')

	<section class="merchant-banner" style="background:#ccc url(storage/app/public/cover_pic/{{ @$merchant->cover_pic }}) no-repeat left center;">
	</section>

	<section class="merchnt-body">
		<div class="container">
			<div class="merchant-profile">
				<div class="toottrip-area">
					<ul>
						<li><a class="faqs" href="javascript:void(0);" data-id="1" ><i class="fa fa-user" aria-hidden="true"></i></a>
							<div class="open-tool">@lang('front_static.about_me')</div>
						</li>
						<li><a class="faqs" href="javascript:void(0);" data-id="2" ><i class="fa fa-star" aria-hidden="true"></i></a>
							<div class="open-tool">@lang('front_static.reviews')</div>
						</li>
						<li><a class="faqs" href="javascript:void(0);" data-id="3" ><i class="fa fa-th-large" aria-hidden="true"></i></a>
							<div class="open-tool">@lang('front_static.products')</div>
						</li>
					</ul>
				</div>
				<div class="pro-merchant">
					@if(@$merchant->image)
					<span><img src="{{url('storage/app/public/profile_pics/'.@$merchant->image)}}" alt=""></span>
					@else
					<span><img src="{{ asset('public/admin/assets/images/person_icon.png') }}" alt=""></span>
					@endif
				</div>
				<div class="right-merchant">
					<div class="merchant-dtls">
						<h2>{{ @$merchant->company_name }}</h2>
						<h3><img src="{{ asset('public/frontend/images/location.png') }}" alt=""> {{ @$merchant->Country->name }}</h3>
						<p>(@if(@$merchant->total_no_review != 0) {{ @$merchant->total_no_review }} @else No @endif 

						@lang('front_static.reviews'))</p><br/>
						<p>@if(@$merchant->rate != 0){{ @$merchant->rate }}@endif</p>
						@if(@$merchant->rate != 0.000)
						@if(@$merchant->rate > 4.000)
						<ul>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
						</ul>
						@elseif(@$merchant->rate == 4.000)
                        <ul>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
						</ul>
                        @elseif(@$merchant->rate >3.000)
                        <ul>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star4.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
						</ul>
                        @elseif(@$merchant->rate >2.000)
                        <ul>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star4.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
						</ul> 
                        @elseif(@$merchant->rate >1.000)
                        <ul>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star4.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
						</ul>
                        @elseif(@$merchant->rate == 1.000)
                        <ul>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star3.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
						</ul>
						@elseif(@$merchant->rate < 1.000)
                        <ul>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/star4.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
							<li><a href="javascript:void(0)"><img src="{{ asset('public/frontend/images/blank-star.png') }}" alt=""></a></li>
						</ul>
                        @endif
						
						@endif
					</div>
					<div class="merchant-share">
						<ul>
							<li>@lang('front_static.share') : </li>
							<!-- <li><a href="#"><img src="{{ asset('public/frontend/images/social1.png') }}" alt=""></a></li>
							<li><a href="#"><img src="{{ asset('public/frontend/images/social2.png') }}" alt=""></a></li>
							<li><a href="#"><img src="{{ asset('public/frontend/images/social3.png') }}" alt=""></a></li> -->
							<div class="sharethis-inline-share-buttons"></div>
						</ul>
					</div>
					<div class="merchant-status">
						<ul>
							@if(@$merchant->show_order_processed == 'Y')
							<li>
								<h4>@lang('front_static.order_processed')</h4>
								<h5>{{ @$order->count() }}</h5>
							</li>
							@endif
							<li>
								<h4>@lang('front_static.no_of_products')</h4>
								<h5>{{ $total_products }}</h5>
							</li>
							<li>
								<h4>@lang('front_static.since')</h4>
								<h5>{{ date('jS M Y', strtotime(@$merchant->created_at->toDateString())) }}</h5>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="about-company" id="faq_1">
				<div class="left-about">
					<h3>@lang('front_static.about_company')</h3>
					<p>{!! substr(@$merchant->merchantCompanyDetailsByLanguage->description, 0,414) !!}<span id="showText" style="display: none;">{!! substr(@$merchant->merchantCompanyDetailsByLanguage->description, 414) !!}</span></p>
					@if(strlen(@$merchant->merchantCompanyDetailsByLanguage->description)>414)
					<a href="javascript:void(0);" id="hdSwBtn">Read More</a>
					@endif
				</div>
				<div class="right-about">
					<p><strong>@lang('front_static.location') </strong> <span>{{ @$merchant->Country->name }}</span></p>
					<!-- <p><strong>Company Type</strong> <span>Manufactory</span></p> -->
					@if($product_cat)
					<p><strong>@lang('front_static.selling')</strong> </p>
					<ul>
						@foreach($product_cat as $proCat)
						<li><a href="{{ route('search', $proCat->Category->slug) }}">{{ $proCat->Category->categoryByLanguage->title }}</a></li>
						@endforeach
					</ul>
					@endif
				</div>
			</div>

			<!-- <div class="about-company" id="faq_1">
					<h3>Portfolio Images</h3>
				<div class="portfolio-img">
					@if($merchant->merchantImage)
						@foreach($merchant->merchantImage as $mrIm)
						<img src="{{ asset('public/frontend/images/rev-man1.png') }}" alt="">
						@endforeach
					@endif
				</div>
			</div> -->
			<!-- start -->
			<div class="port" id="faq_3">
				<h2>Portfolio Images</h2>
				<div class="all-reviews" style="margin-bottom: 25px;">
					<div class="row">
						@if(count($merchant->merchantImage) > 0)
						@foreach($merchant->merchantImage as $mrIm)
						<div class="col-lg-4 col-md-4 col-sm-12">
							<div class="portfolio-img">
								<img src="{{url('storage/app/public/merchant_portfolio/'.@$mrIm->image)}}" alt="">
							</div>
						</div>
						@endforeach
						@endif
					</div>
				</div>
			</div>
			<!-- end -->
			@if(@$order)
			<div class="received-review" id="faq_2">
				<h2>@lang('admin_lang.received_reviews')</h2>
				<div class="all-reviews">
					<div class="row">
						@if(@$order)
							@foreach(@$order as $ord)
								@if($ord->orderMasterDetails)
									@foreach($ord->orderMasterDetails as $dtls)
									<div class="col-lg-6 col-md-12">
										<div class="review-box">
											<div class="review-box-head">
												<span>
													@if(@$ord->user_id != 0)
														@if(@$ord->customerDetails)
															@if(@$ord->customerDetails->image)
															<img src="{{ url('storage/app/public/customer/profile_pics/'.@$ord->customerDetails->image) }}" alt="">
															@else
															<img src="{{ asset('public/admin/assets/images/person_icon.png') }}" alt="">
															@endif
														@endif
													@else
														<img src="{{ asset('public/admin/assets/images/person_icon.png') }}" alt="">
													@endif
												</span>
												<h4>
													{{ @$ord->shipping_fname }} {{ @$ord->shipping_lname }}
												</h4>
												@if(@$dtls->rate == 5)
												<ul>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
												</ul>
												@elseif(@$dtls->rate == 4)
												<ul>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star2.png') }}" alt=""></a></li>
												</ul>
												@elseif(@$dtls->rate == 3)
												<ul>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star2.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star2.png') }}" alt=""></a></li>
												</ul>
												@elseif(@$dtls->rate == 2)
												<ul>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star2.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star2.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star2.png') }}" alt=""></a></li>
												</ul>
												@elseif(@$dtls->rate == 1)
												<ul>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star1.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star2.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star2.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star2.png') }}" alt=""></a></li>
													<li><a href="javascript:void(0);"><img src="{{ asset('public/frontend/images/star2.png') }}" alt=""></a></li>
												</ul>
												@endif
												<p>
													<!-- <img src="{{ asset('public/frontend/images/.png') }}" alt="">  -->
													{{ @$dtls->updated_at }}
												</p>
											</div>
											<!-- <h5>Review Comment</h5> -->
											<p>{{@$dtls->comment}}</p>
										</div>
									</div>
									@endforeach
								@endif
							@endforeach
						@endif
					</div>
				</div>
			</div>
			@endif
		</div>
	</section>

	<section class="best-seller" id="faq_3">
		@if(count($product) > 0)
		<div class="container">
			<div class="section-header">
				<h2><span>@lang('front_static.products_of_this_seller')</span></h2>
				<span class="section-header-line"></span>
			</div>
			<div class="main-deals">
				<div id="owl-demo-2" class="owl-carousel">
					@foreach($product as $newPro)
					@php
						$today = date('Y-m-d');
					@endphp
					<div class="item">
						<div class="product-box">
							<div class="product-image">
								<a href="{{ route('product',@$newPro->slug) }}">
									@if(@$newPro->defaultImage->image)
									<img src="{{url('storage/app/public/products/'.@$newPro->defaultImage->image)}}" alt="">
									@else
									<img src="public/frontend/images/featured2.png" alt="">
									@endif
								</a>
								@if(@$newPro->discount_price != '0.00' && ($today >= @$newPro->from_date  && $today <= @$newPro->to_date))
								<span class="new-sale">{{ round(((@$newPro->price - @$newPro->discount_price)/@$newPro->price) * 100) }}% @lang('front_static.off')</span>
								@endif
								@auth
								<a class="fav-span removeAdd fav-span{{ @$newPro->id }}" @if(@$newPro->wishlist) style="color: #2486e1" @endif href="javascript:void(0);" data-id="{{ @$newPro->id }}"><i class="fa fa-heart" aria-hidden="true"></i></a>
								@else
								<a class="fav-span" href="{{ route('login') }}"><i class="fa fa-heart" aria-hidden="true"></i></a>
								@endauth
							</div>
							<div class="product-dtls">
								<h3>@lang('front_static.sold_by') : {{ @$newPro->productMarchant->fname }} {{ @$newPro->productMarchant->lname }}</h3>
								<h4><a href="{{ route('product',@$newPro->slug) }}">{{ @$newPro->productByLanguage->title }}</a></h4>
								<p>
									
									@if(@$newPro->discount_price != '0.00' && ($today >= @$newPro->from_date  && $today <= @$newPro->to_date))
									<strong>{{ @$newPro->price }} {{ getCurrency() }} </strong>  -  <span>{{ @$newPro->discount_price }} {{ getCurrency() }}</span>
									@else
									<span>{{ @$newPro->price }} {{ getCurrency() }}</span>
									@endif
								</p>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
		@endif
	</section>

	@include('includes.footer')

	@endsection

	@section('scripts')
	@include('includes.scripts')

	<script src="{{ asset('public/frontend/toastr/toastr.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			/* Menu Start */
			$('#sm').mouseover(function(){
				$(this).show();
			});
			$('#sm').mouseout(function(){
				$(this).hide();
			});

			$('#m1').mouseover(function(){
				$('.all_menu').show();
				$('').hide();
				$('#sm').show();
				$('.all_menu').pinterest_grid({
					no_columns: 4,
					padding_x: 10,
					padding_y: 10,
					margin_bottom: 50,
					single_column_breakpoint: 700
				});
			});


			$('#m1, #m2 ').mouseleave(function(){
				$('#sm').hide();
				$('#sm1, #sm2').hide();
			});
			/* Menu Start */

			$('#paste-lan').click(function(){
				$('.open-lan').toggle();
			/*var lang = '';// $('.open-lan').children('li').children('a').data('id');
			//alert(lang);
			if(lang == 'english') {
				$('#paste-lan').html('<img src="images/english-flag1.png" alt="">')
				$('.open-lan').css('display', 'none');
			} else {
				if(lang == 'arabic') {
					$('#paste-lan').html('src="images/arabic-flag1.png"')
					$('.open-lan').css('display', 'none');
				} else {
					$('.open-lan').css('display', 'none');
				}
			}*/
		});
			$('.open-lan').find('a').click(function() {
				var lang = $(this).data("id");
				if(lang == 'english') {
					$('#paste-lan').html('<img src="images/english-flag1.png" alt="">');
				} else {
					$('#paste-lan').html('<img src="images/arabic-flag1.png" alt="">');
				}
				$('.open-lan').hide();
			});
		});
	</script>

	<script>

		$(document).ready(function(){
			$('.faqs').click(function(){
				var id=$(this).data('id');
				$('html,body').animate({
					scrollTop: ($('#faq_'+id).offset().top -15)+'px'
				}, 1000);
			});
		});

	</script>
	{{-- For add or remove from wishlist --}}

	<script>

		$(document).ready(function(){

			$(".removeAdd").click(function(){
				var add_id = $(this).data('id');
				var reqDataa = {
					jsonrpc: '2.0'
				};

				$.ajax({
					type:'GET',
					url: '{{ url('add-to-fevourite') }}/'+add_id,
					data:reqDataa,
					success:function(response){

						if(response.sucess) {
                    // console.log(response);
                    if(response.sucess.result == 'Add') {
                    	$(".fav-span"+add_id).css('color','#2486e1');
                    	toastr.success('Product successfully added to wishlist.');
                    }
                    if(response.sucess.result == 'Remove') {
                    	$(".fav-span"+add_id).css('color','#d90000');
                    	toastr.success('Product successfully removed from wishlist.');
                    }

                } else {
                	console.log(response.error.message);
                }
            }
        });

			});


			$('#hdSwBtn').click(function(){
				$('#showText').fadeToggle();
				var btn = $(this).html();
				var txt = btn == 'Read Less' ? 'Read More' : 'Read Less';
				$('#hdSwBtn').html(txt);
			});
		});
		
	</script>
	@endsection
