@extends('layouts.app')

@section('title')
Find your best sellers
@endsection

@section('links')
@include('includes.links')
<link href="{{ asset('public/frontend/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
	.city-clear {
		top: 11px;
	}
</style>
@endsection

@section('content')
<!--wrapper start-->
<div class="wrapper">
	
	@include('includes.header')

	
	<section class="search-body shoping-cart-body">
		<div class="container">
			<div class="bed-cumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0);">@lang('admin_lang.home')</a></li>
					<li class="breadcrumb-item active">@lang('admin_lang.browse_seller')</li>
				</ol>
			</div>

			<div class="info-area guest">
				<div class="new-adrs-area new1">
					<form action="{{ route('seller.search') }}" method="POST">
						@csrf
						<div class="row">
							<div class="col-lg-5 col-md-6 col-sm-12">
								<div class="one-main">
									<select class="login_type login-select required shp_inp_msg" name="shipping_country" id="shipping_country">
										<option value="">@lang('front_static.select_country')</option>
										@foreach($country as $cn)
										<option value="{{ $cn->id }}" @if(@$key['shipping_country'] == $cn->id) selected="" @endif>{{ @$cn->countryDetailsBylanguage->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-lg-2 col-md-6 col-sm-12">
								<div class="one-main">
									<button type="submit" class="view-sell" id="searchBtn" style="border: navajowhite; margin-top: 0;padding: 12px 24px;">@lang('admin_lang.Search')</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div> 

			<div class="received-review dtls-rev my-2">
				@if(count($merchant) > 0)
				@foreach($merchant as $merchant)
				<div class="all-reviews my-1">
					<div class="seller-image">
						@if(@$merchant->image)
						<span>
							<img src="{{url('storage/app/public/profile_pics/'.@$merchant->image)}}" alt=""></span>
						@else
						<span>
							<img src="{{ asset('public/admin/assets/images/person_icon.png') }}" alt=""></span>
						@endif
					</div>
					<div class="seller-dtls" style="width: 70%;">
						<h3> <a href="{{ route('merchant.profile', @$merchant->slug) }}"> {{ @$merchant->company_name }}</a></h3>
						<!-- <h3>{{ @$merchant->fname }} {{ @$merchant->lname }}</h3> -->
						<p>
							<!-- @if(@$merchant->country == 134) {{ @$merchant->merchantCityDetails->name }}  @else {{ @$merchant->city }} @endif, -->
						 {{ @$merchant->Country->name }}</p>
						<p>{!! substr(@$merchant->merchantCompanyDetailsByLanguage->description, 0,114) !!}...</p>
						<a class="view-sell" href="{{ route('merchant.profile', @$merchant->slug) }}">@lang('admin_lang.view_seller_profile')</a>
					</div>
				</div>
				@endforeach
				@else
				<div class="all-reviews my-1">
					<h3>@lang('front_static.no_records_found')</h3>
				</div>
				@endif
			</div>   
		</div>
	</section>


	@include('includes.footer')
	
	@endsection

	@section('scripts')
	@include('includes.scripts')

	<script src="{{ asset('public/frontend/toastr/toastr.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			/* Menu Start */
			$('#sm').mouseover(function(){
				$(this).show();
			});
			$('#sm').mouseout(function(){
				$(this).hide();
			});

			$('#m1').mouseover(function(){
				$('.all_menu').show();
				$('').hide();
				$('#sm').show();
				$('.all_menu').pinterest_grid({
					no_columns: 4,
					padding_x: 10,
					padding_y: 10,
					margin_bottom: 50,
					single_column_breakpoint: 700
				});
			});


			$('#m1, #m2 ').mouseleave(function(){
				$('#sm').hide();
				$('#sm1, #sm2').hide();
			});
			/* Menu Start */

			$('#paste-lan').click(function(){
				$('.open-lan').toggle();
			/*var lang = '';// $('.open-lan').children('li').children('a').data('id');
			//alert(lang);
			if(lang == 'english') {
				$('#paste-lan').html('<img src="images/english-flag1.png" alt="">')
				$('.open-lan').css('display', 'none');
			} else {
				if(lang == 'arabic') {
					$('#paste-lan').html('src="images/arabic-flag1.png"')
					$('.open-lan').css('display', 'none');
				} else {
					$('.open-lan').css('display', 'none');
				}
			}*/
		});
			$('.open-lan').find('a').click(function() {
				var lang = $(this).data("id");
				if(lang == 'english') {
					$('#paste-lan').html('<img src="images/english-flag1.png" alt="">');
				} else {
					$('#paste-lan').html('<img src="images/arabic-flag1.png" alt="">');
				}
				$('.open-lan').hide();
			});
		});
	</script>

	<script>

		$(document).ready(function(){
			$('.faqs').click(function(){
				var id=$(this).data('id');
				$('html,body').animate({
					scrollTop: ($('#faq_'+id).offset().top -15)+'px'
				}, 1000);
			});
		});

	</script>
	{{-- For add or remove from wishlist --}}

	<script>

		$(document).ready(function(){

			$(".removeAdd").click(function(){
				var add_id = $(this).data('id');
				var reqDataa = {
					jsonrpc: '2.0'
				};

				$.ajax({
					type:'GET',
					url: '{{ url('add-to-fevourite') }}/'+add_id,
					data:reqDataa,
					success:function(response){

						if(response.sucess) {
                    // console.log(response);
                    if(response.sucess.result == 'Add') {
                    	$(".fav-span"+add_id).css('color','#2486e1');
                    	toastr.success("@lang('admin_lang.pro_suc_added_wish')");
                    }
                    if(response.sucess.result == 'Remove') {   
                    	$(".fav-span"+add_id).css('color','#d90000');
                    	toastr.success("@lang('admin_lang.remove_wishlist')");
                    }

                } else {
                	console.log(response.error.message);
                }
            }
        });     

			}); 


			$('#hdSwBtn').click(function(){
				$('#showText').fadeToggle();
				var btn = $(this).html();
				var txt = btn == 'Read Less' ? 'Read More' : 'Read Less';
				$('#hdSwBtn').html(txt);
			});


			$('#shipping_country').change(function(){
				if($(this).val() == 134) {
					$('#shipping_city_id').attr('readonly', false);
				} else {
					$('#shipping_city_id').attr('readonly', true);					
				}
			});

			$('#shipping_city_id').keyup(function(){ 
				$('.shipping_city_id_err').html('');
				var city = $(this).val();
				if(city != '')
				{
					var _token = '{{ @csrf_token() }}';
					$.ajax({
						url:"{{ route('autocomplete.city') }}",
						method:"POST",
						data:{city:city, _token:_token},
						success:function(response){
							if(response.status == 'ERROR') {
								$('#shpCityList').html("@lang('admin_lang.nothing_found')");
							} else {
								$('#shpCityList').fadeIn();  
								$('#shpCityList').html(response.result);
								flag = 1;
							}
						}
					});
				}
			});


			$('body').on('click', 'li', function(){ 
				$('#shipping_city_id').val($(this).text()); 
				$('#shipping_city_id').prop('readonly', true);
				$('#shpCityList').hide();   
				$('#shpCityClr').show();
			});  
			$("body").click(function(){
				$("#shpCityList").hide();
			});

			$('body').on('click', '#shpCityClr', function(){ 
				$(this).hide();
				$('#shipping_city_id').val('');
				$('#shipping_city_id').prop('readonly', false);
			}); 

		}); 

	</script>
	@endsection