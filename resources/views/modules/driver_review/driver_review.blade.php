@extends('layouts.app')

@section('title')
{{ config('app.name', 'Aswagna') }} | Driver Review
@endsection

@section('links')
    @include('includes.links')
    <style>
      
        .rate-base-layer
        {
            color: #007eff;
        }
        .rate-hover-layer
        {
            color: orange;
        }
    </style>
    <style>
        .no_service {
            width: 400px;
            height: 215px;
            padding: 62px 0;
            text-align: center;
            margin: 0 auto;
            background: #eee;
            box-shadow: 1px 1px 3px #c3c3c3;
        }
        .no_service i { 
            font-size: 35px;
            color: #111e6c;
        }
        .no_service h4 { 
            margin-top: 17px;
            color: #111e6c;
        }
        .serv_btn {
            background: #f9eb0a;
            margin-top: 10px;
        }
        .box-class{
            height: 176px;
            width: 82%;
            text-align: center;
            margin-top: 55px;
            margin-left: 60px;
        }
        .b-box{
            width: 80%;
            margin-left: 44px;
            padding: 20px;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
@endsection
<script src="{{ URL::asset('public/frontend/js/jquery-3.2.1.js') }}" ></script>
@section('content')
    <!--wrapper start-->
    <div class="wrapper">
        
    @include('includes.header')
    <section class="login-page">
        <div class="container">
            <div class="login-section addClass" >
                @if($order != null)
                    @if(@$checkReview == null)
                    <div class="b-box" id="showMsg" style="">
                        <h2>@lang('front_static.post_review')</h2>
        
                        @if($errors->any())
                            <div class="card-body" style="font-size: 20px; padding: 0">
                                @foreach($errors->all() as $err)
                                    <div class="alert alert-danger" role="alert">
                                        {{ $err }}
                                    </div>
                                @endforeach
                                {{@$message}}
                            </div>
                            @endif
                            @if(@session()->get('success'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                {{session()->get('success')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            
                            @endif
        
                            @if(@session()->get('error'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                {{session()->get('error')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif
        
        
                        <div class="main-form" >
                            
                            <div class="from-field rev-post">
                                <div class="pull-right">
                                    <h5>@lang('front_static.order_no'): {{ request()->segment(2) }}</h5>
                                    <h5>@lang('front_static.driver_name'): {{ @$order->orderMasterDetails[0]->driverDetails->fname }} {{ @$order->orderMasterDetails[0]->driverDetails->lname }}</h5>
                                </div>
        
                                <form id="review_form" method="post" action="{{ route('submit.driver.review.mail') }}" enctype="multipart/form-data">
                                @csrf
                                    
                                <div class="col-md-12">
                                    <script>
                                        $(document).ready(function(){
                                            var options = {
                                                max_value: 5,
                                                step_size: 1.0,
                                                selected_symbol_type: 'utf8_star',
                                                initial_value: 0.0,
                                                update_input_field_name: $("#input1"),
                                            }
                                            $(".rate").rate();							            

                                            $(".rate1").rate(options);

                                            $(".rate1").on("change", function(ev, data){
                                                console.log(data.from, data.to);
                                            });

                                            $(".rate1").on("updateError", function(ev, jxhr, msg, err){
                                                console.log("This is a custom error event");
                                            });

                                            $(".rate1").rate("setAdditionalData", {id: 42});
                                            $(".rate1").on("updateSuccess", function(ev, data){
                                                console.log(data);
                                            });
                                        
                                        });
                                    </script>
                                    <style type="text/css">
                                        .rate1
                                        {
                                            font-size: 35px;
                                        }
                                        .rate1 .rate-hover-layer
                                        {
                                            color: #bdf;
                                        }
                                        .rate1 .rate-select-layer
                                        {
                                            color: #0072ff;
                                        }
                                    </style>


                                    <div class="form-group">
                                        <div class="left_line">
                                            <label>@lang('front_static.rate_driver'):</label>
                                            <span id="showRateError" style="margin-left:20px;color:red;display:none">@lang('front_static.driver_rating')</span>
                                        </div>
                                        <div class="left_line_left">
                                            <div class="rate1"></div>
                                            <input id="input1" name="driver_rating" type="hidden">
                                        </div>
                                        <input name="order_id" value="{{ @$order->id }}" type="hidden">
                                        <input name="customer_id" value="{{ request()->segment(3) }}" type="hidden">
                                        <input name="driver_id" value="{{ @$order->orderMasterDetails[0]->driver_id }}" type="hidden">
                                    </div>



                                    <div class="form-group">
                                        <label>@lang('front_static.comment'): :</label>
                                        <textarea rows="5" class="edt_type2 form-control required" name="comment" placeholder=""></textarea>
                                    </div>
                                    <button type="button" class="btn btn-primary checkValidation">Submit</button>
                                </div>
                                    
                                        
        
                                </form>
        
        
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="b-box">
                        
                        <div class="wrapper-page">
                            <div class="panel box-class" >
                                <div class="panel-body">
                                    <h2 style="color:#d39b1d">@lang('front_static.rating_given')</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @else
                    <div class="b-box">
                        <div class="wrapper-page">
                            <div class="panel box-class" >
                                <div class="panel-body">
                                    <h2 style="color:#d39b1d">@lang('front_static.token_expire')</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
    @include('includes.new_footer')
</div>
<!--wrapper end-->

@endsection

@section('scripts')
@include('includes.scripts')
<script src="{{ URL::asset('public/frontend/js/rater.js') }}" charset="utf-8"></script>
<script>
    $(document).ready(function(){
        var order = "{{ $order }}";
        var checkAdded = "{{ @$checkReview }}";
        if(order == "" || checkAdded != ""){
            $('.addClass').css("background", "#acbce3");
        }
        $('body').on('click','.checkValidation',function(e){
            if($('#input1').val() == 0){
                e.preventDefault();
                $('#showRateError').show();
            }else{
                var form = $('#review_form');
                $.ajax({
                    'url':"{{ route('submit.driver.review.mail') }}",
                    'type':"POST",
                    'data':form.serialize(),
                    success:function(res){
                        if(res == 1){
                            $('#showMsg').html(
                                '<div class="b-box">'+
                                    '<div class="wrapper-page">'+
                                        '<div class="panel box-class" >'+
                                            '<div class="panel-body">'+
                                                '<h2 style="color: green">@lang("front_static.thanks_review")</h2>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'
                            )
                        }else{
                            $('#showMsg').html(
                                '<div class="b-box">'+
                                    '<div class="wrapper-page">'+
                                        '<div class="panel box-class" >'+
                                            '<div class="panel-body">'+
                                                '<h2 style="color: red">@lang("front_static.review_error")</h2>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'
                            )
                        }
                        $('.addClass').css("background", "#acbce3");
                    }
                })
            }
        })
    });
</script>
@endsection  