@extends('layouts.app')

@section('title')
@if(@$category)
{{ $category->categoryByLanguage->title }}
@else
Search your best products
@endif
@endsection

@section('links')
@include('includes.links')
<link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
@endsection

@section('meta')
	<meta property="og:url"                content="{{ url('') }}" />
	<meta property="og:type"               content="website" />
	<meta property="og:title"              content="Aswagna" />
    <meta property="og:description"        content="Aswagna is an ecommerce website and apps where you can buy from different sellers in one checkout." />
	<meta property="og:image"              content="{{ url('public/frontend/images/default_product.png') }}" />
@endsection

@section('content')
<div class="wrapper">
	@include('includes.header')

	<search-component></search-component>

	@include('includes.footer')
</div>
@endsection

@section('scripts')
<script src="{{ asset('public/frontend/js/site_lang-'.Config::get('app.locale').'.js') }}"></script>
<script src="{{ asset('public/js/app.js') }}"></script>
@include('includes.scripts')

<script>
	$(document).ready(function(){
		$(".mobile_filter").click(function(){
			$(".left-search").slideToggle();
		});
	});
</script>
@endsection