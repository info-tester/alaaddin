@extends('layouts.app')

@section('title')
{{ config('app.name', '') }} | Fastest ecommerce portal for shopping products

@endsection

@section('links')

@include('includes.links')
<link href="{{ asset('public/frontend/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
@endsection
<meta name="title" content="Fastest ecommerce portal for shopping products">
<meta name="description" content="Alaaddin brings you joy with hustle free fruit and vegetables shoping with the trusted seller in all over India online and brings your product safely (Insurance ) to you.">
<meta name="keywords" content="Vegetable , Home Delivery , Fruit , Fresh , Fresh Fruits , Fresh Vegetables , Free Shipping , vegetables App , Grocery App , App, ecommerce , shopping, India Online" />

@section('content')
<!--wrapper start-->
<div class="wrapper-new">
			<div class="opacitys"></div>
    <div class="logo_div">
        <a href="https://play.google.com/store/apps/details?id=com.buyer.alaaddin"><img src="{{asset('public/frontend/images/unnamed.webp')}}" alt="" style="width:130px;" /></a>
        <a href="https://play.google.com/store/apps/details?id=com.buyer.alaaddin" target="_blank" class="new_apps">
    			<img src="{{asset('public/frontend/images/goolapp.png')}}" alt="">
    		</a>
    </div>
    <div class="banner_divs" >
    	<div class="grocery-texts">
    		<p>Get Your <br> <span>Products</span> <br> Delivered  <br> <span> Fast</span></p>
    		
    	</div>
    	<img src="{{asset('public/frontend/images/wait.png')}}" alt="">
    </div>

    <div class="copy-rights" >
    	<p>Copyright © 2023 Alaaddin.in | All Rights Reserved.</p>
    </div>
		
</div>
@endsection
@section('scripts')
@include('includes.scripts')
@endsection