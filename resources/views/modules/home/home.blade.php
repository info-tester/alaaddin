@extends('layouts.app')

@section('title')
Aswagna is an ecommerce website and apps where you can buy everythings
@endsection

@section('links')
@include('includes.links')
<link href="{{ asset('public/frontend/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('meta')
	<meta property="og:url"                content="{{ url('') }}" />
	<meta property="og:type"               content="website" />
	<meta property="og:title"              content="Aswagna" />
    <meta property="og:description"        content="Aswagna is an ecommerce website and apps where you can buy from different sellers in one checkout." />
	<meta property="og:image"              content="{{ url('public/frontend/images/default_product.png') }}" />
@endsection

@section('content')
<!--wrapper start-->
		<div class="wrapper">
			@include('includes.header')

			<section class="banner">
				<div id="demo" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ul class="carousel-indicators">
						<li data-target="#demo" data-slide-to="0" class="active"></li>
						@foreach($all_banners as $i => $banner)
							<li data-target="#demo" data-slide-to="{{$i+1}}"></li>
						@endforeach
					</ul>
					<!-- The slideshow -->
					<div class="carousel-inner">
						<div class="carousel-item active">
							@if(@$first_banners->banner_image)
		                        @php
		                            $image_path = 'storage/app/public/bannerimg/'.@$first_banners->banner_image; 
		                        @endphp
		                        @if(file_exists(@$image_path))
		                        	@if(@$banner->banner_buttton_cap)
		                            	<img id="profilePicture" src="{{ URL::to('storage/app/public/bannerimg/'.$first_banners->banner_image) }}" alt="">
		                            @else
		                            	<img id="profilePicture" src="{{ URL::to('storage/app/public/bannerimg/'.$first_banners->banner_image) }}" alt="" data-url="{{@$first_banners->banner_buttton_url}}" onclick="myFunction()">
		                            @endif
		                        @endif
		                    @else
								<img src="public/frontend/images/banner1.jpg" alt="">
							@endif

							@if(@$first_banners->banner_buttton_cap || @$first_banners->banner_heading || @$first_banners->banner_desc || @$first_banners->banner_buttton_url) 
							<div class="ban_contain">
								<div class="container">
									<div class="ban_caption">
										<!-- <h4>@lang('front_static.for_womens')</h4> -->
										@if(@$first_banners->banner_heading)
										<h1>{{@$first_banners->banner_heading}}</h1>
										@endif
										@if(@$first_banners->banner_sub_heading)
										<h3>{{@$first_banners->banner_sub_heading}}</h3>
										@endif
										@if(@$first_banners->banner_desc)
										<p> {{@$first_banners->banner_desc}}</p>
										@endif
										@if(@$first_banners->banner_buttton_cap)
										<a class="ban-btn" href="{{@$first_banners->banner_buttton_url}}">{{@$first_banners->banner_buttton_cap}}</a>
										@endif
									</div>
								</div>
							</div>
							@endif
						</div>
						@foreach($all_banners as $banner)
						<div class="carousel-item">
							@if(@$banner->banner_image)
		                        @php
		                            $image_path = 'storage/app/public/bannerimg/'.@$banner->banner_image; 
		                        @endphp
		                        @if(file_exists(@$image_path))
		                        	@if(@$banner->banner_buttton_url)
		                            	<img id="profilePicture" src="{{ URL::to('storage/app/public/bannerimg/'.$banner->banner_image) }}" alt="" class="bannerUrl" data-url= "{{@$banner->banner_buttton_url}}">
		                            @else
		                            	<img id="profilePicture" src="{{ URL::to('storage/app/public/bannerimg/'.$banner->banner_image) }}" alt="" data-url= "{{@$banner->banner_buttton_url}}">
		                            @endif
		                        @endif
		                    @endif
		                    @if(@$banner->banner_heading || @$banner->banner_sub_heading || @$banner->banner_desc || @$banner->banner_buttton_cap)
							<div class="ban_contain">
								<div class="container">
									<div class="ban_caption">
										@if(@$banner->banner_heading)
											<h1>{{@$banner->banner_heading}}</h1>
										@endif
										@if(@$banner->banner_sub_heading)
											<h3>{{@$banner->banner_sub_heading}}</h3>
										@endif
										@if(@$banner->banner_desc)
										<p>{{@$banner->banner_desc}}</p>
										@endif
										@if(@$banner->banner_buttton_cap) 
										<a class="ban-btn" href="{{@$banner->banner_buttton_url}}">{{@$banner->banner_buttton_cap}}</a>
										@endif
									</div>
								</div>
							</div>
							@endif
						</div>
						@endforeach
						
					</div>
					<!-- Left and right controls --> 
				</div>
			</section>

			<section class="featured-area">
				<div class="container">
					<div class="section-header">
						<h2><span>@lang('front_static.featured_products')</span></h2>
						<span class="section-header-line"></span>
					</div>
					<div class="all-featured">
						<div id="owl-demo-4" class="owl-carousel">

							@foreach($featured_products as $pro)
							@if($pro->is_featured == 'Y')
							
							<div class="item">
								<div class="featured_box">
									<div class="featured-images">
										<a href="{{ route('product',@$pro->slug) }}">
											@if(@$pro->defaultImage->image)
											<img src="{{url('storage/app/public/products/300/'.@$pro->defaultImage->image)}}" alt="">
											@else
											<img src="{{ getDefaultImageUrl() }}" alt="">
											@endif
										</a>
										@if(@$pro->discount_price > 0 && @$pro->discount_price != 0.00 && date('Y-m-d')>=@$pro->from_date && date('Y-m-d')<=@$pro->to_date)
										<span class="new-sale2">{{ round(((@$pro->price - @$pro->discount_price)/@$pro->price) * 100) }}% @lang('front_static.off')</span>
										@endif
										@auth
										<a class="fav-span removeAdd fav-span{{ @$pro->id }}" @if(@$pro->wishlist) style="color: #d90000" @endif data-id="{{ @$pro->id }}" href="javascript:void(0);"><i class="fa fa-heart" aria-hidden="true"></i></a>
										@else
										<a class="fav-span" href="{{ route('login') }}"><i class="fa fa-heart" aria-hidden="true"></i></a>
										@endauth
									</div>
									<div class="featured-dtls">
										<a href="{{ route('product',@$pro->slug) }}">
											<h4>{{ @$pro->productByLanguage->title }} </h4>
										</a>
										@if(@$pro->discount_price != 0 && date('Y-m-d')>=@$pro->from_date && date('Y-m-d')<=@$pro->to_date)
										<h6>
											<span>{{ @$pro->price }} {{ getCurrency() }}</span> {{ @$pro->discount_price }}{{ getCurrency() }}
										</h6>
										@else
										<h6>{{ @$pro->price }} {{ getCurrency() }}</h6>
										@endif
										<p>{{  substr(strip_tags(@$pro->productByLanguage->description), 0,100)	}}...</p>
										<a href="{{ route('product',@$pro->slug) }}" class="Shop-now">@lang('front_static.shop_now')</a>
									</div>
								</div>
							</div>
							@endif
							@endforeach
						</div>
					</div>
				</div>
			</section>

			<section class="scrool-area">
				<div id="owl-demo-3" class="owl-carousel">
					@foreach($category as $cat)
					<div class="item">
						<div class="seller_box">
							<a href="{{route('search',$cat->slug)}}"> 
								@if(@$cat->picture)
								<img src="{{ URL::to('storage/app/public/category_pics/'.@$cat->picture) }}" alt="" style="width: 100px; height: 100px;">
								@else
								<img src="public/frontend/images/default_product.png" alt="">
								@endif
								<h5>{{ $cat->categoryByLanguage->title }}</h5>
								@if($cat->productByCategoryCount)
								<p>{{ count(@$cat->productByCategoryCount) }} @lang('front_static.items')</p>
								@endif
							</a>
						</div>
					</div>
					@endforeach
				</div>
			</section>

			<section class="new-product">
				<div class="container">
					<div class="section-header">
						<h2><span>@lang('front_static.new_products')</span></h2>
						<span class="section-header-line"></span>
					</div>
					<div class="all-products">
						<div class="row">
							@foreach($new_products as $key => $newPro)
							@if($key < 8)
							<div class="col-lg-3 col-md-4 col-sm-6">
								<div class="product-box">
									<div class="product-image">
										<a href="{{ route('product',@$newPro->slug) }}">
											@if(@$newPro->defaultImage->image)
											<img src="{{url('storage/app/public/products/300/'.@$newPro->defaultImage->image)}}" alt="">
											@else
											<img src="{{ getDefaultImageUrl() }}" alt="">
											@endif
										</a>
										@if( @$newPro->price > 0 &&  @$newPro->discount_price != 0 && date('Y-m-d')>=@$newPro->from_date && date('Y-m-d')<=@$newPro->to_date)
										<span class="new-sale">{{ round(((@$newPro->price - @$newPro->discount_price)/@$newPro->price) * 100) }}% @lang('front_static.off')</span>
										@endif
										@auth
										<a class="fav-span removeAdd fav-span{{ @$newPro->id }}" @if(@$newPro->wishlist) style="color: #d90000" @endif href="javascript:void(0);" data-id="{{ @$newPro->id }}"><i class="fa fa-heart" aria-hidden="true"></i></a>
										@else
										<a class="fav-span" href="{{ route('login') }}"><i class="fa fa-heart" aria-hidden="true"></i></a>
										@endauth
									</div>
									<div class="product-dtls">
										<h3>@lang('front_static.sold_by') : {{ @$newPro->productMarchant->fname }} {{ @$newPro->productMarchant->lname }}</h3>
										<h4><a href="{{ route('product',@$newPro->slug) }}">{{ @$newPro->productByLanguage->title }}</a></h4>
										<p>												
											@if(@$newPro->discount_price != 0 && date('Y-m-d')>=@$newPro->from_date && date('Y-m-d')<=@$newPro->to_date)
											<strong>{{ @$newPro->price }} {{ getCurrency() }} </strong>  -  <span>{{ @$newPro->discount_price }} {{ getCurrency() }}</span>
											@else
											<span>{{ @$newPro->price }} {{ getCurrency() }}</span>
											@endif
										</p>
									</div>
								</div>
							</div>
							@endif
							@endforeach
						</div>
					</div>
				</div>
			</section>

			<section class="deals-area">
				<div class="container">
					<div class="section-header">
						<h2><span>@lang('front_static.best_deals')</span></h2>
						<span class="section-header-line"></span>
					</div>
					<div class="main-deals">
						<div id="owl-demo-2" class="owl-carousel">
							@foreach($bestDealProduct as $key => $bestPro)
							<div class="item">
								<div class="deals-box">
									<div class="deals-image">
										<a href="{{ route('product',@$bestPro->slug) }}">
											@if(@$bestPro->defaultImage->image)
											<img src="{{url('storage/app/public/products/300/'.@$bestPro->defaultImage->image)}}" alt="">
											@else
											<img src="{{ getDefaultImageUrl() }}" alt="">
											@endif
										</a>
										@if(@$bestPro->price >0 && @$bestPro->discount_price != 0.00 && date('Y-m-d')>=@$bestPro->from_date && date('Y-m-d')<=@$bestPro->to_date)
										<span class="orrer-circel">{{ round(((@$bestPro->price - @$bestPro->discount_price)/@$bestPro->price) * 100) }}% @lang('front_static.off')</span>
										@endif
										@auth
										<a class="fav-span removeAdd fav-span{{ @$bestPro->id }}" @if(@$bestPro->wishlist) style="color: #d90000" @endif href="javascript:void(0);" data-id="{{ @$bestPro->id }}"><i class="fa fa-heart" aria-hidden="true"></i></a>
										@else
										<a class="fav-span" href="{{ route('login') }}"><i class="fa fa-heart" aria-hidden="true"></i></a>
										@endauth
									</div>
									<div class="deals-dtls">
										<h4><a href="{{ route('product',@$bestPro->slug) }}">{{ @$bestPro->productByLanguage->title }}</a></h4>
										<p>										
											@if(@$bestPro->discount_price != 0 && date('Y-m-d')>=@$bestPro->from_date && date('Y-m-d')<=@$bestPro->to_date)
											<strong>{{ @$bestPro->price }} {{ getCurrency() }} </strong>  - <span>{{ @$bestPro->discount_price }} {{ getCurrency() }}</span></p> 
											@else
											<span>{{ @$bestPro->price }} {{ getCurrency() }}</span>
											@endif
										</p> 
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</section>

			<section class="best-seller">
				<div class="container">
					<div class="section-header">
						<h2><span>@lang('front_static.best_sellers')</span></h2>
						<span class="section-header-line"></span>
						<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim</p> -->
					</div>
					<div class="all-seller">
						<div id="owl-demo-1" class="owl-carousel">
							@foreach($merchant as $merch)
							<div class="item">
								<div class="seller_box">
									<a href="{{ route('merchant.profile', $merch->slug) }}">
										@if($merch->image)
										<img src="{{url('storage/app/public/profile_pics/'.@$merch->image)}}" alt="">
										@else
										<img src="public/frontend/images/person_icon.png" alt="">
										@endif
									</a>
									<a href="{{ route('merchant.profile', $merch->slug) }}">
										<p>{{ @$merch->fname }} {{ @$merch->lname }}</p>
									</a>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</section>

			@include('includes.footer')
		</div>
<!--wrapper end-->
@endsection

@section('scripts')
@include('includes.scripts')
<script src="{{ asset('public/frontend/toastr/toastr.js') }}"></script>

{{-- For add or remove from wishlist --}}
<script type="text/javascript">
	function myFunction(){
    	window.location.href = "{{@$first_banners->banner_buttton_url}}";
    } 
    $(".bannerUrl").click(function(e){
    	var url = $(this).attr("data-url");
    	window.location.href = url;
    });
	$(document).ready(function(){

		$(".removeAdd").click(function(){
			var add_id = $(this).data('id');
			var reqDataa = {
				jsonrpc: '2.0'
			};

			$.ajax({
				type:'GET',
				url: '{{ url('add-to-fevourite') }}/'+add_id,
				data:reqDataa,
				success:function(response){
					if(response.sucess) {
	                    if(response.sucess.result == 'Add') {
	                    	$(".fav-span"+add_id).css('color','#d90000');
	                    	toastr.success("@lang('admin_lang.pro_suc_added_wish')");
	                    }
	                    if(response.sucess.result == 'Remove') {   
	                    	$(".fav-span"+add_id).css('color','#ececec');
	                    	toastr.success("@lang('admin_lang.remove_wishlist')");
	                    }
	                } else {
	                	console.log(response.error.message);
	                }
	            }
        	});
		});
	});
</script>
@endsection