@extends('layouts.app')
@section('title')
@lang('front_static.check_out')
@endsection
@section('links')
@include('includes.links')
@endsection
@section('content')
{{-- @include('includes.header') --}}
<table class="table table-striped">
    <thead>
        <tr>
            <td>Merchant Name</td>
            <td>Total </td>
            <td>Total Commision</td>
            <td>Commision(%)</td>
            <td>Earning</td>
            <td>Paid</td>
            {{-- <td>Due</td> --}}
            <td>Invoice In</td>
            <td>Invoice Out</td>
        </tr>
    </thead>
    <tbody>
        @if(@$merchant)
            @foreach($merchant as $val)
                @php
                    $total = 0;
                    $total_commission = 0;
                    $earning = 0;
                    $total_paid = 0;
                    $flag = 0;
                @endphp
                @if(@$val->getOrderSellerDetails)
                    @foreach(@$val->getOrderSellerDetails as $val2)
                        @if(@$val2->orderMasterTab)
                            @php
                                if($val2->orderMasterTab->status == 'OD' && $val2->orderMasterTab->order_type == 'I'){
                                    $flag = 1;
                                }
                                if($val2->orderMasterTab->status == 'OD' && $val2->orderMasterTab->created_at > date('Y-m-d H:i:s',strtotime("2021-01-31")) ){
                                    
                                    $total = $total + (@$val2->subtotal - @$val2->total_discount);
                                    $total_commission = $total_commission + @$val2->total_commission;
                                }
                            @endphp
                        @endif
                    @endforeach
                @endif
                @if(@$val->getWithdrawRecord)
                    @foreach(@$val->getWithdrawRecord as $val1)
                        @php  
                            $total_paid = $total_paid + @$val1->amount;
                        @endphp
                    @endforeach
                @endif
                @php
                    $earning = $total - $total_commission ;
                @endphp
                @if($flag == 1)
                <tr>
                    <td>{{@$val->fname}} {{@$val->lname}}</td>
                    <td>
                        {{@$total}}
                    </td>
                    <td>{{@$total_commission}}</td>
                    <td>{{@$val->commission}}</td>
                    <td>{{@$earning}}</td>
                    <td>{{@$total_paid}}</td>
                    {{-- <td>{{@$val->total_due}}</td> --}}
                    <td>{{@$val->invoice_in}}</td>
                    <td>{{@$val->invoice_out}}</td>
                </tr>
                @endif
            @endforeach
        @endif
    </tbody>
</table>
{{-- @include('includes.new_footer') --}}
@endsection
@section('scripts')
@include('includes.scripts')