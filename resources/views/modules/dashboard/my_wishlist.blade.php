@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.my_wishlist')
@endsection
@section('links')
@include('includes.links')
<link href="{{ asset('public/frontend/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .error {
        border: solid 1px #fb8282 !important;
        background: #ff000014 !important;
    }
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="search-body">
        <div class="container">
            <div class="bed-cumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('front_static.home')</a></li>
                    <li class="breadcrumb-item">@lang('front_static.dashboard')</li>
                    <li class="breadcrumb-item active">@lang('front_static.my_wishlist')</li>
                </ol>
            </div>
            <div class="main-dash">
                @include('includes.user_sidebar')
                
                <div class="right-dashboard wishlist">
                 <div class="row">
                    @if(count($favProduct) > 0)
                    @foreach($favProduct as $favPro)
                    <div class="col-lg-4 col-md-12 col-sm-6 removeDiv{{ @$favPro->id }}">
                        <div class="product-box">
                            <div class="product-image">
                                <a href="{{ route('product',@$favPro->getProduct->slug) }}">
                                    @if(@$favPro->getProduct->defaultImage->image)
                                    <img src="{{url('storage/app/public/products/'.@$favPro->getProduct->defaultImage->image)}}" alt="">
                                    @else
                                    <img src="https://aswagna.co/public/frontend/images/default_product.png" alt="">
                                    @endif
                                </a>
                                @if(@$favPro->getProduct->discount_price != '0.00')
                                <span class="new-sale">
                                    {{ round(((@$favPro->getProduct->price - @$favPro->getProduct->discount_price)/@$favPro->getProduct->price) * 100) }}% OFF
                                </span>
                                @endif
                                <a class="fav-span2 removeFav removeFav{{ @$favPro->id }}" href="javascript:void(0);" data-id="{{ @$favPro->id }}"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                            </div>
                            <div class="product-dtls">
                                <h3>@lang('admin_lang.sold_by') : {{ @$favPro->getProduct->productMarchant->fname }} {{ @$favPro->getProduct->productMarchant->lname }}</h3>
                                <h4><a href="{{ route('product',@$favPro->getProduct->slug) }}">{{ @$favPro->getProduct->productByLanguage->title }}</a></h4>
                                <p>
                                    @if(@$favPro->getProduct->discount_price != '0.00')
                                    <strong>{{ @$favPro->getProduct->price }} {{ getCurrency() }} </strong>  - <span>{{ @$favPro->getProduct->discount_price }} {{ getCurrency() }}</span></p> 
                                    @else
                                    <span>{{ @$favPro->getProduct->price }} {{ getCurrency() }}</span>
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="col-lg-12 text-center">
                        <img src="{{ asset('public/frontend/no_record.png') }}" style="max-width: 100%;" alt="">
                        <h4 style="line-height: 100px;">@lang('front_static.no_records_found')</h4>
                    </div>
                    @endif
                    
                </div> 
            </div>
        </div>
    </div>
</section>
@include('includes.new_footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{ asset('public/frontend/toastr/toastr.js') }}"></script>

{{-- For remove from wishlist --}}

<script>

    $(document).ready(function(){

        $(".removeFav").click(function(){
            var add_id = $(this).data('id');
            var reqDataa = {
                jsonrpc: '2.0'
            };

            $.ajax({
                type:'GET',
                url: '{{ url('remove-wishlist') }}/'+add_id,
                data:reqDataa,
                success:function(response){
                    if(response.sucess) {
                    // console.log(response.sucess);
                    if(response.sucess.result == 'Remove') {
                        $(".fav-icon"+add_id).removeClass("fa fa-heart");
                        $(".fav-icon"+add_id).addClass("fa fa-heart-o");
                        $(".removeDiv"+add_id).hide();
                        toastr.success(response.sucess.message);
                    }

                } else {
                    console.log(response.error.message);
                }
            }
        });     

        });   

    });

</script>
@endsection