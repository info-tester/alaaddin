@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.address_book')
@endsection
@section('links')
@include('includes.links')
<link href="{{ asset('public/frontend/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .error {
        border: solid 1px #fb8282 !important;
        background: #ff000014 !important;
    }
    .shipping-box {
        border: 1px solid #dcdbdb;
    }
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="search-body">
        <div class="container">
            <div class="bed-cumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('front_static.home')</a></li>
                    <li class="breadcrumb-item">@lang('front_static.dashboard')</li>
                    <li class="breadcrumb-item active">@lang('front_static.address_book')</li>
                </ol>
            </div>
            <div class="main-dash">
                @include('includes.user_sidebar')
                <div class="right-dashboard">
                    <div class="shipping-area address-book">
                        <div class="row">
                            <div class="col-lg-12">
                                <a class="add-new" href="{{ route('user.add.address.book') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> @lang('front_static.add_new_address')</a>
                            </div>
                            @if (session()->has('success'))
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="alert alert-success vd_hidden" style="display: block;">
                                    <a class="close" data-dismiss="alert" aria-hidden="true">
                                        <i class="icon-cross"></i>
                                    </a>
                                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                                    <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                                </div>
                            </div>
                            @elseif ((session()->has('error')))
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="alert alert-danger vd_hidden" style="display: block;">
                                    <a class="close" data-dismiss="alert" aria-hidden="true">
                                        <i class="icon-cross"></i>
                                    </a>
                                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                                    <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                                </div>
                            </div>
                            @endif
                            @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible" style="text-align: center;">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul style="margin-bottom: 0; padding-left: 0; ">
                                    @foreach ($errors->all() as $error)
                                    <li style="list-style: none; font-size: 14px; color: #fff;">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            @if($userDefAddr)
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="shipping-box">
                                    <h3>@lang('front_static.default_shipping_address')<a href="{{ route('user.edit.address.book', $userDefAddr->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></h3>
                                    <p>{{ $userDefAddr->shipping_fname }} {{ $userDefAddr->shipping_lname }}</p>
                                    <p>{{ $userDefAddr->city }}, {{ $userDefAddr->street }},
                                        @if($userDefAddr->block)
                                        {{ $userDefAddr->block }},
                                        @endif
                                        @if($userDefAddr->building) {{ $userDefAddr->building }},
                                        @endif
                                        @if($userDefAddr->more_address) {{ $userDefAddr->more_address }},@endif
                                        {{ @$userDefAddr->getCountry->name }}
                                        @if($userDefAddr->postal_code), {{ $userDefAddr->postal_code }}@endif
                                    </p>
                                    <p>Email : {{ $userDefAddr->email }}</p>
                                    <p>Phone No : {{ $userDefAddr->phone }}</p>
                                </div>
                            </div>
                            @else
                            <div class="col-lg-12 text-center">
                                <img src="{{ asset('public/frontend/no_record.png') }}" style="max-width: 100%;" alt="">
                                <h4 style="line-height: 100px;">@lang('front_static.no_records_found')</h4>
                            </div>
                            @endif
                            @foreach($userAddr as $key => $ua)
                            <div class="col-lg-6 col-md-12 col-sm-12 removeDiv{{ $ua->id }}">
                                <div class="shipping-box">
                                    <h3>@lang('front_static.additional_address') {{ $key+1 }}  
                                        <a class="exit-address removeAdd removeAdd{{ @$ua->id }}" href="javascript:void(0);" data-id="{{ $ua->id }}"><i class="fa fa-times" aria-hidden="true"></i></a> 
                                        <a href="{{ route('user.edit.address.book', @$ua->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="{{ route('user.set.def.address', @$ua->id) }}" style="margin-right: 4px;"><i class="fa fa-check" aria-hidden="true"></i></a>
                                    </h3>
                                    <p>{{ $ua->shipping_fname }} {{ $ua->shipping_lname }}</p>
                                    <p>{{ $ua->city }}, {{ $ua->street }},
                                        @if($ua->block)
                                        {{ $ua->block }},
                                        @endif
                                        @if($ua->building) {{ $ua->building }},
                                        @endif
                                        @if($ua->more_address) {{ $ua->more_address }},@endif
                                        {{ @$ua->getCountry->name }}
                                        @if($ua->postal_code), {{ $ua->postal_code }}@endif
                                    </p>
                                    <p>@lang('front_static.email') : {{ $ua->email }}</p>
                                    <p>@lang('front_static.phone_no') : {{ $ua->phone }}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('includes.new_footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{ asset('public/frontend/toastr/toastr.js') }}"></script>
<script>
    $(document).ready(function(){

        $('#password').keyup(function(event) {
            $('#new_password').addClass('required');
        });
        $("#edit_form" ).validate({
         rules:{ 

            email : {
                required: true,
                email:true
            },
            phone : {
                required: true,
                digits: true
            },
            zipcode : {
                required: true,
                digits: true
            },
            new_password : {
                minlength : 6,
                maxlength: 15
            },
            confirm_password : {          
                required: false,
                equalTo : "#new_password"
            }
        },
        errorPlacement: function (error , element) {
                //toastr:error(error.text());
            },
        });
    });
</script>
{{-- For remove address --}}

<script>

    $(document).ready(function(){

        $(".removeAdd").click(function(){
            var add_id = $(this).data('id');
            var reqDataa = {
                jsonrpc: '2.0'
            };

            $.ajax({
                type:'GET',
                url: '{{ url('remove-address') }}/'+add_id,
                data:reqDataa,
                success:function(response){
                    if(response.sucess) {
                    // console.log(response.sucess);
                    if(response.sucess.result == 'Remove') {
                        $(".fav-icon"+add_id).removeClass("fa fa-heart");
                        $(".fav-icon"+add_id).addClass("fa fa-heart-o");
                        $(".removeDiv"+add_id).hide();
                        toastr.success(response.sucess.message);
                    }

                } else {
                    console.log(response.error.message);
                }
            }
        });     

        });   

    });

</script>
@endsection