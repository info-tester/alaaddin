@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.edit_address_book')
@endsection
@section('links')
@include('includes.links')
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
    .error {
        border: solid 1px #fb8282 !important;
        background: #ff000014 !important;
    }
    .chosen-container {
        background: #fff;
        height: 50px;
        border: none;
        border-radius: 2px;
        width: 100%;
        padding: 10px 15px;
        color: #333;
        font-family: 'Poppins', sans-serif;
        font-size: 14px;
        font-weight: 400;
        float: left;
        appearance: none;
        -moz-appearance: none;
        -webkit-appearance: none;
        border: 1px solid #cccbcb;
        margin-bottom: 0px;
    }
    .chosen-container-single .chosen-drop {
        margin-top: -1px;
        border-radius: 0 0 4px 4px;
        left: 0;
        background-clip: padding-box;
    }
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="search-body">
        <div class="container">
            <div class="bed-cumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('front_static.home')</a></li>
                    <li class="breadcrumb-item">@lang('front_static.dashboard')</li>
                    <li class="breadcrumb-item active">@lang('front_static.edit_address_book')</li>
                </ol>
            </div>
            <div class="main-dash">
                @include('includes.user_sidebar')
                <div class="right-dashboard">
                    <form method="post" action="{{ route('user.update.address.book', $userAddr->id) }}" id="address_form">
                        @csrf
                        <div class="information-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>@lang('front_static.personal_information')</h3>
                                </div>
                                @if (session()->has('success'))
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="alert alert-success vd_hidden" style="display: block;">
                                        <a class="close" data-dismiss="alert" aria-hidden="true">
                                            <i class="icon-cross"></i>
                                        </a>
                                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                                    </div>
                                </div>
                                @elseif ((session()->has('error')))
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="alert alert-danger vd_hidden" style="display: block;">
                                        <a class="close" data-dismiss="alert" aria-hidden="true">
                                            <i class="icon-cross"></i>
                                        </a>
                                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                                        <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                                    </div>
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" style="text-align: center;">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul style="margin-bottom: 0; padding-left: 0; ">
                                        @foreach ($errors->all() as $error)
                                        <li style="list-style: none; font-size: 14px; color: #fff;">{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.first_name')</label>
                                        <input type="text" class="login_type required" name="shipping_fname" id="shipping_fname" placeholder="@lang('front_static.first_name')" value="{{ $userAddr->shipping_fname }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.last_name')</label>
                                        <input type="text" class="login_type required" name="shipping_lname" id="shipping_lname" placeholder="@lang('front_static.last_name')" value="{{ $userAddr->shipping_lname }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.email_address')</label>
                                        <input type="text" class="login_type only-read required" name="email" id="email" placeholder="@lang('front_static.email_address')" value="{{ $userAddr->email }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.mobile_number')</label>
                                        <input type="text" class="login_type required" name="phone" id="phone" placeholder="@lang('front_static.mobile_number')" value="{{ $userAddr->phone }}" onkeypress='validate(event)'>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="information-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>@lang('front_static.address_information')</h3>
                                </div>
                                {{-- <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.address1')</label>
                                        <input type="text" class="login_type required" name="address1" id="address1" placeholder="@lang('front_static.address1')" value="{{ $userAddr->address1 }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.address2')</label>
                                        <input type="text" class="login_type" name="address2" id="address2" placeholder="@lang('front_static.address2')" value="{{ $userAddr->address2 }}">
                                    </div>
                                </div> --}}
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.country')</label>
                                        <select class="login_type login-select required" name="country" id="country">
                                            <option value="">@lang('front_static.select_country')</option>
                                            @foreach($country as $cn)
                                            <option value="{{ $cn->id }}" @if($cn->id == $userAddr->country) selected="" @elseif($cn->status == 'I') style="display:none;"  @endif>{{ @$cn->countryDetailsBylanguage->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12" id="kuCity"  @if($userAddr->country != 134) style="display: none;" @endif>
                                    <div class="one-main" style="position: relative;">
                                        <label class="from-label">@lang('front_static.city')</label>
                                        {{-- <select class="login_type login-select selectCh required" name="city_id" id="city_id" tabindex="3">
                                            <option value="">Select City</option>
                                            @foreach($city as $city)
                                            <option value="{{ $city->name }}" @if($city->name == $userAddr->city) selected="" @endif>{{ $city->name }}</option>
                                            @endforeach
                                        </select> --}}
                                        <input type="text" class=" login_type login-select required" name="city_id" id="city_id" placeholder="@lang('front_static.city')" @if($userAddr->country == 134) value="{{ $userAddr->city }}" readonly="" @endif>
                                        <div id="cityList"></div>
                                        @if($userAddr->country == 134)
                                        <span id="cityClr" class="city-clear"><i class="fa fa-times-circle"></i></span>
                                        @else
                                        <span id="cityClr" class="city-clear" style="display: none;"><i class="fa fa-times-circle"></i></span>
                                        @endif
                                        <span class="text-danger city_id_err"></span>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12"  @if($userAddr->country == 134) style="display: none;" @endif id="notKuCity">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.city')</label>
                                        <input type="text" class="login_type required" name="city" id="city" placeholder="@lang('front_static.city')" @if($userAddr->country != 134) value="{{ $userAddr->city }}" @endif>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.block') <span class="optShow" @if($userAddr->country == 134) style="display: none;" @endif> (@lang('front_static.optional'))</span></label>
                                        <input type="text" class="login_type only-read @if($userAddr->country == 134) required @endif" name="block" id="block" placeholder="@lang('front_static.block')" value="{{ $userAddr->block }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.street')</label>
                                        <input type="text" class="login_type only-read required" name="street" id="street" placeholder="@lang('front_static.street')" value="{{ $userAddr->street }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12" id="postal_code_div" @if($userAddr->country == 134) style="display: none;" @endif>
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.postal_code') (@lang('front_static.optional'))</label>
                                        <input type="text" class="login_type" name="postal_code" id="postal_code" placeholder="@lang('front_static.postal_code')" value="{{ $userAddr->postal_code }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.building') <span class="optShow" @if($userAddr->country == 134) style="display: none;" @endif> (@lang('front_static.optional'))</span></label>
                                        <input type="text" class="login_type only-read @if($userAddr->country == 134) required @endif" name="building" id="building" placeholder="@lang('front_static.building')" value="{{ $userAddr->building }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.more_address_details')  (@lang('front_static.optional'))</label>
                                        <input type="text" class="login_type only-read" name="more_address" id="more_address" placeholder="@lang('front_static.more_address_details') " value="{{ $userAddr->more_address }}">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">Enter URL or Text</label>
                                        <input type="text" placeholder="Enter URL or Text" name="location" class="login_type only-read" value="{{ @$userAddr->location }}">
                                        
                                        {{-- <input type="hidden" name="lat" id="lat" value="{{ $userAddr->lat }}">
                                        <input type="hidden" name="lng" id="lng" value="{{ $userAddr->lng }}"> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="information-box">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button class="submit-for-user" type="submit" value="">@lang('front_static.save')</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        @include('includes.new_footer')
    </div>
    <!--wrapper end-->
    @endsection
    @section('scripts')
    @include('includes.scripts')

    <script>
    function initMap() {
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
        autocomplete.setTypes(['address']);

        autocomplete.addListener('place_changed', function() {
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
          $('#lat').val(place.geometry.location.lat())
          $('#lng').val(place.geometry.location.lng())
        });
    }

    $(document).ready(function() {
        $('#pac-input').blur(function() {
            if($(this).val() == '') {
                $('#lat').val('')
                $('#lng').val('')
            }
        })
    })
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API') }}&libraries=places&callback=initMap" async defer></script>

    <script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            // $('#city_id').chosen();
            $("#address_form" ).validate({
             rules:{ 
                email : {
                    required: true,
                    email:true
                },
                phone : {
                    required: true,
                    digits: true
                },
                postal_code : {
                    digits: true
                }
            },
            debug:true,            
            submitHandler: function (form) {
                var cn = $('#country').val();                
                if(cn == 134) {
                    if($('#city_id').val()) {
                        var _token = $('input[name="_token"]').val();
                        $.ajax({
                            url:"{{ route('exist.city.check') }}",
                            method:"POST",
                            data:{city:$('#city_id').val(), _token:_token},
                            dataType:'json',
                            success:function(response){
                                if(response.status == 'ERROR') {
                                    $('.city_id_err').html('@lang('front_static.city_check')');
                                    return false;
                                } else {
                                    form.submit();
                                }
                            }
                        });
                    }else{
                        console.log('second else');    
                    }
                } else {
                    form.submit();
                }
            },
            errorPlacement: function (error , element) {
                //toastr:error(error.text());
            }
        });

            $('#city_id').change(function() {
                $('.city_id_err').html('');
            });
            $('#country').change(function() {
                var cn = $(this).val();
                if(cn != 134) {
                    $('#block').removeClass('required');
                    $('#block').removeClass('error');
                    $('#building').removeClass('required');
                    $('#building').removeClass('error');
                    $('.optShow').show('required');
                    $('#postal_code_div').show();
                    $('#kuCity').hide();
                    $('#city_id').val('');
                    $('#notKuCity').show();
                } else {
                    $('#block').addClass('required');
                    $('#building').addClass('required');
                    $('.optShow').hide('required');
                    $('#postal_code_div').hide();  
                    $('#kuCity').show();
                    $('#notKuCity').hide();              
                    $('#city').val('');              
                }
            });
        });

        function validate(evt) {
          var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
        }
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>

{{-- city autocomplete --}}

<script>
    $(document).ready(function(){
        $('#city_id').keyup(function(){ 
            var city = $(this).val();
            if(city != '') {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('autocomplete.city') }}",
                    method:"POST",
                    data:{city:city, _token:_token},
                    success:function(response){
                        if(response.status == 'ERROR') {
                            $('#cityList').html('nothing found');
                        } else {
                            $('#cityList').fadeIn();  
                            $('#cityList').html(response.result);
                        }
                    }
                });
            }
        });

        $('body').on('click', 'li', function(){  
            $('#city_id').val($(this).text());  
            $('#city_id').prop('readonly', true);
            $('#cityList').hide();  
            $('#cityClr').show();
        });  
        $("body").click(function(){
            $("#cityList").hide();
        });

        $('body').on('click', '#cityClr', function(){ 
            $(this).hide();
            $('#city_id').val('');
            $('#city_id').prop('readonly', false);
        });  
    });
</script>
@endsection