@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.my_wishlist')
@endsection
@section('links')
@include('includes.links')
<style type="text/css">
    .error {
        border: solid 1px #fb8282 !important;
        background: #ff000014 !important;
    }
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="search-body">
        <div class="container">
            <div class="bed-cumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('front_static.home')</a></li>
                    <li class="breadcrumb-item">@lang('front_static.dashboard')</li>
                    <li class="breadcrumb-item active">@lang('front_static.my_wishlist')</li>
                </ol>
            </div>
            <div class="main-dash">
                @include('includes.user_sidebar')
                
                <div class="right-dashboard wishlist">
                 <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-6">
                        <div class="product-box">
                            <div class="product-image">
                                <a href="#">
                                    <img src="images/new-pro1.png" alt="">
                                </a>
                                <span class="new-sale">25% OFF</span>
                                <a class="fav-span" href="#"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                            </div>
                            <div class="product-dtls">
                                <h3>Sold by : Seller Name</h3>
                                <h4><a href="#">Camo logo print hoodie</a></h4>
                                <p><strong>$ 8100 </strong> - <span>$ 1400</span></p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-12 col-sm-6">
                        <div class="product-box">
                            <div class="product-image">
                                <a href="#">
                                    <img src="images/new-pro2.png" alt="">
                                </a>
                                <span class="new-sale">25% OFF</span>
                                <a class="fav-span" href="#"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                            </div>
                            <div class="product-dtls">
                                <h3>Sold by : Seller Name</h3>
                                <h4><a href="#">Camo logo print hoodie</a></h4>
                                <p><strong>$ 8100 </strong> - <span>$ 1400</span></p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-12 col-sm-6">
                        <div class="product-box">
                            <div class="product-image">
                                <a href="#">
                                    <img src="images/new-pro3.png" alt="">
                                </a>
                                <span class="new-sale">25% OFF</span>
                                <a class="fav-span" href="#"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                            </div>
                            <div class="product-dtls">
                                <h3>Sold by : Seller Name</h3>
                                <h4><a href="#">Camo logo print hoodie</a></h4>
                                <p><strong>$ 8100 </strong> - <span>$ 1400</span></p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-12 col-sm-6">
                        <div class="product-box">
                            <div class="product-image">
                                <a href="#">
                                    <img src="images/new-pro4.png" alt="">
                                </a>
                                <span class="new-sale">25% OFF</span>
                                <a class="fav-span" href="#"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                            </div>
                            <div class="product-dtls">
                                <h3>Sold by : Seller Name</h3>
                                <h4><a href="#">Camo logo print hoodie</a></h4>
                                <p><strong>$ 8100 </strong> - <span>$ 1400</span></p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-12 col-sm-6">
                        <div class="product-box">
                            <div class="product-image">
                                <a href="#">
                                    <img src="images/new-pro5.png" alt="">
                                </a>
                                <span class="new-sale">25% OFF</span>
                                <a class="fav-span" href="#"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                            </div>
                            <div class="product-dtls">
                                <h3>Sold by : Seller Name</h3>
                                <h4><a href="#">Camo logo print hoodie</a></h4>
                                <p><strong>$ 8100 </strong> - <span>$ 1400</span></p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-12 col-sm-6">
                        <div class="product-box">
                            <div class="product-image">
                                <a href="#">
                                    <img src="images/new-pro6.png" alt="">
                                </a>
                                <span class="new-sale">25% OFF</span>
                                <a class="fav-span" href="#"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                            </div>
                            <div class="product-dtls">
                                <h3>Sold by : Seller Name</h3>
                                <h4><a href="#">Camo logo print hoodie</a></h4>
                                <p><strong>$ 8100 </strong> - <span>$ 1400</span></p>
                            </div>
                        </div>
                    </div>
                    
                </div> 
            </div>
        </div>
    </div>
</section>
@include('includes.new_footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
<script>
    $(document).ready(function(){

        $('#password').keyup(function(event) {
            $('#new_password').addClass('required');
        });
        $("#edit_form" ).validate({
           rules:{ 

            email : {
                required: true,
                email:true
            },
            phone : {
                required: true,
                digits: true
            },
            zipcode : {
                required: true,
                digits: true
            },
            new_password : {
                minlength : 6,
                maxlength: 15
            },
            confirm_password : {          
                required: false,
                equalTo : "#new_password"
            }
        },
        errorPlacement: function (error , element) {
                //toastr:error(error.text());
            },
        });
    });
</script>
{{-- For show image --}}
<script>
    $(document).ready(function() {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').css({"width": "100px", "height": "100px"});
            $('#blah').attr('src', e.target.result);
            $('#blah').hide();
            $('#blah').fadeIn(500);
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#original").change(function(){
            readURL(this);
        });
    });
</script>
{{-- For email validation --}}
<script>
    $(document).ready(function() {


        $('#email').blur(function(event) {

            if($('#email').val() != ''){
                // alert('hi');
                $("#chck_eml_rd").html('');
                $("#chck_eml_grn").html('');
                var $this = $(this);
                var email = $(this).val();
                var re =/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(!re.test(email)) {
                    $("#chck_eml_rd").html('Please enter a valid email address');
                    $('#email').val('');
                } else {
                    var reqData = {
                        '_token': '{{ @csrf_token() }}', 
                        'params': {
                            'email': email
                        }
                    };
                    if(email != '') {
                        $.ajax({
                            url: '{{ route('user.check.new.email') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: reqData,
                        })
                        .done(function(response) {
                            if(!!response.error) {
                                $("#chck_eml_rd").html(response.error.user);
                                $('#email').val('');
                            } else {
                                $("#chck_eml_grn").html(response.result.message);
                            }
                        })
                        .fail(function(error) {
                            console.log(error);
                        });
                    }
                }     
            } else {
                $("#chck_eml_grn").html('');
            }  
        });
    });
</script>
@endsection