@extends('layouts.app')

@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.dashboard')
@endsection

@section('links')
@include('includes.links')
<style type="text/css">
  .shipping-box {
    border: 1px solid #dcdbdb;
  }
</style>
@endsection

@section('content')
<!--wrapper start-->
<div class="wrapper my_arabic">

  @include('includes.header')

  <section class="search-body">
   <div class="container">
     <div class="bed-cumb">
       <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('front_static.home')</a></li>
        <li class="breadcrumb-item active">@lang('front_static.dashboard')</li>
      </ol>
    </div>

    <div class="main-dash">
      @include('includes.user_sidebar')
      <div class="right-dashboard">
       <div class="users-info">
         <div class="row">
           <div class="col-lg-4 col-md-12 col-sm-12">
             <div class="user-info-box">
               <img src="{{ asset('public/frontend/images/user-info1.png') }}" alt="">
               <h5 style="    width: 253px;">@lang('admin_lang.your_orders')</h5>
               <!-- <p>Lorem ipsum Dolor Sit</p> -->
             </div>
           </div>
           <div class="col-lg-4 col-md-12 col-sm-12">
             <div class="user-info-box">
               <img src="{{ asset('public/frontend/images/user-info2.png') }}" alt="">
               <h5>@lang('admin_lang.login_security')</h5>
               <!-- <p>Lorem ipsum Dolor Sit</p> -->
             </div>
           </div>
           <div class="col-lg-4 col-md-12 col-sm-12">
             <div class="user-info-box">
               <img src="{{ asset('public/frontend/images/user-info3.png') }}" alt="">
               <h5>@lang('admin_lang.wishlist')</h5>
               <!-- <p>Lorem ipsum Dolor Sit</p> -->
             </div>
           </div>
           <div class="col-lg-4 col-md-12 col-sm-12">
             <div class="user-info-box">
               <img src="{{ asset('public/frontend/images/user-info4.png') }}" alt="">
               <h5>@lang('admin_lang.address')</h5>
               <!-- <p>Lorem ipsum Dolor Sit</p> -->
             </div>
           </div>
         </div>
       </div>
       @if(@$order)
       <div class="user-order-details">
         <div class="info-graw">
           <h6>@lang('admin_lang.your_recent_order')</h6>
           <p>@lang('admin_lang.order_no') : {{ @$order->order_no }}</p>
         </div>
         <div class="info-middl">
          <span>
            
            @if(@$order->orderMasterDetails[0]->productDetails->defaultImage->image)
            @php
            $image_path = 'storage/app/public/products/'.@$order->orderMasterDetails[0]->productDetails->defaultImage->image; 
            @endphp
            @if(file_exists(@$image_path))
            <img src="{{ URL::to('storage/app/public/products/'.@$order->orderMasterDetails[0]->productDetails->defaultImage->image) }}" alt="">
            @endif
            @endif
            
          </span>
           <h5>{{ @$order->orderMasterDetails[0]->productDetails->productByLanguage->title }} @php
            $attr = json_decode($order->orderMasterDetails[0]->variants,true);
            $attributes ="";
            $separator =" ";
            @endphp
            @if(@$attr)
            @foreach(@$attr as $value)
            @foreach(@$value as $key=>$v)
            @if($key == getLanguage()->id)
            @php
            $variant = $v['variant'];
            $variant_value = $v['variant_value'];
            @endphp
            @php
            $attributes = $attributes.$separator.$variant." :".$variant_value;
            @endphp
            @endif
            @endforeach
            @php
            $separator =" , ";
            @endphp
            @endforeach
            @endif
            {{ @$attributes }}</h5>
            @if(@$count != 0)
            <h6>+ {{@$count}}products</h6>
            @endif
           <!-- ->productDetails->defaultImage != null -->
            <p>@lang('admin_lang.total_payment_kwd') : <strong class="order_total_ar"> {{@$order->order_total}} KWD</strong></p>

         </div>
         <div class="info-bttns">
           <a href="{{route('order.history.order.details',@$order->order_no)}}">@lang('admin_lang.view_details')</a>
         </div>
       </div>
       @endif
       <div class="shipping-area">
         <div class="row">
           <div class="col-lg-6 col-md-12 col-sm-12">
             <div class="shipping-box">
               <h3>@lang('front_static.personal_information') <a href="{{ route('user.edit.profile') }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></h3>
               <p>{{ @Auth::user()->fname }} {{ @Auth::user()->lname }}</p>
               <p>@lang('admin_lang.email') : {{ @Auth::user()->email }}</p>
               <p>@lang('admin_lang.phone_number') : {{ @Auth::user()->phone }}</p>
               <p><a href="{{ route('user.edit.profile') }}">@lang('admin_lang.change_password') <i class="fa fa-unlock-alt" aria-hidden="true"></i></a></p>
             </div>
           </div>
           @if($userDefAddr)
           <div class="col-lg-6 col-md-12 col-sm-12">
             <div class="shipping-box">
               <h3>@lang('front_static.shipping_address') <a href="{{ route('user.edit.address.book', $userDefAddr->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></h3>
               <p>{{ $userDefAddr->shipping_fname }} {{ $userDefAddr->shipping_lname }}</p>
               <p>{{ $userDefAddr->city }}, {{ $userDefAddr->street }},
                @if($userDefAddr->block)
                {{ $userDefAddr->block }},
                @endif
                @if($userDefAddr->building) {{ $userDefAddr->building }},
                @endif
                @if($userDefAddr->more_address) {{ $userDefAddr->more_address }},@endif
                {{ @$userDefAddr->getCountry->name }}
                @if($userDefAddr->postal_code), {{ $userDefAddr->postal_code }}@endif
              </p>
              <p>@lang('admin_lang.email') : {{ $userDefAddr->email }}</p>
              <p>@lang('admin_lang.phone_number') : {{ $userDefAddr->phone }}</p>
            </div>
          </div>
          @endif
        </div>
      </div>
    </div>

  </div>
</div>
</section>



@include('includes.new_footer')
</div>
<!--wrapper end-->


@endsection

@section('scripts')
@include('includes.scripts')
@endsection