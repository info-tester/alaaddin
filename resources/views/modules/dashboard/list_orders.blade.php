@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.order_history')
@endsection
@section('links')
@include('includes.links')
<style type="text/css">
    .error {
        border: solid 1px #fb8282 !important;
        background: #ff000014 !important;
    }
</style>
@endsection
@section('content')
<!--wrapper start-->
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="search-body">
        <div class="container">
            <div class="bed-cumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">@lang('front_static.home')</a></li>
                  <li class="breadcrumb-item">@lang('front_static.dashboard')</li>
                  <li class="breadcrumb-item active">@lang('front_static.order_history')</li>
              </ol>
          </div>

          <div class="main-dash">
            @include('includes.user_sidebar')
            <div class="right-dashboard">
                @if(count(@$orders) > 0)
                @foreach(@$orders as $order)
                <div class="singel-order">
                    <span class="id-top"><a href="{{ route('order.history.order.details',@$order->order_no) }}" class="order-id">{{ @$order->order_no }}</a></span>
                    <div class="product-show-left">
                        @if(@$order->orderMasterDetails[0]->defaultImage->image)
                        <span class="ppro-img"><img src="{{url('storage/app/public/products/'.@$order->orderMasterDetails[0]->defaultImage->image)}}" alt=""></span>
                        @else
                        <span class="ppro-img">
                            <img src="{{ getDefaultImageUrl() }}" alt="">
                        </span>
                        @endif
                        <h5>{{ @$order->orderMasterDetails[0]->productDetails->productByLanguage->title }}</h5>
                        <h6>@if(@$order->orderMasterDetails->count()-1 != 0) + {{ @$order->orderMasterDetails->count()-1 }} products @endif</h6>
                        <p>@lang('admin_lang.ordered_date_time') : <span> {{ @$order->created_at }}</span></p>
                    </div>
                    <div class="qunty">
                        <p>@lang('front_static.quantity') : <span> {{ @$order->orderMasterDetails->count() }}</span></p>
                        <p>@lang('front_static.total') : <span> {{ @$order->order_total }}</span></p>
                    </div>
                    <div class="qunty delivry">
                        <p>
                            Your order is
                            @if(@$order->status == 'I')
                            Incomplete
                            @elseif(@$order->status == 'N')
                            New
                            @elseif(@$order->status == 'OA')
                            @lang('admin_lang.order_accepted')
                            @elseif(@$order->status == 'DA' || @$order->status == 'PP' || @$order->status == 'PC')
                            out for delivery
                            @elseif(@$order->status == 'RP')
                            @lang('admin_lang.ready_for_pickup')
                            @elseif(@$order->status == 'OP')
                            Picked up
                            @elseif(@$order->status == 'OD')
                            @lang('admin_lang.delivered_1')
                            @elseif(@$order->status == 'OC')
                            @lang('admin_lang.Cancelled_1')
                            @endif
                            @lang('front_static.on') : <span>{{ @$order->updated_at }} </span></p>
                            @if(@$order->status == 'OD' && @$order->is_review_done == 'N')
                                <a href="{{ route('order.history.order.details',@$order->order_no) }}">@lang('front_static.write_review')</a>
                            @endif
                            <a class="view" href="{{ route('order.history.order.details',@$order->order_no) }}">@lang('front_static.view')</a>
                            @if(@$order->status != 'OC')
                            @php
                            $orderDt = $order->created_at;
                            $date = now();
                            $current_date = $date->format('Y-m-d');
                            $cur_time = $date->format('H:i:s');

                            $order_date = $orderDt->format('Y-m-d');
                            $order_time = $orderDt->format('H:i:s');
                            $hours =  3;
                            $orderDt->add(new DateInterval("PT{$hours}H"));
                            $new_time = $orderDt->format('H:i:s');
                            $flag = 0;
                            @endphp
                            @php
                            if($current_date == $order_date){
                                if($cur_time >= $new_time){
                                    $flag = 1;
                                }else{
                                    $flag = 0;
                                }
                            }else{
                                $flag = 1;
                            }
                            @endphp
                            @endif

                            @if(@$order->status != 'OC' && @$order->status != 'DA' && @$order->status != 'OD' && @$order->status != 'RP' && @$order->status != 'OP' && @$order->status != 'PP' && @$order->status != 'PC')
                                @if($flag == 0)
                                    <a class="Cancel" href="{{ route('order.history.order.cancel',@$order->order_no) }}">@lang('front_static.cancel')</a>
                                @endif
                            @endif
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="col-lg-12 text-center">
                        <img src="{{ asset('public/frontend/no_record.png') }}" style="max-width: 100%;" alt="">
                        <h4 style="line-height: 100px;">@lang('front_static.no_records_found')</h4>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    @include('includes.new_footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
@endsection