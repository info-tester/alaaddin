@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.edit_profile')
@endsection
@section('links')
@include('includes.links')
<style type="text/css">
    .error {
        border: solid 1px #fb8282 !important;
        background: #ff000014 !important;
    }
</style>
<style type="text/css">
    label.error{
    border: none !important;
    background-color: #fff !important;
    color: #f00;
    }
    #cityList{
    position: absolute;
    width: 96%;
    /*height: 200px;*/
    z-index: 99;
    }
    #cityList ul{
    background: #fff;
    width: 96%;
    border: solid 1px #eee;
    padding: 0;
    max-height: 200px;
    overflow-y: scroll;
    }
    #cityList ul li{
    list-style: none;
    padding: 5px 15px;
    cursor: pointer;
    border-bottom: solid 1px #eee;
    }
    #cityList ul li:hover{
    background: #3a82c4;
    color: #fff;
    }
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="search-body">
        <div class="container">
            <div class="bed-cumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('front_static.home')</a></li>
                    <li class="breadcrumb-item">@lang('front_static.dashboard')</li>
                    <li class="breadcrumb-item active">@lang('front_static.edit_profile')</li>
                </ol>
            </div>
            <div class="main-dash">
                @include('includes.user_sidebar')
                <div class="right-dashboard">
                    @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                    <form method="post" action="{{ route('store.user.details') }}" enctype="multipart/form-data" id="edit_form">
                        @csrf
                        <div class="information-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>@lang('front_static.personal_information')</h3>
                                </div>

                                @if (session()->has('success'))
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="alert alert-success vd_hidden" style="display: block;">
                                        <a class="close" data-dismiss="alert" aria-hidden="true">
                                            <i class="icon-cross"></i>
                                        </a>
                                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                                    </div>
                                </div>
                                @elseif ((session()->has('error')))
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="alert alert-danger vd_hidden" style="display: block;">
                                        <a class="close" data-dismiss="alert" aria-hidden="true">
                                            <i class="icon-cross"></i>
                                        </a>
                                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                                        <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                                    </div>
                                </div>
                                @endif
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.first_name')</label>
                                        <input type="text" class="login_type required" name="fname" id="fname" placeholder="@lang('front_static.first_name')" value="{{ @Auth::user()->fname }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.last_name')</label>
                                        <input type="text" class="login_type required" name="lname" id="lname" placeholder="@lang('front_static.last_name')" value="{{ @Auth::user()->lname }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.email_address')</label>
                                        <input type="text" class="login_type only-read required" name="email" id="email" placeholder="@lang('front_static.email_address')" value="{{ @Auth::user()->email }}">
                                        @if(@Auth::user()->tmp_email != 'null' && @Auth::user()->tmp_email != '')
                                            <span class="text-success">
                                                {{ @Auth::user()->tmp_email }}
                                            </span><a class="badge badge-danger text-white" href="{{ route('resend.email') }}">@lang('front_static.verify')</a>
                                        @endif
                                        <span id="chck_eml_rd" class="chck_eml_rd text-danger"></span>
                                        <span id="chck_eml_grn" class="chck_eml_grn text-success"></span>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.mobile_number')</label>
                                        <input type="text" class="login_type required" name="phone" id="phone" placeholder="@lang('front_static.mobile_number')" value="{{ @Auth::user()->phone }}" onkeypress='validate(event)'>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="information-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>@lang('front_static.address_information')</h3>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.address')</label>
                                        <input type="text" class="login_type required" name="address" id="address" placeholder="@lang('front_static.address')" value="{{ @Auth::user()->address }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 outside_kuwait_city" @if( @Auth::user()->country == 134) style="display:none;" @else style="display:block;" @endif>
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.city')</label>
                                        <input type="text" class="login_type required" name="city" id="city" placeholder="@lang('front_static.city')" value="{{ @Auth::user()->city }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12  inside_kuwait_city" @if(@Auth::user()->country == 134) style="display:block;" @else style="display:none;" @endif>
                                    <label for="kuwait_city_lb" class="col-form-label">@lang('admin_lang.city')(@lang('admin_lang.required'))</label>
                                    <input type="text" class="form-control required insideKuwCity" name="kuwait_city" id="kuwait_city" placeholder="@lang('admin_lang.city')" value="{{@$customer->userCityDetails->name }}" autocomplete="off">
                                    <div id="cityList"></div>
                                    <input type="hidden" id="kuwait_city_value_id" name="kuwait_city_value_id" value="{{@$customer->userCityDetails->city_id }}">
                                    <input type="hidden" id="kuwait_city_value_name" name="kuwait_city_value_name" value="{{@$customer->userCityDetails->name }}">
                                    <span class="error_kuwait_city removeText city_id_err text-danger" ></span>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.state')</label>
                                        <input type="text" class="login_type only-read required" name="state" id="state" placeholder="@lang('front_static.state')" value="{{ @Auth::user()->state }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.zip_code')</label>
                                        <input type="text" class="login_type required" name="zipcode" id="zipcode" placeholder="@lang('front_static.zip_code')" value="{{ @Auth::user()->zipcode }}">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.country')</label>
                                        <select class="login_type login-select required" name="country" id="country">
                                            <option value="">@lang('front_static.select_country')</option>
                                            @foreach($country as $cn)
                                            <option value="{{ $cn->id }}" @if($cn->id == @Auth::user()->country) selected="" @elseif($cn->status == 'I') style="display:none;"  @endif>{{ @$cn->countryDetailsBylanguage->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        <div class="information-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>@lang('front_static.profile_picture')</h3>
                                </div>
                                <div class="col-sm-12">
                                    <div id="file-upload-cont">
                                        <input id="original" type="file" name="image">
                                        <span id="overlay" class="upldicn">@lang('front_static.upload_your_profile_image')</span>
                                    </div>
                                    <div class="uploaded_ppc" >
                                        @if(@Auth::user()->image)
                                        <img id="blah" src="{{url('storage/app/public/customer/profile_pics/'.@Auth::user()->image)}}" style="height: 100px; width: 100px;" />
                                        @else
                                        <img id="blah" />
                                        @endif
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <h3>@lang('front_static.password_information')</h3>
                                </div>
                                @if(@Auth::user()->password)
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <div class="one-main">
                                            <label class="from-label">@lang('front_static.password')</label>
                                            <input type="password" class="login_type" name="password" id="password" placeholder="@lang('front_static.password')" autocomplete="off">
                                        </div>
                                    </div>
                                @endif
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.new_password')</label>
                                        <input type="password" class="login_type" name="new_password" id="new_password" placeholder="@lang('front_static.new_password')" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="one-main">
                                        <label class="from-label">@lang('front_static.confirm_password')</label>
                                        <input type="password" class="login_type only-read" name="confirm_password" id="confirm_password" placeholder="@lang('front_static.confirm_password')" autocomplete="off">
                                    </div>
                                </div>


                                <div class="col-sm-12">
                                    <button class="submit-for-user" type="submit" value="">@lang('front_static.save_changes')</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @include('includes.new_footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
<script>
    $(document).ready(function(){
        $('#new_password').removeClass('required');
        $("#password").val("");
        $("#new_password").val("");
        $("#confirm_password").val("");
        $('#password').blur(function(event) {
            var pp = $(this).val();
            if(pp != ""){
                $('#new_password').addClass('required');    
                $('#confirm_password').addClass('required');    
            }else{
                $('#new_password').removeClass('required');    
                $('#confirm_password').removeClass('required');    
            }
            
        });
        $('#new_password').blur(function(event) {
            var pp = $(this).val();
            if(pp != ""){
                $('#password').addClass('required');    
                $('#confirm_password').addClass('required');    
            }else{
                $('#password').removeClass('required');    
                $('#confirm_password').removeClass('required');    
            }
            
        });
        $('#confirm_password').blur(function(event) {
            var pp = $(this).val();
            if(pp != ""){
                $('#password').addClass('required');    
                $('#new_password').addClass('required');    
            }else{
                $('#password').removeClass('required');    
                $('#new_password').removeClass('required');    
            }
            
        });
        $('#country').change(function(){
            var country = $(this).val();
            $("#kuwait_city").val("");
            $("#city").val("");
            if(country == 134){
                $(".outside_kuwait_city").hide();
                $(".inside_kuwait_city").show();
                $("#city").removeClass("required");
                $("#kuwait_city").addClass("required");
            }else{
                $(".outside_kuwait_city").show();
                $(".inside_kuwait_city").hide();
                $("#kuwait_city").removeClass("required");
                $("#city").addClass("required");
            }
        });
        $('#kuwait_city').keyup(function () {
        var city = $(this).val();
        // alert(city);
        if (city != '') {
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('fetch.kuwait.customer.details') }}",
                method: "POST",
                data: {
                    city: city,
                    _token: _token
                },
                success: function (response) {
                    if (response.error) {
                        // alert("error");
                    } 
                    else
                    {
                        // alert("success");
                        var cityHtml = '<ul><li data-id="" class="kuwait_cities" data-nm="">Select City</li>';
                        response.result.cities.forEach(function (item, index) {
                            cityHtml = cityHtml + '<li class="kuwait_cities" data-id="' + item.city_details_by_language.city_id + '" data-nm="' + item.city_details_by_language.name + '">' + item.city_details_by_language.name + '</li>';
                        })
                        cityHtml = cityHtml + '</ul>';

                        $('#cityList').show();
                        $('#cityList').html(cityHtml);
                    }

                }
            });
        }

    });
    $(document).on("keyup", "#kuwait_city", function () {
        $("#k_cityname").val($.trim($("#kuwait_city").val()));
        var value = $.trim($(this).val());
        if (value == "") {
            $("#kuwait_city_value_id").val("");
        }
    });
    $(document).on("blur", "#kuwait_city", function () {
        var id = $.trim($("#kuwait_city_value_id").val());
        var name = $.trim($("#kuwait_city_value_name").val());
        var city = $.trim($("#kuwait_city").val());
        if (city != name) {
            $("#kuwait_city").val("");
            $("#kuwait_city_value_id").val("");
            $("#kuwait_city_value_name").val("");
            $(".city_id_err").text("@lang('admin_lang.please_select_name_city')");
        }else 
        {
            $(".city_id_err").text("");
        }
    });
    $('body').on('click', '.cityChange', function () {
        $('#kuwait_city').val($(this).text());
        $('#cityList').fadeOut();
    });
    $("body").click(function () {
        $("#cityList").fadeOut();
    });
    $(document).on("click", ".kuwait_cities", function (e) {
        var id = $(e.currentTarget).attr("data-id");
        $("#kuwait_city_value_id").val(id);
    });
    $(document).on("click", ".kuwait_cities", function (e) {
        var id = $(e.currentTarget).attr("data-id");
        var name = $(e.currentTarget).attr("data-nm");
        $("#kuwait_city_value_id").val(id);
        $("#kuwait_city_value_name").val(name);

        $("#kuwait_city").val(name);
        $(".city_id_err").text("");
        $("error_kuwait_city").text("");
    });
        $("#edit_form" ).validate({
         rules:{ 

            email : {
                required: true,
                email:true
            },
            phone : {
                required: true,
                digits: true
            },
            zipcode : {
                required: true,
                digits: true
            },
            new_password : {
                minlength : 6,
                maxlength: 15
            },
            confirm_password : { 
                equalTo : "#new_password"
            }
        },
        errorPlacement: function (error , element) {
                //toastr:error(error.text());
            },
        });
    });
</script>
{{-- For show image --}}
<script>
    $(document).ready(function() {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').css({"width": "100px", "height": "100px"});
            $('#blah').attr('src', e.target.result);
            $('#blah').hide();
            $('#blah').fadeIn(500);
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#original").change(function(){
            readURL(this);
        });
    });
</script>
{{-- For email validation --}}
<script>
    $(document).ready(function() {


        $('#email').blur(function(event) {

            if($('#email').val() != ''){
                // alert('hi');
                $("#chck_eml_rd").html('');
                $("#chck_eml_grn").html('');
                var $this = $(this);
                var email = $(this).val();
                var re =/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(!re.test(email)) {
                    $("#chck_eml_rd").html("@lang('admin_lang.email_validate_required')");
                    $('#email').val('');
                } else {
                    var reqData = {
                        '_token': '{{ @csrf_token() }}', 
                        'params': {
                            'email': email
                        }
                    };
                    if(email != '') {
                        $.ajax({
                            url: '{{ route('user.check.new.email') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: reqData,
                        })
                        .done(function(response) {
                            if(!!response.error) {
                                $("#chck_eml_rd").html(response.error.user);
                                $('#email').val('');
                            } else {
                                // $("#chck_eml_grn").html(response.result.message);
                            }
                        })
                        .fail(function(error) {
                            console.log(error);
                        });
                    }
                }     
            } else {
                $("#chck_eml_grn").html('');
            }  
        });
    });

    function validate(evt) {
        var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          }
          var regex = /[0-9]|\./;
          if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
</script>
@endsection