@extends('layouts.app')

@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.rewards_point')
@endsection

@section('links')
@include('includes.links')
@endsection

@section('content')
<!--wrapper start-->
<div class="wrapper">

  @include('includes.header')

  <section class="search-body">
    <div class="container">
        <div class="bed-cumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('front_static.home')</a></li>
                <li class="breadcrumb-item">@lang('front_static.dashboard')</li>
                <li class="breadcrumb-item active">@lang('front_static.rewards_point')</li>
            </ol>
        </div>

        <div class="main-dash">
            @include('includes.user_sidebar')
            <div class="totalrwrd">
                <h5>@lang('front_static.points_earned')</h5>
                <i><img src="{{ asset('public/frontend/images/rwrd.png') }}" alt=""></i>
                <strong>{{ @Auth::user()->loyalty_total }}</strong>
                <ul>
                    <li><p>@lang('front_static.points_used') : <span>{{ @Auth::user()->loyalty_used }}</span></li>
                    <li>|</li>
                    <li><p>@lang('front_static.points_available') : <span>{{ @Auth::user()->loyalty_balance }}</span></p></li>
                </ul>
            </div>
                
            <div class="right-dashboard">
                <div class="rwrdash">
                    <div class="row">
                        <div class="col-sm-12">
                            @if(count($rewards) > 0)
                            <div class="table-responsive">
                                <div class="table">
                                    <div class="one_row1 hidden-sm-down only_shawo">
                                        <div class="cell1 tab_head_sheet2">@lang('front_static.order_no') </div>
                                        <div class="cell1 tab_head_sheet2">@lang('front_static.date')</div>
                                        <div class="cell1 tab_head_sheet2">@lang('front_static.points_received')</div>
                                        <div class="cell1 tab_head_sheet2">@lang('front_static.point_redeemed')</div>
                                        <div class="cell1 tab_head_sheet2">@lang('front_static.balance')</div>
                                    </div>
                                    @foreach($rewards as $rewards) 
                                    <div class="one_row1 small_screen31">
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('front_static.order_no')</span>
                                            <p class="add_ttrr"><a href="{{ route('order.history.order.details',@$rewards->order_no) }}">{{ $rewards->order_no }}</a></p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('front_static.date')</span>
                                            <p class="add_ttrr">{{ date('d/m/Y', strtotime($rewards->created_at->toDateString())) }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('front_static.points_received')</span>
                                            <p class="add_ttrr">+{{ $rewards->loyalty_point_received }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('front_static.point_redeem')</span>
                                            <p class="add_ttrr">-{{ $rewards->loyalty_point_used }}</p>
                                        </div>
                                        <div class="cell1 tab_head_sheet_1">
                                            <span class="W55_1">@lang('front_static.balance')</span>
                                            <p class="add_ttrr">{{ @Auth::user()->loyalty_balance }}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @else
                            <div class="col-lg-12 text-center">
                                <img src="{{ asset('public/frontend/no_record.png') }}" style="max-width: 100%;" alt="">
                                <h4 style="line-height: 100px; text-align: center;">@lang('front_static.no_records_found')</h4>
                            </div>
                            @endif
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</section>



@include('includes.new_footer')
</div>
<!--wrapper end-->


@endsection

@section('scripts')
@include('includes.scripts')
@endsection