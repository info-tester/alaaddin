@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.help')
@endsection
@section('links')
@include('includes.links')
<style type="text/css">
    .abut-sec h6 {
    margin-top: 17px;
    }
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="search-body shoping-cart-body">
        <div class="container">
            <div class="bed-cumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                    <li class="breadcrumb-item active">@lang('front_static.help')</li>
                </ol>
            </div>
            <div class="">
                <div class="abutt">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <h6 style="margin-top: 0;">{!! strip_tags(@$help->title) !!}</h6>
                            <p>{!! @$help->description !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('includes.footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
@endsection