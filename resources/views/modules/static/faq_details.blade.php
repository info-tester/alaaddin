@extends('layouts.app')

@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.faq')
@endsection

@section('links')
@include('includes.links')
<style type="text/css">
  
</style>
@endsection

@section('content')
<!--wrapper start-->
<div class="wrapper">

  @include('includes.header')

  

<section class="search-body shoping-cart-body">
    <div class="container">
        <div class="bed-cumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('admin_lang.home')</a></li>
                <li class="breadcrumb-item active">@lang('admin_lang.Faq')</li>
            </ol>
        </div>
        <div class="main-dash">
            <div class="left-dash">
                <!-- <div class="mobile_filter">
                    <i class="fa fa-filter" aria-hidden="true"></i>
                    <p>Show user menu</p>
                </div> -->
                <div class="user-menus2">
                    <ul>
                        @foreach($faqcat as $faqcatname)
                        <li class="active">
                            <a href="{{ route('faq.details',@$faqcatname->id) }}">
                                <span><i><img src="{{ asset('public/frontend/images/f1.png') }}"></i></span> 
                                <strong>{{ @$faqcatname->categoryDetailsByLanguage[0]->faq_category }}</strong>
                            </a>
                        </li>


                        @endforeach
                        <!-- <li><a href="javascript:void(0);"><span><i><img src="{{ asset('public/frontend/images/f2.png') }}"></i></span> <strong>Non Order Related</strong></a></li>
                        <li><a href="javascript:void(0);"><span><i><img src="{{ asset('public/frontend/images/f3.png') }}"></i></span> <strong>Cancellations & Returns</strong></a></li>
                        <li ><a href="javascript:void(0);"><span><i><img src="{{ asset('public/frontend/images/f4.png') }}"></i></span> <strong>Wallet</strong></a></li>
                        <li><a href="javascript:void(0);"><span><i><img src="{{ asset('public/frontend/images/f5.png') }}"></i></span> <strong>Shopping</strong></a></li>
                        <li><a href="javascript:void(0);"><span><i><img src="{{ asset('public/frontend/images/f6.png') }}"></i></span> <strong>Payment</strong></a></li>
                        <li><a href="javascript:void(0);"><span><i><img src="{{ asset('public/frontend/images/f7.png') }}"></i></span> <strong>Others</strong></a></li> -->
                    

                    </ul>
                </div>
            </div>
            <div class="right-dashboard2">
                <ul id="accordion" class="accordion">

                    @foreach($faqdetails as $details)
                    <li>
                        <div class="link">{{@$details->faqDetailsByLanguage->faq_ques}}<i class="fa fa-chevron-down"></i></div>
                        <ul class="submenu">
                            <p class="pm">{{@$details->faqDetailsByLanguage->faq_answer}}</p>
                        </ul>
                    </li>

                    

                    @endforeach
                </ul>



            </div>
        </div>
    </div>
</section>


  @include('includes.new_footer')
</div>
<!--wrapper end-->


@endsection

@section('scripts')
@include('includes.scripts')


<script>
  $(document).ready(function() {
    (function($) {
      $.fn.spinner = function() {
        console.log(this);
        this.each(function() {
          var el = $(this);

        // add elements
        el.wrap('<span class="spinner"></span>');     
        el.after('<span class="add">+</span>');
        el.before('<span class="sub">-</span>');

        // substract
        el.parent().on('click', '.sub', function () {
          if (el.val() > parseInt(el.attr('min')))
            el.val( function(i, oldval) { return --oldval; });
        });

        // increment
        el.parent().on('click', '.add', function () {
          if (el.val() < parseInt(el.attr('max')))
            el.val( function(i, oldval) { return ++oldval; });
        });
      });
      };
    })(jQuery);
    $('input[type=number]').spinner();
  });
</script> 


<script>
  $(function() {
    var Accordion = function(el, multiple) {
      this.el = el || {};
      this.multiple = multiple || false;

    // Variables privadas
    var links = this.el.find('.link');
    // Evento
    links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
  }

  Accordion.prototype.dropdown = function(e) {
    var $el = e.data.el;
    $this = $(this),
    $next = $this.next();

    $next.slideToggle();
    $this.parent().toggleClass('open');

    if (!e.data.multiple) {
      $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
    };
  } 

  var accordion = new Accordion($('#accordion'), false);
});
</script>
@endsection