@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.contact_us')
@endsection
@section('links')
@include('includes.links')
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="search-body">
        <div class="container">
            <div class="bed-cumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('admin_lang.home')</a></li>
                    <li class="breadcrumb-item active">@lang('admin_lang.contact_us')</li>
                </ol>
            </div>
            <div class="main-dash">
                <div class="contact-sec">
                    <h3>{{$contact_content->sec_heading_contact}}</h3>
                    <div class="row rwtdh rm_for_arabicc rm01 rm02">
                        <div class="col-lg-4 col-md-4 col-sm-12 coll">
                            <div class="threebx">
                                <h4><img src="{{ asset('public/frontend/images/cht.png') }}">{{$contact_content->first_sec_first_title}}</h4>
                                <p><?= $contact_content->first_sec_first_content ?></p>
                                <a href="{{$contact_content->boxone_butn_link}}">{{$contact_content->boxone_hed_caption}}</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 coll">
                            <div class="threebx">
                                <h4><img src="{{ asset('public/frontend/images/cht2.png') }}">{{$contact_content->first_sec_second_heading}}</h4>
                                <p><?= $contact_content->first_sec_second_content ?></p>
                                <a href="{{$contact_content->boxtwo_butn_link}}">{{$contact_content->boxtwo_hed_caption}}</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 coll">
                            <div class="threebx">
                                <h4><img src="{{ asset('public/frontend/images/cht3.png') }}">{{$contact_content->first_sec_third_heading}}</h4>
                                <p><?= $contact_content->first_sec_third_content ?></p>
                                <a href="{{$contact_content->boxthree_butn_link}}">{{$contact_content->boxthree_hed_caption}}</a>
                            </div>
                        </div>
                    </div>
                    <h3>{{$contact_content->contact_us_form_heading}}</h3>
                    <div class="row rwtdh rm_for_arabicc rm02">
                        <div class="col-lg-6 col-md-6 col-sm-12 coll">
                            <div class="cnctfrm">
                                @if (session()->has('success'))
                                <div class="alert alert-success vd_hidden" style="display: block;">
                                    <a class="close" data-dismiss="alert" aria-hidden="true">
                                        <i class="icon-cross"></i>
                                    </a>
                                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                                    {{ session('success') }} 
                                </div>
                                @elseif ((session()->has('error')))
                                <div class="alert alert-danger vd_hidden" style="display: block;">
                                    <a class="close" data-dismiss="alert" aria-hidden="true">
                                        <i class="icon-cross"></i>
                                    </a>
                                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                                     {{ session('error') }} 
                                </div>
                                @endif
                                <form id="myform" method="post" action="{{ route('contact.us.formpost') }}" enctype="multipart/form-data" name="contact-form">
                                    @csrf
                                    <input type="hidden" name="recaptcha" id="recaptcha">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 coll">
                                            <div class="one-main">
                                                <select class="login_type login-select">
                                                    <option>@lang('admin_lang.please_select_subject')</option>
                                                    <option>@lang('admin_lang.subject1')</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 coll">
                                            <div class="one-main">
                                                <input type="text" class="login_type required" placeholder="@lang('admin_lang.first_name')" name="firstname">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 coll">
                                            <div class="one-main">
                                                <input type="text" class="login_type required" placeholder="@lang('admin_lang.last_name')" name="lastname">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 coll">
                                            <div class="one-main">
                                                <input type="text" class="login_type required" placeholder="@lang('admin_lang.email_address_label')" name="email">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 coll">
                                            <div class="one-main">
                                                <input type="text" class="login_type required" placeholder="@lang('admin_lang.phn_no')" name="phone">
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 coll">
                                            <div class="one-main">
                                                <textarea placeholder="Type your message" class="required" name="message"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 coll">
                                            <button class="submit-for-user g-recaptcha" type="submit"   value="">@lang('admin_lang.submit')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 coll">
                            <div class="cnctmap">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3086977.356626524!2d-104.88968320850377!3d40.94989194176094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8775809f9553c651%3A0x4e3e0f0907df1436!2sMcCook%2C%20NE%2069001%2C%20USA!5e0!3m2!1sen!2sin!4v1578487063614!5m2!1sen!2sin" width="100%" height="222" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                <span><img src="images/gmap.png"></span>
                                <p>
                                    McCook<br>
                                    Willow Grove, NE 69001, USA
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('includes.new_footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
<script src="https://www.google.com/recaptcha/api.js?render={{ config('services.recaptcha.sitekey') }}"></script>
<script>
    $(document).ready(function(){
        $("#myform").validate();
        
    });
</script>
<script>
    grecaptcha.ready(function() {
        grecaptcha.execute("{{ config('services.recaptcha.sitekey') }}", {action: "contact"}).then(function(token) {
        if (token) {
            document.getElementById('recaptcha').value = token;
        }
        });
    });
</script>
<style>
    .error{
    color: red !important;
    }
</style>
@endsection