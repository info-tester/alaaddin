@extends('layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('front_static.terms_and_conditions')
@endsection
@section('links')
@include('includes.links')
{{-- <style type="text/css">
    .abut-sec h6 {
    margin-top: 17px;
    }
</style> --}}
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
@include('includes.header')
<section class="search-body shoping-cart-body">
    <div class="container">
        <div class="bed-cumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('admin_lang.home')</a></li>
                <li class="breadcrumb-item active">@lang('front_static.terms_and_conditions')</li>
            </ol>
        </div>
        <div class="abut-sec">
            <div class="abutt">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h3>@lang('front_static.terms_and_conditions')</h3>
                            </div>
                        </div>
                        {!! $term_content->description !!}
                        
                    </div>
                </div>
            </div>
        </div>
</section>
@include('includes.footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
@endsection