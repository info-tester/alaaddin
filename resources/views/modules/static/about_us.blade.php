@extends('layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | About Us
@endsection
@section('links')
@include('includes.links')
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
@include('includes.header')
<section class="search-body shoping-cart-body">
    <div class="container">
        <div class="bed-cumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">@lang('admin_lang.home')</a></li>
                <li class="breadcrumb-item active">About Us</li>
            </ol>
        </div>
        <div class="abut-sec">
            <div class="abutt">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h3>ABOUT US</h3>
                    </div>
                </div>
                
                {!! @$about_content->about_us_description !!}
            </div>
        </div>
</section>
@include('includes.footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
@endsection