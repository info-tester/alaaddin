@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('front_static.order_placement')
@endsection
@section('links')
@include('includes.links')
<link href="{{ asset('public/frontend/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .error {
        border: solid 1px #fb8282 !important;
        background: #ff000014 !important;
    }
    .check-out-bttns button {
        color: #fff;
        font-size: 16px;
        font-weight: 500;
        background: #0771d4;
        font-family: 'Poppins', sans-serif;
        padding: 8px 10px;
        display: inline-block;
        margin-left: 9px;
        border-radius: 2px;
        border: none;
        cursor: pointer;
    }
    .check-out-bttns button:hover {
        color: #fff;
        background: #000 !important;
    }
    .apply_coupon{
        color: #ef1600;
        cursor: pointer;
    }
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    
    <section class="search-body shoping-cart-body">
        <div class="container">
            <div class="bed-cumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">@lang('front_static.home')</a></li>
                    <li class="breadcrumb-item ">@lang('front_static.my_cart')</li>
                    <li class="breadcrumb-item">@lang('front_static.check_out')</li>
                    <li class="breadcrumb-item active">@lang('front_static.order_placement')</li>
                </ol>
            </div>
          
            <div class="main-dash">
                <form action="{{ route('place.order',$orderNo) }}" method="post">
                    @csrf
                    <h4 class="pls-rev">@lang('front_static.please_review_your_order')</h4>
                    <div class="cart-tabel modify-cart-table">
                        <div class="table-responsive">
                            <div class="table">
                                <div class="one_row1 hidden-sm-down only_shawo">
                                    <div class="cell1 tab_head_sheet">@lang('front_static.product_details')</div>
                                    <div class="cell1 tab_head_sheet">@lang('front_static.seller')</div>
                                    <div class="cell1 tab_head_sheet">@lang('front_static.quantity')</div>
                                    <div class="cell1 tab_head_sheet">@lang('front_static.unit_price') </div>
                                    <div class="cell1 tab_head_sheet">@lang('front_static.total')</div>
                                </div>                            
                                @foreach(@$orderDetails as $od)    
                                <div class="one_row1 small_screen31">
                                    <div class="cell1 tab_head_sheet_1 lft">
                                        <span class="W55_1">@lang('front_static.product_details')</span>
                                        <span class="add_ttrr">
                                            <a href="{{ route('product',@$od->productDetails->slug) }}"> 
                                                @if(@$od->defaultImage->image)
                                                <span class="tabel-image"><img src="{{url('storage/app/public/products/'.@$od->defaultImage->image)}}" alt=""></span>
                                                @else
                                                <span class="tabel-image">
                                                    <img src="{{ getDefaultImageUrl() }}" alt="">
                                                </span>
                                                @endif
                                            </a>
                                            <a href="{{ route('product',@$od->productDetails->slug) }}"> 
                                                <h5>{{ $od->productByLanguage->title }}</h5>
                                            </a>
                                            @if(json_decode($od->variants) != NULL)
                                            @foreach(json_decode($od->variants) as $row)
                                            <h6>{{ $row->{$language_id}->variant }} : {{ $row->{$language_id}->variant_value }}</h6>
                                            @endforeach
                                            @endif
                                        </span>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('front_static.seller')</span>
                                        <span class="add_ttrr">
                                            @if(@$od->sellerDetails->image)
                                            <span class="seller-logo"><img src="{{url('storage/app/public/profile_pics/'.@$od->sellerDetails->image)}}" alt=""></span>
                                            @else
                                            <span class="seller-logo">
                                                <img src="{{ getDefaultImageUrl() }}" alt="">
                                            </span>
                                            @endif
                                            <h4>{{ @$od->sellerDetails->company_name }}</h4>
                                        </span>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('front_static.quantity')</span>
                                        <p class="add_ttrr">{{ $od->quantity }}</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('front_static.unit_price') </span>
                                        <p class="add_ttrr">{{ number_format($od->original_price, 3) }} {{ getCurrency() }}</p>
                                    </div>
                                    <div class="cell1 tab_head_sheet_1">
                                        <span class="W55_1">@lang('front_static.total')</span>
                                        <p class="add_ttrr">{{ number_format($od->sub_total, 3) }} {{ getCurrency() }}</p>
                                    </div>
                                </div>
                                @endforeach 
                            </div>
                        </div>
                            
                        <div class="cart-total-area check-out-sucs">
                            <div class="left-point">
                                <div class="point-border">
                                    <div style="float: left; width: 100%">
                                        @php $loyalty_discount = 0; @endphp
                                        @if(@Auth::user()->loyalty_balance > 0 && @Auth::user()->loyalty_balance >= @$setting->min_point)
                                        <h6>@lang('front_static.you_have') {{ @Auth::user()->loyalty_balance }} @lang('front_static.point_available')</h6>
                                        <p>@lang('front_static.do_you_want_to_redeem_your_point')</p>
                                        <div class="radio-custom">
                                            <p id="yes">
                                                <input type="radio" id="test1" name="reward_point" class="reward_point" value="Y" checked>
                                                <label for="test1">@lang('front_static.yes')</label>
                                            </p>
                                            <p id="no">
                                                <input type="radio" id="test2" name="reward_point" class="reward_point" value="N">
                                                <label for="test2">@lang('front_static.no')</label>
                                            </p>
                                        </div>
                                        @php
                                        
                                        if(@Auth::user()->loyalty_balance*@$setting->one_point_to_kwd < $order->order_total) {
                                            $loyalty_discount = @Auth::user()->loyalty_balance*@$setting->one_point_to_kwd;
                                        } else if($setting->max_discount < $order->order_total) {
                                            $loyalty_discount = @Auth::user()->loyalty_balance*@$setting->one_point_to_kwd;
                                        } else {
                                            $loyalty_discount = $order->order_total;
                                        }

                                        if($loyalty_discount > @$setting->max_discount) {
                                            $loyalty_discount = @$setting->max_discount;
                                            $point_used = $loyalty_discount/@$setting->one_point_to_kwd;
                                        } else {
                                            $point_used = @Auth::user()->loyalty_balance;
                                        }
                                        @endphp
                                        <input type="hidden" name="loyalty_point" value="{{ number_format(@$point_used, 3, '.', '') }}">
                                        @endif
                                    </div>
                                    <div class="coupon-area">
                                        <input type="text" name="coupon_code" id="coupon_code" class="form-control" placeholder="Enter coupon code" autocomplete="off">
                                        <input type="hidden" name="valid_coupon_code" id="valid_coupon_code">
                                        <button class="apply-btn" id="apply" type="button" title="you can click it to apply the coupoun" style="cursor: pointer;">APPLY</button>
                                        <span id="coupon_msg"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="right-totals">
                                <div class="totals">
                                    <p>@lang('front_static.subtotal') <span style="width: auto; margin-left: 5px; color: #2ea745;">{{ number_format($order->subtotal - $order->total_discount, 3) }} {{ getCurrency() }}</span> <span style="text-decoration: line-through; font-size: 12px">{{ number_format($order->subtotal, 3) }} {{ getCurrency() }}</span></p>

                                    <p>@lang('front_static.loyalty_point_discount') <span class="text-success loyalty_discount">- {{ number_format(@$loyalty_discount, 3) }} {{ getCurrency() }}</span></p>

                                    <p>Coupon Discount <span class="apply_coupon">Apply Coupon</span></p>

                                    <p>@lang('front_static.shipping_charges') <span class="shipping_charges">{{ number_format($order->shipping_price, 3) }} {{ getCurrency() }}</span></p>

                                    <p>@lang('front_static.total_discount') <span class="text-success total_discount"> - {{ number_format($order->total_discount + @$loyalty_discount, 3) }} {{ getCurrency() }}</span></p>

                                    <div class="borders"></div>
                                    <h4>@lang('front_static.total') <span class="order_total">{{ number_format($order->order_total-number_format(@$loyalty_discount, 3), 3) }} {{ getCurrency() }}</span></h4>
                                    <div class="borders"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="address-bbox">
                        <div class="head-adrs-box">
                            <h4>@lang('front_static.shipping_information')</h4>
                            @auth
                            <a href="{{ route('user.edit.address',$orderNo) }}"><i class="fa fa-pencil" aria-hidden="true"></i> @lang('front_static.edit')</a>
                            @else
                            <a href="{{ route('guest.edit.address',$orderNo) }}"><i class="fa fa-pencil" aria-hidden="true"></i> @lang('front_static.edit')</a>
                            @endauth
                        </div>
                        <div class="address-dtls">
                            <p><span>@lang('front_static.name')</span> {{ $order->shipping_fname }} {{ $order->shipping_lname }}</p>
                            <p><span>@lang('front_static.email_address')</span>{{ $order->shipping_email }}</p>
                            <p><span>@lang('front_static.phone_number')</span> {{ $order->shipping_phone }}</p>
                            <p><span>@lang('front_static.country')</span> {{ @$order->getCountry->name }}</p>
                            {{-- <p><span>State</span> {{ $order->shipping_state }}</p> --}}
                            <p><span>@lang('front_static.city')</span>{{ $order->shipping_city }}</p>
                            @if($order->shipping_zip)
                            <p><span>@lang('front_static.postal_code')</span> {{ $order->shipping_zip }}</p>
                            @endif
                            <p><span>@lang('front_static.full_address')</span> 
                                <strong>
                                    {{ $order->shipping_city }}, {{ $order->shipping_street }},
                                    @if($order->shipping_block)
                                    {{ $order->shipping_block }},
                                    @endif
                                    @if($order->shipping_building) {{ $order->shipping_building }},
                                    @endif
                                    @if($order->shipping_more_address) {{ $order->shipping_more_address }},@endif
                                    {{ @$order->getCountry->name }}
                                    @if($order->shipping_postal_code), {{ $order->shipping_postal_code }}@endif
                                </strong>
                            </p>
                        </div>
                    </div>

                    <div class="address-bbox" style="margin-right:0px;">
                        <div class="head-adrs-box">
                            <h4>@lang('front_static.billing_address')</h4>
                            @auth
                            <a href="{{ route('user.edit.address',$orderNo) }}"><i class="fa fa-pencil" aria-hidden="true"></i> @lang('front_static.edit')</a>
                            @else
                            <a href="{{ route('guest.edit.address',$orderNo) }}"><i class="fa fa-pencil" aria-hidden="true"></i> @lang('front_static.edit')</a>
                            @endauth
                        </div>
                        <div class="address-dtls">
                            <p><span>@lang('front_static.name')</span> {{ $order->billing_fname }} {{ $order->billing_lname }}</p>
                            <p><span>@lang('front_static.email_address')</span>{{ $order->billing_email }}</p>
                            <p><span>@lang('front_static.phone_number')</span> {{ $order->billing_phone }}</p>
                            <p><span>@lang('front_static.country')</span> {{ @$order->getBillingCountry->name }}</p>
                            {{-- <p><span>State</span> {{ $order->billing_state }}</p> --}}
                            <p><span>@lang('front_static.city')</span>{{ $order->billing_city }}</p>
                            @if($order->billing_zip)
                            <p><span>@lang('front_static.postal_code')</span> {{ $order->billing_zip }}</p>
                            @endif
                            <p><span>@lang('front_static.full_address')</span> 
                                <strong>
                                {{ $order->billing_city }}, {{ $order->billing_street }},
                                @if($order->billing_block)
                                {{ $order->billing_block }},
                                @endif
                                @if($order->billing_building) {{ $order->billing_building }},
                                @endif
                                @if($order->billing_more_address) {{ $order->billing_more_address }},@endif
                                {{ @$order->getBillingCountry->name }}
                                @if($order->billing_postal_code), {{ $order->billing_postal_code }}@endif
                                </strong>
                            </p>
                        </div>
                    </div>

                    <div class="payment-method">
                        <h4>@lang('front_static.payment_method') : <span>@if($order->payment_method=='C') @lang('front_static.cash_on_delivery') @else @lang('front_static.online_payment') @endif</span></h4>
                    </div>

                    <div class="check-out-bttns chk-page">
                        @if($order->payment_method=='C')
                        <button type="submit" class="proc">@lang('front_static.confirm_and_place_order')</button>
                        @else
                        <button type="submit" class="proc">@lang('front_static.confirm_and_pay')</button>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </section>
    @include('includes.new_footer')
</div>
    <!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
<script src="{{ asset('public/frontend/toastr/toastr.js') }}"></script>
<script>
    $(document).ready(function() {
        var loyalty_discount        = parseFloat('{{ @$loyalty_discount }}')
        console.log(loyalty_discount)
        if(loyalty_discount > 0) {
            appliedLoyalty = true
        }
        var total_discount          = parseFloat('{{ $order->total_discount }}') + loyalty_discount
        var shipping_price          = parseFloat('{{ $order->shipping_price }}')
        var order_total             = parseFloat('{{ $order->order_total }}') - loyalty_discount
        var coupon_discount_on_sub  = 0;
        var coupon_discount_on_sip  = 0;
        var appliedLoyalty          = false;
        
        $('.reward_point').click(function(){
            if($(this).val() == 'Y') {
                loyalty_discount = parseFloat('{{ @$loyalty_discount }}')
                total_discount = parseFloat(total_discount) + parseFloat(loyalty_discount)
                order_total = order_total - loyalty_discount
                appliedLoyalty = true
                $('.loyalty_discount').html('- ' + parseFloat(loyalty_discount).toFixed(3) + ' KWD');
                $('.total_discount').html('- ' + (total_discount + coupon_discount_on_sub).toFixed(3) + ' KWD');
                $('.order_total').html((order_total - coupon_discount_on_sub).toFixed(3) + ' KWD');
            } else {
                total_discount = parseFloat(total_discount) - parseFloat(loyalty_discount)
                order_total = order_total + loyalty_discount
                loyalty_discount = 0;
                appliedLoyalty = false;
                $('.loyalty_discount').html('- ' + loyalty_discount.toFixed(3) + ' KWD');
                $('.total_discount').html('- ' + (total_discount + coupon_discount_on_sub).toFixed(3) + ' KWD');
                $('.order_total').html((order_total - coupon_discount_on_sub).toFixed(3) + ' KWD');
            }
        });
        
        $('#apply').click(function() {
            var couponCode = $('#coupon_code').val()
            var reqData = {
                jsonrpc: '2.0',
                _token: '{{ csrf_token() }}',
                params: {
                    order_id: '{{ $order->id }}',
                    coupon_code: couponCode
                }
            }
            $.ajax({
                url: '{{ route('apply.coupon') }}',
                type: 'POST',
                data: reqData,
                success: function(response) {
                    if(response.error) {
                        $('#coupon_msg').html('<i class="fa fa-exclamation-circle"></i> ' + response.error.message);
                        $('#coupon_msg').css('color', '#f00')
                        $('#coupon_code').val('')
                    } else {
                        $('#valid_coupon_code').val(couponCode)
                        if(response.result.discount_for == 'SUB') {
                            coupon_discount_on_sub = parseFloat(response.result.coupon_discount)
                            $('#coupon_msg').html('<i class="fa fa-check"></i> Get ' + (coupon_discount_on_sub).toFixed(3) + ' KWD OFF on subtotal!')
                            $('#coupon_msg').css('color', '#2ea745')
                            if(appliedLoyalty == false) {
                                $('.total_discount').html('- ' + (parseFloat(total_discount) + parseFloat(coupon_discount_on_sub)).toFixed(3) + ' KWD');
                                $('.order_total').html((order_total - coupon_discount_on_sub).toFixed(3) + ' KWD')
                            } else {
                                $('.total_discount').html('- ' + (parseFloat(total_discount) + parseFloat(loyalty_discount) + parseFloat(coupon_discount_on_sub)).toFixed(3) + ' KWD');
                                $('.order_total').html((order_total - coupon_discount_on_sub).toFixed(3) + ' KWD')
                            }
                        } else if(response.result.discount_for == 'SHI') {
                            coupon_discount_on_sip = response.result.coupon_discount
                            if((shipping_price - coupon_discount_on_sip) == 0) {
                                $('.shipping_charges').html('<span style="width: auto; float: left; text-decoration: line-through;">' + (shipping_price).toFixed(3) + '</span> <span style="width: auto">FREE</span>')    
                            } else {
                                $('.shipping_charges').html('<span style="width: auto; float: left; text-decoration: line-through;">' + (shipping_price).toFixed(3) + '</span> <span style="width: auto">' + (shipping_price - coupon_discount_on_sip).toFixed(3) + '</span>')    
                            }
                            $('.order_total').html((order_total - coupon_discount_on_sip).toFixed(3) + ' KWD')
                            if(coupon_discount_on_sip == 100) {
                                $('#coupon_msg').html('<i class="fa fa-check"></i> Get free shipping')
                                $('#coupon_msg').css('color', '#2ea745')
                            } else if(shipping_price == coupon_discount_on_sip) {
                                $('#coupon_msg').html('<i class="fa fa-check"></i> Get free shipping')
                                $('#coupon_msg').css('color', '#2ea745')
                            } else {
                                $('#coupon_msg').html('<i class="fa fa-check"></i> Get ' + (coupon_discount_on_sip).toFixed(3) + ' KWD OFF on shipping fees!')
                                $('#coupon_msg').css('color', '#2ea745')    
                            }
                        }
                        $('.apply_coupon').html('- ' + parseFloat(response.result.coupon_discount).toFixed(3) + ' KWD')
                        $('.apply_coupon').css('color', '#28a745')
                    }
                }
            })
        });

        $('.apply_coupon').click(function() {
            $('#coupon_code').focus();
        })
    });

    $(document).ready(function() {
        (function($) {
            $.fn.spinner = function() {
                this.each(function() {
                    var el = $(this);
                    // add elements
                    el.wrap('<span class="spinner"></span>');
                    el.after('<span class="add">+</span>');
                    el.before('<span class="sub">-</span>');

                    // substract
                    el.parent().on('click', '.sub', function () {
                      if (el.val() > parseInt(el.attr('min')))
                        el.val( function(i, oldval) { return --oldval; });
                    });

                    // increment
                    el.parent().on('click', '.add', function () {
                      if (el.val() < parseInt(el.attr('max')))
                        el.val( function(i, oldval) { return ++oldval; });
                    });
                });
            };
        })(jQuery);
        $('input[type=number]').spinner();
    });
    $(document).ready(function() {
        $('.ship_address').click(function(){
            if($(this).is(':checked')) {
                if($(this).val()==1) {
                    $('.new1').show();
                    $('.save1').hide();
                }
                if($(this).val()==2) {
                    $('.new1').hide();
                    $('.save1').show();
                }
            }
        });
        $('.bill_address').click(function(){
            if($(this).is(':checked')) {
                if($(this).val()==1) {
                    $('.new2').show();
                    $('.save2').hide();
                }
                if($(this).val()==2) {
                    $('.new2').hide();
                    $('.save2').show();
                }
            }
        });
        
    });
</script>
@endsection