@extends('layouts.app')
@section('title')
{{ config('app.name', 'Aswagna') }} | Order Failed
@endsection
@section('links')
@include('includes.links')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />
<style>
.order_h{
    text-align: center !important;
    margin-top: 20px;
} 
.order_p {
    text-align:justify !important;
}
.order_i{
    text-align: center !important;
    background: #ef1600;
    padding: 10px 16px 12px 16px;
    border-radius: 50%;
    color: #ffffff;
    font-size: 50px;
}
.login_sec{
    text-align: center;
}
.login_body p a {
    color: #007bff !important;
}
.guest_success i{
    position: absolute;
    right: 13px;
    top: 27px;
    font-size: 19px;
    cursor: pointer;
}
</style>
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="banner">
        <div class="sgnupbnr">
            <div class="container">
                <div class="login_body">
                    <div class="login_sec" style="min-height: auto;">
                        <i class="order_i fa fa-times" aria-hidden="true"></i>
                        <h3 class="order_h">Payment Failed!</h3>
                        <p class="order_p">Your payment was failed. If any amount has been deducted from your account then it will refund to your account wintin 7 days. For further assistance <a href="{{ route('contact.us') }}">contact us </a>.<br><strong class="text-center">Ref No.: {{ $ref_id }}</strong></p>
                        <button type="submit" class="login_submit" onclick="location.href='/'">Home</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('includes.new_footer')
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')

@endsection