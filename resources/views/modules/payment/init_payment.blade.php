@extends('layouts.app')

@section('title')
{{ config('app.name', 'Aswagna') }} | Make Secure Payment
@endsection

@section('links')
@include('includes.links')
<link href="{{ asset('public/frontend/css/ninja-slider.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('public/frontend/css/thumbnail-slider.css') }}" rel="stylesheet" type="text/css">
{{-- <link rel="stylesheet" href="{{ asset('public/css/app.css') }}"> --}}
<link rel="stylesheet" href="{{ asset('public/css/style.css') }}">

<style type="text/css">


    .payment-method {
      /*display: block;*/
      margin-bottom: 30px;
      justify-content: space-between;
  }

  .method {
      display: flex;
      flex-direction: column;
      width: 382px;
      height: 122px;
      padding-top: 20px;
      cursor: pointer;
      border: 1px solid transparent;
      border-radius: 2px;
      background-color: rgb(249, 249, 249);
      justify-content: center;
      align-items: center;
  }

  .card-logos {
      display: flex;
      width: 400px;
      justify-content: space-between;
      align-items: center;
  }

  .card-logos ul li{
    float: left;
}
.creditly-card-form{
    float: left;
}

.payment_faild{
        width: 400px;
    background: #fff6f6;
    color: #de2020;
    height: 220px;
    padding: 30px 40px;
    border: #f00 solid 1px;
    margin: 0 auto;
    margin-top: 100px;
}
.payment_faild i{
        font-size: 50px;
}
.payment_failed p{
    font-size: 15px;
    font-weight: 600;
}
.payment_failed small{
    font-weight: 600;
}


.payment_method{
    text-align: center;
}
.payment_method ul{
    text-align: left;
}
.payment_method ul li {
    border: 1px solid #eee;
    padding: 5px;
    cursor: pointer;
}
.payment_method ul li:hover {
    background: #eee;
}
.payment_method ul li img {
    width: 100px;
    margin-right: 18px;
}
.payment_method ul li i {
    text-align: right;
    float: right;
    margin-top: 4%;
    margin-right: 29px;
    font-size: 21px;
    color: #04bb04;
    display: none;
}
</style>
<link rel="stylesheet" href="public/css/creditly.css">
@endsection

@section('content')
<!--wrapper start-->
<div class="wrapper">
    @include('includes.header')
    <section class="search-body shoping-cart-body">
        <div class="container">

            <div class="main-dash head-dash">

                <div class="section-header">
                    <h2><span>@lang('admin_lang.secure_payment')</span></h2>
                    <span class="section-header-line"></span>
                </div>
                <div class="payment-method payment-div">
                    <label for="card" class="method card">
                        <div class="card-logos" style="display: none">
                            <ul>
                                <li><img src="public/frontend/images/pay1.png" alt=""></li>
                                <li><img src="public/frontend/images/pay2.png" alt=""></li>
                                <li><img src="public/frontend/images/pay3.png" alt=""></li>
                                <li><img src="public/frontend/images/pay4.png" alt=""></li>
                                <li><img src="public/frontend/images/pay5.png" alt=""></li>
                                <li><img src="public/frontend/images/pay6.png" alt=""></li>
                            </ul>
                        </div>
                        <div class="radio-input">
                          Pay {{ $order->order_total }} KWD @lang('admin_lang.with_cc_dc')
                      </div>
                    </label>

                    <div class="payment_method">
                        <ul>
                            @foreach($payment_methods as $row)
                            <li class="card_type" data-id="{{ @$row->PaymentMethodId }}" data-amount="{{ @$row->TotalAmount }}"><img src="{{ $row->ImageUrl }}"> {{ $row->PaymentMethodEn }} <i class="fa fa-check"></i></li>
                            @endforeach
                        </ul>
                        <button type="button" class="btn btn-primary choose_pay_method" style="margin-top:30px ">Choose Payment Method</button>
                    </div>

                    <form class="creditly-card-form" style="display: none">
                        <section class="creditly-wrapper blue-theme">
                            <div class="credit-card-wrapper">
                                <div class="first-row form-group">
                                    <div class="col-sm-12 controls">
                                        <label class="control-label">@lang('admin_lang.card_no')</label>
                                        <input class="number credit-card-number form-control"
                                        type="text" name="number"
                                        pattern="(\d*\s){3}\d*"
                                        inputmode="numeric" autocomplete="cc-number" autocompletetype="cc-number" x-autocompletetype="cc-number" placeholder="&#149;&#149;&#149;&#149; &#149;&#149;&#149;&#149; &#149;&#149;&#149;&#149; &#149;&#149;&#149;&#149;">
                                    </div>
                                    
                                </div>
                                <div class="second-row form-group">
                                    <div class="col-sm-6 controls" style="float: left;">
                                        <label class="control-label">@lang('admin_lang.expiration')</label>
                                        <input class="expiration-month-and-year form-control"
                                        type="text" name="expiration-month-and-year"
                                        placeholder="MM / YY">
                                    </div>
                                    <div class="col-sm-6 controls"  style="float: left;">
                                        <label class="control-label">@lang('admin_lang.cvv')</label>
                                        <input class="security-code form-control"
                                        inputmode="numeric"
                                        pattern="\d*"
                                        type="password" name="security-code"
                                        placeholder="&#149;&#149;&#149;">
                                    </div>
                                    
                                </div>
                                <div class="second-row form-group">
                                    <div class="col-sm-12 controls">
                                        <label class="control-label">@lang('admin_lang.nm_of_the_card')</label>
                                        <input class="billing-address-name form-control"
                                        type="text" name="name"
                                        placeholder="John Smith">
                                    </div>
                                    
                                </div>
                                <div class="card-type">
                                </div>
                            </div>
                        </section>
                        <button class="view-sell view-sell-extra pay_now"><span>@lang('admin_lang.make_payment')</span></button>
                    </form>
                </div>

                <div class="process-bar" style="display: none">
                    <p>@lang('admin_lang.we_are_process_request_dont_close_refresh')</p>
                    <div class="progress" style="width: 40%; margin: 45px auto;">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                        @lang('admin_lang.processing')
                        </div>
                    </div>
                </div>

                <div class="payment_faild" style="display: none">
                    <i class="fa fa-exclamation-circle"></i>
                    <h4>@lang('admin_lang.payment_failed')</h4>
                    <p id="res_data"></p>
                    <small id="ref_no"></small>
                </div>
            </div>
        </div>
    </section>

@include('includes.footer')

</div>
<!--wrapper end-->
@endsection

@section('scripts')

@include('includes.scripts')

<script type="text/javascript">
    $(document).ready(function() {
        var paymentMethodId = '', amount = 0;
        $('.card_type').click(function() {
            paymentMethodId = $(this).data('id')
            amount = $(this).data('amount')

            $('.card_type i').hide();
            $(this).children('i').show();
        });

        $('.choose_pay_method').click(function(){
            if(paymentMethodId != '') {
                $(this).html('Processing..')
                $(this).attr('disabled', 'disabled')


                $.ajax({
                    url: '{{ route('execute.payment') }}',
                    data: {
                        _token: '{{ @csrf_token() }}',
                        params: {
                            payment_method_id: paymentMethodId,
                            amount: amount,
                            order_id: '{{ request()->track }}'
                        }
                    },
                    dataType: 'json',
                    type: 'POST',
                    success: function(response) {
                        console.log(response)
                        if(response.IsSuccess) {
                            if(response.Data.IsDirectPayment) {
                                $('.creditly-card-form').show();
                                $('.payment_method').hide()
                            } else {
                                location.href=response.Data.PaymentURL
                            }
                        }
                    }
                })
            } else {
                alert('Please select payment method.')
            }
            
        })
    })
</script>

<script src="public/js/creditly.js"></script>

<script type="text/javascript">
    $(function() {
        var creditly = Creditly.initialize(
          '.creditly-wrapper .expiration-month-and-year',
          '.creditly-wrapper .credit-card-number',
          '.creditly-wrapper .security-code',
          '.creditly-wrapper .card-type');

        $(".pay_now").click(function(e) {
            e.preventDefault();
            var output = creditly.validate();
            if (output) {
                $('.creditly-card-form').hide();
                $('.process-bar').show();
                // Your validated credit card output
                var reqData = {
                    _token: '{{ csrf_token() }}',
                    params: output
                }
                reqData.params['order_id'] = '{{ $orderId }}'
                $.ajax({
                    url: '{{ route('submit.payment') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: reqData,
                })
                .done(function(response) {
                    if(response.error) {
                        $('#res_data').html(response.error.message)
                        $('#ref_no').html('Ref No.: ' + response.error.ref_no)
                        $('.process-bar').hide();
                        $('.payment-div').hide();
                        $('.payment_faild').show();
                    } else {
                        location.href="{{ route('order.success', ['trackId' => $orderId]) }}"
                    }
                })
                .fail(function() {
                    console.log("error");
                });
            }
        });
    });
</script>
@endsection