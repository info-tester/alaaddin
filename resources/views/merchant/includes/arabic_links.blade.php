<!--favicon-->
<link href="{{ asset('public/merchant/arabic/assets/images/favicon.ico') }}" rel="shortcut icon">

<!--Preloader-CSS-->
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/preloader/preloader.css') }}">

<!--bootstrap-4-->
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/css/bootstrap.min.css') }}">

<!--Custom Scroll-->
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/customScroll/jquery.mCustomScrollbar.min.css') }}">
<!--Font Icons-->
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/simple-line/css/simple-line-icons.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/dripicons/dripicons.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/ionicons/css/ionicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/eightyshades/eightyshades.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/fontawesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/foundation/foundation-icons.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/metrize/metrize.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/typicons/typicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/weathericons/css/weather-icons.min.css') }}">

<!--Date-range-->
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/date-range/daterangepicker.css') }}">
<!--Drop-Zone-->
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/dropzone/dropzone.css') }}">
<!--Full Calendar-->
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/full-calendar/fullcalendar.min.css') }}">
<!--Normalize Css-->
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/css/normalize.css') }}">
<!--Main Css-->
<link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/css/main.css') }}">