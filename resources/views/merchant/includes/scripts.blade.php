<!--Jquery-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/js/jquery-3.2.1.min.js') }}"></script>
<!--Bootstrap Js-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/merchant/assets/js/bootstrap.min.js') }}"></script>
<!--Modernizr Js-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/js/modernizr.custom.js') }}"></script>

<!--Morphin Search JS-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/morphin-search/classie.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/morphin-search/morphin-search.js') }}"></script>
<!--Morphin Search JS-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/preloader/pathLoader.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/preloader/preloader-main.js') }}"></script>

<!--Chart js-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/charts/Chart.min.js') }}"></script>

<!--Sparkline Chart Js-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/sparkline/jquery.charts-sparkline.js') }}"></script>

<!--Custom Scroll-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/customScroll/jquery.mCustomScrollbar.min.js') }}"></script>
<!--Sortable Js-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/sortable2/sortable.min.js') }}"></script>
<!--DropZone Js-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/dropzone/dropzone.js') }}"></script>
<!--Date Range JS-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/date-range/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/date-range/daterangepicker.js') }}"></script>
<!--CK Editor JS-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/ckEditor/ckeditor.js') }}"></script>
<!--Data-Table JS-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/data-tables/datatables.min.js') }}"></script>
<!--Editable JS-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/editable/editable.js') }}"></script>
<!--Full Calendar JS-->
<script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/full-calendar/fullcalendar.min.js') }}"></script>

<!--- Main JS -->
<script src="{{ asset('public/merchant/assets/js/main.js') }}"></script>
<script src="{{ asset('public/merchant/assets/js/jquery.validate.js') }}"></script>
<script src="{{ asset('public/merchant/assets/js/chosen.jquery.min.js') }}"></script>



<script type="text/javascript">
	$(function() {
		var nWidth =$(window).width();

	    if(nWidth<992){
	        $('#navigation').removeClass('open');
	        $('#sidebar-panel').removeClass('open');
	    }
	    else if(nWidth<1096){
	        $('#sidebar-panel').removeClass('open');
	    }

	    /*----------------------------------------
	     // - #5  MENU/NAVIGATION
	     ----------------------------------------*/
	    "use strict";
	    $('.menu-items li a').on('click', function(){
	        "use strict";
	        if($(this).next('ul.sub-menu').length !== 0 && !$(this).hasClass('show')){
	            $('ul.sub-menu').slideUp(300);
	            $('.menu-items li a').removeClass('show');
	            $(this).addClass('show');
	            $(this).next('ul.sub-menu').slideDown(300);
	        }
	        else if($(this).hasClass('show')){
	            $(this).removeClass('show');
	            $(this).next('ul.sub-menu').slideToggle(300);
	        }
	    });

	    // - Toggle Logged User Menu
	    $(document).on('click',function(e){
	        'use strict';
	        if ($(e.target).is('.logged-user-menu,.logged-user-menu *')){
	        }
	        else if($(e.target).is('#show-user-menu, #show-user-menu *')){
	            $('.logged-user-menu').toggleClass('show');
	        }
	        else if($('.logged-user-menu').hasClass('show')){

	            $('.logged-user-menu').removeClass('show')
	        }
	    });

	    // - Toggle Sidebar
	    $(document).on('click',function(e){
	        'use strict';
	        if ($(e.target).is('#sidebar-panel,#sidebar-panel *')){
	        }
	        else if($(e.target).is('#toggle-sidebar, #toggle-sidebar *')){
	            $('#sidebar-panel').toggleClass('open');
	        }
	        else if($('#sidebar-panel').hasClass('open') && nWidth<1096){

	            $('#sidebar-panel').removeClass('open')
	        }
	    });

	    // - Toggle Navigation
	    $(document).on('click',function(e){
	        'use strict';
	        if ($(e.target).is('#navigation,#navigation *')){
	        }
	        else if($(e.target).is('#toggle-navigation, #toggle-navigation *')){
	            $('#navigation').toggleClass('open');
	        }
	        else if($('#navigation').hasClass('open') && nWidth<992){

	            $('#navigation').removeClass('open');
	        }
	    });

	    /*----------------------------------------
	     // - #6. FULL BODY COLORED SCROLL
	     ----------------------------------------*/
	    // - #7. NAVIGATION SCROLL
	    $('.custom-scroll').mCustomScrollbar({
	        theme:"dark",
	        scrollInertia: 300,
	        advanced:{
	            autoExpandHorizontalScroll:false
	        }
	    });

	    /*----------------------------------------
	     // - #8. BOOTSTRAP RELATED JS ACTIVATIONS
	     ----------------------------------------*/
	    // - Activate Date pickers
	    $('input.single-date-picker').daterangepicker({"singleDatePicker": true});
	    $('input.date-range-picker').daterangepicker({ "startDate": "03/28/2017", "endDate": "01/10/2017"});

	    // - Activate tooltips
	    $('[data-toggle="tooltip"]').tooltip({html:true,trigger: 'hover'});

	    // - Activate popovers
	    $('[data-toggle="popover"]').popover();

	    // - Activate Data Tables
	    $('[data-table="data-table"]').DataTable({ buttons: ['copy', 'excel', 'pdf'] });

	    //- Activate Nav pill
	    $('#myTab a').click(function (e) {
	        e.preventDefault();
	        $(this).tab('show');
	    });


	    $('.table-editable').editableTableWidget();

	    $(document).on('click','.right-side-toggle',function(){
	        $('.right-sidebar').toggleClass('show');
	    });

	    var pathname = window.location.href;
	    $('.side-nav li a,.top-nav li a').on('click',function(){
	        var a = pathname.split('/');
	        var layColor = $(this).attr("data-laycolor");
	        var url1 = pathname.replace(a[a.length-3]+'/','');
	        var b = url1.split('/');
	        var urlNew = url1.replace(a[a.length-2],layColor);
	        window.location=urlNew
	    });
	});
</script>
