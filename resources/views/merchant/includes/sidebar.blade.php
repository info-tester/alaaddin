
<div class="merchant-lang-div">
    @if(Config::get('app.locale') == 'en')
    <a href="javascript:;" class="merchant-lang" onclick="location.href='{{ route('merchant.change.lang') }}'" id="paste-lan">عربي</a>
    @elseif(Config::get('app.locale') == 'ar')
    <a href="javascript:;" class="merchant-lang" onclick="location.href='{{ route('merchant.change.lang') }}'" id="paste-lan">English</a>
    @endif
</div>
<!--Navigation Menu Links-->
<div class="navigation-menu">
    <ul class="menu-items custom-scroll" style="max-height: 420px;">
        <li>
            <a href="{{ route('merchant.dashboard') }}" class="">
                <span class="icon-thumbnail"><i class="fa fa-pie-chart"></i></span>
                <span class="title">@lang('merchant_lang.Dashboard')</span>
            </a>

        </li>
        <li>
            <a href="{{ route('merchant.message.show') }}" class="">
                <span class="icon-thumbnail"><i class="fa fa-envelope"></i></span>
                <span class="title">@lang('admin_lang.msg')</span>
            </a>

        </li>
        <li>
            <a href="{{ route('merchant.edit.profile') }}">
                <span class="icon-thumbnail"><i class="fa fa-pencil-square-o"></i></span>
                <span class="title">@lang('merchant_lang.EditProfile')</span>
            </a>
        </li>
        <li>
            <a href="{{ route('merchant.manage.product') }}">
                <span class="icon-thumbnail"><i class="fa fa-shopping-bag"></i></span>
                <span class="title">@lang('merchant_lang.Products') </span>
            </a>
        </li>
        <li>
            <a href="{{ route('merchant.list.order') }}" class="<?php if(Request::segment(2)=='merchant-order' || Request::segment(2)=='merchant-view-order'){echo "active";}?>">
            {{-- <a href="#"> --}}
                <span class="icon-thumbnail"><i class="fa fa-calendar"></i></span>
                <span class="title">@lang('merchant_lang.Orders') </span>
            </a>
        </li>
        <li>
            <a href="{{ route('merchant.manage.address.book') }}" class="<?php if(Request::segment(2)=='manage-address-book' || Request::segment(2)=='add-address'|| Request::segment(2)=='edit-address'){echo "active";}?>">
            {{-- <a href="#"> --}}
                <span class="icon-thumbnail"><i class="fa fa-address-card-o"></i></span>
                <span class="title">@lang('merchant_lang.manage_address_book') </span>
            </a>
        </li>
        <li>
            <a href="{{ route('merchant.add.external.order') }}" class="<?php if(Request::segment(2)=='merchant-add-external-order'){echo "active";}?>">
                <span class="icon-thumbnail"><i class="fa fa-cart-plus" aria-hidden="true"></i></span>
                <span class="title">@lang('admin_lang.create_external_order') </span>
            </a>
        </li>
        <li>
            <a href="{{ route('merchant.finance') }}">
                <span class="icon-thumbnail"><i class="fa fa-flag"></i></span>
                <span class="title">@lang('admin_lang.fnc')</span>
            </a>
        </li>
        <li>
            <a href="{{ route('merchant.order.report') }}" class="<?php if(Request::segment(2)=='order-report'){echo "active";}?>">
                <span class="icon-thumbnail"><i class="fa fa-calendar"></i></span>
                <span class="title">@lang('merchant_lang.order_report')</span>
            </a>
        </li>
        <li>
            <a href="{{ route('merchant.invoice') }}" class="<?php if(Request::segment(2)=='invoice-list'){echo "active";}?>">
                <span class="icon-thumbnail"><i class="fa fa-database"></i></span>
                <span class="title">@lang('admin_lang.manage_invoice')</span>
            </a>
        </li>
        <!-- <li>
            <a href="{{ route('merchant.earning.report') }}" class="<?php if(Request::segment(2)=='earning-report'){echo "active";}?>">
                <span class="icon-thumbnail"><i class="fa fa-calendar"></i></span>
                <span class="title">Earning Report</span>
            </a>
        </li> -->


    </ul>
    
</div>