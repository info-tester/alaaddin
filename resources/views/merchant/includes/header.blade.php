
<!---Preloader Starts Here--->
{{-- <div id="ip-container" class="ip-container">
    <header class="ip-header">
        <h1 class="ip-logo text-center"><img class="img-fluid" src="{{ asset('public/merchant/assets/images/logo-c.png') }}" alt="" class="ip-logo text-center"/></h1>
        <div class="ip-loader">
            <svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">
                <path class="ip-loader-circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                <path id="ip-loader-circle" class="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
            </svg>
        </div>
    </header>
</div> --}}
<!---Preloader Ends Here--->


<!--Navigation-->
<nav id="navigation" class="navigation-sidebar bg-primary">
    <div class="navigation-header">
        <a href="{{ route('merchant.dashboard') }}"><span class="logo"><img src="{{ asset('public/merchant/assets/images/logo-c2.png') }}"></span></a>
        <!--<img src="logo.png" alt="logo" class="brand" height="50">-->
    </div>
    <!--Navigation Profile area-->
    <div class="navigation-profile">
        @if(@Auth::guard('merchant')->user()->image)
        <img class="profile-img rounded-circle" src="{{url('storage/app/public/profile_pics/'.@Auth::guard('merchant')->user()->image)}}" alt="profile image">
        @else
        <!-- <img class="profile-img rounded-circle" src="{{ getPersonDefaultImageUrl() }}" alt="profile image"> -->
        <img class="profile-img rounded-circle" src="{{url('public/admin/assets/images/person_icon.png')}}" alt="profile image">
        @endif
        <h4 class="name">{{ @Auth::guard('merchant')->user()->fname  }}</h4>
        <span class="designation">{{ @Auth::guard('merchant')->user()->lname }}</span>

        <a id="show-user-menu" href="javascript:void(0);" class="circle-white-btn profile-setting"><i class="fa fa-cog"></i></a>

        <!--logged user hover menu-->
        <div class="logged-user-menu bg-white">
            <div class="avatar-info">                        
                @if(@Auth::guard('merchant')->user()->image)
                <img class="profile-img rounded-circle" src="{{url('storage/app/public/profile_pics/'.@Auth::guard('merchant')->user()->image)}}" alt="profile image">
                @else
                <img class="profile-img rounded-circle" src="{{url('public/admin/assets/images/person_icon.png')}}" alt="profile image">
                <!-- <img class="profile-img rounded-circle" src="{{ getPersonDefaultImageUrl() }}" alt="profile image"> -->
                @endif
                <h4 class="name">{{ @Auth::guard('merchant')->user()->fname }}</h4>
                <span class="designation">Merchant</span>
            </div>

            <ul class="list-unstyled">
                <!-- <li>
                    <a href="javascript:void(0);">
                        <i class="ion-ios-email-outline"></i>
                        <span>Emails</span>
                    </a>
                </li> -->
                <li>
                    <a href="{{ route('merchant.edit.profile') }}">
                        <i class="fa fa-pencil-square-o"></i>
                        <span>@lang('admin_lang.edit_my_profile')</span>
                    </a>
                </li>
                <li>
                    <a  href="{{ url('/merchant/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="ion-log-out"></i>
                        <span>Logout</span>
                    </a>                    
                    <form id="logout-form" action="{{ url('/merchant/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </div>
    </div>
    @include('merchant.includes.sidebar')
</nav>
