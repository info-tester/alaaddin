<!--favicon-->
<link href="{{ asset('public/merchant/assets/images/favicon.ico') }}" rel="shortcut icon">

<!--Preloader-CSS-->
<link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/preloader/preloader.css') }}">

<!--bootstrap-4-->
<link rel="stylesheet" href="{{ asset('public/merchant/assets/css/bootstrap.min.css') }}">

<!--Custom Scroll-->
<link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/customScroll/jquery.mCustomScrollbar.min.css') }}">
<!--Font Icons-->
<link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/simple-line/css/simple-line-icons.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/dripicons/dripicons.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/ionicons/css/ionicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/eightyshades/eightyshades.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/fontawesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/foundation/foundation-icons.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/metrize/metrize.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/typicons/typicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/weathericons/css/weather-icons.min.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/buttons.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/select.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/fixedHeader.bootstrap4.css') }}">

<!--Date-range-->
<link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/date-range/daterangepicker.css') }}">
<!--Drop-Zone-->
<link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/dropzone/dropzone.css') }}">
<!--Full Calendar-->
<link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/full-calendar/fullcalendar.min.css') }}">
<!--Normalize Css-->
<link rel="stylesheet" href="{{ asset('public/merchant/assets/css/normalize.css') }}">
<!--Main Css-->
<link rel="stylesheet" href="{{ asset('public/merchant/assets/css/main.css') }}">