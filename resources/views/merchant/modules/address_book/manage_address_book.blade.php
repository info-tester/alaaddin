@extends('merchant.layouts.app')
@section('title', 'Aswagna | Merchant | Manage Address Book')
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">@lang('merchant_lang.Dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('merchant_lang.manage_address_book')</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--Header Fixed-->
        <div class="header fixed-header">
            <div class="container-fluid" style="padding: 10px 25px">
                <div class="row">
                    <div class="col-9 col-md-6 d-lg-none">
                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                        <span class="logo">@lang('admin_lang_static.merchant')</span>
                    </div>
                    <div class="col-lg-8 d-none d-lg-block">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">@lang('admin_lang_static.dashboard')</a></li>
                            <li class="breadcrumb-item active">@lang('merchant_lang.manage_address_book')</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content sm-gutter">
            <div class="container-fluid padding-25 sm-padding-10">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title">
                            <h4>@lang('merchant_lang.manage_address_book') <a href="{{ route('merchant.add.address') }}" class="btn btn-dark rghtbtn"><i class="fa fa-plus"></i> @lang('admin_lang_static.add')</a></h4>
                        </div>
                    </div>
                    <div class="col-md-12">
                        @if (session()->has('success'))
                        <div class="alert alert-success vd_hidden" style="display: block;">
                            <a class="close" data-dismiss="alert" aria-hidden="true">
                                <i class="icon-cross"></i>
                            </a>
                            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                            <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                        </div>
                        @elseif ((session()->has('error')))
                        <div class="alert alert-danger vd_hidden" style="display: block;">
                            <a class="close" data-dismiss="alert" aria-hidden="true">
                                <i class="icon-cross"></i>
                            </a>
                            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                            <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                        </div>
                        @endif
                        <div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
                            <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                            </a>
                            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                            <strong class="success_msg">Success!</strong> 
                        </div>
                        <div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
                            <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                            </a>
                            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                            <strong class="error_msg"></strong> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="block table-block mb-4">
                            <div class="row">
                                <div class="table-responsive listBody">
                                    <table class="table table-striped table-bordered first" id="address">
                                        <thead>
                                            <tr>
                                                <th>@lang('admin_lang.country')</th>
                                                <th>@lang('admin_lang.city')</th>
                                                <th>@lang('admin_lang.block')</th>
                                                <th>@lang('admin_lang.street')</th>
                                                <th>@lang('admin_lang.postal_code')</th>
                                                <th>@lang('admin_lang.building')</th>
                                                <th>@lang('admin_lang.more_address_details')</th>
                                                <th>@lang('admin_lang.Action')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>@lang('admin_lang.country')</th>
                                                <th>@lang('admin_lang.city')</th>
                                                <th>@lang('admin_lang.block')</th>
                                                <th>@lang('admin_lang.street')</th>
                                                <th>@lang('admin_lang.postal_code')</th>
                                                <th>@lang('admin_lang.building')</th>
                                                <th>@lang('admin_lang.more_address_details')</th>
                                                <th>@lang('admin_lang.Action')</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('public/admin/assets/libs/js/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>

<script>
    $(document).ready(function(){


        $('body').on('click', '.reset_search', function() {
            $('#address').DataTable().search('').columns().search('').draw();
        })

        function filterColumn ( i ) {
            console.log(i, $('#col'+i+'_filter').val())
            $('#address').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }
        $('#address').DataTable( {
            stateSave: true,
            "order": [[0, "desc"]],
            "stateLoadParams": function (settings, data) {
                $('#col1_filter').val(data.search.keyword)
                $('#col2_filter').val(data.search.applied_for)
                $('#col3_filter').val(data.search.applied_on)
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col1_filter').val()
                data.search.applied_for = $('#col2_filter').val()
                data.search.applied_on = $('#col3_filter').val()
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('merchant.manage.address.book') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
                { 
                    data: "country",
                    render: function(data, type, full) {
                        return full.country_details_bylanguage.name;
                    }
                },
                {  
                    data: 'city',
                    render: function(data, type, full) {
                        if(full.city_id == null){
                            return full.city;
                        }else{
                            return full.get_city_name_by_language.name;
                        }
                    }
                },
                {   data: 'block' },
                {   data: 'street' },
                {   data: 'postal_code' },
                {   
                    data: 'building_no'
                },
                {  
                    data: 'more_address',
                },
                {  
                    render: function(data, type, full) {
                        var a = '';
                        a += ' <a href="{{ url('merchant/edit-address') }}/'+full.id+'"><i class="fa fa-edit" title="@lang('admin_lang.Edit')"></i></a>';
                        a += ' <a class="delete_address" href="javascript:void(0)" data-id="'+full.id+'"><i class=" fa fa-trash" title="@lang('admin_lang.Delete')"></i></a>';

                        return a;
                    }
                },
            ],
            // "createdRow": function( row, data, dataIndex ) {
            //     console.log( row, data, dataIndex)
            //     // add your condition here...
            //     $(row).addClass( 'important' );
            // }
        });

        // change event
        $('input.keyword').on( 'keyup click blur', function () {
            filterColumn( 1 );
        });
        $('.applied_for').on( 'change', function () {
            filterColumn( 2 );
        });
        $('.applied_on').on( 'change', function () {
            filterColumn( 3 );
        });

        $(document).on("click",".delete_address",function(e){
            var id = $(e.currentTarget).attr("data-id");
            var $this = $(this);
            if(confirm("@lang('merchant_lang.delete_address_warning')")){

                var reqData = {
                    "jsonrpc":"2.0",
                    "_token":"{{ csrf_token() }}",
                    "data":{
                        id:id
                    }
                };
                $.ajax({
                    url:"{{ route('merchant.delete.address') }}",
                    type:"post",
                    data:reqData,
                    success:function(resp){
                        console.log(resp)
                        if(resp.status == 1){
                            $(".success_msg").html('@lang('merchant_lang.delete_address_success')');
                            $(".success_msg_div").show();
                            $(".error_msg_div").hide();
                            $(".tr_"+id).hide();
                            $this.parent().parent().remove();
                        }else{
                            $(".error_msg_div").show();
                            $(".success_msg_div").hide();
                            $(".error_msg").html('@lang('merchant_lang.delete_address_error')');
                        }
                        $(".session_success_div").hide();
                        $(".session_error_div").hide();
                    }
                });
            }
            // }
        });
   
    });
</script>
@endsection