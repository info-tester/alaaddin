@extends('merchant.layouts.app')
{{-- @section('title', 'Aswagna | Merchant | Create External Order') --}}
@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('admin_lang.merchant') | @lang('merchant_lang.edit_address_book')
@endsection
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    .remove-img {
    width: 18px;
    height: 18px;
    font-size: 13px;
    color: #fff;
    text-align: center;
    background: #0771d4;
    border-radius: 50%;
    line-height: 16px;
    float: right;
    margin-left: 4px;
    }
    .remove-img a {
    color: #fff;
    font-size: 10px;
    }
    .action-opt {
    position: absolute;
    right: 0;
    top: 5px;
    z-index: 1;
    }
    label.error{
        border: none !important;
        background-color: #fff !important;
        color: #f00;
    }
</style>
<style type="text/css">
    label.error{
    border: none !important;
    background-color: #fff !important;
    color: #f00;
    }

    #cityList{
        position: absolute;
        width: 97%;
        /*height: 200px;*/
        z-index: 99;
    }
    #cityList ul{
        background: #fff;
        width: 97%;
        border: solid 1px #eee;
        padding: 0;
        max-height: 200px;
        overflow-y: scroll;
    }
    #cityList ul li{
        list-style: none;
        padding: 5px 15px;
        cursor: pointer;
        border-bottom: solid 1px #eee;
    }
    #cityList ul li:hover{
        background: #3a82c4;
        color: #fff;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">
                            @lang('admin_lang.Dashboard')</a>
                        </li>
                        <li class="breadcrumb-item active">@lang('merchant_lang.edit_address_book')</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('merchant_lang.edit_address_book')</h4>
                    </div>
                </div>
                <div class="col-md-12">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session()->has('success'))
                    <div class="alert alert-success vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                    </div>
                    @elseif ((session()->has('error')))
                    <div class="alert alert-danger vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    @endif
                    <div class="block form-block mb-4">
                        <form id="addAddressForm" method="POST" action="{{ route('merchant.update.address') }}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{ @$address->id }}">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="country" class="col-form-label">@lang('admin_lang.country') (@lang('admin_lang.required'))</label>
                                    <select class="custom-select form-control required" id="country" name="country">
                                        <option value="">@lang('admin_lang.select_country')</option>
                                        @if(@$countries)
                                        @foreach(@$countries as $country)
                                        <option value="{{ @$country->id }}" @if($country->id == @$address->country) selected  @endif>{{ @$country->countryDetailsBylanguage->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-row">
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label class="col-form-label">@lang('admin_lang.phone') (@lang('admin_lang.required'))</label>
                                    <input id="phone" name="phone" class="form-control required number" placeholder='@lang('admin_lang.phone')' type="tel"  onkeypress="return isNumber(event)" value="{{ @$address->phone }}"> 
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 outside_kuwait_city">
                                    <label for="city" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                    <input id="city" name="city" type="text" class="form-control" placeholder='@lang('admin_lang.city')' value="{{ @$address->city }}">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 inside_kuwait_city">
                                    <label for="kuwait_city_label" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                    
                                    <input type="text" class="form-control required custom-select" name="kuwait_city" id="kuwait_city" placeholder="@lang('admin_lang.city')" value="{{ @$address->getCityNameByLanguage->name }}" >
                                    <div id="cityList"></div>
                                    <span class="text-danger city_id_err"></span>
                                </div>
                                
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 blockDiv">
                                    <label for="" class="col-form-label block_label">@lang('admin_lang.block_required') </label>
                                    <input type="text" id="block" name="block" type="text" class="form-control required" placeholder='@lang('admin_lang.block')' value="{{ @$address->block }}"/>
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="" class="col-form-label">@lang('admin_lang.street') (@lang('admin_lang.required'))</label>
                                    <input id="street" name="street" type="text" class="form-control required" placeholder='@lang('admin_lang.street')' value="{{ @$address->street }}"> 
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 zipDiv" >
                                <label for="zip" class="col-form-label zip_label">@lang('admin_lang.postal_code_optional')</label>
                                <input id="zip" name="zip" type="number" class="form-control" placeholder='@lang('admin_lang.postal_code')' onkeypress="return isNumber(event)" value="{{ @$address->postal_code }}">
                                </div>
                                
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 buidingDiv buidingDiv">
                                    <label for="" class="col-form-label building_label">@lang('admin_lang.build_required')</label>
                                    <input type="text" id="building" name="building" type="text" class="form-control required" placeholder='@lang('admin_lang.building')' value="{{ @$address->building_no }}"/>
                                </div>
                                
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label class="col-form-label">@lang('front_static.type_url') (@lang('front_static.optional'))</label>
                                    <input id="" type="text" placeholder="@lang('front_static.type_url')" name="location" class="form-control" value="{{ @$address->location }}">
                                    
                                    <input type="hidden" name="lat" id="lat1">
                                    <input type="hidden" name="lng" id="lng1">
                                </div>

                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="" class="col-form-label">@lang('admin_lang.more_add_details_opt')</label>
                                    <input type="text" id="address" name="address" type="text" class="form-control" placeholder='@lang('admin_lang.more_address_details')' value="{{ @$address->more_address }}"/>
                                </div>
                            </div>
                            
                            <div class="form-row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <label>
                                        <input type="checkbox" name="is_default" id="is_default" value="Y" placeholder="@lang('front_static.is_default')" @if(@$address->is_default == 'Y') {{ 'checked' }} @endif> @lang('front_static.is_default')
                                    </label>
                                </div>
                                <hr>
                                <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <input type="hidden" id="k_cityname" name="k_cityname" value="">
                                    <button class="btn btn-primary" id="addAddressBtn" type="submit">@lang('merchant_lang.save_address')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    $(document).ready(function(){

        var country = "{{ @$address->country }}";
        console.log(country);
        if(country == 134) {

            $(".outside_kuwait_state").hide();
            $(".outside_kuwait_city").hide();
            $(".zipDiv").hide();
                //add text required
                
            $(".block_label").html('@lang('admin_lang.block_required')');
            $(".building_label").html('@lang('admin_lang.buildings_required')');
            //add text optional

            // add required
            $('#street').addClass('required');
            $('#kuwait_city').addClass('required');
            $('#street').addClass('required');
            $('#block').addClass('required');
            $('#building').addClass('required');
            
            
            // remove required
            $('#city').removeClass('required');
            $('#city').removeClass('error');
            $("#street-error").text("");
            $(".zipDiv").hide();
            
        } 
        // out side kuwait
        else {
            $('.inside_kuwait_city').hide();
            $(".block_label").html('@lang('admin_lang.block_optional')');
            $(".building_label").html('@lang('admin_lang.building_optional')');
            //add text required
            
            //add text optional

            //add required
            // block-error
            $("#block-error").text("");
            $("#building-error").text("");
            $('#city').addClass('required');
            

            //remove required
            $('#block').removeClass('required');
            $('#block').removeClass('error');
            $('#building').removeClass('required');
            $('#building').removeClass('error');
            $('#kuwait_city').removeClass('required');
            $('#kuwait_city').removeClass('error');
            $(".zipDiv").show();
            //remove text

        }

        $(document).on("change","#country",function(e){
            $(".removeText").text("");
            var country = $.trim($("#country").val());
            if(country == 134){ //if inside kuwait
                $(".outside_kuwait_state").hide();
                $(".outside_kuwait_city").hide();
                $(".inside_kuwait_state").show();
                $(".inside_kuwait_city").show();
                $(".zipDiv").hide();
            }else{
                $(".inside_kuwait_state").hide();
                $(".outside_kuwait_state").show();
                $(".inside_kuwait_city").hide();
                $(".outside_kuwait_city").show();
                $(".zipDiv").show();
            }
        });
        $('#kuwait_city').change(function(event) {
            $('.city_id_err').html('');    
        });
        $("#addAddressForm").validate({
            messages: { 
                street: { 
                    required: '@lang('validation.required')'
                },
                city: { 
                    required: '@lang('validation.required')'
                },
                kuwait_city: { 
                    required: '@lang('validation.required')'
                },
                zip: { 
                    required: '@lang('validation.required')'
                },
                country: { 
                    required: '@lang('validation.required')'
                },
            }
        });

        $('#kuwait_city').keyup(function(){ 
            var city = $(this).val();
            if(city != '')
            {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('merchant.external.fetch.city') }}",
                    method:"POST",
                    data:{city:city, _token:_token},
                    success:function(response){
                        // alert(JSON.stringify(response));
                        if(response.status == 'ERROR') {
                            $('#cityList').html('@lang('admin_lang.nothing_found')');
                        } else {
                        $('#cityList').fadeIn();  
                        $('#cityList').html(response.result);
                        }
                    }
                });
            }
        });
        $(document).on("blur","#kuwait_city",function(){
            $("#k_cityname").val($.trim($("#kuwait_city").val()));
            var keyword = $.trim($("#kuwait_city_value_id").val());
            if(keyword == ""){
                $("#kuwait_city").val("");
                $(".kuwait_city-error").text('@lang('admin_lang.please_select_name_city')');
            }
        });

        $('body').on('click', '.cityChange', function(){  
            $('#kuwait_city').val($(this).text());  
            $('#cityList').fadeOut();  
        });  
        $("body").click(function(){
            $("#cityList").fadeOut();
        });
        $("#kuwait_city").click(function(){
            var city = $(this).val();
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url:"{{ route('merchant.external.fetch.city') }}",
                method:"POST",
                data:{city:city, _token:_token},
                success:function(response){
                    if(response.status == 'ERROR') {
                        $('#cityList').html('@lang('admin_lang.nothing_found')');
                    } else {
                    $('#cityList').fadeIn();  
                    $('#cityList').html(response.result);
                    }
                }
            });
        });
        $(document).on("click",".kuwait_cities",function(e){
            var id = $(e.currentTarget).attr("data-id");
            $("#kuwait_city_value_id").val(id);
        });
        // when change country
        $('#country').change(function(event) {
            country = $(this).val();
            $('#street').addClass('required');

            // seller and buyer both are from kuwait
            if(country == 134) {
                //add text required
                
                $(".block_label").html('@lang('admin_lang.block_required')');
                $(".building_label").html('@lang('admin_lang.buildings_required')');
                //add text optional

                // add required
                $('#street').addClass('required');
                $('#kuwait_city').addClass('required');
                $('#street').addClass('required');
                $('#block').addClass('required');
                $('#building').addClass('required');
                
                
                // remove required
                $('#city').removeClass('required');
                $('#city').removeClass('error');
                $("#street-error").text("");
                $(".zipDiv").hide();
                
            } 
            // out side kuwait
            else {
                $(".block_label").html('@lang('admin_lang.block_optional')');
                $(".building_label").html('@lang('admin_lang.building_optional')');
                //add text required
                
                //add text optional

                //add required
                // block-error
                $("#block-error").text("");
                $("#building-error").text("");
                $('#city').addClass('required');
                

                //remove required
                $('#block').removeClass('required');
                $('#block').removeClass('error');
                $('#building').removeClass('required');
                $('#building').removeClass('error');
                $('#kuwait_city').removeClass('required');
                $('#kuwait_city').removeClass('error');
                $(".zipDiv").show();
                //remove text

            }
        });
        
    });
    function validate(evt) {
        var theEvent = evt || window.event;
        // Handle paste
        if (theEvent.type === 'paste') {
            key = event.clipboardData.getData('text/plain');
        } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        }
        var regex = /[0-9]|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>

<script>
function initMap1() {
    var input = document.getElementById('pac-input1');
    var autocomplete = new google.maps.places.Autocomplete(input);
    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    autocomplete.setTypes(['address']);

    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
      $('#lat1').val(place.geometry.location.lat())
      $('#lng1').val(place.geometry.location.lng())
    });
}
$(document).ready(function() {
    $('#pac-input1').blur(function() {
        if($(this).val() == '') {
            $('#lat1').val('')
            $('#lng1').val('')
        }
    })
})
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API') }}&libraries=places&callback=initMap1" async defer></script>
@endsection