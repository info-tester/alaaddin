@extends('merchant.layouts.app')
@section('title', 'Alaaddin | Merchant | Awaiting Approval')
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<style type="text/css">
  #showVariants{
    width: 100%;
  }
  .adedfrm{
    width: 100%;
    float: left;
  }
  .adedfrm .form-group {
    width: 19%;
    float: left;
    margin-right: 1%;
  }
  .col-form-label { font-size: 15px; width: 100%; }
  .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
    border: 1px solid #003eff;
    background: #007fff;
    font-weight: normal;
    text-align: center;
    color: #ffffff;
  }
  .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {
    border: 1px solid #c5c5c5;
    background: #f6f6f6;
    font-weight: normal;
    color: #454545;
    text-align: center;
  }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
  <!--Header Fixed-->
  <div class="header fixed-header">
    <div class="container-fluid" style="padding: 10px 25px">
      <div class="row">
        <div class="col-12 col-md-6 d-lg-none">
          <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
          <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
        </div>
        <div class="col-lg-8 d-none d-lg-block">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ ('merchant.dashboard') }}">@lang('admin_lang_static.dashboard')</a></li>
            <li class="breadcrumb-item active">@lang('admin_lang_static.awaiting_approval')</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
      <div class="row">
        <div class="col-12">
          <div class="section-title">
            <h4>@lang('admin_lang_static.awaiting_approval')</h4>
          </div>
        </div>
        <div class="col-md-12">
          <div class="alert alert-danger vd_hidden" id="erMsg" style="display: none;">
            <a class="close" data-dismiss="alert" aria-hidden="true">
              <i class="icon-cross"></i>
            </a>
            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
            <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
          </div>
          <div class="block form-block mb-4">
            <h4>You can sell products after admin approval.</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( ".from_date" ).datepicker({dateFormat: "yy-mm-dd"});
    $( ".to_date" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
</script>
<script>
  $(document).ready(function(){ 
    $("#dependent_form" ).validate({
      rules: {
        "discount_price":{
         required:true,
         number:true
       }
     },
     errorPlacement: function (error , element) {
              //toastr:error(error.text());
            }
          });
  });
</script>
<script>
  $(document).ready(function() {
    $('.slct').chosen();
  })
</script>
{{-- for fetching models of brand  --}}
<script>
  $(document).ready(function(){
    
   $('#input-select').change(function(){
    if($(this).val() != '')
    {
     var value  = $(this).val();
     var _token = $('input[name="_token"]').val();
     $.ajax({
      url:"{{ route('sub.cat.fetch') }}",
      method:"POST",
      data:{value:value, _token:_token},
      success:function(result)
      {
       $('#subCategory').html(result);
     }
     
   })
   }
 });
   
   $('#input-select').change(function(){
    $('#subCategory').val('');
  }); 
   
 });
</script>
@endsection