@extends('merchant.layouts.app')
@section('title', 'Aswagna | Merchant | Manage Product')
@section('links')
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    #example_filter{
        display: none;
    }
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: 0em;
        content: "";
    }
    table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
        right: 0em;
        content: "";
    }
    .dataTables_length select{
        display: inline-block!important;
        width: 75px !important;
        margin: 0px 5px;
        vertical-align: middle;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">@lang('merchant_lang.Dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('merchant_lang.EditProfile')</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper">
        <!--Header Fixed-->
        <div class="header fixed-header">
            <div class="container-fluid" style="padding: 10px 25px">
                <div class="row">
                    <div class="col-9 col-md-6 d-lg-none">
                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                        <span class="logo">@lang('admin_lang_static.merchant')</span>
                    </div>
                    <div class="col-lg-8 d-none d-lg-block">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">@lang('admin_lang_static.dashboard')</a></li>
                            <li class="breadcrumb-item active">@lang('admin_lang_static.manage_product')</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content sm-gutter">
            <div class="container-fluid padding-25 sm-padding-10">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title">
                            <h4>@lang('admin_lang_static.manage_product') <a href="{{ route('merchant.add.product.step.one') }}" class="btn btn-dark rghtbtn"><i class="fa fa-plus"></i> @lang('admin_lang_static.add')</a></h4>
                        </div>
                    </div>
                    <div class="col-md-12">
                        @if (session()->has('success'))
                        <div class="alert alert-success vd_hidden" style="display: block;">
                            <a class="close" data-dismiss="alert" aria-hidden="true">
                                <i class="icon-cross"></i>
                            </a>
                            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                            <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                        </div>
                        @elseif ((session()->has('error')))
                        <div class="alert alert-danger vd_hidden" style="display: block;">
                            <a class="close" data-dismiss="alert" aria-hidden="true">
                                <i class="icon-cross"></i>
                            </a>
                            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                            <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                        </div>
                        @endif
                        <div class="block form-block mb-4">
                        
                        <form action="{{ route('merchant.manage.product') }}" method="post">
                            <div class="form-row">
                                <div class="form-group col-md-6" data-column="4">
                                    <label>@lang('admin_lang_static.keyword')</label>
                                    <input id="col4_filter" type="text" class="form-control keyword" placeholder="@lang('admin_lang_static.search_by_title')" name="keyword" value="{{ @$key['keyword'] }}">
                                </div>
                                
                                <div class="form-group col-md-6" data-column="1">
                                    <label>@lang('admin_lang_static.category')</label>
                                    <select class="form-control category" name="category" id="col1_filter">
                                        <option value="">@lang('admin_lang_static.select_category')</option>
                                        @foreach(@$category as $cat)
                                        <option value="{{ @$cat->id }}" @if(@$key['category'] == $cat->id) selected @endif>{{ @$cat->categoryByLanguage->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6" data-column="2">
                                    <label>@lang('admin_lang_static.sub_category')</label>
                                    <select class="form-control sub_category" name="sub_category" id="col2_filter">
                                        <option value="">@lang('admin_lang_static.select_sub_category')</option>
                                        @foreach(@$subCategory as $subCat)
                                        <option value="{{ @$subCat->id }}" @if(@$key['sub_category'] == @$subCat->id) selected @endif>{{ @$subCat->categoryByLanguage->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6" data-column="7">
                                    <label>@lang('admin_lang_static.status')</label>
                                    <select class="form-control status" name="status" id="col7_filter">
                                        <option value="">@lang('admin_lang_static.select_status')</option>
                                        <option value="A" @if(@$key['status'] == 'A') selected @endif>@lang('admin_lang_static.active')</option>
                                        <option value="I" @if(@$key['status'] == 'I') selected @endif>@lang('admin_lang_static.inactive')</option>
                                        <option value="W" @if(@$key['status'] == 'W') selected @endif>@lang('admin_lang_static.awaiting_approval')</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6" data-column="5" id="priceVal">
                                    <label>@lang('admin_lang_static.price_range')</label>
                                    <div>
                                        <input type="hidden" id="amount" name="price" readonly style="border:0; color:#f6931f; font-weight:bold;" value="{{ @$key['price'] }}">
                                        <span class="price_from"></span>
                                        <span class="price_to" style="position: absolute; right: 25px"></span>
                                    </div>
                                    <div id="slider-range"></div>
                                </div>
                                <div class="form-group col-md-12">
                                    <a class="btn btn-primary" id="search" href="javascript:void(0)">@lang('admin_lang_static.search')</a>
                                    <input type="reset" value="@lang('admin_lang.reset_search')" class="btn btn-default fstbtncls reset_search">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="">
                        <div class="block table-block mb-4">
                            <div class="row">
                                <div class="table-responsive listBody">
                                    <table id="dataTable1" class="display table table-striped">
                                        <thead>
                                            <tr>
                                                <th>@lang('admin_lang_static.image')</th>
                                                <th>@lang('admin_lang_static.product_code')</th>
                                                <th>@lang('admin_lang_static.category')</th>
                                                <th>@lang('admin_lang_static.sub_category')</th>
                                                <th>@lang('admin_lang_static.title')</th>
                                                <th>@lang('admin_lang.total_stock')</th>
                                                <th>@lang('admin_lang_static.price') (KWD)</th>
                                                <th>@lang('admin_lang_static.is_featured')</th>
                                                <th>@lang('admin_lang_static.status')</th>
                                                <th>@lang('admin_lang_static.action')</th>
                                            </tr>
                                        </thead>
                                        <tbody class=""></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script>
    $( function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            @if(@$highestPrice->price)
            max: {{ @$highestPrice->price }},
            @else
            max: 1000.000,
            @endif
            @php
            if(@$key['price']){
                $price_val = $key['price'];
            }else{
                if(@$highestPrice->price) {
                $price_val = '0, '. (Int)@$highestPrice->price;
                } else {
                $price_val = '0, 1000';
                }
            }
            @endphp
            values: [ {{ $price_val }} ],
            slide: function( event, ui ) {
                $( "#amount" ).val( ui.values[ 0 ]  + "," + ui.values[ 1 ] );
                $('.price_from').html(ui.values[ 0 ] + ' {{ getCurrency() }}')
                $('.price_to').html(ui.values[ 1 ] + ' {{ getCurrency() }}')
            }
        });
        $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 )  +
          "," + $( "#slider-range" ).slider( "values", 1 ) );
        $('.price_from').html($( "#slider-range" ).slider( "values", 0 ) + ' {{ getCurrency() }}');
        $('.price_to').html($( "#slider-range" ).slider( "values", 1 ) + ' {{ getCurrency() }}');

        /*$( "#slider-range" ).slider({
            change: function( event, ui ) {
                var status = $.trim($("#status").val()),
                keyword = $.trim($("#keyword").val()),
                category = $.trim($("#category").val()),
                sub_category = $.trim($("#subCategory").val()),
                price = $.trim($("#amount").val());
                $.ajax({
                    type:"POST",
                    url:"{{ route('merchant.manage.product') }}",
                    data:{
                        status:status,
                        _token: '{{ csrf_token() }}',
                        keyword:keyword,
                        category:category,
                        sub_category:sub_category,
                        price:price
                    },
                    beforeSend:function(){
                        $(".loader").show();
                    },
                    success:function(resp){
                        $(".listBody").html(resp);
                        $(".loader").hide();
                    }
                });
            }
        });*/
    });
</script>
<script>
    $(document).ready(function() {
        function filterColumn ( i ) {
            $('#dataTable1').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
                ).draw();
        }
        function filterColumnPrice ( i ) {
            $('#dataTable1').DataTable().search(
                $('#amount').val(),
                ).draw();
        }

        $('.slct').chosen();
        function getImg(data, type, full, meta) {
            if(data) {
                return '<img width="100" height="70" src="storage/app/public/products/'+data+'" />';
            } else {
                return '<img width="100" height="70" src="public/frontend/images/default_product.png" />';
            }
        }
        function getStatus(data, type, full, meta) {
            console.log(data)
            if(data == 'Y') 
                return "<span class='status_"+full.id+"'>Yes</span>";
            else if(data == 'N') 
                return "<span class='status_"+full.id+"'>No</span>";
            else if(data == 'A') 
                return "<span class='status_"+full.id+"'>Active</span>";
            else if(data == 'I') 
                return "<span class='status_"+full.id+"'>Inactive</span>";
            else if(data == 'W') 
                return "<span class='status_"+full.id+"'>Awaiting Approval</span>";
        }

        function getAction(data, id, full, meta) {
            var a = '';
            var ln = full.product_sub_category.category.price_variant;
            if(full.status == 'A') {
                a += '<a class="status_change" href="{{ url('merchant/merchant-status-product') }}/'+full.id+'" data-id="'+full.id+'" data-status=""><i class="fa fa-ban icon_change_'+full.id+'" title="@lang('admin_lang.block')"></i></a> '
            } else if(full.status == 'I') {
                a += '<a class="status_change" href="{{ url('merchant/merchant-status-product') }}/'+full.id+'" data-id="'+full.id+'" data-status="'+full.id+'"><i class="fa fa-check-circle icon_change_'+full.id+'" title="@lang('admin_lang.unblock')"></i></a> '
            }
            
            a += '<a href="{{ url('merchant/merchant-edit-product') }}/'+full.slug+'"><i class="fa fa-edit" title="Edit"></i></a> '
            
            if(full.product_variants.length != 0) {
                a += '<a class="view_stock" href="{{ url('merchant/merchant-add-product-step-three').'/' }}'+full.slug+'" data-id="'+full.id+'"><i class="fa fa-archive" aria-hidden="true" title="@lang('admin_lang.view_stock')"></i></a> ';
            } else {
                a += '<a class="view_stock" href="{{ url('merchant/merchant-edit-product') }}/'+full.slug+'" data-id="'+full.id+'"><i class="fa fa-archive" aria-hidden="true" title="@lang('admin_lang.view_stock')"></i></a> ';
            }

            if(ln.length > 0) {
                a += '<a href="{{ url('merchant/merchant-product-discount').'/' }}'+full.slug+'"><i class="fa fa-percent" title="Set Discount Price"></i></a> '
            }
            return a;
        }
        $('#dataTable1').DataTable({
            stateSave: true,
            order: [
                [0, "desc"]
            ],
            stateLoadParams: function (settings, data) {
                $('#col4_filter').val(data.search.keyword)
                $('#col1_filter').val(data.search.category)
                $('#col2_filter').val(data.search.subcategory)
                $('#col7_filter').val(data.search.status)
                $('#amount').val(data.search.price)
                const a = data.search.price.split(',');
                if (a[0] && a[1]) {
                    $('#slider-range').slider("option", "values", a);
                }
            },
            stateSaveParams: function (settings, data) {
                data.search.keyword = $('#col4_filter').val();
                data.search.category = $('#col1_filter').val();
                data.search.subcategory = $('#col2_filter').val();
                data.search.status = $('#col7_filter').val();
                data.search.price = $('#amount').val();
            },
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            ajax: {
                "url": "{{ route('merchant.get.product.datatable') }}",
                "data": function ( d ) {
                    d.myKey = "myValue";
                    d._token = "{{ @csrf_token() }}";
                }
            },
            columns: [
                { 
                    data: 'default_image.image',
                    render: getImg
                },
                { data: 'product_code' },
                { data: 'product_parent_category.category.category_by_language.title' },
                { data: 'product_sub_category.category.category_by_language.title' },
                { data: 'product_by_language.title' },
                {
                    render: function(data, type, full, meta){
                        if(full.product_variants.length != 0){
                            var totStock = 0;
                            for(var i=0;i<full.product_variants.length;i++){
                                totStock = totStock + full.product_variants[i].stock_quantity;    
                            }
                            return totStock;
                        }else{
                            return full.stock;
                        }
                    }
                },
                { data: 'price' },
                { 
                    data: 'is_featured',
                    render: getStatus
                },
                { 
                    data: 'status',
                    render: getStatus
                },
                { 
                    data: 'seller_status,id',
                    render: getAction
                },
            ]
        });

        $('.keyword').on( 'keyup click', function () {
            filterColumn( $(this).parents('div').data('column') );
        });

        $('.category').on('change', function () {
            filterColumn( $(this).parents('div').data('column') );
        });

        $('.subcategory').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        });

        $('.status').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        });

        $('body').on('mouseleave', '.ui-slider-handle', function () {
            filterColumnPrice( $('#priceVal').data('column') );
        });

        $('body').on('click', '.reset_search', function() {
            $('#dataTable1').DataTable().search('').columns().search('').draw();
        })
    })
</script>
{{-- for fetching models of brand  --}}
<script>
    $(document).ready(function(){
        $('#category').change(function(){
            if($(this).val() != '') {
                var value  = $(this).val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('merchant.sub.cat.fetch') }}",
                    method:"POST",
                    data:{value:value, _token:_token},
                    success:function(response)
                    {
                        $('#subCategory').html(response.result.output);
                    }
                })
            }
        });
        $('#input-select').change(function(){
            $('#subCategory').val('');
        });
    });
</script>
@endsection