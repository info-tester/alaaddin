<table id="dataTable1" class="display table table-striped" data-table="data-table">
    <thead>
        <tr>
            <th>@lang('admin_lang_static.image')</th>
            <th>@lang('admin_lang_static.category')</th>
            <th>@lang('admin_lang_static.sub_category')</th>
            <th>@lang('admin_lang_static.title')</th>
            <th>@lang('admin_lang_static.price') (KWD)</th>
            <th>@lang('admin_lang_static.is_featured')</th>
            <th>@lang('admin_lang_static.status')</th>
            <th>@lang('admin_lang_static.action')</th>
        </tr>
    </thead>
    <tbody class="">
        @foreach($product as $pro)
        <tr>
            <td>
                <div class="m-r-10">
                    @if(@$pro->defaultImage->image)
                    <img src="{{url('storage/app/public/products/'.@$pro->defaultImage->image)}}" alt="user" class="rounded" width="45">
                    @else
                    <img src="{{url('public/admin/assets/images/eco-product-img-1.png')}}" alt="user" class="rounded" width="45">
                    @endif
                </div>
            </td>
            @if(count(@$pro->productCategory)>0)
            @foreach(@$pro->productCategory as $proCat)
            <td>{{ $proCat->Category->categoryByLanguage->title }}</td>
            @endforeach
            @else
            <td></td>
            <td></td>
            @endif
            <td>{{ @$pro->productByLanguage->title }}</td>
            <td>{{ $pro->price }} {{ getCurrency() }}</td>
            <td>
                @if($pro->merchant_is_featured == 'Y')
                Yes
                @elseif($pro->merchant_is_featured == 'N')
                No
                @endif
            </td>
            <td>
                @if($pro->seller_status == 'A')
                @lang('admin_lang_static.active')
                @elseif($pro->seller_status == 'I')
                @lang('admin_lang_static.inactive')
                @elseif($pro->seller_status == 'W')
                @lang('admin_lang_static.awaiting_approval')
                @endif
            </td>
            <td>
                @if($pro->seller_status == 'A')
                <a href="{{ route('merchant.status.product',$pro->id) }}" onclick="return confirm('Do you want to inactive this product ?');"><i class="fa fa-ban" title="Inactive"></i></a>
                @elseif($pro->seller_status == 'I')
                <a href="{{ route('merchant.status.product',$pro->id) }}" onclick="return confirm('Do you want to active this product ? ');"><i class="fa fa-check-circle" title="Active"></i></a>
                @endif
                <a href="{{ route('merchant.edit.product',$pro->slug) }}"><i class="fa fa-edit" title="Edit"></i></a>
                @if(count(@$pro->productSubCategory->Category->priceVariant) > 0)
                <a href="{{ route('merchant.product.discount',$pro->slug) }}"><i class="fa fa-percent" title="Set Discount Price"></i></a>  
                @endif
                <a href="{{ route('merchant.remove.product', $pro->id) }}" onclick="return confirm('Do you want to delete this product ? ');"><i class=" fa fa-trash"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
       $('#show-user-menu').click(function(){
        $('.logged-user-menu').toggleClass('show');
      }); 
   });
</script>