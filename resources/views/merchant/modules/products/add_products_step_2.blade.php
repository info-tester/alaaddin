@extends('merchant.layouts.app')
@section('title', 'Alaaddin | Merchant | Add Product Step Two')
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    #showVariants{
        width: 100%;
    }
    .adedfrm{
        width: 100%;
        float: left;
    }
    .adedfrm .form-group {
        width: 19%;
        float: left;
        margin-right: 1%;
    }
    .col-form-label { font-size: 15px; width: 100%; }
    .plsad {
        padding: 8px 8px;
        border-radius: 3px;
        background: #2486e1;
        color: #fff;
        font-size: 16px;
        margin: 1px;
        border: none;
        cursor: pointer;
    }
    .sub-header-size{
        font-size: 14px;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ ('merchant.dashboard') }}">@lang('admin_lang_static.dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('admin_lang_static.add_product')</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('admin_lang_static.add_product')<span class="sub-header-size"> > {{ @$productName }} > @lang('admin_lang_static.step2_of_4_pricing')</span></h4>
                    </div>
                </div>
                <div class="col-md-12">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session()->has('success'))
                    <div class="alert alert-success vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                    </div>
                    @elseif ((session()->has('error')))
                    <div class="alert alert-danger vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    @endif
                    <div class="alert alert-danger vd_hidden" id="erMsg" style="display: none;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    <div class="block form-block mb-4">
                        <!-- <div class="block-heading">
                            <h5>Default Layout</h5>
                        </div> -->
                        <form action="" method="POST" id="dependent_form">
                            @csrf
                            <div class="row" id="appendRow" style="margin: 0;">
                                <input type="hidden" name="product_id" value="{{ $product_id }}">
                                <input type="hidden" name="slug" id="slug" value="{{ $slug }}">
                                <div class="adedfrm">
                                    @foreach($price_variant as $pv)
                                    <div class="form-group">
                                        <label for="input-select" class="col-form-label">{{ $pv->variantByLanguage->name }}</label>
                                        <select class="form-control required price_dependent" name="price_variant[]">
                                            <option value="">Select {{ $pv->variantByLanguage->name }}</option>
                                            @foreach($pv->variantValues as $pvVal)
                                            <option value="{{ $pvVal->id }}">{{ $pvVal->variantValueByLanguage->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endforeach
                                    @foreach($stock_variant as $key=>$row)
                                    <div class="form-group">
                                        <label for="inputText3" class="col-form-label">{{ @$row->variantByLanguage->name }}</label>
                                        <select class="select slt slct required" name="stock_variant[{{ $key }}][]" tabindex="3"  multiple="">
                                            {{-- 
                                                <option value="">Select {{ @$row->variantByLanguage->name }}</option>
                                                --}}
                                                @foreach(@$row->variantValues as $svVal)
                                                <option value="{{ @$svVal->id }}">{{ @$svVal->variantValueByLanguage->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @endforeach
                                        @if($price_variant->count())
                                        <div class="form-group">
                                            <label for="price" class="col-form-label">Price</label>
                                            <input id="price" type="text" class="form-control required" id="product_price" name="price" placeholder="Enter Price" onkeypress='validate(event)'>
                                        </div>
                                        @endif
                                        <div class="form-group useadmrgn">
                                            <label for="price" class="col-form-label">&nbsp;</label>
                                            <button type="submit" id="onSubmit" class="plsad"><i class="fa fa-plus" id="add_btn"></i></button>
                                        </div>
                                    </div>
                                    <div id="showVariants">
                                        @php
                                        $i=0;
                                        @endphp
                                        @foreach($fetch_data as $fd)
                                        {{-- @dump($fd) --}}
                                        @if($i!=0)
                                        <div class="adedfrm remove-rw-{{ $fd->group_id }}">
                                            @else
                                            <div class="adedfrm">
                                                @endif
                                                @foreach($fd->productVariantDetails as $pvd)
                                                <div class="form-group">
                                                    @if($i==0)
                                                    <label style="font-weight: 600" for="input-select" class="col-form-label">{{ $pvd->variantByLanguage->name }}</label>
                                                    @endif
                                                    <span class="show-detail remove-rw-{{ $fd->id }}">{{ variantValues($pvd->product_variant_id, $fd->group_id, $pvd->variant_id,  $pvd->getVariantValueId) }}</span>
                                                </div>
                                                @endforeach
                                                @if($price_variant->count())
                                                <div class="form-group">
                                                    @if($i==0)
                                                    <label style="font-weight:600" for="price" class="col-form-label">@lang('admin_lang_static.price')</label>
                                                    @endif
                                                    <span class="show-detail remove-rw-{{ $fd->id }}">{{ $fd->price }} {{ getCurrency() }}</span>
                                                </div>
                                                @endif
                                                <div class="form-group">
                                                    @if($i==0)
                                                    <label style="font-weight:600" class="col-form-label">@lang('admin_lang_static.action') &nbsp;</label>
                                                    @endif
                                                    <a href="javascript:;" title="Remove" class="removeVar remove-rw-{{ $fd->group_id }}" data-id="{{ $fd->group_id }}"><i class="fa fa-times"></i></a>
                                                </div>
                                            </div>
                                            @php
                                            $i++;
                                            @endphp
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="col-form-label"></label>
                                        <a href="{{ route('merchant.edit.product',$slug) }}" class="btn btn-primary nxtop">@lang('admin_lang_static.previous')</a>
                                        <a href="{{ route('merchant.add.product.step.three',$slug) }}" class="btn btn-primary nxtop" @if(count($fetch_data) < 1) style="display: none;" @endif id="nxtBtn">@lang('admin_lang_static.next')</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('scripts')
    {{-- @include('merchant.includes.scripts') --}}
    @if(Config::get('app.locale') == 'en')
    @include('merchant.includes.scripts')
    @elseif(Config::get('app.locale') == 'ar')
    @include('merchant.includes.arabic_scripts')
    @endif
    <script>
        $(document).ready(function(){ 
          $.validator.setDefaults({ ignore: ":hidden:not(.slct)" });
          $("#dependent_form" ).validate({
              rules:{
                  "stock_variant[]":{
                   required:true,
               }
           },
           errorPlacement: function (error , element) {
              //toastr:error(error.text());
          }
      });
          
          $('body').on('click','#add_btn',function(){
              // alert($('.slct').val());
              /*if($('.slct').val() == null){
                  $('.chosen-container').addClass('error');
              }
              if($('.slct').val() != null){
                  $('.chosen-container').removeClass('error');
              }*/
          });
        $('body').on('click','#onSubmit',function(){
            var product_price = $('#price').val();
            if(product_price == 0){
                $('#price').addClass('error');
                return false;
            }else{
                $('#price').removeClass('error');
                return true;
            }
        })
      });
  </script>
  <script>
    $(document).ready(function() {
        $('.slct').chosen();
    })
</script>

{{-- For insert variants with ajax --}}
<script type="text/javascript">
    $(document).ready(function(){
      $('#dependent_form').submit(function(event){
        var emptyStockCount = 0, emptyPriceCount = 0;
        $('.slct').each(function() {
            if($(this).val() == '') {
                emptyStockCount += 1;
                $(this).next().addClass('error')
            } else {
                $(this).next().removeClass('error')    
            }
        });

        $('.price_dependent').each(function() {
            if($(this).val() == '') {
                emptyPriceCount += 1;
                $(this).addClass('error')
            } else {
                $(this).removeClass('error')    
            }
        })

        if(emptyStockCount > 0 || emptyPriceCount > 0) {
            return false;
        }

        var $this = $(this);
        event.preventDefault();
        $('#add_btn').attr('disabled', 'disabled');
        var form_data = new FormData(this);
        $.ajax({
            url:"{{ route('merchant.store.product.variant') }}",
            method:"POST",
            data: form_data,
            contentType: false,
            cache:false,
            processData: false,
            dataType:"json",
            success:function(response) {
                $('#add_btn').removeAttr('disabled');
                $(".slct").val('').trigger("chosen:updated");
                $this[0].reset();
                var html = '';
                if(response.error) {
                    html = (response.error.message) ;
                    $('#erMsg').show();
                    $('#form_result').html(html);
                } else {
                    $('#erMsg').hide();
                    $("#showVariants").html(response.result.output);
                    $('#nxtBtn').show();   
                }
            }
        });
    });
  });
</script>
{{-- for remove row  --}}
<script>
    $(document).ready(function(){
        $('body').on('click','.removeVar',function(){
            var var_id = $(this).data('id');
            var reqDataa = {
                jsonrpc: '2.0'
            };
            
            $.ajax({
                type:'GET',
                url : '{{ url('merchant/merchant-remove-variant') }}/'+var_id,
                data:reqDataa,
                success:function(response){
                    if(response.sucess) {
                        console.log(response.success.proVarCount);
                        $(".remove-rw-"+var_id).hide();
                        if(response.success.proVarCount < 1) {
                            $('#nxtBtn').hide();          
                        } 
                    } else {
                        console.log(response.success.proVarCount);
                        $(".remove-rw-"+var_id).hide();
                        if(response.success.proVarCount < 1) {
                            $('#nxtBtn').hide();          
                        } 
                    }
                }
            });     
        });
    });
    function validate(evt) {
        var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}

</script>
@endsection