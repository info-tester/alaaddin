@extends('merchant.layouts.app')
@section('title', 'Aswagna | Merchant | Add Product Discount')
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    #showVariants{
        width: 100%;
    }
    .adedfrm{
        width: 100%;
        float: left;
    }
    .adedfrm .form-group {
        width: 19%;
        float: left;
        margin-right: 1%;
    }
    .col-form-label { font-size: 15px; width: 100%; }
    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
        border: 1px solid #003eff;
        background: #007fff;
        font-weight: normal;
        text-align: center;
        color: #ffffff;
    }
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {
        border: 1px solid #c5c5c5;
        background: #f6f6f6;
        font-weight: normal;
        color: #454545;
        text-align: center;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ ('merchant.dashboard') }}">@lang('admin_lang_static.dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('admin_lang_static.product_discount')</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('admin_lang_static.product_discount')</h4>
                    </div>
                </div>
                <div class="col-md-12">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session()->has('success'))
                    <div class="alert alert-success vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                    </div>
                    @elseif ((session()->has('error')))
                    <div class="alert alert-danger vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    @endif
                    <div class="alert alert-danger vd_hidden" id="erMsg" style="display: none;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    <div class="block form-block mb-4">
                        <!-- <div class="block-heading">
                            <h5>Default Layout</h5>
                        </div> -->
                        <form action="{{ route('merchant.store.product.discount') }}" method="POST" id="dependent_form">
                            @csrf
                            <div class="row" id="appendRow" style="margin: 0;">
                                <input type="hidden" name="product_id" value="{{ $product_id }}">
                                <input type="hidden" name="slug" id="slug" value="{{ $slug }}">
                                {{-- @dd($fetch_data) --}}
                                @foreach($fetch_data as $key=>$fd)
                                <div class="adedfrm">
                                    @foreach($fd->productVariantDetails as $key1=>$pvd)
                                    @if($pvd->getVariant->type == 'P')
                                    <div class="form-group">
                                        {{-- @if($key == 0) --}}
                                        <label for="input-select" class="col-form-label">{{ $pvd->variantByLanguage->name }}</label>
                                        {{-- @endif --}}
                                        <input type="text" class="form-control required" name="" value="{{ $pvd->variantValueName->default_name }}" disabled="">
                                    </div>
                                    @endif
                                    @endforeach
                                    <div class="form-group">
                                        {{-- @if($key == 0) --}}
                                        <label for="price" class="col-form-label">@lang('admin_lang_static.price')</label>
                                        {{-- @endif --}}
                                        <input id="price" type="number" class="form-control required" name="price[{{ $fd->id }}]" placeholder="Price" value="{{ $fd->price }}" min="0" readonly="">
                                    </div>
                                    <div class="form-group">
                                        {{-- @if($key == 0) --}}
                                        <label for="discount_price" class="col-form-label">@lang('admin_lang_static.discount_price')</label>
                                        {{-- @endif --}}
                                        <input id="discount_price" type="text" class="form-control required" name="discount_price[{{ $fd->id }}]" placeholder="Discount Price" value="{{ $fd->discount_price }}">
                                    </div>
                                    <div class="form-group">
                                        {{-- @if($key == 0) --}}
                                        <label for="from_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
                                        {{-- @endif --}}
                                        <input type="text" class="form-control from_date from_date{{ $fd->id }} datepicker required" name="from_date[{{ $fd->id }}]" data-val="{{ $fd->id }}" placeholder="From Date" value="{{ $fd->from_date }}" readonly="">
                                        <span class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        {{-- @if($key == 0) --}}
                                        <label for="to_date" class="col-form-label">@lang('admin_lang_static.to_date')</label>
                                        {{-- @endif --}}
                                        <input type="text" class="form-control to_date to_date{{ $fd->id }} datepicker required" name="to_date[{{ $fd->id }}]" placeholder="To Date" data-val="{{ $fd->id }}" value="{{ $fd->to_date }}" readonly="">
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="col-form-label"></label>
                                {{-- <a href="{{ route('merchant.add.product.step.two',$slug) }}" class="btn btn-primary nxtop">Previous</a> --}}
                                <button type="submit" class="btn btn-primary nxtop" id="submit">@lang('admin_lang_static.save')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $(".datepicker").datepicker({dateFormat: "yy-mm-dd",
           defaultDate: new Date(),
           minDate: new Date(),
           changeMonth: true,
           changeYear: true,
           yearRange: '-100:+0'
       }); 

        $(".to_date").change(function(){
          var add_id    = $(this).data('val');
          var from_date = $(".from_date"+add_id).val();
          var to_date   = $(this).val();
          if(to_date < from_date) {
            $($(this).next().html('To date can not less than from date.'));
            $("#submit").attr("disabled", true);
        } else {
            $($(this).next().html(''));
            $($('.from_date'+add_id).next().html(''));
            $("#submit").attr("disabled", false);
        }
    })
        $(".from_date").change(function(){
          var add_id    = $(this).data('val');
          var from_date = $(this).val();
          var to_date   = $(".to_date"+add_id).val();
          if(to_date < from_date) {
            $($(this).next().html('From date can not greater than to date.'));
            $("#submit").attr("disabled", true);
        } else {
            $($(this).next().html(''));
            $($('.to_date'+add_id).next().html(''));
            $("#submit").attr("disabled", false);
        }
    })
    });
</script>
<script>
    $(document).ready(function(){ 
      $("#dependent_form" ).validate({
          rules: {
              "discount_price":{
                 required:true,
                 number:true
             }
         },
         errorPlacement: function (error , element) {
              //toastr:error(error.text());
          }
      });
  });
</script>
<script>
    $(document).ready(function() {
        $('.slct').chosen();
    })
</script>
{{-- for fetching models of brand  --}}
<script>
    $(document).ready(function(){

     $('#input-select').change(function(){
      if($(this).val() != '')
      {
       var value  = $(this).val();
       var _token = $('input[name="_token"]').val();
       $.ajax({
        url:"{{ route('sub.cat.fetch') }}",
        method:"POST",
        data:{value:value, _token:_token},
        success:function(result)
        {
         $('#subCategory').html(result);
     }

 })
   }
});

     $('#input-select').change(function(){
      $('#subCategory').val('');
  }); 

 });
</script>
@endsection