@extends('merchant.layouts.app')
@section('title', 'Aswagna | Merchant | Add Product Step Four')
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    #showVariants{
        width: 100%;
    }
    .adedfrm{
        width: 100%;
        float: left;
    }
    .adedfrm .form-group {
        width: 19%;
        float: left;
        margin-right: 1%;
    }
    .col-form-label { font-size: 15px; width: 100%; }
    .action-opt{
        position: absolute;
        right: 0;
        top:5px;
        z-index:1;
    }   
    .set-default-img {
        width: 18px;
        height: 18px;
        font-size: 13px;
        color: #fff;
        text-align: center;
        background:#0771d4;
        border-radius: 50%;
        line-height: 16px;
        float:right;
        margin-left:4px;
    }
    .set-default-img a {
        color: #fff;
        font-size: 10px;
    }
    .remove-img{
        width: 18px;
        height: 18px;
        font-size: 13px;
        color: #fff;
        text-align: center;
        background: #0771d4;
        border-radius: 50%;
        line-height: 16px;
        float:right;
        margin-left:4px;
    }
    .remove-img a {
        color: #fff;
        font-size: 10px;
    }   
    .default-image-span{
        width:100%;
        float:left;
        position:absolute;
        background:rgba(0, 0, 0, 0.5);
        color:#fff;
        text-align:center;
        bottom:0;
        left:0;
        z-index:1;
    }   
    .sub-header-size{
        font-size: 14px;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ ('merchant.dashboard') }}">@lang('admin_lang_static.dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('admin_lang_static.add_product')</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('admin_lang_static.add_product')<span class="sub-header-size"> > {{ @$productName }} > @lang('admin_lang_static.step4_of_4_image')</span></h4>
                    </div>
                </div>
                <div class="col-md-12">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session()->has('success'))
                    <div class="alert alert-success vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                    </div>
                    @elseif ((session()->has('error')))
                    <div class="alert alert-danger vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    @endif
                    <div class="alert alert-danger vd_hidden" id="erMsg" style="display: none;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    <div class="block form-block mb-4">
                        <!-- <div class="block-heading">
                            <h5>Default Layout</h5>
                        </div> -->
                        <form action="{{ route('merchant.store.product.step.four') }}" method="POST" id="image_form" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                                <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 up-btns">
                                    <input type="file" class="custom-file-input inpt" name="image[]" id="gallery-photo-add" multiple="">
                                    <label class="custom-file-label extrlft" for="customFile">@lang('admin_lang_static.upload_image')</label>
                                </div>
                                <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    @if(count($productImg)>0)
                                    <div class="uplodpic">
                                        @foreach($productImg as $proImg)
                                        <li class=" remove-rw-{{ $proImg->id }}">
                                            <img src="{{url('storage/app/public/products/'.@$proImg->image)}}">
                                            @if($proImg->is_default == 'Y')
                                            <span class="default-image-span">Default</span>
                                            @else
                                            <div class="action-opt">
                                                <span class="set-default-img">
                                                    <a href="{{ route('merchant.set.default.product',$proImg->id) }}" title="Set default" class="default-image" data-id="{{ $proImg->id }}">
                                                        <i class="fa fa-check"></i>
                                                    </a>
                                                </span>
                                                <span class="remove-img">
                                                    <a href="javascript:;" title="Remove" class="remove-image" data-id="{{ $proImg->id }}">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </span>
                                            </div>
                                            @endif
                                        </li>
                                        @endforeach
                                    </div>
                                    @endif
                                    <div class="uplodpic gallery">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="col-form-label"></label>
                                @if(@$price_variant > 0 || @$stock_variant > 0)
                                <a href="{{ route('merchant.add.product.step.three',$product->slug) }}" class="btn btn-primary nxtop">@lang('admin_lang_static.previous')</a>
                                @else
                                <a href="{{ route('merchant.edit.product',$product->slug) }}" class="btn btn-primary nxtop">@lang('admin_lang_static.previous')</a>
                                @endif
                                {{-- <a href="#" class="btn btn-primary nxtop">Save</a> --}}
                                <button type="submit" class="btn btn-primary nxtop">@lang('admin_lang_static.save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif
<script>
    $(document).ready(function(){ 
      $("#image_form" ).validate({
          errorPlacement: function (error , element) {
              //toastr:error(error.text());
          }
      });
  });
</script>
<script>
    $(document).ready(function() {
        $('.slct').chosen();
    })
</script>
{{-- for fetching models of brand  --}}
<script>
    $(document).ready(function(){
        
     $('#input-select').change(function(){
      if($(this).val() != '')
      {
       var value  = $(this).val();
       var _token = $('input[name="_token"]').val();
       $.ajax({
        url:"{{ route('sub.cat.fetch') }}",
        method:"POST",
        data:{value:value, _token:_token},
        success:function(result)
        {
         $('#subCategory').html(result);
     }
     
 })
   }
});
     
     $('#input-select').change(function(){
      $('#subCategory').val('');
  }); 
     
 });
</script>
<script type="text/javascript">
    $(function() {
        // Multiple images preview in browser
        var imagesPreview = function(input, placeToInsertImagePreview) {
            
            if (input.files) {
                var filesAmount = input.files.length;
                
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    
                    reader.onload = function(event) {
                        var new_html = '<li><img src="'+event.target.result+'"></li>';
                        $('.gallery').append(new_html);
                        // $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }
                    
                    reader.readAsDataURL(input.files[i]);
                }
            }
            
        };
        
        $('#gallery-photo-add').on('change', function() {
            imagesPreview(this, 'div.gallery');
            $('.gallery').html('');
        });
    });
    
</script>
{{-- for remove row  --}}
<script>
    $(document).ready(function(){
        $('body').on('click','.remove-image',function(){
            var var_id = $(this).data('id');
            var reqDataa = {
                jsonrpc: '2.0'
            };
            
            $.ajax({
                type:'GET',
                url : '{{ url('admin/remove-image') }}/'+var_id,
                data:reqDataa,
                success:function(response){
                    if(response.sucess) {
                        // if(response.sucess.result == 'Add') {
                            $(".remove-rw-"+var_id).hide();
                        // }
                        
                    } else {
                        $(".remove-rw-"+var_id).hide();
                        // console.log(".remove-rw-"+var_id);
                    }
                }
            });     
            
        });   
        
    });
    
</script>
@endsection