@extends('merchant.layouts.app')
@section('title', 'Aswagna | Merchant | Add Product Step Three')
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    #showVariants{
        width: 100%;
    }
    .adedfrm{
        width: 100%;
        float: left;
    }
    .adedfrm .form-group {
        width: 19%;
        float: left;
        margin-right: 1%;
    }
    .col-form-label { font-size: 15px; width: 100%; }
    .sub-header-size{
        font-size: 14px;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ ('merchant.dashboard') }}">@lang('admin_lang_static.dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('admin_lang_static.add_product')</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('admin_lang_static.add_product')<span class="sub-header-size"> > {{ @$productName }} > @lang('admin_lang_static.step3_of_4_stock')</span></h4>
                    </div>
                </div>
                <div class="col-md-12">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session()->has('success'))
                    <div class="alert alert-success vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                    </div>
                    @elseif ((session()->has('error')))
                    <div class="alert alert-danger vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    @endif
                    <div class="alert alert-danger vd_hidden" id="erMsg" style="display: none;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    <div class="block form-block mb-4">
                        <!-- <div class="block-heading">
                            <h5>Default Layout</h5>
                        </div> -->
                        <form action="{{ route('merchant.store.product.step.three') }}" method="POST" id="dependent_form">
                            @csrf
                            <div class="row" id="appendRow" style="margin: 0;">
                                <input type="hidden" name="product_id" value="{{ $product_id }}">
                                <input type="hidden" name="slug" id="slug" value="{{ $slug }}">
                                {{-- @dd($fetch_data) --}}
                                @foreach($fetch_data as $key=>$fd)
                                <div class="adedfrm">
                                    @foreach($fd->productVariantDetails as $key1=>$pvd)
                                    <div class="form-group">
                                        @if($key == 0)
                                        <label for="input-select" class="col-form-label">{{ $pvd->variantByLanguage->name }}</label>
                                        @endif
                                        <input type="text" class="form-control required" name="" value="{{ $pvd->variantValueName->default_name }}" disabled="">
                                    </div>
                                    @endforeach
                                    <div class="form-group">
                                        @if($key == 0)
                                        <label for="stock_quantity" class="col-form-label">@lang('admin_lang_static.stock_quantity')</label>
                                        @endif
                                        <input id="stock_quantity" type="number" class="form-control required" name="stock_quantity[{{ $fd->id }}]" placeholder="Stock Quantity" value="{{ $fd->stock_quantity }}" min="0">
                                    </div>
                                    <div class="form-group">
                                        @if($key == 0)
                                        <label for="weight" class="col-form-label">@lang('admin_lang_static.weight') (Grams)</label>
                                        @endif
                                        <input id="weight" type="text" class="form-control required" name="weight[{{ $fd->id }}]" placeholder="@lang('admin_lang_static.weight')" value="{{ $fd->weight }}" min="0" onkeypress='validate(event)'>
                                    </div>
                                    {{-- 
                                        <div class="form-group useadmrgn">
                                            <a class="crss" href="{{ route('delete.product.variant',$fd->id) }}"><i class="fas fa-times"></i></a>
                                        </div>
                                        --}}                                          
                                    </div>
                                    @endforeach
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword" class="col-form-label"></label>
                                    <a href="{{ route('merchant.add.product.step.two',$slug) }}" class="btn btn-primary nxtop">@lang('admin_lang_static.previous')</a>
                                    {{-- <a href="{{ route('add.product.step.three',$slug) }}" class="btn btn-primary nxtop">Next</a> --}}
                                    <button type="submit" class="btn btn-primary nxtop">@lang('admin_lang_static.next')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif
<script>
    $(document).ready(function(){ 
      $("#dependent_form" ).validate({
          rules: {
           "weight":{
             required:true,
             number:true
         }
     },
     errorPlacement: function (error , element) {
              //toastr:error(error.text());
          }
      });
  });
</script>
<script>
    $(document).ready(function() {
        $('.slct').chosen();
    })
</script>
{{-- for fetching models of brand  --}}
<script>
    $(document).ready(function(){
        
     $('#input-select').change(function(){
      if($(this).val() != '')
      {
       var value  = $(this).val();
       var _token = $('input[name="_token"]').val();
       $.ajax({
        url:"{{ route('sub.cat.fetch') }}",
        method:"POST",
        data:{value:value, _token:_token},
        success:function(result)
        {
         $('#subCategory').html(result);
     }
     
 })
   }
});
     
     $('#input-select').change(function(){
      $('#subCategory').val('');
  }); 
     
 });
    function validate(evt) {
        var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>
@endsection