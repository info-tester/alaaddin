@extends('layouts.app')

@section('title')
{{ config('app.name', 'Alaaddin') }} | Link Expired
@endsection

@section('links')
@include('includes.links')
@endsection

@section('content')
    <!--wrapper start-->
    <div class="wrapper">
    <section class="banner">
        <div class="sgnupbnr"style="background: #ff5252; height: 800px; overflow: hidden;"> 
            <div class="container">
                <div class="login_body">
                    <div class="login_sec" style="min-height: auto;">
                        <h3>Link Expired!</h3>
                        <p>This email link has been expired.</p>
                    </div>
                </div>
            </div>
        
        </div>
  </section>
</div>
<!--wrapper end-->

@endsection

@section('scripts')
@include('includes.scripts')
@endsection