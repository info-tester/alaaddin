@extends('layouts.app')
@section('title')
{{ config('app.name', 'Alaaddin') }} | Mail Verification Success
@endsection
@section('links')
@include('includes.links')
@endsection
@section('content')
<!--wrapper start-->
<div class="wrapper">
    <section class="banner">
        <div class="sgnupbnr"  style="background: #ff5252; height: 800px; overflow: hidden;">
            <div class="container">
                <div class="login_body">
                    <div class="login_sec" style="min-height: auto;">
                        <h3>Verification successful!</h3>
                        <p>You have successfully verified your account.Now you can access your account.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--wrapper end-->
@endsection
@section('scripts')
@include('includes.scripts')
@endsection