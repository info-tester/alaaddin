<table id="dataTable1" class="display table table-striped" data-table="data-table">
    <thead>
    <tr>
        <th>@lang('admin_lang.slno_order')</th>
        <th>@lang('admin_lang.customer_1')</th>
        <th>@lang('admin_lang.date_1')</th>
        <th>@lang('admin_lang.sub_total_kwd')</th>
        <th>@lang('admin_lang.shipping_kwd')</th>
        
        <th>@lang('admin_lang.total_payment_kwd')</th>
        <th>@lang('admin_lang.payment_1')</th>
        <th>@lang('admin_lang.type_2')</th>
        <th>@lang('admin_lang.Status')</th>
        <th>@lang('admin_lang.loyalty_point_received')</th>
        <th>@lang('admin_lang.loyalty_point_used')</th>
        <!-- <th>@lang('admin_lang.Action')</th> -->
    </tr>
    </thead>
    <tbody>
        @if(@$orders)
            @foreach(@$orders as $key=>$order)
                <tr>
                    <td>{{ @$order->order_no }}</td>
                    <td>
                        @if(@$order->user_id != 0)
                        {{ @$order->customerDetails->fname." ".@$order->customerDetails->lname }}
                        @else
                        {{ @$order->fname." ".@$order->lname }}
                        @endif
                        
                    </td>
                    <td>{{ @$order->created_at }}</td>
                    <td>{{ @$order->subtotal }}</td>
                    <td>{{ @$order->shipping_price }}</td>
                    
                    <td>{{ @$order->order_total }}</td>
                    <td>
                        @if(@$order->payment_method == 'C')
                            @lang('admin_lang.cod')
                        @else
                            @lang('admin_lang.online')
                        @endif
                    </td>
                    <td>
                        @if(@$order->order_type == "I")
                        @lang('admin_lang.internal_1')
                        @else
                        @lang('admin_lang.external_1')
                        @endif
                    </td>
                    <td>
                        @if(@$order->status == 'I')
                        Incomplete
                        @elseif(@$order->status == 'N')
                        New
                        @elseif(@$order->status == 'OA')
                        @lang('admin_lang.order_accepted')
                        @elseif(@$order->status == 'DA')
                        @lang('admin_lang.driver_assigned')
                        @elseif(@$order->status == 'RP')
                        @lang('admin_lang.ready_for_pickup')
                        @elseif(@$order->status == 'OP')
                        Picked up
                        @elseif(@$order->status == 'OD')
                        @lang('admin_lang.delivered_1')
                        @elseif(@$order->status == 'OC')
                        @lang('admin_lang.Cancelled_1')
                        @endif
                    </td>
                    <td>{{ @$order->loyalty_point_received }}</td>
                    <td>{{ @$order->loyalty_point_used }}</td>
                    <!-- <td>
                        <a href="{{ route('merchant.view.order',@$order->id) }}"><i class="fa fa-eye" title="View"></i></a>
                        @if(@$order->order_type == "E")
                        @if(@$order->status == "N")

                        <a href="{{ route('merchant.edit.external.order',@$order->id) }}"><i class="fa fa-edit" title='@lang('admin_lang.Edit')'></i></a>
                        @endif
                        @endif
                        {{-- @if(@$order->status == 'N')
                        <a href="{{ route('merchant.cancel.order',@$order->id) }}"><i class="fa fa-times-circle" title='@lang('admin_lang.cancel')'></i></a> 
                        @endif --}}

                    </td> -->
                </tr>
            @endforeach
        @endif
    </tbody>
</table>