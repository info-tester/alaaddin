@extends('merchant.layouts.app')
{{-- @section('title', 'Aswagna | Merchant | Manage Order') --}}
@section('title')
{{ config('app.name', 'Aswagna') }} | @lang('admin_lang.merchant') |@lang('admin_lang.earning_report')
@endsection
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ URL::to('public/merchant/assets/css/chosen.css') }}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    .remove-img {
        width: 18px;
        height: 18px;
        font-size: 13px;
        color: #fff;
        text-align: center;
        background: #0771d4;
        border-radius: 50%;
        line-height: 16px;
        float: right;
        margin-left: 4px;
    }
    .remove-img a {
        color: #fff;
        font-size: 10px;
    }
    .action-opt {
        position: absolute;
        right: 0;
        top: 5px;
        z-index: 1;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">@lang('merchant_lang.Dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('admin_lang.earning_report')</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('admin_lang.earning_report')
                        </h4>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="block form-block mb-4">
                            <!-- <div class="block-heading">
                                <h5>Default Layout</h5>
                            </div> -->

                            <form id="searchForm" method="get" action="{{route('merchant.earning.report.export')}}">
                                <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>@lang('admin_lang.keyword')</label>
                                    <input id="keyword" name="keyword" class="form-control" placeholder="Search by #order/name/email/phone" type="text">
                                </div>
                                <div class="form-group  col-md-6">
                                    <label>@lang('admin_lang.cust')</label>
                                    <select data-placeholder="Choose a Customer..." class="select slt slct"  tabindex="3" id="customer" name="customer">
                                        <option value="">@lang('admin_lang.select_customer')</option>
                                        @if(@$customers)
                                            @foreach(@$customers as $customer)
                                            <option value="{{ @$customer->customerDetails->id }}" data-external="n">{{ @$customer->customerDetails->fname." ".@$customer->customerDetails->lname }}</option>
                                            @endforeach
                                        @endif
                                        @if(@$external_customer)
                                            @foreach(@$external_customer as $external)
                                                <option value="{{ @$external->id }}" data-external="y">{{ @$external->fname." ".@$external->lname }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label>@lang('admin_lang.payment_method')</label>
                                    <select data-placeholder="Choose a payment method..." class="select slt slct"  tabindex="3" id="payment" name="payment">
                                        <option value="">@lang('admin_lang.select_payment_method')</option>
                                        <option value="C">@lang('admin_lang.cod')</option>
                                        <option value="O">@lang('admin_lang.online')</option>
                                    </select>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label>Select order type</label>
                                    <select data-placeholder="Choose a order type..." class="select slt slct"  tabindex="3" id="order_type" name="order_type">
                                        <option value="">Select order type</option>
                                        <option value="I">Internal</option>
                                        <option value="E">External</option>
                                    </select>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for="from_date" class="col-form-label">@lang('admin_lang_static.from_date')</label>
                                    <input type="text" class="form-control datepicker" name="from_date" id="from_date" placeholder="From Date" value="" readonly="">
                                    <span class="text-danger"></span>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for="to_date" class="col-form-label">@lang('admin_lang_static.to_date')</label>
                                    <input type="text" class="form-control datepicker" name="to_date" id="to_date" placeholder="To Date" value="" readonly="">
                                    <span class="text-danger"></span>
                                </div>
                                <div class="form-group flwdt">
                                <button class="btn btn-primary" type="button" id="searchList">@lang('admin_lang.Search')</button>
                                <button class="btn btn-primary" type="submit" id="searchList">@lang('admin_lang.export')</button>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-12" style="padding:0; ">
                        <div class="block table-block mb-4">
                            <div class="row">
                                <div class="table-responsive listBody">
                                    <table id="dataTable1" class="display table table-striped" data-table="data-table">
                                        <thead>
                                        <tr>
                                            <th>@lang('admin_lang.slno_order')</th>
                                            <th>@lang('admin_lang.customer_1')</th>
                                            <th>@lang('admin_lang.date_1')</th>
                                            <th>@lang('admin_lang.sub_total_kwd')</th>
                                            <th>@lang('admin_lang.shipping_kwd')</th>
                                            <th>@lang('admin_lang.total_payment_kwd')</th>
                                            <th>@lang('admin_lang.payment_1')</th>
                                            <th>@lang('admin_lang.type_2')</th>
                                            <th>@lang('admin_lang.Status')</th>
                                            <th>@lang('admin_lang.loyalty_point_received')</th>
                                            <th>@lang('admin_lang.loyalty_point_used')</th>
                                            <!-- <th>@lang('admin_lang.Action')</th> -->
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @if(@$orders)
                                                @foreach(@$orders as $key=>$order)
                                                    <tr>
                                                        <td> {{ @$order->order_no }}</td>
                                                        <td>
                                                            @if(@$order->user_id != 0)
                                                            {{ @$order->customerDetails->fname." ".@$order->customerDetails->lname }}
                                                            @else
                                                            {{ @$order->shipping_fname." ".@$order->shipping_lname }}
                                                            @endif
                                                            
                                                        </td>
                                                        <td>{{ @$order->created_at }}</td>
                                                        <td>{{ @$order->subtotal }}</td>
                                                        <td>{{ @$order->shipping_price }}</td>
                                                        
                                                        <td>{{ @$order->order_total }}</td>
                                                        <td>
                                                            @if(@$order->payment_method == 'C')
                                                                @lang('admin_lang.cod')
                                                            @else
                                                                @lang('admin_lang.online')
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(@$order->order_type == 'I')
                                                            @lang('admin_lang.internal_1')
                                                            @else
                                                            @lang('admin_lang.external_1')
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(@$order->status == 'I')
                                                            Incomplete
                                                            @elseif(@$order->status == 'N')
                                                            New
                                                            @elseif(@$order->status == 'OA')
                                                            @lang('admin_lang.order_accepted')
                                                            @elseif(@$order->status == 'DA')
                                                            @lang('admin_lang.driver_assigned')
                                                            @elseif(@$order->status == 'RP')
                                                            @lang('admin_lang.ready_for_pickup')
                                                            @elseif(@$order->status == 'OP')
                                                            Picked up
                                                            @elseif(@$order->status == 'OD')
                                                            @lang('admin_lang.delivered_1')
                                                            @elseif(@$order->status == 'OC')
                                                            @lang('admin_lang.Cancelled_1')
                                                            @endif
                                                        </td>
                                                        <td>{{ @$order->loyalty_point_received }}</td>
                                                        <td>{{ @$order->loyalty_point_used }}</td>
                                                        <!-- <td>
                                                            <a href="{{ route('merchant.view.order',@$order->id) }}"><i class="fa fa-eye" title="View"></i></a>
                                                            @if(@$order->order_type == "E")
                                                            @if(@$order->status == "N")

                                                            <a href="{{ route('merchant.edit.external.order',@$order->id) }}"><i class="fa fa-edit" title='admin_lang.Edit'></i></a>
                                                            @endif
                                                            @endif
                                                            {{-- @if(@$order->status == 'N')
                                                            <a href="{{ route('merchant.cancel.order',@$order->id) }}"><i class="fa fa-times-circle" title="Cancel"></i></a> 
                                                            @endif --}}

                                                        </td> -->
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loader" style="display: none;">
        <img src="{{url('public/loader.gif')}}">
    </div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
    @if(Config::get('app.locale') == 'en')
    @include('merchant.includes.scripts')
    @elseif(Config::get('app.locale') == 'ar')
    @include('merchant.includes.arabic_scripts')
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script>
    
      $( function() {
        $(".datepicker").datepicker({dateFormat: "yy-mm-dd",
         defaultDate: new Date(),
         maxDate: new Date(),
         changeMonth: true,
         changeYear: true,
         yearRange: '-100:+0'
     }); 
    })
    $( function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 500,
            values: [ 45, 475 ],
            slide: function( event, ui ) {
                $( "#amount" ).val( ui.values[ 0 ]  + "," + ui.values[ 1 ] );
                $('.price_from').html('$' + ui.values[ 0 ])
                $('.price_to').html('$' + ui.values[ 1 ])
            }
        });
        $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 )  +
          "," + $( "#slider-range" ).slider( "values", 1 ) );
        $('.price_from').html('$' + $( "#slider-range" ).slider( "values", 0 ));
        $('.price_to').html('$' + $( "#slider-range" ).slider( "values", 1 ));
        $('.slct').chosen();
        $("#keyword").on("keyup", function(e) {
            var value = $(this).val().toLowerCase();
            $("#dataTable1 tbody tr").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $('#dataTable1').DataTable();

        $("#searchList").click(function(e){

            var customer = $.trim($("#customer").val()),
            external = $.trim($("#customer").children("option:selected").attr("data-external")),
            payment = $.trim($("#payment").val()),
            order_type = $.trim($("#order_type").val()),
            keyword = $.trim($("#keyword").val());
            // alert(external);
            $.ajax({
                type:"GET",
                url:"{{ route('merchant.search.earning.report') }}",
                data:{
                    customer:customer,
                    external:external,
                    payment:payment,
                    order_type:order_type,
                    keyword:keyword
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $('#dataTable1').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                   });
                    $('#dataTable1').DataTable();
                }
            });
        });
        $("#payment").change(function(e){

            var customer = $.trim($("#customer").val()),
            external = $.trim($("#customer").children("option:selected").attr("data-external")),
            payment = $.trim($("#payment").val()),
            order_type = $.trim($("#order_type").val()),
            keyword = $.trim($("#keyword").val());
            
            $.ajax({
                type:"GET",
                url:"{{ route('merchant.search.earning.report') }}",
                data:{
                    customer:customer,
                    external:external,
                    payment:payment,
                    order_type:order_type,
                    keyword:keyword
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $('#dataTable1').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                   });
                    $('#dataTable1').DataTable();
                }
            });
        });
        $("#customer").change(function(e){

            var customer = $.trim($("#customer").val()),
            external = $.trim($("#customer").children("option:selected").attr("data-external")),
            payment = $.trim($("#payment").val()),
            order_type = $.trim($("#order_type").val()),
            keyword = $.trim($("#keyword").val());
            
            $.ajax({
                type:"GET",
                url:"{{ route('merchant.search.earning.report') }}",
                data:{
                    customer:customer,
                    external:external,
                    payment:payment,
                    order_type:order_type,
                    keyword:keyword
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $('#dataTable1').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                   });
                    $('#dataTable1').DataTable();
                }
            });
        });
        $("#order_type").change(function(e){

            var customer = $.trim($("#customer").val()),
            external = $.trim($("#customer").children("option:selected").attr("data-external")),
            payment = $.trim($("#payment").val()),
            order_type = $.trim($("#order_type").val()),
            keyword = $.trim($("#keyword").val());
            // alert(external);
            $.ajax({
                type:"GET",
                url:"{{ route('merchant.search.earning.report') }}",
                data:{
                    customer:customer,
                    external:external,
                    payment:payment,
                    order_type:order_type,
                    keyword:keyword
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $('#dataTable1').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                   });
                    $('#dataTable1').DataTable();
                }
            });
        });


                $("#from_date").change(function(e){

                    var customer = $.trim($("#customer").val()),
                    external = $.trim($("#customer").children("option:selected").attr("data-external")),
                    payment = $.trim($("#payment").val()),
                    order_type = $.trim($("#order_type").val()),
                    keyword = $.trim($("#keyword").val());
                    from_date = $.trim($("#from_date").val());
                    to_date = $.trim($("#to_date").val());
                    if(to_date == '' && to_date < from_date) {
                        return false;
                    }
                    
                    $.ajax({
                        type:"GET",
                        url:"{{ route('merchant.search.earning.report') }}",
                        data:{
                            customer:customer,
                            external:external,
                            payment:payment,
                            order_type:order_type,
                            keyword:keyword,
                            from_date:from_date,
                            to_date:to_date
                        },
                        success:function(resp){
                            $(".listBody").html(resp);
                            $('#dataTable1').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                  });
                            $('#dataTable1').DataTable();
                        }
                    });
                });


                $("#to_date").change(function(e){

                    var customer = $.trim($("#customer").val()),
                    external = $.trim($("#customer").children("option:selected").attr("data-external")),
                    payment = $.trim($("#payment").val()),
                    order_type = $.trim($("#order_type").val()),
                    keyword = $.trim($("#keyword").val());
                    from_date = $.trim($("#from_date").val());
                    to_date = $.trim($("#to_date").val());
                    if(from_date == '' && from_date > to_date) {
                        return false;
                    }
                    
                    $.ajax({
                        type:"GET",
                        url:"{{ route('merchant.search.earning.report') }}",
                        data:{
                            customer:customer,
                            external:external,
                            payment:payment,
                            order_type:order_type,
                            keyword:keyword,
                            from_date:from_date,
                            to_date:to_date
                        },
                        success:function(resp){
                            $(".listBody").html(resp);
                            $('#dataTable1').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                  });
                            $('#dataTable1').DataTable();
                        }
                    });
                });
        $("#searchList").trigger("click");
    });
</script>
<script src="{{ URL::to('public/merchant/assets/js/chosen.jquery.min.js') }}"></script>
@endsection