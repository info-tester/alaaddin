@extends('merchant.layouts.app')
@section('title', 'Alaaddin | Merchant | Dashboard')
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />
<style type="text/css">
    .btn-outline-light {
        color: #fff;
        border: 1px solid #1880c8;
        background: #1880c9;
        transition: all 0.3s;
    }

    .btn-outline-light:hover {
        color: var(--dark-color);
        background: var(--light-color);
        border-color: var(--light-color);
        box-shadow: none;
        transition: all 0.3s;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">@lang('merchant_lang.Dashboard')</a></li>
                    </ol>
                </div>
                
            </div>
        </div>
    </div>


    <!--Main Content-->
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('merchant_lang.add_external_order_link')</h4>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="block counter-block mb-4">
                        <p style="max-width: 100%" >
                            <a id="track_link" href="{{route('merchant.create.anonymous.external.order', ['slug' =>Auth::guard('merchant')->user()->slug,'token' => md5(Auth::guard('merchant')->user()->id)])}}" target="_blank">{{route('merchant.create.anonymous.external.order', ['slug' =>Auth::guard('merchant')->user()->slug,'token' => md5(Auth::guard('merchant')->user()->id)])}}</a>
                            &nbsp;
                            <i class="fa fa-clone" id="copy_link" aria-hidden="true" title="Copy link" data-toggle="tooltip"></i>
                        </p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('admin_lang.overview')</h4>
                    </div>
                </div>
                
                <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="block counter-block mb-4">
                        <div class="value totalProfitMerchant">{{$totalProfit}} @lang('merchant_lang.kwd')</div>
                        <!-- <div class="trending trending-up">
                            <span>12%</span>
                            <i class="fa fa-caret-up"></i>
                        </div> -->
                        <p class="label">@lang('merchant_lang.total_sell') </p>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="block counter-block mb-4">
                        <div class="value todayProfitMerchant">{{$todayProfit}} @lang('merchant_lang.kwd')</div>
                        <!-- <div class="trending trending-down-basic">
                            <span>12%</span>
                            <i class="fa fa-long-arrow-down"></i>
                        </div> -->
                        <p class="label">@lang('merchant_lang.today_sell')</p>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="block counter-block down mb-4">
                        <div class="value total_products">{{$totalProducts}}</div>
                        <p class="label">@lang('merchant_lang.total_products')</p>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="block counter-block counter-bg-img mb-4" style="background: url({{ asset('public/merchant/assets/images/counter-bg-img.jpg') }});">
                        <div class="fade-color">
                            <div class="value text-white totalOrders">{{$totalOrders}}</div>
                            <p class="label text-white">@lang('merchant_lang.total_orders')</p>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('front_static.today')</h4>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="block counter-block mb-4">
                        <div class="row">
                            <div class="col-12">
                                <h4 class="todayExOrders">{{$todayExOrders}}</h4>
                                <p class="label">@lang('merchant_lang.today_ext_order') @lang('admin_lang.delivered_1')</p>
                            </div>
                        </div>
                    </div>
                   
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="block counter-block mb-4">
                        <div class="row">
                            <div class="col-12">
                                <h4 class="todayInOrderEarnings">{{$todayInOrderEarnings}} KWD</h4>
                                <p class="label">@lang('merchant_lang.today_int_order') @lang('admin_lang.delivered_1')</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="block counter-block mb-4">
                        <div class="row">
                            <div class="col-12">
                                <h4 class="todayInOrders">{{$todayInOrders}}</h4>
                                <p class="label">@lang('merchant_lang.todays_int_orders') @lang('admin_lang.delivered_1')</p>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-12 col-sm-6 col-md-3">
                    <div class="block counter-block mb-4">
                        <div class="row">
                            <div class="col-12">
                                <h4 class="todayExOrderEarnings">{{$todayExOrderEarnings}} KWD</h4>
                                <p class="label">@lang('merchant_lang.todays_ext_earn_orders') @lang('admin_lang.delivered_1')</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('merchant_lang.this_month')</h4>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                    <div class="block counter-block bg-primary py-4 mb-4">
                        <div class="row">
                            <div class="col-6">
                                <h4 class="text-white monthlyExOrders">{{$monthlyExOrders}}</h4>
                                <!-- <p class="label text-white">@lang('merchant_lang.this_month_ext_order')</p> -->
                            </div>
                            <div class="col-6">
                                <span class="sparklineBarChartWhite float-right"></span>
                            </div>
                            <div class="col-12">
                            <p class="label text-white">@lang('merchant_lang.this_month_ext_order') @lang('admin_lang.delivered_1')</p>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="block counter-block bg-dark mb-4">
                        <div class="row">
                            <div class="col-12">
                                <h4 class="text-white monthlyInOrderEarnings">{{$monthlyInOrderEarnings}} KWD</h4>
                                <p class="label text-white">@lang('merchant_lang.this_month_int_earn_order') @lang('admin_lang.delivered_1')</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="block counter-block bg-dark mb-4">
                        <div class="row">
                            <div class="col-6">
                                <h4 class="text-white monthlyInOrders">{{$monthlyInOrders}}</h4>
                                <!-- <p class="label text-white">@lang('merchant_lang.this_month_int_ords')</p> -->
                            </div>
                            <div class="col-6">
                                <span class="sparklineBarChartPrimary float-right"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="label text-white">@lang('merchant_lang.this_month_int_ords') @lang('admin_lang.delivered_1')</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                    <div class="block counter-block bg-primary mb-4">
                        <div class="row">
                            <div class="col-12">
                                <h4 class="text-white monthlyExOrderEarnings">{{$monthlyExOrderEarnings}} KWD</h4>
                                <p class="label text-white">@lang('merchant_lang.this_month_ext_earn_ords') @lang('admin_lang.delivered_1')</p>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="col-12 col-md-6 col-lg-6">
                    <div class="block table-block mb-4">
                        <div class="block-heading d-flex align-items-center" style="border:0; padding-bottom: 0;">
                            <h5 class="text-truncate">@lang('merchant_lang.Products')</h5>
                        </div>
                        <div class="custom-scroll" style="max-height: 250px;">
                            <div class="table-responsive text-no-wrap">
                                <table class="table">
                                    <tbody class="text-middle changeResponse">
                                        <tr style="font-weight: 600;">
                                            <td>@lang('admin_lang_static.image')</td>
                                            <td>@lang('admin_lang.title')</td>
                                            <td>@lang('admin_lang_static.price')</td>
                                            <td>@lang('admin_lang.Status')</td>
                                        </tr>
                                        <tr>
                                            @if(count($products) > 0)
                                            @foreach($products as $product)
                                            <td class="product">
                                                @if(@$product->defaultImage->image)
                                                <img class="product-img" data-toggle="tooltip" data-title="{{$product->productByLanguage->title}}" src="{{url('storage/app/public/products/'.@$product->defaultImage->image)}}">
                                                @else
                                                <img class="product-img" data-toggle="tooltip" data-title="{{$product->productByLanguage->title}}" src="{{ asset('public/frontend/images/default_product.png') }}">
                                                @endif
                                            </td>
                                            <td class="product">{{$product->productByLanguage->title}}</td>
                                            <td class="price">{{$product->price}} @lang('admin_lang.kwd')</td>
                                            <td class="status">   
                                                @if($product->seller_status == 'A')
                                                <span class="badge badge-pill bg-success">@lang('admin_lang_static.active')</span>
                                                @elseif($product->seller_status == 'I')
                                                <span class="badge badge-pill bg-danger">@lang('admin_lang_static.inactive')</span>
                                                @elseif($product->seller_status == 'W')
                                                <span class="badge badge-pill bg-warning">@lang('admin_lang_static.awaiting_approval')</span>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="5"><a href="{{route('merchant.manage.product')}}" class="btn btn-outline-light float-right">@lang('admin_lang.view_all')</a></td>
                                        </tr>
                                        @else
                                        <tr>
                                            <td colspan="5"><a href="javascript:;" class="text-center">@lang('admin_lang.no_records_found')</a></td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-6">
                    <div class="block table-block mb-4">
                        <div class="block-heading d-flex align-items-center" style="border:0; padding-bottom: 0;">
                            <h5 class="text-truncate">@lang('admin_lang.order_history')</h5>
                        </div>
                        <div class="custom-scroll" style="max-height: 250px;">
                            <div class="table-responsive text-no-wrap">
                                <table class="table">
                                    <tbody class="text-middle changeOrderResponse" style="overflow-x: scroll;overflow-y: scroll;">
                                        <tr style="font-weight: 600;">
                                            <td>@lang('admin_lang.order_no')</td>
                                            <td>@lang('admin_lang.date')</td>
                                            <td>@lang('admin_lang.Type')</td>
                                            <td>@lang('admin_lang.order_total')</td>
                                            <td>@lang('admin_lang.Status')</td>
                                        </tr>
                                        <tr>
                                            @if(count($orders) > 0)
                                            @foreach($orders as $orders)
                                            <td style="font-weight: 600;"><a href="{{ route('merchant.view.order',@$orders->id) }}"> {{$orders->order_no}} </a></td>
                                            <td class="product">{{$orders->created_at}}</td>
                                            <td class="status">
                                                @if($orders->order_type == 'I')
                                                <span class="badge badge-pill bg-info">@lang('admin_lang.internal_1')</span>
                                                @elseif($orders->order_type == 'E')
                                                <span class="badge badge-pill bg-primary">@lang('admin_lang.external_1')</span>
                                                @endif
                                            </td>
                                            <td class="price">{{$orders->order_total}} @lang('admin_lang.kwd')</td>
                                            <td class="status">                                                
                                                @if($orders->status == 'N')
                                                <span class="badge badge-pill bg-warning">@lang('admin_lang.new')</span>
                                                @elseif($orders->status == 'OA')
                                                <span class="badge badge-pill bg-secondary">@lang('admin_lang.order_accepted')</span>
                                                @elseif($orders->status == 'DA')
                                                <span class="badge badge-pill bg-info">@lang('admin_lang.driver_assigned')</span>
                                                @elseif($orders->status == 'RP')
                                                <span class="badge badge-pill bg-primary">@lang('admin_lang.ready_for_pickup')</span>
                                                @elseif($orders->status == 'OP')
                                                <span class="badge badge-pill bg-warning">@lang('admin_lang.order_picked_up')</span>
                                                @elseif($orders->status == 'OD')
                                                <span class="badge badge-pill bg-success">@lang('admin_lang.order_delivered')</span>
                                                @elseif($orders->status == 'OC')
                                                <span class="badge badge-pill bg-danger">@lang('admin_lang.order_canceled')</span>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="5"><a href="{{route('merchant.list.order')}}" class="btn btn-outline-light float-right">@lang('admin_lang.view_all')</a></td>
                                        </tr>
                                        @else
                                        <tr>
                                            <td colspan="5"><a href="javascript:;" class="text-center">@lang('admin_lang.no_records_found')</a></td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>
<link rel='stylesheet' type='text/css' href='/resources/tutorial/css/example.css'>


<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js"></script>

<!-- stackoverflow solution -->
<script src='https://cdn.firebase.com/js/client/2.2.1/firebase.js'></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-messaging.js"></script>

<script src="{{ asset('public/merchant/assets/js/firebase-notification.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha256-3blsJd4Hli/7wCQ+bmgXfOdK7p/ZUMtPXY08jmxSSgk=" crossorigin="anonymous"></script>
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')

@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif

<script type="text/javascript">
    /*

 Main javascript functions to init most of the elements

 #1. FORM VALIDATION
 #2. CK EDITOR
 #3. SORTABLE -- TASKS
 #4. CHARTJS CHARTS
 #4.1 LINE CHARTS
 #4.2 BAR CHARTS
 #4.3 PIE & DOUGHNUT CHARTS

 #5  MENU/NAVIGATION
 #6. FULL BODY COLORED SCROLL
 #7. NAVIGATION SCROLL
 #8. BOOTSTRAP RELATED JS ACTIVATIONS

 */

'use strict';
$(function () {
    /*----------------------------------------
     // - #0. COLOR VARIABLES
     ----------------------------------------*/

    var primaryColor = '#1880c9';
    var primaryAlphaDot5 = 'rgba(24,128,201,0.5)';
    var primaryAlpha = 'rgba(24,128,201,0)';

    var whiteColor = '#fff';
    var whiteAlphaDot5 = 'rgba(255,255,255,0.5)';
    var whiteAlphaDot25 = 'rgba(255,255,255,0.25)';
    var whiteAlpha = 'rgba(255,255,255,0)';

    var lightColor ='#f1f1f1';

    var darkColor = '#2a3f5a';

    var secondaryColor = '#a5b5c5';
    var secondaryAlphaDot5 = 'rgba(165,181,197,0.5)';

    var successColor = '#66bb6a';
    var successAlphaDot5 = 'rgba(102,187,106,0.5)';

    var dangerColor = '#f65f6e';
    var dangerAlphaDot5 = 'rgba(246,95,110,0.5)';

    /*----------------------------------------
     // - #1. FORM VALIDATION
     ----------------------------------------*/
    if ($('#needs-validation').length) {
        "use strict";
        var form = document.getElementById("needs-validation");
        form.addEventListener("submit", function(event) {
            if (form.checkValidity() == false) {
                event.preventDefault();
                event.stopPropagation();
            }
            form.classList.add("was-validated");
        }, false);
    }


    if ($("#fullCalendar").length) {
        var calendar, d, date, m, y;
        date = new Date();
        d = date.getDate();
        m = date.getMonth();
        y = date.getFullYear();

        calendar = $("#fullCalendar").fullCalendar({
            header: {
                left: "prev,next today",
                center: "title",
                right: "month,agendaWeek,agendaDay"
            },
            selectable: true,
            selectHelper: true,
            select: function select(start, end, allDay) {
                var title;
                title = prompt("Event Title:");
                if (title) {
                    calendar.fullCalendar("renderEvent", {
                        title: title,
                        start: start,
                        end: end,
                        allDay: allDay
                    }, true);
                }
                return calendar.fullCalendar("unselect");
            },
            editable: true,
            events: [{
                title: "Long Event",
                start: new Date(y, m, 3, 12, 0),
                end: new Date(y, m, 7, 14, 0)
            }, {
                title: "Lunch",
                start: new Date(y, m, d, 12, 0),
                end: new Date(y, m, d + 2, 14, 0),
                allDay: false
            }, {
                title: "Click for Google",
                start: new Date(y, m, 28),
                end: new Date(y, m, 29),
                url: "http://google.com/"
            }]
        });
    }

    /*----------------------------------------
     // - #2. CK EDITOR
     ----------------------------------------*/
    if ($('#textEditor').length) {
        CKEDITOR.replace('textEditor');
    }

    /*----------------------------------------
     // - #3. SORTABLE -- TASKS
     ----------------------------------------*/
    if($('#sortable1').length){
        var todo = document.getElementById('todo');
        var inprog = document.getElementById('inprogress');
        var complete = document.getElementById('taskdone');

        Sortable.create(todo, {
            group: "shared",
            scroll: true,
            sort: true
        });

        Sortable.create(inprog, {
            group: "shared",
            scroll: true,
            sort: true
        });

        Sortable.create(complete, {
            group: "shared",
            scroll: true,
            sort: true
        });
    }

    
    if($('.sparklineBarChartWhite').length){
        var myValue = {!! json_encode(@$extValue) !!};
        $('.sparklineBarChartWhite').sparkline(myValue,{
            type:'bar',
            barColor: whiteColor,
            height: "60",
            barWidth: 3,
            resize: true,
            barSpacing: 8
        });
    }

    if($('.sparklineBarChartPrimary').length){
        var myValue = {!! json_encode(@$intValue) !!};
        $('.sparklineBarChartPrimary').sparkline(myValue,{
            type:'bar',
            barColor: whiteColor,
            height: "60",
            barWidth: 3,
            resize: true,
            barSpacing: 8
        });
    }


    /*----------------------------------------
     // #4. CHARTJS CHARTS http://www.chartjs.org/
     ----------------------------------------*/

    if (typeof Chart !== 'undefined') {

        var fontFamily = 'Roboto';
        // set defaults
        Chart.defaults.global.defaultFontFamily = fontFamily;
        Chart.defaults.global.tooltips.titleFontSize = 14;
        Chart.defaults.global.tooltips.titleMarginBottom = 4;
        Chart.defaults.global.tooltips.displayColors = false;
        Chart.defaults.global.tooltips.bodyFontSize = 12;
        Chart.defaults.global.tooltips.xPadding = 10;
        Chart.defaults.global.tooltips.yPadding = 8;

        // init lite line chart if element exists

        // - #4.1 LINE CHARTS

        // Filled Line Chart //

        if ($("#filledLineChart").length) {
            var filledLineChart = document.getElementById("filledLineChart").getContext('2d');

            var primaryGradient = filledLineChart.createLinearGradient(0, 0, 0, 200);
            primaryGradient.addColorStop(0, primaryAlphaDot5);
            primaryGradient.addColorStop(1, primaryAlpha);


            // line chart data
            var filledLineData = {
                labels: ["1", "2", "3", "4", "5", "6", "7", "8"],
                datasets: [{
                    label: "Visitors Graph",
                    fill: true,
                    backgroundColor: primaryGradient,
                    borderColor: primaryColor,
                    borderCapStyle: 'butt',
                    borderWidth: 2,
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "transparent",
                    pointBackgroundColor: primaryColor,
                    pointBorderWidth: 0,
                    pointHoverRadius: 4,
                    pointHoverBackgroundColor: primaryColor,
                    pointHoverBorderColor: primaryColor,
                    pointHoverBorderWidth: 0,
                    pointRadius: 3,
                    pointHitRadius: 10,
                    data: [16, 12, 24, 12, 15, 12, 25, 24]
                }]
            };

            // line chart init
            var filledLineChart = new Chart(filledLineChart, {
                type: 'line',
                data: filledLineData,
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: '11',
                                fontColor: secondaryColor
                            },
                            gridLines: {
                                color: lightColor,
                                zeroLineColor: lightColor
                            }
                        }],
                        yAxes: [{
                            /*display: true,*/
                            ticks: {
                                beginAtZero: true,
                                max: 40,
                                stepSize: 10,
                                fontSize: '11',
                                fontColor: secondaryColor
                            },
                            gridLines: {
                                color: 'transparent',
                                zeroLineColor: 'transparent'
                            }
                        }]
                    }
                }
            });
        }

        // Single Line Chart //

        if ($("#singleLineChart").length) {

            var singleLineChart = document.getElementById("singleLineChart").getContext('2d');

            // line chart data
            var singleLineData = {
                labels: ["1", "2", "3", "4", "5", "6", "7", "8"],
                datasets: [{
                    label: "Visitors Graph",
                    fill: false,
                    borderColor: primaryColor,
                    borderCapStyle: 'butt',
                    borderWidth: 2,
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "transparent",
                    pointBackgroundColor: primaryColor,
                    pointBorderWidth: 0,
                    pointHoverRadius: 4,
                    pointHoverBackgroundColor: primaryColor,
                    pointHoverBorderColor: primaryColor,
                    pointHoverBorderWidth: 0,
                    pointRadius: 3,
                    pointHitRadius: 10,
                    data: [16, 12, 24, 12, 15, 12, 25, 24]
                }]
            };

            var singleLineChart = new Chart(singleLineChart, {
                type: 'line',
                data: singleLineData,
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: '11',
                                fontColor: secondaryColor
                            },
                            gridLines: {
                                color: lightColor,
                                zeroLineColor: lightColor
                            }
                        }],
                        yAxes: [{
                            /*display: true,*/
                            ticks: {
                                beginAtZero: true,
                                max: 40,
                                stepSize: 10,
                                fontSize: '11',
                                fontColor: secondaryColor
                            },
                            gridLines: {
                                color: 'transparent',
                                zeroLineColor: 'transparent'
                            }
                        }]
                    }
                }
            });
        }


        if ($("#filledWhiteLineChart").length) {
            var filledWhiteLineChart = document.getElementById("filledWhiteLineChart").getContext('2d');

            var whiteGradient = filledWhiteLineChart.createLinearGradient(0, 0, 0, 200);
            whiteGradient.addColorStop(0, whiteAlphaDot5);
            whiteGradient.addColorStop(1, whiteAlpha);

            // line chart data
            var filledWhiteLineData = {
                labels: ["1", "2", "3", "4", "5", "6", "7", "8"],
                datasets: [{
                    label: "Visitors Graph",
                    fill: true,
                    backgroundColor: whiteGradient,
                    borderColor: whiteColor,
                    borderCapStyle: 'butt',
                    borderWidth: 2,
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "transparent",
                    pointBackgroundColor: whiteColor,
                    pointBorderWidth: 0,
                    pointHoverRadius: 4,
                    pointHoverBackgroundColor: whiteColor,
                    pointHoverBorderColor: whiteColor,
                    pointHoverBorderWidth: 0,
                    pointRadius: 3,
                    pointHitRadius: 10,
                    data: [16, 12, 24, 12, 15, 12, 25, 24]
                }]
            };

            // line chart init
            var filledWhiteLineChart = new Chart(filledWhiteLineChart, {
                type: 'line',
                data: filledWhiteLineData,
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: '11',
                                fontColor: whiteColor
                            },
                            gridLines: {
                                color: whiteAlphaDot25,
                                zeroLineColor: whiteAlphaDot25
                            }
                        }],
                        yAxes: [{
                            /*display: true,*/
                            ticks: {
                                beginAtZero: true,
                                max: 40,
                                stepSize: 10,
                                fontSize: '11',
                                fontColor: whiteColor
                            },
                            gridLines: {
                                color: 'transparent',
                                zeroLineColor: 'transparent'
                            }
                        }]
                    }
                }
            });
        }



        //Double Line Chart

        if ($("#doubleLineChart").length) {
            var doubleLineChart = document.getElementById("doubleLineChart").getContext('2d');


            // line chart data
            var doubleLineData = {
                labels: ["1", "2", "3", "4", "5", "6", "7", "8"],
                datasets: [{
                    label: "Visitors Graph",
                    fill: false,
                    borderColor: primaryColor,
                    borderCapStyle: 'butt',
                    borderWidth: 2,
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "transparent",
                    pointBackgroundColor: primaryColor,
                    pointBorderWidth: 0,
                    pointHoverRadius: 4,
                    pointHoverBackgroundColor: primaryColor,
                    pointHoverBorderColor: primaryColor,
                    pointHoverBorderWidth: 0,
                    pointRadius: 3,
                    pointHitRadius: 10,
                    data: [16, 12, 24, 12, 15, 12, 25, 24]
                },{
                    label: "Downloads Graph",
                    fill: false,
                    borderColor: darkColor,
                    borderCapStyle: 'butt',
                    borderWidth: 2,
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "transparent",
                    pointBackgroundColor: darkColor,
                    pointBorderWidth: 0,
                    pointHoverRadius: 4,
                    pointHoverBackgroundColor: darkColor,
                    pointHoverBorderColor: darkColor,
                    pointHoverBorderWidth: 0,
                    pointRadius: 3,
                    pointHitRadius: 10,
                    data: [21, 25, 13, 25, 20, 24, 12, 13]
                }]
            };

            // line chart init
            var doubleLineChart = new Chart(doubleLineChart, {
                type: 'line',
                data: doubleLineData,
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontSize: '11',
                                fontColor: secondaryColor
                            },
                            gridLines: {
                                color: lightColor,
                                zeroLineColor: lightColor
                            }
                        }],
                        yAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                max: 40,
                                stepSize: 10,
                                fontSize: '11',
                                fontColor: secondaryColor
                            },
                            gridLines: {
                                color: 'transparent',
                                zeroLineColor: 'transparent'
                            }
                        }]
                    }
                }
            });
        }

        // - #4.2 BAR CHARTS

        // - Bar Chart --> Primary Color
        if($('#barGraphPrimary').length) {

            var barGraphPrimary = document.getElementById("barGraphPrimary").getContext("2d");

            var barPrimaryData1 = {
                labels: ["January", "February", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct"],
                datasets: [{
                    label: "Orders",
                    backgroundColor: primaryColor,
                    hoverBackgroundColor: primaryColor,
                    hoverBorderColor: primaryAlphaDot5,
                    hoverBorderWidth: 5,
                    data: [24, 42, 18, 34, 56, 28, 24, 42, 18, 34]
                }]
            };
            // -----------------
            // init bar chart
            // -----------------
            var myBarPrimary = new Chart(barGraphPrimary,
                {
                    type: 'bar',
                    data: barPrimaryData1,
                    options: {
                        scales: {
                            xAxes: [{
                                display: false,
                                ticks: {
                                    fontSize: '11',
                                    fontColor: secondaryColor
                                },
                                gridLines: {
                                    display: false,
                                    color: lightColor,
                                    zeroLineColor: lightColor
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    fontColor: secondaryColor,
                                },
                                gridLines: {
                                    color: lightColor,
                                    zeroLineColor: primaryColor
                                }
                            }]
                        },
                        legend: {
                            display: false
                        },
                        animation: {
                            animateScale: true,
                            duration: 5000
                        }

                    }
                });
        }


        //- Bar Graph --> Gradient
        if($('#barGraphGradient').length) {

            var barGraphGradient = document.getElementById("barGraphGradient").getContext("2d");

            var primaryGradient = barGraphGradient.createLinearGradient(0, 0, 0, 180);
            primaryGradient.addColorStop(0, primaryAlphaDot5);
            primaryGradient.addColorStop(1, primaryColor);

            var barGradientData1 = {
                labels: ["January", "February", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct"],
                datasets: [{
                    label: "Orders",
                    backgroundColor: primaryGradient,
                    hoverBackgroundColor: primaryColor,
                    hoverBorderColor: primaryAlphaDot5,
                    hoverBorderWidth: 5,
                    data: [24, 42, 18, 34, 56, 28, 24, 42, 18, 34]
                }]
            };
            // -----------------
            // init bar chart
            // -----------------
            var myBarGradient = new Chart(barGraphGradient,
                {
                    type: 'bar',
                    data: barGradientData1,
                    options: {
                        scales: {
                            xAxes: [{
                                display: false,
                                ticks: {
                                    fontSize: '11',
                                    fontColor: secondaryColor
                                },
                                gridLines: {
                                    display: false,
                                    color: lightColor,
                                    zeroLineColor: lightColor
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    fontColor: secondaryColor,
                                },
                                gridLines: {
                                    color: lightColor,
                                    zeroLineColor: primaryColor
                                }
                            }]
                        },
                        legend: {
                            display: false,
                        },
                        animation: {
                            animateScale: true
                        }
                    }
                });

        }

        //- Bar Graph --> White Outline With Primary Background

        if($('#barGraphWhite').length) {

            var barGraphWhite = document.getElementById("barGraphWhite").getContext("2d");

            var barWhiteData1 = {
                labels: ["January", "February", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct"],
                datasets: [{
                    label: "Orders",
                    backgroundColor: whiteAlpha,
                    hoverBackgroundColor: whiteColor,
                    borderColor: whiteColor,
                    hoverBorderColor: whiteColor,
                    borderWidth: 2,
                    data: [24, 42, 18, 34, 56, 28, 24, 42, 18, 34]
                }]
            };
            // -----------------
            // init bar chart
            // -----------------
            var myBarWhite = new Chart(barGraphWhite,
                {
                    type: 'bar',
                    data: barWhiteData1,
                    options: {
                        scales: {
                            xAxes: [{
                                display: false,
                                ticks: {
                                    fontSize: '11',
                                    fontColor: whiteAlphaDot5
                                },
                                gridLines: {
                                    display: false,
                                    color: 'rgba(255,255,255,0.15)',
                                    zeroLineColor: whiteAlphaDot5
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    fontColor: whiteAlphaDot5,
                                },
                                gridLines: {
                                    color: 'rgba(255,255,255,0.15)',
                                    zeroLineColor: whiteAlphaDot5
                                }
                            }]
                        },
                        legend: {
                            display: false,
                        },
                        animation: {
                            animateScale: true,
                        }
                    }
                });

        }

        //- Bar Graph --> Dark Colors
        if($('#barGraphDark').length) {
            var barGraphDark = document.getElementById("barGraphDark").getContext("2d");
            var barDarkData1 = {
                labels: ["January", "February", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct"],
                datasets: [{
                    label: "Orders",
                    backgroundColor: darkColor,
                    hoverBackgroundColor: primaryColor,
                    hoverBorderColor: primaryAlphaDot5,
                    hoverBorderWidth: 5,
                    data: [24, 42, 18, 34, 56, 28, 24, 42, 18, 34]
                }]
            };
            // -----------------
            // init bar chart
            // -----------------
            var myBarDark = new Chart(barGraphDark,
                {
                    type: 'bar',
                    data: barDarkData1,
                    options: {
                        scales: {
                            xAxes: [{
                                display: false,
                                ticks: {
                                    fontSize: '11',
                                    fontColor: secondaryColor
                                },
                                gridLines: {
                                    display: false,
                                    color: lightColor,
                                    zeroLineColor: lightColor
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    fontColor: secondaryColor,
                                },
                                gridLines: {
                                    color: lightColor,
                                    zeroLineColor: darkColor
                                }
                            }]
                        },
                        legend: {
                            display: false
                        },
                        animation: {
                            animateScale: true
                        }
                    }
                });

        }

        //- Bar Graph --> Light Grey Color

        if($('#barGraphLight').length) {

            var barGraphLight = document.getElementById("barGraphLight").getContext("2d");

            var barLightData1 = {
                labels: ["January", "February", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct"],
                datasets: [{
                    label: "Orders",
                    backgroundColor: secondaryColor,
                    hoverBackgroundColor: primaryColor,
                    hoverBorderColor: primaryAlphaDot5,
                    hoverBorderWidth: 5,
                    data: [24, 42, 18, 34, 56, 28, 24, 42, 18, 34]
                }]
            };
            // -----------------
            // init bar chart
            // -----------------
            var myBarLight = new Chart(barGraphLight,
                {
                    type: 'bar',
                    data: barLightData1,
                    options: {
                        scales: {
                            xAxes: [{
                                display: false,
                                ticks: {
                                    fontSize: '11',
                                    fontColor: secondaryColor
                                },
                                gridLines: {
                                    color: lightColor,
                                    zeroLineColor: lightColor
                                }
                            }],
                            yAxes: [{
                                display: true,
                                ticks: {
                                    beginAtZero: true,
                                    fontColor: secondaryColor,
                                },
                                gridLines: {
                                    color: lightColor,
                                    zeroLineColor: secondaryColor
                                }
                            }]
                        },
                        legend: {
                            display: false,
                        },
                        animation: {
                            animateScale: true,
                        }
                    }
                });

        }



        // - #4.3 PIE & DOUGHNUT CHARTS

        // Pie Chart
        if($('#pieChart').length){
            var pieChart = document.getElementById("pieChart").getContext('2d');

            var pieData = {
                labels: ["Processed", "Pending", "Cancelled","Completed"],
                datasets: [{
                    data: [135, 300,95,225],
                    backgroundColor: [primaryColor, secondaryColor,successColor,dangerColor],
                    hoverBackgroundColor: [primaryColor, secondaryColor,successColor,dangerColor],
                    borderWidth: 0,
                    hoverBorderWidth: 6,
                    hoverBorderColor: [primaryAlphaDot5, secondaryAlphaDot5,successAlphaDot5,dangerAlphaDot5]
                }]
            };

            var pieChartData = new Chart(pieChart, {
                type: 'pie',
                data: pieData,
                options: {
                    legend: {
                        display: false,
                    },
                    animation: {
                        animateScale: true,
                    }
                }
            });
        }


        // Doughnut Chart
        if($('#doghnutChart').length){
            var doghnutChart = document.getElementById("doghnutChart").getContext('2d');

            var doghnutData = {
                labels: ["Processed", "Pending", "Cancelled","Completed"],
                datasets: [{
                    data: [135, 300,95,225],
                    backgroundColor: [primaryColor, secondaryColor,successColor,dangerColor],
                    hoverBackgroundColor: [primaryColor, secondaryColor,successColor,dangerColor],
                    borderWidth: 0,
                    hoverBorderWidth: 6,
                    hoverBorderColor: [primaryAlphaDot5, secondaryAlphaDot5,successAlphaDot5,dangerAlphaDot5]
                }]
            };

            var doughnutChartData = new Chart(doghnutChart, {
                type: 'doughnut',
                data: doghnutData,
                options: {
                    legend: {
                        display: false
                    },
                    animation: {
                        animateScale: true
                    },
                    cutoutPercentage: 80
                }
            });
        }


    }
});
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#copy_link').click(function() {
            CopyToClipboard('track_link')
        })
    })
    function CopyToClipboard(containerid) {
        if (window.getSelection) {
            if (window.getSelection().empty) { // Chrome
                window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) { // Firefox
                window.getSelection().removeAllRanges();
            }
        } else if (document.selection) { // IE?
            document.selection.empty();
        }

        if (document.selection) {
            var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById(containerid));
            range.select().createTextRange();
            document.execCommand("copy");
        } else if (window.getSelection) {
            var range = document.createRange();
            range.selectNode(document.getElementById(containerid));
            window.getSelection().addRange(range);
            document.execCommand("copy");
        }
        toastr.success('Link copied!');
    }
</script>
@endsection
