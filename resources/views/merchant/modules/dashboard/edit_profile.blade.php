@extends('merchant.layouts.app')
@section('title', 'Alaaddin | Merchant | Edit Profile')
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">
<style type="text/css">
    .remove-img {
        width: 18px;
        height: 18px;
        font-size: 13px;
        color: #fff;
        text-align: center;
        background: #0771d4;
        border-radius: 50%;
        line-height: 16px;
        float: right;
        margin-left: 4px;
    }
    .remove-img a {
        color: #fff;
        font-size: 10px;
    }
    .action-opt {
        position: absolute;
        right: 0;
        top: 5px;
        z-index: 1;
    }
    .width-set .file_div {
        width: 40%;
    }
</style>
<style type="text/css">
    label.error{
    border: none !important;
    background-color: #fff !important;
    color: #f00;
    }
    #cityList{
    position: absolute;
    width: 96%;
    /*height: 200px;*/
    z-index: 99;
    }
    #cityList ul{
    background: #fff;
    width: 96%;
    border: solid 1px #eee;
    padding: 0;
    max-height: 200px;
    overflow-y: scroll;
    }
    #cityList ul li{
    list-style: none;
    padding: 5px 15px;
    cursor: pointer;
    border-bottom: solid 1px #eee;
    }
    #cityList ul li:hover{
    background: #3a82c4;
    color: #fff;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">@lang('merchant_lang.Dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('merchant_lang.EditProfile')</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('merchant_lang.EditProfile')</h4>
                    </div>
                </div>
                <div class="col-md-12">

                    @if (session()->has('success'))
                    <div class="alert alert-success vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                    </div>
                    @elseif ((session()->has('error')))
                    <div class="alert alert-danger vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong id="form_result">{{ session('error')['code'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    @endif
                    <div class="block form-block mb-4">
                        <!-- <div class="block-heading">
                            <h5>Default Layout</h5>
                        </div> -->
                        <form action="{{ route('merchant.store.profile') }}" method="POST" id="edit_form" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>@lang('merchant_lang.FirstName')</label>
                                    <input class="form-control" placeholder="@lang('merchant_lang.FirstName')" name="fname" type="text" value="{{ @Auth::guard('merchant')->user()->fname }}" readonly="">
                                </div>
                                <div class="form-group  col-md-6">
                                    <label>@lang('merchant_lang.LastName')</label>
                                    <input class="form-control" placeholder="@lang('merchant_lang.LastName')" name="lname" type="text" value="{{ @Auth::guard('merchant')->user()->lname }}" readonly="">
                                </div>
                                <div class="form-group  col-md-6">
                                    <label>@lang('merchant_lang.Email')</label>
                                    <input class="form-control" placeholder="@lang('merchant_lang.Email')" type="text" name="email" id="email" value="{{ @Auth::guard('merchant')->user()->email }}" readonly="">
                                    <span id="chck_eml_rd" class="chck_eml_rd text-danger"></span>
                                    <span id="chck_eml_grn" class="chck_eml_grn text-success"></span>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label>@lang('merchant_lang.PhoneNumber')</label>
                                    <input class="form-control" placeholder="@lang('merchant_lang.PhoneNumber')" type="text" name="phone" value="{{ @Auth::guard('merchant')->user()->phone }}" readonly="">
                                </div>
                                <div class="form-group  col-md-6">
                                    <label>@lang('merchant_lang.CompanyName')</label>
                                    <input class="form-control" placeholder="@lang('merchant_lang.CompanyName')" type="text" name="company_name" value="{{ @Auth::guard('merchant')->user()->company_name }}" readonly="">
                                </div>
                                <div class="form-group  col-md-6 mmtp">
                                    {{-- <div class="file_upload">
                                        @lang('merchant_lang.UploadImage')
                                        <input type="file" class="form-control choose_question @if(!@Auth::guard('merchant')->user()->image) @endif" id="imgInp" name="image" accept="*" value="{{ @Auth::guard('merchant')->user()->image }}" />
                                    </div> --}}
                                    <div class="uploaded_ppc" >
                                        @if(@Auth::guard('merchant')->user()->image)
                                        <img id="blah" src="{{url('storage/app/public/profile_pics/'.@Auth::guard('merchant')->user()->image)}}">
                                        @else
                                        <img id="blah" />
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <h5 class="fultxt">@lang('merchant_lang.StoreAddress')</h5>
                                </div>
                                <hr>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="country" class="col-form-label">@lang('merchant_lang.Country')</label>
                                    <select class="custom-select form-control" id="country" name="country" disabled="">
                                        <option value="">@lang('merchant_lang.select_country')</option>
                                        @foreach($country as $cn)
                                        <option value="{{ $cn->id }}" @if($cn->id == @Auth::guard('merchant')->user()->country) selected @endif>{{ @$cn->countryDetailsBylanguage->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @php
                                $c = @Auth::guard('merchant')->user()->country; 
                                @endphp
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 outside_kuwait_city" @if($c==134) style="display: none;"@endif>
                                    <label for="city" class="col-form-label">@lang('merchant_lang.City')</label>
                                    <input id="city" type="text" class="form-control" placeholder="@lang('merchant_lang.City')" name="city" value="{{ @Auth::guard('merchant')->user()->city }}" readonly="">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12  inside_kuwait_city" @if($c!=134) style="display: none;"@endif>
                                    <label for="kuwait_city_lb" class="col-form-label">@lang('admin_lang.city')(@lang('admin_lang.required'))</label>
                                    <input type="text" class="form-control required insideKuwCity" name="kuwait_city" id="kuwait_city" placeholder="@lang('admin_lang.city')" value="@if(@$merchant->merchantCityDetails != null) {{@$merchant->merchantCityDetails->name }} @endif" autocomplete="off">
                                    <div id="cityList"></div>
                                    <input type="hidden" id="kuwait_city_value_id" name="kuwait_city_value_id" value="{{@$merchant->merchantCityDetails->city_id }}">
                                    <input type="hidden" id="kuwait_city_value_name" name="kuwait_city_value_name" value="{{@$merchant->merchantCityDetails->name }}">
                                    <span class="error_kuwait_city removeText city_id_err text-danger" ></span>
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="state" class="col-form-label">@lang('merchant_lang.State')</label>
                                    {{-- <select class="custom-select form-control required" id="state" name="state">
                                        <option value="">@lang('merchant_lang.select_state')</option>
                                        @foreach($state as $st)
                                        <option value="{{ $st->id }}" @if($st->id == @Auth::guard('merchant')->user()->state) selected="" @endif>{{ $st->name }}</option>
                                        @endforeach
                                    </select> --}}
                                    <input type="text" class="form-control" placeholder="@lang('merchant_lang.State')" name="state" value="{{ @Auth::guard('merchant')->user()->state }}" readonly="">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="address" class="col-form-label">@lang('merchant_lang.Street')</label>
                                    <input id="address" type="text" class="form-control" placeholder="@lang('merchant_lang.Street')" name="address" value="{{ @Auth::guard('merchant')->user()->address }}" readonly="">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="avenue" class="col-form-label">@lang('merchant_lang.avenue')</label>
                                    <input id="avenue" type="text" class="form-control" placeholder="@lang('merchant_lang.avenue')" name="avenue" value="{{ @Auth::guard('merchant')->user()->avenue }}" readonly="">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="building_number" class="col-form-label">@lang('merchant_lang.building_number')</label>
                                    <input id="Building Number" type="text" class="form-control" placeholder="@lang('merchant_lang.building_number')" name="building_number" value="{{ @Auth::guard('merchant')->user()->building_number }}" readonly="">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="zipcode" class="col-form-label">@lang('merchant_lang.Zip')</label>
                                    <input id="zipcode" type="text" class="form-control" placeholder="Zip" name="zipcode" value="{{ @Auth::guard('merchant')->user()->zipcode }}" readonly="">
                                </div>
                                <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="address_note" class="col-form-label">@lang('merchant_lang.address_note')</label>
                                    {{-- <input id="address_note" type="text" class="form-control" placeholder="Address Note" name="address_note" value="{{ @Auth::guard('merchant')->user()->address_note }}" readonly=""> --}}
                                    <textarea class="form-control" rows="4" name="address_note" disabled="">{{ @Auth::guard('merchant')->user()->address_note }}</textarea>
                                </div>
                                <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="display: none;" >
                                    <div class="file_upload">
                                        @lang('merchant_lang.UploadPortfolioImages')
                                        <input type="file" class="form-control choose_question" id="gallery-photo-add" name="images[]" accept="*" multiple=""/>
                                    </div>
                                </div>
                                <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">                    
                                    @if(count(@$marchentImg)>0)
                                    <div class="uplodpic">
                                        @foreach($marchentImg as $marchImg)
                                        <li class=" remove-rw-{{ $marchImg->id }}">
                                            <img src="{{url('storage/app/public/merchant_portfolio/'.@$marchImg->image)}}">
                                            {{-- @if($proImg->is_default == 'Y')
                                            <span class="default-image-span">Default</span>
                                            @else --}}
                                            <div class="action-opt">
                                                {{-- <span class="set-default-img">
                                                    <a href="{{ route('set.default.product',$marchImg->id) }}" title="Set default" class="default-image" data-id="{{ $marchImg->id }}">
                                                        <i class="fas fa-check"></i>
                                                    </a>
                                                </span> --}}

                                                {{-- //for remove image --}}
                                                {{-- <span class="remove-img">
                                                    <a href="javascript:;" title="Remove" class="remove-image" data-id="{{ $marchImg->id }}">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </span> --}}

                                            </div>
                                            {{-- @endif --}}
                                        </li>
                                        @endforeach
                                    </div>
                                    @endif                
                                    <div class="uplodpic gallery">
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <h5 class="fultxt">@lang('merchant_lang.OrdersNotificationEmail')</h5>
                                </div>
                                <hr>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="email1" class="col-form-label">
                                    @lang('merchant_lang.Email3')</label>
                                    <input id="email1" type="email" class="form-control" placeholder="Email 1" name="email1" value="{{ @Auth::guard('merchant')->user()->email1 }}" readonly="">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="email2" class="col-form-label">@lang('merchant_lang.Email2')</label>
                                    <input id="email2" type="email" class="form-control" placeholder="Email 2" name="email2" value="{{ @Auth::guard('merchant')->user()->email2 }}" readonly="">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="email3" class="col-form-label">@lang('merchant_lang.Email1')</label>
                                    <input id="email3" type="email" class="form-control" placeholder="Email 3" name="email3" value="{{ @Auth::guard('merchant')->user()->email3 }}" readonly="">
                                </div>
                                @foreach(@$language as $langKey => $lang)
                                <div class="form-group col-md-12">
                                    <label>@lang('merchant_lang.CompanyDescription') [{{ $lang->name }}] </label>
                                    <textarea class="form-control required" rows="3" name="description[{{ $lang->id }}][]" disabled="">@if(@$marchentDesc[$langKey]->language_id == $lang->id){{ strip_tags(@$marchentDesc[$langKey]->description) }}@endif</textarea>
                                </div>
                                @endforeach
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <h5 class="fultxt">@lang('merchant_lang.BankAccountInformation')</h5>
                                </div>
                                <hr>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="bank_name" class="col-form-label">@lang('merchant_lang.BankName')</label>
                                    <input id="bank_name" type="text" class="form-control" placeholder="Bank Name" name="bank_name" value="{{ @Auth::guard('merchant')->user()->bank_name }}" readonly="">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="account_name" class="col-form-label">@lang('merchant_lang.BankAccountName')</label>
                                    <input id="account_name" type="text" class="form-control" placeholder="Account Name" name="account_name" value="{{ @Auth::guard('merchant')->user()->account_name }}" readonly="">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="account_number" class="col-form-label">@lang('merchant_lang.BankAccountNumber')</label>
                                    <input id="account_number" type="text" class="form-control" placeholder="Account Number" name="account_number" value="{{ @Auth::guard('merchant')->user()->account_number }}" readonly="">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="iban_number" class="col-form-label">@lang('merchant_lang.IbanNumber')</label>
                                    <input id="iban_number" type="text" class="form-control" placeholder="IBAN Number" name="iban_number" value="{{ @Auth::guard('merchant')->user()->iban_number }}" readonly="">
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <h5 class="fultxt">@lang('merchant_lang.PaymentAcceptanceMode')</h5>
                                </div>
                                <hr>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="input-select" class="col-form-label">@lang('merchant_lang.PaymentMode')</label>
                                    <select class="form-control" id="input-select" name="payment_mode" disabled="">
                                        <option value="">Select @lang('merchant_lang.PaymentMode')</option>
                                        <option value="O" @if(@Auth::guard('merchant')->user()->payment_mode == "O") selected="" @endif>@lang('merchant_lang.only_online')</option>
                                        <option value="C" @if(@Auth::guard('merchant')->user()->payment_mode == "C") selected="" @endif>@lang('merchant_lang.only_cod')</option>
                                        <option value="A" @if(@Auth::guard('merchant')->user()->payment_mode == "A") selected="" @endif>@lang('merchant_lang.only_online_cod')</option>
                                    </select>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <h5 class="fultxt">@lang('merchant_lang.OpeningHours')</h5>
                                </div>
                                <hr>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    @php 
                                    $days = getDays();
                                    @endphp
                                    @foreach($days as $key => $value)
                                    <div class="day_div">
                                        <span class="day_name">
                                            <div class="form-group check_group logchk">
                                                @lang('merchant_lang.'.$value)
                                            </div>
                                        </span>
                                        <div class="tymm width-set">
                                            <input type="hidden" name="day[]" value="{{ $key+1 }}">
                                            <span class="file_div">
                                                <input type="text" class="form-control timepicker from_time required_remove" placeholder="From Time" name="from_time[]" disabled="" value="@if(@$time[$key]->from_time && @$time[$key]->days == $key+1)  {{ date('h:ia',strtotime(@$time[$key]->from_time)) }} @endif">
                                                <span class="from_time_error_{{ $key }} text-danger"></span>
                                            </span>
                                            <span class="file_div"><input type="text" class="form-control timepicker to_time" placeholder="To Time" name="to_time[]" disabled="" value="@if(@$time[$key]->to_time && @$time[$key]->days == $key+1)  {{ date('h:ia',strtotime(@$time[$key]->to_time)) }} @endif"><span class="to_time_error_{{ $key }} text-danger"></span></span>

                                            {{-- <a href="#"><img src="assets/images/swp.png"></a> --}}
                                        </div>
                                    </div>
                                    @endforeach

                                </div>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <h5 class="fultxt">@lang('merchant_lang.change_password')</h5>
                                </div>
                                <hr>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="password" class="col-form-label">@lang('merchant_lang.old_password')</label>
                                    <input id="password" type="password" class="form-control" placeholder="Old Password" name="password">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="new_password" class="col-form-label">@lang('merchant_lang.new_password')</label>
                                    <input id="new_password" type="password" class="form-control" placeholder="New Password" name="new_password">
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="confirm_password" class="col-form-label">@lang('merchant_lang.confirm_password')</label>
                                    <input id="confirm_password" type="password" class="form-control" placeholder="Confirm Password" name="confirm_password">
                                </div>
                            </div>
                            <hr>
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
    $(document).ready(function(){

        $('#password').keyup(function(event) {
            $('#new_password').addClass('required');
        });
        $("#edit_form" ).validate({
           rules:{ 
                    // account_number : {
                    //     digits : true
                    // },
                    // iban_number : {
                    //     digits : true
                    // },
                    new_password : {
                        minlength : 6,
                        maxlength: 15
                    },
                    confirm_password : {          
                        required: false,
                        equalTo : "#new_password"
                    }
                },
                errorPlacement: function (error , element) {
                //toastr:error(error.text());
            },
        });

        $('body').on('change','.required_remove',function(){    
            alert();
        });
        $("#edit_form").submit( function(e) {
            var counter = 0
            // $("input.from_time").each(function(){
            //     if ($(this).val() == '') {
            //         e.preventDefault();
            //         $($(this).next().html('This field is required'))
            //         counter++
            //     }else{
            //         $($(this).next().html(''))
            //     }
            //     console.log($(this).val())
            // });
            // $("input.to_time").each(function(){
            //     if ($(this).val() == '') {
            //         e.preventDefault();
            //         $($(this).next().html('This field is required'))
            //         counter++
            //     }else{
            //         $($(this).next().html(''))
            //     }
            //     console.log($(this).val())
            // });

            if(counter > 0) {
                return false;
            }
        });


        $('#email').blur(function(event) {

            if($('#email').val() != ''){
                // alert('hi');
                $("#chck_eml_rd").html('');
                $("#chck_eml_grn").html('');
                var $this = $(this);
                var email = $(this).val();
                var re =/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(!re.test(email)) {
                    $("#chck_eml_rd").html('Please enter a valid email address');
                    $('#email').val('');
                } else {
                    var reqData = {
                        '_token': '{{ @csrf_token() }}', 
                        'params': {
                            'email': email
                        }
                    };
                    if(email != '') {
                        $.ajax({
                            url: '{{ route('check.new.email') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: reqData,
                        })
                        .done(function(response) {
                            if(!!response.error) {
                                $("#chck_eml_rd").html(response.error.merchant);
                                $('#email').val('');
                            } else {
                                // $("#chck_eml_grn").html(response.result.message);
                            }
                        })
                        .fail(function(error) {
                            console.log(error);
                        });
                    }
                }     
            } else {
                $("#chck_eml_grn").html('');
            }  
        });

        $('input.timepicker').timepicker({
            change: function(time) {
                if ($(this).val() != '') {
                    $($(this).next().html(''))
                } else {
                    $($(this).next().html('This field is required'))
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    var new_html = '<li><img src="'+event.target.result+'"></li>';
                    $('.gallery').append(new_html);
                    // $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery');
        $('.gallery').html('');
    });
});

</script>

{{-- For show image --}}
<script>
    var reader = new FileReader();
    reader.onload = function (e) {
        $('#blah').attr('src', e.target.result);
        $('#blah').hide();
        $('#blah').fadeIn(500);
    }
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this);
    });
</script>

{{-- for fetching state  --}}
<script>
    $(document).ready(function(){

       $('#country').change(function(){
          if($(this).val() != '')
          {
             var value  = $(this).val();
             var _token = $('input[name="_token"]').val();
             $.ajax({
                url:"{{ route('state.fetch') }}",
                method:"POST",
                data:{value:value, _token:_token},
                success:function(result)
                {
                   $('#state').html(result);
               }

           })
         }
     });

    $('#country').change(function(){
        $('#state').val('');
    }); 
    $('#kuwait_city').keyup(function () {
        var city = $(this).val();
        // alert(city);
        if (city != '') {
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('admin.fetch.city.all') }}",
                method: "POST",
                data: {
                    city: city,
                    _token: _token
                },
                success: function (response) {
                    if (response.error) {
                        // alert("error");
                    } 
                    else
                    {
                        // alert("success");
                        var cityHtml = '<ul><li data-id="" class="kuwait_cities" data-nm="">Select City</li>';
                        response.result.cities.forEach(function (item, index) {
                            cityHtml = cityHtml + '<li class="kuwait_cities" data-id="' + item.city_details_by_language.city_id + '" data-nm="' + item.city_details_by_language.name + '">' + item.city_details_by_language.name + '</li>';
                        })
                        cityHtml = cityHtml + '</ul>';

                        $('#cityList').show();
                        $('#cityList').html(cityHtml);
                    }

                }
            });
        }
    });
    $(document).on("keyup", "#kuwait_city", function () {
        $("#k_cityname").val($.trim($("#kuwait_city").val()));
        var value = $.trim($(this).val());
        if (value == "") {
            $("#kuwait_city_value_id").val("");
        }
    });
    $(document).on("blur", "#kuwait_city", function () {
        var id = $.trim($("#kuwait_city_value_id").val());
        var name = $.trim($("#kuwait_city_value_name").val());
        var city = $.trim($("#kuwait_city").val());
        if (city != name) {
            $("#kuwait_city").val("");
            $("#kuwait_city_value_id").val("");
            $("#kuwait_city_value_name").val("");
            $(".city_id_err").text("@lang('admin_lang.please_select_name_city')");
        }else 
        {
            $(".city_id_err").text("");
        }
    });
    $('body').on('click', '.cityChange', function () {
        $('#kuwait_city').val($(this).text());
        $('#cityList').fadeOut();
    });
    $("body").click(function () {
        $("#cityList").fadeOut();
    });
    $(document).on("click", ".kuwait_cities", function (e) {
        var id = $(e.currentTarget).attr("data-id");
        $("#kuwait_city_value_id").val(id);
    });
    $(document).on("click", ".kuwait_cities", function (e) {
        var id = $(e.currentTarget).attr("data-id");
        var name = $(e.currentTarget).attr("data-nm");
        $("#kuwait_city_value_id").val(id);
        $("#kuwait_city_value_name").val(name);

        $("#kuwait_city").val(name);
        $(".city_id_err").text("");
        $("error_kuwait_city").text("");
    });
   });
</script>


{{-- for remove row  --}}
<script>

    $(document).ready(function(){
        $('body').on('click','.remove-image',function(){
            var var_id = $(this).data('id');
            var reqDataa = {
                jsonrpc: '2.0'
            };

            $.ajax({
                type:'GET',
                url : '{{ url('merchant/remove-image') }}/'+var_id,
                data:reqDataa,
                success:function(response){
                    if(response.sucess) {
                        // if(response.sucess.result == 'Add') {
                            $(".remove-rw-"+var_id).hide();
                        // }

                    } else {
                        $(".remove-rw-"+var_id).hide();
                        // console.log(".remove-rw-"+var_id);
                    }
                }
            });     

        });   

    });

</script>
@endsection