@extends('merchant.layouts.app')
{{-- @section('title', 'Alaaddin | Merchant | Create External Order') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.merchant') | @lang('admin_lang.create_external_order')
@endsection
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    .remove-img {
    width: 18px;
    height: 18px;
    font-size: 13px;
    color: #fff;
    text-align: center;
    background: #0771d4;
    border-radius: 50%;
    line-height: 16px;
    float: right;
    margin-left: 4px;
    }
    .remove-img a {
    color: #fff;
    font-size: 10px;
    }
    .action-opt {
    position: absolute;
    right: 0;
    top: 5px;
    z-index: 1;
    }
    label.error{
        border: none !important;
        background-color: #fff !important;
        color: #f00;
    }
</style>
<style type="text/css">
    label.error{
    border: none !important;
    background-color: #fff !important;
    color: #f00;
    }

    #cityList{
        position: absolute;
        width: 97%;
        /*height: 200px;*/
        z-index: 99;
    }
    #cityList ul{
        background: #fff;
        width: 97%;
        border: solid 1px #eee;
        padding: 0;
        max-height: 200px;
        overflow-y: scroll;
    }
    #cityList ul li{
        list-style: none;
        padding: 5px 15px;
        cursor: pointer;
        border-bottom: solid 1px #eee;
    }
    #cityList ul li:hover{
        background: #3a82c4;
        color: #fff;
    }

    #pickup_cityList{
        position: absolute;
        width: 97%;
        /*height: 200px;*/
        z-index: 99;
    }
    #pickup_cityList ul{
        background: #fff;
        width: 97%;
        border: solid 1px #eee;
        padding: 0;
        max-height: 200px;
        overflow-y: scroll;
    }
    #pickup_cityList ul li{
        list-style: none;
        padding: 5px 15px;
        cursor: pointer;
        border-bottom: solid 1px #eee;
    }
    #pickup_cityList ul li:hover{
        background: #3a82c4;
        color: #fff;
    }
    .ad1{
        float: left;
        border: 1px solid #d7d3d3;
        width: 24%;
        margin: 18px 1% 15px 16px;
        padding: 10px 15px;
    }
    .ad1 h5{
        margin: 0 0 10px 0;
        font-size: 18px;
    }
    .ad1Span{
        margin: 0 0 0 15px;
    }
    .ad1Span2{
        margin: 0 0 12px 35px;
    }
    .addNot{
        font-size: 19px;
        margin: 0 0 8px 0;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">
                            @lang('admin_lang.Dashboard')</a>
                        </li>
                        <li class="breadcrumb-item active">@lang('admin_lang.external_order_label')</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('admin_lang.external_order_label')</h4>
                    </div>
                </div>
                <div class="col-md-12">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session()->has('success'))
                    <div class="alert alert-success vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                    </div>
                    @elseif ((session()->has('error')))
                    <div class="alert alert-danger vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    @endif
                    <div class="block form-block mb-4">
                        <form id="addExternalOrderForm" method="POST" action="" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="from_address" id="from_address" value="T">
                            <input type="hidden" name="from_address_id" id="from_address_id" value="{{ @$default_address->id }}">
                            <input type="hidden" name="from_address_country_id" id="from_address_country_id" value="{{ @$default_address->country }}">
                            <input type="hidden" name="to_address" id="to_address" value="F">
                            <input type="hidden" name="to_address_id" id="to_address_id" value="">
                            <input type="hidden" name="to_address_country_id" id="to_address_country_id" value="">
                            <div class="form-row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <h5 class="fultxt addNot">@lang('merchant_lang.pickup_address')</h5>
                                    <div class="row" style="width: 100%">
                                        <span class="ad1Span">
                                            <input type="radio" name="radio1"  id="input_address1" > Input Address
                                        </span>
                                        <span class="ad1Span2">
                                            <input type="radio" name="radio1"  id="address_book1" checked> Choose from address book
                                        </span>
                                        <span class="ad1Span2">
                                            <label id="from_address_show_error" class="error" style="display:none"></label>
                                        </span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="showField1" style="display:none">
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="pickup_country" class="col-form-label">@lang('merchant_lang.pickup_country') (@lang('admin_lang.required'))</label>
                                        <select class="custom-select form-control required" id="pickup_country" name="pickup_country">
                                            <option value="">@lang('admin_lang.select_country')</option>
                                            @if(@$countries)
                                            @foreach(@$countries as $country)
                                            <option value="{{ @$country->id }}" @if(@$default_address->country) @if(@$default_address->country == @$country->id) selected @endif @elseif($country->id == 134) selected  @elseif($country->status == 'I') style="display:none;" @endif>{{ @$country->countryDetailsBylanguage->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label class="col-form-label">@lang('admin_lang.pickup_phone') (@lang('admin_lang.required'))</label>
                                        <input id="pickup_phone" name="pickup_phone" class="form-control required number" placeholder='@lang('admin_lang.pickup_phone')' type="tel"  onkeypress="return isNumber(event)" value="{{ @$default_address->phone }}"> 
                                    </div>
                                    @if(@$default_address)
                                        @if(@$default_address->city_id)
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 inside_kuwait_city1">
                                                <label for="pickup_kuwait_city_label" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                                
                                                <input type="text" class="form-control required custom-select" name="pickup_kuwait_city" id="pickup_kuwait_city" placeholder="@lang('admin_lang.city')" value="{{ @$default_address->city }}">
                                                <div id="pickup_cityList"></div>
                                                <span class="text-danger pickup_city_id_err"></span>
                                            </div>
                                        @else
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ">
                                                <label for="pickup_city" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                                <input id="pickup_city" name="pickup_city" type="text" class="form-control" placeholder='@lang('admin_lang.city')' value="{{ @$default_address->city }}" >
                                            </div>
                                        @endif
                                    @else
                                        <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 inside_kuwait_city1">
                                            <label for="pickup_kuwait_city_label" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                            
                                            <input type="text" class="form-control required custom-select" name="pickup_kuwait_city" id="pickup_kuwait_city" placeholder="@lang('admin_lang.city')" value="{{ @$default_address->city }}">
                                            <div id="pickup_cityList"></div>
                                            <span class="text-danger pickup_city_id_err"></span>
                                        </div>
                                    @endif
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 blockDiv">
                                        <label for="" class="col-form-label block_label1">@lang('admin_lang.block_required') </label>
                                        <input type="text" id="pickup_block" name="pickup_block" type="text" class="form-control required" placeholder='@lang('admin_lang.block')' value="{{ @$default_address->block }}"/>
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="" class="col-form-label">@lang('admin_lang.street') (@lang('admin_lang.required'))</label>
                                        <input id="pickup_street" name="pickup_street" type="text" class="form-control required" placeholder='@lang('admin_lang.street')' value="{{ @$default_address->street }}">
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 zipDiv1" >
                                    <label for="pickup_zip" class="col-form-label zip_label1">@lang('admin_lang.postal_code_optional')</label>
                                    <input id="pickup_zip" name="pickup_zip" type="number" class="form-control" placeholder='@lang('admin_lang.postal_code')' onkeypress="return isNumber(event)" value="{{ @$default_address->postal_code }}">
                                    </div>
                                    
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 buidingDiv">
                                        <label for="" class="col-form-label building_label1">@lang('admin_lang.build_required')</label>
                                        <input type="text" id="pickup_building" name="pickup_building" type="text" class="form-control required" placeholder='@lang('admin_lang.building')' value="{{ @$default_address->building_no }}"/>
                                    </div>
                                    
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label class="col-form-label">@lang('front_static.type_url') (@lang('front_static.optional'))</label>
                                        <input id="" type="text" placeholder="@lang('front_static.type_url')" name="pickup_location" class="form-control" value="{{ @$default_address->location }}">
                                        
                                        <input type="hidden" name="lat1" id="lat1" value="{{ @$default_address->lat }}">
                                        <input type="hidden" name="lng1" id="lng1" value="{{ @$default_address->lng }}">
                                    </div>

                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="" class="col-form-label">@lang('admin_lang.more_add_details_opt')</label>
                                        <input type="text" id="pickup_address" name="pickup_address" type="text" class="form-control" placeholder='@lang('admin_lang.more_address_details')' value="{{ @$default_address->more_address }}"/>
                                    </div>
                                </div>
                                <div class="row col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="showAddressBook1" >
                                    @if(@$address)
                                        @foreach($address as $i=> $add1)
                                            <div class="ad1">
                                                <h5 style="font-size: 17px;">Address {{ $i+1 }} <input type="radio" name="radio4" class="addressRadioFrom" value="{{ @$add1->id }}" data-country="{{ @$add1->country }}" @if(@$add1->is_default == 'Y') checked @endif></h5>
                                                <div>
                                                    {{ @$add1->countryDetailsBylanguage->name }},<br>
                                                    @if($add1->phone)
                                                        {{ @$add1->phone }},<br>
                                                    @endif
                                                    @if($add1->city)
                                                        {{ @$add1->city }}
                                                    @else
                                                        {{ @$add1->getCityNameByLanguage->name }}
                                                    @endif
                                                    @if(@$add1->street)
                                                        ,{{ @$add1->street }}<br>
                                                    @endif
                                                    @if(@$add1->building_no)
                                                        ,{{ @$add1->building_no }}
                                                    @endif
                                                    @if(@$add1->location)
                                                        ,{{ @$add1->location }}
                                                    @endif
                                                    @if(@$add1->more_address)
                                                        ,{{ @$add1->more_address }}
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <h5 class="fultxt addNot">@lang('admin_lang.shipping_address')</h5>
                                    <div class="row" style="width: 100%">
                                        <span class="ad1Span">
                                            <input type="radio" name="radio2"  id="input_address2" checked> Input Address
                                        </span>
                                        <span class="ad1Span2">
                                            <input type="radio" name="radio2"  id="address_book2" > Choose from address book
                                        </span>
                                        <span class="ad1Span2">
                                            <label id="to_address_show_error" class="error" style="display:none"></label>
                                        </span>
                                    </div>
                                </div>
                                <div id="showField2" class="row col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" >
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="shipping_country" class="col-form-label">@lang('merchant_lang.shipping_country') (@lang('admin_lang.required'))</label>
                                        <select class="custom-select form-control required" id="country" name="country">
                                            <option value="">@lang('admin_lang.select_country')</option>
                                            @if(@$countries)
                                            @foreach(@$countries as $country)
                                            <option value="{{ @$country->id }}" @if($country->id == 134) selected  @elseif($country->status == 'I') style="display:none;" @endif>{{ @$country->countryDetailsBylanguage->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 outside_kuwait_city">
                                        <label for="city" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                        <input id="city" name="city" type="text" class="form-control" placeholder='@lang('admin_lang.city')'>
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 inside_kuwait_city">
                                        <label for="kuwait_city_label" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                        
                                        <input type="text" class="form-control required custom-select" name="kuwait_city" id="kuwait_city" placeholder="@lang('admin_lang.city')" value="">
                                        <div id="cityList"></div>
                                        <span class="text-danger city_id_err"></span>
                                    </div>
                                    
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 blockDiv">
                                        <label for="" class="col-form-label block_label">@lang('admin_lang.block_required') </label>
                                        <input type="text" id="block" name="block" type="text" class="form-control required" placeholder='@lang('admin_lang.block')' />
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="" class="col-form-label">@lang('admin_lang.street') (@lang('admin_lang.required'))</label>
                                        <input id="street" name="street" type="text" class="form-control required" placeholder='@lang('admin_lang.street')'>
                                    </div>
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 zipDiv" >
                                        <label for="zip" class="col-form-label zip_label">@lang('admin_lang.postal_code_optional')</label>
                                        <input id="zip" name="zip" type="number" class="form-control" placeholder='@lang('admin_lang.postal_code')' onkeypress="return isNumber(event)">
                                    </div>
                                    
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 buidingDiv buidingDiv">
                                        <label for="" class="col-form-label building_label">@lang('admin_lang.build_required')</label>
                                        <input type="text" id="building" name="building" type="text" class="form-control required" placeholder='@lang('admin_lang.building')' />
                                    </div>
                                    
                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label class="col-form-label">@lang('front_static.type_url') (@lang('front_static.optional'))</label>
                                        <input id="" type="text" placeholder="@lang('front_static.type_url')" name="location" class="form-control">
                                        
                                        <input type="hidden" name="lat" id="lat2">
                                        <input type="hidden" name="lng" id="lng2">
                                    </div>

                                    <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                        <label for="" class="col-form-label">@lang('admin_lang.more_add_details_opt')</label>
                                        <input type="text" id="address" name="address" type="text" class="form-control" placeholder='@lang('admin_lang.more_address_details')' />
                                    </div>
                                </div>
                                <div class="row col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" id="showAddressBook2" style="display: none">
                                    @if(@$address)
                                        @foreach($address as $i=> $add1)
                                            <div class="ad1">
                                                <h5 style="font-size: 17px">Address {{ $i+1 }} <input type="radio" name="radio5" class="addressRadioTo" value="{{ @$add1->id }}" data-country="{{ @$add1->country }}"></h5>
                                                <div>
                                                    {{ @$add1->countryDetailsBylanguage->name }},<br>
                                                    @if($add1->phone)
                                                        {{ @$add1->phone }},<br>
                                                    @endif
                                                    @if($add1->city)
                                                        {{ @$add1->city }}
                                                    @else
                                                        {{ @$add1->getCityNameByLanguage->name }}
                                                    @endif
                                                    @if(@$add1->street)
                                                        ,{{ @$add1->street }}<br>
                                                    @endif
                                                    @if(@$add1->building_no)
                                                        ,{{ @$add1->building_no }}
                                                    @endif
                                                    @if(@$add1->location)
                                                        ,{{ @$add1->location }}
                                                    @endif
                                                    @if(@$add1->more_address)
                                                        ,{{ @$add1->more_address }}
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="country" class="col-form-label">@lang('admin_lang.delivery_date_required')</label>
                                    @php
                                    $todayDate = date("Y-m-d");
                                    @endphp
                                    <input type="text" id="datepicker" name="delivery_date" size="30" class="form-control from_date datepicker" placeholder="Date" value="{{ $todayDate }}" readonly="" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="country" class="col-form-label">@lang('admin_lang.delivery_time_required')</label>
                                    <div class="day_div">
                                        <div class="tymm width-set">
                                            <input type="hidden" name="day[]" value="">
                                            <span class="file_div">
                                                <input type="text" class="form-control timepicker from_time required_remove required" placeholder="@lang('admin_lang.delivery_time_required')" name="from_time" value="" data-value="" readonly>
                                                <span class="error_time_from from_time_error_1 text-danger error_from_1"></span>
                                            </span>
                                            <a href="#"><img src="{{ ('public/admin/assets/images/swp.png') }}"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label class="fname_label">@lang('admin_lang.first_name')  (@lang('admin_lang.optional')) </label>
                                    <input id="fname" name="fname" class="form-control" placeholder='@lang('admin_lang.first_name')' type="text">
                                </div>
                                <div class="form-group  col-md-4">
                                    <label class="lname_label">@lang('admin_lang.last_name') (@lang('admin_lang.optional'))</label>
                                    <input id="lname" name="lname" class="form-control" placeholder='@lang('admin_lang.last_name')' type="text">
                                </div>
                                <div class="form-group  col-md-4">
                                    <label>@lang('admin_lang.email') (@lang('admin_lang.optional'))</label>
                                    <input id="email" name="email" class="form-control" placeholder='@lang('admin_lang.email')' type="text">
                                </div>
                                <div class="form-group  col-md-4">
                                    <label>@lang('admin_lang.phone_required')</label>
                                    <input id="phone" name="phone" class="form-control required number" placeholder='@lang('admin_lang.phone_required')' type="tel"  onkeypress="return isNumber(event)"> 
                                </div>
                                <div class="form-group  col-md-4">
                                    <label id="order_totallabel">@lang('admin_lang.total_cost_required')</label>
                                    <input id="order_total" name="order_total" class="form-control required number" placeholder='@lang('admin_lang.total_cost_required')' type="text">
                                </div>
                                <div class="form-group  col-md-4">
                                    <label @if(Auth::guard('merchant')->user()->country == 134) class="product_weight_label" @endif>@lang('admin_lang.product_weight') (@lang('admin_lang.gms_label')) @if(Auth::guard('merchant')->user()->country != 134) (Required)  @endif @if(Auth::guard('merchant')->user()->country == 134) (@lang('admin_lang.optional'))  @endif</label>
                                    <input @if(Auth::guard('merchant')->user()->country == 134) id="product_weight" @endif name="product_weight" class="form-control @if(Auth::guard('merchant')->user()->country != 134) required @endif" placeholder='@lang('admin_lang.product_weight')' type="number"  onkeypress='validate(event)'>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label>@lang('admin_lang.invoice_number_optional')</label>
                                    <input id="invoice" name="invoice" class="form-control" placeholder='@lang('admin_lang.invoice_no')' type="text">
                                </div>
                                <div class="form-group  col-md-4">
                                    <label>@lang('admin_lang.invoice_details_optional')</label>
                                    <input id="invoice_details" name="invoice_details" class="form-control" placeholder='@lang('admin_lang.invoice_details')' type="text">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="country" >@lang('admin_lang.payment_way_required') </label>
                                    <select class="custom-select form-control required" id="payment_method" name="payment_method">
                                        <option value="">@lang('admin_lang.select_payment_way')</option>
                                        <option value="C">@lang('admin_lang.cod')</option>

                                        @if(@Auth::guard('merchant')->user()->payment_mode == 'O' || @Auth::guard('merchant')->user()->payment_mode == 'A')
                                        <option value="O">@lang('admin_lang.online')</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group  col-md-6 up-btns">
                                    <input type="file" class="custom-file-input inpt" name="image[]" id="gallery-photo-add" multiple="">
                                    <label class="custom-file-label extrlft" for="customFile">@lang('admin_lang_static.upload_image')</label>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="uplodpic">
                                    </div>
                                    <div class="uplodpic gallery">
                                    </div>
                                </div>
                                <div class="form-group  col-md-6">
                                </div>
                            </div>
                            <div class="form-row" style="margin-top: 10px">
                                <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <input type="hidden" id="k_cityname" name="k_cityname" value="">
                                    <input type="hidden" id="pickup_k_cityname" name="pickup_k_cityname" value="">
                                    <button class="btn btn-primary" id="create_external_order" type="submit">@lang('admin_lang.create_order')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        $('input.timepicker').timepicker();
        // $('.slct').chosen();
        $(".outside_kuwait_state").hide();
        $(".outside_kuwait_city").hide();
        $(".zipDiv").hide();
        $(".outside_kuwait_state1").hide();
        $(".outside_kuwait_city1").hide();
        $(".zipDiv1").hide();
        $(".datepicker").datepicker({dateFormat: "yy-mm-dd",
            defaultDate: new Date(),
            minDate: new Date(),
            changeMonth: true,
            changeYear: true,
            yearRange: '-100:+0'
        }); 
        // $( "#datepicker").datepicker();
        // $( "#anim" ).on( "change", function() {
        //   $( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
        // });
        
        // $(document).on('blur',".timepicker",function(e){

        //     var day = $.trim($(this).attr("data-value")),
        //     from_time = $.trim($("."+day).val()),
        //     to_time = $.trim($(this).val());
        //     if(from_time !="" && to_time != ""){
        //         var stt = new Date("November 13, 2013 " + from_time);
        //         stt = stt.getTime();

        //         var endt = new Date("November 13, 2013 " + to_time);
        //         endt = endt.getTime();

        //         //by this you can see time stamp value in console via firebug
        //         // console.log("Time1: "+ stt + " Time2: " + endt);

        //         if(stt > endt) {
        //             $(this).val("");
        //             $("."+day).val("");
        //             $(".error_to_"+day).text("From time isn't valid!");
        //             $(".error_from_"+day).text("To time isn't valid!");
        //         }
        //     }
            
        // });
        var imagesPreview = function(input, placeToInsertImagePreview) {
            
            if (input.files) {
                var filesAmount = input.files.length;
                
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    
                    reader.onload = function(event) {
                        var new_html = '<li><img src="'+event.target.result+'"></li>';
                        $('.gallery').append(new_html);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
            
        };
        
        $('#gallery-photo-add').on('change', function() {
            imagesPreview(this, 'div.gallery');
            $('.gallery').html('');
        });


        // Changes for address book start
        $('#address_book1').click(function(){
            $('#showAddressBook1').show();
            $('#showField1').hide();
            $('#from_address').val('T');
            var merchant_id = "{{ Auth::guard('merchant')->user()->id }}";
            var reqMerchantData = {
                "jsonrpc":"2.0",
                "_token":"{{ csrf_token() }}",
                "data"  :{
                    merchant_id:merchant_id
                }
            };
            $.ajax({
                url:"{{ route('merchant.get.merchant.address') }}",
                type:"POST",
                data:reqMerchantData,
                success:function(res){

                    var pickupAddress = '';
                    if(res.address != null){
                        $.each(res.address,function(i,v){
                            var count = i+1;
                            var is_default = '';
                            var city = '';
                            var street = '';
                            var building_no = '';
                            var location = '';
                            var more_address = '';

                            if(v.is_default == 'Y'){
                                is_default = 'checked';
                                $('#from_address_id').val(v.id);
                                $('#from_address_country_id').val(v.country);
                            }else{
                                is_default = '';
                            }

                            if(v.city != null){
                                city = v.city;
                            }else{
                                city = v.get_city_name_by_language.name;
                            }

                            if(v.street != null){
                                street = ','+v.street;
                            }else{
                                street = '';
                            }

                            if(v.building_no != null){
                                building_no = ','+v.building_no;
                            }else{
                                building_no = '';
                            }

                            if(v.location != null){
                                location = ','+v.location;
                            }else{
                                location = '';
                            }

                            if(v.more_address != null){
                                more_address = ','+v.more_address;
                            }else{
                                more_address = '';
                            }
                            if(v.phone != null){
                                phone = ','+v.phone+',<br>';
                            }else{
                                phone = '';
                            }
                            pickupAddress += '<div class="ad1">'+
                                '<h5 style="font-size: 17px;">Address '+ count +'&nbsp;<input type="radio" name="radio4" class="addressRadioFrom" value="'+ v.id +'" data-country="'+ v.country +'"'+is_default+' style="display:inline-block"></h5>'+
                                '<div>'+v.country_details_bylanguage.name+',<br>'+phone + city + street + '<br>'+
                                    building_no + location + '<br>'+ more_address +
                                '</div>'+
                            '</div>';
                        })
                        
                    }
                    $('#showAddressBook1').html(pickupAddress);
                }
            })
        })
        $('#address_book2').click(function(){
            $('#showAddressBook2').show();
            $('#showField2').hide();
            $('#to_address').val('T');
        })
        
        $('#input_address1').click(function(){
            $('#showAddressBook1').hide();
            $('#showField1').show();
            $('#from_address').val('F');
            $('#from_address_id').val('');
            $('#from_address_country_id').val('');
            $('#from_address_show_error').hide();
            $('#from_address_show_error').text("");
            $('.addressRadioFrom').prop('checked',false);
            var country = $('#pickup_country').val();
            var coun = '';
            if($('#to_address').val() == 'F'){
                coun = $('#country').val();
            }else{
                coun = $('#to_address_country_id').val();;
            }

            if(country == 134 && coun == 134){
                $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_optional')');
                $('#product_weight').removeClass('required');
                $('#product_weight').removeClass('error');
            }else{
                $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_required')');
                $('#product_weight').addClass('required');
                $('#product_weight_label').text('@lang('admin_lang.product_weight_gms_required')');
            }
        })

        $('#input_address2').click(function(){
            $('#showAddressBook2').hide();
            $('#showField2').show();
            $('#to_address').val('F');
            $('#to_address_id').val('');
            $('#to_address_country_id').val('');
            $('#to_address_show_error').hide();
            $('#to_address_show_error').text("");
            $('.addressRadioTo').prop('checked',false)

            var country = $('#pickup_country').val();
            var coun = '';
            if($('#from_address').val() == 'F'){
                coun = $('#country').val();
            }else{
                coun = $('#from_address_country_id').val();;
            }

            if(country == 134 && coun == 134){
                $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_optional')');
                $('#product_weight').removeClass('required');
                $('#product_weight').removeClass('error');
            }else{
                $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_required')');
                $('#product_weight').addClass('required');
                $('#product_weight_label').text('@lang('admin_lang.product_weight_gms_required')');
            }
        })

        $('body').on('click','.addressRadioFrom',function(e){
            var id = $(this).val();
            var country = $(this).attr('data-country');
            var to_address_id = $('#to_address_id').val();
            var coun = '';
            if($('#to_address').val() == 'F'){
                coun = $('#country').val();
            }else{
                coun = $('#to_address_country_id').val();;
            }

            if(country == 134 && coun == 134){
                $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_optional')');
                $('#product_weight').removeClass('required');
                $('#product_weight').removeClass('error');
            }else{
                $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_required')');
                $('#product_weight').addClass('required');
                $('#product_weight_label').text('@lang('admin_lang.product_weight_gms_required')');
            }
            $('#from_address_country_id').val(country);
            $('#from_address_id').val(id);
            if(id == to_address_id){
                $('#from_address_show_error').show();
                $('#from_address_show_error').text("@lang('merchant_lang.select_different_address')");
            }else{
                $('#from_address_show_error').hide();
                $('#from_address_show_error').text("");
            }

        })
        $('body').on('click','.addressRadioTo',function(){
            var id = $(this).val();
            var country = $(this).attr('data-country');
            var from_address_id = $('#from_address_id').val();

            if($('#from_address').val() == 'F'){
                coun = $('#country').val();
            }else{
                coun = $('#from_address_country_id').val();;
            }

            if(country == 134 && coun == 134){
                $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_optional')');
                $('#product_weight').removeClass('required');
                $('#product_weight').removeClass('error');
            }else{
                $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_required')');
                $('#product_weight').addClass('required');
                $('#product_weight_label').text('@lang('admin_lang.product_weight_gms_required')');
            }
            $('#to_address_country_id').val(country);
            $('#to_address_id').val(id);
            if(id == from_address_id){
                $('#to_address_show_error').show();
                $('#to_address_show_error').text("@lang('merchant_lang.select_different_address')");
            }else{
                $('#to_address_show_error').hide();
                $('#to_address_show_error').text("");
            }
        })

        $('#create_external_order').on('click',function(e){
            var from_address = $('#from_address').val();
            var from_address_id = $('#from_address_id').val();
            var to_address = $('#to_address').val();
            var to_address_id = $('#to_address_id').val();
            if(from_address == 'T' && from_address_id == ""){
                e.preventDefault();
                $('#from_address_show_error').show();
                $('#from_address_show_error').text("@lang('merchant_lang.select_an_address')");
            }else{
                $('#from_address_show_error').hide();
                $('#from_address_show_error').text("");
            }

            if(to_address == 'T' && to_address_id == ""){
                e.preventDefault();
                $('#to_address_show_error').show();
                $('#to_address_show_error').text("@lang('merchant_lang.select_an_address')");
            }else{
                $('#to_address_show_error').hide();
                $('#to_address_show_error').text("");
            }
            if(from_address == 'T' && to_address == 'T'){
                if(from_address_id == to_address_id){
                    e.preventDefault();
                    $('#to_address_show_error').show();
                    $('#to_address_show_error').text("@lang('merchant_lang.select_different_address')");
                }
            }
            
        })
        // changes for address book end


        $(document).on("change","#country",function(e){
            $(".removeText").text("");
            var country = $.trim($("#country").val());
            if(country == 134){ //if inside kuwait
                $(".outside_kuwait_state").hide();
                $(".outside_kuwait_city").hide();
                $(".inside_kuwait_state").show();
                $(".inside_kuwait_city").show();
                $(".zipDiv").hide();
            }else{
                $(".inside_kuwait_state").hide();
                $(".outside_kuwait_state").show();
                $(".inside_kuwait_city").hide();
                $(".outside_kuwait_city").show();
                $(".zipDiv").show();
            }
        });
        $(document).on("change","#pickup_country",function(e){
            $(".removeText").text("");
            var country = $.trim($("#pickup_country").val());
            if(country == 134){ //if inside kuwait
                $(".outside_kuwait_state1").hide();
                $(".outside_kuwait_city1").hide();
                $(".inside_kuwait_state1").show();
                $(".inside_kuwait_city1").show();
                $(".zipDiv1").hide();
            }else{
                $(".inside_kuwait_state1").hide();
                $(".outside_kuwait_state1").show();
                $(".inside_kuwait_city1").hide();
                $(".outside_kuwait_city1").show();
                $(".zipDiv1").show();
            }
        });
        $('#kuwait_city').change(function(event) {
            $('.city_id_err').html('');  
            
        });
        $('#pickup_kuwait_city').change(function(event) {
            $('.pickup_city_id_err').html('');  
        });
        $("#addExternalOrderForm").validate({
            messages: { 
                fname: { 
                    required: '@lang('validation.required')'
                },
                lname: { 
                    required: '@lang('validation.required')'
                },
                email: { 
                    required: '@lang('validation.required')',
                    email:'Please provide valid email'
                },
                phone: { 
                    required: '@lang('validation.required')',
                    digits: 'Please provide valid phone number'
                },
                order_total: { 
                    required: '@lang('validation.required')',
                    number:"@lang('admin_lang.provide_total_cost')"
                },
                street: { 
                    required: '@lang('validation.required')'
                },
                city: { 
                    required: '@lang('validation.required')'
                },
                kuwait_city: { 
                    required: '@lang('validation.required')'
                },
                zip: { 
                    required: '@lang('validation.required')'
                },
                country: { 
                    required: '@lang('validation.required')'
                },
                pickup_street: { 
                    required: '@lang('validation.required')'
                },
                pickup_city: { 
                    required: '@lang('validation.required')'
                },
                pickup_kuwait_city: { 
                    required: '@lang('validation.required')'
                },
                pickup_zip: { 
                    required: '@lang('validation.required')'
                },
                pickup_country: { 
                    required: '@lang('validation.required')'
                },
                delivery_date: { 
                    required: '@lang('validation.required')'
                },
                from_time: { 
                    required: '@lang('validation.required')'
                },
                payment_method: { 
                    required: '@lang('validation.required')'
                }
            }
        });
    
    });
    
    
    function validate(evt) {
    var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
    }
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    $(document).ready(function() {
        var country = $('#country').val();
        var sellerCountry = $('#pickup_country').val();
        // console.log(country)
        if(country == 134) {
            $('#phone').addClass('required')
        }

        if(country == 134 && sellerCountry == 134){
            //product_weight
            $('#product_weight').removeClass('required');
            $('#product_weight').removeClass('error');
        }else{
            $('#product_weight').addClass('required');
            $('#product_weight_label').text("");
        }

        // when change country
        $('#country').change(function(event) {
            country = $(this).val();
            $('#order_total').addClass('required');
            $('#phone').addClass('required');
            $('#street').addClass('required');
            $('#payment_method').addClass('required');

            // seller and buyer both are from kuwait
            if(country == 134) {
                //add text required
                
                $(".fname_label").html('@lang('admin_lang.fname_optional')');
                $(".lname_label").html('@lang('admin_lang.lname_optional')');
                $(".block_label").html('@lang('admin_lang.block_required')');
                $(".building_label").html('@lang('admin_lang.buildings_required')');
                $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_optional')');
                //add text optional

                // add required
                $('#order_total').addClass('required');
                
                $('#phone').addClass('required');
                $('#street').addClass('required');
                $('#payment_method').addClass('required');
                $('#kuwait_city').addClass('required');
                $('#street').addClass('required');
                $('#block').addClass('required');
                $('#building').addClass('required');
                
                
                // remove required
                $('#fname').removeClass('required');
                $('#fname').removeClass('error');

                $('#lname').removeClass('required');
                $('#lname').removeClass('error');

                $('#city').removeClass('required');
                $('#city').removeClass('error');

                $("#fname-error").text("");
                $("#lname-error").text("");
                $("#product_weight-error").text("");
                $("#street-error").text("");
                $(".zipDiv").hide();
                
            } 
            // out side kuwait
            else {
                $(".fname_label").html('@lang('admin_lang.first_name_required')');
                $(".lname_label").html('@lang('admin_lang.last_name_required')');
                $(".block_label").html('@lang('admin_lang.block_optional')');
                $(".building_label").html('@lang('admin_lang.building_optional')');
                $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_required')');
                // alert("fgdfgfdgf");
                //add text required
                
                //add text optional

                //add required
                // block-error
                $("#block-error").text("");
                $("#building-error").text("");

                $('#fname').addClass('required');
                $('#lname').addClass('required');
                $('#city').addClass('required');
                
                // $('#street').addClass('required');
                $('#product_weight').addClass('required');

                //remove required
                $('#block').removeClass('required');
                $('#block').removeClass('error');
                $('#building').removeClass('required');
                $('#building').removeClass('error');
                $('#kuwait_city').removeClass('required');
                $('#kuwait_city').removeClass('error');
                $(".zipDiv").show();
                //remove text

            }
            if(country == 134 && sellerCountry == 134){
                //product_weight
                $('#product_weight').removeClass('required');
                $('#product_weight').removeClass('error');
            }else{
                $('#product_weight').addClass('required');
                $('#product_weight_label').text('@lang('admin_lang.product_weight_gms_required')');
            }
        });

        $('#pickup_country').change(function(event) {
            country = $(this).val();
            $('#order_total').addClass('required');
            $('#phone').addClass('required');
            $('#pickup_street').addClass('required');
            $('#payment_method').addClass('required');

            // seller and buyer both are from kuwait
            if(country == 134) {
                //add text required
                
                $(".fname_label").html('@lang('admin_lang.fname_optional')');
                $(".lname_label").html('@lang('admin_lang.lname_optional')');
                $(".block_label1").html('@lang('admin_lang.block_required')');
                $(".building_label1").html('@lang('admin_lang.buildings_required')');
                $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_optional')');
                //add text optional

                // add required
                $('#order_total').addClass('required');
                
                $('#phone').addClass('required');
                $('#pickup_street').addClass('required');
                $('#payment_method').addClass('required');
                $('#pickup_kuwait_city').addClass('required');
                $('#pickup_street').addClass('required');
                $('#pickup_block').addClass('required');
                $('#pickup_building').addClass('required');
                
                
                // remove required
                $('#fname').removeClass('required');
                $('#fname').removeClass('error');

                $('#lname').removeClass('required');
                $('#lname').removeClass('error');

                $('#pickup_city').removeClass('required');
                $('#pickup_city').removeClass('error');

                $("#fname-error").text("");
                $("#lname-error").text("");
                $("#product_weight-error").text("");
                $("#pickup_street-error").text("");
                $(".zipDiv1").hide();
                
            } 
            // out side kuwait
            else {
                $(".fname_label").html("@lang('admin_lang.first_name_required')");
                $(".lname_label").html("@lang('admin_lang.last_name_required')");
                $(".block_label1").html("@lang('admin_lang.block_optional')");
                $(".building_label1").html("@lang('admin_lang.building_optional')");
                $(".product_weight_label").html("@lang('admin_lang.product_weight_gms_required')");
                // alert("fgdfgfdgf");
                //add text required
                
                //add text optional

                //add required
                // block-error
                $("#pickup_block-error").text("");
                $("#pickup_building-error").text("");

                $('#fname').addClass('required');
                $('#lname').addClass('required');
                $('#pickup_city').addClass('required');
                
                // $('#street').addClass('required');
                $('#product_weight').addClass('required');

                //remove required
                $('#pickup_block').removeClass('required');
                $('#pickup_block').removeClass('error');
                $('#pickup_building').removeClass('required');
                $('#pickup_building').removeClass('error');
                $('#pickup_kuwait_city').removeClass('required');
                $('#pickup_kuwait_city').removeClass('error');
                $(".zipDiv1").show();
                //remove text

            }
            if(country == 134 && sellerCountry == 134){
                //product_weight
                $('#product_weight').removeClass('required');
                $('#product_weight').removeClass('error');
            }else{
                $('#product_weight').addClass('required');
                $('#product_weight_label').text("@lang('admin_lang.product_weight_gms_required')");
            }
        });
        
    });

</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#kuwait_city').keyup(function(){ 
            var city = $(this).val();
            if(city != '')
            {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('merchant.external.fetch.city') }}",
                    method:"POST",
                    data:{city:city, _token:_token},
                    success:function(response){
                        // alert(JSON.stringify(response));
                        if(response.status == 'ERROR') {
                            $('#cityList').html('@lang('admin_lang.nothing_found')');
                        } else {
                        $('#cityList').fadeIn();  
                        $('#cityList').html(response.result);
                        }
                    }
                });
            }
        });
        $('#pickup_kuwait_city').keyup(function(){ 
            var city = $(this).val();
            if(city != '')
            {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('merchant.external.fetch.pickup.city') }}",
                    method:"POST",
                    data:{city:city, _token:_token},
                    success:function(response){
                        // alert(JSON.stringify(response));
                        if(response.status == 'ERROR') {
                            $('#pickup_cityList').html('@lang('admin_lang.nothing_found')');
                        } else {
                        $('#pickup_cityList').fadeIn();  
                        $('#pickup_cityList').html(response.result);
                        }
                    }
                });
            }
        });
    // $(document).on("blur","#kuwait_city",function(){
    //     $("#k_cityname").val($.trim($("#kuwait_city").val()));
    // });
    $(document).on("blur","#kuwait_city",function(){
        $("#k_cityname").val($.trim($("#kuwait_city").val()));
        var keyword = $.trim($("#kuwait_city_value_id").val());
        if(keyword == ""){
            $("#kuwait_city").val("");
            $(".kuwait_city-error").text('@lang('admin_lang.please_select_name_city')');
        }
    });
    $(document).on("blur","#pickup_kuwait_city",function(){
        $("#pickup_k_cityname").val($.trim($("#pickup_kuwait_city").val()));
        // var keyword = $.trim($("#pickup_kuwait_city_value_id").val());
        // if(keyword == ""){
        //     $("#pickup_kuwait_city").val("");
        //     $(".pickup_kuwait_city-error").text('@lang('admin_lang.please_select_name_city')');
        // }
    });
    // $(document).on("blur","#kuwait_city",function(){
    //     // var keyword = $.trim($("#kuwait_city").val());
    //     var city = $(this).val();
    //     if(city != '')
    //     {
    //        var _token = $('input[name="_token"]').val();
    //        $.ajax({
    //           url:"{{ route('merchant.external.fetch.city') }}",
    //           method:"POST",
    //           data:{city:city, _token:_token},
    //           success:function(response){
    //             // alert(JSON.stringify(response));
    //             if(response.status == 'ERROR') {
    //                 $('#cityList').html('nothing found');
    //                 $("#kuwait_city").val("");
    //             } else {
    //              $('#cityList').fadeIn();  
    //              $('#cityList').html(response.result);
    //             }
    //          }
    //      });
    //    }
    // });
    $('body').on('click', '.cityChange', function(){  
        $('#kuwait_city').val($(this).text());  
        $('#cityList').fadeOut();  
    });  
    $("body").click(function(){
        $("#cityList").fadeOut();
        $("#pickup_cityList").fadeOut();
    });
    $('body').on('click', '.pickUpCityChange', function(){  
        $('#pickup_kuwait_city').val($(this).text());  
        $('#pickup_cityList').fadeOut();  
    });  
    // $("#kuwait_city").click(function(){
    //     var city = $(this).val();
    //     var _token = $('input[name="_token"]').val();
    //        $.ajax({
    //           url:"{{ route('merchant.external.fetch.city') }}",
    //           method:"POST",
    //           data:{city:city, _token:_token},
    //           success:function(response){
    //             if(response.status == 'ERROR') {
    //                 $('#cityList').html('@lang('admin_lang.nothing_found')');
    //                 // $("#kuwait_city_value_id").val("");
    //             } else {
    //              $('#cityList').fadeIn();  
    //              $('#cityList').html(response.result);
    //              // $("#kuwait_city_value_id").val($.trim($(this).val());
    //             }
    //          }
    //      });
    // });
    $(document).on("click",".kuwait_cities",function(e){
        var id = $(e.currentTarget).attr("data-id");
        $("#kuwait_city_value_id").val(id);
    });
    $(document).on("click",".pickup_kuwait_cities",function(e){
        var id = $(e.currentTarget).attr("data-id");
        $("#pickup_kuwait_city_value_id").val(id);
    });
    });
</script>

<script>
function initMap1() {
    var input = document.getElementById('pac-input1');
    var autocomplete = new google.maps.places.Autocomplete(input);
    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    autocomplete.setTypes(['address']);

    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
      $('#lat1').val(place.geometry.location.lat())
      $('#lng1').val(place.geometry.location.lng())
    });
}
function initMap2() {
    var input = document.getElementById('pac-input2');
    var autocomplete = new google.maps.places.Autocomplete(input);
    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    autocomplete.setTypes(['address']);

    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
      $('#lat2').val(place.geometry.location.lat())
      $('#lng2').val(place.geometry.location.lng())
    });
}
$(document).ready(function() {
    $('#pac-input1').blur(function() {
        if($(this).val() == '') {
            $('#lat1').val('')
            $('#lng1').val('')
        }
    })
    $('#pac-input2').blur(function() {
        if($(this).val() == '') {
            $('#lat2').val('')
            $('#lng2').val('')
        }
    })
})
</script>

<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API') }}&libraries=places&callback=initMap1" async defer></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API') }}&libraries=places&callback=initMap2" async defer></script>
@endsection