    <option value="">Select city</option>
    @if(@$cities)
        @foreach(@$cities as $city)
            <option value="{{ @$city->id }}">{{ @$city->name }}</option>
        @endforeach
    @endif
