@extends('merchant.layouts.app')
{{-- @section('title', 'Alaaddin | Merchant | Create External Order') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.merchant') | @lang('admin_lang.create_external_order')
@endsection
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    .remove-img {
    width: 18px;
    height: 18px;
    font-size: 13px;
    color: #fff;
    text-align: center;
    background: #0771d4;
    border-radius: 50%;
    line-height: 16px;
    float: right;
    margin-left: 4px;
    }
    .remove-img a {
    color: #fff;
    font-size: 10px;
    }
    .action-opt {
    position: absolute;
    right: 0;
    top: 5px;
    z-index: 1;
    }
    label.error{
        border: none !important;
        background-color: #fff !important;
        color: #f00;
    }
</style>
<style type="text/css">
    label.error{
    border: none !important;
    background-color: #fff !important;
    color: #f00;
    }

    #cityList{
        position: absolute;
        width: 97%;
        /*height: 200px;*/
        z-index: 99;
    }
    #cityList ul{
        background: #fff;
        width: 97%;
        border: solid 1px #eee;
        padding: 0;
        max-height: 200px;
        overflow-y: scroll;
    }
    #cityList ul li{
        list-style: none;
        padding: 5px 15px;
        cursor: pointer;
        border-bottom: solid 1px #eee;
    }
    #cityList ul li:hover{
        background: #3a82c4;
        color: #fff;
    }
    .page-container {
        padding-left: 0 !important;
    }
</style>
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="text-align: center">
            <a href="javascript:;"><span class="logo"><img src="{{ asset('public/admin/assets/images/logo.png') }}"></span></a>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('admin_lang.external_order_label')</h4>
                    </div>
                </div>
                <div class="col-md-12">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (session()->has('success'))
                    <div class="alert alert-success vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                        <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                    </div>
                    @elseif ((session()->has('error')))
                    <div class="alert alert-danger vd_hidden" style="display: block;">
                        <a class="close" data-dismiss="alert" aria-hidden="true">
                        <i class="icon-cross"></i>
                        </a>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                        <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
                    </div>
                    @endif
                    <div class="block form-block mb-4">
                        <form id="addExternalOrderForm" method="POST" action="" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="country" class="col-form-label">@lang('admin_lang.country') (@lang('admin_lang.required'))</label>
                                    <select class="custom-select form-control required" id="country" name="country">
                                        <option value="">@lang('admin_lang.select_country')</option>
                                        @if(@$countries)
                                        @foreach(@$countries as $country)
                                        <option value="{{ @$country->id }}" @if($country->id == 134) selected  @elseif($country->status == 'I') style="display:none;" @endif>{{ @$country->countryDetailsBylanguage->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="country" class="col-form-label">@lang('merchant_lang.prefered_delivery_date')(@lang('admin_lang.required'))</label>
                                    @php
                                    $todayDate = date("Y-m-d");
                                    @endphp
                                    <input type="text" id="datepicker" name="delivery_date" size="30" class="form-control from_date datepicker" placeholder="Date" value="{{ $todayDate }}" readonly="" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="country" class="col-form-label">@lang('merchant_lang.prefered_delivery_time')(@lang('admin_lang.required'))</label>
                                    <div class="day_div">
                                        <div class="tymm width-set">
                                            <span class="file_div">
                                                <select class="form-control required" name="from_time" id="from_time">
                                                    <option value="">@lang('merchant_lang.select_preferred_delivery_time')</option>
                                                    @if(@$timeslot)
                                                        @foreach($timeslot as $v)
                                                            <option value="{{ @$v->id }}"> {{ date('h:i a',strtotime(@$v->from_time)) }} - {{ date('h:i a',strtotime(@$v->to_time)) }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <span class="error_time_from from_time_error_1 text-danger error_from_1"></span>
                                            </span>
                                            <a href="#"><img src="{{ ('public/admin/assets/images/swp.png') }}"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label class="fname_label">@lang('admin_lang.first_name')  (@lang('admin_lang.optional')) </label>
                                    <input id="fname" name="fname" class="form-control" placeholder='@lang('admin_lang.first_name')' type="text">
                                </div>
                                <div class="form-group  col-md-4">
                                    <label>@lang('admin_lang.phone_required')</label>
                                    <input id="phone" name="phone" class="form-control required number" placeholder='@lang('admin_lang.phone_required')' type="tel"  onkeypress="return isNumber(event)"> 
                                </div>
                                <div class="form-group  col-md-4">
                                    <label id="order_totallabel">@lang('merchant_lang.total_unpaid_payment')(@lang('admin_lang.required'))</label>
                                    <input id="order_total" name="order_total" class="form-control required number" placeholder='@lang('merchant_lang.total_unpaid_payment')' type="text">
                                </div>
                                {{-- <div class="form-group  col-md-4">
                                    <label @if($merchant->country == 134) class="product_weight_label" @endif>@lang('admin_lang.product_weight') (@lang('admin_lang.gms_label')) @if($merchant->country != 134) (Required)  @endif @if($merchant->country == 134) (@lang('admin_lang.optional'))  @endif</label>
                                    <input @if($merchant->country == 134) id="product_weight" @endif name="product_weight" class="form-control @if($merchant->country != 134) required @endif" placeholder='@lang('admin_lang.product_weight')' type="number"  onkeypress='validate(event)'>
                                </div> --}}
                            </div>
                            <div class="form-row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <h5 class="fultxt">@lang('admin_lang.shipping_address')</h5>
                                </div>
                                <hr>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 outside_kuwait_city">
                                    <label for="city" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                    <input id="city" name="city" type="text" class="form-control" placeholder='@lang('admin_lang.city')'>
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 inside_kuwait_city">
                                    <label for="kuwait_city_label" class="col-form-label">@lang('admin_lang.city') (@lang('admin_lang.required'))</label>
                                    
                                    <input type="text" class="form-control required custom-select" name="kuwait_city" id="kuwait_city" placeholder="@lang('admin_lang.city')" value="">
                                    <div id="cityList"></div>
                                    <span class="text-danger city_id_err"></span>
                                </div>
                                
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 blockDiv">
                                    <label for="" class="col-form-label block_label">@lang('admin_lang.block_required') </label>
                                    <input type="text" id="block" name="block" type="text" class="form-control required" placeholder='@lang('admin_lang.block')' />
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="" class="col-form-label">@lang('admin_lang.street') (@lang('admin_lang.required'))</label>
                                    <input id="street" name="street" type="text" class="form-control required" placeholder='@lang('admin_lang.street')'>
                                </div>
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 zipDiv" >
                                <label for="zip" class="col-form-label zip_label">@lang('admin_lang.postal_code_optional')</label>
                                <input id="zip" name="zip" type="number" class="form-control" placeholder='@lang('admin_lang.postal_code')' onkeypress="return isNumber(event)">
                                </div>
                                
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 buidingDiv ">
                                    <label for="" class="col-form-label building_label">@lang('admin_lang.build_required')</label>
                                    <input type="text" id="building" name="building" type="text" class="form-control required" placeholder='@lang('admin_lang.building')' />
                                </div>
                                
                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label class="col-form-label">@lang('front_static.type_url') (@lang('front_static.optional'))</label>
                                    <input id="" type="text" placeholder="@lang('front_static.type_url')" name="location" class="form-control">
                                    
                                    <input type="hidden" name="lat" id="lat">
                                    <input type="hidden" name="lng" id="lng">
                                </div>

                                <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="" class="col-form-label">@lang('admin_lang.more_add_details_opt')</label>
                                    <input type="text" id="address" name="address" type="text" class="form-control" placeholder='@lang('admin_lang.more_address_details')' />
                                </div>
                                {{-- <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                    <label for="country" class="col-form-label">@lang('admin_lang.payment_way_required') </label>
                                    <select class="custom-select form-control required" id="payment_method" name="payment_method">
                                        <option value="">@lang('admin_lang.select_payment_way')</option>
                                        <option value="C">@lang('admin_lang.cod')</option>
        
                                        @if(@Auth::guard('merchant')->user()->payment_mode == 'O' || @Auth::guard('merchant')->user()->payment_mode == 'A')
                                        <option value="O">@lang('admin_lang.online')</option>
                                        @endif
                                    </select>
                                </div> --}}
                            </div>
                    <div class="form-row">
                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <input type="hidden" id="k_cityname" name="k_cityname" value="">
                            <button class="btn btn-primary" id="create_external_order" type="submit">@lang('admin_lang.create_order')</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        $('input.timepicker').timepicker();
        // $('.slct').chosen();
        $(".outside_kuwait_state").hide();
        $(".outside_kuwait_city").hide();
        $(".zipDiv").hide();
        $(".datepicker").datepicker({dateFormat: "yy-mm-dd",
            defaultDate: new Date(),
            minDate: new Date(),
            changeMonth: true,
            changeYear: true,
            yearRange: '-100:+0'
        }); 
        // $( "#datepicker").datepicker();
        // $( "#anim" ).on( "change", function() {
        //   $( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
        // });
        
        // $(document).on('blur',".timepicker",function(e){

        //     var day = $.trim($(this).attr("data-value")),
        //     from_time = $.trim($("."+day).val()),
        //     to_time = $.trim($(this).val());
        //     if(from_time !="" && to_time != ""){
        //         var stt = new Date("November 13, 2013 " + from_time);
        //         stt = stt.getTime();

        //         var endt = new Date("November 13, 2013 " + to_time);
        //         endt = endt.getTime();

        //         //by this you can see time stamp value in console via firebug
        //         // console.log("Time1: "+ stt + " Time2: " + endt);

        //         if(stt > endt) {
        //             $(this).val("");
        //             $("."+day).val("");
        //             $(".error_to_"+day).text("From time isn't valid!");
        //             $(".error_from_"+day).text("To time isn't valid!");
        //         }
        //     }
            
        // });
        var imagesPreview = function(input, placeToInsertImagePreview) {
            
            if (input.files) {
                var filesAmount = input.files.length;
                
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    
                    reader.onload = function(event) {
                        var new_html = '<li><img src="'+event.target.result+'"></li>';
                        $('.gallery').append(new_html);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
            
        };
        
        $('#gallery-photo-add').on('change', function() {
            imagesPreview(this, 'div.gallery');
            $('.gallery').html('');
        });
        
        
        $(document).on("change","#country",function(e){
            $(".removeText").text("");
            var country = $.trim($("#country").val());
            if(country == 134){ //if inside kuwait
                $(".outside_kuwait_state").hide();
                $(".outside_kuwait_city").hide();
                $(".inside_kuwait_state").show();
                $(".inside_kuwait_city").show();
                $(".zipDiv").hide();
            }else{
                $(".inside_kuwait_state").hide();
                $(".outside_kuwait_state").show();
                $(".inside_kuwait_city").hide();
                $(".outside_kuwait_city").show();
                $(".zipDiv").show();
            }
        });
        $('#kuwait_city').change(function(event) {
            // var cn = $.trim($('#country').val());
            // if(cn == 134){
            //     if($('#kuwait_city').val() == '') {
            //             $('.city_id_err').html('Please select a city');
            //     }
            // }else{
                $('.city_id_err').html('');    
            // }
            
        });
        $("#addExternalOrderForm").validate({
            messages: { 
                fname: { 
                    required: '@lang('validation.required')'
                },
                lname: { 
                    required: '@lang('validation.required')'
                },
                email: { 
                    required: '@lang('validation.required')',
                    email:'Please provide valid email'
                },
                phone: { 
                    required: '@lang('validation.required')',
                    digits: 'Please provide valid phone number'
                },
                order_total: { 
                    required: '@lang('validation.required')',
                    number:"@lang('admin_lang.provide_total_cost')"
                },
                street: { 
                    required: '@lang('validation.required')'
                },
                city: { 
                    required: '@lang('validation.required')'
                },
                kuwait_city: { 
                    required: '@lang('validation.required')'
                },
                zip: { 
                    required: '@lang('validation.required')'
                },
                country: { 
                    required: '@lang('validation.required')'
                },
                delivery_date: { 
                    required: '@lang('validation.required')'
                },
                from_time: { 
                    required: '@lang('validation.required')'
                },
                payment_method: { 
                    required: '@lang('validation.required')'
                }
            }
            // ,
            // submitHandler: function (form) {
            //     var cn = $('#country').val();

            //     if(cn == 134) {
            //         if($('#city_id').val()) {
            //             var _token = $('input[name="_token"]').val();
            //             $.ajax({
            //                 url:"{{ route('merchant.exist.city.check') }}",
            //                 method:"POST",
            //                 data:{city:$('#city_id').val(), _token:_token},
            //                 dataType:'json',
            //                 success:function(response){
            //                     // alert("kjgkjdfgkjdf");
            //                     if(response.status == 'ERROR') {
            //                         $('.city_id_err').html('Please select a city from list.');
            //                         return false;
            //                     } else {
            //                         $('.city_id_err').html('');
            //                         form.submit();
            //                     }
            //                 }
            //             });
            //         }else{
            //             console.log('second else');    
            //         }
            //     } else {
            //         form.submit();
            //     }
            // }
        });

        
        // });
    
    });
    
    
    function validate(evt) {
    var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
    }
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    $(document).ready(function() {
        var country = $('#country').val();
        var sellerCountry = '{{ @$merchant->country }}'
        // console.log(country)
        if(country == 134) {
            $('#phone').addClass('required')
        }

        if(country == 134 && sellerCountry == 134){
            //product_weight
            $('#product_weight').removeClass('required');
            $('#product_weight').removeClass('error');
        }else{
            $('#product_weight').addClass('required');
            $('#product_weight_label').text("");
        }

        // when change country
        $('#country').change(function(event) {
            country = $(this).val();
            $('#order_total').addClass('required');
            $('#phone').addClass('required');
            $('#street').addClass('required');
            $('#payment_method').addClass('required');

            // seller and buyer both are from kuwait
            if(country == 134) {
                //add text required
                
                $(".fname_label").html('@lang('admin_lang.fname_optional')');
                $(".lname_label").html('@lang('admin_lang.lname_optional')');
                $(".block_label").html('@lang('admin_lang.block_required')');
                $(".building_label").html('@lang('admin_lang.buildings_required')');
                $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_optional')');
                //add text optional

                // add required
                $('#order_total').addClass('required');
                
                $('#phone').addClass('required');
                $('#street').addClass('required');
                $('#payment_method').addClass('required');
                $('#kuwait_city').addClass('required');
                $('#street').addClass('required');
                $('#block').addClass('required');
                $('#building').addClass('required');
                
                
                // remove required
                $('#fname').removeClass('required');
                $('#fname').removeClass('error');

                $('#lname').removeClass('required');
                $('#lname').removeClass('error');

                $('#city').removeClass('required');
                $('#city').removeClass('error');

                $("#fname-error").text("");
                $("#lname-error").text("");
                $("#product_weight-error").text("");
                $("#street-error").text("");
                $(".zipDiv").hide();
                
            } 
            // out side kuwait
            else {
                $(".fname_label").html('@lang('admin_lang.first_name_required')');
                $(".lname_label").html('@lang('admin_lang.last_name_required')');
                $(".block_label").html('@lang('admin_lang.block_optional')');
                $(".building_label").html('@lang('admin_lang.building_optional')');
                $(".product_weight_label").html('@lang('admin_lang.product_weight_gms_required')');
                // alert("fgdfgfdgf");
                //add text required
                
                //add text optional

                //add required
                // block-error
                $("#block-error").text("");
                $("#building-error").text("");

                $('#fname').addClass('required');
                $('#lname').addClass('required');
                $('#city').addClass('required');
                
                // $('#street').addClass('required');
                $('#product_weight').addClass('required');

                //remove required
                $('#block').removeClass('required');
                $('#block').removeClass('error');
                $('#building').removeClass('required');
                $('#building').removeClass('error');
                $('#kuwait_city').removeClass('required');
                $('#kuwait_city').removeClass('error');
                $(".zipDiv").show();
                //remove text

            }
            if(country == 134 && sellerCountry == 134){
                //product_weight
                $('#product_weight').removeClass('required');
                $('#product_weight').removeClass('error');
            }else{
                $('#product_weight').addClass('required');
                $('#product_weight_label').text('@lang('admin_lang.product_weight_gms_required')');
            }
        });
        
    });

</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#kuwait_city').keyup(function(){ 
        var city = $(this).val();
        if(city != '')
        {
           var _token = $('input[name="_token"]').val();
           $.ajax({
              url:"{{ route('merchant.external.fetch.city') }}",
              method:"POST",
              data:{city:city, _token:_token},
              success:function(response){
                // alert(JSON.stringify(response));
                if(response.status == 'ERROR') {
                    $('#cityList').html('@lang('admin_lang.nothing_found')');
                } else {
                 $('#cityList').fadeIn();  
                 $('#cityList').html(response.result);
                }
             }
         });
       }
   });
    // $(document).on("blur","#kuwait_city",function(){
    //     $("#k_cityname").val($.trim($("#kuwait_city").val()));
    // });
    $(document).on("blur","#kuwait_city",function(){
        $("#k_cityname").val($.trim($("#kuwait_city").val()));
        var keyword = $.trim($("#kuwait_city_value_id").val());
        if(keyword == ""){
            $("#kuwait_city").val("");
            $(".kuwait_city-error").text('@lang('admin_lang.please_select_name_city')');
        }
    });
    // $(document).on("blur","#kuwait_city",function(){
    //     // var keyword = $.trim($("#kuwait_city").val());
    //     var city = $(this).val();
    //     if(city != '')
    //     {
    //        var _token = $('input[name="_token"]').val();
    //        $.ajax({
    //           url:"{{ route('merchant.external.fetch.city') }}",
    //           method:"POST",
    //           data:{city:city, _token:_token},
    //           success:function(response){
    //             // alert(JSON.stringify(response));
    //             if(response.status == 'ERROR') {
    //                 $('#cityList').html('nothing found');
    //                 $("#kuwait_city").val("");
    //             } else {
    //              $('#cityList').fadeIn();  
    //              $('#cityList').html(response.result);
    //             }
    //          }
    //      });
    //    }
    // });
    $('body').on('click', '.cityChange', function(){  
        $('#kuwait_city').val($(this).text());  
        $('#cityList').fadeOut();  
    });  
    $("body").click(function(){
        $("#cityList").fadeOut();
    });
    $("#kuwait_city").click(function(){
        var city = $(this).val();
        var _token = $('input[name="_token"]').val();
           $.ajax({
              url:"{{ route('merchant.external.fetch.city') }}",
              method:"POST",
              data:{city:city, _token:_token},
              success:function(response){
                if(response.status == 'ERROR') {
                    $('#cityList').html('@lang('admin_lang.nothing_found')');
                    // $("#kuwait_city_value_id").val("");
                } else {
                 $('#cityList').fadeIn();  
                 $('#cityList').html(response.result);
                 // $("#kuwait_city_value_id").val($.trim($(this).val());
                }
             }
         });
    });
    $(document).on("click",".kuwait_cities",function(e){
        var id = $(e.currentTarget).attr("data-id");
        $("#kuwait_city_value_id").val(id);
    });
    });
</script>

<script>
function initMap() {
    var input = document.getElementById('pac-input');
    var autocomplete = new google.maps.places.Autocomplete(input);
    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    autocomplete.setTypes(['address']);

    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
      $('#lat').val(place.geometry.location.lat())
      $('#lng').val(place.geometry.location.lng())
    });
}
$(document).ready(function() {
    $('#pac-input').blur(function() {
        if($(this).val() == '') {
            $('#lat').val('')
            $('#lng').val('')
        }
    })
})
</script>

<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API') }}&libraries=places&callback=initMap" async defer></script>
@endsection