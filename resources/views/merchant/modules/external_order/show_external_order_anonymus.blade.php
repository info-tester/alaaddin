@extends('merchant.layouts.app')
{{-- @section('title', 'Alaaddin | Merchant | View Order Details') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.merchant') | @lang('admin_lang.view_order_details')
@endsection
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ URL::to('public/merchant/assets/css/chosen.css') }}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    .remove-img {
        width: 18px;
        height: 18px;
        font-size: 13px;
        color: #fff;
        text-align: center;
        background: #0771d4;
        border-radius: 50%;
        line-height: 16px;
        float: right;
        margin-left: 4px;
    }
    .remove-img a {
        color: #fff;
        font-size: 10px;
    }
    .action-opt {
        position: absolute;
        right: 0;
        top: 5px;
        z-index: 1;
    }

    .page-container {
        padding-left: 0 !important;
    }
</style>
@endsection

@section('content')
<div class="page-content-wrapper">
        <!--Header Fixed-->
        <div class="header fixed-header">
            <div class="container-fluid" style="padding: 10px 25px">
                <div class="row">
                    <div class="container-fluid" style="text-align: center">
                        <a href="javascript:;"><span class="logo"><img src="{{ asset('public/admin/assets/images/logo.png') }}"></span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="content sm-gutter">
            <div class="container-fluid padding-25 sm-padding-10" >
                <div class="row">
                    <div class="col-12">
                        <div class="section-title" style="margin-top:20px">
                            <h4>@lang('admin_lang.view_order_details')</h4>
                            <a class="extrnl btn btn-primary" href="{{ route('merchant.create.anonymous.external.order', ['slug' =>$merchant->slug,'token' => md5($merchant->id)]) }}">@lang('merchant_lang.new_order')</a>
                        </div>
                    </div>

                   <div class="col-md-12">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        @if (session()->has('success'))
                        <div class="alert alert-success vd_hidden" style="display: block;">
                            <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                            </a>
                            <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                            <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }} 
                        </div>
                        @elseif ((session()->has('error')))
                        <div class="alert alert-danger vd_hidden" style="display: block;">
                            <a class="close" data-dismiss="alert" aria-hidden="true">
                            <i class="icon-cross"></i>
                            </a>
                            <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                            <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }} 
                        </div>
                        @endif
                        <div class="block form-block mb-4">
                            <form action="#">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.order_details')

                                        </h4>
                                        <p><h5><span> @lang('admin_lang.order_no') </span> <span ><strong>: </strong>
                                            {{ @$order->order_no }}
                                        </span> </h5></p>
                                        <br>
                                        {{-- no of item --}}
                                        <p><span class="titel-span"> @lang('admin_lang.no_of_item') </span> <span class="deta-span"><strong>: </strong>
                                            {{ @$total_no_item }}
                                        </span> </p>

                                        <p><span class="titel-span">@lang('admin_lang.order_date')</span> <span class="deta-span"><strong>:</strong>{{ @$order->created_at }}</span></p>
                                        {{-- delivery date & time --}}
                                        @if(@$order->order_type == 'E')
                                        <p><span class="titel-span">@lang('merchant_lang.prefered_delivery_date')</span> <span class="deta-span"><strong>:</strong>
                                           {{ @$order->preferred_delivery_date }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('merchant_lang.prefered_delivery_time')</span> <span class="deta-span"><strong>:</strong>
                                            {{ date('h:i a',strtotime(@$order->getPreferredTime->from_time)) }} - {{ date('h:i a',strtotime(@$order->getPreferredTime->to_time)) }}
                                        </span>
                                        </p>
                                        @endif
                                        <br>
                                        <p><span class="titel-span">Order Subtotal</span> <span class="deta-span"><strong>:</strong>{{ @$ord_sel->subtotal }} @lang('admin_lang.kwd')</span></p>

                                        <p><span class="titel-span">@lang('admin_lang.shipping_kwd')</span> <span class="deta-span"><strong>:</strong>{{ @$order->shipping_price }} @lang('admin_lang.kwd')</span></p>

                                        <p><span class="titel-span">Total Product Discount </span> <span class="deta-span"><strong>:</strong>{{ @$ord_sel->total_discount }} @lang('admin_lang.kwd')</span></p>

                                        <p><span class="titel-span">Order total</span> <span class="deta-span"><strong>:</strong>{{ @$ord_total }} @lang('admin_lang.kwd')</span></p>
                                        </span></p>
                                        {{-- order type --}}
                                        <p><span class="titel-span">@lang('admin_lang.order_type')</span> <span class="deta-span"><strong>:</strong>
                                        @if(@$order->order_type == 'I')
                                        @lang('admin_lang.internal_1')
                                        @else
                                        @lang('admin_lang.external_1')
                                        @endif
                                        </span></p>
                                        
                                        {{-- status --}}
                                        <p><span class="titel-span">@lang('admin_lang.Status')</span> <span class="deta-span"><strong>:</strong> 
                                        @if(@$order->status == 'I')
                                        Incomplete
                                        @elseif(@$order->status == 'N')
                                        @lang('admin_lang.new')
                                        @elseif(@$order->status == 'OA')
                                        @lang('admin_lang.order_accepted')
                                        @elseif(@$order->status == 'DA' || @$order->status == 'PP' || @$order->status == 'PC')
                                        @lang('admin_lang.driver_assigned')
                                        @elseif(@$order->status == 'RP')
                                        @lang('admin_lang.ready_for_pickup')
                                        @elseif(@$order->status == 'OP')
                                        @lang('admin_lang.picked_up')
                                        @elseif(@$order->status == 'OD')
                                        @lang('admin_lang.delivered_1')
                                        @elseif(@$order->status == 'OC')
                                        @lang('admin_lang.Cancelled_1')
                                        @endif
                                        </span></p>
                                    </div>
                                    {{-- @if() --}}
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;"> @lang('admin_lang.customer_details')</h4>
                                        <!-- <p><span class="titel-span">@lang('admin_lang.country')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->userCountryDetails->countryDetailsBylanguage->name }}    
                                            @else
                                            {{ @$order->getCountry->name }}
                                            @endif
                                            </span>
                                        </p> -->
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->fname }} {{ @$order->customerDetails->lname }}
                                            @else
                                            {{ @$order->fname }} {{ @$order->lname }}
                                            @endif
                                            </span>
                                        </p>
                                        {{-- email --}}
                                        {{-- phone --}}
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->phone }}    
                                            @else
                                            {{ @$order->shipping_phone }}
                                            @endif
                                            </span>
                                        </p>
                                        {{-- shipping address --}}
                                        <br>
                                        
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.shipping_address')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.country')</span>
                                            <span class="deta-span"><strong>:</strong>
                                                {{ @$order->getCountry->name }}
                                            </span>
                                        </p>
                                        
                                        @if(@$order->shipping_city)
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_city }}
                                            </span>
                                        </p>
                                        @else
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCityNameByLanguage->name }}
                                            </span>
                                        </p>
                                        @endif

                                        <p><span class="titel-span">@lang('admin_lang.street')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_street }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.block')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_block }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.building')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_building }}
                                            </span>
                                        </p>
                                        @if(@$order->shipping_country != 134)
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_zip }}
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->shipping_address }}
                                            </span>
                                        </p>
                                    </div>
                                </div>
                                <div class="row fld">
                                    @if(@$order->order_type == 'E')
                                    <div class="col-md-6">
                                        
                                    </div>
                                    {{-- driver details --}}
                                    {{-- <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">Driver Details</h4>
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->driverDetails->fname." ".@$order->orderMasterExtDetails->driverDetails->lname }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->driverDetails->email }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->driverDetails->phone }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">Profile picture</span> <span class="deta-span"><strong>:</strong>
                                        @if(@$order->orderMasterExtDetails->driverDetails->image)
                                        @php
                                        $image_path = 'storage/app/public/driver/profile_pics/'.@$order->orderMasterExtDetails->driverDetails->image; 
                                        @endphp
                                        @if(file_exists(@$image_path))
                                        <div class="profile" style="display: block;">
                                            <img id="driverProfilePicture" src="{{ URL::to('storage/app/public/driver/profile_pics/'.@$order->orderMasterExtDetails->driverDetails->image) }}" alt="" style="width: 100px;height: 100px;">
                                        </div>
                                        @endif
                                        @else
                                        <div class="profile" style="display: block;">
                                            <img id="driverProfilePicture" src="{{ getDefaultImageUrl() }}" alt="" style="width: 100px;height: 100px;">
                                        </div>
                                        @endif
                                        </span>
                                        </p>
                                    </div> --}}
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
    @if(Config::get('app.locale') == 'en')
    @include('merchant.includes.scripts')
    @elseif(Config::get('app.locale') == 'ar')
    @include('merchant.includes.arabic_scripts')
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script>
    $( function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 500,
            values: [ 45, 475 ],
            slide: function( event, ui ) {
                $( "#amount" ).val( ui.values[ 0 ]  + "," + ui.values[ 1 ] );
                $('.price_from').html('$' + ui.values[ 0 ])
                $('.price_to').html('$' + ui.values[ 1 ])
            }
        });
        $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 )  +
          "," + $( "#slider-range" ).slider( "values", 1 ) );
        $('.price_from').html('$' + $( "#slider-range" ).slider( "values", 0 ));
        $('.price_to').html('$' + $( "#slider-range" ).slider( "values", 1 ));
        $('.slct').chosen();
        $("#keyword").on("keyup", function(e) {
            var value = $(this).val().toLowerCase();
            $("#dataTable1 tbody tr").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $('#dataTable1').DataTable();

        $("#searchList").click(function(e){

            var customer = $.trim($("#customer").val()),
            payment = $.trim($("#payment").val()),
            keyword = $.trim($("#keyword").val());
            
            $.ajax({
                type:"GET",
                url:"{{ route('merchant.search.order') }}",
                data:{
                    customer:customer,
                    payment:payment,
                    keyword:keyword
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $('#dataTable1').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                   });
                    $('#dataTable1').DataTable();
                }
            });
        });
        $("#payment").change(function(e){

            var customer = $.trim($("#customer").val()),
            payment = $.trim($("#payment").val()),
            keyword = $.trim($("#keyword").val());
            
            $.ajax({
                type:"GET",
                url:"{{ route('merchant.search.order') }}",
                data:{
                    customer:customer,
                    payment:payment,
                    keyword:keyword
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $('#dataTable1').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                   });
                    $('#dataTable1').DataTable();
                }
            });
        });
        $("#customer").change(function(e){

            var customer = $.trim($("#customer").val()),
            payment = $.trim($("#payment").val()),
            keyword = $.trim($("#keyword").val());
            
            $.ajax({
                type:"GET",
                url:"{{ route('merchant.search.order') }}",
                data:{
                    customer:customer,
                    payment:payment,
                    keyword:keyword
                },
                success:function(resp){
                    $(".listBody").html(resp);
                    $('#dataTable1').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                   });
                    $('#dataTable1').DataTable();
                }
            });
        });

    });
</script>
<script src="{{ URL::to('public/merchant/assets/js/chosen.jquery.min.js') }}"></script>

@endsection