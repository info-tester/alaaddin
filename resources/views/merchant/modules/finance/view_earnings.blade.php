@extends('merchant.layouts.app')
{{-- @section('title', 'Alaaddin | Merchant | View Earnings') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.merchant') |@lang('admin_lang.view_earnings')
@endsection
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    .remove-img {
    width: 18px;
    height: 18px;
    font-size: 13px;
    color: #fff;
    text-align: center;
    background: #0771d4;
    border-radius: 50%;
    line-height: 16px;
    float: right;
    margin-left: 4px;
    }
    .remove-img a {
    color: #fff;
    font-size: 10px;
    }
    .action-opt {
    position: absolute;
    right: 0;
    top: 5px;
    z-index: 1;
    }
    .dataTables_length select{
        display: inline-block!important;
        width: 75px !important;
        margin: 0px 5px;
        vertical-align: middle;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
<!--Header Fixed-->
<div class="header fixed-header">
    <div class="container-fluid" style="padding: 10px 25px">
        <div class="row">
            <div class="col-9 col-md-6 d-lg-none">
                <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                <span class="logo">@lang('admin_lang.seller') - {{ @Auth::guard('merchant')->user()->fname }} {{ @Auth::guard('merchant')->user()->lname }}</span>
            </div>
            <div class="col-lg-8 d-none d-lg-block">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">@lang('admin_lang.Dashboard')</a></li>
                    <li class="breadcrumb-item active">@lang('admin_lang.finance')</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h4>@lang('admin_lang.finance')</h4>
                </div>
            </div>
            <div class="col-12">
                <div class="financedtl">
                    <ul>
                        <li>
                            <p><span>@lang('admin_lang.tot_merc_dashb') :</span> {{ @Auth::guard('merchant')->user()->total_earning }} @lang('merchant_lang.kwd')</p>
                        </li>
                        <!-- <li>
                            <p><span>@lang('admin_lang.comission') received from internal order(only):</span> {{ @Auth::guard('merchant')->user()->total_commission }} @lang('merchant_lang.kwd')</p>
                        </li> -->
                        <li>
                            <p><span>@lang('merchant_lang.paid'):</span> {{ @Auth::guard('merchant')->user()->total_paid }} @lang('merchant_lang.kwd') </p>
                        </li>
                        <li>
                            <p><span>Invoice In:</span> {{ @Auth::guard('merchant')->user()->invoice_in }} @lang('merchant_lang.kwd') </p>
                        </li>
                        <li>
                            <p><span>Invoice Out:</span> {{ @Auth::guard('merchant')->user()->invoice_out }} @lang('merchant_lang.kwd') </p>
                        </li>
                        <li>
                            <p><span>@lang('merchant_lang.due'):</span> {{ @Auth::guard('merchant')->user()->total_due }} @lang('merchant_lang.kwd')</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (session()->has('success'))
                <div class="alert alert-success vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
                    <strong>{{ session('success')['message'] }}</strong> {{ session('success')['meaning'] }}
                </div>
                @elseif ((session()->has('error')))
                <div class="alert alert-danger vd_hidden" style="display: block;">
                    <a class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="icon-cross"></i>
                    </a>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
                    <strong>{{ session('error')['message'] }}</strong> {{ session('error')['meaning'] }}
                </div>
                @endif
                <div class="">
                    <div class="block table-block mb-4">
                        <div class="row">
                            <div class="wrqstpop">
                                <a class="rqstw" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter">@lang('admin_lang.request_withdrawl')</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="table-responsive">
                                <table id="dataTable1" class="display table table-striped" data-table="data-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('admin_lang.idss')</th>
                                            <th>@lang('merchant_lang.request_date')</th>
                                            <th>@lang('merchant_lang.process_date')</th>
                                            <th>@lang('merchant_lang.notes')</th>
                                            <th>@lang('merchant_lang.bank_info')</th>
                                            <th>@lang('merchant_lang.amount')</th>
                                            <th>@lang('merchant_lang.status')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(@$withdraws)
                                        @foreach(@$withdraws as $key=>$withdraw)
                                        <tr>
                                            <td>{{ @$key+1 }}</td>
                                            <td>{{ @$withdraw->id }}</td>
                                            <td>{{ @$withdraw->request_date }}</td>
                                            <td>{{ @$withdraw->payment_date }}</td>
                                            <td>{{ @$withdraw->description }}</td>
                                            <td>{{ @$withdraw->account_name }},<br>
                                                {{ @$withdraw->bank_name }}<br>
                                                {{ @$withdraw->account_no }}<br>
                                                {{ @$withdraw->iban_number }}
                                            </td>
                                            <td>{{ @$withdraw->amount }} @lang('merchant_lang.kwd') </td>
                                            <td>
                                                @if(@$withdraw->status == 'N')
                                                @lang('merchant_lang.new')
                                                @elseif(@$withdraw->status == 'C')
                                                @lang('merchant_lang.cancel')
                                                @elseif(@$withdraw->status == 'S')
                                                @lang('merchant_lang.success')
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalCenter">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('merchant_lang.request')</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="basic-form">
                    <form id="request_withdraw_form" method="post" action="{{ route('merchant.request.withdraw',Auth::guard('merchant')->user()->id) }}">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <p class="shw">@lang('merchant_lang.balence'): {{ @Auth::guard('merchant')->user()->total_due }} @lang('merchant_lang.kwd')</p>
                                <input type="hidden" id="balence" name="balence" value="{{ @$due }}">
                                <label>@lang('merchant_lang.enter_amt_kwd_withdraw')</label>
                                <input id="amount" name="amount" type="text" class="form-control" placeholder="@lang('merchant_lang.enter_amt_kwd_withdraw')" onkeypress='validate(event)'>
                                <span class="error_amount" style="color: red;"></span>
                            </div>
                            <div class="form-group col-md-12">
                                <label></label>
                                <button type="button" id="send_request" class="btn btn-primary popbtntp">@lang('admin_lang.send_request')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="loader" style="display: none;">
    <img src="{{url('public/loader.gif')}}">
</div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.slct').chosen();
    $("#amount").blur(function(){
        var balence = $.trim($("#balence").val());
        if(balence != ""){
            // balence = balence.toFixed(3);
            balence = parseFloat(balence);
            var amt = parseFloat($.trim($("#amount").val()));
            if(amt>balence){
                $("#amount").val("");
                $(".error_amount").text('@lang('validation.error_balence')');
            }
        }
    });
    $("#send_request").click(function(){
        var amount = $.trim($("#amount").val()),
        balence = parseFloat($.trim($("#balence").val())).toFixed(3),
        error = 0;
        if(amount != ""){
            amount = parseFloat(amount);
            amount = amount.toFixed(3);
            if(amount <= balence && amount != 0.000){
                $.ajax({
                    type:"GET",
                    url:"{{ route('check.duplicate.request.withdraw') }}",
                    data:{
                        seller_id: "{{ @Auth::guard('merchant')->user()->id }}"
                    },
                    success:function(resp){
                        if(resp == 0){
                            error++;
                            $(".error_amount").text("@lang('merchant_lang.re_with_merc_clear_req')");
                        }else{
                            $("#request_withdraw_form").submit();
                        }
                    }
                });
            }else{
                if(amount == 0.000){
                    $(".error_amount").text("@lang('admin_lang.amt_zero_chk')");
                }else{
                    $(".error_amount").text("@lang('admin_lang.amt_sh_less_eq_bale')");
                }
            }
        }else{
            error++;
            $(".error_amount").text("@lang('validation.required')");
        }
    });
});
function validate(evt) {
    var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>
@endsection
