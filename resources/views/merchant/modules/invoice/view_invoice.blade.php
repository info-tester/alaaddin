@extends('merchant.layouts.app')
{{-- @section('title', 'Alaaddin | Merchant | View Order Details') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.merchant') | @lang('admin_lang.view_order_details')
@endsection
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ URL::to('public/merchant/assets/css/chosen.css') }}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    .remove-img {
        width: 18px;
        height: 18px;
        font-size: 13px;
        color: #fff;
        text-align: center;
        background: #0771d4;
        border-radius: 50%;
        line-height: 16px;
        float: right;
        margin-left: 4px;
    }
    .remove-img a {
        color: #fff;
        font-size: 10px;
    }
    .action-opt {
        position: absolute;
        right: 0;
        top: 5px;
        z-index: 1;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-9 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo">@lang('admin_lang.view_order_details') - {{ @$order->order_no }}</span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">@lang('admin_lang.Dashboard')</a></li>
                        <li class="breadcrumb-item ">@lang('admin_lang.manage_invoice')</li>
                        <li class="breadcrumb-item active">@lang('admin_lang.view_invoice')</li>
                    </ol>
                </div>
                
            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title pull-right">
                        <a class="extrnl btn btn-primary" href="{{ route('merchant.print.invoice.details',['id'=> @$invoice->id]) }}">@lang('admin_lang.print_invoice')</a> &nbsp; <a class="extrnl btn btn-primary" href="{{ route('merchant.invoice') }}">@lang('admin_lang.back')</a>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="block form-block mb-4">
                        <div class="row fld">
                            <div class="col-md-12">
                                <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.invoice_details')</h4>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6" style="padding-left:0px;">
                                    <p><span class="titel-span">@lang('admin_lang.invoice_date') </span> <span class="deta-span"><strong>:</strong> {{ @$invoice->invoice_date }}</span> </p>
                                    <p><span class="titel-span">@lang('admin_lang.invoice_no') </span> <span class="deta-span"><strong>:</strong> {{ @$invoice->invoice_no }}</span> </p>
                                    <p><span class="titel-span">@lang('admin_lang.invoice_type')</span> <span class="deta-span"><strong>:</strong> @if(@$invoice->invoice_type == 'I' ) {{ "In" }} @else {{ "Out" }} @endif</span> </p>
                                    <p><span class="titel-span">@lang('admin_lang.applicable_to')</span> <span class="deta-span"><strong>:</strong>@if(@$invoice->merchant_id ) {{ @$invoice->getMerchant->fname." ".@$invoice->getMerchant->lname }}  (@lang('admin_lang.mer')) @else {{ @$invoice->name }} (@lang('admin_lang.others')) @endif </span></p>
                                    <p><span class="titel-span">@lang('admin_lang.total')</span> <span class="deta-span"><strong>:</strong> {{ @$invoice->total }}</span></p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-12 ">
                    <div class="block form-block mb-4">
                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.product_details')</h4>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered first">
                                <thead>
                                    <tr>
                                        <th>@lang('admin_lang.product_name')</th>
                                        <th>@lang('admin_lang.price')</th>
                                        <th>@lang('admin_lang.qty')</th>
                                        <th>@lang('admin_lang.sub_total_kwd')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(@$invoice->getInvoiceDetails)
                                        @foreach($invoice->getInvoiceDetails as $pro)
                                        <tr>
                                            <td>{{ @$pro->product_name }}</td>
                                            <td>{{ @$pro->price }}</td>
                                            <td>{{ @$pro->qty  }}</td>
                                            <td>{{ @$pro->subtotal }}</td>
                                            
                                        </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>@lang('admin_lang.product_name')</th>
                                        <th>@lang('admin_lang.price')</th>
                                        <th>@lang('admin_lang.qty')</th>
                                        <th>@lang('admin_lang.sub_total_kwd')</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
@endsection
@section('scripts')
    @if(Config::get('app.locale') == 'en')
    @include('merchant.includes.scripts')
    @elseif(Config::get('app.locale') == 'ar')
    @include('merchant.includes.arabic_scripts')
    @endif
@endsection