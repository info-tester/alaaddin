@extends('merchant.layouts.app')
{{-- @section('title', 'Alaaddin | Merchant | Manage Order') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.merchant') |@lang('admin_lang.manage_orders')
@endsection
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ URL::to('public/merchant/assets/css/chosen.css') }}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    .remove-img {
        width: 18px;
        height: 18px;
        font-size: 13px;
        color: #fff;
        text-align: center;
        background: #0771d4;
        border-radius: 50%;
        line-height: 16px;
        float: right;
        margin-left: 4px;
    }
    .remove-img a {
        color: #fff;
        font-size: 10px;
    }
    .action-opt {
        position: absolute;
        right: 0;
        top: 5px;
        z-index: 1;
    }
    .dataTables_length select{
        display: inline-block!important;
        width: 75px !important;
        margin: 0px 5px;
        vertical-align: middle;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">@lang('merchant_lang.Dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('admin_lang.manage_invoice')</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('admin_lang.manage_invoice')
                            {{-- <a class="extrnl" href="{{ route('merchant.add.external.order') }}">@lang('admin_lang.create_external_order')</a> --}}
                        </h4>
                    </div>
                </div>

                <div class="col-md-12">
                    
                    <div class="col-md-12" style="padding:0; ">
                        <div class="block table-block mb-4">
                            <div class="row">
                                <div class="col-md-12" style="margin-bottom: 20px;">
                                    <button class="btn btn-warning print_order_details"><i class="fa fa-print"></i> <span class="print_mark_text"> Print Selected</span></button>
                                </div>
                                <div class="table-responsive listBody">
                                    <table id="dataTable1" class="display table table-striped" data-table="data-table">
                                        <thead>
                                            <tr>
                                                <th class="no-sort" style="padding: 10px"><input type="checkbox" class="select_all" >  Select all</th>
                                                <th>@lang('admin_lang.invoice_date')</th>
                                                <th>@lang('admin_lang.invoice_no')</th>
                                                <th>@lang('admin_lang.invoice_type')</th>
                                                <th>@lang('admin_lang.applicable_to')</th>
                                                <th>@lang('admin_lang.total')</th>
                                                <th>@lang('admin_lang.actions')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="no-sort"></th>
                                                <th>@lang('admin_lang.invoice_date')</th>
                                                <th>@lang('admin_lang.invoice_no')</th>
                                                <th>@lang('admin_lang.invoice_type')</th>
                                                <th>@lang('admin_lang.applicable_to')</th>
                                                <th>@lang('admin_lang.total')</th>
                                                <th>@lang('admin_lang.actions')</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loader" style="display: none;">
        <img src="{{url('public/loader.gif')}}">
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" />

<script>
    $( function() {
        
        
        $('body').on('click', '.reset_search', function() {
            $('#dataTable1').DataTable().search('').columns().search('').draw();
        });

        function filterColumn ( i ) {
            $('#dataTable1').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }

        $('#dataTable1').DataTable({
            "destroy": true, //use for reinitialize datatable
            "order": [
                [0, "desc"]
            ],
            "stateLoadParams": function (settings, data) {
            },
            stateSaveParams: function (settings, data) {
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('merchant.invoice') }}",
                "data": function ( d ) {
                    d._token = "{{ @csrf_token() }}";
                }
            },
            'columns': [
                {
                    "orderable": false,
                    render: function(data, type, full) {
                        return '<input type="checkbox" class="mark_order" name="mark_order" value="'+ full.id +'">'
                    }
                },
                { 
                    data: 'invoice_date',
                },
                { 
                    data: 'invoice_no',
                },
                {  
                     data: 'invoice_type',
                     render: function(data, type, full) {
                         if(data == 'I'){
                             return "@lang('admin_lang.in')";
                         }else{
                            return "@lang('admin_lang.out')"
                         }
                     }
                },
                { 
                    render: function(data, type, full) {
                        if(full.merchant_id != null){
                            return full.get_merchant.fname+' '+full.get_merchant.lname
                        }else{
                            return full.name;
                        }
                        
                    }
                },
                { 
                    data: 'total',
                },
                { 
                    data: 0,
                    render: function(data, id, full, meta) {
                        var a = '';

                        a += ' <a href="{{ url('merchant/view-invoice') }}/'+full.id+'"><i class="fa fa-eye" title="'+"@lang('admin_lang.view')"+'"></i></a>'

                        return a;
                    }
                }
            ]
        });
        $("select[name=dataTable1_length]").change(function() {
            markOrders = [];
            $('.print_mark_text').html(' Print Selected')
            $('.select_all').prop('checked', false);
        });
        $('body').on('click','.paginate_button',function(){
            markOrders = [];
            $('.print_mark_text').html(' Print Selected')
            $('.select_all').prop('checked', false);
        })
    
    });

    var markOrders = [];

    $('body').on('click', '.select_all', function() {
        markOrders = [];
        if($(this).is(':checked')) {
            $('.mark_order').prop('checked', true);
            $('.mark_order').each(function(item, index) {
                markOrders.push($(this).val())
            }); 
        } else {
            $('.mark_order').prop('checked', false);
            $('.mark_order').each(function(item, index1) {
                const index = markOrders.indexOf($(this).val());
                if (index > -1) {
                   markOrders.splice(index, 1);
                }
            });
        }
        if(markOrders.length) {
            $('.mark_delete').removeAttr('disabled')
            $('.delete_mark_text').html(markOrders.length + ' Orders Seleted')
            $('.print_mark_text').html(markOrders.length + ' Orders Seleted')
        }
        if(markOrders.length == 0) {
            $('.mark_delete').attr('disabled', 'disabled')
            $('.delete_mark_text').html(' Delete Selected')
            $('.print_mark_text').html(' Print Selected')
        }
    });

    $('body').on('click', '.mark_order', function() {
        if($(this).is(':checked')) {
            markOrders.push($(this).val())
        } else {
            const index = markOrders.indexOf($(this).val());
            if (index > -1) {
               markOrders.splice(index, 1);
            }
            $('.select_all').prop('indeterminate', true);
        }
        if(markOrders.length) {
            $('.mark_delete').removeAttr('disabled')
            $('.delete_mark_text').html(markOrders.length + ' Orders Seleted')
            $('.print_mark_text').html(markOrders.length + ' Orders Seleted')
        }
        if(markOrders.length == 0) {
            $('.mark_delete').attr('disabled', 'disabled')
            $('.delete_mark_text').html(' Delete Selected')
            $('.print_mark_text').html(' Print Selected')
        }
    })

    // Print bulk order
    $('.print_order_details').on('click',function(){
        if(markOrders.length <= 100){
            if(markOrders.length <1) {
                toastr.error('Please select at least 1 order');
            } else {
                orderIds = btoa(JSON.stringify(markOrders));
                window.open("merchant/print-bulk-invoice-details/"+orderIds ,orderIds,"location=0,toolbar=no,scrollbars=yes,height=850,width=800,left=100,top=10");
            }
        } else {
            toastr.error('Maximum 100 orders can be selected at a time');
        }
    });
    
</script>
<script src="{{ URL::to('public/merchant/assets/js/chosen.jquery.min.js') }}"></script>
@endsection