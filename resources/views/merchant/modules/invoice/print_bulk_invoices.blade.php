@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | View Customer Profile') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.merchant') | @lang('admin_lang.print_invoice')
@endsection
@section('content')
@section('links')
{{-- @include('admin.includes.links') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
<link rel="stylesheet" href="{{ asset('public/admin/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
<link href="{{ asset('public/admin/assets/vendor/fonts/circular-std/style.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('public/admin/assets/libs/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('public/admin/assets/vendor/fonts/fontawesome/css/fontawesome-all.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/buttons.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/select.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/vendor/datatables/css/fixedHeader.bootstrap4.css') }}">
<style>
    /* The container */
    .container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 15px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-family: 'Circular Std Book';
    font-style: normal;
    font-weight: normal;
    }
    /* Hide the browser's default radio button */
    .container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    line-height: 1.42857143;
    }
    /* Create a custom radio button */
    .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
    border-radius: 50%;
    }
    /* On mouse-over, add a grey background color */
    .container:hover input ~ .checkmark {
    background-color: #ccc;
    }
    /* When the radio button is checked, add a blue background */
    .container input:checked ~ .checkmark {
    background-color: #2196F3;
    }
    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
    content: "";
    position: absolute;
    display: none;
    }
    /* Show the indicator (dot/circle) when checked */
    .container input:checked ~ .checkmark:after {
    display: block;
    }
    /* Style the indicator (dot/circle) */
    .container .checkmark:after {
    top: 9px;
    left: 9px;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background: white;
    }
    div .page-break{
        page-break-after : always;
    }
</style>
@endsection
@section('header')
@endsection
@section('sidebar')
@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
    @foreach($invoices as $i => $invoice)
    <div class="dashboard-wrapper"  style="margin-left: 0px;    margin-right: 0px;">
        <div class="dashboard-ecommerce">
            <div class="container-fluid dashboard-content ">
                <div class="row">
                    <!-- ============================================================== --> 
                    <!-- basic table  --> 
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header cardTest" style="line-height: 50px">
                                <strong style="font-size:30px">DRIVERS GROUP</strong> 
                                <img src="{{ URL::to('public/admin/assets/images/logo.png')}}" alt="" style="height: 50px;width: 143px;" class="pull-right">
                            </h5>
                            <div class="card-body">
                                <div class="manager-dtls tsk-div">
                                    <div class="row fld">
                                        <div class="col-md-12">
                                            <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.invoice_details')</h4>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6" style="padding-left:0px;">
                                                <p><span class="titel-span">@lang('admin_lang.invoice_date') </span> <span class="deta-span"><strong>:</strong> {{ @$invoice->invoice_date }}</span> </p>
                                                <p><span class="titel-span">@lang('admin_lang.invoice_no') </span> <span class="deta-span"><strong>:</strong> {{ @$invoice->invoice_no }}</span> </p>
                                                <p><span class="titel-span">@lang('admin_lang.invoice_type')</span> <span class="deta-span"><strong>:</strong> @if(@$invoice->invoice_type == 'I' ) {{ "In" }} @else {{ "Out" }} @endif</span> </p>
                                                <p><span class="titel-span">@lang('admin_lang.applicable_to')</span> <span class="deta-span"><strong>:</strong>@if(@$invoice->merchant_id ) {{ @$invoice->getMerchant->fname." ".@$invoice->getMerchant->lname }}  (@lang('admin_lang.mer')) @else {{ @$invoice->name }} (@lang('admin_lang.others')) @endif </span></p>
                                                <p><span class="titel-span">@lang('admin_lang.total')</span> <span class="deta-span"><strong>:</strong> {{ @$invoice->total }}</span></p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.product_details')</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>@lang('admin_lang.product_name')</th>
                                                <th>@lang('admin_lang.price')</th>
                                                <th>@lang('admin_lang.qty')</th>
                                                <th>@lang('admin_lang.sub_total_kwd')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(@$invoice->getInvoiceDetails)
                                                @foreach($invoice->getInvoiceDetails as $pro)
                                                <tr>
                                                    <td>{{ @$pro->product_name }}</td>
                                                    <td>{{ @$pro->price }}</td>
                                                    <td>{{ @$pro->qty  }}</td>
                                                    <td>{{ @$pro->subtotal }}</td>
                                                    
                                                </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>@lang('admin_lang.product_name')</th>
                                                <th>@lang('admin_lang.price')</th>
                                                <th>@lang('admin_lang.qty')</th>
                                                <th>@lang('admin_lang.sub_total_kwd')</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                        </div>
                    </div>
                    <!-- ============================================================== --> 
                    <!-- end basic table  --> 
                    <!-- ============================================================== --> 
                </div>
            </div>
            
            <!-- footer -->   
            @include('admin.includes.footer')
            <!-- end footer -->
        </div>
        @if(count($invoices) != $i+1)
            <div class="page-break"></div>
            <br><br><br>
        @endif
    </div>
    @endforeach
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}

@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif

<script src="{{ asset('public/admin/cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/datatables/js/data-table.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/admin/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('public/admin/cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('public/admin/assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/slimscroll/jquery.slimscroll.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/multi-select/js/jquery.multi-select.js') }}"></script> 
<script src="{{ asset('public/admin/assets/libs/js/main-js.js') }}"></script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script>
    $(document).ready(function(){
        window.print();
    })
</script>
@endsection