<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Design_Gurus" name="author">
    <meta content="WOW Merchant dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::to('public/frontend/images/fav.png')}}">
    <title>Aswagna | Merchant | Driver Review</title>

    @if(Config::get('app.locale') == 'en')
    <!--favicon-->
    <link href="{{ asset('public/merchant/assets/images/favicon.ico') }}" rel="shortcut icon">
    <!--Preloader-CSS-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/preloader/preloader.css') }}">
    <!--bootstrap-4-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/css/bootstrap.min.css') }}">
    <!--Custom Scroll-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/customScroll/jquery.mCustomScrollbar.min.css') }}">
    <!--Font Icons-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/simple-line/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/dripicons/dripicons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/eightyshades/eightyshades.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/foundation/foundation-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/metrize/metrize.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/typicons/typicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/weathericons/css/weather-icons.min.css') }}">
    <!--Date-range-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/date-range/daterangepicker.css') }}">
    <!--Drop-Zone-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/dropzone/dropzone.css') }}">
    <!--Full Calendar-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/full-calendar/fullcalendar.min.css') }}">
    <!--Normalize Css-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/css/normalize.css') }}">
    <!--Main Css-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/css/main.css') }}">
    
    @elseif(Config::get('app.locale') == 'ar')
    <!--favicon-->
    <link href="{{ asset('public/merchant/arabic/assets/images/favicon.ico') }}" rel="shortcut icon">
    <!--Preloader-CSS-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/preloader/preloader.css') }}">
    <!--bootstrap-4-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/css/bootstrap.min.css') }}">
    <!--Custom Scroll-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/customScroll/jquery.mCustomScrollbar.min.css') }}">
    <!--Font Icons-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/simple-line/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/dripicons/dripicons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/eightyshades/eightyshades.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/foundation/foundation-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/metrize/metrize.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/typicons/typicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/weathericons/css/weather-icons.min.css') }}">
    <!--Date-range-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/date-range/daterangepicker.css') }}">
    <!--Drop-Zone-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/dropzone/dropzone.css') }}">
    <!--Full Calendar-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/full-calendar/fullcalendar.min.css') }}">
    <!--Normalize Css-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/css/normalize.css') }}">
    <!--Main Css-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/css/main.css') }}">

    @endif
    <style>
      
        .rate-base-layer
        {
            color: #007eff;
        }
        .rate-hover-layer
        {
            color: orange;
        }
    </style>
    <style>
       
        .box-class{
            height: 176px;
            width: 82%;
            text-align: center;
            margin-top: 55px;
            margin-left: 60px;
        }
        .b-box {
            width: 100%;
            margin-left: 0px;
            padding: 30px 30px;
            float: left;
        }
        .b_headT {
            width: 100%;
            float: left;
        }
        .b_headT h2 {
            font-size: 32px;
            font-weight: 500;
            float: left;
        }
        .main-form {
            width: 100%;
            float: left;
        }
        .from-field.rev-post {
            width: 100%;
            float: left;
        }
        .review_content {
            width: 100%;
            float: left;
        }
        .left_line label {
            font-size: 18px;
        }
        .login-div {
            margin: 60px auto 30px;
            max-width: 400px;
            padding: 40px 30px 20px;
            background: var(--white-color);
            border-radius: 6px;
        }
        @media (max-width:767px){
            .login-div {
                margin: 20px auto 30px;
            }
            .b-box {
                padding: 20px 20px;
            }

            .b_headT h2 {
                font-size: 25px;
            }

            .b_headT .pull-right h5 {
                font-size: 17px;
                margin-bottom: 3px;
            }
            .left_line label {
                font-size: 17px;
            }

            .rate1 {
                font-size: 30px;
            }
            .resComment{
                font-size: 18px;
            }
            .subClASS{
                font-size: 16px;
                padding: 7px 12px;
            }
        }

        @media (max-width:450px){
            .b_headT h2 {
                width: 100%;
            }
            .b_headT .pull-right {
                float: left !important;
                margin: 10px 0px;
            }
        }
        
    </style>
<body>
    <script type="text/javascript" src="{{ asset('public/merchant/assets/js/jquery-3.2.1.min.js') }}"></script>
    <section style="background: url(../../../images.pexels.com/photos/176851/pexels-photo-176851663a.jpg?w=940&amp;h=650&amp;auto=compress&amp;cs=tinysrgb);background-size: cover">
        <div class="height-100-vh bg-primary-trans">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-8 col-lg-8">
                        <div class="text-center"><a href="javascript:;"><img class="logo-img" src="{{ asset('public/admin/assets/images/logo.png') }}" alt="logo" height="52px" width="100px" style="background: #fff;margin-top: 54px;"></a></div>
                        <div class="login-div addClass" style="padding: 0px !important;
                        max-width: none !important;
                        min-height: auto;
                        width: 100%;
                        float: left;">
                            @if(@$checkForToken != null)
                                @if(@$checkReview == null)
                                    <div class="b-box b_headT hidediv" style="">
                                        <h2>@lang('front_static.post_review')</h2>
                                        @if($errors->any())
                                        <div class="card-body" style="font-size: 20px; padding: 0">
                                            @foreach($errors->all() as $err)
                                                <div class="alert alert-danger" role="alert">
                                                    {{ $err }}
                                                </div>
                                            @endforeach
                                            {{@$message}}
                                        </div>
                                        @endif
                                        @if(@session()->get('success'))
                                        <div class="alert alert-success alert-dismissible" role="alert">
                                            {{session()->get('success')}}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                    
                                        @if(@session()->get('error'))
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            {{session()->get('error')}}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                    
                                        <div class="pull-right" >
                                            <h5>@lang('front_static.order_no'): {{ request()->segment(3) }}</h5>
                                            <h5>@lang('front_static.driver_name'): {{ @$order->orderMasterDetails[0]->driverDetails->fname }} {{ @$order->orderMasterDetails[0]->driverDetails->lname }}</h5>
                                        </div>
                                        <div class="main-form" >
                                            
                                            <div class="from-field rev-post">
                                                <form id="review_form" method="post" action="{{ route('submit.driver.review.mail') }}" enctype="multipart/form-data">
                                                @csrf
                                                    
                                                <div class="review_content">
                                                    <script>
                                                        $(document).ready(function(){
                                                            var options = {
                                                                max_value: 5,
                                                                step_size: 1.0,
                                                                selected_symbol_type: 'utf8_star',
                                                                initial_value: 0.0,
                                                                update_input_field_name: $("#input1"),
                                                            }
                                                            $(".rate").rate();							            

                                                            $(".rate1").rate(options);

                                                            $(".rate1").on("change", function(ev, data){
                                                                console.log(data.from, data.to);
                                                            });

                                                            $(".rate1").on("updateError", function(ev, jxhr, msg, err){
                                                                console.log("This is a custom error event");
                                                            });

                                                            $(".rate1").rate("setAdditionalData", {id: 42});
                                                            $(".rate1").on("updateSuccess", function(ev, data){
                                                                console.log(data);
                                                            });
                                                        
                                                        });
                                                    </script>
                                                    <style type="text/css">
                                                        .rate1
                                                        {
                                                            font-size: 35px;
                                                        }
                                                        .rate1 .rate-hover-layer
                                                        {
                                                            color: #bdf;
                                                        }
                                                        .rate1 .rate-select-layer
                                                        {
                                                            color: #0072ff;
                                                        }
                                                    </style>


                                                    <div class="form-group">
                                                        <div class="left_line">
                                                            <label>@lang('front_static.rate_driver')</label>
                                                            <span id="showRateError" style="margin-left:20px;color:red;display:none">@lang('front_static.driver_rating')</span>
                                                        </div>
                                                        <div class="left_line_left">
                                                            <div class="rate1"></div>
                                                            <input id="input1" name="driver_rating" type="hidden">
                                                        </div>
                                                        <input name="order_id" value="{{ @$order->id }}" type="hidden">
                                                        <input name="merchant_id" value="{{ request()->segment(4) }}" type="hidden">
                                                        <input name="driver_id" value="{{ @$order->orderMasterDetails[0]->driver_id }}" type="hidden">
                                                    </div>



                                                    <div class="form-group">
                                                        <label style="font-size: 21px;" class="resComment">@lang('front_static.comment') :</label>
                                                        <textarea rows="5" class="edt_type2 form-control required" style="padding: 8px !important;font-size: 17px !important;resize: none;" name="comment" placeholder=""></textarea>
                                                    </div>
                                                    <button type="button" class="btn btn-primary checkValidation subClASS">Submit</button>
                                                </div>
                                                    
                                                        
                        
                                                </form>
                        
                        
                                            </div>
                                        </div>
                                    </div>
                                    <div id="showMsg"></div>
                                @else
                                    <div class="b-box">
                                        
                                        <div class="wrapper-page">
                                            <div class="panel box-class" >
                                                <div class="panel-body">
                                                    <img style="margin-bottom: 20px;" src="{{ url('public/merchant/assets/images/expire.png') }}">
                                                    <h2 style="color:#e74c3c">@lang('front_static.rating_given')</h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @else
                                <div class="b-box">
                                    <div class="wrapper-page">
                                        <div class="panel box-class" >
                                            <div class="panel-body">
                                                <img style="margin-bottom: 20px;" src="{{ url('public/merchant/assets/images/expire.png') }}">
                                                <h2 style="color:#e74c3c">@lang('front_static.token_expire')</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!---Right Tray--->
    @if(Config::get('app.locale') == 'en')
    <!--Jquery-->
    
    <script src="{{ URL::asset('public/frontend/js/rater.js') }}" charset="utf-8"></script>
    <!--Bootstrap Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/assets/js/bootstrap.min.js') }}"></script>
    <!--Modernizr Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/js/modernizr.custom.js') }}"></script>
    <!--Morphin Search JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/morphin-search/classie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/morphin-search/morphin-search.js') }}"></script>
    <!--Morphin Search JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/preloader/pathLoader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/preloader/preloader-main.js') }}"></script>
    <!--Chart js-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/charts/Chart.min.js') }}"></script>
    <!--Sparkline Chart Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/sparkline/jquery.charts-sparkline.js') }}"></script>
    <!--Custom Scroll-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/customScroll/jquery.mCustomScrollbar.min.js') }}"></script>
    <!--Sortable Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/sortable2/sortable.min.js') }}"></script>
    <!--DropZone Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/dropzone/dropzone.js') }}"></script>
    <!--Date Range JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/date-range/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/date-range/daterangepicker.js') }}"></script>
    <!--CK Editor JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/ckEditor/ckeditor.js') }}"></script>
    <!--Data-Table JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/data-tables/datatables.min.js') }}"></script>
    <!--Editable JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/editable/editable.js') }}"></script>
    <!--Full Calendar JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/full-calendar/fullcalendar.min.js') }}"></script>
    <!--- Main JS -->
    <script src="{{ asset('public/merchant/assets/js/main.js') }}"></script>
    <script src="{{ asset('public/merchant/assets/js/jquery.validate.js') }}"></script>
    <script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js"></script>
    @elseif(Config::get('app.locale') == 'ar')
    <!--Jquery-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/js/jquery-3.2.1.min.js') }}"></script>
    <!--Bootstrap Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/js/bootstrap.min.js') }}"></script>
    <!--Modernizr Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/js/modernizr.custom.js') }}"></script>
    <!--Morphin Search JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/morphin-search/classie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/morphin-search/morphin-search.js') }}"></script>
    <!--Morphin Search JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/preloader/pathLoader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/preloader/preloader-main.js') }}"></script>
    <!--Chart js-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/charts/Chart.min.js') }}"></script>
    <!--Sparkline Chart Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/sparkline/jquery.charts-sparkline.js') }}"></script>
    <!--Custom Scroll-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/customScroll/jquery.mCustomScrollbar.min.js') }}"></script>
    <!--Sortable Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/sortable2/sortable.min.js') }}"></script>
    <!--DropZone Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/dropzone/dropzone.js') }}"></script>
    <!--Date Range JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/date-range/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/date-range/daterangepicker.js') }}"></script>
    <!--CK Editor JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/ckEditor/ckeditor.js') }}"></script>
    <!--Data-Table JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/data-tables/datatables.min.js') }}"></script>
    <!--Editable JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/editable/editable.js') }}"></script>
    <!--Full Calendar JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/full-calendar/fullcalendar.min.js') }}"></script>
    <!--- Main JS -->
    <script src="{{ asset('public/merchant/arabic/assets/js/main.js') }}"></script>
    <script src="{{ asset('public/merchant/arabic/assets/js/jquery.validate.js') }}"></script>
    <script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js"></script>
    @endif
    <script>
        $(document).ready(function(){
            var checkForToken = "{{ $checkForToken }}";
            var checkAdded = "{{ @$checkReview }}";
            if(checkForToken == "" || checkAdded != ""){
                //$('.addClass').css("background-color", "#fff");
            }
            $('body').on('click','.checkValidation',function(e){
                if($('#input1').val() == 0){
                    e.preventDefault();
                    $('#showRateError').show();
                }else{
                    var form = $('#review_form');
                    $.ajax({
                        'url':"{{ route('merchant.submit.driver.review.mail') }}",
                        'type':"POST",
                        'data':form.serialize(),
                        success:function(res){
                            if(res == 1){
                                $('#showMsg').html(
                                    '<div class="b-box">'+
                                        '<div class="wrapper-page">'+
                                            '<div class="panel box-class" >'+
                                                '<div class="panel-body">'+
                                                    '<img style="margin-bottom: 20px;" src="{{ url('public/merchant/assets/images/thanku2.png') }}">'+
                                                    '<h2 style="color: green">@lang("front_static.thanks_review")</h2>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'
                                )
                                $('.addClass').css("background", "#b4aeae;");
                                $('.hidediv').hide();
                            } else {
                                $('#showMsg').html(
                                    '<div class="b-box">'+
                                        '<div class="wrapper-page">'+
                                            '<div class="panel box-class" >'+
                                                '<div class="panel-body">'+
                                                    '<h2 style="color: red">@lang("front_static.review_error")</h2>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'
                                )
                                $('.addClass').css("background", "#687680");
                                $('.hidediv').hide();
                            }
                        }
                    })
                }
            })
        });
    </script>
</body>
</html>