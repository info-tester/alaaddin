@extends('admin.layouts.app')
{{-- @section('title', 'Alaaddin | Admin | View Order Details') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.admin') | @lang('admin_lang.v_order_dtls')
@endsection
@section('links')
{{-- @include('admin.includes.links') --}}
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/assets/libs/css/chosen.css') }}">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    /* The container */
    .container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 15px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-family: 'Circular Std Book';
    font-style: normal;
    font-weight: normal;
    }
    /* Hide the browser's default radio button */
    .container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    line-height: 1.42857143;
    }
    /* Create a custom radio button */
    .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
    border-radius: 50%;
    }
    /* On mouse-over, add a grey background color */
    .container:hover input ~ .checkmark {
    background-color: #ccc;
    }
    /* When the radio button is checked, add a blue background */
    .container input:checked ~ .checkmark {
    background-color: #2196F3;
    }
    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
    content: "";
    position: absolute;
    display: none;
    }
    /* Show the indicator (dot/circle) when checked */
    .container input:checked ~ .checkmark:after {
    display: block;
    }
    /* Style the indicator (dot/circle) */
    .container .checkmark:after {
    top: 9px;
    left: 9px;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background: white;
    }
    div .page-break{
    page-break-after : always;
    }
</style>
@if(Config::get('app.locale') == 'en')
@include('admin.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_links')
@endif
@endsection
@section('header')

@endsection
@section('sidebar')

@endsection
@section('content')
<!-- wrapper  -->
<!-- ============================================================== -->
@foreach($orders as $i => $order)
<div class="dashboard-wrapper" style="margin-left: 0px;    margin-right: 0px;">
    <div class="dashboard-ecommerce">
        
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== --> 
            <!-- pageheader --> 
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">@lang('admin_lang.view_order_details')</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        
            <!-- ============================================================== --> 
            <!-- end pageheader --> 
            <!-- ============================================================== -->
            <div class="row printDiv" style="">
                <!-- ============================================================== -->
                <!-- basic table  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="margin-top: -68px;">
                    <div class="card" >
                        <h5 class="card-header cardTest" style="line-height: 50px">
                            <strong style="font-size:30px">DRIVER GROUP</strong> 
                            <img src="{{ URL::to('public/admin/assets/images/logo.png')}}" alt="" style="height: 50px;width: 143px;" class="pull-right">
                            {{-- <a class="adbtn btn btn-primary" class="print_invoice" style="color: white;margin-left: 14px;" onclick="window.print()"><i class="fas fa-less-than"></i>@lang('admin_lang.print_invoice')</a> --}}
                        </h5>
                        <div class="card-body">
                            <div class="manager-dtls tsk-div">
                                <div class="row fld">
                                    {{-- Order details --}}
                                    <div class="col-md-12">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.order_details')</h4>
                                        <p>
                                            <h4>
                                                <span class="titel-span" style="font-weight:600;font-size:20px">@lang('admin_lang.order_number')</span> <span class="deta-span" style="font-weight:600;font-size:20px"><strong>:</strong>
                                                {{ @$order->order_no }}
                                                </span> 
                                            </h4>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.no_of_item')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterDetails->count() }}
                                            </span> 
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.order_date')</span> <span class="deta-span"><strong>:</strong>{{ @$order->created_at }}</span></p>
                                        <p><span class="titel-span">Product Total Weight</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterDetails->sum('weight') }} gms
                                            </span>
                                            <br><br><br>
                                        </p>
                                        
                                        <p><span class="titel-span">Original Price</span> <span class="deta-span"><strong>:</strong>{{ @$order->orderSellerInfo->subtotal }} {{getCurrency()}}</span></p>

                                        <p><span class="titel-span">Total Product Discount</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderSellerInfo->total_discount }} {{getCurrency()}}
                                            </span>
                                        </p>

                                        @if(@$order->orderSellerInfo->coupon_bearer == 'M')
                                        <p><span class="titel-span">Coupon Discount</span> <span class="deta-span"><strong>:</strong>-{{ @$order->orderSellerInfo->coupon_distributed_amount }} {{getCurrency()}}</span></p>
                                        @endif

                                        <p><span class="titel-span">Subtotal</span> <span class="deta-span"><strong>:</strong>{{ number_format(@$order->orderSellerInfo->subtotal - @$order->orderSellerInfo->total_discount, 3) }} {{getCurrency()}}</span></p>

                                        @if(@$order->order_type == 'E')
                                        <p><span class="titel-span">@lang('admin_lang.shipping_kwd')</span> <span class="deta-span"><strong>:</strong>{{ @$order->shipping_price }} {{getCurrency()}}</span></p>
                                        @endif

                                        <p><span class="titel-span">@lang('admin_lang.order_total')</span> <span class="deta-span"><strong>:</strong>{{ @$order->orderSellerInfo->order_total }} @lang('admin_lang.kwd') </span></p>

                                        <p><span class="titel-span">@lang('admin_lang.payment_method')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->payment_method == 'C')
                                            @lang('admin_lang.cod')
                                            @else
                                            @lang('admin_lang.online')
                                            @endif
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.order_type')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->order_type == 'I')
                                            @lang('admin_lang.internal_1')
                                            @else
                                            @lang('admin_lang.external_1')
                                            @endif
                                            </span>
                                        </p>
                                    </div>
                                    {{-- Customer details --}}
                                    @if(@$order->order_type == 'I')
                                    <div class="col-md-6">
                                        @if(Auth::guard('merchant')->user()->hide_customer_info == 'N')
                                            <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.customer_details')</h4>
                                            {{-- name --}}
                                            <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                                @if(@$order->user_id != 0)
                                                {{ @$order->customerDetails->fname }} {{ @$order->customerDetails->lname }}
                                                @else
                                                {{ @$order->fname }} {{ @$order->lname }}
                                                @endif
                                                </span>
                                            </p>
                                            {{-- email --}}
                                            <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                                @if(@$order->user_id != 0)  
                                                {{ @$order->customerDetails->email }}    
                                                @else
                                                {{ @$order->email }}
                                                @endif
                                                </span>
                                            </p>
                                            {{-- phone --}}
                                            <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                                @if(@$order->user_id != 0)  
                                                {{ @$order->customerDetails->phone }}    
                                                @else
                                                {{ @$order->shipping_phone }}
                                                @endif
                                                </span>
                                            </p>
                                         
                                            <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.shipping_address')</h4>
                                            <p><span class="titel-span">@lang('admin_lang.country')</span>
                                                <span class="deta-span"><strong>:</strong>
                                                {{ @$order->getCountry->name }}
                                                </span>
                                            </p>
                                            <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->shipping_fname }} {{ @$order->shipping_lname }}
                                                </span>
                                            </p>
                                            <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->shipping_email }}
                                                </span>
                                            </p>
                                            <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->shipping_phone }}
                                                </span>
                                            </p>
                                            <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->getCityNameByLanguage->name }}    
                                                </span>
                                            </p>
                                            <p><span class="titel-span">@lang('admin_lang.street')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->shipping_street }}    
                                                </span>
                                            </p>
                                            <p><span class="titel-span">@lang('admin_lang.block')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->shipping_block }}    
                                                </span>
                                            </p>
                                            <p><span class="titel-span">@lang('admin_lang.building')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->shipping_building }}    
                                                </span>
                                            </p>
                                            @if(@$order->shippingAddress->country == 134)
                                            <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->shipping_postal_code }}    
                                                </span>
                                            </p>
                                            @endif
                                            <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->shipping_more_address }} 
                                                </span>
                                            </p>
                                        @endif
                                    </div>
                                    @endif
                                    @if(@$order->order_type == 'E')
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.customer_details')</h4>
                                        @if(@$order->order_type == 'I')
                                        <p><span class="titel-span">@lang('admin_lang.country')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->userCountryDetails->countryDetailsBylanguage->name }}    
                                            @else
                                            {{ @$order->countryDetails->countryDetailsBylanguage->name }}
                                            @endif
                                            </span>
                                        </p>
                                        @endif
                                        {{-- name --}}
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->fname }} {{ @$order->customerDetails->lname }}
                                            @else
                                            {{ @$order->fname }} {{ @$order->lname }}
                                            @endif
                                            </span>
                                        </p>
                                        {{-- email --}}
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->email }}    
                                            @else
                                            {{ @$order->email }}
                                            @endif
                                            </span>
                                        </p>
                                        {{-- phone --}}
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)  
                                            {{ @$order->customerDetails->phone }}    
                                            @else
                                            {{ @$order->shipping_phone }}
                                            @endif
                                            </span>
                                        </p>
                                        @if(@$order->order_type == 'I')
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->city }}    
                                            @else
                                            {{ @$order->getCityNameByLanguage->name }}
                                            @endif
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.street') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            @else
                                            {{ @$order->shipping_street }}
                                            @endif
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.block') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_block }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.building') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_building }}
                                            </span>
                                        </p>
                                        @if($order->shipping_country != 134)
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->zipcode }}    
                                            @else
                                            {{ @$order->shipping_zip }}
                                            @endif
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->user_id != 0)
                                            {{ @$order->customerDetails->address }}
                                            @else
                                            {{ @$order->shipping_address }}
                                            @endif
                                            </span>
                                        
                                        </p>
                                        @endif
                                        <br><br>
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.shipping_address')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.country')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCountry->name }}
                                            </span>
                                        </p>
                                        {{-- name --}}
                                        @if(@$order->order_type == 'I')
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_fname }} {{ @$order->shipping_lname }}
                                            </span>
                                        </p>
                                        {{-- email --}}
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_email }}
                                            </span>
                                        </p>
                                        {{-- phone --}}
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_phone }}
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.city')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCityNameByLanguage->name }}   
                                            </span>
                                        </p>
                                        @if(@$order->order_type == 'E')
                                        <p><span class="titel-span">@lang('admin_lang.street') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_street }}    
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.block') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_block }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.building') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_building }}
                                            </span>
                                        </p>
                                        @if($order->shipping_country != 134)
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_postal_code }}    
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_address }}
                                            </span>
                                        </p>
                                        @endif
                                    </div>
                                    @endif
                                </div>
                                @if(@$order->order_type == 'E')
                                <div class="row fld">
                                    @if(@$order->external_order_type == 'S')
                                    @endif
                                    <div class="col-md-6">
                                    </div>
                                </div>
                                @endif
                                @if(@$order->order_type == 'I')
                                <div class="row fld">
                                    @if(@$order->user_id != 0)
                                    {{-- shipping address --}}
                                    <div class="col-md-6">
                                    </div>
                                    @endif
                                </div>
                                @endif
                                <hr>
                            </div>
                            
                            <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.order_history') :</h4>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered first">
                                    <thead>
                                        @if(@$order->order_type == 'I')
                                        <tr>
                                            <th>@lang('admin_lang.product_image')</th>
                                            <th>@lang('admin_lang.product')</th>
                                            <th>@lang('admin_lang.attribute')</th>
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight') (Gms)</th>
                                            <th>@lang('admin_lang.sub_total') ({{getCurrency()}})</th>
                                            <th>@lang('admin_lang.total_1') ({{getCurrency()}})</th>
                                        </tr>
                                        @else
                                        <tr>
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight')</th>
                                            <th>@lang('admin_lang.shipping_cst')</th>
                                            <th>@lang('admin_lang.sub_total')</th>
                                            <th>@lang('admin_lang.total_1')</th>
                                            <th>@lang('admin_lang.driver_notes')</th>
                                        </tr>
                                        @endif
                                    </thead>
                                    <tbody>
                                        @if(@$order->orderMasterDetails)
                                        @foreach(@$order->orderMasterDetails as $key=>$details)
                                        @if(@$order->order_type == 'I')
                                        <tr>
                                            <td>
                                                @if(@$details->productDetails->defaultImage->image)
                                                @php
                                                $image_path = 'storage/app/public/products/'.@$details->productDetails->defaultImage->image; 
                                                @endphp
                                                @if(file_exists(@$image_path))
                                                <div class="profile" style="display: block;">
                                                    <img id="profilePicture" src="{{ URL::to('storage/app/public/products/'.@$details->productDetails->defaultImage->image) }}" alt="" style="width: 100px;height: 100px;">
                                                </div>
                                                @endif
                                                @endif
                                            </td>
                                            <td>
                                                {{ $details->productDetails->productByLanguage->title }}
                                            </td>
                                            <td>
                                                @php
                                                $attr = json_decode($details->variants,true);
                                                $attributes ="";
                                                $separator =" ";
                                                @endphp
                                                @if(@$attr)
                                                @foreach(@$attr as $value)
                                                @foreach(@$value as $key=>$v)
                                                @if($key == getLanguage()->id)
                                                @php
                                                $variant = $v['variant'];
                                                $variant_value = $v['variant_value'];
                                                @endphp
                                                @php
                                                $attributes = $attributes.$separator.$variant." :".$variant_value;
                                                @endphp
                                                @endif
                                                @endforeach
                                                @php
                                                $separator =" , ";
                                                @endphp
                                                @endforeach
                                                @endif
                                                {{ @$attributes }}
                                            </td>
                                            <td>
                                                {{ @$details->quantity  }}
                                            </td>
                                            <td>{{ @$details->weight  }}</td>

                                            <td>
                                                @if(@$details->discounted_price != 0.000)
                                                {{ @$details->discounted_price }}
                                                @else
                                                {{ @$details->original_price }}
                                                @endif
                                            </td>
                                            <td>
                                                {{ @$details->total }}
                                            </td>
                                        </tr>
                                        @else
                                        <tr>
                                            <td>
                                                {{ @$order->orderMasterDetails->count() }}
                                            </td>
                                            <td>
                                                {{ @$order->product_total_weight }}
                                            </td>
                                            <td>
                                                {{ @$order->shipping_price }}
                                            </td>
                                            <td>
                                                {{ @$order->subtotal }}
                                            </td>
                                            <td>
                                                {{ @$order->order_total }}
                                            </td>
                                            <td>{!!strip_tags(@$details->driver_notes)!!}</td>
                                        </tr>
                                        @endif
                                        @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        @if(@$order->order_type == 'I')
                                        <tr>
                                            <th>@lang('admin_lang.product_image')</th>
                                            <th>@lang('admin_lang.product')</th>
                                            <th>@lang('admin_lang.attribute')</th>
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight') (Gms)</th>
                                            <th>@lang('admin_lang.sub_total') ({{getCurrency()}})</th>
                                            <th>@lang('admin_lang.total_1') ({{getCurrency()}})</th>
                                        </tr>
                                        @else
                                        <tr>
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight')</th>
                                            <th>@lang('admin_lang.shipping_cst')</th>
                                            <th>@lang('admin_lang.sub_total')</th>
                                            <th>@lang('admin_lang.total_1')</th>
                                            <th>Driver Notes</th>
                                        </tr>
                                        @endif
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(count($orders) != $i+1)
        <div class="page-break"></div>
        <br><br><br>
    @endif
</div>
@endforeach
<!-- end main wrapper -->
@endsection
@section('scripts')
{{-- @include('admin.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('admin.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('admin.includes.arabic_scripts')
@endif
<script src="{{ asset('public/admin/assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/slimscroll/jquery.slimscroll.js') }}"></script> 
<script src="{{ asset('public/admin/assets/vendor/multi-select/js/jquery.multi-select.js') }}"></script> 
<script src="{{ asset('public/admin/assets/libs/js/main-js.js') }}"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        window.print();
    });
</script>
@endsection