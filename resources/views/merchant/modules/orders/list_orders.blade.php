@extends('merchant.layouts.app')
{{-- @section('title', 'Alaaddin | Merchant | Manage Order') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.merchant') |@lang('admin_lang.manage_orders')
@endsection
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ URL::to('public/merchant/assets/css/chosen.css') }}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    .remove-img {
        width: 18px;
        height: 18px;
        font-size: 13px;
        color: #fff;
        text-align: center;
        background: #0771d4;
        border-radius: 50%;
        line-height: 16px;
        float: right;
        margin-left: 4px;
    }
    .remove-img a {
        color: #fff;
        font-size: 10px;
    }
    .action-opt {
        position: absolute;
        right: 0;
        top: 5px;
        z-index: 1;
    }
    .dataTables_length select{
        display: inline-block!important;
        width: 75px !important;
        margin: 0px 5px;
        vertical-align: middle;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">@lang('merchant_lang.Dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('admin_lang.manage_orders')</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('admin_lang.manage_orders')
                            <a class="extrnl" href="{{ route('merchant.add.external.order') }}">@lang('admin_lang.create_external_order')</a>
                        </h4>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="block form-block mb-4">
                        <form id="searchForm">
                            <div class="form-row">
                                <div class="form-group col-md-4" data-column="0">
                                    <label>@lang('admin_lang.keyword')</label>
                                    <input id="col0_filter" name="keyword" class="form-control keyword" placeholder="@lang('admin_lang.search_by_nm_ord')" type="text">
                                </div>
                                @if(Auth::guard('merchant')->user()->hide_customer_info == 'N')
                                <div class="form-group  col-md-4" data-column="15">
                                    <label>@lang('admin_lang.cust')</label>
                                    <select data-placeholder="Choose a Customer..." class="select slt slct customer"  tabindex="3" id="col15_filter" name="customer">
                                        <option value="">@lang('admin_lang.select_customer')</option>
                                        @if(@$customers)
                                        @foreach(@$customers as $customer)
                                        <option value="{{ @$customer->customerDetails->id }}" data-external="n">{{ @$customer->customerDetails->fname." ".@$customer->customerDetails->lname }}</option>
                                        @endforeach
                                        @endif
                                        @if(@$external_customer)
                                        @foreach(@$external_customer as $external)
                                        <option value="{{ @$external->id }}" data-external="y">{{ @$external->fname." ".@$external->lname }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                @endif

                                <div class="form-group col-md-4" data-column="5">
                                    <label>@lang('admin_lang.payment_method')</label>
                                    <select data-placeholder="Choose a payment method..." class="select slt slct payment_method"  tabindex="3" id="col5_filter" name="payment">
                                        <option value="">@lang('admin_lang.select_payment_method')</option>
                                        <option value="C">@lang('admin_lang.cod')</option>
                                        <option value="O">@lang('admin_lang.online')</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4" data-column="4">
                                    <label>@lang('admin_lang.slt_order_type')</label>
                                    <select data-placeholder="Choose a order type..." class="select slt slct order_type"  tabindex="3" id="col4_filter" name="order_type">
                                        <option value="">@lang('admin_lang.slt_order_type')</option>
                                        <option value="I">@lang('admin_lang.internal_1')</option>
                                        <option value="E">@lang('admin_lang.external_1')</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4" data-column="6">
                                    <label>@lang('admin_lang.slct_ord_status')</label>
                                    <select data-placeholder="Choose a order status..." class="select slt slct order_status"  tabindex="3" id="col6_filter" name="order_status">
                                        <option value="">@lang('admin_lang.slct_ord_status')</option>
                                        <option value="N">@lang('admin_lang.new')</option>
                                        <option value="DA">@lang('admin_lang.driver_assigned')</option>
                                        <option value="RP">@lang('admin_lang.ready_for_pickup')</option>
                                        <option value="OP">@lang('admin_lang.ord_picked_up')</option>
                                        <option value="OD">@lang('admin_lang.order_delivered')</option>
                                        <option value="OC">@lang('admin_lang.order_cancel')</option>
                                    </select>
                                </div>

                                <div class="form-group  col-md-4">
                                    <label>@lang('merchant_lang.external_order_from')</label>
                                    <select data-placeholder="Choose a external order type..." class="select slt slct"  tabindex="3" id="external_order_type" name="external_order_type">
                                        <option value="">@lang('merchant_lang.select_external_order_type')</option>
                                        <option value="S">@lang('merchant_lang.website')</option>
                                        <option value="L">@lang('merchant_lang.link')</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-12">
                                    <button class="btn btn-primary" type="button" id="searchList">@lang('admin_lang.Search')</button>
                                    <input type="reset" value="@lang('admin_lang.reset_search')" class="btn btn-default reset_search">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12" style="padding:0; ">
                        <div class="block table-block mb-4">
                            <div class="row">
                                <div class="col-md-12" style="margin-bottom: 20px;">
                                    <button class="btn btn-warning print_order_details"><i class="fa fa-print"></i> <span class="print_mark_text"> Print Selected</span></button>
                                </div>
                                <div class="table-responsive listBody">
                                    <table id="dataTable1" class="display table table-striped" data-table="data-table">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" class="select_all"> Select all</th>
                                                <th>@lang('admin_lang.slno_order')</th>
                                                @if(Auth::guard('merchant')->user()->hide_customer_info == 'N')
                                                <th>@lang('admin_lang.customer_1')</th>
                                                @endif
                                                <th>@lang('admin_lang.date_1')</th>
                                                <th>@lang('admin_lang.sub_total_kwd')</th>
                                                <th>@lang('admin_lang.shipping_kwd')</th>
                                                <th>@lang('admin_lang.total_payment_kwd')</th>
                                                <th>@lang('admin_lang.payment_1')</th>
                                                <th>@lang('admin_lang.city')</th>
                                                <th>@lang('admin_lang.type_2')</th>
                                                <th>@lang('admin_lang.delivery_date')</th>
                                                <th>@lang('admin_lang.delivery_time')</th>
                                                <th>@lang('admin_lang.Status')</th>
                                                <th>@lang('merchant_lang.external_order_from')</th>
                                                <th>@lang('admin_lang.notes')</th>
                                                <th>@lang('admin_lang.driver_notes')</th>
                                                <th>@lang('admin_lang.Action')</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loader" style="display: none;">
        <img src="{{url('public/loader.gif')}}">
    </div>
</div>
@endsection
@section('scripts')
{{-- @include('merchant.includes.scripts') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.scripts')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_scripts')
@endif
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" />

<script>
    $( function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 500,
            values: [ 45, 475 ],
            slide: function( event, ui ) {
                $( "#amount" ).val( ui.values[ 0 ]  + "," + ui.values[ 1 ] );
                $('.price_from').html('$' + ui.values[ 0 ])
                $('.price_to').html('$' + ui.values[ 1 ])
            }
        });
        $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 )  +
          "," + $( "#slider-range" ).slider( "values", 1 ) );
        $('.price_from').html('$' + $( "#slider-range" ).slider( "values", 0 ));
        $('.price_to').html('$' + $( "#slider-range" ).slider( "values", 1 ));
        $('.slct').chosen();
        
        $('body').on('click', '.reset_search', function() {
            $('#dataTable1').DataTable().search('').columns().search('').draw();
        });

        function filterColumn ( i ) {
            $('#dataTable1').DataTable().column( i ).search(
                $('#col'+i+'_filter').val(),
            ).draw();
        }

        $('#dataTable1').DataTable({
            "destroy": true, //use for reinitialize datatable
            "order": [
                [0, "desc"]
            ],
            "stateLoadParams": function (settings, data) {
                $('#col0_filter').val(data.search.order_no);
                $('#col4_filter').val(data.search.order_type);
                $('#col5_filter').val(data.search.payment_method);
                $('#col6_filter').val(data.search.status);
                $('#col15_filter').val(data.search.customer_search);
            },
            stateSaveParams: function (settings, data) {
                data.search.order_no = $('#col0_filter').val();
                data.search.order_type = $('#col4_filter').val();
                data.search.payment_method = $('#col5_filter').val();
                data.search.status = $('#col6_filter').val();
                data.search.customer_search = $('#col15_filter').val();
            },
            "processing": true,
            "serverSide": true,
            'serverMethod': 'post',
            "ajax": {
                "url": "{{ route('merchant.search.order') }}",
                "data": function (d) {
                    d.myKey = "myValue";
                    d._token = "{{ @csrf_token() }}";
                },
                /*"dataSrc": function(response) {
                    $('#showOnline').html(response.totalOnlineShpPrc+' KWD');
                    $('#showCod').html(response.totalCodShpPrc+' KWD');
                    $('#showCodSub').html(response.totalCodSubtotal+' KWD');
                    $('#showOnlineSub').html(response.totalOnlineSubtotal+' KWD');
                    $('#showTotalDel').html(response.totalOrderDelivered);
                    return response.aaData
                }*/
            },
            'columns': [
                {
                    "orderable": false,
                    render: function(data, type, full) {
                        return '<input type="checkbox" class="mark_order" name="mark_order" value="'+ full.id +'">'
                    }
                },
                {
                    data: 'order_no',
                },
                @if(Auth::guard('merchant')->user()->hide_customer_info == 'N')
                {
                    render: function(data, type, full) {
                        if(full.order_type == 'I'){
                            var fname="--",lname="--";
                            if(full.shipping_fname == null || full.shipping_fname == ""){
                                fname = "--";
                                return fname+" "+lname;
                            }
                            else if(full.shipping_lname == null || full.shipping_lname == "")
                            {
                                lname = "--";
                                return fname+" "+lname;
                            }else{
                                return full.shipping_fname+" "+full.shipping_lname;
                            }
                        } else {
                            var fname="",lname="";
                            if(full.fname == null || full.fname == ""){
                                return full.lname;
                            }
                            else if(full.lname == null || full.lname == "") {
                                return full.fname;
                            }else{
                                return full.fname+" "+full.lname;
                            }
                        }
                    }
                },
                @endif
                {
                    data: 'created_at'
                },
                {
                    render: function(data, type, full) {
                        return (full.order_seller_info.subtotal - full.order_seller_info.total_discount).toFixed(3)
                    }
                },
                {
                    render: function(data, type, full) {
                        if(full.order_type == 'E') {
                            return full.shipping_price
                        } else {
                            return 'N/A'
                        }
                    }
                },
                {
                    render: function(data, type, full) {
                        var total = '';
                        if(full.order_type == 'E') {
                            if(full.order_seller_info.subtotal != null && full.order_seller_info.shipping_price != null){
                                total = (parseFloat(full.order_seller_info.subtotal) - parseFloat(full.order_seller_info.total_discount) + parseFloat(full.shipping_price)).toFixed(3);
                            }else{
                                total = 'N.A';
                            }
                            
                        } else {
                            var total = (parseFloat(full.order_seller_info.subtotal) - parseFloat(full.order_seller_info.total_discount)).toFixed(3);
                        }
                        return total;
                    }
                },
                {
                    render: function(data, type, full) {
                        if(full.payment_method == 'C') {
                            return 'COD'
                        } else if(full.payment_method == 'O') {
                            return 'Online'
                        }else{
                            return '--'
                        }
                    }
                },
                {
                    render: function(data, type, full) {
                        if(full.shipping_country == 134 ) {
                            return full.shipping_city
                            // return full.get_city_name_by_language.name
                        } else {
                            return full.shipping_city
                        }
                    }
                },
                {
                    render: function(data, type, full) {
                        if(full.order_type == 'E') {
                            return 'External'
                        } else {
                            return 'Internal'
                        }
                    }
                },
                { data: 'delivery_date' },
                { data: 'delivery_time' },
                {
                    render: function(data, type, full) {
                        if(full.status == 'I') {
                            return 'Incomplete'
                        } else if(full.order_seller_info.status == 'N') {
                            return "@lang('admin_lang.new')"
                        } else if(full.order_seller_info.status == 'OA') {
                            return "@lang('admin_lang.order_accepted')"
                        } else if(full.order_seller_info.status == 'DA' || full.order_seller_infostatus == 'PP' || full.order_seller_infostatus == 'PC') {
                            return "@lang('admin_lang.driver_assigned')"
                        } else if(full.order_seller_info.status == 'RP') {
                            return "@lang('admin_lang.ready_for_pickup')"
                        } else if(full.order_seller_info.status == 'OP') {
                            return "@lang('admin_lang.ord_picked_up')"
                        } else if(full.order_seller_info.status == 'OD') {
                            return "@lang('admin_lang.delivered_1')"
                        } else if(full.order_seller_info.status == 'OC') {
                            return "@lang('admin_lang.Cancelled_1')"
                        }
                    }
                },
                {
                    render: function(data, id, full) {
                        if(full.external_order_type == 'S') {
                            return 'Website'
                        } else {
                            return 'Link'
                        }
                    }
                },
                {
                    data: 'notes'
                },
                { data: 'driver_notes' },
                {
                    render: function(data, type, full) {
                        var a = '';
                        a += '<a href="{{ url('merchant/merchant-view-order') }}/'+ full.id +'"><i class="fa fa-eye" title="View"></i></a>'
                        if(full.order_type == 'E') {
                            if(full.status == 'N' || full.status == 'DA') {
                                a += ' <a href="{{ url('merchant/edit-external-order') }}/' + full.id + '"><i class="fa fa-edit" title="@lang('admin_lang.admin_lang.Edit')"></i></a>'
                            }
                        }
                        return a;
                    }
                }
            ]
        });

        $('input.keyword').on( 'keyup click', function () {
            filterColumn( $(this).parents('div').data('column') );
        });
        $('.order_type').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        });

        $('.payment_method').on( 'change', function () {
            filterColumn( $(this).parents('div').data('column') );
        });

        $('.order_status').on('change', function () {
            filterColumn(6);
        });

        $('.customer_search').on('change', function () {
            filterColumn( $(this).parents('div').data('column'));
        });

        $("select[name=dataTable1_length]").change(function() {
            markOrders = [];
            $('.print_mark_text').html(' Print Selected')
            $('.select_all').prop('checked', false);
        });
        $('body').on('click','.paginate_button',function(){
            markOrders = [];
            $('.print_mark_text').html(' Print Selected')
            $('.select_all').prop('checked', false);
        })
    });

    var markOrders = [];

    $('body').on('click', '.select_all', function() {
        markOrders = [];
        if($(this).is(':checked')) {
            $('.mark_order').prop('checked', true);
            $('.mark_order').each(function(item, index) {
                markOrders.push($(this).val())
            }); 
        } else {
            $('.mark_order').prop('checked', false);
            $('.mark_order').each(function(item, index1) {
                const index = markOrders.indexOf($(this).val());
                if (index > -1) {
                   markOrders.splice(index, 1);
                }
            });
        }
        if(markOrders.length) {
            $('.mark_delete').removeAttr('disabled')
            $('.delete_mark_text').html(markOrders.length + ' Orders Seleted')
            $('.print_mark_text').html(markOrders.length + ' Orders Seleted')
        }
        if(markOrders.length == 0) {
            $('.mark_delete').attr('disabled', 'disabled')
            $('.delete_mark_text').html(' Delete Selected')
            $('.print_mark_text').html(' Print Selected')
        }
    });

    $('body').on('click', '.mark_order', function() {
        if($(this).is(':checked')) {
            markOrders.push($(this).val())
        } else {
            const index = markOrders.indexOf($(this).val());
            if (index > -1) {
               markOrders.splice(index, 1);
            }
            $('.select_all').prop('indeterminate', true);
        }
        if(markOrders.length) {
            $('.mark_delete').removeAttr('disabled')
            $('.delete_mark_text').html(markOrders.length + ' Orders Seleted')
            $('.print_mark_text').html(markOrders.length + ' Orders Seleted')
        }
        if(markOrders.length == 0) {
            $('.mark_delete').attr('disabled', 'disabled')
            $('.delete_mark_text').html(' Delete Selected')
            $('.print_mark_text').html(' Print Selected')
        }
    })

    // Print bulk order
    $('.print_order_details').on('click',function(){
        if(markOrders.length <= 100){
            if(markOrders.length <1) {
                toastr.error('Please select at least 1 order');
            } else {
                orderIds = btoa(JSON.stringify(markOrders));
                window.open("merchant/bulk-print-orders/"+orderIds ,orderIds,"location=0,toolbar=no,scrollbars=yes,height=850,width=800,left=100,top=10");
            }
        } else {
            toastr.error('Maximum 100 orders can be selected at a time');
        }
    });

    
</script>
<script src="{{ URL::to('public/merchant/assets/js/chosen.jquery.min.js') }}"></script>
@endsection