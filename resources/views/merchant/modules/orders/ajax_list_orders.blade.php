 <table id="dataTable1" class="display table table-striped" data-table="data-table">
    <thead>
        <tr>
            <th>@lang('admin_lang.slno_order')</th>
            <th>@lang('admin_lang.customer_1')</th>
            <th>@lang('admin_lang.date_1')</th>
            <th>@lang('admin_lang.sub_total_kwd')</th>
            <th>@lang('admin_lang.shipping_kwd')</th>
            <th>@lang('merchant_lang.pickup_country')</th>
            <th>@lang('merchant_lang.shipping_country')</th>
            <th>@lang('merchant_lang.pickup_city')</th>
            <th>@lang('merchant_lang.shipping_city')</th>
            <th>@lang('admin_lang.total_payment_kwd')</th>
            <th>@lang('admin_lang.payment_1')</th>
            <th>@lang('admin_lang.type_2')</th>
            <th>@lang('admin_lang.delivery_date')</th>
            <th>@lang('admin_lang.delivery_time')</th>
            <th>@lang('admin_lang.Status')</th>
            <th>@lang('merchant_lang.external_order_from')</th>
            <th>@lang('admin_lang.notes')</th>
            <th>@lang('admin_lang.driver_notes')</th>
            <th>@lang('admin_lang.Action')</th>
            <!-- <th>@lang('admin_lang.Action')</th> -->
        </tr>
    </thead>
    <tbody>
        @if(@$orders)
        @foreach(@$orders as $key=>$order)
        <tr>
            <td> {{ @$order->order_no }}</td>
            <td>
                @if(@$order->user_id != 0)
                {{ @$order->customerDetails->fname." ".@$order->customerDetails->lname }}
                @else
                {{ @$order->shipping_fname." ".@$order->shipping_lname }}
                @endif

            </td>
            <td>{{ @$order->created_at }}</td>
            <td>
                {{@$order->orderSellerInfo->subtotal - @$order->orderSellerInfo->total_discount}}  
                <!-- {{ @$order->subtotal }} -->
                
            </td>

            <td>@if(@$order->order_type == 'E') {{ @$order->shipping_price }} @endif</td>
            <td>{{ @$order->getpickupCountryDetails->name }}</td>
            <td>{{ @$order->countryDetails->countryDetailsBylanguage->name }}</td>
            <td>@if(@$order->pickup_country == 134) {{ @$order->getpickupCityDetails->name }}  @else {{ @$order->pickup_city }} @endif</td>
            <td>@if(@$order->shipping_country == 134) {{ @$order->getCityNameByLanguage->name }}  @else {{ @$order->shipping_city }} @endif</td>

            <td>

                @if(@$order->order_type == 'E')
                {{@$order->orderSellerInfo->subtotal - @$order->orderSellerInfo->total_discount + @$order->shipping_price }}  
                @else
                {{@$order->orderSellerInfo->subtotal - @$order->orderSellerInfo->total_discount }}  
                @endif

                <!-- {{ @$order->order_total }} -->
            </td>
            <td>
                @if(@$order->payment_method == 'C')
                    @lang('admin_lang.cod')
                @elseif(@$order->payment_method == 'O')
                    @lang('admin_lang.online')
                @else
                    --
                @endif
            </td>
            <td>
                @if(@$order->order_type == 'I')
                @lang('admin_lang.internal_1')
                @else
                @lang('admin_lang.external_1')
                @endif
            </td>
            <td>{{ @$order->delivery_date }}</td>
            <td>{{ @$order->delivery_time }}</td>
            <td>
                @if(@$order->status == 'I')
                Incomplete
                @elseif(@$order->status == 'N')
                @lang('admin_lang.new')
                @elseif(@$order->status == 'OA')
                @lang('admin_lang.order_accepted')
                @elseif(@$order->status == 'DA' || @$order->status == 'PP' || @$order->status == 'PC')
                @lang('admin_lang.driver_assigned')
                @elseif(@$order->status == 'RP')
                @lang('admin_lang.ready_for_pickup')
                @elseif(@$order->status == 'OP')
                @lang('admin_lang.ord_picked_up')
                @elseif(@$order->status == 'OD')
                @lang('admin_lang.delivered_1')
                @elseif(@$order->status == 'OC')
                @lang('admin_lang.Cancelled_1')
                @endif
            </td>
            <td>
                @if(@$order->external_order_type == 'S')
                    @lang('merchant_lang.website')
                @else
                    @lang('merchant_lang.link')
                @endif
            </td>
            <td>
                {{@$order->notes}}
            </td>
            <td>{!! strip_tags(@$order->driver_notes) !!}</td>
            <td>
                <a href="{{ route('merchant.view.order',@$order->id) }}"><i class="fa fa-eye" title="View"></i></a>
                @if(@$order->order_type == "E")
                @if(@$order->status != "OP" && @$order->status != "OD" && @$order->status != "OC")
                @if(@$order->status == 'N' || @$order->status == 'DA')
                <a href="{{ route('merchant.edit.external.order',@$order->id) }}"><i class="fa fa-edit" title="@lang('admin_lang.admin_lang.Edit')"></i></a>
                @endif
                @endif
                @endif
            </td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>