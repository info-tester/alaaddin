@extends('merchant.layouts.app')
{{-- @section('title', 'Alaaddin | Merchant | View Order Details') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.merchant') | @lang('admin_lang.view_order_details')
@endsection
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ URL::to('public/merchant/assets/css/chosen.css') }}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    .remove-img {
        width: 18px;
        height: 18px;
        font-size: 13px;
        color: #fff;
        text-align: center;
        background: #0771d4;
        border-radius: 50%;
        line-height: 16px;
        float: right;
        margin-left: 4px;
    }
    .remove-img a {
        color: #fff;
        font-size: 10px;
    }
    .action-opt {
        position: absolute;
        right: 0;
        top: 5px;
        z-index: 1;
    }
    .dataTables_length select{
        display: inline-block!important;
        width: 75px !important;
        margin: 0px 5px;
        vertical-align: middle;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
        <!--Header Fixed-->
        <div class="header fixed-header">
            <div class="container-fluid" style="padding: 10px 25px">
                <div class="row">
                    <div class="col-9 col-md-6 d-lg-none">
                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                        <span class="logo">@lang('admin_lang.view_order_details') - {{ @$order->order_no }}</span>
                    </div>
                    <div class="col-lg-8 d-none d-lg-block">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">@lang('admin_lang.Dashboard')</a></li>
                            <li class="breadcrumb-item ">@lang('admin_lang.manage_orders')</li>
                            <li class="breadcrumb-item active">@lang('admin_lang.view_order_details')</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="content sm-gutter">
            <div class="container-fluid padding-25 sm-padding-10">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title">
                            <h4>@lang('admin_lang.view_order_details') - {{ @$order->order_no }}</h4>
                            <a class="extrnl" href="{{ route('merchant.list.order') }}">@lang('admin_lang.back')</a>
                        </div>
                    </div>

                   <div class="col-md-12">
                        <div class="block form-block mb-4">
                            <form action="#">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.order_details')

                                        </h4>
                                        {{-- no of item --}}
                                        <p><span class="titel-span"> @lang('admin_lang.no_of_item') </span> <span class="deta-span"><strong>: </strong>
                                            {{ @$total_no_item }}
                                        </span> </p>
                                        {{-- driver --}}

                                        <p><span class="titel-span">@lang('admin_lang.order_date')</span> <span class="deta-span"><strong>:</strong>{{ @$order->created_at }}</span></p>

                                        {{-- delivery date & time --}}
                                        @if(@$order->order_type == 'E')
                                            @if(@$order->external_order_type == 'S')
                                                <p>
                                                    <span class="titel-span">@lang('admin_lang.delivery_date')</span> <span class="deta-span"><strong>:</strong>{{ @$order->delivery_date }}</span>
                                                </p>
                                                <p>
                                                    <span class="titel-span">@lang('admin_lang.delivery_time')</span> 
                                                    <span class="deta-span"><strong>:</strong> {{ @$order->delivery_time }}</span>
                                                </p>
                                            @elseif(@$order->external_order_type == 'L')
                                                <p><span class="titel-span">@lang('merchant_lang.prefered_delivery_date')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->preferred_delivery_date }}
                                                    </span>
                                                </p>
                                                <p><span class="titel-span">@lang('merchant_lang.prefered_delivery_time')</span> <span class="deta-span"><strong>:</strong>
                                                    {{ date('h:i a',strtotime(@$order->getPreferredTime->from_time)) }} - {{ date('h:i a',strtotime(@$order->getPreferredTime->to_time)) }}
                                                </span>
                                                </p>
                                            @endif
                                        @endif
                                        <p><span class="titel-span">Order Subtotal</span> <span class="deta-span"><strong>:</strong>{{ @$ord_sel->subtotal }} @lang('admin_lang.kwd')</span></p>

                                        <p><span class="titel-span">Total Product Discount </span> <span class="deta-span"><strong>:</strong>- {{ @$ord_sel->total_discount }} @lang('admin_lang.kwd')</span></p>
                                        
                                        @if($ord_sel->coupon_bearer == 'M')
                                        <p><span class="titel-span">Coupon Discount</span> <span class="deta-span"><strong>:</strong>@if(@$ord_sel->coupon_distributed_amount) - {{ @$ord_sel->coupon_distributed_amount }} @lang('admin_lang.kwd') @endif</span></p>
                                        @endif
                                        <p><span class="titel-span">Order Total</span> <span class="deta-span"><strong>:</strong>{{ @$ord_total }} @lang('admin_lang.kwd')</span></p>
                                        {{-- payment method --}}
                                        <p><span class="titel-span">@lang('admin_lang.payment_method')</span> <span class="deta-span"><strong>:</strong>
                                        @if(@$order->payment_method == 'C')
                                            @lang('admin_lang.cod')
                                        @elseif($order->payment_method == 'O')
                                            @lang('admin_lang.online')
                                        @else
                                         --
                                        @endif
                                        </span></p>
                                        {{-- order type --}}
                                        <p><span class="titel-span">@lang('admin_lang.order_type')</span> <span class="deta-span"><strong>:</strong>
                                        @if(@$order->order_type == 'I')
                                        @lang('admin_lang.internal_1')
                                        @else
                                        @lang('admin_lang.external_1')
                                        @endif
                                        </span></p>
                                        
                                        {{-- status --}}
                                        <p><span class="titel-span">@lang('admin_lang.Status')</span> <span class="deta-span"><strong>:</strong> 
                                        @if(@$ord_sel->status == 'I')
                                        Incomplete
                                        @elseif(@$ord_sel->status == 'N')
                                        @lang('admin_lang.new')
                                        @elseif(@$ord_sel->status == 'OA')
                                        @lang('admin_lang.order_accepted')
                                        @elseif(@$ord_sel->status == 'DA' || @$order->ord_sel == 'PP' || @$order->ord_sel == 'PC')
                                        @lang('admin_lang.driver_assigned')
                                        @elseif(@$ord_sel->status == 'RP')
                                        @lang('admin_lang.ready_for_pickup')
                                        @elseif(@$ord_sel->status == 'OP')
                                        @lang('admin_lang.picked_up')
                                        @elseif(@$ord_sel->status == 'OD')
                                        @lang('admin_lang.delivered_1')
                                        @elseif(@$ord_sel->status == 'OC')
                                        @lang('admin_lang.Cancelled_1')
                                        @endif
                                        </span></p>
                                        {{-- images --}}
                                        @if(@$order->order_type == 'E')
                                            <p><span class="titel-span">@lang('admin_lang.images')</span> <span class="deta-span"><strong>:</strong> 
                                            @if(@$order->getOrderAllImages)
                                                @foreach(@$order->getOrderAllImages as $imgs)
                                                    @php
                                                    $image_path = 'storage/app/public/merchant_portfolio/'.@$img->image; 
                                                    @endphp
                                                    @if(file_exists(@$image_path))
                                                        <img id="merchantPic" src="{{ URL::to('storage/app/public/merchant/external_order/photo/'.@$imgs->images) }}" alt="" style="width: 100px;height: 100px;">
                                                    @else
                                                    <!-- <img src="{{ getDefaultImageUrl() }}" alt=""> -->
                                                    <img src="{{ asset('public/frontend/images/default_product.png') }}" alt="">
                                                    @endif
                                                @endforeach
                                            @endif
                                            </span></p>
                                        @endif
                                    </div>
                                    @if(Auth::guard('merchant')->user()->hide_customer_info == 'N' && @$order->order_type == 'I')
                                        <div class="col-md-6">
                                            <h4 class="fultxt" style="margin-bottom:10px;"> @lang('admin_lang.customer_details')</h4>
                                            <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                                @if(@$order->user_id != 0)
                                                {{ @$order->customerDetails->fname }} {{ @$order->customerDetails->lname }}
                                                @else
                                                {{ @$order->fname }} {{ @$order->lname }}
                                                @endif
                                                </span>
                                            </p>
                                            {{-- email --}}
                                            <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                                @if(@$order->user_id != 0)  
                                                {{ @$order->customerDetails->email }}    
                                                @else
                                                {{ @$order->email }}
                                                @endif
                                                </span>
                                            </p>
                                            {{-- phone --}}
                                            <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                                @if(@$order->user_id != 0)  
                                                {{ @$order->customerDetails->phone }}    
                                                @else
                                                {{ @$order->shipping_phone }}
                                                @endif
                                                </span>
                                            </p>
                                            {{-- shipping address --}}
                                            
                                            {{-- start --}}
                                            @if(@$order->order_type == 'I')
                                                @if(@$order->order_type == 'E')    
                                                    @if($order->shipping_country != 134)
                                                    <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                                        @if(@$order->user_id != 0)
                                                        {{ @$order->customerDetails->zipcode }}    
                                                        @else
                                                        {{ @$order->shipping_postal_code }}
                                                        @endif
                                                        </span>
                                                    </p>
                                                    @endif
                                                    <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                                        @if(@$order->user_id != 0)
                                                            {{ @$order->customerDetails->address }}
                                                        @else
                                                            {{ @$order->shipping_more_address }}
                                                        @endif
                                                        </span>
                                                    </p>
                                                @endif
                                            @endif
                                            {{-- end --}}
                                        </div>
                                    @elseif(@$order->order_type == 'E') 
                                        <div class="col-md-6">
                                            <h4 class="fultxt" style="margin-bottom:10px;"> @lang('admin_lang.customer_details')</h4>
                                            <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                                @if(@$order->user_id != 0)
                                                {{ @$order->customerDetails->fname }} {{ @$order->customerDetails->lname }}
                                                @else
                                                {{ @$order->fname }} {{ @$order->lname }}
                                                @endif
                                                </span>
                                            </p>
                                            {{-- email --}}
                                            <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                                @if(@$order->user_id != 0)  
                                                {{ @$order->customerDetails->email }}    
                                                @else
                                                {{ @$order->email }}
                                                @endif
                                                </span>
                                            </p>
                                            {{-- phone --}}
                                            <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                                @if(@$order->user_id != 0)  
                                                {{ @$order->customerDetails->phone }}    
                                                @else
                                                {{ @$order->shipping_phone }}
                                                @endif
                                                </span>
                                            </p>
                                            {{-- shipping address --}}
                                            
                                            {{-- start --}}
                                            @if(@$order->order_type == 'I')
                                                @if(@$order->order_type == 'E')    
                                                    @if($order->shipping_country != 134)
                                                    <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                                        @if(@$order->user_id != 0)
                                                        {{ @$order->customerDetails->zipcode }}    
                                                        @else
                                                        {{ @$order->shipping_postal_code }}
                                                        @endif
                                                        </span>
                                                    </p>
                                                    @endif
                                                    <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                                        @if(@$order->user_id != 0)
                                                            {{ @$order->customerDetails->address }}
                                                        @else
                                                            {{ @$order->shipping_more_address }}
                                                        @endif
                                                        </span>
                                                    </p>
                                                @endif
                                            @endif
                                            {{-- end --}}
                                        </div>
                                    @endif
                                </div>
                                <div class="row fld">
                                    @if(@$order->order_type == 'E')
                                    {{-- Pickup address --}}
                                    @if(@$order->external_order_type == 'S')
                                        <div class="col-md-6">
                                            <h4 class="fultxt" style="margin-bottom:10px;">@lang('merchant_lang.pickup_address')</h4>
                                            <p><span class="titel-span">@lang('admin_lang.country')</span>
                                                <span class="deta-span"><strong>:</strong>
                                                    {{ @$order->getpickupCountryDetails->name }}
                                                </span>
                                            </p>
                                            <p><span class="titel-span">@lang('admin_lang.pickup_phone')</span>
                                                <span class="deta-span"><strong>:</strong>
                                                    {{ @$order->pickup_phone }}
                                                </span>
                                            </p>
                                            @if(@$order->pickup_city)
                                            <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->pickup_city }}
                                                </span>
                                            </p>
                                            @else
                                            <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->getpickupCityDetails->name }}
                                                </span>
                                            </p>
                                            @endif

                                            <p><span class="titel-span">@lang('admin_lang.street')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->pickup_street }}
                                                </span>
                                            </p>
                                            <p><span class="titel-span">@lang('admin_lang.block')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->pickup_block }}
                                                </span>
                                            </p>
                                            <p><span class="titel-span">@lang('admin_lang.building')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->pickup_building }}
                                                </span>
                                            </p>
                                            @if(@$order->shipping_country != 134)
                                            <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->pickup_zip }}
                                                </span>
                                            </p>
                                            @endif
                                            <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                                    {{ @$order->pickup_address }}
                                                </span>
                                            </p>
                                        </div>
                                    @endif
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.shipping_address')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.country')</span>
                                            <span class="deta-span"><strong>:</strong>
                                                {{ @$order->getCountry->name }}
                                            </span>
                                        </p>
                                        
                                        @if(@$order->shipping_city)
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_city }}
                                            </span>
                                        </p>
                                        @else
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCityNameByLanguage->name }}
                                            </span>
                                        </p>
                                        @endif

                                        <p><span class="titel-span">@lang('admin_lang.street')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_street }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.block')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_block }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.building')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_building }}
                                            </span>
                                        </p>
                                        @if(@$order->shipping_country != 134)
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_zip }}
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->shipping_address }}
                                            </span>
                                        </p>
                                    </div>
                                    {{-- driver details --}}
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">Driver Details</h4>
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->driverDetails->fname." ".@$order->orderMasterExtDetails->driverDetails->lname }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->driverDetails->email }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->orderMasterExtDetails->driverDetails->phone }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">Profile picture</span> <span class="deta-span"><strong>:</strong>
                                        @if(@$order->orderMasterExtDetails->driverDetails->image)
                                        @php
                                        $image_path = 'storage/app/public/driver/profile_pics/'.@$order->orderMasterExtDetails->driverDetails->image; 
                                        @endphp
                                        @if(file_exists(@$image_path))
                                        <div class="profile" style="display: block;">
                                            <img id="driverProfilePicture" src="{{ URL::to('storage/app/public/driver/profile_pics/'.@$order->orderMasterExtDetails->driverDetails->image) }}" alt="" style="width: 100px;height: 100px;">
                                        </div>
                                        @endif
                                        @else
                                        <div class="profile" style="display: block;">
                                            <img id="driverProfilePicture" src="{{ getDefaultImageUrl() }}" alt="" style="width: 100px;height: 100px;">
                                        </div>
                                        @endif
                                        </span>
                                        </p>
                                    </div>
                                    @endif
                                    @if(@$order->order_type == 'I')
                                    @if(@$order->user_id != 0 && Auth::guard('merchant')->user()->hide_customer_info == 'N')
                                    {{-- billing address --}}
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.billing_address')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.country')</span>
                                            <span class="deta-span"><strong>:</strong>
                                                {{ @$order->getBillingCountry->name }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_fname }} {{ @$order->billing_lname }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_email }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_phone }}
                                            </span>
                                        </p>
                                        
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            @if(@$order->billing_country == 134)
                                            {{ @$order->getBillingCityNameByLanguage->name }}
                                            @else
                                            {{ @$order->billing_city }}
                                            @endif
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.street')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_street }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.block')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_block }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.building')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_building }}
                                            </span>
                                        </p>
                                        @if(@$order->billingAddress->country != 134)
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->billing_postal_code }}
                                            </span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>
                                                {{ @$order->billing_more_address }}
                                            </span>
                                        </p>
                                    </div>
                                    {{-- shipping address --}}
                                    <div class="col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.shipping_address')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.country')</span>
                                            <span class="deta-span"><strong>:</strong>
                                                {{ @$order->getCountry->name }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.Name') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_fname }} {{ @$order->shipping_lname }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.email') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_email }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.phone') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_phone }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.city') </span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->getCityNameByLanguage->name }}    
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.street')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_street }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.block')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_block }}
                                            </span>
                                        </p>
                                        <p><span class="titel-span">@lang('admin_lang.building')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_building }}
                                            </span>
                                        </p>
                                        @if(@$order->shippingAddress->country != 134)
                                        <p><span class="titel-span">@lang('admin_lang.postal_code')</span> <span class="deta-span"><strong>:</strong>
                                            {{ @$order->shipping_postal_code }}</span>
                                        </p>
                                        @endif
                                        <p><span class="titel-span">@lang('admin_lang.more_address_details')</span> <span class="deta-span"><strong>:</strong>{{ @$order->shipping_more_address }}</span>
                                        </p>
                                    </div>
                                    @endif
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="block table-block mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.order_history')</h4>
                                </div>
                                <div class="table-responsive listBody">
                                    <table id="dataTable1" class="display table table-striped" data-table="data-table">
                                        <thead>
                                        <tr>
                                            @if(@$order->order_type == 'I')
                                            <th>@lang('admin_lang.product')</th>
                                            <th>@lang('admin_lang.product')</th>
                                            <th>@lang('admin_lang.attribute')</th>
                                            <th>@lang('admin_lang.seller')</th>
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight')</th>
                                            <th>@lang('admin_lang.unit_price') {{ getCurrency() }}</th>
                                            <th>@lang('admin_lang.sub_total') {{ getCurrency() }}</th>
                                            <th>@lang('admin_lang.total_discount') {{ getCurrency() }}</th>
                                            <th>@lang('admin_lang.total_1')</th>
                                            {{-- <th>@lang('admin_lang.payment_1')</th> --}}
                                            <th>@lang('admin_lang.drivers')</th>
                                            <th>Product Note</th>
                                            <th>@lang('admin_lang.status_1')</th>
                                            <th>@lang('admin_lang.ratings')</th>
                                            <th>@lang('admin_lang.review_comments')</th>
                                            
                                            <th>@lang('admin_lang.action_1')</th>
                                            @else
                                            <th>@lang('admin_lang.seller')</th>
                                            <th>@lang('admin_lang.qty')</th>
                                            <th>@lang('admin_lang.weight')</th>
                                            <th>@lang('admin_lang.shipping_cst')</th>
                                            <th>@lang('admin_lang.sub_total')</th>
                                            <th>@lang('admin_lang.total_1')</th>
                                            {{-- <th>@lang('admin_lang.payment_1')</th> --}}
                                            <th>@lang('admin_lang.drivers')</th>
                                            <th>@lang('admin_lang.status_1')</th>
                                            <th>@lang('admin_lang.action_1')</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @if(@$order->orderMasterDetails)
                                            @foreach(@$order->orderMasterDetails as $key=>$details)
                                            @if(@$order->order_type == 'I')
                                            @if(@$details->seller_id == @Auth::guard('merchant')->user()->id)
                                            <tr>
                                                <td>
                                                    @if(@$details->productDetails->defaultImage->image)
                                                    @php
                                                    $image_path = 'storage/app/public/products/'.@$detafils->productDetails->defaultImage->image; 
                                                    @endphp
                                                    @if(file_exists(@$image_path))
                                                    <div class="profile" style="display: block;">
                                                        <img id="profilePicture" src="{{ URL::to('storage/app/public/products/'.@$details->productDetails->defaultImage->image) }}" alt="" style="width: 100px;height: 100px;">
                                                    </div>

                                                    @endif
                                                    @else 
                                                        <div class="profile" style="display: block;">
                                                            <img id="profilePicture" src="{{ getDefaultImageUrl() }}" alt="" style="width: 100px;height: 100px;">
                                                        </div>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $details->productDetails->productByLanguage->title }}
                                                </td>
                                                <td>
                                                    @php
                                                    $attr = json_decode($details->variants,true);
                                                    $attributes ="";
                                                    $separator =" ";
                                                    @endphp
                                                    @if(@$attr)
                                                    @foreach(@$attr as $value)
                                                    @foreach(@$value as $key=>$v)
                                                    @if($key == getLanguage()->id)
                                                    @php
                                                    $variant = $v['variant'];
                                                    $variant_value = $v['variant_value'];
                                                    @endphp
                                                    @php
                                                    $attributes = $attributes.$separator.$variant." :".$variant_value;
                                                    @endphp
                                                    @endif
                                                    @endforeach
                                                    @php
                                                    $separator =" , ";
                                                    @endphp
                                                    @endforeach
                                                    @endif
                                                    {{ @$attributes }}
                                                </td>
                                                <td>
                                                    {{ $details->sellerDetails->fname." ".$details->sellerDetails->lname }}
                                                </td>
                                                <td>
                                                    {{ @$details->quantity  }}
                                                </td>
                                                <td>
                                                    {{ @$details->weight  }} Gms
                                                </td>
                                                <td>
                                                    @if(@$details->discounted_price == 0.000) 
                                                    {{ @$details->original_price }} 
                                                    @else 
                                                    <!-- <span style="text-decoration: line-through;"> {{ @$details->original_price }}  </span> -->
                                                    {{ @$details->original_price }}
                                                    @endif
                                                </td>
                                                
                                                <td>
                                                    {{ @$details->sub_total }}
                                                </td>
                                                <td>
                                                    {{ @$details->sub_total - @$details->total }}
                                                </td>
                                                <td>
                                                    {{ @$details->total }}
                                                </td>
                                                {{-- <td>
                                                    @if(@$order->payment_method == 'C')
                                                    @lang('admin_lang.cod')
                                                    @else
                                                     @lang('admin_lang.online')
                                                    @endif
                                                </td> --}}
                                                <td>{{ @$details->driverDetails->fname }} {{ @$details->driverDetails->lname }}</td>
                                                <td>{!!strip_tags(@$details->product_note)!!}</td>
                                                <td>
                                                    @if(@$details->status == 'I')
                                                    Incomplete
                                                    @elseif(@$details->status == 'N')
                                                    New
                                                    @elseif(@$details->status == 'OA')
                                                    @lang('admin_lang.order_accepted')
                                                    @elseif(@$details->status == 'DA' || @$details->status == 'PP' || @$details->status == 'PC')
                                                    @lang('admin_lang.driver_assigned')
                                                    @elseif(@$details->status == 'RP')
                                                    @lang('admin_lang.ready_for_pickup')
                                                    @elseif(@$details->status == 'OP')
                                                    Picked up
                                                    @elseif(@$details->status == 'OD')
                                                    @lang('admin_lang.delivered_1')
                                                    @elseif(@$details->status == 'OC')
                                                    @lang('admin_lang.Cancelled_1')
                                                    @endif
                                                    
                                                </td>
                                                <td>@if(@$details->rate != 0){{ @$details->rate }} @endif</td>
                                                <td>{{ @$details->comment }}</td>
                                                <td>
                                                    
                                                    @if(@$details->status == 'DA')
                                                    <a href="{{ route('merchant.status.change.order',@$details->id) }}" onclick="return confirm('Are you ready for the pickup ? ');"><i class="fa fa-truck" title="Ready for pickup"></i></a>
                                                    @endif 
                                                </td>
                                            </tr>
                                            @endif
                                            @elseif(@$order->order_type == 'E')
                                            <tr>
                                                
                                                <td>
                                                    {{ @$details->sellerDetails->fname }} {{ @$details->sellerDetails->lname }}
                                                </td>
                                                <td>
                                                    {{ @$order->orderMasterDetails->count() }}
                                                </td>
                                                <td>
                                                    {{ @$order->product_total_weight }}
                                                </td>
                                                <td>
                                                    {{ @$order->shipping_price }}
                                                </td>
                                                <td>
                                                    {{ @$order->subtotal }}
                                                </td>
                                                <td>
                                                    {{ @$order->order_total }}
                                                </td>
                                                {{-- <td>
                                                    @if(@$order->payment_method == 'C')
                                                    @lang('admin_lang.cod')
                                                    @else
                                                    @lang('admin_lang.online')
                                                    @endif
                                                </td> --}}
                                                <td>{{ @$details->driverDetails->fname }} {{ @$details->driverDetails->lname }}</td>
                                                <td>
                                                    @if(@$details->status == 'I')
                                                    Incomplete
                                                    @elseif(@$details->status == 'N')
                                                    New
                                                    @elseif(@$details->status == 'OA')
                                                    @lang('admin_lang.order_accepted')
                                                    @elseif(@$details->status == 'DA')
                                                    @lang('admin_lang.driver_assigned')
                                                    @elseif(@$details->status == 'RP')
                                                    @lang('admin_lang.ready_for_pickup')
                                                    @elseif(@$details->status == 'OP')
                                                    Picked up
                                                    @elseif(@$details->status == 'OD')
                                                    @lang('admin_lang.delivered_1')
                                                    @elseif(@$details->status == 'OC')
                                                    @lang('admin_lang.Cancelled_1')
                                                    @endif
                                                </td>
                                                <td></td>
                                            </tr>
                                            @endif
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @if(Config::get('app.locale') == 'en')
    @include('merchant.includes.scripts')
    @elseif(Config::get('app.locale') == 'ar')
    @include('merchant.includes.arabic_scripts')
    @endif
@endsection