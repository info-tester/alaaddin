@extends('merchant.layouts.app')
{{-- @section('title', 'Alaaddin | Merchant | View Order Details') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.merchant') | @lang('admin_lang.view_order_details')
@endsection
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ URL::to('public/merchant/assets/css/chosen.css') }}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    .remove-img {
        width: 18px;
        height: 18px;
        font-size: 13px;
        color: #fff;
        text-align: center;
        background: #0771d4;
        border-radius: 50%;
        line-height: 16px;
        float: right;
        margin-left: 4px;
    }
    .remove-img a {
        color: #fff;
        font-size: 10px;
    }
    .action-opt {
        position: absolute;
        right: 0;
        top: 5px;
        z-index: 1;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
        <!--Header Fixed-->
        <div class="header fixed-header">
            <div class="container-fluid" style="padding: 10px 25px">
                <div class="row">
                    <div class="col-9 col-md-6 d-lg-none">
                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                        <span class="logo">@lang('admin_lang.seller') - </span>
                    </div>
                    <div class="col-lg-8 d-none d-lg-block">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">@lang('admin_lang.Dashboard')</a></li>
                            <li class="breadcrumb-item ">@lang('admin_lang.msgs')</li>
                            <li class="breadcrumb-item active">@lang('admin_lang.view_msgs')</li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="content sm-gutter">
            <div class="container-fluid padding-25 sm-padding-10">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title">
                            <h4>@lang('admin_lang.view_msgs')</h4>
                            <a class="extrnl" href="{{ route('merchant.message.show') }}">@lang('admin_lang.back')</a>
                        </div>
                    </div>

                   <div class="col-md-12">
                        <div class="block form-block mb-4">
                            <form action="#">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <h4 class="fultxt" style="margin-bottom:10px;">@lang('admin_lang.view_msgs_send_by_admin')</h4>
                                        <p><span class="titel-span">@lang('admin_lang.con_id')</span> <span class="deta-span"><strong> : </strong>
                                            <b>{{ @$message->conversation_id }}</b>
                                        </span> </p>
                                        <p><span class="titel-span">@lang('admin_lang.title')</span> <span class="deta-span"><strong> : </strong>
                                            <b>{{@$message->title}}</b>
                                        </span> </p>
                                        <p><span class="titel-span">@lang('admin_lang.msg_mail')</span> <span class="deta-span"><strong>:</strong><b>{!! strip_tags(@$message->messages) !!}</b></span></p>
                                        <p><span class="titel-span">@lang('admin_lang.dte_time')</span> <span class="deta-span"><strong> : </strong>
                                            <b>{{@$message->created_at }}</b>
                                        </span> </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @if(Config::get('app.locale') == 'en')
    @include('merchant.includes.scripts')
    @elseif(Config::get('app.locale') == 'ar')
    @include('merchant.includes.arabic_scripts')
    @endif
@endsection