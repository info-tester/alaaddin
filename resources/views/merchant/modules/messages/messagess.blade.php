@extends('merchant.layouts.app')
{{-- @section('title', 'Alaaddin | Merchant | Manage Order') --}}
@section('title')
{{ config('app.name', 'Alaaddin') }} | @lang('admin_lang.merchant') | Messages
@endsection
@section('links')
{{-- @include('merchant.includes.links') --}}
@if(Config::get('app.locale') == 'en')
@include('merchant.includes.links')
@elseif(Config::get('app.locale') == 'ar')
@include('merchant.includes.arabic_links')
@endif
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ URL::to('public/merchant/assets/css/chosen.css') }}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<style type="text/css">
    .remove-img {
        width: 18px;
        height: 18px;
        font-size: 13px;
        color: #fff;
        text-align: center;
        background: #0771d4;
        border-radius: 50%;
        line-height: 16px;
        float: right;
        margin-left: 4px;
    }
    .remove-img a {
        color: #fff;
        font-size: 10px;
    }
    .action-opt {
        position: absolute;
        right: 0;
        top: 5px;
        z-index: 1;
    }
</style>
@endsection
@section('header')
@include('merchant.includes.header')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--Header Fixed-->
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 10px 25px">
            <div class="row">
                <div class="col-12 col-md-6 d-lg-none">
                    <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    <span class="logo logo1"><img src="{{ asset('public/merchant/assets/images/logo-c.png') }}"></span>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('merchant.dashboard') }}">@lang('merchant_lang.Dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('admin_lang.email_messages')</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h4>@lang('admin_lang.email_messages')
                            <!-- <a class="extrnl" href="{{ route('merchant.add.external.order') }}">@lang('admin_lang.create_external_order')</a> -->
                        </h4>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="block form-block mb-4">
                            <!-- <div class="block-heading">
                                <h5>Default Layout</h5>
                            </div>

                            
                            </div>
                        </div> -->
                        <div class="col-md-12" style="padding:0; ">
                            <div class="block table-block mb-4">
                                <div class="row">
                                    <div class="table-responsive listBody">
                                        <table class="display table table-striped" id="dataTable1">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>@lang('admin_lang.conversation_id')</th>
                                                    <th>@lang('admin_lang.email_title')</th>
                                                    <th>@lang('admin_lang.msgs')</th>
                                                    <th>@lang('merchant_lang.dte_time')</th>
                                                    <th>@lang('admin_lang.actions')</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(@$conversations)
                                                    @foreach(@$conversations as $key=>$message)
                                                        <tr>
                                                            <td>{{ $key+1 }}</td>
                                                            <td>{{ @$message->conversation_id }}</td>
                                                            <td>{{ $message->title }}</td>
                                                            <td>{!! strip_tags($message->messages) !!}</td>
                                                            <td>{{@$message->created_at}}</td>
                                                            <td>
                                                                <a href="{{ route('merchant.view.message.conversation',@$message->id) }}"><i class="fa fa-eye" title="@lang('admin_lang.view')"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>@lang('admin_lang.conversation_id')</th>
                                                    <th>@lang('admin_lang.email_title')</th>
                                                    <th>@lang('admin_lang.msgs')</th>
                                                    <th>@lang('merchant_lang.dte_time')</th>
                                                    <th>@lang('admin_lang.actions')</th>
                                               </tr>
                                           </tfoot>
                                       </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('scripts')
        @if(Config::get('app.locale') == 'en')
        @include('merchant.includes.scripts')
        @elseif(Config::get('app.locale') == 'ar')
        @include('merchant.includes.arabic_scripts')
        @endif
        @endsection