<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Design_Gurus" name="author">
    <meta content="WOW Merchant dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::to('public/frontend/images/fav.png')}}">
    <title>Aswagna | Merchant | Signup</title>
    @if(Config::get('app.locale') == 'en')
    <!--favicon-->
    <link href="{{ asset('public/merchant/assets/images/favicon.ico') }}" rel="shortcut icon">
    <!--Preloader-CSS-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/preloader/preloader.css') }}">
    <!--bootstrap-4-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/css/bootstrap.min.css') }}">
    <!--Custom Scroll-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/customScroll/jquery.mCustomScrollbar.min.css') }}">
    <!--Font Icons-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/simple-line/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/dripicons/dripicons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/eightyshades/eightyshades.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/foundation/foundation-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/metrize/metrize.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/typicons/typicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/weathericons/css/weather-icons.min.css') }}">
    <!--Date-range-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/date-range/daterangepicker.css') }}">
    <!--Drop-Zone-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/dropzone/dropzone.css') }}">
    <!--Full Calendar-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/full-calendar/fullcalendar.min.css') }}">
    <!--Normalize Css-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/css/normalize.css') }}">
    <!--Main Css-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/css/main.css') }}">

    @elseif(Config::get('app.locale') == 'ar')
    <!--favicon-->
    <link href="{{ asset('public/merchant/arabic/assets/images/favicon.ico') }}" rel="shortcut icon">
    <!--Preloader-CSS-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/preloader/preloader.css') }}">
    <!--bootstrap-4-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/css/bootstrap.min.css') }}">
    <!--Custom Scroll-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/customScroll/jquery.mCustomScrollbar.min.css') }}">
    <!--Font Icons-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/simple-line/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/dripicons/dripicons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/eightyshades/eightyshades.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/foundation/foundation-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/metrize/metrize.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/typicons/typicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/weathericons/css/weather-icons.min.css') }}">
    <!--Date-range-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/date-range/daterangepicker.css') }}">
    <!--Drop-Zone-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/dropzone/dropzone.css') }}">
    <!--Full Calendar-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/full-calendar/fullcalendar.min.css') }}">
    <!--Normalize Css-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/css/normalize.css') }}">
    <!--Main Css-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/css/main.css') }}">
    @endif
    <style type="text/css" media="screen">
      .error{
            /*border: solid 1px #fb8282 !important;
            background: #ff000014 !important;*/
            color: #fb8282 !important;
        }  
    </style>
    <style type="text/css">
    label.error{
    border: none !important;
    background-color: #fff !important;
    color: #f00;
    }
    #cityList{
    position: absolute;
    width: 96%;
    /*height: 200px;*/
    z-index: 99;
    }
    #cityList ul{
    background: #fff;
    width: 96%;
    border: solid 1px #eee;
    padding: 0;
    max-height: 200px;
    overflow-y: scroll;
    }
    #cityList ul li{
    list-style: none;
    padding: 5px 15px;
    cursor: pointer;
    border-bottom: solid 1px #eee;
    }
    #cityList ul li:hover{
    background: #3a82c4;
    color: #fff;
    }
</style>
</head>
<body>
    <!---Preloader Starts Here--->
    <div id="ip-container" class="ip-container">
        <header class="ip-header">
            <h1 class="ip-logo text-center"><img class="img-fluid" src="{{ asset('public/merchant/assets/images/logo-c.png') }}" alt="" class="ip-logo text-center"/></h1>
            <div class="ip-loader">
                <svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">
                    <path class="ip-loader-circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id="ip-loader-circle" class="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </header>
    </div>
    <!---Preloader Ends Here--->
    <section style="background: url(../../../images.pexels.com/photos/38519/macbook-laptop-ipad-apple-38519663a.jpg?w=940&amp;h=650&amp;auto=compress&amp;cs=tinysrgb);background-size: cover">
        <div class="height-100-vh bg-primary-trans">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-7 col-lg-5">
                        <div class="register-div">
                            <p class="logo mb-1"><img src="{{ asset('public/merchant/assets/images/logo.png') }}"></p>
                            <p class="mb-4" style="color: #a5b5c5">Create an account.</p>

                            @if ($errors->any())                     
                            <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>                                
                                    {{ $errors->first() }}
                                </strong>
                            </div>
                            @endif
                            <form id="myForm" action="{{ route('merchant.register') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <label>@lang('merchant_lang.FirstName')</label>
                                        <input class="form-control input-lg required" placeholder="First name" type="text" name="fname">
                                    </div>
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <label>@lang('merchant_lang.LastName')</label>
                                        <input class="form-control input-lg required" placeholder="Last name" type="text" name="lname">
                                    </div>
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <label>@lang('merchant_lang.Email')</label>
                                        <input class="form-control input-lg required" placeholder="Enter email address" type="email" id="email" name="email">
                                        <span id="chck_eml_rd" class="chck_eml_rd text-danger"></span>
                                        <span id="chck_eml_grn" class="chck_eml_grn text-success"></span>
                                    </div>
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <label>@lang('merchant_lang.PhoneNumber')</label>
                                        <input class="form-control input-lg required" placeholder="Phone Number" type="text" name="phone" id="phone"onkeypress='validate(event)'>
                                    </div>
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <label>@lang('merchant_lang.CompanyName')</label>
                                        <input class="form-control input-lg required" placeholder="Company Name" type="text" name="company_name" id="company_name">
                                    </div>
                                    <div class="form-group  col-md-6 mmtp">
                                        <div class="file_upload">
                                            @lang('merchant_lang.UploadImage')
                                            <input type="file" class="form-control choose_question" id="imgInp" name="image" accept="*"/>
                                        </div>
                                        <label for="imgInp" generated="true" class="error" style="display: none;"></label>
                                        <span id="imgErr"></span>
                                        <div class="uploaded_ppc" >
                                            <img id="blah" />
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h5 class="fultxt adrs">Address</h5>
                                    </div>
                                    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <label>@lang('merchant_lang.Country')</label>
                                        <select class="custom-select form-control required" id="country" name="country">
                                            <option value="">Select @lang('merchant_lang.Country')</option>
                                            @foreach($country as $cn)
                                            <option value="{{ $cn->id }}" @if($cn->id == 134) selected=""  @elseif($cn->status == 'I') style="display:none;" @endif>{{ @$cn->countryDetailsBylanguage->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12  inside_kuwait_city" style="display: block;">
                                        <label for="kuwait_city_lb" class="col-form-label">@lang('admin_lang.city')(@lang('admin_lang.required'))</label>
                                        <input type="text" class="custom-select form-control required insideKuwCity" name="kuwait_city" id="kuwait_city" placeholder="@lang('admin_lang.city')" value="" autocomplete="off">
                                        <div id="cityList"></div>
                                        <input type="hidden" id="kuwait_city_value_id" name="kuwait_city_value_id" value="">
                                        <input type="hidden" id="kuwait_city_value_name" name="kuwait_city_value_name" value="">
                                        <span class="error_kuwait_city removeText city_id_err text-danger" ></span>
                                    </div>
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 outside_kuwait_city" style="display: none;">
                                        <label>@lang('merchant_lang.City')</label>
                                        <input id="city" type="text" class="form-control required" placeholder="City" name="city">
                                    </div>
                                    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <label>@lang('merchant_lang.State')</label>
                                            {{-- <select class="custom-select form-control required" id="state" name="state">
                                                <option value="">Select @lang('merchant_lang.State')</option>
                                            </select> --}}
                                            <input id="state" type="text" class="form-control required" placeholder="State" name="state">
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label>@lang('merchant_lang.Street')</label>
                                            <input id="address" name="address" type="text" class="form-control required" placeholder="Street">
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label>@lang('merchant_lang.avenue')</label>
                                            <input id="avenue" name="avenue" type="text" class="form-control required" placeholder="Avenue">
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label>@lang('merchant_lang.building_number')</label>
                                            <input id="building_number" name="building_number" type="text" class="form-control required" placeholder="Building Number">
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label>@lang('merchant_lang.Zip')</label>
                                            <input id="zipcode" type="text" class="form-control required" placeholder="Zip" name="zipcode">
                                        </div>

                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label class="from-label">@lang('front_static.location')</label>
                                            <input id="pac-input" type="text" placeholder="@lang('front_static.location')" name="location" class="form-control">
                                            
                                            <input type="hidden" name="lat" id="lat">
                                            <input type="hidden" name="lng" id="lng">
                                        </div>

                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label>@lang('merchant_lang.address_note')</label>
                                            <input id="address_note" type="text" class="form-control required" placeholder="Address Note" name="address_note">
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label>Password</label>
                                            <input class="form-control input-lg required" placeholder="Password" type="password" name="password" id="password">
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label>Confirm Password</label>
                                            <input class="form-control input-lg required" placeholder="Confirm password" type="password" name="password_confirmation" id="confirm_password">
                                        </div>
                                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <label>@lang('admin_lang.payment_method')</label>
                                            <select class="custom-select form-control required" id="payment_method" name="payment_method">
                                            <option value="">Select @lang('admin_lang.payment_method')</option>
                                            <option value="C">@lang('admin_lang.cod')</option>
                                            <option value="O">@lang('admin_lang.online')</option>
                                            <option value="A">@lang('admin_lang.all')</option>
                                        </select>
                                        </div>
                                        <div class="checkbox">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input required" id="terms_and_conditions" name="terms">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">I agree to the Grapes <a href="javascript:void(0);" class="btn-link">Terms and Privacy</a>.</span>
                                            </label>
                                            <label for="terms" generated="true" class="error" style="display: none;"></label>
                                            {{-- <span id="terms" class="chck_eml_rd text-danger"></span> --}}

                                        </div>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <button class="btn btn-primary mt-2" type="submit" id="submit">Sign Up</button>
                                        </div>
                                        <small class="text-muted mt-5 mb-1 d-block"> Already have an account?  <a href="{{ route('merchant.login') }}">Login Now!</a></small>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!---Right Tray--->
        <!--Jquery-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/js/jquery-3.2.1.min.js') }}"></script>
        <!--Bootstrap Js-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/js/popper.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/js/bootstrap.min.js') }}"></script>
        <!--Modernizr Js-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/js/modernizr.custom.js') }}"></script>
        <!--Morphin Search JS-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/morphin-search/classie.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/morphin-search/morphin-search.js') }}"></script>
        <!--Morphin Search JS-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/preloader/pathLoader.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/preloader/preloader-main.js') }}"></script>
        <!--Chart js-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/charts/Chart.min.js') }}"></script>
        <!--Sparkline Chart Js-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/sparkline/jquery.charts-sparkline.js') }}"></script>
        <!--Custom Scroll-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/customScroll/jquery.mCustomScrollbar.min.js') }}"></script>
        <!--Sortable Js-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/sortable2/sortable.min.js') }}"></script>
        <!--DropZone Js-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/dropzone/dropzone.js') }}"></script>
        <!--Date Range JS-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/date-range/moment.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/date-range/daterangepicker.js') }}"></script>
        <!--CK Editor JS-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/ckEditor/ckeditor.js') }}"></script>
        <!--Data-Table JS-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/data-tables/datatables.min.js') }}"></script>
        <!--Editable JS-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/editable/editable.js') }}"></script>
        <!--Full Calendar JS-->
        <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/full-calendar/fullcalendar.min.js') }}"></script>
        <!--- Main JS -->
        <script src="{{ asset('public/merchant/arabic/assets/js/main.js') }}"></script>
        <script src="{{ asset('public/merchant/arabic/assets/js/jquery.validate.js') }}"></script>
        {{-- for fetching state  --}}
        <script>
            $(document).ready(function(){

               $('#country').change(function(){
                 //  if($(this).val() != '')
                 //  {
                 //     var value  = $(this).val();
                 //     var _token = $('input[name="_token"]').val();
                 //     $.ajax({
                 //        url:"{{ route('state.fetch') }}",
                 //        method:"POST",
                 //        data:{value:value, _token:_token},
                 //        success:function(result)
                 //        {
                 //           $('#state').html(result);
                 //       }

                 //   })
                 // }
             });

               $('#country').change(function(){
                  $('#state').val('');
              }); 

           });
       </script>
       <script>
        $(document).ready(function() {
            var terms = false;
                // console.log(terms);
                $("#chck_eml_rd1").html('');
                $("#chck_eml_grn1").html('');

                $('#terms_and_conditions').click(function() {
                    if(!$(this).is(':checked')) {
                        $("#terms").html('Please select terms and conditions.');
                        terms = false;
                    } else {
                        terms = true;
                        $("#terms").html('');
                    }
                });

                $('#email').blur(function(event) {

                    if($('#email').val() != ''){
                        // alert('hi');
                        $("#chck_eml_rd").html('');
                        $("#chck_eml_grn").html('');
                        var $this = $(this);
                        var email = $(this).val();
                        var re =/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                        if(!re.test(email)) {
                            $("#chck_eml_rd").html('Please enter a valid email address');
                            $('#email').val('');
                        } else {
                            var reqData = {
                                '_token': '{{ @csrf_token() }}', 
                                'params': {
                                    'email': email
                                }
                            };
                            if(email != '') {
                                $.ajax({
                                    url: '{{ route('check.email') }}',
                                    type: 'POST',
                                    dataType: 'json',
                                    data: reqData,
                                })
                                .done(function(response) {
                                    if(!!response.error) {
                                        $("#chck_eml_rd").html(response.error.merchant);
                                        $('#email').val('');
                                        $($(this).next().html(''));
                                    } else {
                                        $("#chck_eml_grn").html(response.result.message);
                                    }
                                })
                                .fail(function(error) {
                                    console.log(error);
                                });
                            }
                        }     
                    } else {
                        $("#chck_eml_grn").html('');
                    }  
                });
                $('#kuwait_city').keyup(function () {
        var city = $(this).val();
        // alert(city);
        if (city != '') {
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('fetch.kuwait.city.all.details') }}",
                method: "POST",
                data: {
                    city: city,
                    _token: _token
                },
                success: function (response) {
                    if (response.error) {
                        // alert("error");
                    } 
                    else
                    {
                        // alert("success");
                        var cityHtml = '<ul><li data-id="" class="kuwait_cities" data-nm="">Select City</li>';
                        response.result.cities.forEach(function (item, index) {
                            cityHtml = cityHtml + '<li class="kuwait_cities" data-id="' + item.city_details_by_language.city_id + '" data-nm="' + item.city_details_by_language.name + '">' + item.city_details_by_language.name + '</li>';
                        })
                        cityHtml = cityHtml + '</ul>';

                        $('#cityList').show();
                        $('#cityList').html(cityHtml);
                    }

                }
            });
        }

    });
    $(document).on("keyup", "#kuwait_city", function () {
        $("#k_cityname").val($.trim($("#kuwait_city").val()));
        var value = $.trim($(this).val());
        if (value == "") {
            $("#kuwait_city_value_id").val("");
        }
    });
    $(document).on("blur", "#kuwait_city", function () {
        var id = $.trim($("#kuwait_city_value_id").val());
        var name = $.trim($("#kuwait_city_value_name").val());
        var city = $.trim($("#kuwait_city").val());
        if (city != name) {
            $("#kuwait_city").val("");
            $("#kuwait_city_value_id").val("");
            $("#kuwait_city_value_name").val("");
            $(".city_id_err").text("@lang('admin_lang.please_select_name_city')");
        }else 
        {
            $(".city_id_err").text("");
        }
    });
    $('body').on('click', '.cityChange', function () {
        $('#kuwait_city').val($(this).text());
        $('#cityList').fadeOut();
    });
    $("body").click(function () {
        $("#cityList").fadeOut();
    });
    $(document).on("click", ".kuwait_cities", function (e) {
        var id = $(e.currentTarget).attr("data-id");
        $("#kuwait_city_value_id").val(id);
    });
    $(document).on("click", ".kuwait_cities", function (e) {
        var id = $(e.currentTarget).attr("data-id");
        var name = $(e.currentTarget).attr("data-nm");
        $("#kuwait_city_value_id").val(id);
        $("#kuwait_city_value_name").val(name);

        $("#kuwait_city").val(name);
        $(".city_id_err").text("");
        $("error_kuwait_city").text("");
    });
                $('#country').change(function(){
                    var country = $(this).val();
                    if(country == 134){
                        $(".outside_kuwait_city").hide();
                        $(".inside_kuwait_city").show();
                        $("#city").removeClass("required");
                        $("#kuwait_city").addClass("required");
                    }else{
                        $(".outside_kuwait_city").show();
                        $(".inside_kuwait_city").hide();
                        $("#kuwait_city").removeClass("required");
                        $("#city").addClass("required");
                    }
                });
                $('#myForm').validate({
                    rules: {
                        fname : {
                            required: true,
                        },
                        lname : {
                            required: true,
                        },                    
                        email : {
                            required: true,
                            email:true
                        },
                        phone : {
                            required: true,
                            minlength : 10,
                            maxlength: 15,
                            digits: true
                        },
                        company_name : {
                            required: true,
                        },
                        // image : {
                        //     required: true,
                        // },
                        address : {
                            required: true,
                        },
                        city : {
                            required: true,
                        },
                        kuwait_city : {
                            required: true,
                        },
                        country : {
                            required: true,
                        },
                        state : {
                            required: true,
                        },
                        zipcode : {
                            required: true,
                            // digits: true
                        },
                        address_note : {
                            required: true
                        },
                        building_number : {
                            required: true
                        },
                        address_note     : {
                            required: true
                        },
                        password : {
                            required: true,
                            minlength: 6
                        },
                        terms : {
                            required: true,
                        }, 
                        payment_method : {
                            required: true,
                        },    
                        password_confirmation : {          
                            required: true,
                            equalTo : "#password"
                        }
                    },
                    messages: {
                        fname: {
                            required: '@lang('merchant_lang.this_field_req')'
                        },
                        lname : {
                            required: '@lang('merchant_lang.this_field_req')',
                        },                    
                        email : {
                            required: '@lang('merchant_lang.this_field_req')',
                            email:'@lang('merchant_lang.valid_email')'
                        },
                        phone : {
                            required: '@lang('merchant_lang.this_field_req')',
                            minlength : '@lang('merchant_lang.phone_char')',
                            maxlength: '@lang('merchant_lang.phone_char_max')',
                            digits: '@lang('merchant_lang.only_digit')'
                        },
                        company_name : {
                            required: '@lang('merchant_lang.this_field_req')',
                        },
                        // image : {
                        //     required: '@lang('merchant_lang.this_field_req')',
                        // },
                        address : {
                            required: '@lang('merchant_lang.this_field_req')',
                        },
                        city : {
                            required: '@lang('merchant_lang.this_field_req')',
                        },
                        country : {
                            required: '@lang('merchant_lang.this_field_req')',
                        },
                        state : {
                            required: '@lang('merchant_lang.this_field_req')',
                        },
                        zipcode : {
                            required: '@lang('merchant_lang.this_field_req')',
                            digits: '@lang('merchant_lang.only_digit')'
                        },
                        address_note : {
                            required: '@lang('merchant_lang.this_field_req')'
                        },
                        building_number : {
                            required: '@lang('merchant_lang.this_field_req')'
                        },
                        address_note     : {
                            required: '@lang('merchant_lang.this_field_req')'
                        },
                        password : {
                            required: '@lang('merchant_lang.this_field_req')',
                            minlength: '@lang('merchant_lang.pass_char_list')'
                        },
                        terms : {
                            required: '@lang('merchant_lang.terms_cond')',
                        },   
                        payment_method : {
                            required: '@lang('merchant_lang.this_field_req')',
                        },    
                        password_confirmation : {          
                            required: '@lang('merchant_lang.this_field_req')',
                            equalTo : '@lang('merchant_lang.confirm_pass')'
                        },
                        location: {
                            required: '@lang('merchant_lang.this_field_req')'
                        }
                    },

                    submitHandler: function (form) {
                        // alert();
                        //terms and condition checking

                        if(terms == true) {
                            $("#terms").html('');
                            if($('#imgInp')!= ''){
                                $("#imgErr").html('');
                                form.submit();   
                            }else{
                                $("#imgErr").html('Please select an image');
                                return false;
                            }
                        } else {
                            if(terms == false) {
                                $("#terms").html('Please accept terms and conditions');
                                return false;
                                // toastr.error('Please accept terms and conditions');
                            }
                        }
                    }

                });
});    
</script> 

{{-- For show image --}}
<script>
    var reader = new FileReader();
    reader.onload = function (e) {
        $('#blah').attr('src', e.target.result);
        $('#blah').hide();
        $('#blah').fadeIn(500);
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    });

    

function validate(evt) {
    var theEvent = evt || window.event;
          // Handle paste
          if (theEvent.type === 'paste') {
              key = event.clipboardData.getData('text/plain');
          } else {
          // Handle key press
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
      }
      var regex = /[0-9]/;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>

<script>
function initMap() {
    var input = document.getElementById('pac-input');
    var autocomplete = new google.maps.places.Autocomplete(input);
    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    autocomplete.setTypes(['address']);

    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }
      $('#lat').val(place.geometry.location.lat())
      $('#lng').val(place.geometry.location.lng())
    });
}
$(document).ready(function() {
    $('#pac-input').blur(function() {
        if($(this).val() == '') {
            $('#lat').val('')
            $('#lng').val('')
        }
    })
})
</script>

<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API') }}&libraries=places&callback=initMap" async defer></script>
</body>
</html>