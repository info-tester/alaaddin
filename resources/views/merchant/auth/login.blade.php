<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Design_Gurus" name="author">
    <meta content="WOW Merchant dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::to('public/frontend/images/fav.png')}}">
    <title>Aswagna | Merchant | Login</title>

    @if(Config::get('app.locale') == 'en')
    <!--favicon-->
    <link href="{{ asset('public/merchant/assets/images/favicon.ico') }}" rel="shortcut icon">
    <!--Preloader-CSS-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/preloader/preloader.css') }}">
    <!--bootstrap-4-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/css/bootstrap.min.css') }}">
    <!--Custom Scroll-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/customScroll/jquery.mCustomScrollbar.min.css') }}">
    <!--Font Icons-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/simple-line/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/dripicons/dripicons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/eightyshades/eightyshades.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/foundation/foundation-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/metrize/metrize.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/typicons/typicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/icons/weathericons/css/weather-icons.min.css') }}">
    <!--Date-range-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/date-range/daterangepicker.css') }}">
    <!--Drop-Zone-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/dropzone/dropzone.css') }}">
    <!--Full Calendar-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/plugins/full-calendar/fullcalendar.min.css') }}">
    <!--Normalize Css-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/css/normalize.css') }}">
    <!--Main Css-->
    <link rel="stylesheet" href="{{ asset('public/merchant/assets/css/main.css') }}">
    
    @elseif(Config::get('app.locale') == 'ar')
    <!--favicon-->
    <link href="{{ asset('public/merchant/arabic/assets/images/favicon.ico') }}" rel="shortcut icon">
    <!--Preloader-CSS-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/preloader/preloader.css') }}">
    <!--bootstrap-4-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/css/bootstrap.min.css') }}">
    <!--Custom Scroll-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/customScroll/jquery.mCustomScrollbar.min.css') }}">
    <!--Font Icons-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/simple-line/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/dripicons/dripicons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/eightyshades/eightyshades.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/foundation/foundation-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/metrize/metrize.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/typicons/typicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/icons/weathericons/css/weather-icons.min.css') }}">
    <!--Date-range-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/date-range/daterangepicker.css') }}">
    <!--Drop-Zone-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/dropzone/dropzone.css') }}">
    <!--Full Calendar-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/plugins/full-calendar/fullcalendar.min.css') }}">
    <!--Normalize Css-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/css/normalize.css') }}">
    <!--Main Css-->
    <link rel="stylesheet" href="{{ asset('public/merchant/arabic/assets/css/main.css') }}">

    @endif
    <style type="text/css" media="screen">
      .error{
        border: solid 1px #fb8282 !important;
        background: #ff000014 !important;
    }  
</style>
<body>
    <!---Preloader Starts Here--->
    <div id="ip-container" class="ip-container">
        <header class="ip-header">
            <h1 class="ip-logo text-center"><img class="img-fluid" src="{{ asset('public/merchant/assets/images/logo-c.png') }}" alt="" class="ip-logo text-center"/></h1>
            <div class="ip-loader">
                <svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">
                    <path class="ip-loader-circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id="ip-loader-circle" class="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </header>
    </div>
    <!---Preloader Ends Here--->
    <section style="background: url(../../../images.pexels.com/photos/176851/pexels-photo-176851663a.jpg?w=940&amp;h=650&amp;auto=compress&amp;cs=tinysrgb);background-size: cover">
        <div class="height-100-vh bg-primary-trans">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="login-div">
                            <p class="logo mb-1"><img src="{{ asset('public/merchant/assets/images/logo.png') }}"></p>
                            <p class="mb-4" style="color: #a5b5c5">Sign into your pages account</p>
                            @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible" style="text-align: center;">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul style="margin-bottom: 0; padding-left: 0; ">
                                    @foreach ($errors->all() as $error)
                                    <li style="list-style: none; font-size: 14px; color: #fff;">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <form id="loginForm" action="{{ route('merchant.login') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Login</label>
                                    <input class="form-control required input-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Username" type="email"  name="email" value="{{ old('email') }}" autofocus>
                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control required input-lg{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Credentials" type="password">
                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="checkbox">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Remember me</span>
                                    </label>
                                </div>
                                <button class="btn btn-primary mt-2">Sign In</button>
                                <small class="text-muted mt-5 mb-1 d-block">Don't have an account? <a href="{{route('merchant.register')}}">Register Now!</a></small>
                                <small class="text-muted mb-1 d-block"><a href="{{route('merchant.password.request')}}">Froget Password?</a></small>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!---Right Tray--->
    @if(Config::get('app.locale') == 'en')
    <!--Jquery-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/js/jquery-3.2.1.min.js') }}"></script>
    <!--Bootstrap Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/assets/js/bootstrap.min.js') }}"></script>
    <!--Modernizr Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/js/modernizr.custom.js') }}"></script>
    <!--Morphin Search JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/morphin-search/classie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/morphin-search/morphin-search.js') }}"></script>
    <!--Morphin Search JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/preloader/pathLoader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/preloader/preloader-main.js') }}"></script>
    <!--Chart js-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/charts/Chart.min.js') }}"></script>
    <!--Sparkline Chart Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/sparkline/jquery.charts-sparkline.js') }}"></script>
    <!--Custom Scroll-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/customScroll/jquery.mCustomScrollbar.min.js') }}"></script>
    <!--Sortable Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/sortable2/sortable.min.js') }}"></script>
    <!--DropZone Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/dropzone/dropzone.js') }}"></script>
    <!--Date Range JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/date-range/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/date-range/daterangepicker.js') }}"></script>
    <!--CK Editor JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/ckEditor/ckeditor.js') }}"></script>
    <!--Data-Table JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/data-tables/datatables.min.js') }}"></script>
    <!--Editable JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/editable/editable.js') }}"></script>
    <!--Full Calendar JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/assets/plugins/full-calendar/fullcalendar.min.js') }}"></script>
    <!--- Main JS -->
    <script src="{{ asset('public/merchant/assets/js/main.js') }}"></script>
    <script src="{{ asset('public/merchant/assets/js/jquery.validate.js') }}"></script>
    <script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js"></script>
    @elseif(Config::get('app.locale') == 'ar')
    <!--Jquery-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/js/jquery-3.2.1.min.js') }}"></script>
    <!--Bootstrap Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/js/bootstrap.min.js') }}"></script>
    <!--Modernizr Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/js/modernizr.custom.js') }}"></script>
    <!--Morphin Search JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/morphin-search/classie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/morphin-search/morphin-search.js') }}"></script>
    <!--Morphin Search JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/preloader/pathLoader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/preloader/preloader-main.js') }}"></script>
    <!--Chart js-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/charts/Chart.min.js') }}"></script>
    <!--Sparkline Chart Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/sparkline/jquery.charts-sparkline.js') }}"></script>
    <!--Custom Scroll-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/customScroll/jquery.mCustomScrollbar.min.js') }}"></script>
    <!--Sortable Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/sortable2/sortable.min.js') }}"></script>
    <!--DropZone Js-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/dropzone/dropzone.js') }}"></script>
    <!--Date Range JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/date-range/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/date-range/daterangepicker.js') }}"></script>
    <!--CK Editor JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/ckEditor/ckeditor.js') }}"></script>
    <!--Data-Table JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/data-tables/datatables.min.js') }}"></script>
    <!--Editable JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/editable/editable.js') }}"></script>
    <!--Full Calendar JS-->
    <script type="text/javascript" src="{{ asset('public/merchant/arabic/assets/plugins/full-calendar/fullcalendar.min.js') }}"></script>
    <!--- Main JS -->
    <script src="{{ asset('public/merchant/arabic/assets/js/main.js') }}"></script>
    <script src="{{ asset('public/merchant/arabic/assets/js/jquery.validate.js') }}"></script>
    <script src="https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js"></script>
    @endif
    <script>
        $(document).ready(function(){ 
            $("#loginForm" ).validate({
                errorPlacement: function (error , element) {
                        //toastr:error(error.text());
                    }
                });
        });
    </script>
</body>
</html>