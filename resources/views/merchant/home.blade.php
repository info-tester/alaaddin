@extends('merchant.layouts.app')
@section('title', 'Alaaddin | Merchant | Dashboard')
@section('links')
    @include('merchant.includes.links')
@endsection
@section('header')
    @include('merchant.includes.header')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!--Header Fixed-->
        <div class="header fixed-header">
            <div class="container-fluid" style="padding: 10px 25px">
                <div class="row">
                    <div class="col-9 col-md-6 d-lg-none">
                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                        <span class="logo">WOW - Admin</span>
                    </div>
                    <div class="col-lg-8 d-none d-lg-block">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        </ol>
                    </div>
                    
                </div>
            </div>
        </div>


        <!--Main Content-->
        <div class="content sm-gutter">
            <div class="container-fluid padding-25 sm-padding-10">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title">
                            <h4>Overview</h4>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="block counter-block mb-4">
                            <div class="value">57</div>
                            <div class="trending trending-up">
                                <span>12%</span>
                                <i class="fa fa-caret-up"></i>
                            </div>
                            <p class="label">Products Sold</p>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="block counter-block mb-4">
                            <div class="value">$315</div>
                            <div class="trending trending-down-basic">
                                <span>12%</span>
                                <i class="fa fa-long-arrow-down"></i>
                            </div>
                            <p class="label">Todays Profit</p>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="block counter-block down mb-4">
                            <div class="value">195</div>
                            <div class="trending">
                                <span>12%</span>
                                <i class="fa fa-caret-down"></i>
                            </div>
                            <p class="label">New Visitors</p>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="block counter-block counter-bg-img mb-4" style="background: url({{ asset('public/merchant/assets/images/counter-bg-img.jpg') }});">
                            <div class="fade-color">
                                <div class="value text-white">109</div>
                                <div class="trending trending-up">
                                    <span>12%</span>
                                    <i class="fa fa-caret-up"></i>
                                </div>
                                <p class="label text-white">New Lovers</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-9 col-lg-9">
                        <div class="section-title">
                            <h4>Multi Charts</h4>
                        </div>

                        <div class="block graph-block mb-4">
                            <div class="graph-big-text mb-4">
                                <p class="graph-label">Unique Visitors</p>
                                <h4 class="graph-value">12,537</h4>
                            </div>
                            <div class="graph-pills">
                                <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#two-lines-graph" role="tab" aria-controls="pills-home" aria-expanded="true">Lines 1</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-profile-tab2" data-toggle="pill" href="#lines-graph" role="tab" aria-controls="pills-profile" aria-expanded="true">Line 2</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-profile-tab3" data-toggle="pill" href="#colored-line-graph" role="tab" aria-controls="pills-profile" aria-expanded="true">C-Line 3</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-profile-tab4" data-toggle="pill" href="#bar-graph" role="tab" aria-controls="pills-profile" aria-expanded="true">Bar 1</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-profile-tab5" data-toggle="pill" href="#gradient-bar-graph" role="tab" aria-controls="pills-profile" aria-expanded="true">C-Bar 2</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <canvas id="filledLineChart" height="100"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3 col-lg-3">

                        <div class="section-title">
                            <h4>Donut Chart</h4>
                        </div>

                        <div class="block chart-block mb-4">

                            <div class="doughnut-chart mb-3">
                                <div class="inside-doughnut-chart-label">
                                    <strong>142</strong>
                                    <span>Total Orders</span>
                                </div>
                                <canvas id="doghnutChart" class="chart"></canvas>
                            </div>

                            <div class="chart-legends">
                                <div class="legend-value-w">
                                    <div class="legend-pin" style="background-color: var(--primary-color)"></div>
                                    <div class="legend-value">Processed</div>
                                </div>
                                <div class="legend-value-w">
                                    <div class="legend-pin" style="background-color: var(--light-color)"></div>
                                    <div class="legend-value">Pending</div>
                                </div>
                                <div class="legend-value-w">
                                    <div class="legend-pin" style="background-color: var(--success-color)"></div>
                                    <div class="legend-value">Completed</div>
                                </div>
                                <div class="legend-value-w">
                                    <div class="legend-pin" style="background-color: var(--danger-color)"></div>
                                    <div class="legend-value">Cancelled</div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="block counter-block py-4 mb-4">
                            <div class="row">
                                <div class="col-6">
                                    <h4>$168.90</h4>
                                    <p class="label">This Month</p>
                                </div>
                                <div class="col-6">
                                    <span class="sparklineBarChartPrimary float-right"></span>
                                </div>
                            </div>
                        </div>

                        <div class="block counter-block bg-dark py-4 mb-4">
                            <div class="row">
                                <div class="col-6">
                                    <h4 class="text-white">$168.90</h4>
                                    <p class="label text-white">This Month</p>
                                </div>
                                <div class="col-6">
                                    <span class="sparklineBarChartPrimary float-right"></span>
                                </div>
                            </div>
                        </div>

                        <div class="block counter-block bg-primary py-4 mb-4">
                            <div class="row">
                                <div class="col-6">
                                    <h4 class="text-white">$168.90</h4>
                                    <p class="label text-white">This Month</p>
                                </div>
                                <div class="col-6">
                                    <span class="sparklineBarChartWhite float-right"></span>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="block counter-block mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <h4>$168.90</h4>
                                    <p class="label">This Month</p>
                                </div>
                                <div class="col-12">
                                    <div class="progress mt-2">
                                        <div class="progress-bar" aria-valuemax="100" aria-valuemin="0" aria-valuenow="32" role="progressbar" style="width: 32%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block counter-block bg-dark mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <h4 class="text-white">$168.90</h4>
                                    <p class="label text-white">This Month</p>
                                </div>
                                <div class="col-12">
                                    <div class="progress mt-2">
                                        <div class="progress-bar" aria-valuemax="100" aria-valuemin="0" aria-valuenow="56" role="progressbar" style="width: 56%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block counter-block bg-primary mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <h4 class="text-white">$168.90</h4>
                                    <p class="label text-white">This Month</p>
                                </div>
                                <div class="col-12">
                                    <div class="progress white mt-2">
                                        <div class="progress-bar" aria-valuemax="100" aria-valuemin="0" aria-valuenow="82" role="progressbar" style="width: 82%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="block counter-block mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <h4>$168.90</h4>
                                    <p class="label">This Month</p>
                                </div>
                                <div class="col-12">
                                    <div class="progress mt-2">
                                        <div class="progress-bar bg-success" aria-valuemax="100" aria-valuemin="0" aria-valuenow="32" role="progressbar" style="width: 32%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block counter-block mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <h4>$168.90</h4>
                                    <p class="label">This Month</p>
                                </div>
                                <div class="col-12">
                                    <div class="progress mt-2">
                                        <div class="progress-bar bg-danger" aria-valuemax="100" aria-valuemin="0" aria-valuenow="56" role="progressbar" style="width: 56%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block counter-block mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <h4>$168.90</h4>
                                    <p class="label">This Month</p>
                                </div>
                                <div class="col-12">
                                    <div class="progress mt-2">
                                        <div class="progress-bar bg-warning" aria-valuemax="100" aria-valuemin="0" aria-valuenow="82" role="progressbar" style="width: 82%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 col-lg-6">
                        <div class="block table-block mb-4">
                            <div class="block-heading d-flex align-items-center" style="border:0; padding-bottom: 0;">
                                <h5 class="text-truncate">Order History</h5>
                                <div class="ml-auto w-25">
                                    <select class="custom-select form-rounded form-control input-sm">
                                        <option>Todays</option>
                                        <option>Yesterday</option>
                                        <option>Last Week</option>
                                    </select>
                                </div>
                            </div>
                            <div class="custom-scroll" style="max-height: 250px;">
                                <div class="table-responsive text-no-wrap">
                                    <table class="table">
                                        <tbody class="text-middle">
                                        <tr>
                                            <td class="name">John Mayers</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/1.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/2.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/3.jpg') }}">
                                                <div class="product-img-more">+4</div>
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-warning">pending</span></td>
                                            <td class="price">$190</td>
                                        </tr>
                                        <tr>
                                            <td class="name">Meera</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/4.jpg') }}">
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-success">Success</span></td>
                                            <td class="price">$25</td>
                                        </tr>
                                        <tr>
                                            <td class="name">William J</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/5.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/6.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/7.jpg') }}">
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-warning">pending</span></td>
                                            <td class="price">$350</td>
                                        </tr>
                                        <tr>
                                            <td class="name">Bruno Black</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/8.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/9.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/10.jpg') }}">
                                                <div class="product-img-more">+10</div>
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-danger">Over Due</span></td>
                                            <td class="price">$1999</td>
                                        </tr>
                                        <tr>
                                            <td class="name">mishti</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/11.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/12.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/1.jpg') }}">
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-success">Success</span></td>
                                            <td class="price">$85</td>
                                        </tr>
                                        <tr>
                                            <td class="name">Inayat Sheikh</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/2.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/3.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/4.jpg') }}">
                                                <div class="product-img-more">+2</div>
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-warning">pending</span></td>
                                            <td class="price">$72</td>
                                        </tr>
                                        <tr>
                                            <td class="name">John Mayers</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/5.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/6.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/7.jpg') }}">
                                                <div class="product-img-more">+4</div>
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-warning">pending</span></td>
                                            <td class="price">$190</td>
                                        </tr>
                                        <tr>
                                            <td class="name">Meera</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/8.jpg') }}">
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-success">Success</span></td>
                                            <td class="price">$25</td>
                                        </tr>
                                        <tr>
                                            <td class="name">William J</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/9.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/10.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/11.jpg') }}">
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-warning">pending</span></td>
                                            <td class="price">$350</td>
                                        </tr>
                                        <tr>
                                            <td class="name">Bruno Black</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/12.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/1.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/2.jpg') }}">
                                                <div class="product-img-more">+10</div>
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-danger">Over Due</span></td>
                                            <td class="price">$1999</td>
                                        </tr>
                                        <tr>
                                            <td class="name">mishti</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/3.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/4.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/5.jpg') }}">
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-success">Success</span></td>
                                            <td class="price">$85</td>
                                        </tr>
                                        <tr>
                                            <td class="name">Inayat Sheikh</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/6.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/7.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/8.jpg') }}">
                                                <div class="product-img-more">+2</div>
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-success">Success</span></td>
                                            <td class="price">$72</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12 col-md-6 col-lg-6">
                        <div class="block bg-trans table-block mb-4">
                            <div class="block-heading d-flex align-items-center" style="border:0; padding-bottom: 0;">
                                <h5 class="text-truncate">Order History</h5>
                                <div class="ml-auto w-25">
                                    <select class="custom-select form-rounded form-control input-sm">
                                        <option>Todays</option>
                                        <option>Yesterday</option>
                                        <option>Last Week</option>
                                    </select>
                                </div>
                            </div>
                            <div class="custom-scroll" style="max-height: 250px;">
                                <div class="table-responsive text-no-wrap">
                                    <table class="table">
                                        <tbody class="text-middle">
                                        <tr>
                                            <td class="name">John Mayers</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/1.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/2.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/3.jpg') }}">
                                                <div class="product-img-more">+4</div>
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-warning">pending</span></td>
                                            <td class="price">$190</td>
                                        </tr>
                                        <tr>
                                            <td class="name">Meera</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/4.jpg') }}">
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-success">Success</span></td>
                                            <td class="price">$25</td>
                                        </tr>
                                        <tr>
                                            <td class="name">William J</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/5.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/6.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/7.jpg') }}">
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-warning">pending</span></td>
                                            <td class="price">$350</td>
                                        </tr>
                                        <tr>
                                            <td class="name">Bruno Black</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/8.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/9.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/10.jpg') }}">
                                                <div class="product-img-more">+10</div>
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-danger">Over Due</span></td>
                                            <td class="price">$1999</td>
                                        </tr>
                                        <tr>
                                            <td class="name">mishti</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/11.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/12.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/1.jpg') }}">
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-success">Success</span></td>
                                            <td class="price">$85</td>
                                        </tr>
                                        <tr>
                                            <td class="name">Inayat Sheikh</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/2.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/3.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/4.jpg') }}">
                                                <div class="product-img-more">+2</div>
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-warning">pending</span></td>
                                            <td class="price">$72</td>
                                        </tr>
                                        <tr>
                                            <td class="name">John Mayers</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/5.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/6.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/7.jpg') }}">
                                                <div class="product-img-more">+4</div>
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-warning">pending</span></td>
                                            <td class="price">$190</td>
                                        </tr>
                                        <tr>
                                            <td class="name">Meera</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/8.jpg') }}">
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-success">Success</span></td>
                                            <td class="price">$25</td>
                                        </tr>
                                        <tr>
                                            <td class="name">William J</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/9.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/10.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/11.jpg') }}">
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-warning">pending</span></td>
                                            <td class="price">$350</td>
                                        </tr>
                                        <tr>
                                            <td class="name">Bruno Black</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/12.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/1.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/2.jpg') }}">
                                                <div class="product-img-more">+10</div>
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-danger">Over Due</span></td>
                                            <td class="price">$1999</td>
                                        </tr>
                                        <tr>
                                            <td class="name">mishti</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/3.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/4.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/5.jpg') }}">
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-success">Success</span></td>
                                            <td class="price">$85</td>
                                        </tr>
                                        <tr>
                                            <td class="name">Inayat Sheikh</td>
                                            <td class="product">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/6.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/7.jpg') }}">
                                                <img class="product-img" data-toggle="tooltip" data-title="Product Name" src="{{ asset('public/merchant/assets/images/products/8.jpg') }}">
                                                <div class="product-img-more">+2</div>
                                            </td>
                                            <td class="status"><span class="badge badge-pill bg-success">Success</span></td>
                                            <td class="price">$72</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('merchant.includes.scripts')
@endsection
