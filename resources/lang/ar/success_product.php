<?php
//jayatri
//arbic language
return [
	'-800' => [
		'code' 		=> '-800',
		'message'	=> 'تم اختياره مسبقاً',
		'meaning'	=> 'The combination already exists.-Ar'
	],
	'-801' => [
		'code' 		=> '-801',
		'message'	=> 'تم تغيير حالة المنتج بنجاح',
		'meaning'	=> 'Product status successfully changed.-Ar'
	],
	'-802' => [
		'code' 		=> '-802',
		'message'	=> 'تم حذف المنتج بنجاح',
		'meaning'	=> 'Product deleted successfully.-Ar'
	],
	'-1000' => [
		'code' 		=> '-1000',
		'message'	=> 'تم تحديث المنتج بنجاح',
		'meaning'	=> 'Success!Product updated successfully!-Ar'
	],
	'-4001' => [
		'code' 		=> '-4001',
		'message'	=> 'تم حفظ المنتج بنجاح',
		'meaning'	=> 'Success!Product successfully saved!-Ar'
	],
	'-4002' => [
		'code' 		=> '-4002',
		'message'	=> 'تم بنجاح تفعيل خصم المنتج',
		'meaning'	=> 'Success!Product discount price updated successfully!-Ar'
	],
	'-3001' => [
		'code' 		=> '-4001',
		'message'	=> 'يجب اختيار صورة',
		'meaning'	=> 'Must select an image!-Ar'
	]
];