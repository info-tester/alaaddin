<?php 
return [
	'-32700' => [
		'code' 		=> '-32700',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.'
	],
	'-33034' => [
		'code' 		=> '-33034',
		'message'	=> 'Error!',
		'meaning'	=> 'Error!Your account is inactive by admin!'
	],
	//surojit error added in this file 
	'-32700' => [
		'code' 		=> '-32700',
		'message'	=> 'Parse error',
		'meaning'	=> 'Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.'
	],
	'-33000' => [
		'code' 		=> '-33000',
		'message'	=> 'Email is already exists.',
		'meaning'	=> 'This email you entered is already exists. Please try with another email.'
	],
	'-33001' => [
		'code' 		=> '-33001',
		'message'	=> 'Profile picture has been successfully updated.',
		'meaning'	=> 'Your profile picture has been successfully changed.'
	],
	'-33002' => [
		'code' 		=> '-33002',
		'message'	=> 'This email is already exists. Please verify your mail id.',
		'meaning'	=> 'You are using the mail is already exist in the database and it is unverified.'
	],
	'-33003' => [
		'code' 		=> '-33003',
		'message'	=> 'This email is already exists. Your account is temporarily inactive. Please write to '. env('SUPPORT_EMAIL'),
		'meaning'	=> 'Account is temporarily inactivate by admin. contact our support team.'
	],
	'-33004' => [
		'code' 		=> '-33004',
		'message'	=> 'This email is already exists. Register with another email .',
		'meaning'	=> 'This email is already using an another account.'
	],
	'-33005' => [
		'code' 		=> '-33005',
		'message'	=> 'This account has been deleted from our site. Please contact our support team.',
		'meaning'	=> 'Account deleted by user or admin.'
	],
	'-33006' => [
		'code' 		=> '-33006',
		'message'	=> 'This email is available.',
		'meaning'	=> 'Email is available.'
	],
	'-33007' => [
		'code' 		=> '-33007',
		'message'	=> 'Invalid email format.',
		'meaning'	=> 'The email entered is not valid.'
	],
	'-33008' => [
		'code' 		=> '-33008',
		'message'	=> 'Email is mandatory',
		'meaning'	=> 'Email is mandatory'
	],
	'-33009' => [
		'code' 		=> '-33009',
		'message'	=> 'Wrong login credentials',
		'meaning'	=> 'Email or Password do not match'
	],
	'-33010' => [
		'code' 		=> '-33010',
		'message'	=> 'Could not create token',
		'meaning'	=> 'Token Could not be created'
	],
	'-33017' => [
		'code' 		=> '-33017',
		'message'	=> 'Profile updated successfully',
		'meaning'	=> 'You have updated your profile'
	],
	'-33018' => [
		'code' 		=> '-33018',
		'message'	=> 'No information',
		'meaning'	=> 'The information is not present in our database'
	],
	'-33029' => [
		'code' 		=> '-33029',
		'message'	=> 'Invalid request',
		'meaning'	=> 'Invalid request'
	],
	'-33034' => [
		'code' 		=> '-33034',
		'message'	=> 'Thank you for registering',
		'meaning'	=> 'Thank you for registering.'
	],
	'-33039' => [
		'code' 		=> '-33039',
		'message'	=> 'Wrong OTP verification code',
		'meaning'	=> 'The OTP verification code does not match'
	],
	'-33040' => [
		'code' 		=> '-33040',
		'message'	=> 'Account verified, registration was successful.',
		'meaning'	=> 'Account verified, registration was successful.'
	],
	'-33041' => [
		'code' 		=> '-33041',
		'message'	=> 'No result',
		'meaning'	=> 'Not found.'
	],
	'-33042' => [
		'code' 		=> '-33042',
		'message'	=> 'Password changed correctly',
		'meaning'	=> 'Password changed correctly'
	],
	'-33049' => [
		'code' 		=> '-33049',
		'message'	=> 'Order inserted correctly',
		'meaning'	=> 'Order successfully registered'
	],
	'-33058' => [
		'code' 		=> '-33058',
		'message'	=> 'Profile picture updated.',
		'meaning'	=> 'Profile picture updated.'
	],
	'-33059' => [
		'code' 		=> '-33059',
		'message'	=> 'Profile updated',
		'meaning'	=> 'Profile updated'
	],
	'-33072' => [
		'code' 		=> '-33072',
		'message'	=> 'Password reset successful.',
		'meaning'	=> 'Password reset successful.'
	],
	'-33073' => [
		'code' 		=> '-33073',
		'message'	=> 'Verification successful.',
		'meaning'	=> 'Verification code OK'
	],
	'-33077' => [
		'code' 		=> '-33077',
		'message'	=> 'Your account is not verified',
		'meaning'	=> 'Your account is not verified'
	],
	'-33078' => [
		'code' 		=> '-33078',
		'message'	=> 'Your account has been removed by the administrator',
		'meaning'	=> 'Your account has been removed by the administrator'
	],
	'-33079' => [
		'code' 		=> '-33079',
		'message'	=> 'OTP verification code sent successfully',
		'meaning'	=> 'Check your mailbox'
	],
	'-33080' => [
		'code' 		=> '-33080',
		'message'	=> 'Product added',
		'meaning'	=> 'Product successfully added'
	],
	'-33081' => [
		'code' 		=> '-33081',
		'message'	=> 'No category',
		'meaning'	=> 'No categories found'
    ],
    '-33082' => [
		'code' 		=> '-33082',
		'message'	=> 'Updated product',
		'meaning'	=> 'Updated product'
	],
    '-33083' => [
		'code' 		=> '-33083',
		'message'	=> 'Opening updated',
		'meaning'	=> 'Opening updated successfully'
	],
    '-33084' => [
		'code' 		=> '-33084',
		'message'	=> 'Product status',
		'meaning'	=> 'Product status updated'
	],
    '-33085' => [
		'code' 		=> '-33085',
		'message'	=> 'Token Expired',
		'meaning'	=> 'User session has expired'
	],
	'-33086' => [
		'code' 		=> '-33086',
		'message'	=> 'Login error!',
		'meaning'	=> 'Account temporarily inactive'
	],
	'-5003' => [
		'code' 		=> '-5003',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.-ar'
	],
	'-6000' => [
		'code' 		=> '-6000',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'لا يمكن حذف هذا التصنيف لأن لديه التنصيف لديها تصنيفات فرعية'
	],
	'-5001' => [
		'code' 		=> '-5001',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'حث خطأ'
	],
	'-5002' => [
		'code' 		=> '-5002',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به'
	],
	'-5010' => [
		'code' 		=> '-5010',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به لم يتم تغيير الحالة'
	],
	'-5004' => [
		'code' 		=> '-5004',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به لم يتم تغيير بيانات التصنيف'
	],
	'-5005' => [
		'code' 		=> '-5005',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به لم يتم حذف بيانات التصنيف'
	],
	'-5006' => [
		'code' 		=> '-5006',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به لم يتم حذف بيانات التصنيف'
	],
	'-5007' => [
		'code' 		=> '-5007',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به لم يتم اضافة بيانات المصنّع'
	],
	'-5008' => [
		'code' 		=> '-5008',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به لم يتم حذف بيانات المصنّع'
	],
	'-5009' => [
		'code' 		=> '-5009',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به لم يتم اضافة قيمة خيار المنتج'
	],
	'-5010' => [
		'code' 		=> '-5010',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به قيمة بيانات المنتج لم يتم حذفها'
	],
	'-5011' => [
		'code' 		=> '-5011',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به قيمة خيارات المنتج لم يتم حذفها'
	],
	'-5012' => [
		'code' 		=> '-5012',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به قيمة خيارات المنتج لم يتم تحديثها'
	],
	'-5015' => [
		'code' 		=> '-5015',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به قيمة خيار المنتج لم يتم حذفها'
	],
	'-5016' => [
		'code' 		=> '-5016',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به المستخدم لم يتم إنشاؤه'
	],
	'-5017' => [
		'code' 		=> '-5017',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به تصنيف المنتج لم يتم تحديثه'
	],
	'-5018' => [
		'code' 		=> '-5018',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به تصنيف حالة المنتج لم يتم تحديثه'
	],
	'-5019' => [
		'code' 		=> '-5019',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به حالة المستخدم لم يتم تحديثها'
	],
	'-5020' => [
		'code' 		=> '-5020',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به بيانات المستخدم لم يتم تحديثها'
	],
	'-5021' => [
		'code' 		=> '-5021',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به خيار حالة المنتج لم يتم تحديثه'
	],
	'-5022' => [
		'code' 		=> '-5022',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به البائع لم يتم إنشاؤه'
	],
	'-5023' => [
		'code' 		=> '-5023',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به بيانات البائع لم يتم تحديثها'
	],
	'-5024' => [
		'code' 		=> '-5024',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'بيانات البائع لم يتم حذفها'
	],
	'-5025' => [
		'code' 		=> '-5025',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به الرسالة لم يتم إرسالها'
	],
	'-5026' => [
		'code' 		=> '-5026',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به حالة العميل لم يتم تحديثها'
	],
	'-5027' => [
		'code' 		=> '-5027',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به العميل لم يتم إنشاؤه'
	],
	'-5028' => [
		'code' 		=> '-5028',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به العميل لم يتم تحديثه'
	],
	'-5029' => [
		'code' 		=> '-5029',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به السائق لم يتم إنشاؤه'
	],
	'-5030' => [
		'code' 		=> '-5030',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به بيانات السائق لم يتم تحديثها'
	],
	'-5031' => [
		'code' 		=> '-5031',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به بيانات السائق لم تتم حذفها'
	],
	'-5032' => [
		'code' 		=> '-5032',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به حالة السائق لم يتم تحديثها'
	],
	'-5033' => [
		'code' 		=> '-5033',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به الرسالة لم يتم إرسالها'
	],
	'-5034' => [
		'code' 		=> '-5034',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به هذا البائع قد أضاف عدة منتجات و لا يمكن حذفه'
	],
	'-5035' => [
		'code' 		=> '-5035',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به تكلفة التوصيل لم تتم إضافتها'
	],
	'-5036' => [
		'code' 		=> '-5036',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به تكلفة التوصيل لم يتم تحديثها'
	],
	'-5037' => [
		'code' 		=> '-5037',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به تكلفة التوصيل لم يتم حذفها'
	],
	'-5038' => [
		'code' 		=> '-5038',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به لم يتم إنشاء طلب خارج الموقع'
	],
	'-5039' => [
		'code' 		=> '-5039',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'لا يمكن إضافة المزيد من المنتجات إلى سلة التسوق ، يمكنك إضافة الحد  بند :item عنصر إلى سلة التسوق!'
	],
	'-5040' => [
		'code' 		=> '-5040',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به هذا الطلب لم يتم إلغاؤه'
	],
	'-5041' => [
		'code' 		=> '-5041',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به حالة الطلب لم تتغير'
	],
	'-5042' => [
		'code' 		=> '-5042',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به السحب المطلوب لم يتم بنجاح'
	],
	'-5043' => [
		'code' 		=> '-5043',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به السحب المطلوب لم يتم بنجاح...الحالة المطلوبة لم تتغير'	
	],
	'-5044' => [
		'code' 		=> '-5044',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به السحب المطلوب لم يتم إلغاؤه ...الحالة المطلوبة لم تتغير'
	],
	'-5050' => [
		'code' 		=> '-5050',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'انت والبائع خارج دولة الكويت'
	],
	'-5051' => [
		'code' 		=> '-5051',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به تكلفة التوصيل غير لم يتم تحديثها'	
	],
	'-5052' => [
		'code' 		=> '-5052',
		'message'	=> 'دث خطأ!',
		'meaning'	=> 'دخول غير مصرح به العمولة لم يتم تحديثها'
	],
	'-5055' => [
		'code' 		=> '-5055',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access!Driver is not assigned!-Ar'
	],
	'-5053' => [
		'code' 		=> '-5053',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access!Merchant is not verified!-Ar'
	],
	'-5054' => [
		'code' 		=> '-5054',
		'message'	=> 'Error!',
		'meaning'	=> 'Status of international order is not changed!-Ar'
	],
	'-5055' => [
		'code' 		=> '-5055',
		'message'	=> 'Error!',
		'meaning'	=> 'Product status is not changed!-Ar'
	],
	'-5056' => [
		'code' 		=> '-5056',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access!Driver is not changed!-Ar'
	],
	'-5057' => [
		'code' 		=> '-5057',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access!Your profile is not updated!-Ar'
	],
	'-5058' => [
		'code' 		=> '-5058',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access!Settings of maintenence is not changed!-Ar'
	],
	'-5059' => [
		'code' 		=> '-5059',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access!Country not added!-Ar'
	],
	'-5060' => [
		'code' 		=> '-5060',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access!Country not updated!-Ar'
	],
	'-5061' => [
		'code' 		=> '5061',
		'message'	=> 'Error!',
		'meaning'	=> 'Unauthorized access!Country not deleted!-Ar'
	],
	'-5062' => [
		'code' 		=> '5062',
		'message'	=> 'Error!',
		'meaning'	=> 'Error!Please add stock for this items!Stock is not enough & will get nagative!-Ar'
	],
	'-5063' => [
		'code' 		=> '5063',
		'message'	=> 'Error!',
		'meaning'	=> 'Error!Password is not changed!-Ar'
	],
	'-5064' => [
		'code' 		=> '5064',
		'message'	=> 'Error!',
		'meaning'	=> 'Error!Already paid by online!Payment method is not changed!So you cannot change online to cash on delivery for internal order if it is already paid by online!'
	],
	'-5065' => [
		'code' 		=> '5065',
		'message'	=> 'Error!',
		'meaning'	=> 'Error!Many products are asociated with this category and subcategory!So if you delete all the products associated with this category and subcategory then you can add new options for this category and subcategory!'
	],
	'-5066' => [
		'code' 		=> '-5066',
		'message'	=> 'Error!',
		'meaning'	=> 'Error!Note for order is not updated!'
	],
	'-5067' => [
		'code' 		=> '-5067',
		'message'	=> 'Error!',
		'meaning'	=> 'Error!Your account is deleted by admin!'
	],
	'-5068' => [
		'code' 		=> '-5068',
		'message'	=> 'Error!',
		'meaning'	=> 'Error!Your account is inactive by admin!'
	],
	'-5070' => [
		'code' 		=> '-5070',
		'message'	=> 'Logout error!',
		'meaning'	=> 'Sorry, the driver cannot be logged out'
	],
	'-5071' => [
		'code' 		=> '-5071',
		'message'	=> 'Error!',
		'meaning'	=> 'Password reset link is not sent!Something went wrong!'
	],
	'-5072' => [
		'code' 		=> '-5072',
		'message'	=> 'Error!',
		'meaning'	=> 'Pasword is not changed!'
	],
	'-5073' => [
		'code' 		=> '-5073',
		'message'	=> 'Error!',
		'meaning'	=> 'Removed item from wishlist is failed!'
	],
	'-5074' => [
		'code' 		=> '-5074',
		'message'	=> 'Error!',
		'meaning'	=> 'Address removed successfully!'
	],
	'-5075' => [
		'code' 		=> '-5075',
		'message'	=> 'Error!',
		'meaning'	=> 'Remove from favourite successfully!'
	],
	'-5076' => [
		'code' 		=> '-5076',
		'message'	=> 'Error!',
		'meaning'	=> 'Product already added as favourite and added in wishlist!'
	],
	'-5077' => [
		'code' 		=> '-5077',
		'message'	=> 'Error!',
		'meaning'	=> 'Please login to add product in your wishlist!'
	],
	'-5078' => [
		'code' 		=> '-5078',
		'message'	=> 'Error!',
		'meaning'	=> 'Another driver is already grab this order!'
	],

	# faq cat
	'-5079' => [
		'code' 		=> '-5079',
		'message'	=> 'Error!',
		'meaning'	=> 'FAQ category cannot be deleted. This category has some FAQ, delete them and try again.'
	],

	'-5080' => [
		'code' 		=> '-5080',
		'message'	=> 'Error!',
		'meaning'	=> 'حدث خطأ'
	],
	'-5081' => [
		'code' 		=> '-5081',
		'message'	=> 'Error!',
		'meaning'	=> 'حدث خطأ'
	],

	'-5082' => [
		'code' 		=> '-5082',
		'message'	=> 'Error!',
		'meaning'	=> 'حدث خطأ'
	],

	'-5083' => [
		'code' 		=> '-5083',
		'message'	=> 'Error!',
		'meaning'	=> 'حدث خطأ'
	],

	'-5084' => [
		'code' 		=> '-5084',
		'message'	=> 'Error!',
		'meaning'	=> 'حدث خطأ'
	],

	'-5085' => [
		'code' 		=> '-5085',
		'message'	=> 'Error!',
		'meaning'	=> 'حدث خطأ'
	],
	'-5086' => [
		'code' 		=> '-5086',
		'message'	=> 'Error!',
		'meaning'	=> 'المنطثة موجودة مسبقا'
	],

	'-5087' => [
		'code' 		=> '-5087',
		'message'	=> 'Error!',
		'meaning'	=> 'حدث خطأ'
	],
	'-5088' => [
		'code' 		=> '-5088',
		'message'	=> 'Error!',
		'meaning'	=> 'التوقيت موجود مسبقا'
	],
	'-5089' => [
		'code' 		=> '-5089',
		'message'	=> 'Error!',
		'meaning'	=> 'حدث خطأ'
	],

	'-5090' => [
		'code' 		=> '-5090',
		'message'	=> 'Error!',
		'meaning'	=> 'حدث خطأ'
	],
	'-5091' => [
		'code' 		=> '-5091',
		'message'	=> 'Error!',
		'meaning'	=> 'حدث خطأ'
	],
	'-5092' => [
		'code' 		=> '-5092',
		'message'	=> 'Error!',
		'meaning'	=> 'Please select an address from address book or enter new address'
	],
	'-5093' => [
		'code' 		=> '-5093',
		'message'	=> 'Error!',
		'meaning'	=> 'Product price can not be 0'
	],
	'-5094' => [
		'code' 		=> '-5094',
		'message'	=> 'Error!',
		'meaning'	=> 'Product Price can not be less than Discounted Price'
	],
];