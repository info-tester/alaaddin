<?php
//surajit
//english language
return [
	'-700' => [
			'code' 		=> '-700',
			'message'	=> 'Success!',
			'meaning'	=> 'Profile successfully updated!'
		],
	'-701' => [
			'code' 		=> '-701',
			'message'	=> 'Error!',
			'meaning'	=> 'Password not changed, You have entered an incorrect old password!'
		],
	'-702' => [
			'code' 		=> '-702',
			'message'	=> 'Success!',
			'meaning'	=> 'Address Book successfully added!'
		],
	'-703' => [
			'code' 		=> '-703',
			'message'	=> 'Error!',
			'meaning'	=> 'Unauthorized access!'
		],
	'-704' => [
			'code' 		=> '-704',
			'message'	=> 'Success!',
			'meaning'	=> 'Address Book successfully updated!'
		],
	'-705' => [
			'code' 		=> '-705',
			'message'	=> 'Success!',
			'meaning'	=> 'Set default address successfully!'
		],
	'-707' => [
			'code' 		=> '-707',
			'message'	=> 'Success!',
			'meaning'	=> 'Mail successfully sent!'
		],
	'-708' => [
			'code' 		=> '-708',
			'message'	=> 'Error!',
			'meaning'	=> 'Please remove the not available product before place order!'
		],
	'-709' => [
			'code' 		=> '-709',
			'message'	=> 'Error!',
			'meaning'	=> 'You and merchant both are outside of Kuwait. Please remove the products of following merchants : '
		],
	'-710' => [
			'code' 		=> '-710',
			'message'	=> 'Error!',
			'meaning'	=> 'International order is not available for following merchants : '
		],
	'-711' => [
			'code' 		=> '-711',
			'message'	=> 'Error!',
			'meaning'	=> 'International order is not available. '
		],
	'-722' => [
			'code' 		=> '-722',
			'message'	=> 'Error!',
			'meaning'	=> 'City name does not exist.'
		],
	'-723' => [
		'code' 		=> '-723',
		'message'	=> 'Error!',
		'meaning'	=> 'International order is not available for following merchants : :mrs'
	],
	'-800' => [
		'code' 		=> '-800',
		'message'	=> 'Success!',
		'meaning'	=> 'You have successfully placed the order'
	],
	'-900' => [
		'code' 		=> '-900',
		'message'	=> 'error!',
		'meaning'	=> 'Payment failed !'
	],
];