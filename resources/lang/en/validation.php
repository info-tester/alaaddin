<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_equals' => 'The :attribute must be a date equal to :date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute must be a valid email address.',
    'exists' => 'The selected :attribute is invalid.',
    'file' => 'The :attribute must be a file.',
    'filled' => 'The :attribute field must have a value.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values',
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],
    //custome validation for javacsript
    'required' => 'This field is required',
    'image_extension_type'                  => 'Please upload image with extension jpg/jpeg/png',
    'email_id_already_exist'                => 'Email-id already exist',
    'please_provide_valid_email_id'         => 'Please provide a valid email-id',
    'phone_number_already_exist'            => 'Phone number already exist',
    'first_name_cannot_be_left_blank'       => 'First name cannot be left blank',
    'last_name_cannot_be_left_blank'        => 'Last name cannot be left blank',
    'password_length_should_be'             => 'Please provide valid password with minimum length should be 8.',
    'password_did_not_match'                => 'Password did not match',
    // 'please_provide_valid_email_id'         => 'Please provide valid email-id',
    'please_provide_valid_phone_number'     => 'Please provide valid phone number',
    'please_upload_your_profile_photo_with_extension_type_jpg_jpeg_png'         => 'Please upload your profile photo with extension type .jpg/.jpeg/.png',
    'error_for_subadmin_permission'         => 'Please select atleast one checkbox for sidebar(menu) access permission.',
    'check_atleast_one_sidebar'             => 'Please check atleast one sidebar',
    'confirm_password'                      => 'Password doesnot match!',
    'error_zipcode'                         => 'Zip code should be numeric value!',
    'error_phone'                           => 'Phone number should be numeric value!',
    'error_phn'                             => 'Please provide a valid phone number & should be in digits only!',
    'error_zip'                             => 'Please provide a valid zipcode & should be in digits only!',
    'error_zip'                             => 'Please provide a valid zipcode & should be in digits only!',
    'title_error'                           => "Title of mail cannot be left blank",
    'message_error'                         => "Message of mail cannot be left blank",
    'msg_snt'                               => "Message has been sent successfully!",
    'error_phn_valid'                       => "Please provide valid phone number![Ex:123456789015]!",
    'error_commission_valid'                => "Please provide commision in % with 2 decimal places![Ex: 80.00]!",
    'com_percentage'                        => "Commission must be lesser than 100 with 2 decimal places! [Ex: 80.15]",
    'shippingcost_error'                    => "Please provide shipping cost with 3 decimal places![ex: 58936.568]",
    'zip_error'                             => "Please provide zipcode in digits only!",
    'error_weight'                          => "Please provide weight with 3 decimal places![Ex: 5.894]",
    'error_internal_order_rate'             => "Please provide internal order rate with 3 decimal places![Ex: 5.894]",
    'error_external_order_rate'             => "Please provide external order rate with 3 decimal places![Ex: 5.894]",
    'error_balence'                         => "Please provide valid amount which should be equal or less than balence!",
    'from_time_invalid'                     => "From time isn't valid!",
    'to_time_invalid'                       => "To time isn't valid!",
    'product_approved_success'              => "Product approved successfully!",
    'error_withdrawl_request_msg'           => "You have already applied for withdrawl!You can request once the previous one is cleared!",
    'please_upload_profile_picture_with'    => "Please upload profile picture with extension jpg,jpeg,png",
    'please_upload_img_with'                => "Please upload image with extension jpg/jpeg/png!",
    'msg_not_sent'                          => "Unauthorized access!Message has not  been sent!",
    'please_provide_valid_email'            => "Please provide a valid email-id",
    'upload_img_extension_jpg_jpeg_png'     => "Please upload image with extension jpg/jpeg/png only!",
    'please_provide_comment'                => "Please provide comment!",
    // 'email_id_already'                            => "Email-id already exist!",
  
    

];
