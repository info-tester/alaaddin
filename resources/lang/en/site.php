<?php

return [
	'merchant_new_order' => "You got a new order :orderNoin Alaaddin.co website. Please prepare it as fast as possible. \nThank you for your collaboration.\n\n📞 22213707 \n 📧 Info@aswagna.co",

	'order_picked_up' => "Your order :orderNo is fulfilled and it will be delivered as soon as possible.\n It is our pleasure to serve you.\n\n
	لقد تم تجميع طلبكم  :orderNo من موقع اسواقنا بنجاح .. 👍🏼
🚗💨 سوف يتم توصيل طلبكم بأسرع وقت ممكن  .. 
سعيدين بخدمتكم",
	
	'order_delivered' => "Your order :orderNo is delivered. It is our pleasure to serve you.\n\n You can shop again using our website \n ".env('APP_URL')."\n\nOr our apps\nAndroid (Play Store): ".env('ANDROID_LINK')."\nIOS (App Store):  ".env('IOS_LINK')."\n\n
	لقد تم توصيل طلبكم  :orderNoبنجاح .. 
سعيدين لخدمتكم .. \n
Driver Group Company  ... 🚙
 الان يمكنكم  اقتناء كل ما تحتاجونه في سلة واحده 
📦 🛍️ ".env('APP_URL')." من خلال موقعنا 
".env('ANDROID_LINK').":ابلكيشن اندرويد 
".env('IOS_LINK').":ابلكيشن اب ستور "
];