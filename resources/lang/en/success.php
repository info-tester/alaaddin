<?php
//jayatri
//english language
return [
	'-800' => [
		'code' 		=> '-800',
		'message'	=> 'Success!',
		'meaning'	=> 'Product category deleted successfully!'
	],
	'-1000' => [
		'code' 		=> '-1000',
		'message'	=> 'Success!',
		'meaning'	=> 'Product category added successfully!'
	],
	'-4001' => [
		'code' 		=> '-4001',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Subcategory added successfully!'
	],
	'-4000' => [
		'code' 		=> '-4000',
		'message'	=> 'Success!',
		'meaning'	=> 'Product category deleted successfully!'
	],
	'-4004' => [
		'code' 		=> '-4004',
		'message'	=> 'Success!',
		'meaning'	=> 'Product subcategory deleted successfully!'
	],
	'-4002' => [
		'code' 		=> '-4002',
		'message'	=> 'Success!',
		'meaning'	=> 'Category updated successfully!'
	],
	'-4003' => [
		'code' 		=> '-4003',
		'message'	=> 'Success!',
		'meaning'	=> 'Status changed successfully!'
	],
	'-4006' => [
		'code' 		=> '-4006',
		'message'	=> 'Success!',
		'meaning'	=> 'Manufacturer added successfully!'
	],
	'-4007' => [
		'code' 		=> '-4007',
		'message'	=> 'Success!',
		'meaning'	=> 'Manufacturer details updated successfully!'
	],
	'-4008' => [
		'code' 		=> '-4008',
		'message'	=> 'Success!',
		'meaning'	=> 'Manufacturer details deleted successfully!'
	],
	'-4009' => [
		'code' 		=> '-4009',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Option details created successfully!'
	],
	'-4011' => [
		'code' 		=> '-4011',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Option details updated successfully!'
	],
	'-4010' => [
		'code' 		=> '-4010',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Option values created successfully!'
	],
	'-4012' => [
		'code' 		=> '-4012',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Option details deleted successfully!'
	],
	'-4014' => [
		'code' 		=> '-4014',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Option values updated successfully!'
	],
	'-4018' => [
		'code' 		=> '-4018',
		'message'	=> 'Success!',
		'meaning'	=> 'Product Option values deleted successfully!'
	],
	'-4015' => [
		'code' 		=> '-4015',
		'message'	=> 'Success!',
		'meaning'	=> 'Sub-Admin created successfully!Details has been sent to mail!'
	],
	'-4016' => [
		'code' 		=> '-4016',
		'message'	=> 'Success!',
		'meaning'	=> 'Product category updated successfully!'
	],
	'-4017' => [
		'code' 		=> '-4017',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of product category updated successfully!'
	],
	'-4019' => [
		'code' 		=> '-4019',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of subadmin updated successfully!'
	],
	'-4020' => [
		'code' 		=> '-4020',
		'message'	=> 'Success!',
		'meaning'	=> 'Details of sub-admin updated successfully!'
	],
	'-4021' => [
		'code' 		=> '-4021',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of product option updated successfully!'
	],
	'-4022' => [
		'code' 		=> '-4022',
		'message'	=> 'Success!',
		'meaning'	=> 'Merchant created successfully!Merchant details sent to the email-id!-ar'
	],
	'-4023' => [
		'code' 		=> '-4023',
		'message'	=> 'Success!',
		'meaning'	=> 'Merchant details updated successfully!'
	],
	'-4024' => [
		'code' 		=> '-4024',
		'message'	=> 'Success!',
		'meaning'	=> 'Merchant status has been changed successfully!'
	],
	'-4025' => [
		'code' 		=> '-4025',
		'message'	=> 'Success!',
		'meaning'	=> 'Merchant approved successfully!'
	],
	'-4026' => [
		'code' 		=> '-4026',
		'message'	=> 'Success!',
		'meaning'	=> 'Merchant deleted successfully!'
	],
	'-4027' => [
		'code' 		=> '-4027',
		'message'	=> 'Success!',
		'meaning'	=> 'Message has been sent successfully!'
	],
	'-4028' => [
		'code' 		=> '-4028',
		'message'	=> 'Success!',
		'meaning'	=> 'Customer deleted successfully!'
	],
	'-4029' => [
		'code' 		=> '-4029',
		'message'	=> 'Success!',
		'meaning'	=> 'Customer created successfully!'
	],
	'-4030' => [
		'code' 		=> '-4030',
		'message'	=> 'Success!',
		'meaning'	=> 'Customer updated successfully!'
	],
	'-4031' => [
		'code' 		=> '-4031',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of customer updated successfully!'
	],
	'-4032' => [
		'code' 		=> '-4032',
		'message'	=> 'Success!',
		'meaning'	=> 'Driver created successfully!Login details has been sent to registered email-id!'
	],
	'-4033' => [
		'code' 		=> '-4033',
		'message'	=> 'Success!',
		'meaning'	=> 'Driver details updated successfully!'
	],
	'-4034' => [
		'code' 		=> '-4034',
		'message'	=> 'Success!',
		'meaning'	=> 'Driver details deleted successfully!'
	],
	'-4035' => [
		'code' 		=> '-4035',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of driver updated successfully!'
	],
	'-4036' => [
		'code' 		=> '-4036',
		'message'	=> 'Success!',
		'meaning'	=> 'Message has been sent successfully!'
	],
	'-4037' => [
		'code' 		=> '-4037',
		'message'	=> 'Success!',
		'meaning'	=> 'Driver details updated successfully!Verification mail of your email-id has been sent!'
	],
	'-4038' => [
		'code' 		=> '-4038',
		'message'	=> 'Success!',
		'meaning'	=> 'Shipping cost is added successfully!'
	],
	'-4039' => [
		'code' 		=> '-4039',
		'message'	=> 'Success!',
		'meaning'	=> 'Shipping cost is updated successfully!'
	],
	'-4040' => [
		'code' 		=> '-4040',
		'message'	=> 'Success!',
		'meaning'	=> 'Shipping cost is deleted successfully!'
	],
	'-4041' => [
		'code' 		=> '-4041',
		'message'	=> 'Success!',
		'meaning'	=> 'Settings for shipping cost is updated successfully!'
	],
	'-4042' => [
		'code' 		=> '-4042',
		'message'	=> 'Success!',
		'meaning'	=> 'External order is created successfully!'
	],
	'-4043' => [
		'code' 		=> '-4043',
		'message'	=> 'Success!',
		'meaning'	=> 'Order has been cancelled successfully!'
	],
	'-4044' => [
		'code' 		=> '-4044',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of order has been changed!'
	],
	'-4045' => [
		'code' 		=> '-4045',
		'message'	=> 'Success!',
		'meaning'	=> 'Withdrawal request successfully submitted, You will be notified via email when admin accept or reject your request!'
	],
	'-4046' => [
		'code' 		=> '-4046',
		'message'	=> 'Success!',
		'meaning'	=> 'Withdrawal request successfully completed!'
	],
	'-4047' => [
		'code' 		=> '-4047',
		'message'	=> 'Success!',
		'meaning'	=> 'Withdrawal request successfully cancelled!'
	],
	'-4048' => [
		'code' 		=> '-4048',
		'message'	=> 'Success!',
		'meaning'	=> 'Loyalty point successfully updated!'
	],
	'-4049' => [
		'code' 		=> '-4049',
		'message'	=> 'Success!',
		'meaning'	=> 'City created successfully!'
	],
	'-4050' => [
		'code' 		=> '-4050',
		'message'	=> 'Success!',
		'meaning'	=> 'City updated successfully!'
	],
	'-4051' => [
		'code' 		=> '-4051',
		'message'	=> 'Success!',
		'meaning'	=> 'City deleted successfully!'
	],
	'-4052' => [
		'code' 		=> '-4052',
		'message'	=> 'Success!',
		'meaning'	=> 'External order details updated successfully!'
	],
	'-4053' => [
		'code' 		=> '-4053',
		'message'	=> 'Success!',
		'meaning'	=> 'Shipping cost updated successfully!'
	],
	'-4054' => [
		'code' 		=> '-4054',
		'message'	=> 'Success!',
		'meaning'	=> 'Commission updated successfully!'
	],
	'-4055' => [
		'code' 		=> '-4055',
		'message'	=> 'Success!',
		'meaning'	=> 'Driver assigned successfully!'
	],
	'-4056' => [
		'code' 		=> '-4056',
		'message'	=> 'Success!',
		'meaning'	=> 'Merchant verified successfully!Now merchant can login!'
	],
	'-4057' => [
		'code' 		=> '-4057',
		'message'	=> 'Success!',
		'meaning'	=> 'Status of international order is successfully changed!'
	],
	'-4058' => [
		'code' 		=> '-4058',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Driver is changed successfully!'
	],
	'-4059' => [
		'code' 		=> '-4059',
		'message'	=> 'Success!',
		'meaning'	=> 'Your profile has been successfully updated!'
	],
	'-4060' => [
		'code' 		=> '-4060',
		'message'	=> 'Success!',
		'meaning'	=> 'Configuration is changed successfully!'
	],
	'-4061' => [
		'code' 		=> '-4061',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Country added successfully!'
	],
	'-4062' => [
		'code' 		=> '-4061',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Country updated successfully!'
	],
	'-4063' => [
		'code' 		=> '-4061',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Country deleted successfully!'
	],
	'-4064' => [
		'code' 		=> '-4064',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Password changed successfully!'
	],
	'-4065' => [
		'code' 		=> '-4065',
		'message'	=> 'Success!',
		'meaning'	=> 'Success ! Payment method changed successfully!'
	],
	'-4066' => [
		'code' 		=> '-4066',
		'message'	=> 'Success!',
		'meaning'	=> 'Success !Notes for order updated successfully!'
	],
	'-4067' => [
		'code' 		=> '-4067',
		'message'	=> 'Success!',
		'meaning'	=> 'Successfully removed from cart!'
	],
	'-4068' => [
		'code' 		=> '-4068',
		'message'	=> 'Success!',
		'meaning'	=> 'Faq successfully updated!'
	],
	'-4069' => [
		'code' 		=> '-4069',
		'message'	=> 'Success!',
		'meaning'	=> 'Faq deleted successfully!'
	],
	'-4070' => [
		'code' 		=> '-4070',
		'message'	=> 'Success!',
		'meaning'	=> 'Faq added successfully!'
	],
	'-4071' => [
		'code' 		=> '-4071',
		'message'	=> 'Success!',
		'meaning'	=> 'Content updated successfully!'
	],
	'-4072' => [
		'code' 		=> '-4072',
		'message'	=> 'Success!',
		'meaning'	=> 'Success!Driver logged out successfully!'
	],
	'-4073' => [
		'code' 		=> '-4073',
		'message'	=> 'Success!',
		'meaning'	=> 'Password reset link is sent to your email id!'
	],
	'-4075' => [
		'code' 		=> '-4075',
		'message'	=> 'Success!',
		'meaning'	=> 'Otp is sent to your email id for reset password!'
	],
	'-4074' => [
		'code' 		=> '-4074',
		'message'	=> 'Success!',
		'meaning'	=> 'Otp has been sent!Please check!'
	],
	'-4076' => [
		'code' 		=> '-4076',
		'message'	=> 'Success!',
		'meaning'	=> 'Your message has been sent successfully!We will contact you soon!'
	],
	'-4077' => [
		'code' 		=> '-4077',
		'message'	=> 'Success!',
		'meaning'	=> 'Address removed successfully!'
	],
	'-4078' => [
		'code' 		=> '-4078',
		'message'	=> 'Success!',
		'meaning'	=> 'Address added successfully in your address book!'
	],
	'-4079' => [
		'code' 		=> '-4079',
		'message'	=> 'Success!',
		'meaning'	=> 'Add to favourite successfully!'
	],
	'-4080' => [
		'code' 		=> '-4080',
		'message'	=> 'Success!',
		'meaning'	=> 'Remove from favourite successfully!'
	],
	'-4081' => [
		'code' 		=> '-4081',
		'message'	=> 'Success!',
		'meaning'	=> 'Push notification sent successfully!'
	],
	'-4082' => [
		'code' 		=> '-4082',
		'message'	=> 'Success!',
		'meaning'	=> 'Notification will sent!'
	],
	'-4083' => [
		'code' 		=> '-4083',
		'message'	=> 'Success!',
		'meaning'	=> 'Notification sent successfully!'
	],
	'-4084' => [
		'code' 		=> '-4084',
		'message'	=> 'Success!',
		'meaning'	=> 'Review posted successfully!'
	],
	'-4085' => [
		'code' 		=> '-4085',
		'message'	=> 'Success!',
		'meaning'	=> 'Review deleted successfully!'
	],
	'-4086' => [
		'code' 		=> '-4086',
		'message'	=> 'Success!',
		'meaning'	=> 'FAQ category saved successfully!'
	],
	'-4087' => [
		'code' 		=> '-4087',
		'message'	=> 'Success!',
		'meaning'	=> 'FAQ category deleted successfully!'
	],
	'-4088' => [
		'code' 		=> '-4088',
		'message'	=> 'Success!',
		'meaning'	=> 'Customer verification successful.'
	],
	'-4089' => [
		'code' 		=> '-4089',
		'message'	=> 'Success!',
		'meaning'	=> 'App settings successfully saved.'
	],

	'-4090' => [
		'code' 		=> '-4090',
		'message'	=> 'Success!',
		'meaning'	=> 'Coupon added successfully'
	],

	'-4091' => [
		'code' 		=> '-4091',
		'message'	=> 'Success!',
		'meaning'	=> 'Coupon updated successfully'
	],

	'-4092' => [
		'code' 		=> '-4092',
		'message'	=> 'Success!',
		'meaning'	=> 'Zone added successfully'
	],

	'-4093' => [
		'code' 		=> '-4093',
		'message'	=> 'Success!',
		'meaning'	=> 'Zone edited successfully'
	],

	'-4094' => [
		'code' 		=> '-4094',
		'message'	=> 'Success!',
		'meaning'	=> 'Zone rate added successfully'
	],

	'-4095' => [
		'code' 		=> '-4095',
		'message'	=> 'Success!',
		'meaning'	=> 'Zone rate edited successfully'
	],

	'-4096' => [
		'code' 		=> '-4096',
		'message'	=> 'Success!',
		'meaning'	=> 'Time slot added successfully'
	],

	'-4097' => [
		'code' 		=> '-4097',
		'message'	=> 'Success!',
		'meaning'	=> 'Time Slot edited successfully'
	],

	'-4098' => [
		'code' 		=> '-4098',
		'message'	=> 'Success!',
		'meaning'	=> 'Invoice added successfully'
	],
	'-4099' => [
		'code' 		=> '-4099',
		'message'	=> 'Success!',
		'meaning'	=> 'Invoice edited successfully'
	],
	'-5000' => [
		'code' 		=> '-5000',
		'message'	=> 'Success!',
		'meaning'	=> 'Banner delete successfully'
	],
	'-5001' => [
		'code' 		=> '-5001',
		'message'	=> 'Success!',
		'meaning'	=> 'The order status has not been changed, Because the order has been delivered to the customer.'
	],
	'-5002' => [
		'code' 		=> '-5002',
		'message'	=> 'Success!',
		'meaning'	=> 'The order status has not been changed, Because the order has been canceled.'
	],
	'-5003' => [
		'code' 		=> '-5003',
		'message'	=> 'Success!',
		'meaning'	=> 'Terms and Conditions updated successfully!'
	],
	'-5004' => [
		'code' 		=> '-5004',
		'message'	=> 'Success!',
		'meaning'	=> 'Privacy and Policy updated successfully!'
	],
	'-5005' => [
		'code' 		=> '-5005',
		'message'	=> 'Success!',
		'meaning'	=> 'Banner added successfully!'
	],
	'-5006' => [
		'code' 		=> '-5006',
		'message'	=> 'Success!',
		'meaning'	=> 'Banner updated successfully!'
	],
	'-5007' => [
		'code' 		=> '-5007',
		'message'	=> 'Success!',
		'meaning'	=> 'Otp verified!You have successfully logged in!'
	],
	'-5008' => [
		'code' 		=> '-5008',
		'message'	=> 'Success!',
		'meaning'	=> 'Please pay now!'
	],
	'-5009' => [
		'code' 		=> '-5009',
		'message'	=> 'Success!',
		'meaning'	=> 'This cancelled successfully!'
	],
];