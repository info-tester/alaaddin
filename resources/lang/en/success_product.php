<?php
//jayatri
//english language
return [
	'-800' => [
		'code' 		=> '-800',
		'message'	=> 'Error: ',
		'meaning'	=> 'The combination already exists.'
	],
	'-801' => [
		'code' 		=> '-801',
		'message'	=> 'Success: ',
		'meaning'	=> 'Product status successfully changed.'
	],
	'-802' => [
		'code' 		=> '-802',
		'message'	=> 'Success: ',
		'meaning'	=> 'Product deleted successfully.'
	],
	'-1000' => [
		'code' 		=> '-1000',
		'message'	=> 'Success: ',
		'meaning'	=> 'Product updated successfully!'
	],
	'-4001' => [
		'code' 		=> '-4001',
		'message'	=> 'Success: ',
		'meaning'	=> 'Product successfully saved!'
	],
	'-4002' => [
		'code' 		=> '-4002',
		'message'	=> 'Success: ',
		'meaning'	=> 'Product discount price updated successfully!'
	],
	'-3001' => [
		'code' 		=> '-3001',
		'message'	=> 'Error: ',
		'meaning'	=> 'Must select an image!'
	],
	'-3002' => [
		'code' 		=> '-3002',
		'message'	=> 'Success: ',
		'meaning'	=> 'Product default image set successfully!'
	],
	'-3003' => [
		'code' 		=> '-3003',
		'message'	=> 'Success: ',
		'meaning'	=> 'Product image delete successfully!'
	]
];