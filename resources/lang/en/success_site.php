<?php
return [
	'-15000' => [
		'code' 		=> '-15000',
		'message'	=> 'Success!',
		'meaning'	=> 'Product successfully added to wishlist.'
	],
	'-15001' => [
		'code' 		=> '-15001',
		'message'	=> 'Success!',
		'meaning'	=> 'Product successfully removed from your wishlist.'
	],
	'-15002' => [
		'code' 		=> '-15002',
		'message'	=> 'Success',
		'meaning'	=> 'Product successfully added to cart.'
	],
];